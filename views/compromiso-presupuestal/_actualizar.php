<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'compromiso-presupuestal/actualizar?ID='.$compromisoPresupuestal->ID,
        'options'           => [
            'id'            => 'frmCompromisoPresupuestal',
            'enctype'       =>'multipart/form-data',
        ]
    ]
); ?>
    <input type="hidden" name="CompromisoPresupuestal[Codigo]" value="<?php echo $compromisoPresupuestal->Codigo ?>">
    <div class="modal-body">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-2 control-label">Memorando<span class="f_req">*</span></label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="CompromisoPresupuestal[Memorando]" value="<?= $compromisoPresupuestal->Memorando ?>" required>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-2 control-label">Documento:</label>
                <div class="col-sm-6">
                    <input class="form-control" onchange="return Imagen(this);" type="file" name="CompromisoPresupuestal[archivo]" id="requerimiento-archivo">
                </div>
            </div>
	    <div class="form-group">
                <label class="col-sm-2 control-label">Requerimiento:<span class="f_req">*</span></label>
                <div class="col-sm-6">
                    <select class="form-control" name="CompromisoPresupuestal[RequerimientoID]" required>
			<option value>Seleccionar </option>
			<?php foreach($requerimientos as $requerimiento){ ?>
			<option value="<?= $requerimiento->ID ?>" <?= ($requerimiento->ID==$compromisoPresupuestal->RequerimientoID)?'selected':'';?>>Requerimiento N° <?= str_pad( $requerimiento->Correlativo , 3, "0", STR_PAD_LEFT)  ."-".$requerimiento->Annio."-".$requerimiento->Sigla ?></option>
			<?php } ?>
		    </select>
                </div>
            </div>
        </div>
	<!--
        <div class="form-horizontal">
            <div class="form-group">
                <button type="button" id="agregar_fuente" class="btn btn-primary">Agregar</button><br><br>
                <table class="table borderless table-hover" id="detalle_tabla" border="0">
                    <thead>
                        <tr>
                            <th class="col-sm-4">Tipo de Servicio</th>
                            <th class="col-sm-4">Correlativo</th>
                            <th width="22">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr id='detalle_addr_1'></tr>
                    </tbody>
                </table>
            </div>
        </div>
        -->
            
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button id="btn-save-desembolso" type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>

<?php
    $correlativos=Yii::$app->getUrlManager()->createUrl('compromiso-presupuestal/correlativo-gasto');
?>
<script>
    var $form = $('#frmCompromisoPresupuestal');
    var formParsleyCompromisoPresupuestal = $form.parsley(defaultParsleyForm());
    detalle=1;
    $("#agregar_fuente").click(function(){
	var proveedores=$('input[name=\'CompromisoPresupuestal[Proveedores][]\']').length;
	
	var error = '';
	if (error != '') {
	    $("#error_fuente_origen").html(error);
            return false;
	}
	else
        {
	    var option = null;
            $('#detalle_addr_'+detalle).html(
					    '<td>'+
						'<input type="hidden" name="CompromisoPresupuestal[Cantidades][]" value="'+detalle+'">'+
						'<select class="form-control col-sm-4"  name="CompromisoPresupuestal[TiposGastos][]" onchange="Correlativo($(this).val(),'+detalle+')"><option value>Seleccionar</option><option value="1">Orden de Servicio con RH</option><option value="2">Orden de Servicio con Factura</option><option value="3">Orden de Compra</option><option>Viaticos</option>Caja Chica<option>Encargos</option></select>'+
					    '</td>'+
					    '<td>'+
						'<select class="form-control col-sm-4"  name="CompromisoPresupuestal[Correlativos][]" id="compromisopresupuestal_correlativo_'+detalle+'"></select>'+
					    '</td>');
            $('#detalle_tabla').append('<tr id="detalle_addr_'+(detalle+1)+'"></tr>');
            detalle++;
            return true;
        }
    });
    
    $("#detalle_tabla").on('click','.eliminar',function(){
        var r = confirm("Estas seguro de Eliminar?");
        var mensaje = '';
        if (r == true) {
            id=$(this).children().val();
            if (id) {
                
                $(this).parent().parent().remove();
            }
            else
            {
                $(this).parent().parent().remove();    
            }   
            mensaje = "Se elimino el Registro Correctamente";
        } 
    });
    
    function Correlativo(valor,correlativo) {
        
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $correlativos ?>',
            type: 'POST',
            async: false,
            data: {TipoGasto:valor,_csrf: toke},
            success: function(data){
               $( "#compromisopresupuestal_correlativo_"+correlativo+"" ).html( data );
            }
        });
    }
</script>