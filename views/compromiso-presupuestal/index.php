
<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay_progress.min.js"></script>
<script src=" //cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>


<div id="page-content" style="min-height: 1342px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Compromisos Presupuestales</h1>
        </div>
        
        <div class="container">
            <div class="form-group">
                <button data-codigo-proyecto="<?= $CodigoProyecto ?>" class="btn btn-primary btn-crear-compromiso-presupuestal">Solicitar Compromiso Presupuestal</button>
            </div>
            <div class="table-responsive">
                <table id="proyectos" class="display table table-bordered table-striped table-condensed table-hover" cellspacing="0" >
                    <thead>
                        <tr>
                            <th style='display:none'>ID</th>
                            <th>Corelativo</th>
                            <th>Tipo de Gasto</th>
                            <th>Memorando</th>
                            <th>Fecha Registro</th>
                            <th>Formato</th>
                            <th>Situación</th>
                            <th>Acciones</th>
                            <th>Ubicación</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-ctrl-compromiso-presupuestal" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width: 800px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Compromiso Presupuestal</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-ctrl-compromiso-adjuntar" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Adjuntar Documento</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>
<?php 
$activa=Yii::$app->getUrlManager()->createUrl('compromiso-presupuestal/activar-compromiso');
?>
<script>
    function js_buscar()
    {
       
    }
    var tblresultjs;
        var configDTjs={
            "order": [[ 0, "desc" ]],
            "lengthMenu": [[20, 50, 100, -1], [20, 50, 100, "Todos"]],
                "language": {
                    "sProcessing":    "Procesando...",
                    "sLengthMenu":    "Mostrar _MENU_ registros",
                    "sZeroRecords":   "No se encontraron resultados",
                    "sEmptyTable":    "Ningún dato disponible en esta lista",
                    "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":   "",
                    "sSearch":        "Buscar:",
                    "sUrl":           "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":    "Último",
                        "sNext":    "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
        };
    $(document).ready(function() {
        var CodigoProyecto="<?= $CodigoProyecto ?>";
        $.ajax({
            url:'compromiso-presupuestal/lista-compromisos?CodigoProyecto='+CodigoProyecto,
            async:false,
            data:{},
            beforeSend:function()
            {
                $('.panel-default').LoadingOverlay("show");
            },
            success:function(result)
            {
                    //$tblresultjs.destroy();//
                    $("#proyectos tbody").html(result);
                    $('#proyectos').DataTable(configDTjs);
                    //tblresultjs=$('#example').DataTable(configDTjs);
                    $('.panel-default').LoadingOverlay("hide",true);
            },
            error:function(){
                $('.panel-default').LoadingOverlay("hide",true);
                alert('Error al realizar el proceso de busqueda.');
            }
        });
        
    } );
    
    $('body').on('click', '.btn-crear-compromiso-presupuestal', function (e) {
        e.preventDefault();
        var CodigoProyecto = $(this).attr('data-codigo-proyecto');
        // $('#modal-ctrl-compromiso-presupuestal .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/compromiso-presupuestal/crear?CodigoProyecto='+CodigoProyecto);
        
        AjaxSendForm('<?= \Yii::$app->request->BaseUrl ?>/compromiso-presupuestal/crear', "#modal-ctrl-compromiso-presupuestal .modal-body-main", {'CodigoProyecto':CodigoProyecto}, false);

        $('#modal-ctrl-compromiso-presupuestal').modal('show');
    });
    
    
    
    $('body').on('click', '.btn-edit-compromiso', function (e) {
        e.preventDefault();
        var ID=$(this).attr('data-id');
        // $('#modal-ctrl-compromiso-presupuestal .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/compromiso-presupuestal/actualizar?ID='+ID);
        AjaxSendForm('<?= \Yii::$app->request->BaseUrl ?>/compromiso-presupuestal/actualizar', "#modal-ctrl-compromiso-presupuestal .modal-body-main", {'ID':ID}, false);

        $('#modal-ctrl-compromiso-presupuestal').modal('show');
        
    });

    $(document).on('click','.enviar-aprobacion',function(e){
        e.preventDefault();
        var ID=$(this).attr('data-id');
        // var CodigoProyecto=$(this).attr('data-codigo-proyecto');
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        
        bootBoxConfirm('Esta seguro de enviar el Compromiso Presupuestal',function(){
            $.ajax({
                url: '<?= $activa ?>',
                type: 'POST',
                async: false,
                data: {ID:ID,_csrf: toke},
                success: function (data) {
                    location.reload();
                },
                error: function () {
                    _pageLoadingEnd();
                }
            });
        });
            
    });
    
    
    $('body').on('click', '.btn-remove', function (e) {
        e.preventDefault();
        var $this = $(this);
        bootboxConfirmEliminar($this,
        'Está seguro de eliminar la solicitud de compromiso presupuestal ?',
        function () {
            var id = $this.attr('data-id');
            var toke = '<?=Yii::$app->request->getCsrfToken()?>';
            $.get('<?= \Yii::$app->request->BaseUrl ?>/compromiso-presupuestal/eliminar', { id: id ,_csrf: toke }, function (data) {
                var jx = JSON.parse(data);
                if (jx.Success == true) {
                    location.reload();
                }
            });
        });
    });
    
    $('body').on('click', '.btn-remove-documento', function (e) {
        e.preventDefault();
        var $this = $(this);
        bootboxConfirmEliminar($this,
        '¿Está seguro de eliminar el documento adjunto?',
        function () {
            var id = $this.attr('data-id');
            var toke = '<?=Yii::$app->request->getCsrfToken()?>';
            $.get('<?= \Yii::$app->request->BaseUrl ?>/compromiso-presupuestal/eliminar-adjunto', { id: id ,_csrf: toke }, function (data) {
                var jx = JSON.parse(data);
                if (jx.Success == true) {
                    location.reload();
                }
            });
        });
    });
    
    $('body').on('click', '.btn-adjuntar', function (e) {
        e.preventDefault();
        var ID=$(this).attr('data-id');
        $('#modal-ctrl-compromiso-adjuntar .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/compromiso-presupuestal/adjuntar-documento?ID='+ID);
        $('#modal-ctrl-compromiso-adjuntar').modal('show');
    });
    
    // subir-doc
    var i = 0;
    function AjaxSendForm(url, placeholder,data, append) {
        // var data = $(form).serialize();
        append = (append === undefined ? false : true); 
        $.ajax({
            type: 'GET',
            url: url,
            data: data,
            beforeSend: function() {
                // setting a timeout
                $(placeholder).html("");
                $(placeholder).append('<div class="loading"><i class="fa fa-spinner fa-pulse fa-5x fa-fw"></i></div>');
                i++;
            },
            success: function(data) {
                $(placeholder).html("");
                if (append) {
                    $(placeholder).append(data);
                } else {
                    $(placeholder).html(data);
                }
            },
            error: function(xhr) { // if error occured
                alert("Error occured.please try again");
                $(placeholder).append(xhr.statusText + xhr.responseText);
            },
            complete: function() {
                i--;
                if (i <= 0) {
                    $(".loading").remove();
                }
            },
            dataType: 'html'
        });
    }

</script>

<style type="text/css">
    .modal-body-main .loading{
        text-align: center;
    }

    .loading{
        margin: 40px;
    }
</style>
