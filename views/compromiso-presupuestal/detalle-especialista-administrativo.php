<?php
use app\models\Orden;
use app\models\Siaft;
?>
<div id="page-content" style="min-height: 885px;">
    <div id="wrap">
        <div id="page-heading">
            <h1></h1>
        </div>
        <div class="container">
                    
            <style>
                .panel .panel-body {
                    padding: 10px !important;
                }

                .panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 1px solid #a8a8ab !important;
                }

                .panel-gray .panel-body {
                    border-top: none !important;
                }

                .btn-sm {
                    padding: 5px 10px !important;
                    line-height: 1.5 !important;
                }

                .input-comp-nombre {
                    text-transform: uppercase;
                }

                .tbl-act > tbody > .tr-header > th {
                    text-align: center;
                    font-size: 12px;
                    font-weight: bold;
                    padding: 5px !important;
                    vertical-align: middle !important;
                    /*border: 1px solid #cfcfd0;
                    background-color: #f0f0f1;*/
                    border: 1px solid #c0c0c0;
                    background-color: #e3e3e3;
                }

                /*.tbl-act .tr-item td {
                    padding: 2px !important;
                }*/
                input.form-control, select.form-control {
                    /*height: 28px !important;*/
                }

                .tbl-act .tr-act .input-nombre {
                    min-width: 320px !important;
                }

                .tbl-act .tr-act .input-unidadmedida {
                    min-width: 100px !important;
                }

                .input-number {
                    min-width: 92px !important;
                }

                .btn-add-re {
                    float: right;
                }

                /*.tbl-act > tbody > tr > td:nth-child(2) {
                    min-width: 40px !important;
                    text-align: center !important;
                }*/
                .text-left {
                    text-align: left !important;
                    font-weight: bold;
                }

                .tr-comp input, .tr-proy input {
                    font-weight: bold;
                }

                .th-proy {
                    min-width: 630px !important;
                }
            </style>
            
            <div class="tab-container">
                <ul class="nav nav-tabs">
                    <li id="li-poaf" class="active">
                        <a href="#tab-informacion-general" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Detalle Requerimiento</a>
                    </li>
                </ul>
                <div class="tab-content" style="background: #fff">
                    <div class="tab-pane active" id="tab-informacion-general">
                        <div id="container-poaf">
                            <?php 
                            $mensaje ='';
                            if($cabecera->Situacion==20)
                            {
                                $situacion='Pendiente';
                            }
                            else
                            {
                                $situacion = 'Aprobado';
                            }

                            switch ($cabecera->GastoElegible) {
                                case '1':
                                    $gastos = 'Gasto Elegible';
                                break;
                                case '2':
                                    $gastos = 'Gasto No Elegible';
                                break;
                                case '0':
                                    $gastos = '';
                                break;
                            }
                            ?>
                            <h3>Proyecto <?php echo $cabecera->Codigo ?> / Documento Nº <?php echo $cabecera->Correlativo ?><strong><?= $gastos ?></strong>-<strong><?php echo $situacion ?></strong>
                            <?php echo '<br> Certificación:'.Yii::$app->tools->ObtenerCertificacion($cabecera->Codigo)->CodigoCertificacion;  ?>
                            </h3>

                            <?php if(\Yii::$app->user->identity->rol == 14 || \Yii::$app->user->identity->rol == 13): ?>
                                <h4><strong><?php //echo strtoupper($cabecera->Nombre.' '.$cabecera->ApellidoPaterno) ?></strong></h4>
                            <?php endif; ?>
                            <a target="_blank" class="btn btn-primary" href="<?= \Yii::$app->request->BaseUrl ?>/compromisopresupuestal/<?php echo $cabecera->FormatoUafsi ?>">
                                <span class="fa fa-cloud-download"></span> Descargar compromiso presupuestal
                            </a>

                            <?php if(!empty($cabecera->NotasCompromiso)){
                                $notaColor = 'btn-warning';
                            }else{
                                $notaColor = 'btn-info';
                            }
                                ?>
                            <a target="_blank" data-id="<?= $cabecera->ID ?>" class="btn <?php echo $notaColor ?> btn-obs-nota" href="">
                                <span class="fa fa-address-book"></span> Notas
                            </a>

                            <!-- <a target="_blank" data-id="<?= $cabecera->ID ?>" class="btn btn-default" href="<?= ($cabecera->Formato)?\Yii::$app->request->BaseUrl.'/compromisopresupuestal/'.$cabecera->Formato:''; ?>">
                                <span class="fa fa-cloud-download"></span> Descargar adjunto de compromiso presupuestal!
                            </a> -->

                            <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' data-tipo-gasto='<?php echo $requerimiento->TipoRequerimiento ?>' class='btn btn-danger retraer-compromiso-uafsi'>Retraer a Esp. UAFSI</a>


                            <a href="<?= \Yii::$app->request->BaseUrl ?>/compromiso-presupuestal/especialista-administrativo" class="btn btn-default">VOLVER</a>
                            <br><br>
                          
                           <div id="mensaje">
                               <!-- <div class="alert alert-dismissable alert-danger">
                                    <strong>Errores:</strong>
                                    <ul>
                                        <li>xxxxxxxx.</li>
                                    </ul>
                                </div> -->
                           </div>
                            <!--<a href="#" class="btn btn-primary">Observación</a>-->
                            <hr>

                            <?php if($requerimiento->TipoRequerimiento==1 || $requerimiento->TipoRequerimiento==2 || $requerimiento->TipoRequerimiento==3){?>
                            <table class="table table-hover tbl-act">
                                <tbody>
                                    <tr class="tr-header">
                                        <?php if($cabecera->Situacion!=21){ ?>
                                        <th></th>
                                        <th></th>
                                        <?php } ?>
                                        <th>Tipo</th>
                                        <th>RUC</th>
                                        <th>Razon Social</th>
                                        <th>Monto Total</th>
                                        <th>Formato</th>
                                        <th>Compromiso Anual</th>
                                        <th>Compromiso Mensual</th>
                                    </tr>
                                <?php // echo '<pre>'; print_r($resultados); //die(); ?>
                                    <?php foreach ($resultados as $res): ?>
                                        <tr>
                                            <?php if($cabecera->Situacion!=21){ ?>
                                            <td><input type="checkbox" class="obtener_check" onclick="AgregarCodigo(this,<?= $res['ID'] ?>)" data-detalle-requerimiento-id="<?= $res['ID'] ?>"></td>
                                            <td>
                                                <a data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' data-detalle-compromiso-id="<?= $res['DetalleCompromisoPresupuestalID'] ?>" href="#" class="btn btn-primary btn-observar">Observación</a>
                                            </td>
                                            <?php } ?>
                                            <td><?php echo($res['Nombre']) ?></td>
                                            <td><?php echo($res['RUC']) ?></td>
                                            <td><?php echo($res['RazonSocial']) ?></td>
                                            <!-- <td><?php echo($res["TipoProceso"] == 1 ? 'Comparacion de Precios' : 'Evaluacion de CVs') ?></td> -->
                                            <td><?php echo number_format(($res["Monto"]), 2, '.', ' ') ?></td>
                                            <?php if($res["TipoGasto"]==1){ ?>
                                            <td>
                                                <?php if (!empty($res["Documento"])): ?>
                                                    <a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/ordenServicioFactura/<?= $res["Documento"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Descargar</a>
                                                <?php else: ?>
                                                    <p>No se ha subido documento</p>
                                                <?php endif ?>
                                                <!-- <a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/ordenServicioFactura/<?= $res["Documento"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Descargar</a> -->
                                            </td>
                                            <?php }elseif($res["TipoGasto"]==2){ ?>
                                            <td>
                                                <?php if (!empty($res["Documento"])): ?>
                                                    <a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/ordenServicioRH/<?= $res["Documento"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Descargar</a>
                                                <?php else: ?>
                                                    <p>No se ha subido documento</p>
                                                <?php endif ?>
                                            </td>
                                            <?php }elseif($res["TipoGasto"]==3){ ?>
                                            <td>
                                                <!-- <a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/ordenCompra/<?= $res["Documento"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Descargar </a> -->
                                                <?php if (!empty($res["Documento"])): ?>
                                                    <a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/ordenCompra/<?= $res["Documento"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Descargar</a>
                                                <?php else: ?>
                                                    <p>No se ha subido documento</p>
                                                <?php endif ?>
                                            </td>
                                            <?php } ?>
                                            <?php
                                            $CompAnual = Siaft::find()->where('TipoGastoID=:TipoGastoID AND Orden=:Orden and Fase=9 AND Estado=1',[':TipoGastoID'=>$requerimiento->TipoRequerimiento,':Orden'=>$res['ID']])->one();
                                            $CompMensual = Siaft::find()->where('TipoGastoID=:TipoGastoID AND Orden=:Orden and Fase=10 AND Estado=1',[':TipoGastoID'=>$requerimiento->TipoRequerimiento,':Orden'=>$res['ID']])->one();
                                            if($CompAnual){
                                                $canual = $CompAnual->Siaf."<br>".Yii::$app->tools->validaAnual($CompAnual->Siaf);
                                            }else{
                                                $canual = "--";
                                            }
                                            
                                            if($CompMensual){

                                                $siafX = Yii::$app->administrativo->obtenerSiaf($CompMensual->Siaf,$res['Annio']);
                                                $estadoMensualizado = Yii::$app->tools->validaMensualizado($res['Annio'].$CompMensual->Siaf);
                                                $devengadox = Yii::$app->administrativo->obtenerEstadoDevengado($CompMensual->Siaf,$res['Annio']);
                                                $cmensual = $CompMensual->Siaf."/Siaf: ".$siafX."<br>".$estadoMensualizado.'<br>'.$devengadox;

                                                // $GetExpedienteSiaf=\Yii::$app->db->createCommand('BEGIN DECLARE @a VARCHAR(255); EXEC SP_OBTENER_EXPEDIENTE_SIAF :NUM_REGI_CAB, :ANNO_EJEC_EJE select @a as NUME_SIAF_CAB end;')
                                                //         ->bindValue(':NUM_REGI_CAB',$CompMensual->Siaf)
                                                //         ->bindValue(':ANNO_EJEC_EJE',$res['Annio'])
                                                //         ->queryOne();

                                                // $cmensual = $CompMensual->Siaf."/Siaf: ".$GetExpedienteSiaf["NUME_SIAF_CAB"]."<br>".Yii::$app->tools->validaMensualizado($res['Annio'].$CompMensual->Siaf);
                                                // $cmensual=$CompMensual->Siaf."/Siaf: ".$GetExpedienteSiaf["NUME_SIAF_CAB"];
                                            }else{
                                                $cmensual = "--";
                                            }
                                            ?>
                                            <td><?php echo $canual; ?></td>
                                            <td><?php echo $cmensual; ?></td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                            <?php }elseif($requerimiento->TipoRequerimiento==4){?>
                            <table class="table table-hover tbl-act">
                                <tbody>
                                    <tr class="tr-header">
                                        <?php if($cabecera->Situacion!=21){ ?>
                                        <th></th>
                                        <th></th>
                                        <?php } ?>
                                        <th>Tipo</th>
                                        <th>Correlativo</th>
                                        <th>Datos</th>
                                        <th>DNI</th>
                                        <th>Monto Total</th>
                                        <th>Formato</th>
                                        <th>Compromiso Anual</th>
                                        <th>Compromiso Mensual</th>
                                    </tr>
                                    <?php foreach ($resultados as $res): ?>
                                        <tr>
                                            <?php if($cabecera->Situacion!=21){ ?>
                                            <td><input type="checkbox" class="obtener_check" onclick="AgregarCodigo(this,<?= $res['ID'] ?>)" data-detalle-requerimiento-id="<?= $res['ID'] ?>"></td>
                                            <td>
                                                <a href="#" data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' data-detalle-compromiso-id="<?= $res['DetalleCompromisoPresupuestalID'] ?>" class="btn btn-primary btn-observar">Observación</a>
                                            </td>
                                            <?php } ?>
                                            <td>Viático</td>
                                            <td><?php echo (str_pad($res['Correlativo'], 3, "0", STR_PAD_LEFT)  ."-".$res["Annio"]) ?></td>
                                            <td><?php echo $res['Apellido'].' '.$res['Nombres'] ?></td>
                                            <td><?php echo $res['Dni'] ?></td>
                                            <td>S/.<?php echo $res['Monto'] ?></td>

                                            <td>
                                                <?php if (!empty($res["Documento"])): ?>
                                                    <a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/viatico/<?= $res["Documento"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Descargar</a>
                                                <?php else: ?>
                                                    <p>No se ha subido documento</p>
                                                <?php endif ?>

                                                <!-- <a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/viatico/<?= $res["Documento"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Descarga adjunto</a> -->
                                            </td>

                                            <?php 

                                            $CompAnual = Siaft::find()->where('TipoGastoID=:TipoGastoID AND Orden=:Orden and Fase=9 AND Estado=1',[':TipoGastoID'=>$requerimiento->TipoRequerimiento,':Orden'=>$res['ID']])->one();
                                            $CompMensual = Siaft::find()->where('TipoGastoID=:TipoGastoID AND Orden=:Orden and Fase=10 AND Estado=1',[':TipoGastoID'=>$requerimiento->TipoRequerimiento,':Orden'=>$res['ID']])->one();
                                            if($CompAnual){
                                                // $canual = $CompAnual->Siaf;
                                                $canual = $CompAnual->Siaf."<br>".Yii::$app->tools->validaAnual($CompAnual->Siaf);
                                            }else{
                                                $canual = "--";
                                            }
                                            
                                            if($CompMensual){
                                                $siafX = Yii::$app->administrativo->obtenerSiaf($CompMensual->Siaf,$res['Annio']);
                                                $estadoMensualizado = Yii::$app->tools->validaMensualizado($res['Annio'].$CompMensual->Siaf);
                                                $devengadox = Yii::$app->administrativo->obtenerEstadoDevengado($CompMensual->Siaf,$res['Annio']);
                                                $cmensual = $CompMensual->Siaf."/Siaf: ".$siafX."<br>".$estadoMensualizado.'<br>'.$devengadox;

                                            }else{
                                                $cmensual = "--";
                                            }
                                            ?>
                                            <td><?php echo $canual; ?></td>
                                            <td><?php echo $cmensual; ?></td>
                                            
                                        </tr>
                                        <?php $compromisoID = $res['CompromisoPresupuestalID']; ?>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                            <?php }elseif($requerimiento->TipoRequerimiento==5){?>
                            <table class="table table-hover tbl-act">
                                <tbody>
                                    <tr class="tr-header">
                                        <?php if($cabecera->Situacion!=21){ ?>
                                        <th></th>
                                        <th></th>
                                        <?php } ?>
                                        <th>Tipo de Gasto</th>
                                        <th>Tipo</th>
                                        <th>Bienes</th>
                                        <th>Servicios</th>
                                        <th>Monto Total</th>
                                        <th>Formato</th>
                                        <th>Compromiso Anual</th>
                                        <th>Compromiso Mensual</th>
                                    </tr>

                                    <?php foreach ($resultados as $res): ?>
                                    <tr>
                                        <?php if($cabecera->Situacion!=21){ ?>
                                        <td><input type="checkbox" class="obtener_check" onclick="AgregarCodigo(this,<?= $res['ID'] ?>)" data-detalle-requerimiento-id="<?= $res['ID'] ?>"></td>
                                        <td>
                                            <a href="#" data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' data-detalle-compromiso-id="<?= $res['DetalleCompromisoPresupuestalID'] ?>" class="btn btn-primary btn-observar">Observación</a>
                                        </td>
                                        <?php } ?>
                                        <td>Caja Chica</td>
                                        <td><?php echo($res['Tipo']==1)?'Apertura':'Desembolso'; ?></td>
                                        <td><?php echo number_format(($res['Bienes']), 2, '.', ' ') ?></td>
                                        <td><?php echo number_format(($res['Servicios']), 2, '.', ' ') ?></td>
                                        <td><?php echo number_format(($res['Servicios']+$res['Bienes']), 2, '.', ' ') ?></td>
                                        <td>
                                            <?php if (!empty($res["Documento"])): ?>
                                                    <a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/cajachica/<?= $res["Documento"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Descargar</a>
                                                <?php else: ?>
                                                    <p style="color: red">- No se ha subido formato </p>
                                                <?php endif ?>

                                                <?php if (!empty($res["ResolucionDirectorial"])): ?>
                                                    <a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/cajachica/<?= $res["ResolucionDirectorial"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Resolución Directorial</a>
                                                <?php else: ?>
                                                    <p style="color: red">No se ha subido Resolución</p>
                                                <?php endif ?>
                                            <!-- <a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/cajachica/<?= $res["ResolucionDirectorial"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Descarga adjunto</a> -->
                                        </td>

                                        <?php 

                                        $CompAnual = Siaft::find()->where('TipoGastoID=:TipoGastoID AND Orden=:Orden and Fase=9 AND Estado=1',[':TipoGastoID'=>$requerimiento->TipoRequerimiento,':Orden'=>$res['ID']])->one();
                                        $CompMensual = Siaft::find()->where('TipoGastoID=:TipoGastoID AND Orden=:Orden and Fase=10 AND Estado=1',[':TipoGastoID'=>$requerimiento->TipoRequerimiento,':Orden'=>$res['ID']])->one();
                                        if(count($CompAnual) != 0){
                                            // echo $CompAnual->Siaf;die();
                                            $canual = $CompAnual->Siaf."<br>".Yii::$app->tools->validaAnual($CompAnual->Siaf);
                                        }else{
                                            $canual = "--";
                                        }
                                        
                                        if($CompMensual){
                                            $siafX = Yii::$app->administrativo->obtenerSiaf($CompMensual->Siaf,$res['Annio']);
                                            $estadoMensualizado = Yii::$app->tools->validaMensualizado($res['Annio'].$CompMensual->Siaf);
                                            $devengadox = Yii::$app->administrativo->obtenerEstadoDevengado($CompMensual->Siaf,$res['Annio']);
                                            $cmensual = $CompMensual->Siaf."/Siaf: ".$siafX."<br>".$estadoMensualizado.'<br>'.$devengadox;
                                        }else{
                                            $cmensual = "--";
                                        }
                                        ?>
                                        <td><?php echo $canual; ?></td>
                                        <td><?php echo $cmensual; ?></td>
                                    </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                            <?php $compromisoID = $cabecera->ID; ?>
                            <?php }elseif($requerimiento->TipoRequerimiento==6){?>
                            <table class="table table-hover tbl-act">
                                <tbody>
                                    <tr class="tr-header">
                                        <?php if($cabecera->Situacion!=21){ ?>
                                        <th></th>
                                        <th></th>
                                        <?php } ?>
                                        <th>Tipo de Gasto</th>
                                        <th>Bienes</th>
                                        <th>Servicios</th>
                                        <th>Monto Total</th>
                                        <th>Formato</th>
                                        <th>Compromiso Anual</th>
                                        <th>Compromiso Mensual</th>
                                    </tr>
                                    <?php foreach ($resultados as $res): ?>
                                    <tr>
                                        <?php if($cabecera->Situacion!=21){ ?>
                                        <td><input type="checkbox" class="obtener_check" onclick="AgregarCodigo(this,<?= $res['ID'] ?>)" data-detalle-requerimiento-id="<?= $res['ID'] ?>"></td>
                                        <td>
                                            <a data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' data-detalle-compromiso-id="<?= $res['DetalleCompromisoPresupuestalID'] ?>" href="#" class="btn btn-primary btn-observar">Observación</a>
                                        </td>
                                        <?php } ?>
                                        <td>Encargos</td>
                                        <td><?php echo number_format(($res['Bienes']), 2, '.', ' ') ?></td>
                                        <td><?php echo number_format(($res['Servicios']), 2, '.', ' ') ?></td>
                                        <td><?php echo number_format(($res['Servicios']+$res['Bienes']), 2, '.', ' ') ?></td>
                                        <td>
                                            <?php if (!empty($res["Documento"])): ?>
                                                <a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/encargos/<?= $res["Documento"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Descargar</a>
                                            <?php else: ?>
                                                <p>No se ha subido documento</p>
                                            <?php endif ?>

                                            <?php if (!empty($res["ResolucionDirectorial"])): ?>
                                                <a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/encargos/<?= $res["ResolucionDirectorial"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Resolución Directorial</a>
                                            <?php else: ?>
                                                <p>No se ha subido resolución directorial</p>
                                            <?php endif ?>

                                            <!-- <a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/encargos/<?= $res["Documento"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Descarga adjunto</a> -->
                                        </td>
                                        <?php 
                                        $CompAnual = Siaft::find()->where('TipoGastoID=:TipoGastoID AND Orden=:Orden and Fase=9 AND Estado=1',[':TipoGastoID'=>$requerimiento->TipoRequerimiento,':Orden'=>$res['ID']])->one();
                                        $CompMensual = Siaft::find()->where('TipoGastoID=:TipoGastoID AND Orden=:Orden and Fase=10 AND Estado=1',[':TipoGastoID'=>$requerimiento->TipoRequerimiento,':Orden'=>$res['ID']])->one();

                                        if($CompAnual){
                                            // $canual = $CompAnual->Siaf;
                                            $canual = $CompAnual->Siaf."<br>".Yii::$app->tools->validaAnual($CompAnual->Siaf);
                                        }else{
                                            $canual = "--";
                                        }
                                        
                                        if($CompMensual){
                                            $siafX = Yii::$app->administrativo->obtenerSiaf($CompMensual->Siaf,$res['Annio']);
                                            $estadoMensualizado = Yii::$app->tools->validaMensualizado($res['Annio'].$CompMensual->Siaf);
                                            $devengadox = Yii::$app->administrativo->obtenerEstadoDevengado($CompMensual->Siaf,$res['Annio']);
                                            $cmensual = $CompMensual->Siaf."/Siaf: ".$siafX."<br>".$estadoMensualizado.'<br>'.$devengadox;
                                        }else{
                                            $cmensual = "--";
                                        }
                                        ?>
                                        <td><?php echo $canual; ?></td>
                                        <td><?php echo $cmensual; ?></td>
                                    </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                            <?php $compromisoID = $cabecera->ID; ?>
                            <?php } ?>
                        </div>
                        <?php if(\Yii::$app->user->identity->rol == 14): ?>
                            
                            <?php if($cabecera->Situacion == 20 && ($requerimiento->TipoRequerimiento == 1 ||$requerimiento->TipoRequerimiento == 2 || $requerimiento->TipoRequerimiento == 3) ){ ?>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-primary generar-compromiso'>Generar Compromiso Anual</a>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' data-tipo-gasto='<?php echo $requerimiento->TipoRequerimiento ?>' class='btn btn-danger eliminar-compromiso-anual'>Eliminar Compromiso Anual</a>
                                
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-success generar-compromiso-administrativo'>Generar Compromiso Mensualizado</a>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' data-tipo-gasto='<?php echo $requerimiento->TipoRequerimiento ?>' class='btn btn-danger eliminar-compromiso-mensualizado'>Eliminar Compromiso Mensualizado</a>
                                
                                <!-- <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-primary generar-devengado'>Transferir al Integra</a><br><br> -->
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-primary finalizar-compromiso'>Finalizar</a>
                                
                                <?php if($cabecera->CompromisoAnual!=1 ){ /*?>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-primary generar-compromiso'>Generar Compromiso Anual</a>
                                <?php }elseif($cabecera->CompromisoAnual==1 && $cabecera->CompromisoAdministrativo!=1){?>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-success generar-compromiso-administrativo'>Generar Compromiso Mensualizado</a>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-danger eliminar-compromiso-anual'>X Compromiso Anual</a>
                                <?php }elseif($cabecera->CompromisoAdministrativo==1){?>
                                <!--<a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-primary generar-devengado'>Generar Devengado</a>-->
                                <?php */ } ?>
                            <?php } ?>
                            <?php if($cabecera->Situacion == 20 && ($requerimiento->TipoRequerimiento == 4) ){ ?>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-primary generar-compromiso'>Generar Compromiso Anual</a>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' data-tipo-gasto='<?php echo $requerimiento->TipoRequerimiento ?>' class='btn btn-danger eliminar-compromiso-anual'>Eliminar Compromiso Anual</a>
                                
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-success generar-compromiso-administrativo'>Generar Compromiso Mensualizado</a>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' data-tipo-gasto='<?php echo $requerimiento->TipoRequerimiento ?>' class='btn btn-danger eliminar-compromiso-mensualizado'>Eliminar Compromiso Mensualizado</a>
                                
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-primary generar-devengado'>Transferir al Integra</a>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-primary finalizar-compromiso'>Finalizar</a>
                                <?php if($cabecera->CompromisoAnual!=1 ){ /*?>
                                
                                <?php }elseif($cabecera->CompromisoAnual==1 && $cabecera->CompromisoAdministrativo!=1){?>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-success generar-compromiso-administrativo'>Generar Compromiso Mensualizado</a>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-danger eliminar-compromiso-anual'>X Compromiso Anual</a>
                                <?php }elseif($cabecera->CompromisoAdministrativo==1 && $cabecera->Devengado!=1){?>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-primary generar-devengado'>Generar Devengado</a>
                                <?php */ } ?>
                            <?php } ?>
                            <?php if($cabecera->Situacion == 20 && ($requerimiento->TipoRequerimiento == 5) ){ ?>
                                
                                <?php if($cabecera->CompromisoAnual!=1 ){ ?>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-primary generar-compromiso'>Generar Compromiso Anual</a>
                                <?php }elseif($cabecera->CompromisoAnual==1 && $cabecera->CompromisoAdministrativo!=1){?>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' data-tipo-gasto='<?php echo $requerimiento->TipoRequerimiento ?>' class='btn btn-danger eliminar-compromiso-anual'>Eliminar Compromiso Anual</a>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-success generar-compromiso-administrativo'>Generar Compromiso Mensualizado</a>
                                <?php }elseif($cabecera->CompromisoAdministrativo==1 && $cabecera->Devengado!=1){?>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' data-tipo-gasto='<?php echo $requerimiento->TipoRequerimiento ?>' class='btn btn-danger eliminar-compromiso-mensualizado'>Eliminar Compromiso Mensualizado</a>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-primary generar-devengado'>Transferir al Integra</a>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-primary finalizar-compromiso'>Finalizar</a>
                                <?php  } ?>
                            <?php } ?>
                            <?php if($cabecera->Situacion == 20 && ($requerimiento->TipoRequerimiento == 6) ){ ?>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-primary generar-compromiso'>Generar Compromiso Anual</a>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' data-tipo-gasto='<?php echo $requerimiento->TipoRequerimiento ?>' class='btn btn-danger eliminar-compromiso-anual'>Eliminar Compromiso Anual</a>
                                
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-success generar-compromiso-administrativo'>Generar Compromiso Mensualizado</a>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' data-tipo-gasto='<?php echo $requerimiento->TipoRequerimiento ?>' class='btn btn-danger eliminar-compromiso-mensualizado'>Eliminar Compromiso Mensualizado</a>
                                
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-primary generar-devengado'>Transferir al Integra</a>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-primary finalizar-compromiso'>Finalizar</a>
                                
                                <?php if($cabecera->CompromisoAnual!=1 ){ /*?>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-primary generar-compromiso'>Generar Compromiso Anual</a>
                                <?php }elseif($cabecera->CompromisoAnual==1 && $cabecera->CompromisoAdministrativo!=1){?>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-success generar-compromiso-administrativo'>Generar Compromiso Mensualizado</a>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-danger eliminar-compromiso-anual'>X Compromiso Anual</a>
                                <?php }elseif($cabecera->CompromisoAdministrativo==1 && $cabecera->Devengado!=1){?>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-primary generar-devengado'>Generar Devengado</a>
                                <?php */ } ?>
                            <?php } ?>

                            <?php if($cabecera->Situacion == 21 && ($requerimiento->TipoRequerimiento == 1) ){ ?>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' data-tipo-gasto='<?php echo $requerimiento->TipoRequerimiento ?>' class='btn btn-warning retraer-compromiso'>Re hacer Compromiso</a>
                            <?php } ?>

                            <?php if($cabecera->Situacion == 21 && ($requerimiento->TipoRequerimiento == 2) ){ ?>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' data-tipo-gasto='<?php echo $requerimiento->TipoRequerimiento ?>' class='btn btn-warning retraer-compromiso'>Re hacer Compromiso</a>
                            <?php } ?>

                            <?php if($cabecera->Situacion == 21 && ($requerimiento->TipoRequerimiento == 3) ){ ?>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' data-tipo-gasto='<?php echo $requerimiento->TipoRequerimiento ?>' class='btn btn-warning retraer-compromiso'>Re hacer Compromiso</a>
                            <?php } ?>

                            <?php if($cabecera->Situacion == 21 && ($requerimiento->TipoRequerimiento == 4) ){ ?>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' data-tipo-gasto='<?php echo $requerimiento->TipoRequerimiento ?>' class='btn btn-warning retraer-compromiso'>Re hacer Compromiso</a>
                            <?php } ?>

                            <?php if($cabecera->Situacion == 21 && ($requerimiento->TipoRequerimiento == 5) ){ ?>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' data-tipo-gasto='<?php echo $requerimiento->TipoRequerimiento ?>' class='btn btn-warning retraer-compromiso'>Re hacer Compromiso</a>
                            <?php } ?>

                            <?php if($cabecera->Situacion == 21 && ($requerimiento->TipoRequerimiento == 6) ){ ?>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' data-tipo-gasto='<?php echo $requerimiento->TipoRequerimiento ?>' class='btn btn-warning retraer-compromiso'>Re hacer Compromiso</a>
                            <?php } ?>
                        <?php endif ?>
                        
                    </div>

                </div>
            </div>


        </div>
    </div>
</div>

<div id="modal-loading" class="modal fade modal-loading in" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-hidden="false" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content-no-shadow modal-content">
            <div class="text-center"><i class="fa fa-spinner fa-pulse fa-fw" style="color:#16a085;font-size: 100px;"></i></div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-ctrl-observacion" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Compromiso</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-ctrl-observacion-nota" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Nota</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-financiamiento" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Financiamiento</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<?php 
    $eliminiarcompro = Yii::$app->getUrlManager()->createUrl('compromiso-presupuestal/eliminar-compromiso-anual');
    $eliminiarcompromensualizado = Yii::$app->getUrlManager()->createUrl('compromiso-presupuestal/eliminar-compromiso-mensualizado');
    $compro = Yii::$app->getUrlManager()->createUrl('compromiso-presupuestal/generar-compromiso-anual');
    $devengado = Yii::$app->getUrlManager()->createUrl('compromiso-presupuestal/generar-devengado');
    $administrativo = Yii::$app->getUrlManager()->createUrl('compromiso-presupuestal/generar-administrativo');
    $finalizar = Yii::$app->getUrlManager()->createUrl('compromiso-presupuestal/finalizar-compromiso');
    $rehacer = Yii::$app->getUrlManager()->createUrl('compromiso-presupuestal/rehacer-compromiso');
    $rehacerUafsi = Yii::$app->getUrlManager()->createUrl('compromiso-presupuestal/rehacer-compromiso-uafsi');

    // Financiamiento
    $financia = Yii::$app->getUrlManager()->createUrl('compromiso-presupuestal/financiamiento-fuente-saldo');

?>



<script>
    var Codigos=[];
    function AgregarCodigo(elemento,ID) {
        if ($(elemento)[0].checked) {
            Codigos.push(ID);
        }
        else
        {
            Codigos = jQuery.grep(Codigos, function(value) {
                return value != ID;
            });     
        }
        console.log(Codigos);
    }
    
    $(document).ready(function(){
       $('.fa-search').popover(); 
    });
    
    $('[data-rel=tooltip]').tooltip();
    
    $(document).on('click','.generar-compromiso',function(){
        var ID=$(this).attr('data-id');
        var CodigoProyecto=$(this).attr('data-codigo-proyecto');
        
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        if (Codigos.length==0) {
            bootbox.alert('Debe seleccionar un registro');
            return false;
        }
        console.log(Codigos);
        AjaxSendForm('<?= \Yii::$app->request->BaseUrl ?>/compromiso-presupuestal/financiamiento', "#modal-financiamiento .modal-body-main", {'ID':ID,'CodigoProyecto':CodigoProyecto,'CodigosIDs':Codigos,'_csrf': toke}, false,'POST');
        $('#modal-financiamiento').modal('show');

        // bootBoxConfirm('Esta seguro de Generar el Compromiso Anual',function(){
        //     $.ajax({
        //         url: '<?= $compro ?>',
        //         type: 'POST',
        //         async: false,
        //         data: {ID:ID,CodigoProyecto:CodigoProyecto,CodigosIDs:Codigos,_csrf: toke},
        //         success: function (data) {
        //             // location.reload();
        //         },
        //         error: function () {
        //             _pageLoadingEnd();
        //         }
        //     });
        // });

    });
    
    $(document).on('click','.eliminar-compromiso-anual',function(){
        var ID=$(this).attr('data-id');
        var CodigoProyecto=$(this).attr('data-codigo-proyecto');
        var TipoGasto=$(this).attr('data-tipo-gasto');
        
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        if (Codigos.length==0) {
            bootbox.alert('Debe seleccionar un registro');
            return false;
        }
            
        bootBoxConfirm('¿Esta seguro de eliminar el compromiso anual generado?',function(){
            $.ajax({
                url: '<?= $eliminiarcompro ?>',
                type: 'POST',
                async: false,
                data: {ID:ID,CodigoProyecto:CodigoProyecto,Codigos:Codigos,TipoGasto:TipoGasto,_csrf: toke},
                success: function (data) {
                    // location.reload();
                },
                error: function () {
                    _pageLoadingEnd();
                }
            });
        });
        //}
    });

    $(document).on('click','.retraer-compromiso',function(){
        var ID=$(this).attr('data-id');
        var CodigoProyecto=$(this).attr('data-codigo-proyecto');
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        bootBoxConfirm('¿Esta seguro de re hacer?',function(){
            $.ajax({
                url: '<?= $rehacer ?>',
                type: 'POST',
                async: false,
                data: {ID:ID,CodigoProyecto:CodigoProyecto,_csrf: toke},
                success: function (data) {
                    location.reload();
                },
                error: function () {
                    _pageLoadingEnd();
                }
            });
        });
    });

    // Retraer a Especialista UAFSI
    $(document).on('click','.retraer-compromiso-uafsi',function(){
        var ID=$(this).attr('data-id');
        var CodigoProyecto=$(this).attr('data-codigo-proyecto');
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        bootBoxConfirm('¿Esta seguro de enviar al especialista UAFSI?',function(){
            $.ajax({
                url: '<?= $rehacerUafsi ?>',
                type: 'POST',
                async: false,
                data: {ID:ID,CodigoProyecto:CodigoProyecto,_csrf: toke},
                success: function (data) {
                    // location.reload();
                    location.href = "<?php echo Yii::$app->getUrlManager()->createUrl('compromiso-presupuestal/especialista-administrativo')?>"
                },
                error: function () {
                    _pageLoadingEnd();
                }
            });
        });
    });


    // Fuente Financiamiento
    $(document).on('change','#fuenteFinanciamiento',function(e){
        e.preventDefault();
        var certificacion   = $(this).attr('data-certificacion');
        var annio           = $(this).attr('data-annio');
        var fuente          = $(this).val();
        var toke            = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $financia ?>',
            type: 'GET',
            dataType:'json',
            beforeSend:function(){
                $("#loading").html("");
                $('#saldo').text();
                $("#loading").append('<div class="loading"><i class="fa fa-spinner fa-pulse fa-5x fa-fw"></i></div>');
            },
            data: {certificacion:certificacion,annio:annio,fuente:fuente,_csrf: toke},
            success: function (data) {
                console.log(data);
                $("#loading").html("");
                $('#saldo').text("Saldo en Fuente de Financiamiento: S/. "+ data.Total);
                $('#saldoFinanciamiento').val(data.Total);

                if(data.Total != 0){
                    $('#CorrelativoFuente').removeAttr('disabled','disabled');
                }else{
                    $('#btn-save-desembolso').attr('disabled','disabled');
                }

                var corr = data.Secuencial;
                if(corr.length != 0){
                    var option="";
                    $('#CorrelativoFuente').html("");
                        option += "<option value>Seleccione</option>";
                    $(corr).each(function (index, value) {
                        option += "<option value='"+value.Secuencial+"'>"+value.Secuencial+"</option>";
                    });
                    $('#CorrelativoFuente').append(option);
                    // $('#btn-save-desembolso').removeAttr('disabled','disabled');
                }
            },
            error: function () {
                // _pageLoadingEnd();
            }
        });
    });

    $(document).on('change','#CorrelativoFuente',function(e){
        e.preventDefault();
        var correlativoX = $(this).val();
        console.log(correlativoX);
        var saldo = $('#saldoFinanciamiento').val();
        if(correlativoX == ''){
            $('#btn-save-desembolso').attr('disabled','disabled');
        }

        if(saldo == 0){
            $('#btn-save-desembolso').attr('disabled','disabled');
        }else{
            $('#btn-save-desembolso').removeAttr('disabled','disabled');
        }
    });

    $(document).on('click','.eliminar-compromiso-mensualizado',function(){
        var ID=$(this).attr('data-id');
        var CodigoProyecto=$(this).attr('data-codigo-proyecto');
        var TipoGasto=$(this).attr('data-tipo-gasto');
        
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        if (Codigos.length==0) {
            bootbox.alert('Debe seleccionar un registro');
            return false;
        }
            
        bootBoxConfirm('¿Esta seguro de eliminar el compromiso mensualizado generado?',function(){
            $.ajax({
                url: '<?= $eliminiarcompromensualizado ?>',
                type: 'POST',
                async: false,
                data: {ID:ID,CodigoProyecto:CodigoProyecto,Codigos:Codigos,TipoGasto:TipoGasto,_csrf: toke},
                success: function (data) {
                    location.reload();
                },
                error: function () {
                    _pageLoadingEnd();
                }
            });
        });
    });
    
    $(document).on('click','.generar-devengado',function(){
        var ID=$(this).attr('data-id');
        var CodigoProyecto=$(this).attr('data-codigo-proyecto');
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        if (Codigos.length==0) {
            bootbox.alert('Debe seleccionar un registro');
            return false;
        }
        bootBoxConfirm('Esta seguro de Generar el Devengado',function(){
            $.ajax({
                url: '<?= $devengado ?>',
                type: 'POST',
                async: false,
                data: {ID:ID,CodigoProyecto:CodigoProyecto,CodigosIDs:Codigos,_csrf: toke},
                success: function (data) {
                    location.reload();
                },
                error: function () {
                    _pageLoadingEnd();
                }
            });
        });
    });
    
    $(document).on('click','.generar-compromiso-administrativo',function(e){
        e.preventDefault();
        var ID=$(this).attr('data-id');
        var CodigoProyecto=$(this).attr('data-codigo-proyecto');
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        if (Codigos.length==0) {
            bootbox.alert('Debe seleccionar un registro');
            return false;
        }
        
        
        bootBoxConfirm('Esta seguro de Generar el Compromiso Administrativo',function(){
            $.ajax({
                url: '<?= $administrativo ?>',
                type: 'POST',
                dataType: 'JSON',
                async: false,
                data: {ID:ID,CodigoProyecto:CodigoProyecto,CodigosIDs:Codigos,_csrf: toke},
                success: function (data) {
                    if(data.Success == true){
                        // location.reload();
                    }else{
                        showMessageErrorResponse(data.Mensaje,$('#mensaje'));
                    }
                },
                error: function () {
                    _pageLoadingEnd();
                }
            });
        });
    });
    
    $(document).on('click','.finalizar-compromiso',function(){
        var ID=$(this).attr('data-id');
        var CodigoProyecto=$(this).attr('data-codigo-proyecto');
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        bootBoxConfirm('¿Esta seguro de Finalizar?',function(){
            $.ajax({
                url: '<?= $finalizar ?>',
                type: 'POST',
                async: false,
                data: {ID:ID,CodigoProyecto:CodigoProyecto,_csrf: toke},
                success: function (data) {
                    location.reload();
                },
                error: function () {
                    _pageLoadingEnd();
                }
            });
        });
    });
    
    
    $('body').on('click', '.btn-observar', function (e) {
        e.preventDefault();
        var CodigoProyecto=$(this).attr('data-codigo-proyecto');
        var DetalleCompromisoID=$(this).attr('data-detalle-compromiso-id');
        var ID=<?php echo $cabecera->ID ?>;
        $('#modal-ctrl-observacion .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/compromiso-presupuestal/observar-compromiso?ID='+ID+'&DetalleCompromisoID='+DetalleCompromisoID+'&CodigoProyecto='+CodigoProyecto+'&Tipo=1');
        $('#modal-ctrl-observacion').modal('show');
    });


    $('body').on('click', '.btn-obs-nota', function (e) {
        e.preventDefault();
        var ID = $(this).attr('data-id');

        AjaxSendForm('<?= \Yii::$app->request->BaseUrl ?>/compromiso-presupuestal/crear-nota', "#modal-ctrl-observacion-nota .modal-body-main", {'ID':ID}, false ,'GET');
        $('#modal-ctrl-observacion-nota').modal('show');
    });


    function AjaxSendForm(url, placeholder,data, append, type) {
        var i = 0;
        // var data = $(form).serialize();
        append = (append === undefined ? false : true); 
        $.ajax({
            type: type,
            url: url,
            data: data,
            beforeSend: function() {
                // setting a timeout
                $(placeholder).html("");
                $(placeholder).append('<div class="loading"><i class="fa fa-spinner fa-pulse fa-5x fa-fw"></i></div>');
                i++;
            },
            success: function(data) {
                $(placeholder).html("");
                if (append) {
                    $(placeholder).append(data);
                } else {
                    $(placeholder).html(data);
                }
            },
            error: function(xhr) { // if error occured
                alert("Error occured.please try again");
                $(placeholder).append(xhr.statusText + xhr.responseText);
            },
            complete: function() {
                i--;
                if (i <= 0) {
                    $(".loading").remove();
                }
            },
            dataType: 'html'
        });
    }

</script>