<?php
use app\models\Orden;
use app\models\Siaft;
?>

<div id="page-content" style="min-height: 885px;">
    <div id="wrap">
        <div id="page-heading">
            <h1></h1>
        </div>
        <div class="container">
                    
            <style>
                .panel .panel-body {
                    padding: 10px !important;
                }

                .panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 1px solid #a8a8ab !important;
                }

                .panel-gray .panel-body {
                    border-top: none !important;
                }

                .btn-sm {
                    padding: 5px 10px !important;
                    line-height: 1.5 !important;
                }

                .input-comp-nombre {
                    text-transform: uppercase;
                }

                .tbl-act > tbody > .tr-header > td {
                    text-align: center;
                    font-size: 12px;
                    font-weight: bold;
                    padding: 5px !important;
                    vertical-align: middle !important;
                    /*border: 1px solid #cfcfd0;
                    background-color: #f0f0f1;*/
                    border: 1px solid #c0c0c0;
                    background-color: #e3e3e3;
                }

                /*.tbl-act .tr-item td {
                    padding: 2px !important;
                }*/
                input.form-control, select.form-control {
                    /*height: 28px !important;*/
                }

                .tbl-act .tr-act .input-nombre {
                    min-width: 320px !important;
                }

                .tbl-act .tr-act .input-unidadmedida {
                    min-width: 100px !important;
                }

                .input-number {
                    min-width: 92px !important;
                }

                .btn-add-re {
                    float: right;
                }

                /*.tbl-act > tbody > tr > td:nth-child(2) {
                    min-width: 40px !important;
                    text-align: center !important;
                }*/
                .text-left {
                    text-align: left !important;
                    font-weight: bold;
                }

                .tr-comp input, .tr-proy input {
                    font-weight: bold;
                }

                .th-proy {
                    min-width: 630px !important;
                }
            </style>

            <div class="tab-container">
                <ul class="nav nav-tabs">
                    <li id="li-poaf" class="active">
                        <a href="#tab-informacion-general" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Detalle de Solicitud de compromiso</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-informacion-general">
                        <div id="container-poaf">
                            <?php 
                                switch ($cabecera->GastoElegible) {
                                    case '1':
                                        $gastos = 'Gasto Elegible';
                                    break;
                                    case '2':
                                        $gastos = 'Gasto No Elegible';
                                    break;
                                    case '0':
                                        $gastos = '';
                                    break;
                                }
                                ?>
                                <h3>Proyecto <?php echo $cabecera->Codigo ?> / Documento Nº <?php echo $cabecera->Correlativo ?> <strong><?php //echo $situacion ?></strong></h3>
                                <?php if($observarJefeContabilidad){ ?>
                                <p>Observación del Jefe(a) de Contabilidad</p>
                                <p style="color: red"><?= $observarJefeContabilidad->Observacion ?></p>
                                <?php } ?>
                                
                                <?php if($observarAdministrativo){ ?>
                                    <p class="red">Observación del Especialista administrativo</p>
                                    <ul>
                                        <?php foreach ($observarAdministrativo as $obsad): ?>
                                            <li class="red">
                                                <?php echo $obsad['CodigoProyecto'].'. <strong>#'.$obsad['ID'].'</strong> - '.$obsad['Observacion'] ?> 
                                                <?php if (empty($obsad['Respuesta'])): ?>
                                                    <!-- <a href="#" class="btn btn-danger btn-sm observacion-responder">Responder Observación</a> -->
                                                    <!-- <a data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' data-detalle-compromiso-id="" href="#" class="btn btn-danger btn-sm btn-ver-observar">Responder Observación</a -->
                                                <?php endif ?>
                                            </li>
                                        <?php endforeach ?>
                                    </ul>
                                <?php } ?>

                                
                                <br>
                                <?php if(\Yii::$app->user->identity->rol == 14 || \Yii::$app->user->identity->rol == 13): ?>
                                    <?php if($cabecera->UsuarioAsignado == ''): ?>
                                        <form class="form-horizontal">
                                            <div class="col-md-6">
                                                <label class="form-label">Gasto</label>
                                                <select name="gasto" id="gastos" class="form-control" required="" data-id="<?php echo $cabecera->ID ?>">
                                                    <option value="">[SELECCIONE]</option>
                                                    <?php foreach ($res as $ue) { ?>
                                                        <option value="<?php echo $ue['ID'] ?>"><?php echo $ue['Nombre'] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>

                                            <div class="col-md-12">
                                                <br>
                                                <a href="javascript:;" class="btn btn-primary" id="verifica">ENVIAR</a>
                                            </div>
                                        </form>
                                    <?php else: ?>
                                        <h4><strong><?php echo strtoupper($cabecera->Nombre) ?></strong></h4>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <?php 
                                if($cabecera->Observacion)
                                {
                                    echo "Observación:<p>".$cabecera->Observacion."</p>";
                                }
                                ?>
                            <br>
                            <?php if($requerimiento->TipoRequerimiento==1 || $requerimiento->TipoRequerimiento==2 || $requerimiento->TipoRequerimiento==3){?>
                            <table class="table table-hover tbl-act">
                                <tbody>
                                    <tr class="tr-header">
                                        <td>Expediente SIAF</td>
                                        <td></td>
                                        <td>Tipo</td>
                                        <td>Correlativo</td>
                                        <td>Fecha Orden</td>
                                        <td>Tipo de Proceso</td>
                                        <td>Monto Total</td>
                                        <td>Formato</td>
                                        <td>Observación</td>
                                    </tr>
                                    <?php //echo '<pre>';print_r($resultados); die(); ?>
                                    <?php foreach ($resultados as $res): ?>
                                        <tr>
                                            <?php $expedientSiaf=""; ?>
                                            
                                            
                                            <?php 
                                            $CompAnual = Siaft::find()->where('TipoGastoID=:TipoGastoID AND Orden=:Orden and Fase=9 AND Estado=1',[':TipoGastoID'=>$requerimiento->TipoRequerimiento,':Orden'=>$res['OrdenID']])->one();
                                            $CompMensual = Siaft::find()->where('TipoGastoID=:TipoGastoID AND Orden=:Orden and Fase=10 AND Estado=1',[':TipoGastoID'=>$requerimiento->TipoRequerimiento,':Orden'=>$res['OrdenID']])->one();
                                            if($CompAnual){
                                                $canual = $CompAnual->Siaf;
                                            }else{
                                                $canual = "--";
                                            }
                                            
                                            if($CompMensual){
                                                // $cmensual = $CompMensual->Siaf;
                                                $GetExpedienteSiaf=\Yii::$app->db->createCommand('BEGIN DECLARE @a VARCHAR(255); EXEC SP_OBTENER_EXPEDIENTE_SIAF :NUM_REGI_CAB, :ANNO_EJEC_EJE select @a as NUME_SIAF_CAB end;')
                                                        ->bindValue(':NUM_REGI_CAB',$CompMensual->Siaf)
                                                        ->bindValue(':ANNO_EJEC_EJE',date('Y'))
                                                        ->queryOne();
                                                $expedientSiaf=$GetExpedienteSiaf["NUME_SIAF_CAB"];
                                            }else{
                                                $expedientSiaf = "--";
                                            }
                                            ?>
                                            
                                            
                                            <td><?= $expedientSiaf ?></td>
                                            <?php if($cabecera->Situacion == 2 || $cabecera->Situacion == 3): ?>
                                                <?php if($res['GastoElegible'] == 1): ?>
                                                    <td><strong>Gasto elegible</strong></td>
                                                <?php else: ?>
                                                    <td class="red"><strong>Gasto no elegible</strong></td>
                                                <?php endif ?>
                                            <?php else: ?>
                                                <td></td>
                                            <?php endif ?>
                                            <td><?php echo($res['Nombre']) ?></td>
                                            <td class="text-center"><?php echo(str_pad($res['Correlativo'], 3, "0", STR_PAD_LEFT)  ."-".$res["Annio"]) ?></td>
                                            <td class="text-center"><?php echo(date('d-m-Y',strtotime($res['FechaOrden']))) ?></td>
                                            <td class="text-center"><?php echo($res["TipoProceso"] == 1 ? 'Comparacion de Precios' : 'Evaluacion de CVs') ?></td>
                                            <td class="input-number"><?php echo number_format(($res["Monto"]), 2, '.', ' ') ?></td>
                                            <?php if($res["TipoGasto"]==1){ ?>
                                                <td class="text-center">
                                                    <?php if(!empty($res["Documento"])): ?>
                                                        <a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/ordenServicioFactura/<?= $res["Documento"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Descargar adjunto</a>
                                                    <?php endif ?>
                                                </td>
                                            <?php }elseif($res["TipoGasto"]==2){ ?>
                                                <td class="text-center">
                                                    <?php if(!empty($res["Documento"])): ?>
                                                        <a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/ordenServicioRH/<?= $res["Documento"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Descargar adjunto</a>
                                                    <?php endif ?>
                                                </td>
                                            <?php }elseif($res["TipoGasto"]==3){ ?>
                                                <td class="text-center">
                                                    <?php if(!empty($res["Documento"])): ?>
                                                        <a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/ordenCompra/<?= $res["Documento"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Descargar adjunto</a>
                                                    <?php endif ?>
                                                </td>
                                            <?php } ?>

                                            <?php if($cabecera->Situacion == 2 || $cabecera->Situacion == 3): ?>
                                                <?php if($res['Observacion'] != ''): ?>
                                                    <td><?php echo $res['Observacion'] ?></td>
                                                <?php else: ?>
                                                    <td>--</td>
                                                <?php endif ?>
                                            <?php else: ?>
                                                <td>
                                                    <a data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' data-detalle-compromiso-id="<?= $res['ID'] ?>" href="#" class="btn btn-primary btn-ver-observar">Observación</a>    
                                                </td>
                                            <?php endif ?>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                            <?php $compromisoID = $cabecera->ID; ?>
                            <?php }elseif($requerimiento->TipoRequerimiento==4){?>
                            <table class="table table-hover tbl-act">
                                <tbody>
                                    <tr class="tr-header">
                                        <td>Expediente SIAF</td>
                                        <td></td>
                                        <td>Tipo</td>
                                        <td>Correlativo</td>
                                        <td>Datos</td>
                                        <td>DNI</td>
                                        <td>Monto Total</td>
                                        <td>Formato</td>
                                        <td>Observación</td>
                                    </tr>
                                    <?php foreach ($resultados as $res): ?>
                                        <tr>
                                            <?php $expedientSiaf=""; ?>
                                            
                                            
                                            <?php 
                                            $CompAnual = Siaft::find()->where('TipoGastoID=:TipoGastoID AND Orden=:Orden and Fase=9 AND Estado=1',[':TipoGastoID'=>$requerimiento->TipoRequerimiento,':Orden'=>$res['ViaticoID']])->one();
                                            $CompMensual = Siaft::find()->where('TipoGastoID=:TipoGastoID AND Orden=:Orden and Fase=10 AND Estado=1',[':TipoGastoID'=>$requerimiento->TipoRequerimiento,':Orden'=>$res['ViaticoID']])->one();
                                            if($CompAnual){
                                                $canual = $CompAnual->Siaf;
                                            }else{
                                                $canual = "--";
                                            }
                                            
                                            if($CompMensual){
                                                // $cmensual = $CompMensual->Siaf;
                                                $GetExpedienteSiaf=\Yii::$app->db->createCommand('BEGIN DECLARE @a VARCHAR(255); EXEC SP_OBTENER_EXPEDIENTE_SIAF :NUM_REGI_CAB, :ANNO_EJEC_EJE select @a as NUME_SIAF_CAB end;')
                                                        ->bindValue(':NUM_REGI_CAB',$CompMensual->Siaf)
                                                        ->bindValue(':ANNO_EJEC_EJE',date('Y'))
                                                        ->queryOne();
                                                $expedientSiaf=$GetExpedienteSiaf["NUME_SIAF_CAB"];
                                            }else{
                                                $expedientSiaf = "--";
                                            }
                                            ?>
                                            
                                            <td><?= $expedientSiaf ?></td>
                                            
                                            <?php if($cabecera->Situacion == 2 || $cabecera->Situacion == 3): ?>
                                                <?php if($res['GastoElegible'] == 1): ?>
                                                    <td><strong>GASTO ELEGIBLE</strong></td>
                                                <?php else: ?>
                                                    <td class="red"><strong>GASTO NO ELEGIBLE</strong></td>
                                                <?php endif ?>
                                            <?php else: ?>
                                                <td></td>
                                            <?php endif ?>
                                            <td class="text-center">Viático</td>
                                            <td class="text-center"><?php echo (str_pad($res['Correlativo'], 3, "0", STR_PAD_LEFT)  ."-".$res["Annio"]) ?></td>
                                            <td><?php echo $res['Apellido'].' '.$res['Nombres'] ?></td>
                                            <td class="text-center"><?php echo $res['Dni'] ?></td>
                                            <td class="input-number">S/ <?php echo $res['Monto'] ?></td>
                                            <td class="text-center">
                                                <?php if(!empty($res["Documento"])): ?>
                                                    <a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/viatico/<?= $res["Documento"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Descarga adjunto</a>
                                                <?php else: ?>
                                                    --
                                                <?php endif ?>
                                            </td>

                                            <?php if($cabecera->Situacion == 2 || $cabecera->Situacion == 3): ?>
                                                <?php if($res['Observacion'] != ''): ?>
                                                    <td><?php echo $res['Observacion'] ?></td>
                                                <?php else: ?>
                                                    <td class="text-center">--</td>
                                                <?php endif ?>
                                            <?php else: ?>
                                                <td>
                                                    <a data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' data-detalle-compromiso-id="<?= $res['ID'] ?>" href="#" class="btn btn-primary btn-ver-observar">Observación</a>    
                                                </td>
                                            <?php endif ?>
                                        </tr>
                                        <?php $compromisoID = $res['CompromisoPresupuestalID']; ?>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                            <?php $compromisoID = $cabecera->ID; ?>
                            <?php }elseif($requerimiento->TipoRequerimiento==5){?>
                            <table class="table table-hover tbl-act">
                                <tbody>
                                    <tr class="tr-header">
                                        <td>#</td>
                                        <td></td>
                                        <td>Tipo de Gasto</td>
                                        <td>Tipo</td>
                                        <td>Bienes</td>
                                        <td>Servicios</td>
                                        <td>Monto Total</td>
                                        <td>Formato</td>
                                        <td>Observación</td>
                                    </tr>
                                    <?php foreach ($resultados as $res): ?>
                                    <tr>
                                        <td class="text-center">#<?php echo $res['ID'] ?></td>                                        
                                        <?php if($cabecera->Situacion == 2 || $cabecera->Situacion == 3): ?>
                                            <?php if($res['GastoElegible'] == 1): ?>
                                                <td><strong>GASTO ELEGIBLE</strong></td>
                                            <?php else: ?>
                                                <td class="red"><strong>GASTO NO ELEGIBLE</strong></td>
                                            <?php endif ?>
                                        <?php else: ?>
                                            <td></td>
                                        <?php endif ?>

                                        <td class="text-center">Caja Chica</td>
                                        <td class="text-center"><?php echo($res['Tipo']==1)?'Apertura':'Desembolso'; ?></td>
                                        <td class="input-number"><?php echo number_format(($res['Bienes']), 2, '.', ' ') ?></td>
                                        <td class="input-number"><?php echo number_format(($res['Servicios']), 2, '.', ' ') ?></td>
                                        <td class="input-number"><?php echo number_format(($res['Servicios']+$res['Bienes']), 2, '.', ' ') ?></td>
                                        <td class="text-center">

                                        <?php if (!empty($res["ResolucionDirectorial"])): ?>
                                            <a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/cajachica/<?= $res["ResolucionDirectorial"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Descargar</a>
                                        <?php else: ?>
                                            <p style="color: red">No se ha subido documento</p>
                                        <?php endif ?>

                                        <!-- <a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/cajachica/<?= $res["ResolucionDirectorial"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Descargar adjunto</a> -->
                                        </td>

                                        <?php if($cabecera->Situacion == 2 || $cabecera->Situacion == 3): ?>
                                            <?php if($res['Observacion'] != ''): ?>
                                                <td><?php echo $res['Observacion'] ?></td>
                                            <?php else: ?>
                                                <td class="text-center">--</td>
                                            <?php endif ?>
                                        <?php else: ?>
                                            <td></td>
                                        <?php endif ?>
                                    </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                            <?php $compromisoID = $cabecera->ID; ?>
                            <?php }elseif($requerimiento->TipoRequerimiento==6){?>
                            <table class="table table-hover tbl-act">
                                <tbody>
                                    <tr class="tr-header">
                                        <td>Expediente SIAF</td>
                                        <td></td>
                                        <td>Tipo de Gasto</td>
                                        <td>Bienes</td>
                                        <td>Servicios</td>
                                        <td>Monto Total</td>
                                        <td>Formato</td>
                                        <td>Observación</td>
                                    </tr>
                                    <?php foreach ($resultados as $res): ?>
                                    <tr>
                                            <?php $expedientSiaf=""; ?>
                                            
                                            
                                            <?php 
                                            $CompAnual = Siaft::find()->where('TipoGastoID=:TipoGastoID AND Orden=:Orden and Fase=9 AND Estado=1',[':TipoGastoID'=>$requerimiento->TipoRequerimiento,':Orden'=>$res['EncargoID']])->one();
                                            $CompMensual = Siaft::find()->where('TipoGastoID=:TipoGastoID AND Orden=:Orden and Fase=10 AND Estado=1',[':TipoGastoID'=>$requerimiento->TipoRequerimiento,':Orden'=>$res['EncargoID']])->one();
                                            if($CompAnual){
                                                $canual = $CompAnual->Siaf;
                                            }else{
                                                $canual = "--";
                                            }
                                            
                                            if($CompMensual){
                                                // $cmensual = $CompMensual->Siaf;
                                                $GetExpedienteSiaf=\Yii::$app->db->createCommand('BEGIN DECLARE @a VARCHAR(255); EXEC SP_OBTENER_EXPEDIENTE_SIAF :NUM_REGI_CAB, :ANNO_EJEC_EJE select @a as NUME_SIAF_CAB end;')
                                                        ->bindValue(':NUM_REGI_CAB',$CompMensual->Siaf)
                                                        ->bindValue(':ANNO_EJEC_EJE',date('Y'))
                                                        ->queryOne();
                                                $expedientSiaf=$GetExpedienteSiaf["NUME_SIAF_CAB"];
                                            }else{
                                                $expedientSiaf = "--";
                                            }
                                            ?>
                                            
                                            <td><?= $expedientSiaf ?></td>
                                            
                                        <?php if($cabecera->Situacion == 2 || $cabecera->Situacion == 3): ?>
                                            <?php if($res['GastoElegible'] == 1): ?>
                                                <td><strong>GASTO ELEGIBLE</strong></td>
                                            <?php else: ?>
                                                <td class="red"><strong>GASTO NO ELEGIBLE</strong></td>
                                            <?php endif ?>
                                        <?php else: ?>
                                            <td></td>
                                        <?php endif ?>
                                        <td class="text-center">Encargos</td>
                                        <td class="input-number"><?php echo ($res['Bienes']) ?></td>
                                        <td class="input-number"><?php echo ($res['Servicios']) ?></td>
                                        <td class="input-number"><?php echo ($res['Servicios']+$res['Bienes']) ?></td>
                                        <td class="text-center">
                                            <?php if(!empty($res['Documento'])): ?>
                                            <a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/encargos/<?= $res["Documento"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Descargar adjunto</a>
                                            <?php else: ?>
                                                --
                                            <?php endif ?>
                                        </td>
                                        
                                        <?php if($cabecera->Situacion == 2 || $cabecera->Situacion == 3): ?>
                                            <?php if($res['Observacion'] != ''): ?>
                                                <td><?php echo $res['Observacion'] ?></td>
                                            <?php else: ?>
                                                <td class="text-center">--</td>
                                            <?php endif ?>
                                        <?php else: ?>
                                            <td>
                                                <a data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' data-detalle-compromiso-id="<?= $res['ID'] ?>" href="#" class="btn btn-primary btn-ver-observar">Observación</a>    
                                            </td>
                                        <?php endif ?>
                                    </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                            <?php $compromisoID = $cabecera->ID; ?>
                            <?php } ?>
                            
                        </div>
                        <?php if(\Yii::$app->user->identity->rol == 14): ?>
                            <?php if($cabecera->Situacion == 3): ?>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-primary generar-compromiso'>Generar Compromiso Anual</a>
                            <?php endif ?>
                        <?php endif ?>
                        <a href="<?= \Yii::$app->request->BaseUrl ?>/compromiso-presupuestal" class="btn btn-info">VOLVER</a>
                        <a href="#" class="btn btn-primary btn-adjuntar" data-id="<?php echo $cabecera->ID ?>" >Reemplazar documentos de Compromiso Presupuestal</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal-loading" class="modal fade modal-loading in" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-hidden="false" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content-no-shadow modal-content">
            <div class="text-center"><i class="fa fa-spinner fa-pulse fa-fw" style="color:#16a085;font-size: 100px;"></i></div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-ctrl-observacion" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Recursos</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-ctrl-compromiso-adjuntar" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Adjuntar Documento</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-ctrl-observacion-administrativo" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Compromiso</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<?php 

    if(\Yii::$app->user->identity->rol == 13 || \Yii::$app->user->identity->rol == 14){ 
        $requerimiento = Yii::$app->getUrlManager()->createUrl('requerimiento/actualizar-requerimiento-contabilidad');
    } 

    $compro = Yii::$app->getUrlManager()->createUrl('certificacion-presupuestal/generar-compromiso-anual');
    $ordenes = Yii::$app->getUrlManager()->createUrl('compromiso-presupuestal/situacion-ordenes');
?>



<script>
    $(document).ready(function(){
       $('.fa-search').popover(); 
    });
    $('[data-rel=tooltip]').tooltip();
      

    $('body').on('click', '.btn-adjuntar', function (e) {
        e.preventDefault();
        var ID=$(this).attr('data-id');
        $('#modal-ctrl-compromiso-adjuntar .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/compromiso-presupuestal/adjuntar-documento-ver?ID='+ID);
        $('#modal-ctrl-compromiso-adjuntar').modal('show');
    });
    
    $('#verifica').click(function(){
        var x = $('#gastos').val();
        var a = $('#gastos').attr('data-id');
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        if(x == ''){
            bootbox.alert('Seleccione un item.');
        }else{
            bootBoxConfirm('Esta seguro que desea registrar la información',function(){
                $.ajax({
                    url: '<?php //= $requerimiento ?>',
                    type: 'POST',
                    async: false,
                    data: {gastos:x,_csrf: toke,ID:a},
                    success: function (data) {
                        location.reload();
                    },
                    error: function () {
                        _pageLoadingEnd();
                    }
                });
            });
        }
    });



    $(document).on('click','.generar-compromiso',function(){
        var arrVacio = [];
        var ID=$(this).attr('data-id');
        var CodigoProyecto=$(this).attr('data-codigo-proyecto');
        
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';

        $('.check:checked').each(function () {
            var $this = $(this);
            arrVacio.push({
                vacio: 1
            });
        });

        //if(arrVacio.length == 0){
           // bootbox.alert('Debe de selcconaar un item.');
        //}else{
            bootBoxConfirm('Esta seguro de Generar el Compromiso Anual',function(){
                $.ajax({
                    url: '<?= $compro ?>',
                    type: 'POST',
                    async: false,
                    data: {ID:ID,CodigoProyecto:CodigoProyecto,_csrf: toke},
                    success: function (data) {
                        //location.reload();
                    },
                    error: function () {
                        _pageLoadingEnd();
                    }
                });
            });
        //}
    });


    $('body').on('click', '.check', function (e) {
        // e.preventDefault();
        $(this).attr('checked','checked');
        var ID = $(this).attr('data-id');
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $ordenes ?>',
            type: 'POST',
            async: false,
            data: {ID:ID,_csrf: toke},
            success: function (data) {
                //location.reload();
            },
            error: function () {
                _pageLoadingEnd();
            }
        });
    });
    
    $('body').on('click', '.btn-ver-observar', function (e) {
        e.preventDefault();
        var CodigoProyecto=$(this).attr('data-codigo-proyecto');
        var DetalleCompromisoID=$(this).attr('data-detalle-compromiso-id');
        var ID=<?php echo $cabecera->ID ?>;
        $('#modal-ctrl-observacion-administrativo .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/compromiso-presupuestal/observar-compromiso?ID='+ID+'&DetalleCompromisoID='+DetalleCompromisoID+'&CodigoProyecto='+CodigoProyecto+'&Tipo=2');
        $('#modal-ctrl-observacion-administrativo').modal('show');
    });
    
    $('body').on('click', '.btn-ver-observar-jefe-contabilidad', function (e) {
        e.preventDefault();
        var CodigoProyecto=$(this).attr('data-codigo-proyecto');
        var ID=<?php echo $cabecera->ID ?>;
        $('#modal-ctrl-observacion-administrativo .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/compromiso-presupuestal/observar-compromiso-jefe-contabilidad?ID='+ID+'&CodigoProyecto='+CodigoProyecto+'&Tipo=2');
        $('#modal-ctrl-observacion-administrativo').modal('show');
    });
    
</script>