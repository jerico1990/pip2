<?php

/** Error reporting */
error_reporting(E_ALL);

/** Include path **/
ini_set('include_path', ini_get('include_path').';../web/PHPExcel/Classes/');
//require(__DIR__ . '/../../web/PHPExcel/Classes/');
/** PHPExcel */
include 'PHPExcel.php';

/** PHPExcel_Writer_Excel2007 */
include 'PHPExcel/Writer/Excel2007.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
$sheet = $objPHPExcel->getActiveSheet();

$sheet->setTitle('Requerimientos');


$objPHPExcel->setActiveSheetIndex(0);



$sheet->SetCellValue('A1', '#');
$sheet->SetCellValue('B1', 'Código Proyecto');
$sheet->SetCellValue('C1', 'Compromiso Presupuestal N°');
$sheet->SetCellValue('D1', 'Memorando');
$sheet->SetCellValue('E1', 'Gasto Elegible');
$sheet->SetCellValue('F1', 'Situación');

$i=2;
foreach($resultados as $resultado)
{
    $sheet->SetCellValue('A'.$i, ($i-1));
    $sheet->SetCellValue('B'.$i, $resultado["Codigo"]);
    $sheet->SetCellValue('C'.$i, str_pad($resultado["Correlativo"], 3, "0", STR_PAD_LEFT));
    $sheet->SetCellValue('D'.$i, $resultado["Memorando"]);
    $sheet->SetCellValue('E'.$i, $compromisoPresupuestal->getGastoElegible($resultado["GastoElegible"]));
    $sheet->SetCellValue('F'.$i, $compromisoPresupuestal->getSituacion($resultado["Situacion"]));
    $i++;
}
    




// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="CompromisosPresupuestales.xls"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
