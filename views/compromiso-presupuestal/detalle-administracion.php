<?php
use app\models\Orden;
?>
<div id="page-content" style="min-height: 885px;">
    <div id="wrap">
        <div id="page-heading">
            <h1></h1>
        </div>
        <div class="container">
                    
            <style>
                .panel .panel-body {
                    padding: 10px !important;
                }

                .panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 1px solid #a8a8ab !important;
                }

                .panel-gray .panel-body {
                    border-top: none !important;
                }

                .btn-sm {
                    padding: 5px 10px !important;
                    line-height: 1.5 !important;
                }

                .input-comp-nombre {
                    text-transform: uppercase;
                }

                .tbl-act > tbody > .tr-header > td {
                    text-align: center;
                    font-size: 12px;
                    font-weight: bold;
                    padding: 5px !important;
                    vertical-align: middle !important;
                    /*border: 1px solid #cfcfd0;
                    background-color: #f0f0f1;*/
                    border: 1px solid #c0c0c0;
                    background-color: #e3e3e3;
                }

                /*.tbl-act .tr-item td {
                    padding: 2px !important;
                }*/
                input.form-control, select.form-control {
                    /*height: 28px !important;*/
                }

                .tbl-act .tr-act .input-nombre {
                    min-width: 320px !important;
                }

                .tbl-act .tr-act .input-unidadmedida {
                    min-width: 100px !important;
                }

                .input-number {
                    min-width: 92px !important;
                }

                .btn-add-re {
                    float: right;
                }

                /*.tbl-act > tbody > tr > td:nth-child(2) {
                    min-width: 40px !important;
                    text-align: center !important;
                }*/
                .text-left {
                    text-align: left !important;
                    font-weight: bold;
                }

                .tr-comp input, .tr-proy input {
                    font-weight: bold;
                }

                .th-proy {
                    min-width: 630px !important;
                }
            </style>

            <div class="tab-container">
                <ul class="nav nav-tabs">
                    <li id="li-poaf" class="active">
                        <a href="#tab-informacion-general" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Detalle Solicitud</a>
                    </li>
                </ul>
                <div class="tab-content" style="background: #fff;">
                    <div class="tab-pane active" id="tab-informacion-general">
                        
                        <div id="container-poaf">
                            <?php
                               
                            if($cabecera->Situacion==18)
                            {
                                $situacion='Pendiente';
                            }
                            else
                            {
                                $situacion = 'Aprobado';
                            }
                                
                            switch ($cabecera->GastoElegible) {
                                case '1':
                                    $gastos = 'Gasto Elegible';
                                break;
                                case '2':
                                    $gastos = 'Gasto No Elegible';
                                break;
                                case '0':
                                    $gastos = '';
                                break;
                            }
                            
                            
                            ?>
                            <h3>Proyecto <?php echo $cabecera->Codigo ?> / Documento Nº <?php echo $cabecera->Correlativo ?> - <strong><?php echo $situacion ?></strong></h3>

                            <?php 
                            if($cabecera->Observacion)
                            {
                                echo "Observación:<p>".$cabecera->Observacion."</p>";
                            }
                            ?>
                            <?php if(\Yii::$app->user->identity->rol == 12): ?>
                                <?php if($cabecera->Situacion == 18): ?>
                                    <form class="form-horizontal">
                                        <p>Aprobar el compromiso presupuestal</p>
                                        <a href="javascript:;" class="btn btn-primary verifica-aprobar" data-id="<?php echo $cabecera->ID ?>" id="">APROBAR</a>
                                        <a href="javascript:;" class="btn btn-danger anular" data-id="<?php echo $cabecera->ID ?>" ><i class='fa fa-remove fa-lg'></i></a>
                                        <br>
                                    </form>
                                <?php endif ?>
                            <?php endif; ?>
                            
                            <hr>
                            <?php if($requerimiento->TipoRequerimiento==1 || $requerimiento->TipoRequerimiento==2 || $requerimiento->TipoRequerimiento==3){?>
                                <table class="table table-hover tbl-act">
                                    <tbody>
                                        <tr class="tr-header">
                                            <!-- <td>Tipo</td> -->
                                            <td>Correlativo</td>
                                            <td>Código Matriz</td>
                                            <td>RUC</td>
                                            <td>Razon Social</td>
                                            <td>Monto</td>
                                            <td>Formato</td>
                                            <td>Gasto Elegible</td>
                                            <td>Observación</td>
                                        </tr>
                                        <?php //echo '<pre>';print_r($resultados); die; ?>
                                        <?php $i=0;foreach ($resultados as $res): ?>
                                            <tr>
                                                <!-- <td><?php echo($res['Nombre']) ?></td> -->
                                                <td class="text-center"><?php echo(str_pad($res['Correlativo'], 3, "0", STR_PAD_LEFT)  ."-".$res["Annio"]) ?></td>
                                                <td class="text-center"><?php echo $res['CodigoMatriz'] ?></td>
                                                <td class="text-center"><?php echo $res['RUC'] ?></td>
                                                <td><?php echo $res['RazonSocial'] ?></td>
                                                <td class="text-center input-number">S/ <?php echo number_format(($res["Monto"]), 2, '.', ' ') ?></td>

                                                <?php if($res["TipoGasto"]==1){ ?>
                                                    <td class="text-center">
                                                        <?php if(!empty($res['Documento'])): ?>
                                                            <a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/ordenServicioFactura/<?= $res["Documento"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Descargar adjunto</a>
                                                        <?php else: ?>

                                                        <?php endif; ?>
                                                    </td>
                                                <?php }elseif($res["TipoGasto"]==2){ ?>
                                                    <td class="text-center">
                                                        <?php if(!empty($res['Documento'])): ?>
                                                            <a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/ordenServicioRH/<?= $res["Documento"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Descargar adjunto</a>
                                                        <?php else: ?>
                                                        <?php endif; ?>
                                                    </td>
                                                <?php }elseif($res["TipoGasto"]==3){ ?>
                                                    <td class="text-center">
                                                        <?php if(!empty($res['Documento'])): ?>
                                                            <a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/ordenCompra/<?= $res["Documento"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Descargar adjunto</a>
                                                        <?php else: ?>
                                                            
                                                        <?php endif; ?>
                                                    </td>
                                                <?php } ?>
                                                <?php if($res['GastoElegible'] == 1): ?>
                                                    <td class="text-center"><strong>SI</strong></td>
                                                <?php else: ?>
                                                    <td class="red text-center"><strong>NO</strong></td>
                                                <?php endif ?>
                                                <?php if($res['Observacion'] != ''): ?>
                                                    <td><?php echo $res['Observacion'] ?></td>
                                                <?php else: ?>
                                                    <td class="text-center">--</td>
                                                <?php endif ?>
                                            </tr>
                                            <?php $compromisoID = $res['CompromisoPresupuestalID']; ?>
                                        <?php $i++; endforeach ?>
                                    </tbody>
                                </table>
                            <?php }elseif($requerimiento->TipoRequerimiento==4){?>
                                <table class="table table-hover tbl-act">
                                    <tbody>
                                        <tr class="tr-header">
                                            <td>Correlativo</td>
                                            <td>Código Matriz</td>
                                            <td>DNI</td>
                                            <td>Datos</td>
                                            <td>Monto Total</td>
                                            <td>Formato</td>
                                            <td>Gasto Elegible</td>
                                            <td>Observación</td>

                                        </tr>
                                        <?php $i=0; foreach ($resultados as $res): ?>
                                            <tr>
                                                <!--  -->
                                                <td class="text-center"><?php echo (str_pad($res['Correlativo'], 3, "0", STR_PAD_LEFT)  ."-".$res["Annio"]) ?></td>
                                                
                                                <td class="text-center"><?php echo $res['CodigoMatriz'] ?></td>
                                                <td><?php echo $res['Apellido'].' '.$res['Nombre'] ?></td>

                                                <td class="text-center"><?php echo $res['Dni'] ?></td>
                                                <td class="input-number">S/ <?php echo $res['Monto'] ?></td>
                                                <td class="text-center">
                                                    <?php if(!empty($res["Documento"])): ?>
                                                        <a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/viatico/<?= $res["Documento"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Descarga adjunto</a>
                                                    <?php else: ?>
                                                        --
                                                    <?php endif; ?>
                                                </td>
                                                <?php if($res['GastoElegible'] == 1): ?>
                                                    <td class="text-center"><strong>SI</strong></td>
                                                <?php else: ?>
                                                    <td class="text-center red"><strong>NO</strong></td>
                                                <?php endif ?>
                                                <?php if($res['Observacion'] != ''): ?>
                                                    <td><?php echo $res['Observacion'] ?></td>
                                                <?php else: ?>
                                                    <td class="text-center">--</td>
                                                <?php endif ?>
                                                
                                            </tr>
                                            <?php $compromisoID = $res['CompromisoPresupuestalID']; ?>
                                        <?php $i++; endforeach ?>
                                    </tbody>
                                </table>
                            <?php }elseif($requerimiento->TipoRequerimiento==5){?>
                                <table class="table table-hover tbl-act">
                                    <tbody>
                                        <tr class="tr-header">
                                            <td>Correlativo</td>
                                            <td>Responsable</td>
                                            <td>Tipo</td>
                                            <td>Bienes</td>
                                            <td>Servicios</td>
                                            <td>Monto Total</td>

                                            <td>Gasto Elegible</td>
                                            <td>Formato</td>
                                            <td>Observación</td>

                                        </tr>
                                        <?php $i=0;foreach ($resultados as $res): ?>
                                        <tr>

                                            <td class="text-center"><?php echo(str_pad($res['Correlativo'], 3, "0", STR_PAD_LEFT)  ."-".$res["Annio"]) ?></td>


                                            <td><?php echo $res['Responsable'] ?></td>
                                            <td class="text-center"><?php echo($res['Tipo']==1)?'Apertura':'Desembolso'; ?></td>
                                            <td class="input-number"><?php echo number_format(($res['Bienes']), 2, '.', ' ') ?></td>
                                            <td class="input-number"><?php echo number_format(($res['Servicios']), 2, '.', ' ') ?></td>
                                            <td class="input-number"><?php echo number_format(($res['Servicios']+$res['Bienes']), 2, '.', ' ') ?></td>

                                            <?php if($res['GastoElegible'] == 1): ?>
                                                <td class="text-center"><strong>SI</strong></td>
                                            <?php else: ?>
                                                <td class="text-center red"><strong>NO</strong></td>
                                            <?php endif ?>
                                            

                                            <td  class="text-center">
                                                <?php if(!empty($res["ResolucionDirectorial"])): ?>
                                                    <a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/cajachica/<?= $res["ResolucionDirectorial"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Descargar adjunto</a>
                                                <?php endif ?>
                                            </td>

                                            <?php if($res['Observacion'] != ''): ?>
                                                <td><?php echo $res['Observacion'] ?></td>
                                            <?php else: ?>
                                                <td class="text-center">--</td>
                                            <?php endif ?>


                                        </tr>
                                        <?php $i++;endforeach ?>
                                    </tbody>
                                </table>
                            <?php $compromisoID = $cabecera->ID; ?>
                            <?php }elseif($requerimiento->TipoRequerimiento==6){?>
                                <table class="table table-hover tbl-act">
                                    <tbody>
                                        <tr class="tr-header">
                                            <td>Correlativo</td>
                                            <td>Código Matriz</td>
                                            <td>Datos</td>
                                            <td>DNI</td>
                                            <td>Bienes</td>
                                            <td>Servicios</td>
                                            <td>Monto</td>
                                            <td>Formato</td>
                                            <td>Gasto Elegible</td>
                                            <td>Observación</td>
                                        </tr>
                                        <?php $i=0; foreach ($resultados as $res): ?>
                                        <tr>
                                            <td class="text-center"><?php echo(str_pad($res['Correlativo'], 3, "0", STR_PAD_LEFT)  ."-".$res["Annio"]) ?></td>
                                            <td class="text-center"><?php echo $res['CodigoMatriz'] ?></td>
                                            <td><?php echo $res['ApellidosEncargo'].' '.$res['NombresEncargo'] ?></td>
                                            <td class="text-center"><?php echo $res['DNIEncargo'] ?></td>

                                            <td class="input-number"><?php echo number_format(($res['Bienes']), 2, '.', ' ') ?></td>
                                            <td class="input-number"><?php echo number_format(($res['Servicios']), 2, '.', ' ') ?></td>
                                            <td class="input-number"><?php echo number_format(($res['Servicios']+$res['Bienes']), 2, '.', ' ') ?></td>
                                            <td>
                                                <?php if(!empty($res["Documento"])): ?>
                                                    <a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/encargos/<?= $res["Documento"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Descargar adjunto</a>
                                                <?php else: ?>
                                                    --
                                                <?php endif; ?>
                                            </td>
                                            
                                            <?php if($res['GastoElegible'] == 1): ?>
                                                <td class="text-center"><strong>SI</strong></td>
                                            <?php else: ?>
                                                <td class="text-center red"><strong>NO</strong></td>
                                            <?php endif ?>
                                            <?php if($res['Observacion'] != ''): ?>
                                                <td><?php echo $res['Observacion'] ?></td>
                                            <?php else: ?>
                                                <td class="text-center">--</td>
                                            <?php endif ?>
                                        </tr>
                                        <?php $i++; endforeach ?>
                                    </tbody>
                                </table>
                            <?php $compromisoID = $cabecera->ID; ?>
                            <?php } ?>
                        </div>

                        <hr>
                        <hr>
                        <table class="table table-hover tbl-act" id="detallesx">
                            <thead>
                                <tr class="tr-header">
                                    <th width="150">Codigo</th>
                                    <th width="150">Codigo Matriz</th>
                                    <th width="150">Recurso</th>
                                    <th width="150">Especifica</th>
                                    <th width="100">Unidad Medida</th>
                                    <th width="100">Costo Unitario</th>
                                    <th width="100">Meta Fisica</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($detResult as $det) { ?>
                                <tr>
                                    <td><?php echo $det['CorrelaCompr'].'.'.$det['CorrelaActv'].'.'.$det['CorrelaRecurso']; ?></td>
                                    <td><?php echo $det['RubroElegibleID'].'.'.$det['Codificacion']; ?></td>
                                    <td><?php echo $det['Nombre']; ?></td>
                                    <td><?php echo $det['Especifica']; ?></td>
                                    <td><?php echo $det['UnidadMedida']; ?></td>
                                    <td><?php echo $det['CostoUnitario']; ?></td>
                                    <td><?php echo $det['MetaFisica']; ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                        

                        <?php if(!$cabecera->FormatoUafsi && !$cabecera->GastoElegible){?>
                            <a target="_blank" class="btn btn-primary" href="<?= \Yii::$app->request->BaseUrl ?>/compromisopresupuestal/<?php echo $cabecera->Formato ?>">
                                <span class="fa fa-cloud-download"></span> Descargar compromiso presupuestal
                            </a>
                            <a target="_blank" data-id="<?= $cabecera->ID ?>" class="btn btn-primary btn-adjuntar" href="<?= \Yii::$app->request->BaseUrl ?>/compromisopresupuestal/<?php echo $cabecera->Formato ?>">
                                <span class="fa fa-cloud-upload"></span> Adjuntar compromiso presupuestal firmado
                            </a>
                        <?php }elseif($cabecera->FormatoUafsi && !$cabecera->GastoElegible){ ?>
                            <a target="_blank" class="btn btn-primary" href="<?= \Yii::$app->request->BaseUrl ?>/compromisopresupuestal/<?php echo $cabecera->FormatoUafsi ?>">
                                <span class="fa fa-cloud-download"></span> Descargar compromiso presupuestal
                            </a>
                            <?php if($cabecera->Situacion == 15): ?>
                                <a href='#' class='btn btn-danger btn-remove-documento'  data-id="<?= $cabecera->ID ?>"><i class='fa fa-remove'></i></a>
                            <?php endif; ?>
                            <?php }else{ ?>
                            <a target="_blank" class="btn btn-primary" href="<?= \Yii::$app->request->BaseUrl ?>/compromisopresupuestal/<?php echo $cabecera->FormatoUafsi ?>">
                                <span class="fa fa-cloud-download"></span> Descargar compromiso presupuestal
                            </a>
                        <?php } ?>
                        <?php if($cabecera->Situacion == 15): ?>
                            <?php if($cabecera->FormatoUafsi != ''): ?>
                                <a href="javascript:;" class="btn btn-success" data-id-comp="<?php echo $cabecera->ID ?>" id="verifica">ENVIAR</a>
                            <?php else: ?>
                                <a href="javascript:;" class="btn btn-success" data-id-comp="<?php echo $cabecera->ID ?>" id="verifica" disabled>ENVIAR</a>
                            <?php endif; ?>
                        <?php endif; ?>
                        <a target="_blank" data-id="<?= $cabecera->ID ?>" class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/requerimientos/<?php echo $requerimiento->Documento ?>">
                            <span class="fa fa-cloud-download"></span> Descargar adjunto de requerimiento
                        </a>
                        <a href="<?= \Yii::$app->request->BaseUrl ?>/compromiso-presupuestal/administracion" class="btn btn-default">VOLVER</a>
                    </div>
                </div>
            </div>
            <?php if(\Yii::$app->user->identity->rol == 2): ?>
                <div class="alert alert-warning" style="text-align:justify">
                    <strong>NOTA:</strong> Para poder seleccionar el tipo de gastos y enviar el formulario, se debe de adjuntar la certificación firmada.
                </div>
            <?php endif ?>
        </div>
    </div>
</div>



<div id="modal-loading" class="modal fade modal-loading in" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-hidden="false" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content-no-shadow modal-content">
            <div class="text-center"><i class="fa fa-spinner fa-pulse fa-fw" style="color:#16a085;font-size: 100px;"></i></div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-ctrl-observacion" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Gasto No Elegible</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-ctrl-adjuntar" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Adjuntar Documento</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<?php 


if(\Yii::$app->user->identity->rol == 12){ 
    $enviarCompromisoPresupuestal = Yii::$app->getUrlManager()->createUrl('compromiso-presupuestal/procesar-compromiso-jefe-administracion');
}

?>


<script>
    $(document).ready(function(){
       $('.fa-search').popover(); 
       validarNumeros();
    });
    $('[data-rel=tooltip]').tooltip();
      


   
    $(document).on('click','.verifica-aprobar',function(){
        var ID = $(this).attr('data-id');
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        bootBoxConfirm('¿Esta seguro que el compromiso presupuestal es conforme?',function(){
            $.ajax({
                url: '<?= $enviarCompromisoPresupuestal ?>',
                type: 'POST',
                async: false,
                data: {_csrf: toke,ID:ID},
                success: function (data) {
                    location.reload();
                },
                error: function () {
                    _pageLoadingEnd();
                }
            });
        });
    });
    
    $('body').on('click', '.btn-adjuntar', function (e) {
        e.preventDefault();
        var ID=$(this).attr('data-id');
        $('#modal-ctrl-adjuntar .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/compromiso-presupuestal/adjuntar-documento-uafsi?ID='+ID);
        $('#modal-ctrl-adjuntar').modal('show');
    });
    
    $('body').on('click', '.btn-remove-documento', function (e) {
        e.preventDefault();
        var $this = $(this);
        bootboxConfirmEliminar($this,
        '¿Está seguro de eliminar el documento adjunto?',
        function () {
            var id = $this.attr('data-id');
            var toke = '<?=Yii::$app->request->getCsrfToken()?>';
            $.get('<?= \Yii::$app->request->BaseUrl ?>/compromiso-presupuestal/eliminar-adjunto-uafsi', { id: id ,_csrf: toke }, function (data) {
                var jx = JSON.parse(data);
                if (jx.Success == true) {
                    location.reload();
                }
            });
        });
    });



</script>