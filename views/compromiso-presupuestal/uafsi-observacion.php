<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>


<?php $form = ActiveForm::begin(
    [
        'action'            => '../compromiso-presupuestal/uafsi-observacion?ID='.$compromisoPresupuestal->ID,
        'options'           => [
            'enctype'       =>'multipart/form-data',
            'id'            => 'frmRequerimientos',
            
        ]
    ]
); ?>
    <div class="modal-body">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label">Observación:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="CompromisoPresupuestal[Observacion]" required></textarea>
                </div>
            </div>  
        </div>
    </div>
    
    
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>


<style type="text/css">
    .none{
        display: none;
    }
    .block{
        display: block;
    }

    .ui-draggable, .ui-droppable {
      background-position: top;
    }
    .ui-widget-content{
        position: relative;
        z-index: 9999;
        width: 400px !important;
    }
</style>



<script>
    var $form = $('#frmRequerimientos');
    var formParsleyRequerimientos = $form.parsley(defaultParsleyForm());

</script>

