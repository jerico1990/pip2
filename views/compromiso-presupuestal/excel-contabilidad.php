<?php
use app\models\Orden;
use app\models\Viatico;
use app\models\CajaChica;
use app\models\Encargo;

/** Error reporting */
error_reporting(E_ALL);

/** Include path **/
ini_set('include_path', ini_get('include_path').';../web/PHPExcel/Classes/');
//require(__DIR__ . '/../../web/PHPExcel/Classes/');
/** PHPExcel */
include 'PHPExcel.php';

/** PHPExcel_Writer_Excel2007 */
include 'PHPExcel/Writer/Excel2007.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
$sheet = $objPHPExcel->getActiveSheet();

$sheet->setTitle('Requerimientos');


$objPHPExcel->setActiveSheetIndex(0);



$sheet->SetCellValue('A1', '#');
$sheet->SetCellValue('B1', 'Código Proyecto');
$sheet->SetCellValue('C1', 'Tipo');
$sheet->SetCellValue('D1', 'Razon Social');
$sheet->SetCellValue('E1', 'RUC');
$sheet->SetCellValue('F1', 'Total');

$i=2;
// echo '<pre>';
// print_r($resultado); die();
foreach($resultado as $result)
{
    $sheet->SetCellValue('A'.$i, $result->TipoDetalleOrdenID);
    $sheet->SetCellValue('B'.$i, $result->Codigo);
    $sheet->SetCellValue('C'.$i, $result->NombreGasto);
    // echo $result->NombreID;
    if($result->NombreID == 1  || $result->NombreID == 2 || $result->NombreID == 3){
        $ordn = Orden::findOne($result->TipoDetalleOrdenID);
        if(!empty($ordn)){
        	$sheet->SetCellValue('D'.$i, $ordn->RazonSocial);
        	$sheet->SetCellValue('E'.$i, $ordn->RUC);
        }
    }else if($result->NombreID == 4){
        $ordn = Viatico::findOne($result->TipoDetalleOrdenID);
        if(!empty($ordn)){
	        $datos = $ordn['Apellido'].' '.$ordn['Nombre'];
	        $sheet->SetCellValue('D'.$i, $datos);
	        $sheet->SetCellValue('E'.$i, $ordn['Dni']);
	    }
    }else if($result->NombreID == 5){
        $ordn = CajaChica::findOne($result->TipoDetalleOrdenID);
        if(!empty($ordn)){
	        $sheet->SetCellValue('D'.$i, $ordn['Responsable']);
	        $sheet->SetCellValue('E'.$i, $ordn['Dni']);
        }
    }else if($result->NombreID == 6){
    	$ordn = Encargo::findOne($result->TipoDetalleOrdenID);
        if(!empty($ordn)){
	    	$sheet->SetCellValue('D'.$i, $ordn->ApellidosEncargo.' '.$ordn->NombresEncargo);
	    	$sheet->SetCellValue('E'.$i, $ordn->DNIEncargo);
        }
    }
    $sheet->SetCellValue('F'.$i, $result->Total);

    // $sheet->SetCellValue('C'.$i, str_pad($result["Correlativo"], 3, "0", STR_PAD_LEFT));
    $i++;
}


// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="CompromisosPresupuestales.xls"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
