<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'observar-compromiso?ID='.$ID.'&DetalleCompromisoID='.$DetalleCompromisoID.'&CodigoProyecto='.$CodigoProyecto.'&Tipo='.$Tipo,
        'options'           => [
            'autocomplete'  => 'off',
            'id'            => 'frmObservar'
        ]
    ]
); ?>
<div class="modal-body">
    <div class="form-horizontal">
        <div class="form-group">
            <label class="col-sm-3 control-label">Observación <span class="f_req">*</span></label>
            <div class="col-sm-9">
                <textarea <?= ($Tipo==2)?'disabled':''; ?> class="form-control" name="ObservacionGeneral[Observacion]" required><?= $observar->Observacion ?></textarea>
                <!--<h6 class="pull-left count-message"></h6>-->
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <?php if($Tipo==1){ ?>
        <button id="btn-observacion" type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Enviar</button>
        <?php } ?>
    </div>
</div>


<?php ActiveForm::end(); ?>
<script>
    var formParsleyObservar = $('#frmObservar').parsley(defaultParsleyForm());

</script>