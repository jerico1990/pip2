<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

// $componenteOptions="<option value>Seleccionar</option>";
// foreach($componentes as $componente){
//     $componenteOptions=$componenteOptions."<option value='".$componente->ID."'>".$componente->Nombre."</option>";
// }
?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'generar-compromiso-anual',
        'options'           => [
            'id'            => 'frmCompromisoPresupuestal',
            'enctype'       =>'multipart/form-data',
        ]
    ]
); ?>
    <input type="hidden" name="CodigoProyecto" value="<?php echo $CodigoProyecto ?>">
    <div class="modal-body">
        <div class="form-horizontal">
            <div id="loading" style="position: absolute;z-index: 1;margin-left: 34%;"></div>
            <div class="form-group">
                <p id="saldo" style="color: red;font-weight: bold;text-align: center;"></p>
                <label class="col-sm-2 control-label">Fuente:<span class="f_req">*</span></label>
                <div class="col-sm-10">
                    <input type="hidden" value="" id="saldoFinanciamiento">
                    <input type="hidden" value="<?php echo $CodigosIDs ?>" name="CodigosIDs">
                    <input type="hidden" value="<?php echo $ID ?>" name="ID">
                    <select class="form-control" name="fuenteFinanciamiento" id="fuenteFinanciamiento" data-certificacion = "<?php echo $Certificacion ?>" data-annio = "<?php echo $Annio ?>" required>
                        <option value>Seleccionar </option>
                        <option value="0000">RO</option>
                        <option value="1900">ROOC</option>
                    </select>
                </div>
            </div>

	        <div class="form-group">
                <label class="col-sm-2 control-label">Correlativo:<span class="f_req">*</span></label>
                <div class="col-sm-10">
                    <select class="form-control" name="CorrelativoFuente" id="CorrelativoFuente" disabled required>
            			<option value>Seleccionar </option>
        		    </select>
                </div>
            </div>
        </div>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button id="btn-save-desembolso" type="submit" class="btn btn-primary" disabled><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>


<script type="text/javascript">
    var $form = $('#frmCompromisoPresupuestal');
    var formParsleyConformidadPago = $form.parsley(defaultParsleyForm());
    validarNumeros()
</script>