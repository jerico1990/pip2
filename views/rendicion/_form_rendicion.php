<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'rendicion/crear?CodigoProyecto='.$CodigoProyecto,
        'options'           => [
            'enctype'       =>'multipart/form-data',
            'id'            => 'frmRendicion',
            
        ]
    ]
); ?>

    <input type="hidden" name="Requerimiento[CodigoProyecto]" value="<?= $CodigoProyecto ?>">
    <div class="modal-body">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label">Tipo:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                	<input type="hidden" name="Rendicion[CodigoProyecto]" value="<?php echo $CodigoProyecto ?>">
                    <select type="text" class="form-control" name="Rendicion[TipoGastoID]" id="TipoGasto" required>
                        <option value="">[SELECCIONE]</option>
                        <?php foreach ($tipoGasto as $value): ?>
                            <option value="<?php echo $value->ID ?>"><?php echo $value->Nombre ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Codigo:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <select type="text" class="form-control" name="Rendicion[TransaccionID]" id="tipo" required>
                        <option value="">[SELECCIONE]</option>
                    </select>
                </div>
            </div>

        </div>
    </div>
    
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" id="btn-guardar-rendimiento" data-codigo-proyecto="<?= $CodigoProyecto ?>" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>


<style type="text/css">
    .none{
        display: none;
    }
    .block{
        display: block;
    }

    .ui-draggable, .ui-droppable {
      background-position: top;
    }
    .ui-widget-content{
        position: relative;
        z-index: 9999;
        width: 400px !important;
    }
</style>



<script>
    var $form = $('#frmRendicion');
    var formParsleyRendicion = $form.parsley(defaultParsleyForm());

</script>

