
<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay_progress.min.js"></script>
<script src=" //cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>


<div id="page-content" style="min-height: 1342px;">
    
    <div id="wrap">
        <div id="page-heading">
            <h1>Rendición viático</h1>
        </div>
        
           
        <div class="container">
             <div class="form-group">
                
                <a href="#" class="btn btn-primary btn-crear-rendicion" data-codigo-proyecto="" data-proyecto="<?php echo $CodigoProyecto ?>">Generar Rendición</a>    
                
                
            </div>
            <div class="table-responsive">
                <table id="proyectos" class="display table table-bordered table-striped table-condensed table-hover" cellspacing="0" >
                    <thead>
                        <tr>
                            <th>Nº</th>
                            <th>Datos Personales</th>
                            <th>Dni</th>
                            <th>Recibido</th>
                            <th>Sustentado</th>
                            <th>Reembolso</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-ctrl-rendicion" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Generar Rendición</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-ctrl-viaticos" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Rendición Viáticos</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>


<script>
    function js_buscar()
    {
       
    }
    var tblresultjs;
    var configDTjs={
        "order": [[ 2, "desc" ]],
            "language": {
                "sProcessing":    "Procesando...",
                "sLengthMenu":    "Mostrar _MENU_ registros",
                "sZeroRecords":   "No se encontraron resultados",
                "sEmptyTable":    "Ningún dato disponible en esta lista",
                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":   "",
                "sSearch":        "Buscar:",
                "sUrl":           "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":    "Último",
                    "sNext":    "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
    };

    $(document).ready(function() {
        // var CodigoProyecto="";
        $.ajax({
            url:'rendicion/lista-rendicion?CodigoProyecto='+'<?php echo $CodigoProyecto ?>',
            async:false,
            data:{},
            beforeSend:function()
            {
                $('.panel-default').LoadingOverlay("show");
            },
            success:function(result)
            {
                    //$tblresultjs.destroy();//
                    $("#proyectos tbody").html(result);
                    $('#proyectos').DataTable(configDTjs);
                    //tblresultjs=$('#example').DataTable(configDTjs);
                    $('.panel-default').LoadingOverlay("hide",true);
            },
            error:function(){
                $('.panel-default').LoadingOverlay("hide",true);
                alert('Error al realizar el proceso de busqueda.');
            }
        });
        
    } );
    
    // $('body').on('click', '.btn-crear-rendicion', function (e) {
    //     e.preventDefault();
    //     var CodigoProyecto = $(this).attr('data-codigo-proyecto');
    //     $('#modal-ctrl-rendicion .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/rendicion/generar-rendicion');
    //     $('#modal-ctrl-rendicion').modal('show');
    // });

    
    $('body').on('change','#TipoGasto', function(){
        var gasto = $(this).val();
        // alert(gasto);
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.get('<?= \Yii::$app->request->BaseUrl ?>/rendicion/tipo', { tipo: gasto ,_csrf: toke }, function (data) {
            var jx = JSON.parse(data);
            console.log(jx[0]);
            if (jx[0].Success == true) {
                $('#tipo').html('');
                var ht = '';
                // $.(jx).each(function(x,y){
                jQuery.each(jx, function(x, y) {
                    ht+= '<option value="'+y.ID+'" >'+y.Correlativo+'</option>';
                });
                console.log(ht);
                $('#tipo').html(ht);
            }
        });
    });

    $('body').on('click', '#btn-guardar-rendimiento', function (e) {
        e.preventDefault();
        var $btn = $(this);
        $btn.attr('disabled', 'disabled');
        var isValid = formParsleyRendicion.validate();
        if (isValid) {
            sendFormNoMessage($('#frmRendicion'), $(this), function (msg) {
                $("#modal-ctrl-rendicion").modal('hide');
                location.reload();
                _pageLoadingEnd();
            });
        } else {
        }
    });

    $('body').on('click','.btn-crear-rendicion',function(){
        var tipo = $(this).attr('data-tipo');
        var CodigoProyecto = $(this).attr('data-proyecto');
        var codigo = $(this).attr('data-transaccion');
        
        $('#modal-ctrl-viaticos .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/viatico/rendicion?CodigoProyecto='+CodigoProyecto+'&codigo='+codigo);
        $('#modal-ctrl-viaticos').modal('show');
    });

    $('body').on('click', '#btn-guardar-rendimiento-viatico', function (e) {
        e.preventDefault();
        var $btn = $(this);
        $btn.attr('disabled', 'disabled');
        var isValid = formParsleyRendicion.validate();
        if (isValid) {
            sendFormNoMessage($('#rendicionViatico'), $(this), function (msg) {
                // $("#modal-ctrl-rendicion").modal('hide');
                location.reload();
                _pageLoadingEnd();
            });
        } else {
            $btn.removeAttr('disabled');
        }
    });
    

    
    $('body').on('click', '.btn-remove', function (e) {
        e.preventDefault();
        var $this = $(this);
        bootboxConfirmEliminar($this,
        'Está seguro de eliminar el registro?',
        function () {
            var id = $this.attr('data-id');
            var toke = '<?=Yii::$app->request->getCsrfToken()?>';
            $.get('<?= \Yii::$app->request->BaseUrl ?>/rendicion/eliminar', { id: id ,_csrf: toke }, function (data) {
                var jx = JSON.parse(data);
                if (jx.Success == true) {
                    location.reload();
                }
            });
        });
    });
</script>