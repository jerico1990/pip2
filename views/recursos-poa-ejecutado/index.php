<div id="page-content" style="min-height: 885px;">
    <div id="wrap">
        <div id="page-heading">
            <h1></h1>
        </div>
        <div class="container">
                    
            <style>
                #page-heading {
                    display: none;
                }

                .tab-content {
                    background-color: #fff;
                }

                .panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 1px solid #a8a8ab !important;
                }

                .panel-gray .panel-body {
                    border-top: none !important;
                }

                .panel-heading {
                    height: auto !important;
                }
            </style>

            <div class="tab-container">
                <ul class="nav nav-tabs">
                    <li id="li-poaf" class="active"><a href="#tab-poaf" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Recursos</a></li>
                    <!-- <li id="li-pc"><a href="#container-pc" data-toggle="tab"><i class="fa fa-pencil"></i>&nbsp;Pasos Críticos</a></li> -->
                </ul>
                <div class="tab-content">

                    <div class="tab-pane active" id="tab-poaf">
                        <div id="container-poaf">

                            <style>
                                #container-poaf .input-number {
                                    text-align: right;
                                    min-width: 72px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }

                                #container-poaf .input-number-sm {
                                    text-align: right;
                                    min-width: 52px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }

                                #container-poaf .input-number-md {
                                    text-align: right;
                                    min-width: 82px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }
                                
                                #container-poaf .table-act > tbody > tr > td {
                                    text-align: center !important;
                                    min-width: 82px !important;
                                }
                                
                                
                                

                                #container-poaf .table > tbody > .tr-header > td {
                                    text-align: center;
                                    font-size: 12px;
                                    font-weight: bold;
                                    padding: 1px !important;
                                    vertical-align: middle !important;
                                    /*border: 1px solid #cfcfd0;
                                    background-color: #f0f0f1;*/
                                    border: 1px solid #c0c0c0;
                                    background-color: #e3e3e3;
                                    min-width: 35px !important;
                                }
                                
                                .ejecutado {
                                    text-align: center;
                                    font-size: 12px;
                                    font-weight: bold;
                                    padding: 1px !important;
                                    vertical-align: middle !important;
                                    /*border: 1px solid #cfcfd0;
                                    background-color: #f0f0f1;*/
                                    border: 1px solid #c0c0c0;
                                    background-color: #e3e3e3;
                                    min-width: 35px !important;
                                }
                             

                                #container-poaf h4.panel-title {
                                    font-size: 16px;
                                }

                                #container-poaf .tr-comp td, .tr-proy td {
                                    font-weight: bold;
                                }

                                #container-poaf .table-condensed td {
                                    padding: 1px !important;
                                }

                                #container-poaf td, #table-container input {
                                    font-size: 13px !important;
                                }

                                #container-poaf .form-control[readonly] {
                                    background-color: #f2f3f4;
                                }
                            </style>


                            <!-- <div class="text-center">
                                <button class="btn btn-primary btn-save-poaf"><i class="fa fa-check"></i>&nbsp;Guardar</button>
                                <a href="javascript:void(0);" class="btn btn-inverse-alt" onclick="loadPoaf();">
                                    <i class="fa fa-refresh"></i>&nbsp;Actualizar
                                </a>
                                <a class="btn btn-inverse-alt" href="/SLFC/PasoCritico/DescargarPC_PlanOperativoDetallado">
                                    <i class="fa fa-download"></i>&nbsp;Descargar
                                </a>
                            </div> -->
                            <br>

                            <div id="table-container"></div>
                            
                            <div id="table-container-paso-critico"></div>

                            <br>
                            <div class="text-center">
                                <?php /*if(\Yii::$app->user->identity->Rol==2){?>
                                <button class="btn btn-primary btn-aprobar-poa" ><i class="fa fa-check"></i>&nbsp;Aprobar POA</button>
                                <button class="btn btn-primary btn-observar-poa" ><i class="fa fa-check"></i>&nbsp;Observar POA</button>
                                <?php }*/ ?>
                                <!--<button class="btn btn-primary btn-procesar-ejecucion-recurso" ><i class="fa fa-check"></i>&nbsp;Procesar ejecución de recursos</button>-->
                                <!--
                                <a target="_blank" class="btn btn-inverse-alt" href="<?= Yii::$app->getUrlManager()->createUrl('recursos-poa-ejecutado/descargar-poa?codigo='.$usuario->username.'') ?>">
                                    <i class="fa fa-download"></i>&nbsp;Descargar
                                </a>-->
                            </div> 
                            <!-- <div class="text-center">
                                <button class="btn btn-primary btn-save-poaf"><i class="fa fa-check"></i>&nbsp;Guardar</button>
                                <a class="btn btn-inverse-alt" href="/SLFC/PasoCritico/DescargarPC_PlanOperativoDetallado">
                                    <i class="fa fa-download"></i>&nbsp;Descargar
                                </a>
                            </div> -->

                            <script>
                                
                                
                                var _proyecto = new Object();

                                $(document).ready(function () {
                                    $('#table-container').html('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>');
                                    getProyecto().done(function(json) {
                                        _proyecto = json;
                                        _drawTable();
                                    });
                                    
                                    getPasoCritico().done(function(json) {
                                        _proyectoPaso = json;
                                        _drawTable2();
                                    });
                                });
                                
                                $('.btn-descargar-poa').click(function (e) {
                                    var $btn = $(this);
                                    var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                                    $.ajax({
                                        url: '<?= Yii::$app->getUrlManager()->createUrl('recursos-poa-ejecutado/descargar-poa') ?>',
                                        type: 'GET',
                                        async: false,
                                        data: {codigo:'<?= $usuario->username ?>',_csrf: toke},
                                        success: function(data){
                                           
                                        }
                                    });
                                });
                                
                                function getNum(val) {
                                    if (val == '0.00' || val == '') {
                                         return 0;
                                    }
                                    var val2 = val.replace(',','');
                                    return val2;
                                }
                                

                                $('body').on('change', '.input-poaf-crono-act-mfis', function (e) {
                                    e.preventDefault();
                                    $(this).attr('changed', 'changed');
                                    
                                });
                                
                                
                                $('body').on('click', '.btn-enviar-evaluacion-poa', function (e) {
                                    e.preventDefault();
                                    var $this = $(this);
                                    var codigo="<?= $usuario->username ?>";
                                    var error='';
                                    
                                    $('.recurso-meta-fisica').each(function(){
                                        
                                        var suma=0;
                                        var RecursoID=$(this).attr('data-recurso-id');
                                        var RecursoMetaFisica=$(this).attr('data-meta-fisica');
                                        $('.recurso_'+RecursoID).each(function(){
                                            suma = suma+parseFloat(getNum($(this).val()));
                                            
                                        });
                                        
                                        if (RecursoMetaFisica>suma) {
                                            
                                            error=error+'a';
                                        }
                                        
                                    });
                                    
                                    if (error!='') {
                                        bootbox.alert('Falta programar algunos recurso');
                                        return false;
                                    }
                                    bootbox.alert('Su programación de recursos, esta correcta');
                                    
                                });

                                $('body').on('click', '.input-sub-total', function (e) {
                                    e.preventDefault();
                                    var $this = $(this);
                                    var acumusubtotal = 0.00;
                                    $this.closest('.tr-sub-tot').find('.input-sub-total').each(function () {
                                        var _valor = parseFloat($(this).inputmask('unmaskedvalue'));
                                        if (!isNaN(_valor)) {
                                            acumusubtotal += _valor;
                                        }
                                    });
                                    $this.closest('.tr-sub-tot').find('.input-act-tot-mfs').val(acumusubtotal);
                                });

                                $('body').on('click', '.input-sub2-total', function (e) {

                                    e.preventDefault();
                                    var $this = $(this);
                                    var acumusubtotal = 0.00;
                                    $this.closest('.tr-sub2-tot').find('.input-sub2-total').each(function () {

                                        var _valor = parseFloat($(this).inputmask('unmaskedvalue'));
                                        if (!isNaN(_valor)) {
                                            acumusubtotal += _valor;
                                        }
                                    });
                                    $this.closest('.tr-sub2-tot').find('.input-act-tot-mfss').val(acumusubtotal);
                                    //$(this).attr('changed-poaf-crono-arescat-mf', '');
                                });

                                $('body').on('change', '.input-poaf-crono-arescat-mfin', function (e) {
                                    e.preventDefault();
                                    $(this).attr('changed', 'changed');
                                    
                                });
                                
                                function getProyecto() {
                                    var url = "<?= \Yii::$app->request->BaseUrl ?>/recursos-poa-ejecutado/recursos-finanzas-json?codigo=<?= $usuario->username ?>";
                                    return $.getJSON( url );
                                }
                                
                                function getPasoCritico() {
                                    var url = "<?= \Yii::$app->request->BaseUrl ?>/paso-critico/recursos-json?codigo=<?= $informacionGeneral->Codigo ?>&mesInicio=<?= $pasocritico->MesInicio ?>&mesFin=<?= $pasocritico->MesFin ?>";
                                    return $.getJSON( url );
                                }

                                function _drawTable() {
                                
                                    var html = '';
                                    
                                            html += '<div class="table-responsive">';
                                            html += '<table class="table table-bordered table-condensed table-act">';
                                                html += '<tbody>';
                                                    html += '<tr class="tr-header">';
                                                        html += '<td>';
                                                            html += 'PC';
                                                        html += '</td>';
                                                        html += '<td>';
                                                            html += 'Inicio';
                                                        html += '</td>';
                                                        html += '<td>';
                                                            html += 'Término';
                                                        html += '</td>';
                                                        html += '<td>';
                                                            html += 'Programado';
                                                        html += '</td>';
                                                        
                                                        html += '<td>';
                                                            html += 'Programado Actual';
                                                        html += '</td>';
                                                        
                                                        html += '<td>';
                                                            html += 'Ejecutado';
                                                        html += '</td>';
                                                        html += '<td>';
                                                            html += 'Avance %';
                                                        html += '</td>';
                                                    html += '</tr>';
                                                    var PasoCriticos=_proyecto.PasoCritico;
                                                    console.log(PasoCriticos);
                                                    for (var i_paso = 0; i_paso < _proyecto.PasoCritico.length; i_paso++) {
                                                        
                                                        if (!PasoCriticos[i_paso].MontoTotalEjecutado) {
                                                            PasoCriticos[i_paso].MontoTotalEjecutado=0;
                                                        }
                                                        if (!PasoCriticos[i_paso].MontoTotal) {
                                                            PasoCriticos[i_paso].MontoTotal=0;
                                                        }
                                                        
                                                    html += '<tr>';
                                                        html += '<td>';
                                                            html += PasoCriticos[i_paso].Correlativo;
                                                        html += '</td>';
                                                        html += '<td>';
                                                            html += PasoCriticos[i_paso].MesInicio;
                                                        html += '</td>';
                                                        html += '<td>';
                                                            html += PasoCriticos[i_paso].MesFin;
                                                        html += '</td>';
                                                        html += '<td class="">';
                                                            html += 'S/ '+formato_numero(PasoCriticos[i_paso].MontoTotal,2);
                                                        html += '</td>';
                                                        html += '<td class="">';
                                                            html += 'S/ '+formato_numero(PasoCriticos[i_paso].MontoTotal2,2);
                                                        html += '</td>';
                                                        html += '<td>';
                                                            html += 'S/ '+formato_numero(PasoCriticos[i_paso].MontoTotalEjecutado,2);
                                                        html += '</td>';
                                                        html += '<td>';
                                                            if (PasoCriticos[i_paso].MontoTotal) {
                                                                html += ''+((PasoCriticos[i_paso].MontoTotalEjecutado*100)/PasoCriticos[i_paso].MontoTotal).toFixed(2)+' %';
                                                            }
                                                            else{
                                                                html +='0';
                                                            }
                                                            
                                                        html += '</td>';
                                                    html += '</tr>';
                                                    }
                                                    
                                                html += '</tbody>';
                                            html += '</table>';
                                            html += '</div>';
                                    
                                    $('#table-container').html(html);
                                    $('.table-responsive').css('max-height', $(window).height() * 0.60);
                                    $('.input-number').inputmask("decimal", {
                                        radixPoint: ".",
                                        groupSeparator: ",",
                                        groupSize: 3,
                                        digits: 2,
                                        integerDigits: 4,
                                        autoGroup: true,
                                        allowPlus: false,
                                        allowMinus: true,
                                        placeholder: ''
                                    }).click(function () {
                                        $(this).select();
                                    });
                                    
                                    $('.input-number-porcentaje,.input-number-recurso-porcentaje').inputmask("decimal",{integerDigits: 4,digits: 2,});
                                    
                                }
                                    
                                    
                                    
                                function _drawTable2() {
                                   
                                    var situacionRecurso=<?= \Yii::$app->user->identity->SituacionRecurso ?>;
                                    var html = '';
                                    var cantmeses = _proyectoPaso.Cronogramas.length;
                                    for (var i_comp = 0; i_comp < _proyectoPaso.Componentes.length; i_comp++) {
                                        var comp = _proyectoPaso.Componentes[i_comp];
                                        html += '<div class="panel panel-gray"><div class="panel-heading">'
                                                + '<h4 class="panel-title">'
                                                + (i_comp + 1) + '. Objetivo específico - ' + comp.Nombre
                                                + '</h4></div>';
                                        html += '<div class="panel-body">';
                                        html += '<div class="table-responsive">';
                                        html += '<table class="table table-bordered table-condensed table-act">';
                                        html += '<tbody>';
                                        for (var i_act = 0; i_act < comp.Actividades.length; i_act++)
                                        {
                                            var act = comp.Actividades[i_act];
                                            var TotalActividad=0;
                                            for (var i_are = 0; i_are < act.ActRubroElegibles.length; i_are++) {
                                                var are = act.ActRubroElegibles[i_are];
                                                if (are.AreSubCategorias.length) {
                                                    for (var i_arescat = 0; i_arescat < are.AreSubCategorias.length; i_arescat++) {
                                                        var arescat = are.AreSubCategorias[i_arescat];
                                                        if (arescat) {
                                                            TotalActividad=TotalActividad+(arescat.CostoUnitario*arescat.MetaFisica);
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            html += '<tr class="tr-header">';
                                                html += '<td rowspan="2">#</td>';
                                                html += '<td rowspan="2">Actividad</td>';
                                                html += '<td rowspan="2">Unidad de medida</td>';
                                                html += '<td rowspan="2">Costo unitario</td>';
                                                html += '<td rowspan="2">Meta física</td>';
                                                html += '<td rowspan="2">Total</td>';
                                                for (var i_crono_act = 0; i_crono_act < cantmeses; i_crono_act++) {
                                                    html += '<td  colspan="2"> Mes ' + act.Cronogramas[i_crono_act].MesDescripcion + '</td>';
                                                }
                                            html += '</tr>';
                                            html += '<tr class="tr-header">';
                                                for (var i_crono_act = 0; i_crono_act < cantmeses; i_crono_act++) {
                                                    html += '<td> PROG</td>';
                                                    html += '<td> EJEC</td>';
                                                }
                                            html += '</tr>';
                                            
                                            html += '<tr class="tr-act-mf info" data-act-id="' + act.ID + '">';
                                                html += '<td>' + (i_comp + 1) + '.' + (i_act + 1) + '</td>';
                                                html += '<td><b>' + act.Nombre + '</b></td>';
                                                html += '<td>' + act.UnidadMedida+'</td>';
                                                html += '<td><input type="text" readonly="" class="input-number form-control" value="'+(TotalActividad/act.MetaFisica)+'"></td>'
                                                html += '<td align="center">' + act.MetaFisica + '</td>';
                                                html += '<td><input type="text" readonly="" class="input-number form-control" value="'+TotalActividad+'"></td>';
                                                for (var i_crono_act = 0; i_crono_act < cantmeses; i_crono_act++) {
                                                    html += '<td colspan="2"></td>';
                                                }
                                            html += '</tr>';
                                            
                                            
                                            for (var i_are = 0; i_are < act.ActRubroElegibles.length; i_are++) {
                                                
                                                var are = act.ActRubroElegibles[i_are];
                                                var TotalRubroElegible=0;
                                                if (are.AreSubCategorias.length) {
                                                    for (var i_arescat = 0; i_arescat < are.AreSubCategorias.length; i_arescat++) {
                                                        var arescat = are.AreSubCategorias[i_arescat];
                                                        if (arescat) {
                                                            TotalRubroElegible=TotalRubroElegible+(arescat.CostoUnitario*arescat.MetaFisica);
                                                        }
                                                    }
                                                }
                                                html += '<tr class="tr-sub2-tot success">';
                                                    html += '<td></td>';
                                                    html += '<td>' + are.RubroElegible.Nombre + '</td>';
                                                    html += '<td></td>';
                                                    html += '<td></td>';
                                                    html += '<td></td>';
                                                    html += '<td><input type="text" class="form-control input-number" readonly="" value="'+TotalRubroElegible+'" /></td>';
                                                    for (var i_crono_are = 0; i_crono_are < cantmeses; i_crono_are++) {
                                                        html += '<td colspan="2"></td>';
                                                    }
                                                    // Sumatorias
                                                html += '</tr>';
                                                // Subcategorías
                                                if (are.AreSubCategorias.length) {
                                                    for (var i_arescat = 0; i_arescat < are.AreSubCategorias.length; i_arescat++) {
                                                        var arescat = are.AreSubCategorias[i_arescat];
                                                        if (arescat) {
                                                            html += '<tr class="tr-subact warning">';
                                                                html += '<td align="center"></td>';
                                                                html += '<td class="recurso_alerta_'+arescat.ID+'">' + arescat.Nombre + '</td>';
                                                                html += '<td>' + arescat.UnidadMedida + '</td>';
                                                                html += '<td><input type="text" class="form-control input-number" readonly="" value="' + arescat.CostoUnitario + '" /></td>';
                                                                html += '<td><input type="text"  class=" recurso_alerta_'+arescat.ID+' form-control input-number-sm recurso-meta-fisica" data-meta-fisica="'+ arescat.MetaFisica +'" data-recurso-id="'+ arescat.ID +'" readonly="" value="' + arescat.MetaFisica + '" /></td>';
                                                                html += '<td><input type="text" class="form-control input-number" readonly="" value="'+(arescat.CostoUnitario*arescat.MetaFisica)+'" /></td>';
                                                                for (var i_crono_arescat = 0; i_crono_arescat < cantmeses; i_crono_arescat++) {
                                                                    /*if (arescat.Cronogramas[i_crono_arescat]) {
                                                                        html += '<td></td><td><input disabled type="text"'
                                                                            + 'data-recurso-id="'+ arescat.ID +'"'
                                                                            + 'data-meta-fisica="'+ arescat.MetaFisica +'"'
                                                                            + 'data-cronograma-recurso-id="'+ arescat.Cronogramas[i_crono_arescat].ID +'" class="form-control success input-number recurso_'+arescat.ID+' btn-save-cronograma-recurso" value="'
                                                                            + arescat.Cronogramas[i_crono_arescat].MetaFisica + '" /></td>';
                                                                    }*/
                                                                    html += '<td>'+arescat.Cronogramas[i_crono_arescat].MetaFisica+'</td>';
                                                                    html += '<td></td>';
                                                                }
                                                            html += '</tr>';
                                                        }
                                                        
                                                    }
                                                }
                                            }
                                        }
                                        // Totales por componente
                                        html += '</tbody>';
                                        html += '</table>';
                                        html += '</div>';
                                        html += '</div>';
                                        html += '</div>';
                                    
                                    
                                    $('#table-container-paso-critico').html(html);
                                    }
                                }
                            </script>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal-ctrl-recurso" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Recursos</h4>
                        </div>
                        <div class="modal-body-main">
                        </div>
                    </div>
                </div>
            </div>

            <div id="modal-add-pc" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body-main">
                        </div>
                    </div>
                </div>
            </div>

            <div id="modal-create-indpc" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Indicador de paso crítico</h4>
                        </div>
                        <div class="modal-body-main">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>







<script type="text/javascript">
    $('body').on('click', '.btn-remove-recurso', function (e) {
        e.preventDefault();
        var $this = $(this);
        bootboxConfirmEliminar($this,
            'Está seguro de eliminar el recurso?',
            function () {
                var id = $this.attr('data-id');
                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                $.get('<?= \Yii::$app->request->BaseUrl ?>/recursos-poa-ejecutado/delete-recurso', { id: id ,_csrf: toke }, function (data) {
                    var jx = JSON.parse(data);
                    if (jx.Success == true) {
                        
                    }
                });
            });
    });
    
    $('body').on('click', '.btn-edit-recurso', function (e) {
        e.preventDefault();
        var idActv = $(this).attr('data-act-idd');
        var idInvestigador = <?= $investigadorID ?>;
        var id=$(this).attr('data-id');
        $('#modal-ctrl-recurso .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/recursos-poa-ejecutado/recursos-actualizar?id='+id+'&actv='+idActv);
        $('#modal-ctrl-recurso').modal('show');
    });
    
    $('body').on('click', '.btn-actv-rb', function (e) {
        e.preventDefault();
        var idActv = $(this).attr('data-act-idd');
        var idInvestigador = <?= $investigadorID ?>;
        
        $('#modal-ctrl-recurso .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/recursos-poa-ejecutado/recursos?id='+idActv+'&investigador='+idInvestigador);
        $('#modal-ctrl-recurso').modal('show');
    });
    
    

    $(document).on('click','#btn-save-recurso',function (e) {
        e.preventDefault();
        var $btn = $(this);
        $btn.attr('disabled', 'disabled');
        var isValid = formParsleyRecursos.validate();
        if (isValid) {
            sendFormNoMessage($('#frmRecursos'), $(this), function () {
                //cargarObjetivoProyectoTable();
                $("#div-presupuesto").html('');
                getProyecto().done(function(json) {
                    _proyecto = json;
                    _drawTable();
                });
                
                getPasoCritico().done(function(json) {
                    _pasocritico = json;
                    _drawTable2();
                });
                $("#modal-ctrl-recurso").modal('hide');
                _pageLoadingEnd();
            });
        } else {
            $btn.removeAttr('disabled');
        }
    });


</script>






<!--

<div id="modal-loading" class="modal fade modal-loading in" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-hidden="false" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content-no-shadow modal-content">
            <div class="text-center"><i class="fa fa-spinner fa-pulse fa-fw" style="color:#16a085;font-size: 100px;"></i></div>
        </div>
    </div>
</div>
-->