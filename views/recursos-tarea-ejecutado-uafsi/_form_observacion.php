<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => \Yii::$app->request->BaseUrl.'/recursos-tarea-ejecutado-uafsi/observar-tarea?CronogramaTareaID='.$CronogramaTareaID.'&CodigoProyecto='.$CodigoProyecto.'&InvestigadorID='.$InvestigadorID,
        'options'           => [
	    'enctype'       =>'multipart/form-data',
            'autocomplete'  => 'off',
            'id'            => 'frmObservar'
        ]
    ]
); ?>
    <div class="modal-body">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label">Descripción<span class="f_req">*</span></label>
                <div class="col-sm-9">
		    <textarea class="form-control" name="ObservacionGeneral[Observacion]" required><?= $model->Observacion ?></textarea>
                </div>
            </div>
	    
	    
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	<button id="btn-guardar-observacion" type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>


<script>
    var $form = $('#frmObservar');
    var formParsleyObservar = $form.parsley(defaultParsleyForm());
</script>