<div id="page-content" style="min-height: 885px;">
    <div id="wrap">
        <div id="page-heading">
            <h1></h1>
        </div>
        <div class="container">
                    
            <style>
                #page-heading {
                    display: none;
                }

                .tab-content {
                    background-color: #fff;
                }

                .panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 1px solid #a8a8ab !important;
                }

                .panel-gray .panel-body {
                    border-top: none !important;
                }

                .panel-heading {
                    height: auto !important;
                }
            </style>

            <div class="tab-container">
                <ul class="nav nav-tabs">
                    <li id="li-poaf" class="active"><a href="#tab-poaf" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Tareas</a></li>
                    <!-- <li id="li-pc"><a href="#container-pc" data-toggle="tab"><i class="fa fa-pencil"></i>&nbsp;Pasos Críticos</a></li> -->
                </ul>
                <div class="tab-content">

                    <div class="tab-pane active" id="tab-poaf">
                        <div id="container-poaf">

                            <style>
                                #container-poaf .input-number {
                                    text-align: right;
                                    min-width: 72px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }

                                #container-poaf .input-number-sm {
                                    text-align: right;
                                    min-width: 52px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }

                                #container-poaf .input-number-md {
                                    text-align: right;
                                    min-width: 82px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }
                                
                                #container-poaf table-act > tbody > tr > td:nth-child(1) {
                                    text-align: center;
                                }

                                #container-poaf .table-act > tbody > tr > td:nth-child(5) {
                                    min-width: 320px !important;
                                }

                                #container-poaf .table-act > tbody > tr > td:nth-child(2) {
                                    min-width: 100px !important;
                                }
                                
                                #container-poaf .table-act > tbody > tr > td:nth-child(6) {
                                    min-width: 100px !important;
                                }
                                

                                #container-poaf .table > tbody > .tr-header > td {
                                   min-width: 50px;
                                    text-align: center;
                                    font-size: 12px;
                                    font-weight: bold;
                                    padding: 1px !important;
                                    vertical-align: middle !important;
                                    /*border: 1px solid #cfcfd0;
                                    background-color: #f0f0f1;*/
                                    border: 1px solid #c0c0c0;
                                    background-color: #e3e3e3;
                                }

                                #container-poaf h4.panel-title {
                                    font-size: 16px;
                                }

                                #container-poaf .tr-comp td, .tr-proy td {
                                    font-weight: bold;
                                }

                                #container-poaf .table-condensed td {
                                    padding: 1px !important;
                                }

                                #container-poaf td, #table-container input {
                                    font-size: 13px !important;
                                }

                                #container-poaf .form-control[readonly] {
                                    background-color: #f2f3f4;
                                }
                                
                                .mes
                                {
                                    min-width: 50px !important;
                                }
                            </style>


                            <!-- <div class="text-center">
                                <button class="btn btn-primary btn-save-poaf"><i class="fa fa-check"></i>&nbsp;Guardar</button>
                                <a href="javascript:void(0);" class="btn btn-inverse-alt" onclick="loadPoaf();">
                                    <i class="fa fa-refresh"></i>&nbsp;Actualizar
                                </a>
                                <a class="btn btn-inverse-alt" href="/SLFC/PasoCritico/DescargarPC_PlanOperativoDetallado">
                                    <i class="fa fa-download"></i>&nbsp;Descargar
                                </a>
                            </div> -->
                            
                            <br>
                            <div class="text-center"><h4><b>Avance técnico del proyecto :</b> <span class='avance_proyecto'><?= number_format($proyecto->Avance,2) ?> %</span></h4>   </div>
                            <div class="text-left">Actividad: <span class="actividad_alertas">0</span> alertas pendientes</div>
                            <div class="text-left">Tareas: <span class="tarea_alertas">0</span> alertas pendientes</div>
                            <div id="table-container"></div>
                            <br>
                            <div class="text-center">
                                <button class="btn btn-primary btn-procesar-ejecucion-tareas"><i class="fa fa-check"></i>&nbsp;Procesar ejecución de tareas</button>
                                <a class="btn btn-inverse-alt" href="<?= Yii::$app->getUrlManager()->createUrl('recursos-tarea-ejecutado/descargar-tarea?codigo='.$usuario->username.'') ?>">
                                    <i class="fa fa-download"></i>&nbsp;Descargar
                                </a>
                            </div> 

                            <script>
                                $('.btn-procesar-ejecucion-tareas').click(function (e) {
                                    var $btn = $(this);
                                    bootbox.confirm({
                                        title: '<span style="text-align:center">PNIA - CONFIRMACIÓN</span>',
                                        message: 'Está seguro de procesar la ejecución de tareas?.',
                                        buttons: {
                                            'cancel': {
                                                label: 'CANCELAR'
                                            },
                                            'confirm': {
                                                label: 'PROCESAR EJECUCIÓN'
                                            }
                                        },
                                        callback: function (confirmed) {
                                            if (confirmed) {
                                                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                                                $.ajax({
                                                    url: '<?= Yii::$app->getUrlManager()->createUrl('recursos-tarea-ejecutado/procesar-ejecucion-tareas') ?>',
                                                    type: 'GET',
                                                    async: false,
                                                    data: {codigo:'<?= $usuario->username ?>',_csrf: toke},
                                                    success: function(data){
                                                       
                                                    }
                                                });
                                            } 
                                        }
                                    });
                                });
                                
                               
                                $('body').on('change', '.btn-save-cronograma-tarea-ejecutado', function (e) {
                                    var $this = $(this);
                                    var ID=$this.attr('data-cronograma-tarea-id');
                                    var TareaID=$this.attr('data-tarea-id');
                                    var TareaMetaFisica=$this.attr('data-cronograma-meta-fisica');
                                    var Mes=$this.attr('data-cronograma-mes');
                                    var MetaFisicaEjecutada=$this.val();
                                    var porcentaje=(MetaFisicaEjecutada*100)/TareaMetaFisica;
                                    if (porcentaje>100) {
                                        porcentaje=100;
                                    }
                                    //$('.porcentaje_avance_tarea_'+TareaID+'_'+Mes).html(porcentaje.toFixed(2)+'%');
                                    $.ajax({
                                        type: 'GET',
                                        url: '<?= \Yii::$app->request->BaseUrl ?>/recursos-tarea-ejecutado/grabar-meta-ejecutada-cronograma-tarea',
                                        data: {ID:ID,MetaFisicaEjecutada:MetaFisicaEjecutada},
                                        success: function (data) {
                                            var jx = JSON.parse(data);
                                            if (jx.Success) {
                                                $('.avance_total_tarea_'+TareaID).html(jx.TareaMetaAvance.toFixed(2)+'%');
                                                $('.ponderacion_tar_'+TareaID).html(jx.AvanceTareaActividad.toFixed(2)+'%');
                                                $('.total_actividad_'+jx.ActividadID).html(jx.ActividadAvance.toFixed(2)+'%');
                                                $('.avance_componente_'+jx.ComponenteID).html(jx.ComponenteAvance.toFixed(2)+'%')
                                                $('.avance_proyecto').html(jx.ProyectoAvance.toFixed(2)+'%')
                                                
                                               // loadPoaf();
                                               // _pageLoadingEnd();
                                            } else {
                                            }
                                        },
                                        error: function () {
                                            _pageLoadingEnd();
                                        }
                                    });
                                    return true;
                                });
                                
                                $('body').on('change', '.btn-save-cronograma-actividad-ejecutado', function (e) {
                                    var $this = $(this);
                                    var ID=$this.attr('data-cronograma-actividad-id');
                                    var TareaID=$this.attr('data-actividad-id');
                                    var TareaMetaFisica=$this.attr('data-cronograma-meta-fisica');
                                    var Mes=$this.attr('data-cronograma-mes');
                                    var MetaFisicaEjecutada=$this.val();
                                    var porcentaje=(MetaFisicaEjecutada*100)/TareaMetaFisica;
                                    if (porcentaje>100) {
                                        porcentaje=100;
                                    }
                                    //$('.porcentaje_avance_tarea_'+TareaID+'_'+Mes).html(porcentaje.toFixed(2)+'%');
                                    $.ajax({
                                        type: 'GET',
                                        url: '<?= \Yii::$app->request->BaseUrl ?>/recursos-tarea-ejecutado/grabar-meta-ejecutada-cronograma-actividad',
                                        data: {ID:ID,MetaFisicaEjecutada:MetaFisicaEjecutada},
                                        success: function (data) {
                                            var jx = JSON.parse(data);
                                            if (jx.Success) {
                                                //$('.avance_total_actividad_'+TareaID).html(jx.TareaMetaAvance.toFixed(2)+'%');
                                               // $('.ponderacion_tar_'+TareaID).html(jx.AvanceTareaActividad.toFixed(2)+'%');
                                                //$('.total_actividad_'+jx.ActividadID).html(jx.ActividadAvance.toFixed(2)+'%');
                                                ///$('.avance_componente_'+jx.ComponenteID).html(jx.ComponenteAvance.toFixed(2)+'%')
                                                //$('.avance_proyecto').html(jx.ProyectoAvance.toFixed(2)+'%')
                                                
                                               // loadPoaf();
                                               // _pageLoadingEnd();
                                            } else {
                                            }
                                        },
                                        error: function () {
                                            _pageLoadingEnd();
                                        }
                                    });
                                    return true;
                                });
                                
                                
                                var _proyecto = new Object();

                                $(document).ready(function () {
                                        $('#table-container').html('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>');
                                    getProyecto().done(function(json) {
                                        _proyecto = json;
                                        _drawTable();
                                    });
                                });

                                function getNum(val) {
                                    if (val == '0.00' || val == '') {
                                         return 0;
                                    }
                                    var val2 = val.replace(',','');
                                    return val2;
                                }
    

                                function getProyecto() {
                                    var url = "<?= \Yii::$app->request->BaseUrl ?>/recursos-tarea-ejecutado/tareas-json?codigo=<?= $usuario->username ?>";;
                                    return $.getJSON( url );
                                }

                                function _drawTable() {
                                    var actividad_alertas=1;
                                    var tarea_alertas=1;
                                    var html = '';
                                    var cantmeses = _proyecto.Cronogramas.length;
                                    var situacionTarea=<?= \Yii::$app->user->identity->SituacionTarea ?>;
                                    for (var i_comp = 0; i_comp < _proyecto.Componentes.length; i_comp++) {
                                        var comp = _proyecto.Componentes[i_comp];
                                        html += '<div class="panel panel-gray">';
                                        html += '<div class="panel-heading">'
                                                    + '<h4 class="panel-title">'
                                                        + (comp.Correlativo) + '. Objetivo específico - ' + comp.Nombre
                                                    + '</h4>'
                                        html += '</div>';
                                        html += '<div class="panel-heading">'
                                                    +'<span class="pull-left">'
                                                        +'Peso: '+comp.Peso+' %</span>'
                                                    +'<span class="pull-right">'
                                                +'Avance: <span class="avance_componente_'+comp.ID+'">'+comp.Avance+'%</span></span>';
                                        html += '</div>';
                                        html += '<div class="panel-body">';
                                        html += '<div class="table-responsive">';
                                        html += '<table class="table table-bordered table-condensed table-act">';
                                        html += '<tbody>';
                                        for (var i_act = 0; i_act < comp.Actividades.length; i_act++)
                                        {
                                            var act = comp.Actividades[i_act];
                                            html += '<tr class="tr-header">';
                                                html += '<td rowspan="2">#</td>';
                                                html += '<td rowspan="2">Peso</td>';
                                                html += '<td rowspan="2"></td>';
                                                html += '<td rowspan="2" style="display:none"></td>';
                                                html += '<td rowspan="2">Actividad/Tarea</td>';
                                                html += '<td rowspan="2">Unidad de medida</td>';
                                                html += '<td rowspan="2">Meta física</td>';
                                                html += '<td rowspan="2">Avance %</td>';
                                                html += '<td rowspan="2">Total</td>';
                                            // Cronogramas
                                            
                                            for (var i_crono_act = 0; i_crono_act < cantmeses; i_crono_act++) {
                                                //html += '<td class="mes" colspan="3">Mes ' + act.Cronogramas[i_crono_act].Mes + '</td>';
                                                html += '<td class="mes" colspan="3">Mes ' + act.Cronogramas[i_crono_act].MesDescripcion + '</td>';
                                            }
                                            // Sumatorias
                                            html += '<td rowspan="2">Total</td>';
                                            html +='<tr>';
                                                for (var i_crono_act = 0; i_crono_act < cantmeses; i_crono_act++) {
                                                    html += '<td align="center" class="p" style="vertical-align:middle;min-width:70px !important;background-color: #e3e3e3;border:1px solid #c0c0c0">PRO</td>';
                                                    html += '<td align="center" class="ejecutado" style="vertical-align:middle;min-width:70px !important;border:1px solid #c0c0c0;background-color: #e3e3e3">EJE</td>';
                                                    html += '<td align="center" class="ejecutado" style="vertical-align:middle;min-width:70px !important;border:1px solid #c0c0c0;background-color: #e3e3e3"></td>';
                                                    //html += '<td class="avance" style="vertical-align:middle;min-width:72px !important;border:1px solid #c0c0c0;background-color: #e3e3e3">% Avance</td>';
                                                    
                                                }
                                                html +='</tr>';
                                               
                                                html += '<tr class="tr-act-mf info" data-act-id="' + act.ID + '">';
                                                    html += '<td align="center">' + comp.Correlativo + '.' + act.Correlativo + '</td>'
                                                    html += '<td align="center">'+act.Peso.toFixed(2)+' %</td>';
                                                    
                                                    html += '<td align="center"><span class="total_actividad_'+act.ID+'">'+act.Avance+' %</span></td>';
                                                    //html += '<input class="contador" data-actividad-id="'+act.ID+'" type="hidden" value="'+ctareas+'">';
                                                    html += '<td style="display:none"></td>';
                                                    html += '<td align="center"><b>' + act.Nombre + '</b></td>';
                                                    html += '<td align="center">' + act.UnidadMedida +' </td>';
                                                    html += '<td align="center">'+act.MetaFisica +'</td>';
                                                    html += '<td></td>';
                                                    html += '<td></td>';
                                                for (var i_crono_act = 0; i_crono_act < cantmeses; i_crono_act++) {
                                                        html += '<td>';
                                                        if (act.Cronogramas[i_crono_act].MetaFisica>0) {
                                                        html += '<input disabled  type="text"'
                                                             +   'class="form-control success input-number actividad_'+act.ID+' " value="'
                                                             +   act.Cronogramas[i_crono_act].MetaFisica + '" />';
                                                        }
                                                        html +='</td>';
                                                        
                                                        html +='<td>';
                                                        if (act.Cronogramas[i_crono_act].MetaFisica>0) {
                                                            html +='<input '+deshabilitar+' data-cronograma-mes="'+act.Cronogramas[i_crono_act].Mes+'" data-cronograma-meta-fisica="'+act.Cronogramas[i_crono_act].MetaFisica+'" data-cronograma-actividad-id="'+act.Cronogramas[i_crono_act].ID+'" data-actividad-id="'+act.ID+'" type="text" class="form-control success input-number actividad_ejecutado_'+act.ID+' btn-save-cronograma-actividad-ejecutado" value="'+act.Cronogramas[i_crono_act].MetaFisicaEjecutada+'">';
                                                            
                                                        }
                                                        
                                                        html +='</td>';
                                                        html +='<td>';
                                                        
                                                        if ((act.Cronogramas[i_crono_act].MetaFisicaEjecutada<act.Cronogramas[i_crono_act].MetaFisica) && act.Cronogramas[i_crono_act].MetaFisica!=0 && act.Cronogramas[i_crono_act].MetaFisica!=null && act.Cronogramas[i_crono_act].MesActual==1) {
                                                            html +='<a href="#" data-cronograma-mes="'+act.Cronogramas[i_crono_act].Mes+'" data-cronograma-meta-fisica="'+act.Cronogramas[i_crono_act].MetaFisica+'" data-cronograma-actividad-id="'+act.Cronogramas[i_crono_act].ID+'" data-actividad-id="'+act.ID+'" class="actividad_ejecutado_'+act.ID+'"><span style="color:red" class="fa fa-commenting-o "></span></a>';
                                                            actividad_alertas++;
                                                        }
                                                        
                                                        html +='</td>';
                                                        
                                                    //Max
                                                    //acutotMF += act.Cronogramas[i_crono_act].PoafMetaFisica;
                                                    //End Max
                                                }
                                                html += '<td></td>';
                                                //acutotMF = 0.00;
                                                //
                                                html += '</tr>';
                                            html += '</tr>';
                                          
                                            // Rubros Elegibles
                                            html += '<tr>';
                                                html += '<td colspan="8"> </td>';
                                            html += '</tr>';
                                            var ctareas=1;
                                            for (var i_are = 0; i_are < act.Tareas.length; i_are++) {
                                                
                                                var are = act.Tareas[i_are];
                                                html += '<tr class="tr-sub2-tot success" data-tarea-id="'+i_are+'"  data-meta-fisica="'+are.MetaFisica+'">';
                                                    html += '<td align="center">'+ comp.Correlativo + '.' + act.Correlativo + '.' + are.Correlativo + '</td>';
                                                    html += '<td align="center"></td>';
                                                    
                                                    html += '<td align="center" style="color:#ebf6e1"><span class="ponderacion_tar_'+are.ID+'">'+(are.Peso*are.MetaAvance/100).toFixed(2)+' %</span></td>';
                                                    html += '<input class="contador" data-actividad-id="'+act.ID+'" type="hidden" value="'+(are.Peso*are.MetaAvance/100)+'">';
                                                    html += '<td align="center" style="display:none;min-width:60px !important;color:#ebf6e1"><span class="ponderacion_tareat_'+act.ID+'">'+are.Peso+' %</span></td>'
                                                    html += '<td align="center">'+ are.Descripcion + '</td>';
                                                    html += '<td align="center">'+ are.UnidadMedida+'</td>';
                                                    if (!are.MetaAvance) {
                                                        are.MetaAvance=0.00;
                                                    }
                                                    html += '<td align="center"><input type="text" data-tarea-id="'+are.ID+'" data-meta-fisica="'+are.MetaFisica+'" class="form-control tarea-meta-fisica" readonly value="' + are.MetaFisica+'" ></td>'
                                                    html += '<td align="center"><span class="avance_total_tarea_'+are.ID+'">'+are.MetaAvance+' %</span></td>';
                                                    
                                                    html += '<td></td>'
                                                    for (var i_crono_are = 0; i_crono_are < cantmeses; i_crono_are++) {
                                                        
                                                        var cronogramaTarea=are.Cronogramas[i_crono_are];
                                                        var deshabilitar='disabled';
                                                        if (are.Cronogramas[i_crono_are].SituacionEjecucion==0) {
                                                            deshabilitar='';
                                                        }
                                                        
                                                        html += '<td>';
                                                        if (are.Cronogramas[i_crono_are].MetaFisica>0) {
                                                        html += '<input disabled  type="text"'
                                                             +   'class="form-control success input-number tarea_'+are.ID+' " value="'
                                                             +   are.Cronogramas[i_crono_are].MetaFisica + '" />';
                                                        }
                                                        html +='</td>';
                                                        
                                                        html +='<td>';
                                                        if (are.Cronogramas[i_crono_are].MetaFisica>0) {
                                                            html +='<input '+deshabilitar+' data-cronograma-mes="'+are.Cronogramas[i_crono_are].Mes+'" data-cronograma-meta-fisica="'+are.Cronogramas[i_crono_are].MetaFisica+'" data-cronograma-tarea-id="'+are.Cronogramas[i_crono_are].ID+'" data-tarea-id="'+are.ID+'" type="text" class="form-control success input-number tarea_ejecutado_'+are.ID+' btn-save-cronograma-tarea-ejecutado" value="'+are.Cronogramas[i_crono_are].MetaFisicaEjecutada+'">';
                                                        }
                                                        html +='</td>';
                                                        html +='<td>';
                                                        
                                                        //if ((are.Cronogramas[i_crono_are].MetaFisicaEjecutada<are.Cronogramas[i_crono_are].MetaFisica) && are.Cronogramas[i_crono_are].MetaFisica!=0 && are.Cronogramas[i_crono_are].MetaFisica!=null && are.Cronogramas[i_crono_are].MesActual==1) {
                                                        if (are.Cronogramas[i_crono_are].MetaFisica!=0 && are.Cronogramas[i_crono_are].MetaFisica!=null && are.Cronogramas[i_crono_are].MesActual==1) {
                                                            html +='<a href="#" data-cronograma-mes="'+are.Cronogramas[i_crono_are].Mes+'" data-cronograma-meta-fisica-ejecutada="'+are.Cronogramas[i_crono_are].MetaFisicaEjecutada+'" data-cronograma-meta-fisica="'+are.Cronogramas[i_crono_are].MetaFisica+'" data-cronograma-tarea-id="'+are.Cronogramas[i_crono_are].ID+'" data-tarea-id="'+are.ID+'" class="tarea_ejecutado_'+act.ID+' justificacion"><span  style="color:red" class="fa fa-commenting-o" ></span></a>';
                                                            tarea_alertas++;
                                                        }
                                                        html +='</td>';
                                                        /*
                                                        html +='<td>';
                                                        if (are.Cronogramas[i_crono_are].MetaFisica>0) {
                                                            html +='<span class="input-number-porcentaje porcentaje_avance_tarea_'+are.ID+'_'+are.Cronogramas[i_crono_are].Mes+'">'+are.Cronogramas[i_crono_are].MetaAvance+' %</span>';
                                                        }
                                                        html +='</td>';
                                                        */
                                                    }
                                                    // Sumatorias
                                                    html += '<td></td>';
                                                    
                                                //
                                                html += '</tr>';
                                                // Subcategorías
                                                
                                                ctareas++;
                                                
                                                //$('.ponderacion_tarea_'+act.ID).html((100/ctareas).toFixed(2)+' %');
                                                
                                            }
                                            
                                            
                                        }
                                        // Totales por componente
                                        
                                        html += '</tbody>';
                                        html += '</table>';
                                        html += '</div>';
                                        html += '</div>';
                                        html += '</div>';
                                    }
                                    $('#table-container').html(html);
                                    
                                    $('.actividad_alertas').html(actividad_alertas);
                                    $('.tarea_alertas').html(tarea_alertas);
                                    
                                    $('.input-number-porcentaje,.input-number-recurso-porcentaje').inputmask();
                                }
                            </script>
                        </div>

                    </div>

                </div>
            </div>


            <div class="modal fade" id="modal-ctrl-tarea" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Tarea</h4>
                        </div>
                        <div class="modal-body-main">
                        </div>
                    </div>
                </div>
            </div>


            <div id="modal-add-pc" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body-main">
                        </div>
                    </div>
                </div>
            </div>

            <div id="modal-create-indpc" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Indicador de paso crítico</h4>
                        </div>
                        <div class="modal-body-main">
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="modal fade" id="modal-control-justificacion" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Justificación</h4>
                        </div>
                        <div class="modal-body-main">
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script>
    $('body').on('click', '.justificacion', function (e) {
        e.preventDefault();
        var cronogramatareaid = $(this).attr('data-cronograma-tarea-id');
        var metafisicaejecutada = $(this).attr('data-cronograma-meta-fisica-ejecutada');
        var metafisica = $(this).attr('data-cronograma-meta-fisica');
        $('#modal-control-justificacion .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/recursos-tarea-ejecutado/justificacion-tarea?cronogramatareaid='+cronogramatareaid+'');
            $('#modal-control-justificacion').modal('show');
            /*
        if (metafisicaejecutada<metafisica) {
            $('#modal-control-justificacion .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/recursos-tarea-ejecutado/justificacion-reprogramacion-tarea?cronogramatareaid='+cronogramatareaid+'');
            $('#modal-control-justificacion').modal('show');
        }
        else{
            $('#modal-control-justificacion .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/recursos-tarea-ejecutado/justificacion-tarea?cronogramatareaid='+cronogramatareaid+'');
            $('#modal-control-justificacion').modal('show');
        }
        */
    });
</script>
<!--
<div id="modal-loading" class="modal fade modal-loading in" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-hidden="false" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content-no-shadow modal-content">
            <div class="text-center"><i class="fa fa-spinner fa-pulse fa-fw" style="color:#16a085;font-size: 100px;"></i></div>
        </div>
    </div>
</div>
-->