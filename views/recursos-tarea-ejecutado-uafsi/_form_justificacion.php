<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'recursos-tarea-ejecutado/justificacion-tarea?CronogramaTareaID='.$CronogramaTareaID,
        'options'           => [
	    'enctype'       =>'multipart/form-data',
            'autocomplete'  => 'off',
            'id'            => 'frmJustificacion'
        ]
    ]
); ?>
    <div class="modal-body">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label">Descripción<span class="f_req">*</span></label>
                <div class="col-sm-9">
		    <textarea class="form-control" disabled name="JustificacionTarea[Descripcion]" required><?= $model->Descripcion ?></textarea>
                </div>
            </div>
	    
	    <div class="form-group">
		<label class="col-sm-3 control-label">Evidencia:<span class="f_req">*</span></label>
		<div class="col-sm-9">
		    <div class="fileinput fileinput-new" data-provides="fileinput">
			<div class="input-group">
			    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Eliminar</a>
			</div>
		    </div>
		    <?php if(!empty($model)): ?>
                        <?php if($model->Evidencia): ?>
                            <a href="<?= \Yii::$app->request->BaseUrl ?>/justificaciontarea/<?php echo $model->Evidencia ?>" target="_blank"><i class="fa fa-download"></i>&nbsp;Descargar adjunto</a>
                        <?php endif; ?>
                    <?php endif; ?>
		</div>
	    </div>
	    
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
    </div>
<?php ActiveForm::end(); ?>


<script>
    var $form = $('#frmJustificacion');
    var formParsleyJustificacion = $form.parsley(defaultParsleyForm());
</script>