<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<div id="page-content" style="min-height: 1342px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Reprogramación de tareas</h1>
        </div>
        <div class="container">
            <style>
                .div-label-left b {
                    margin-bottom: 10px;
                    font-size: 18px;
                }
                .count-message {
                    color: #2460AA;
                    margin-top: 5px;
                }
            </style>
            <style>
                .table > tbody > .tr-header > td {
                    text-align: center;
                    font-size: 16px;
                    font-weight: bold;
                    padding: 1px !important;
                    vertical-align: middle !important;
                    /*border: 1px solid #cfcfd0;
                    background-color: #f0f0f1;*/
                    border: 1px solid #c0c0c0;
                    background-color: #e3e3e3;
                    min-width: 75px !important;
                }
                
            </style>
            <?php // $form = ActiveForm::begin(); ?>
            <?php $form = ActiveForm::begin(
                [
                    'action'            => '',
                    'options'           => [
                        'enctype'       =>'multipart/form-data',
                        'id'            => 'frmReprogramacion'
                    ]
                ]
            ); ?>
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <div id="container-resp"></div>
                        <div class="form-horizontal">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <b>Objetivo: </b>  <?= $Componente->Nombre ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <b>Actividad: </b>  <?= $Actividad->Nombre ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <b>Tarea: </b>  <?= $Tarea->Descripcion ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>Justificación:</label>
                                    <textarea class="form-control" name="ReProgramacionTarea[Justificacion]" required></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label>Evidencia:</label>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="input-group">
                                            <div class="form-control uneditable-input" data-trigger="fileinput">
                                                <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                <span class="fileinput-filename">
                                                Seleccione archivo...
                                                </span>
                                            </div>
                                            <span class="input-group-addon btn btn-default btn-file">
                                                <span class="fileinput-new">Seleccionar</span>
                                                <span class="fileinput-exists">Cambiar</span>
                                                <input type="file" onchange="return Imagen(this);" id="file-cv-administrativo" name="ReProgramacionTarea[Archivo]" required>
                                            </span>
                                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Eliminar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="table-responsive col-sm-12">
                                    <table id="repro" class="table table-bordered table-condensed ">
                                        <tbody>
                                            <tr class="tr-header">
                                                <td rowspan="2">Mes</td>
                                                <td>Metas - Programación Original</td>
                                                <td>Metas - Reprogramación</td>
                                            </tr>
                                            <tr class="tr-header">
                                                <td>Física</td>
                                                <td>Física</td>
                                            </tr>
                                            <?php foreach($CronogramaTareas as $CronogramaTarea){?>
                                            <tr>
                                                <td><?= Yii::$app->tools->DescripcionMes($CronogramaTarea->Mes) ?></td>
                                                
                                                <?php if($CronogramaTarea->MetaFisica!=0){ ?>
                                                <td><?= $CronogramaTarea->MetaFisica ?></td>
                                                <?php }else{ ?>
                                                <td>0.00</td>
                                                <?php }?>
                                                <input type="hidden" value="<?= $CronogramaTarea->Mes ?>" class="form-control" name="ReProgramacionTarea[Meses][]">
                                                <td><input type="text" value="0.00" class="form-control" name="ReProgramacionTarea[MetasFisicas][]"></td>
                                               
                                            </tr>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-primary" id="btn-guardar"><i class="fa fa-floppy-o"></i>&nbsp;Guardar</button>
                                    <!--<button type="submit" class="btn btn-primary btn-finalizar" ><i class="fa fa-check"></i>&nbsp;Finalizar</button>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<script>
    var $form = $('#frmReprogramacion');
    var formReprogramacion = $form.parsley(defaultParsleyForm());
    $(document).on('click','.btn-finalizar',function(){
        var a = $(this).attr('data-id');
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        bootBoxConfirm('Esta seguro que desea finalizar el registro',function(){
            return true;
        });
        return false;
    });
</script>