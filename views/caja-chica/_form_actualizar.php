<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'requerimiento-caja-chica/actualizar?ID='.$ID,
        'options'           => [
            'enctype'       =>'multipart/form-data',
            'id'            => 'frmRequerimientosCajaChica',
            
        ]
    ]
); ?>

    <div class="modal-body">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label">Codigo:<span class="f_req">*</span></label>
                <div class="col-sm-3">
                  <!--  <input type="text" class="form-control" name="CajaChica[Servicios]" required>-->
                    <label class="col-sm-3 control-label"><?= $CodigoProyecto ?></label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Tipo:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <select class="form-control" name="CajaChica[Tipo]" required>
                        <option value>Seleccionar</option>
                        <option value=1 <?= ($CajaChica->Tipo==1)?'selected':'';?>>Apertura</option>
                        <option value=2 <?= ($CajaChica->Tipo==2)?'selected':'';?>>Reembolso</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Responsable:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="CajaChica[Responsable]" value="<?= $CajaChica->Responsable  ?>" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Banco:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="CajaChica[Banco]" value="<?= $CajaChica->Banco  ?>" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">CTA:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="CajaChica[CTA]" value="<?= $CajaChica->CTA  ?>" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">CCI:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="CajaChica[CCI]" value="<?= $CajaChica->CCI  ?>" required>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Descripción:<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="CajaChica[Descripcion]" value="<?= $CajaChica->Descripcion  ?>" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Gastos bienes:<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="CajaChica[Bienes] " value="<?= $CajaChica->Bienes  ?>" required> 
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Gastos servicios:<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="CajaChica[Servicios]" value="<?= $CajaChica->Servicios  ?>"required>
                </div>
            </div>
           
            <div class="form-group">
                <label class="col-sm-3 control-label">N° resolución directorial:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="CajaChica[NResolucionDirectorial]" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Adjuntar resolución directorial:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input class="form-control" onchange="return Imagen(this);" type="file" name="CajaChica[archivo]" id="requerimiento-archivo">
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>


<style type="text/css">
    .none{
        display: none;
    }
    .block{
        display: block;
    }

    .ui-draggable, .ui-droppable {
      background-position: top;
    }
</style>



<script>
    var $form = $('#frmRequerimientosCajaChica');
    var formParsleyRequerimientosEncargos = $form.parsley(defaultParsleyForm());

</script>

