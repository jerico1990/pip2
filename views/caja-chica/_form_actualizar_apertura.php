<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'requerimiento-caja-chica/actualizar?ID='.$ID,
        'options'           => [
            'enctype'       =>'multipart/form-data',
            'id'            => 'frmRequerimientosCajaChica',
            
        ]
    ]
); ?>

    <div class="modal-body">
        <div class="form-horizontal">
            
          
            <div class="form-group">
               
                <label class="col-sm-3 control-label">Codigo:<span class="f_req">*</span></label>
                <div class="col-sm-3">
                  <!--  <input type="text" class="form-control" name="RequerimientoCajaChica[Servicios]" required>-->
                    <label class="col-sm-3 control-label"><?= $CodigoProyecto ?></label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Responsable:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="CajaChica[Responsable]" value="<?= $requerimiento->Responsable  ?>" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Banco:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="CajaChica[Banco]" value="<?= $requerimiento->Banco  ?>" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">CTA:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="CajaChica[CTA]" value="<?= $requerimiento->CTA  ?>" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">CCI:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="CajaChica[CCI]" value="<?= $requerimiento->CCI  ?>" required>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Descripción:<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="RequerimientoCajaChica[Descripcion]" value="<?= $requerimiento->Descripcion  ?>" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Gastos Bienes:<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="RequerimientoCajaChica[GastoCompraBienes] " value="<?= $requerimiento->GastoCompraBienes  ?>" required> 
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Gastos Servicios:<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="RequerimientoCajaChica[GastoCompraServicios]" value="<?= $requerimiento->GastoCompraServicios  ?>"required>
                </div>
            </div>
           

            <div class="form-group">
                <label class="col-sm-3 control-label">Resolución Directorial:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input class="form-control" type="file" name="RequerimientoCajaChica[archivo]" id="requerimiento-archivo">
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>


<style type="text/css">
    .none{
        display: none;
    }
    .block{
        display: block;
    }

    .ui-draggable, .ui-droppable {
      background-position: top;
    }
</style>



<script>
    var $form = $('#frmRequerimientosCajaChica');
    var formParsleyRequerimientosEncargos = $form.parsley(defaultParsleyForm());

</script>

