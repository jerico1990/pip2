<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => '../caja-chica/crear?CodigoProyecto='.$CodigoProyecto.'&RequerimientoID='.$RequerimientoID,
        'options'           => [
            'enctype'       =>'multipart/form-data',
            'id'            => 'frmCaja',
            
        ]
    ]
); ?>
    <input type="hidden" name="CajaChica[CodigoProyecto]" value="<?= $CodigoProyecto ?>">
    <div class="modal-body">
        <div class="form-horizontal">
            
            <div id="container-resp"></div>
            <div class="form-group">
               <input type="hidden" name="CajaChica[RequerimientoID]" value="<?= $RequerimientoID ?>">
                <label class="col-sm-3 control-label">Codigo de proyecto:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                  <!--  <input type="text" class="form-control" name="CajaChica[Servicios]" required>-->
                    <label class="col-sm-3 control-label"><?= $CodigoProyecto ?></label>
                </div>
            </div>
            <div class="form-group">
                
                <label class="col-sm-3 control-label">Tipo:<span class="f_req">*</span></label>
                
                <div class="col-sm-9">
                    <?php if(!$CajaChicaActual->Responsable ){?>
                    <input type="hidden" name="CajaChica[Tipo]" value="1">
                    <select class="form-control"  disabled>
                        <option value>Seleccionar</option>
                        <option value=1 selected>Apertura</option>
                        <option value=2 >Reembolso</option>
                    </select>
                    <?php }else {?>
                    <input type="hidden" name="CajaChica[Tipo]" value="2">
                    <select class="form-control"  disabled>
                        <option value>Seleccionar</option>
                        <option value=1 >Apertura</option>
                        <option value=2 selected>Reembolso</option>
                    </select>
                    <?php } ?>
                    
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Responsable:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="CajaChica[Responsable]" value="<?= $CajaChicaActual->Responsable ?>" required>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">DNI:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="CajaChica[Dni]" value="<?= $CajaChicaActual->Dni ?>" required>
                </div>
            </div>
            
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Banco:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="CajaChica[Banco]" value="<?= $CajaChicaActual->Banco ?>" required>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">CTA:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="CajaChica[CTA]" value="<?= $CajaChicaActual->CTA ?>" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">CCI:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="CajaChica[CCI]" value="<?= $CajaChicaActual->CCI ?>" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Descripción:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="CajaChica[Descripcion]" required></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Bienes:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text"  class="form-control" maxlength="4" onKeyPress="return soloNumeros(event);" name="CajaChica[Bienes]" id="caja-chica-bienes" value="<?= $CajaChica->Bienes ?>" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Servicios:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" maxlength="4" onKeyPress="return soloNumeros(event);" name="CajaChica[Servicios]" id="caja-chica-servicios" value="<?= $CajaChica->Servicios ?>" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">N° resolución directorial:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="CajaChica[NResolucionDirectorial]" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Resolución directorial:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="input-group">
                            <div class="form-control uneditable-input" data-trigger="fileinput">
                                <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                <span class="fileinput-filename">
                                Seleccione archivo...
                                </span>
                            </div>
                            <span class="input-group-addon btn btn-default btn-file">
                                <span class="fileinput-new">Seleccionar</span>
                                <span class="fileinput-exists">Cambiar</span>
                                <input type="file" onchange="return Imagen(this);" id="file-cv-administrativo" name="CajaChica[archivo]" required>
                            </span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Eliminar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary" id="btn-guardar-caja"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>


<style type="text/css">
    .none{
        display: none;
    }
    .block{
        display: block;
    }

    .ui-draggable, .ui-droppable {
      background-position: top;
    }
</style>



<script>
    var $form = $('#frmCaja');
    var formParsleyCaja = $form.parsley(defaultParsleyForm());
    
</script>

