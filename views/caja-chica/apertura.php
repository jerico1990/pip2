
<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay_progress.min.js"></script>
<script src=" //cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>


<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<div id="page-content" style="min-height: 1342px;">
    
    <div id="wrap">
        <div id="page-heading">
            <h1>Apertura de Caja Chica</h1>
        </div>
        
           
        <div class="container">
             <div class="form-group">
                <button class="btn btn-crear-apertura-caja-chica" data-codigo-proyecto="<?= $CodigoProyecto ?>">Generar apertura de  Caja Chica</button>
            </div>
        
            <div class="table-responsive">
                <table id="cajachica" class="display table table-bordered table-striped table-condensed table-hover" cellspacing="0" >
                    <thead>
                        <tr>
                            <th style="display:none;">ID</th>
                            <th>Codigo</th>
                            <th>FechaRegistro</th>
                            <th>Descripción</th>
                            <th>Gasto Bienes</th>
                            <th>Gasto Servicio</th>
                            <th>Total</th>
                            <th>Situación</th>
                            <th>Descargar</th>
                            <!--
                            <th>Acciones</th>
                            -->
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
          <a class="btn" href="<?= Yii::$app->getUrlManager()->createUrl('requerimiento-caja-chica/excel?CodigoProyecto='.$CodigoProyecto.'') ?>">Descargar Excel</a>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-ctrl-apertura-caja-chica" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Apertura Caja Chica</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-ctrl-poa" tabindex="-1" role="dialog" >
        <div class="modal-dialog modal-lg" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">POA</h4>
                </div>
                <div class="modal-body-main">
                </div>
            </div>
        </div>
    </div>
<?php 
$requerimiento = Yii::$app->getUrlManager()->createUrl('requerimiento-caja-chica/enviar');
?>
    
<script>

    function js_buscar()
    {
       
    }
    var tblresultjs;
    var configDTjs={
        "order": [[ 1, "desc" ]],
            "language": {
                "sProcessing":    "Procesando...",
                "sLengthMenu":    "Mostrar _MENU_ registros",
                "sZeroRecords":   "No se encontraron resultados",
                "sEmptyTable":    "Ningún dato disponible en esta lista",
                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":   "",
                "sSearch":        "Buscar:",
                "sUrl":           "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":    "Último",
                    "sNext":    "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
    };
    
    $(document).ready(function() {
        var CodigoProyecto="<?= $CodigoProyecto ?>";
        $.ajax({
            url:'../caja-chica/lista-apertura?CodigoProyecto='+CodigoProyecto,
            async:false,
            data:{},
            beforeSend:function()
            {
                $('.panel-default').LoadingOverlay("show");
            },
            success:function(result)
            {
                    //$tblresultjs.destroy();//
                    $("#cajachica tbody").html(result);
                    $('#cajachica').DataTable(configDTjs);
                    //tblresultjs=$('#example').DataTable(configDTjs);
                    $('.panel-default').LoadingOverlay("hide",true);
            },
            error:function(){
                $('.panel-default').LoadingOverlay("hide",true);
                alert('Error al realizar el proceso de busqueda.');
            }
        });
    } );
    
    $('body').on('click', '.btn-crear-apertura-caja-chica', function (e) {
        e.preventDefault();
        var CodigoProyecto = $(this).attr('data-codigo-proyecto');
        $('#modal-ctrl-apertura-caja-chica .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/caja-chica/crear-apertura?CodigoProyecto='+CodigoProyecto);
        $('#modal-ctrl-apertura-caja-chica').modal('show');
    });
    
    $('body').on('click', '.btn-edit-apertura-caja-chica', function (e) {
        e.preventDefault();
        var ID = $(this).attr('data-id');
        $('#modal-ctrl-apertura-caja-chica .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/caja-chica/actualizar-apertura?ID='+ID);
        $('#modal-ctrl-apertura-caja-chica').modal('show');
    });


    $('body').on('click', '.btn-remove', function (e) {
        e.preventDefault();
        var $this = $(this);
        bootboxConfirmEliminar($this,
           'Está seguro de eliminar el requerimiento de caja Chica?'+$(this).attr('data-id'),
            function () {
                var id = $this.attr('data-id');
                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                $.get('<?= \Yii::$app->request->BaseUrl ?>/requerimiento-caja-chica/eliminar', { ID: id ,_csrf: toke }, function (data) {
                    var jx = JSON.parse(data);
                    if (jx.Success == true ) {
                        location.reload();
                        
                    }
                });
            });
    });

    $(document).on('click','.verifica-aprobar',function(){
        var a = $(this).attr('data-id');
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        bootBoxConfirm('Esta seguro que desea enviar en Proeso el requerimiento',function(){
            $.ajax({
                url: '<?= $requerimiento?>',
                type: 'POST',
                async: false,
                data: {_csrf: toke,ID:a},
                success: function (data) {
                    location.reload();
                },
                error: function () {
                    _pageLoadingEnd();
                }
            });
        });
    });

</script>