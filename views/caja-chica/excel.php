<?php

/** Error reporting */
error_reporting(E_ALL);

/** Include path **/
ini_set('include_path', ini_get('include_path').';../web/PHPExcel/Classes/');
//require(__DIR__ . '/../../web/PHPExcel/Classes/');
/** PHPExcel */
include 'PHPExcel.php';

/** PHPExcel_Writer_Excel2007 */
include 'PHPExcel/Writer/Excel2007.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
$sheet = $objPHPExcel->getActiveSheet();

$sheet->setTitle('Requerimientos Caja Chica');


$objPHPExcel->setActiveSheetIndex(0);



$sheet->SetCellValue('A3', '#');
$sheet->SetCellValue('B3', 'Codigo');
/*$sheet->SetCellValue('C3', 'Fecha Registro');
$sheet->SetCellValue('D3', 'Descripcion');
$sheet->SetCellValue('E3', 'Gasto Compra Bienes');
$sheet->SetCellValue('F3', 'Gasto Compra Servicios');
$sheet->SetCellValue('C3', 'Requerimiento N°');
$sheet->SetCellValue('D3', 'Tipo de Requerimiento');
$sheet->SetCellValue('E3', 'Referencia');
$sheet->SetCellValue('F3', 'Situación');
$sheet->SetCellValue('G3', 'Fecha de Solicitud');
$sheet->SetCellValue('H3', 'Objetivo');
$sheet->SetCellValue('I3', 'Actividad');
$sheet->SetCellValue('J3', 'Recurso');
$sheet->SetCellValue('K3', 'Codigo POA');
$sheet->SetCellValue('L3', 'Cantidad');
$sheet->SetCellValue('M3', 'Precio Unitario');
$sheet->SetCellValue('N3', 'PAC');*/

$i=4;
foreach($resultados as $resultado)
{
    $sheet->SetCellValue('A'.$i, ($i-3));

    $sheet->SetCellValue('B'.$i, date('d/m/Y',strtotime($resultado["FechaRegistro"])));

    /*
    $sheet->SetCellValue('B'.$i, $resultado["CodigoProyecto"]);
    $sheet->SetCellValue('C'.$i, date('d/m/Y',strtotime($resultado["FechaRegistro"])));
    $sheet->SetCellValue('D'.$i, $resultado["Descripcion"]);
    $sheet->SetCellValue('E'.$i, $resultado["GastoCompraBienes"]);
    $sheet->SetCellValue('F'.$i, $resultado["GastoCompraServicios"]);
*/
    /*
 
    $sheet->SetCellValue('C'.$i, str_pad($resultado["RCorrelativo"], 3, "0", STR_PAD_LEFT));
    $sheet->SetCellValue('D'.$i, $requerimiento->getTipoServicio($resultado["TipoRequerimiento"]));
    $sheet->SetCellValue('E'.$i, $resultado["Referencia"]);
    $sheet->SetCellValue('F'.$i, $requerimiento->getSituacion($resultado["Situacion"]));
    $sheet->SetCellValue('G'.$i, date('d/m/Y',strtotime($resultado["FechaSolicitud"])));
    $sheet->SetCellValue('H'.$i, $resultado["CComponente"].".".$resultado["NComponente"]);
    $sheet->SetCellValue('I'.$i, $resultado["CActividad"].".".$resultado["NActividad"]);
    $sheet->SetCellValue('J'.$i, $resultado["CAreSubCategoria"].".".$resultado["NAreSubCategoria"]);
    $sheet->SetCellValue('K'.$i, $resultado["CComponente"].".".$resultado["CActividad"].".".$resultado["CAreSubCategoria"]);
    $sheet->SetCellValue('L'.$i, $resultado["CDetalleRequerimiento"]);
    $sheet->SetCellValue('M'.$i, $resultado["PrecioUnitario"]);
    $sheet->SetCellValue('N'.$i, $resultado["Pac"]);*
    */
    $i++;
}
    




// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Requerimientos.xls"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
