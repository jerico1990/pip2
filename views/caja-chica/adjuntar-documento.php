<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => '../caja-chica/adjuntar-documento?ID='.$ID,
        'options'           => [
            'enctype'       =>'multipart/form-data',
            'id'            => 'frmCajaChica',
            
        ]
    ]
); ?>

    <div class="modal-body">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label">Documento:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="input-group">
                            <div class="form-control uneditable-input" data-trigger="fileinput">
                                <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                <span class="fileinput-filename">
                                Seleccione archivo...
                                </span>
                            </div>
                            <span class="input-group-addon btn btn-default btn-file">
                                <span class="fileinput-new">Seleccionar</span>
                                <span class="fileinput-exists">Cambiar</span>
                                <input type="file" onchange="return Imagen(this);" id="file-cv-administrativo" name="DetalleCajaChica[archivo]">
                            </span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Eliminar</a>
                        </div>
                    </div>
                    
                    <input class="form-control" type="hidden" name="DetalleCajaChica[Correlativo]" value="<?= $cajaChica->Correlativo ?>">
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>
<script>
    var $form = $('#frmCajaChica');
    var formParsleyCajaChica = $form.parsley(defaultParsleyForm());
</script>