<div id="page-content" style="min-height: 885px;">
    <div id="wrap">
        <div id="page-heading">
            <h1></h1>
        </div>
        <div class="container">
                    
            <style>
                #page-heading {
                    display: none;
                }

                .tab-content {
                    background-color: #fff;
                }

                .panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 1px solid #a8a8ab !important;
                }

                .panel-gray .panel-body {
                    border-top: none !important;
                }

                .panel-heading {
                    height: auto !important;
                }
            </style>

            <div class="tab-container">
                <ul class="nav nav-tabs">
                    <li id="li-poaf" class="active"><a href="#tab-poaf" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Paso Crítico <?= $pasocritico->Correlativo ?></a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-poaf">
                        <div id="container-poaf">
                            <style>
                                #container-poaf .input-number {
                                    text-align: right;
                                    min-width: 72px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }

                                #container-poaf .input-number-sm {
                                    text-align: right;
                                    min-width: 52px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }

                                #container-poaf .input-number-md {
                                    text-align: right;
                                    min-width: 82px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }

                                #container-poaf table-act > tbody > tr > td:nth-child(1) {
                                    text-align: center;
                                }

                                #container-poaf .table-act > tbody > tr > td:nth-child(2) {
                                    min-width: 320px !important;
                                }

                                #container-poaf .table-act > tbody > tr > td:nth-child(3) {
                                    min-width: 100px !important;
                                }

                                #container-poaf .table > tbody > .tr-header > td {
                                    text-align: center;
                                    font-size: 12px;
                                    font-weight: bold;
                                    padding: 1px !important;
                                    vertical-align: middle !important;
                                    border: 1px solid #c0c0c0;
                                    background-color: #e3e3e3;
                                    min-width: 75px !important;
                                }

                                #container-poaf h4.panel-title {
                                    font-size: 16px;
                                }

                                #container-poaf .tr-comp td, .tr-proy td {
                                    font-weight: bold;
                                }

                                #container-poaf .table-condensed td {
                                    padding: 1px !important;
                                }

                                #container-poaf td, #table-container input {
                                    font-size: 13px !important;
                                }

                                #container-poaf .form-control[readonly] {
                                    background-color: #f2f3f4;
                                }
                            </style>
                            <br>
                            <div id="table-container">
                            </div>
                            <br>
                                <a target="_blank" class="btn btn-inverse-alt" href="<?= Yii::$app->getUrlManager()->createUrl('paso-critico/descargar-poa?PasoCriticoID='.$pasocritico->ID.'') ?>">
                                    <i class="fa fa-download"></i>&nbsp;Descargar Cronograma
                                </a>
                                
                                <!-- <a target="_blank" class="btn btn-inverse-alt" href="<?= Yii::$app->getUrlManager()->createUrl('paso-critico/descargar-poa-financiero?PasoCriticoID='.$pasocritico->ID.'') ?>">
                                    <i class="fa fa-download"></i>&nbsp;Descargar Cronograma Financiero
                                </a> -->
                            <div class="text-center">
                            </div>
                            <script>
                                var _proyecto = new Object();

                                $(document).ready(function () {
                                        $('#table-container').html('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>');
                                    getProyecto().done(function(json) {
                                        _proyecto = json;
                                        _drawTable();
                                    });
                                });
                                
                                $('.btn-descargar-poa').click(function (e) {
                                    var $btn = $(this);
                                    var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                                    $.ajax({
                                        url: '<?= Yii::$app->getUrlManager()->createUrl('paso-critico/descargar-poa') ?>',
                                        type: 'GET',
                                        async: false,
                                        data: {codigo:'<?= $informacionGeneral->Codigo ?>',_csrf: toke},
                                        success: function(data){
                                           
                                        }
                                    });
                                });
                                
                                
                                function getProyecto() {
                                    var url = "<?= \Yii::$app->request->BaseUrl ?>/paso-critico/recursos-json?codigo=<?= $informacionGeneral->Codigo ?>&mesInicio=<?= $pasocritico->MesInicio ?>&mesFin=<?= $pasocritico->MesFin ?>";
                                    return $.getJSON( url );
                                }

                                function _drawTable() {
                                   
                                    var situacionRecurso=<?= \Yii::$app->user->identity->SituacionRecurso ?>;
                                    var html = '';
                                    var cantmeses = _proyecto.Cronogramas.length;
                                    for (var i_comp = 0; i_comp < _proyecto.Componentes.length; i_comp++) {
                                        var comp = _proyecto.Componentes[i_comp];
                                        
                                        html += '<div class="panel panel-gray">';
                                        html += '<div class="panel-heading">';
                                        html += '<h4 class="panel-title">';
                                        html += (comp.Correlativo) + '. Objetivo específico - ' + comp.Nombre
                                        html += '</h4>';
                                        html += '</div>';
                                        var Total=comp.TotalObjetivo;
                                        html += '<div class="panel-heading ">';
                                        html += '<h4 class="panel-title">';
                                        html += 'Total Objetivo : <span class="input-number" style="font-size:15px !important">'+ Total+'</span>';
                                        html += '</h4>';
                                        html += '</div>';
                                        
                                        html += '<div class="panel-body">';
                                        html += '<div class="table-responsive">';
                                        html += '<table class="table table-bordered table-condensed table-act">';
                                        html += '<tbody>';
                                        for (var i_act = 0; i_act < comp.Actividades.length; i_act++)
                                        {
                                            var acutotMF = 0.00;
                                            var acutotMFs = 0.00;
                                            var subacutotCat = 0.00;
                                            var subacutotCats = 0.00;
                                            var acuTotal = 0.00;
                                            var act = comp.Actividades[i_act];
                                            
                                            var TotalActividad=0;
                                            for (var i_are = 0; i_are < act.ActRubroElegibles.length; i_are++) {
                                                var are = act.ActRubroElegibles[i_are];
                                                if (are.AreSubCategorias.length) {
                                                    for (var i_arescat = 0; i_arescat < are.AreSubCategorias.length; i_arescat++) {
                                                        var arescat = are.AreSubCategorias[i_arescat];
                                                        if (arescat) {
                                                            TotalActividad=TotalActividad+(arescat.CostoUnitario*arescat.MetaFisica);
                                                        }
                                                    }
                                                }
                                            }
                                                
                                            html += '<tr class="tr-header">';
                                            html += '<td>#</td><td>Actividad</td><td>Unidad de medida</td><td>Costo unitario</td><td>Meta física</td><td>Total</td>';
                                           
                                            for (var i_crono_act = 0; i_crono_act < cantmeses; i_crono_act++) {
                                                html += '<td> Mes ' + act.Cronogramas[i_crono_act].MesDescripcion + '</td>';
                                            }
                                            html += '</tr>';
                                            html += '<tr class="tr-act-mf info" data-act-id="' + act.ID + '">';
                                            html += '<td>' + (comp.Correlativo) + '.' + (act.Correlativo) + '</td><td><b>' + act.Nombre + '</b></td><td>' + act.UnidadMedida+'</td><td class="input-number">'+(TotalActividad/act.MetaFisica)+'</td>'
                                                    + '<td align="center">' + act.MetaFisica + '</td><td class="input-number">'+TotalActividad+'</td>';
                                            for (var i_crono_act = 0; i_crono_act < cantmeses; i_crono_act++) {
                                                html += '<td></td>';
                                                acutotMF += act.Cronogramas[i_crono_act].PoafMetaFisica;
                                            }
                                            html += '</tr>';
                                            html += '</tr>';
                                            
                                            // Rubros Elegibles
                                            html += '<tr>';
                                            html += '<td colspan="6"> </td>';
                                            html += '</tr>';
                                            for (var i_are = 0; i_are < act.ActRubroElegibles.length; i_are++) {
                                                
                                                var are = act.ActRubroElegibles[i_are];
                                                var TotalRubroElegible=0;
                                                if (are.AreSubCategorias.length) {
                                                    for (var i_arescat = 0; i_arescat < are.AreSubCategorias.length; i_arescat++) {
                                                        var arescat = are.AreSubCategorias[i_arescat];
                                                        if (arescat) {
                                                            TotalRubroElegible=TotalRubroElegible+(arescat.CostoUnitario*arescat.MetaFisica);
                                                        }
                                                    }
                                                }
                                                html += '<tr class="tr-sub2-tot success">';
                                                html += '<td></td><td>' + are.RubroElegible.Nombre + '</td><td>' 
                                                        + '</td><td>'
                                                        + '</td><td>'
                                                        + '</td><td class="input-number">'+TotalRubroElegible+'"</td>';
                                                for (var i_crono_are = 0; i_crono_are < cantmeses; i_crono_are++) {
                                                    html += '<td></td>';
                                                }
                                                // Sumatorias
                                                html += '</tr>';
                                                // Subcategorías
                                                if (are.AreSubCategorias.length) {
                                                    for (var i_arescat = 0; i_arescat < are.AreSubCategorias.length; i_arescat++) {
                                                        var arescat = are.AreSubCategorias[i_arescat];
                                                        if (arescat) {
                                                            html += '<tr class="tr-subact warning"><td align="center">';
                                                            html += arescat.Matriz;
                                                            html += '</td><td class="recurso_alerta_'+arescat.ID+'">' + arescat.Nombre + '</td><td>' + arescat.UnidadMedida
                                                                    + '</td><td class="input-number">' + arescat.CostoUnitario
                                                                    + '</td><td class="input-number">' + arescat.MetaFisica 
                                                                    + '</td><td class="input-number">'+(arescat.CostoUnitario*arescat.MetaFisica)+'"</td>';
                                                            for (var i_crono_arescat = 0; i_crono_arescat < cantmeses; i_crono_arescat++) {
                                                                
                                                                if (arescat.Cronogramas[i_crono_arescat]) {
                                                                    
                                                                    html += '<td class="input-number">'
                                                                        + arescat.Cronogramas[i_crono_arescat].MetaFisica + '</td>';
                                                                }
                                                            }
                                                        }
                                                        html += '</tr>';
                                                    }
                                                }
                                            }
                                        }
                                        // Totales por componente
                                        html += '</tbody>';
                                        html += '</table>';
                                        html += '</div>';
                                        html += '</div>';
                                        html += '</div>';
                                    }
                                    
                                    $('#table-container').html(html);
                                   
                                    
                                    $('.table-responsive').css('max-height', $(window).height() * 0.60);
                                    $('.input-number').inputmask("decimal", {
                                        radixPoint: ".",
                                        groupSeparator: ",",
                                        groupSize: 3,
                                        digits: 2,
                                        integerDigits: 7,
                                        autoGroup: true,
                                        allowPlus: false,
                                        allowMinus: true,
                                        placeholder: ''
                                    }).click(function () {
                                        $(this).select();
                                    });
                                }
                            </script>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div id="modal-loading" class="modal fade modal-loading in" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-hidden="false" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content-no-shadow modal-content">
            <div class="text-center"><i class="fa fa-spinner fa-pulse fa-fw" style="color:#16a085;font-size: 100px;"></i></div>
        </div>
    </div>
</div>