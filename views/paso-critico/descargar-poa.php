<?php
use app\models\Actividad;
use app\models\ActRubroElegible;
use app\models\CronogramaAreSubCategoria;
use app\models\CronogramaProyecto;
use app\models\MarcoLogicoActividad;
use app\models\AreSubCategoria;



/** Error reporting */
error_reporting(E_ALL);

/** Include path **/
ini_set('include_path', ini_get('include_path').';../web/PHPExcel/Classes/');
//require(__DIR__ . '/../../web/PHPExcel/Classes/');
/** PHPExcel */
include 'PHPExcel.php';

/** PHPExcel_Writer_Excel2007 */
include 'PHPExcel/Writer/Excel2007.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
$sheet = $objPHPExcel->getActiveSheet();

$sheet->setTitle('POA');

$Meses=[];
$objPHPExcel->setActiveSheetIndex(0);

$sheet->setCellValueByColumnAndRow(0, 1, "POA");

$sheet->getStyle('A1')->getAlignment()->applyFromArray(
    array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    )
);
$sheet->getRowDimension('1')->setRowHeight(20);
$sheet->getStyle('A1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('c5c5c5');
$sheet->getStyle('A1')->getFont()->setBold(true);

$styleArray = array(
      'borders' => array(
          'allborders' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
          )
      )
  );  // Bordes


$sheet->SetCellValue('A3', '#');
$sheet->SetCellValue('B3', 'OE/Act/Recur');
$sheet->SetCellValue('C3', 'Clasificador');
$sheet->SetCellValue('D3', 'Unidad Medida');
$sheet->SetCellValue('E3', 'Cantidad');
$sheet->SetCellValue('F3', 'Costo Unitario');
$sheet->SetCellValue('G3', 'Total Proyecto');

$sheet->getRowDimension('3')->setRowHeight(30);
$sheet->getColumnDimension('A')->setAutoSize(true);
$sheet->getColumnDimension('B')->setAutoSize(true);
$sheet->getColumnDimension('C')->setAutoSize(true);
$sheet->getColumnDimension('D')->setAutoSize(true);
$sheet->getColumnDimension('E')->setAutoSize(true);
$sheet->getColumnDimension('F')->setAutoSize(true);
$sheet->getColumnDimension('G')->setAutoSize(true);
$sheet->getStyle('A3:G3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle('A3:G3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


foreach(CronogramaProyecto::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->andWhere(['between', 'Mes', $PasoCritico->MesInicio, $PasoCritico->MesFin])->all() as $mes)
{
    array_push ( $Meses , 'Mes '.Yii::$app->tools->DescripcionMes($mes->Mes) );
}

$sheet->fromArray($Meses,null,'H3');

$highestColumm = $objPHPExcel->setActiveSheetIndex(0)->getHighestColumn();
$sheet->mergeCells('A1:'.$highestColumm."1");  // Combinar columna
$sheet->getStyle('A1:'.$highestColumm.'1')->applyFromArray($styleArray);
$sheet->getStyle('H3:'.$highestColumm."3")->getAlignment()->applyFromArray(
    array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // alineacion horizontal
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER, // alineacion vertical
    )
);
$sheet->getStyle('A3:'.$highestColumm."3")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('c5c5c5'); // Color de fondo
$sheet->getStyle('A3:'.$highestColumm."3")->getFont()->setBold(true);  // Negrita
$sheet->getStyle('A3:'.$highestColumm."3")->applyFromArray($styleArray); // Celdas



$i=4;
foreach($componentes as $componente)
{
    //$actividades=Actividad::find()->where('ComponenteID=:ComponenteID',[':ComponenteID'=>$componente->ID])->orderBy('Correlativo asc')->all();
    $actividades=Actividad::find()
                ->select('Actividad.ID,Actividad.ComponenteID,Actividad.Nombre,Actividad.CostoUnitario,Actividad.Correlativo')
                ->innerJoin('ActRubroElegible','ActRubroElegible.ActividadID=Actividad.ID')
                ->innerJoin('AreSubCategoria','AreSubCategoria.ActRubroElegibleID=ActRubroElegible.ID')
                ->innerJoin('CronogramaAreSubCategoria','CronogramaAreSubCategoria.AreSubCategoriaID=AreSubCategoria.ID')
                ->where('Actividad.Estado=:Estado and Actividad.ComponenteID=:ComponenteID and CronogramaAreSubCategoria.MetaFisica!=0',[':Estado' => 1, ':ComponenteID' => $componente->ID])
                ->andWhere(['between', 'CronogramaAreSubCategoria.Mes', $mesInicio, $mesFin])
                ->groupBy('Actividad.ID,Actividad.Correlativo,Actividad.ComponenteID,Actividad.Nombre,Actividad.CostoUnitario')
                ->orderBy('Actividad.Correlativo asc')
                ->all();
                
    $TotalComponente=0;
    foreach($actividades as $actividad)
    {
        /*
        $recursos=ActRubroElegible::find()
                ->select('AreSubCategoria.ID,RubroElegible.Nombre RubroElegible,AreSubCategoria.Nombre Recurso,AreSubCategoria.UnidadMedida,AreSubCategoria.CostoUnitario,AreSubCategoria.MetaFisica')
                ->innerJoin('AreSubCategoria','AreSubCategoria.ActRubroElegibleID=ActRubroElegible.ID')
                ->innerJoin('RubroElegible','RubroElegible.ID=ActRubroElegible.RubroElegibleID')
                ->where('ActRubroElegible.ActividadID=:ActividadID',[':ActividadID'=>$actividad->ID])
                ->all();*/
        $recursos=ActRubroElegible::find()
                ->select('AreSubCategoria.ID,RubroElegible.Nombre RubroElegible,AreSubCategoria.Nombre Recurso,AreSubCategoria.UnidadMedida,AreSubCategoria.CostoUnitario,AreSubCategoria.MetaFisica')
                ->innerJoin('AreSubCategoria','AreSubCategoria.ActRubroElegibleID=ActRubroElegible.ID')
                ->innerJoin('RubroElegible','RubroElegible.ID=ActRubroElegible.RubroElegibleID')
                ->innerJoin('CronogramaAreSubCategoria','CronogramaAreSubCategoria.AreSubCategoriaID=AreSubCategoria.ID')
                ->where('ActividadID=:ActividadID and CronogramaAreSubCategoria.MetaFisica!=0',[':ActividadID' => $actividad->ID])
                ->andWhere(['between', 'CronogramaAreSubCategoria.Mes', $mesInicio, $mesFin])
                ->groupBy('AreSubCategoria.ID,RubroElegible.Nombre,AreSubCategoria.Nombre,AreSubCategoria.UnidadMedida,AreSubCategoria.CostoUnitario,AreSubCategoria.MetaFisica')
                ->all();
                
        
        foreach($recursos as $recurso)
        {
            $TotalComponente=$TotalComponente+($recurso->MetaFisica*$recurso->CostoUnitario);
        }
    }
    $sheet->SetCellValue('A'.$i, ''.$componente->Correlativo.'');
    $sheet->SetCellValue('B'.$i, 'O.E. '.$componente->Nombre);
    $sheet->SetCellValue('C'.$i, '');
    $sheet->SetCellValue('D'.$i, '');
    $sheet->SetCellValue('E'.$i, $componente->Cantidad);
    $sheet->SetCellValue('F'.$i, $componente->CostoUnitario);
    $sheet->SetCellValue('G'.$i, $componente->TotalObjetivo);
    $sheet->getStyle('A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->getStyle('G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // Perzonalizacion
    $sheet->getStyle('A'.$i.':'.$highestColumm.$i)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('c5c5c5');
    $sheet->getStyle('A'.$i.':'.$highestColumm.$i)->getAlignment()->applyFromArray(
    array(
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
        )
    );
    $sheet->getStyle('A'.$i.':'.$highestColumm.$i)->applyFromArray($styleArray);
    // End Perzonalizacion


    
    
    $a=1;
    foreach($actividades as $actividad)
    {
        /*
        $recursos=ActRubroElegible::find()
                ->select('AreSubCategoria.ID,RubroElegible.Nombre RubroElegible,AreSubCategoria.Nombre Recurso,AreSubCategoria.UnidadMedida,AreSubCategoria.CostoUnitario,AreSubCategoria.MetaFisica')
                ->innerJoin('AreSubCategoria','AreSubCategoria.ActRubroElegibleID=ActRubroElegible.ID')
                ->innerJoin('RubroElegible','RubroElegible.ID=ActRubroElegible.RubroElegibleID')
                ->where('ActRubroElegible.ActividadID=:ActividadID',[':ActividadID'=>$actividad->ID])
                ->all();*/
        $recursos=ActRubroElegible::find()
                ->select('AreSubCategoria.ID,RubroElegible.Nombre RubroElegible,AreSubCategoria.Nombre Recurso,AreSubCategoria.UnidadMedida,AreSubCategoria.CostoUnitario,AreSubCategoria.MetaFisica,sum(CronogramaAreSubCategoria.MetaFisica) MetaFisicaCronograma')
                ->innerJoin('AreSubCategoria','AreSubCategoria.ActRubroElegibleID=ActRubroElegible.ID')
                ->innerJoin('RubroElegible','RubroElegible.ID=ActRubroElegible.RubroElegibleID')
                ->innerJoin('CronogramaAreSubCategoria','CronogramaAreSubCategoria.AreSubCategoriaID=AreSubCategoria.ID')
                ->where('ActRubroElegible.ActividadID=:ActividadID and CronogramaAreSubCategoria.MetaFisica!=0',[':ActividadID' => $actividad->ID])
                ->andWhere(['between', 'CronogramaAreSubCategoria.Mes', $mesInicio, $mesFin])
                ->groupBy('AreSubCategoria.ID,RubroElegible.Nombre,AreSubCategoria.Nombre,AreSubCategoria.UnidadMedida,AreSubCategoria.CostoUnitario,AreSubCategoria.MetaFisica')
                ->all();
                
        $TotalActividad=0;
        foreach($recursos as $recurso)
        {
            $TotalActividad=$TotalActividad+($recurso->MetaFisica*$recurso->CostoUnitario);
        }
        
        $marcoLogicoActividad=MarcoLogicoActividad::find()->where('ActividadID=:ActividadID',[':ActividadID'=>$actividad->ID])->one();
        if(!$marcoLogicoActividad)
        {
            $marcoLogicoActividad=new MarcoLogicoActividad;
        }
        else
        {
            $sheet->SetCellValue('A'.($a+$i), ''.$componente->Correlativo.'.'.$actividad->Correlativo.'' );
            $sheet->SetCellValue('B'.($a+$i), 'Act. '.$actividad->Nombre);
            $sheet->SetCellValue('C'.($a+$i), '');
            $sheet->SetCellValue('D'.($a+$i), $marcoLogicoActividad->IndicadorUnidadMedida);
            $sheet->SetCellValue('E'.($a+$i), $actividad->Cantidad);
            
            $sheet->SetCellValue('F'.($a+$i), $actividad->CostoUnitario);
            $sheet->SetCellValue('G'.($a+$i), $TotalActividad);
    
            $sheet->getStyle('A'.($a+$i))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('G'.($a+$i))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            // Perzonalizacion
            $sheet->getStyle('A'.($a+$i).':'.$highestColumm.($a+$i))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ddf4fa');
            $sheet->getStyle('A'.($a+$i).':'.$highestColumm.($a+$i))->getAlignment()->applyFromArray(
            array(
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                )
            );
            $sheet->getStyle('A'.($a+$i).':'.$highestColumm.($a+$i))->applyFromArray($styleArray);
            
            $r=1;
            foreach($recursos as $recurso)
            {
                $sheet->SetCellValue('A'.($a+$i+$r), ''.$componente->Correlativo.'.'.$actividad->Correlativo.'.'.$r.'' );
                $sheet->SetCellValue('B'.($a+$i+$r), 'Rec. '.$recurso->Recurso);
                $sheet->SetCellValue('C'.($a+$i+$r), $recurso->RubroElegible);
                $sheet->SetCellValue('D'.($a+$i+$r), $recurso->UnidadMedida);
                $sheet->SetCellValue('E'.($a+$i+$r), $recurso->MetaFisicaCronograma);
                $sheet->SetCellValue('F'.($a+$i+$r), $recurso->CostoUnitario);
                $sheet->SetCellValue('G'.($a+$i+$r), ($recurso->MetaFisicaCronograma*$recurso->CostoUnitario));
    
                $sheet->getStyle('A'.($a+$i+$r))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('C'.($a+$i+$r))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('D'.($a+$i+$r))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('E'.($a+$i+$r))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('F'.($a+$i+$r))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    
                // Perzonalizacion
                $sheet->getStyle('A'.($a+$i+$r).':'.$highestColumm.($a+$i+$r))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('fcf3d0');
                $sheet->getStyle('A'.($a+$i+$r).':'.$highestColumm.($a+$i+$r))->getAlignment()->applyFromArray(
                array(
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                    )
                );
                $sheet->getStyle('A'.($a+$i+$r).':'.$highestColumm.($a+$i+$r))->applyFromArray($styleArray);
                $sheet->getStyle('G'.($a+$i+$r).':'.$highestColumm.($a+$i+$r))->getAlignment()->applyFromArray(
                    array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // alineacion horizontal
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER, // alineacion vertical
                    )
                );
                // End Perzonalizacion
    
                $cronogramaRecursos=CronogramaAreSubCategoria::find()->where('AreSubCategoriaID=:AreSubCategoriaID',[':AreSubCategoriaID'=>$recurso->ID])->andWhere(['between', 'Mes', $PasoCritico->MesInicio, $PasoCritico->MesFin])->all();
                $cronogramaRecursosMetaFisica=[];
                foreach($cronogramaRecursos as $cronogramaRecurso)
                {
                    array_push ( $cronogramaRecursosMetaFisica , $cronogramaRecurso->MetaFisica );
                }
                $sheet->fromArray($cronogramaRecursosMetaFisica,null,'H'.($a+$i+$r));
                $sheet->setCellValue( 'G'.($a+$i),'=SUM(G'.($a+$i+1).':G'.($a+$i+$r).')');
                $r++;
            }
            
        }
        $a=$r+$a;
    }
    $i=$i+$a;
}




// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="CronogramaFisico.xls"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
