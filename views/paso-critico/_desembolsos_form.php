<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'desembolsos/crear?ProyectoID='.$ProyectoID,
        'options'           => [
            'autocomplete'  => 'off',
            'id'            => 'frmDesembolsos'
        ]
    ]
); ?>
    <input type="hidden" name="PasoCritico[ProyectoID]" value="<?php echo $ProyectoID ?>">
    <div class="modal-body">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label">Mes Inicio<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="PasoCritico[MesInicio]">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Mes Fin<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="PasoCritico[MesFin]">
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button id="btn-save-desembolso" type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>


<script>
    var $form = $('#frmDesembolsos');
    var formParsleyDesembolsos = $form.parsley(defaultParsleyForm());

    $(document).ready(function () {
        $('.count-message').each(function () {
            var $this = $(this);
            var $divformgroup = $this.closest('div.form-group');
            var $txt = $divformgroup.find('textarea, input[type="text"]');
            var text_max = parseInt($txt.attr('maxlength'));

            $txt.keyup(function () {
                var text_length = $txt.val().length;
                var text_remaining = text_max - text_length;
                $this.html(text_remaining + ' caracteres restantes');
            });
        });
    });
</script>