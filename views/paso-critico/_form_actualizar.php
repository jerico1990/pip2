<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => \Yii::$app->request->BaseUrl.'/paso-critico/actualizar?PasoCriticoID='.$model->ID,
        'options'           => [
            'autocomplete'  => 'off',
            'id'            => 'frmDesembolsos'
        ]
    ]
); ?>
    <div class="modal-body">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label">Mes Inicio<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <select class="form-control" name="PasoCritico[MesInicio]" required>
                        <option value>Seleccionar</option>
                        <?php for($i=1;$i<=32;$i++){ ?> 
                        <option value="<?= $i ?>" <?= ($i==$model->MesInicio)?'selected':'';?>><?= Yii::$app->tools->DescripcionMes($i) ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Mes Fin<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <select class="form-control" name="PasoCritico[MesFin]" required>
                        <option value>Seleccionar</option>
                        <?php for($i=2;$i<=32;$i++){ ?>
                        <option value="<?= $i ?>" <?= ($i==$model->MesFin)?'selected':'';?>><?= Yii::$app->tools->DescripcionMes($i) ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button id="btn-save-paso-critico" type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>


<script>
    var $form = $('#frmDesembolsos');
    var formParsleyDesembolsos = $form.parsley(defaultParsleyForm());

</script>