
<div id="page-content" style="min-height: 1342px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Pasos Cr�tico</h1>
        </div>
        <div id="page-heading">
            <!--<button class="btn-crear-desembolso" data-proyecto-id="<?= $proyecto->ID ?>">Crear Paso Critico</button>-->
        </div>
        <div class="container">
            <div class="table-responsive">
                <table id="proyectos" class="display table table-bordered table-striped table-condensed table-hover" cellspacing="0" >
                    <thead>
                        <tr>
                            <!-- <th>C�digo Proyecto</th> -->
                            <th>Proyecto</th>
                            <th>Correlativo</th>
                            <th>Mes Inicio</th>
                            <th>Mes Fin</th>
                            <th>Monto</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-ctrl-desembolsos" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">�</span></button>
                <h4 class="modal-title">Desembolso</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<script>
    
    $(document).ready(function() {
        var ProyectoID=<?= $proyecto->ID ?>;
        $.ajax({
            url:'paso-critico/lista-paso-critico?ProyectoID='+ProyectoID,
            async:false,
            data:{},
            beforeSend:function()
            {
                $('.panel-default').LoadingOverlay("show");
            },
            success:function(result)
            {
                //$tblresultjs.destroy();//
                $("#proyectos tbody").html(result);
            },
            error:function(){
                alert('Error al realizar el proceso de busqueda.');
            }
        });
        
    } );
    
    $('body').on('click', '.btn-crear-desembolso', function (e) {
        e.preventDefault();
        var ProyectoID = $(this).attr('data-proyecto-id');
        $('#modal-ctrl-desembolsos .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/desembolsos/crear?ProyectoID='+ProyectoID);
        $('#modal-ctrl-desembolsos').modal('show');
    });
    
    $('body').on('click', '#btn-save-desembolso', function (e) {
        
        e.preventDefault();
        // alert('s');
        
        var $btn = $(this);
        $btn.attr('disabled', 'disabled');
        var isValid = formParsleyDesembolsos.validate();
        if (isValid) {
            sendFormNoMessage($('#frmDesembolsos'), $(this), function (msg) {
                $("#modal-ctrl-desembolsos").modal('hide');
                _pageLoadingEnd();
		
            });
        } else {
        }
    });
    
</script>