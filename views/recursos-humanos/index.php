<div id="page-content" style="min-height: 934px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Equipo del Proyecto</h1>
        </div>
        <div class="container">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4>Equipo de Gestión y Proyectista</h4>
                    <div class="options">
                        <a class="panel-collapse" href="#">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    <div id="div-equipogestion-main">
                        <div class="form-group div-label-left">
                            <button type="button" id="btn-create-miembroequipogestion" class="btn btn-midnightblue-alt "><i class="fa fa-plus"></i>&nbsp;Agregar</button>
                        </div>
                        <div id="div-equipogestion-table">    
                            <?php if(!empty($miembrosEquipoGestion)): ?>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed table-hover tbl-act">
                                        <thead>
                                            <tr class="tr-header">
                                                <th>Acciones</th>
                                                <th>Función a desempeñar</th>
                                                <th>Nombres</th>
                                                <th>Apellidos</th>
                                                <th>N° Documento</th>
                                                <!--<th>Entidad</th>-->
                                                <th>Titulo Obtenido</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($miembrosEquipoGestion as $miembroEquipoGestion){ ?>
                                                <tr>
                                                    <td class="td-acciones">
                                                        <a href="#" class="btn-remove-miembroequipogestion" data-toggle="tooltip" title="" role-id="<?= $miembroEquipoGestion->MiembroEquipoGestionID ?>" data-original-title="Eliminar"><i class="fa fa-remove fa-lg"></i></a>
                                                        <a href="#" class="btn-edit-miembroequipogestion" data-toggle="tooltip" title="Editar" role-id="<?= $miembroEquipoGestion->ID ?>"><i class="fa fa-edit fa-lg"></i></a>
                                                    </td>
                                                    <td><?= $miembroEquipoGestion->FuncionDesempenar ?></td>
                                                    <td><?= $miembroEquipoGestion->Nombre ?></td>
                                                    <td><?= $miembroEquipoGestion->ApellidoPaterno." ".$miembroEquipoGestion->ApellidoMaterno ?></td>
                                                    <td><?= $miembroEquipoGestion->NroDocumento ?></td>
                                                    <td><?= $miembroEquipoGestion->TituloObtenido ?></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            <?php else: ?>
                                <div class="alert alert-info">
                                    <i class="fa fa-info"></i>&nbsp;&nbsp;No tiene ningun registro.
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>

            <script>
                $('.btn-edit-miembroequipogestion').click(function () {
                    var roleID = $(this).attr('role-id');
                    $('#modal-create-miembroequipoinvestigacion .modal-body').load("<?= Yii::$app->getUrlManager()->createUrl('recursos-humanos/update-miembro-equipo-gestion?ID=') ?>"+roleID );
                    $('#modal-create-miembroequipoinvestigacion').modal('show');
                });
                
                $('#btn-create-miembroequipogestion').click(function () {
                    $('#modal-create-miembroequipoinvestigacion .modal-body').load("<?= Yii::$app->getUrlManager()->createUrl('recursos-humanos/create-miembro-equipo-gestion?investigador='.$investigador) ?>" );
                    $('#modal-create-miembroequipoinvestigacion').modal('show');
                });
                
                $(document).on('click','.btn-remove-miembroequipogestion',function (e) {
                    e.preventDefault();
                    var $btn = $(this)
                    bootboxConfirmEliminar($btn,
                        'Está seguro de eliminar al integrante del equipo de gestión y proyectista?',
                        function () {
                            var epID = $btn.attr('role-id');
                            var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                            $.post('<?= \Yii::$app->request->BaseUrl ?>/recursos-humanos/eliminar-integrante/', { id: epID, _csrf: toke }, function (data) {
                                    var jx = JSON.parse(data);
                                if (jx.Success == true) {
                                    //scargarColaboradoras();
                                    location.reload();
                                }
                            });
                        });
                });
                
            </script>
        </div>
    </div>
</div>

<div id="modal-create-miembroequipogestion" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Miembro del Equipo Administrativo</h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>

<div id="modal-create-miembroequipoinvestigacion" class="modal fade" tabindex="-1" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Miembro del Equipo Técnico</h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<!--
<div id="modal-loading" class="modal fade modal-loading" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content-no-shadow modal-content">
            <div class="text-center"><i class="fa fa-spinner fa-pulse fa-fw" style="color:#16a085;font-size: 100px;"></i></div>
        </div>
    </div>
</div>
-->
<script>
    // var situacionProyecto=<?= \Yii::$app->user->identity->SituacionProyecto ?>;
    // if (situacionProyecto!=0) {
    //     $("#btn-create-miembroequipogestion").hide();
    // }
</script>