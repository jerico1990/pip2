<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<style>
    .modal-dialog
    {
        width:1024px;
        margin:30px auto;
    }
</style>
    <?php //$form = ActiveForm::begin(); ?>
    <?php 
    $form = ActiveForm::begin(
        [
            // 'action'    => 'create-miembro-equipo-gestion?investigador=3' empty($Modelor->Codigo) ? 'entidad-participante/crearrepresentante' : 'entidad-participante/actualizarepresentante?id='.$Modelor->Codigo,
            'options'   => [
                'autocomplete'  => 'off',
                'id'            => 'frmMiembroEquipoGestion'
             ]
        ]
    ); 
    ?>

        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-2 control-label">Tipo de Documento<span class="f_req">*</span></label>
                <div class="col-sm-4">
                    <select class="form-control" id="TipoDocumento" name="Persona[TipoDocumento]" required>
                        <option value>Seleccionar</option>
                        <option value="1" <?= $model->TipoDocumento==1?'selected':'' ?> >DNI</option>
                        <option value="2" <?= $model->TipoDocumento==2?'selected':'' ?> >Carnet de Extranjeria</option>
                    </select>
                    <!--<h6 class="pull-left count-message"></h6>-->
                </div>
                <label class="col-sm-2 control-label">Nro Documento <span class="f_req">*</span></label>
                <div class="col-sm-4">
                    <input class="form-control" id="NroDocumento"  onfocusout="DNI($(this).val())" name="Persona[NroDocumento]" required type="text" onKeyPress="return soloNumeros(event)" value="<?= $model->NroDocumento ?>">
                    <!--<h6 class="pull-left count-message"></h6>-->
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Nombres <span class="f_req">*</span></label>
                <div class="col-sm-10">
                    <input class="form-control" id="Nombre" maxlength="50" name="Persona[Nombre]" required type="text" value="<?= $model->Nombre ?>" >
                    <!--<h6 class="pull-left count-message"></h6>-->
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Apellido Paterno <span class="f_req">*</span></label>
                <div class="col-sm-4">
                    <input class="form-control" id="ApellidoPaterno" maxlength="50" name="Persona[ApellidoPaterno]" required type="text" value="<?= $model->ApellidoPaterno ?>" >
                </div>
                
                <label class="col-sm-2 control-label">Apellido Materno <span class="f_req">*</span></label>
                <div class="col-sm-4">
                    <input class="form-control" id="ApellidoMaterno" maxlength="50" name="Persona[ApellidoMaterno]" required type="text" value="<?= $model->ApellidoMaterno ?>" >
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Correo electrónico <span class="f_req">*</span></label>
                <div class="col-sm-4">
                    <input class="form-control" id="Email" maxlength="50" name="Persona[Email]" required type="email" value="<?= $model->Email ?>">
                    <!--<h6 class="pull-left count-message"></h6>-->
                </div>
                <label class="col-sm-2 control-label">Teléfono <span class="f_req">*</span></label>
                <div class="col-sm-4">
                    <input class="form-control" id="Telefono" maxlength="20" onKeyPress="return soloNumeros(event)" name="Persona[Telefono]" required type="text" value="<?= $model->Telefono ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Función a desempeñar <span class="f_req">*</span></label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="FuncionDesempeñar" name="Persona[FuncionDesempenar]" value="<?= $model->FuncionDesempenar ?>" required>
                </div>
                <label class="col-sm-2 control-label">Entidad <span class="f_req">*</span></label>
                <div class="col-sm-4">
                    <select id="EntidadParticipanteID" name="Persona[EntidadParticipanteID]" class="form-control" required>
                        <option value>Seleccionar</option>
                        <?php foreach($entidadesColaboradoras as $entidadColaboradora){ ?>
                            <option value="<?= $entidadColaboradora->ID ?>" <?= ($entidadColaboradora->ID==$model->EntidadParticipanteID)?'selected':''; ?>><?= $entidadColaboradora->RazonSocial ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Título o grado obtenido <span class="f_req">*</span></label>
                <div class="col-sm-4">
                    <input class="form-control" id="TituloObtenido" maxlength="250" name="Persona[TituloObtenido]" required type="text" value="<?= $model->TituloObtenido ?>">
                    <!--<h6 class="pull-left count-message"></h6>-->
                </div>

                <label class="col-sm-2 control-label">Tipo Contratación  <span class="f_req">*</span></label>
                <div class="col-sm-4">
                    <select id="EntidadParticipanteID" name="Persona[TipoContratacion]" class="form-control" required>
                        <option value>Seleccionar</option>
                        <?php foreach($tipoContratacion as $contrat){ ?>
                            <option value="<?= $contrat->ID ?>" <?php echo $model->TipoContratacion==$contrat->ID?'selected':'' ?>><?= $contrat->Nombre ?></option>
                        <?php } ?>
                    </select>
                </div>

                
            </div>
            <!--             
            <div class="form-group">
                <label class="col-sm-4 control-label"> Adjuntar CV</label>
                <div class="col-sm-8">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="input-group">
                            <div class="form-control uneditable-input" data-trigger="fileinput">
                                <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                <span class="fileinput-filename">
                                    ANEXO_CV
                                </span>
                            </div>
                            <span class="input-group-addon btn btn-default btn-file">
                                <span class="fileinput-new">Selecciona</span>
                                <span class="fileinput-exists">Cambiar</span>
                                <input type="file" id="file-cv-administrativo" name="filePadronProductores_proponente">
                            </span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Eliminar</a>
                        </div>
                    </div>
                    <br>
                    <a href="/SLFC/Descarga/DescargarAnexo_CV_Administrativo"><i class="fa fa-download"></i>&nbsp;Descargar formato</a>
                    &nbsp;&nbsp;&nbsp;
                    <a href="#" id="btn-upload-cv-administrativo"><i class="fa fa-cloud-upload fa-lg"></i>&nbsp;Adjuntar Archivo</a>
                </div>
            </div> -->
            <hr>
            <div class="form-inline text-right">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button id="btn-save-miembroequipogestion" type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
            </div>
        <br>
            <!--
        <div class="alert alert-warning">
            <strong>IMPORTANTE: Para adjuntar un archivo se debe seguir los siguientes pasos:</strong>
            <ol>
                <li>Descargar el formato en caso exista, utilizando la opción &nbsp;<a href="javascript:void(0);"><i class="fa fa-download"></i>&nbsp;Descargar formato</a></li>
                <li>Llenar el formato descargado, imprimirlo, firmarlo y escaneárlo.</li>
                <li>Adjuntar el archivo escaneado, utilizando la opción &nbsp;<a href="javascript:void(0);"><i class="fa fa-cloud-upload fa-lg"></i>&nbsp;Adjuntar archivo</a></li>
            </ol>
        </div>-->
    </div>
<?php ActiveForm::end(); ?>

<script>

    $("#NroDocumento").focus();


    function DNI(valor) {
        $('#Persona_Nombre').val('');
        $('#Persona_ApellidoPaterno').val('');
        $('#Persona_ApellidoMaterno').val('');

        $.get('<?= \Yii::$app->request->BaseUrl ?>/viatico/servicio-reniec/', { valor: valor}, function (data) {
            var jx = JSON.parse(data);
            alert(data[1]);
            console.log(jx.error);
            if (jx.error ==0) {
                $('#Persona_Nombre').attr('disabled','disabled');
                $('#Persona_ApellidoPaterno').attr('disabled','disabled');
                $('#Persona_ApellidoMaterno').attr('disabled','disabled');
                // $('#btn-save-miembroequipogestion').attr('disabled','disabled');
            }else if(jx.error ==1){
                console.log("DNI no se encuentra");
                // $('#btn-save-miembroequipogestion').attr('disabled','disabled');
            }
            else
            {
                // if(jx.error != 1){
                    $('#btn-save-miembroequipogestion').removeAttr('disabled');
                    var name = jx.data.nombres;
                    var paterno = jx.data.apellidoPaterno;
                    var materno = jx.data.apellidoMaterno;
                    $('#Nombre').val(name.trim());
                    $('#ApellidoPaterno').val(paterno.trim());
                    $('#ApellidoMaterno').val(materno.trim());
                // }
            }
            
        });
    }

    
    var formParsleyMiembroEquipoGestion = $('#frmMiembroEquipoGestion').parsley(defaultParsleyForm());

    // document.getElementById('btn-upload-cv-administrativo').onclick = function () {
    //     var formdata = new FormData();
    //     var fileInput = document.getElementById('file-cv-administrativo');
    //     if (fileInput.files.length) {
    //         for (i = 0; i < fileInput.files.length; i++) {
    //             formdata.append(fileInput.files[i].name, fileInput.files[i]);
    //         }
    //         var xhr = new XMLHttpRequest();
    //         xhr.open('POST', '/SLFC/RecursosHumanos/UploadCvAdministrativo?id=' + '251');
    //         xhr.send(formdata);
    //         xhr.onreadystatechange = function () {
    //             if (xhr.readyState == 4 && xhr.status == 200) {
    //                 console.log(xhr);
    //                 var op = JSON.parse(xhr.responseText);
    //                 if (op.Success) {
    //                     bootbox.alert('El archivo se cargó correctamente');
    //                 }
    //             }
    //         }
    //     } else {
    //         bootbox.alert('Debe seleccionar un archivo.');
    //     }
    //     return false;
    // }

    // $(document).ready(function () {
    //     $('#frmMiembroEquipoGestion [required=""]').attr('data-parsley-required', 'true');
    //     $("#frmMiembroEquipoGestion #Experiencia").inputmask('decimal', { min: 0 });
    //     $("#NroDocumento").inputmask('integer');

    //     $('#EntidadParticipanteID').val('282');

    //     $('.count-message').each(function () {
    //         var $this = $(this);
    //         var $divformgroup = $this.closest('div.form-group');
    //         var $txt = $divformgroup.find('textarea, input[type="text"]');
    //         var text_max = parseInt($txt.attr('maxlength'));

    //         $txt.keyup(function () {
    //             var text_length = $txt.val().length;
    //             var text_remaining = text_max - text_length;
    //             $this.html(text_remaining + ' caracteres restantes');
    //         });
    //     });
    // });
    
    $('#btn-save-miembroequipogestion').click(function (e) {
        e.preventDefault();
        var isValid = formParsleyMiembroEquipoGestion.validate();
        if (isValid) {
            sendForm($('#frmMiembroEquipoGestion'), $(this), function () {
                // cargarEquipoGestion();
                location.reload();
            });
        }
    });
    

    $('#TipoDocumento').change(function(){
        var val = $(this).val();

        if(val == 1){
            $('#NroDocumento').removeAttr("disabled");
            $('#NroDocumento').attr("onfocusout","DNI($(this).val())");
            $('#NroDocumento').attr("maxlength","8");
            $('#NroDocumento').attr("minlength","8");

            // $('#Nombre').attr("disabled","disabled");
            // $('#ApellidoPaterno').attr("disabled","disabled");
            // $('#ApellidoMaterno').attr("disabled","disabled");


        }else if(val == 2){
            $('#NroDocumento').removeAttr("disabled");
            $('#NroDocumento').attr("onfocusout","");
            $('#NroDocumento').attr("maxlength","11");
            $('#NroDocumento').attr("minlength","11");
            
            $('#Nombre').removeAttr("disabled");
            $('#ApellidoPaterno').removeAttr("disabled");
            $('#ApellidoMaterno').removeAttr("disabled");

        }else if(val == ''){
            //$('#NroDocumento').attr("disabled","disabled");
        }

    })

</script>