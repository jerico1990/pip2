<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
    <title>PNIA</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?= \Yii::$app->request->BaseUrl ?>/recursos/css/styles.css" rel="stylesheet">
    <link href="<?= \Yii::$app->request->BaseUrl ?>/recursos/css/fonts.css" rel="stylesheet">
    <link href="<?= \Yii::$app->request->BaseUrl ?>/recursos/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/datatables-1.10/dataTables.bootstrap.css" rel="stylesheet">
    <link href="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/form-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="<?= \Yii::$app->request->BaseUrl ?>/recursos/css/site.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>

    <style>
        .focusedform .verticalcenter {
            top: 35%;
            margin-top: 0;
        }

        .focusedform {
            background-color: #f1f6e7;
        }

        h1.title {
            text-transform: uppercase;
            font-weight: 600;
            font-family: "Trebuchet MS", Verdana, sans-serif;
            padding-top: 6rem;
            padding-bottom: 3rem;
            font-size: 3.2rem;
        }

        .mycenter {
            width: 100%;
            height: 500px;
            position: relative;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            margin: auto;
            text-align: center;
        }

        .mycenter-body {
            display: inline-block;
        }
    </style>
</head>
<body class="focusedform">
<?php $this->beginBody() ?>
    <?= $content ?>
 


    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/js/jquery-1.10.2.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/js/bootstrap.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/js/enquire.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/js/jquery.cookie.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/form-autosize/jquery.autosize-min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/form-jasnyupload/fileinput.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/form-parsley/parsley.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/form-parsley/es.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/form-parsley/es.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/form-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/form-datepicker/js/locales/bootstrap-datepicker.es.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/form-inputmask/inputmask.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/form-inputmask/inputmask.numeric.extensions.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/form-inputmask/jquery.inputmask.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/Datejs/date.js"></script>

    <textarea tabindex="-1" style="position: absolute; top: -999px; left: 0px; right: auto; bottom: auto; border: 0px; box-sizing: content-box; word-wrap: break-word; overflow: hidden; transition: none; height: 0px !important; min-height: 0px !important;"></textarea>

    

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
