<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\PasoCritico;
use app\models\Usuario;
use app\models\Investigador;
use app\models\Persona;
use app\models\Proyecto;
use app\models\Rol;
use app\models\UsuarioRol;

AppAsset::register($this);
$session = Yii::$app->session;

if(\Yii::$app->user->identity->rol==7){
    $usuario=Usuario::findOne(\Yii::$app->user->id);
    $datosInvestigador=Persona::findOne($usuario->PersonaID);
    $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
    $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
    $desembolsos=PasoCritico::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->all();
}
else
{
    $usuario=Usuario::findOne(\Yii::$app->user->id);
}

?>
<?php //$this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
    <title>PNIA</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?= \Yii::$app->request->BaseUrl ?>/recursos/css/styles.css" rel="stylesheet">
    <link href="<?= \Yii::$app->request->BaseUrl ?>/recursos/css/fonts.css" rel="stylesheet">
    <link href="<?= \Yii::$app->request->BaseUrl ?>/recursos/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/datatables-1.10/dataTables.bootstrap.css" rel="stylesheet">
    <link href="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/form-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="<?= \Yii::$app->request->BaseUrl ?>/recursos/css/site.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries. Placeholdr.js enables the placeholder attribute -->
    <!--[if lt IE 9]>
        <link rel="stylesheet" href="assets/css/ie8.css">
        <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script>
        <script type="text/javascript" src="assets/plugins/charts-flot/excanvas.min.js"></script>
    <![endif]-->
    <!-- <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/js/jquery-1.10.2.min.js"></script> -->
     <!-- <script src="https://code.jquery.com/jquery-3.2.0.min.js"></script> -->
     <!-- <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script> -->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/js/jquery-1.10.2.min.js"></script> 
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/js/bootstrap.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/js/enquire.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/js/jquery.cookie.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/form-autosize/jquery.autosize-min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/form-jasnyupload/fileinput.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/form-parsley/parsley.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/form-parsley/es.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/form-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/form-datepicker/js/locales/bootstrap-datepicker.es.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/form-inputmask/inputmask.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/form-inputmask/inputmask.numeric.extensions.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/form-inputmask/jquery.inputmask.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/Datejs/date.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/bootbox/bootbox.min.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/datatables-1.10/datatables.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/js/client.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/js/application.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/js/jquery.filter_input.js"></script>
</head>
<body>
<?php //$this->beginBody() ?>
    <img class="img-responsive" src="<?= \Yii::$app->request->BaseUrl ?>/recursos/img/cabecera.jpg" width="100%">
    <header class="navbar navbar-inverse" role="banner">
        <a id="leftmenu-trigger" class="tooltips" data-placement="right"></a>
        <div class="navbar-header pull-left">
            <a class="navbar-brand" href="#/"> Sistema de seguimiento PIP2 </a>
        </div>
        <div class="pull-left net parpadea">
        </div>
        <ul class="nav navbar-nav pull-right toolbar">
            <li class="dropdown">
                <a href="javascript:;" class="dropdown-toggle username" data-toggle="dropdown"><span><?= Yii::$app->user->identity->datos.' - '.Yii::$app->user->identity->username ?> <i class="fa fa-caret-down"></i></span></a>
                <ul class="dropdown-menu userinfo arrow">
                    <li class="userlinks">
                        <ul class="dropdown-menu">
                            <li><a href="#" class="btn-guardar-contrasena text-right">Cambiar Contraseña</a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/site/logout?q=<?php echo $session->get('SysIntegra')?>&w=<?php echo base64_encode($session->get('Url')) ?>" class="text-right">Cerrar Sesión</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>

    </header>

    <div id="page-container">
        <nav id="page-leftbar" role="navigation">
            <ul class="acc-menu" id="sidebar" style="top: 40px;">
                <li>
                  <?php //echo base64_encode($session->get('Url')); ?>
                    <?php if(\Yii::$app->user->identity->rol==11){ ?>
                    <a href="<?= \Yii::$app->request->BaseUrl ?>/panel"><i class="fa fa-info-circle"></i> <span>Panel</span></a>
                    <?php }else{ ?>
                    <a href="<?= \Yii::$app->request->BaseUrl ?>/panel"><i class="fa fa-info-circle"></i> <span>Indicaciones</span></a>
                    <?php }?>
                </li>
                <li class="divider"></li>
                <li class="divider"></li>
                <?php if(\Yii::$app->user->identity->rol==1){ ?>
                    <li class="open  hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Mantenimientos</span></a>
                        <ul class="acc-menu" >
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/persona"><i class="fa fa-info"></i>&nbsp;<span>Persona</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/rol"><i class="fa fa-info"></i>&nbsp;<span>Rol</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/usuario"><i class="fa fa-info"></i>&nbsp;<span>Usuarios</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/usuario-rol"><i class="fa fa-info"></i>&nbsp;<span>Asignar rol</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/usuario-proyecto"><i class="fa fa-university"></i><span>Asignar proyecto</span></a></li>
                        </ul>
                    </li>
                <?php } ?>
                <?php if(\Yii::$app->user->identity->rol==7){ ?>
                    <li class="open  hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Proyecto</span></a>
                        <ul class="acc-menu" >
                            <!--<li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/inbox"><i class="fa fa-info"></i>&nbsp;<span>Inbox</span></a></li>-->
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/informacion-general"><i class="fa fa-info"></i>&nbsp;<span>Información general</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/entidad-participante"><i class="fa fa-university"></i><span>Entidades participantes</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/recursos-humanos"><i class="fa fa-users"></i><span>Equipo del Proyecto</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/marco-logico"><i class="fa fa-search"></i><span>Marco lógico</span></a></li>


                            <!--<li><a href="<?= \Yii::$app->request->BaseUrl ?>/tarea"><i class="fa fa-list-alt"></i><span>Tareas</span></a></li>-->
                        </ul>
                    </li>
                    <li class="hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Programación</span></a>
                        <ul class="acc-menu">
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/recursos-poa"><i class="fa fa-sticky-note-o"></i><span>Recursos</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/recursos-tarea"><i class="fa fa-sticky-note-o"></i><span>Tareas</span></a></li>
                            <!--<li><a href="<?= \Yii::$app->request->BaseUrl ?>/programacion-experimento"><i class="fa fa-sticky-note-o"></i><span>Experimento</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/programacion-evento"><i class="fa fa-sticky-note-o"></i><span>Evento</span></a></li>-->
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/programacion-experimento-new"><i class="fa fa-sticky-note-o"></i><span>Experimento new</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/programacion-evento-new"><i class="fa fa-sticky-note-o"></i><span>Evento new</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/paso-critico"><i class="fa fa-sticky-note-o"></i><span>Pasos críticos</span></a></li>

                            <!-- <li><a href="<?= \Yii::$app->request->BaseUrl ?>/plan-adquisiciones/listado"><i class="fa fa-sticky-note-o"></i><span>Plan de Adquisiciones</span></a></li> -->

                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/plan-adquisiciones/listado"><i class="fa fa-sticky-note-o"></i><span>Plan de adquisiciones </span></a></li>
                            <?php if((\Yii::$app->user->identity->SituacionProyecto==0 || \Yii::$app->user->identity->SituacionProyecto==3)){ ?>
                                <li><a href="<?= \Yii::$app->request->BaseUrl ?>/informacion-general/enviar-evaluacion"><i class="fa fa-sticky-note-o"></i><span>Enviar POA</span></a></li>
                            <?php } ?>
                            <?php if(\Yii::$app->user->identity->SituacionProyecto==1){ ?>
                            <!--<li><a href="<?= \Yii::$app->request->BaseUrl ?>/reprogramacion"><i class="fa fa-sticky-note-o"></i><span>Reprogramación</span></a></li>-->
                            <?php }?>
                        </ul>
                    </li>


                    <?php if(\Yii::$app->user->identity->SituacionProyecto==1){ ?>
                    <!--
                    <li class="hasChild">
                        <a href="javascript:;"><i class="fa fa-file-text-o"></i> <span>Padrón de usuarios</span></a>
                        <ul class="acc-menu">
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/padron"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Padrón de usuarios</span></a></li>
                        </ul>
                    </li>
                    -->
                    <!--
                    <li class="hasChild">
                        <a href="javascript:;"><i class="fa fa-file-text-o"></i> <span>Ejecución Financiera</span></a>
                        <ul class="acc-menu">
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/orden-servicio"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Rendición de Gastos Pendientes</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/orden-servicio"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Rendición de Gastos Definitivos</span></a></li>

                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/orden-servicio"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Solicitud de certificacion</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/orden-compra"><i class="fa fa-list-alt"></i>&nbsp;Nota de certificacion</a></li>

                        </ul>
                    </li>
                    -->
                    <!--
                    <li class="hasChild">
                        <a href="javascript:;"><i class="fa fa-file-text-o"></i> <span>Re programación</span></a>
                        <ul class="acc-menu">
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/re-programacion-recurso"><i class="fa fa-sticky-note-o"></i><span>Recursos</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/re-programacion-tarea"><i class="fa fa-sticky-note-o"></i><span>Tareas</span></a></li>
                        </ul>
                    </li>
                    -->
                    <li class="hasChild">

                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Ejecución</span></a>
                        <ul class="acc-menu">
                            <!--<li><a href="<?= \Yii::$app->request->BaseUrl ?>/caja-chica/apertura"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Apertura de Caja Chica</span></a></li>-->
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/requerimiento"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Requerimiento</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/compromiso-presupuestal"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Compromiso presupuestal</span></a></li>
                            <!--<li><a href="<?= \Yii::$app->request->BaseUrl ?>/termino-referencia"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Termino de referencia</span></a></li>-->

                            <!--<li><a href="<?= \Yii::$app->request->BaseUrl ?>/requerimiento-encargo"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Encargos</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/requerimiento-caja-chica"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Caja Chica</span></a></li>-->


                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/rendicion"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Rendición de viático</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/rendicion-caja-chica"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Rendición caja chica</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/rendicion-encargo"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Rendición encargo</span></a></li>



                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/conformidad-pago"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Conformidad de pago</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/gastos-generales"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Carga Itf</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/comprobante"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Comprobantes de pago</span></a></li>
                            <!--<li><a href="<?= \Yii::$app->request->BaseUrl ?>/pago"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Pago</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/orden/reporte"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Reportes</span></a></li>
                            -->
							
							<li><a href="<?= \Yii::$app->request->BaseUrl ?>/ejecucion-experimento-new"><i class="fa fa-sticky-note-o"></i><span>Experimento new</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/ejecucion-evento-new"><i class="fa fa-sticky-note-o"></i><span>Evento new</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/ejecucion-publicacion-new"><i class="fa fa-sticky-note-o"></i><span>Publicaci&oacute;n new</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/ejecucion-tesis-new"><i class="fa fa-sticky-note-o"></i><span>Tesis new</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/ejecucion-indicadores-new"><i class="fa fa-sticky-note-o"></i><span>Indicadores new</span></a></li>
                        </ul>
                    </li>
                    <!--
                    <li class="hasChild">
                        <a href="javascript:;"><i class="fa fa-file-text-o"></i> <span>Viáticos</span></a>
                        <ul class="acc-menu">
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/viatico/planilla-viaticos"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Planilla de Viáticos</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/requerimiento"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Rendición de cuentas de viáticos</span></a></li>
                        </ul>
                    </li>
                    -->
                    <li class="hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Seguimiento</span></a>
                        <ul class="acc-menu">
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/recursos-poa-ejecutado"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Recursos</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/recursos-tarea-ejecutado"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Tareas</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/programacion-visita"><i class="fa fa-sticky-note-o"></i><span>Programacion de Visitas new</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/seguimiento-proyecto"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Seguimiento de Proyectos new</span></a></li>
                            <!--
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/orden-servicio"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Solicitud de certificacion</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/orden-compra"><i class="fa fa-list-alt"></i>&nbsp;Nota de certificacion</a></li>
                            -->
                        </ul>
                    </li>
                    <li class="hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Medios de verificación</span></a>
                        <ul class="acc-menu">
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/experimento"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Experimento</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/evento"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Evento</span></a></li>

                        </ul>
                    </li>
                    <li class="hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>ITF</span></a>
                        <ul class="acc-menu">
                            <?php $i=1; ?>
                            <?php foreach($desembolsos as $desembolso){ ?>
                                <li class="hasChild">
                                    <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Paso crítico <?= $desembolso->Correlativo ?></span></a>
                                    <ul class="acc-menu">
                                        <li><a href="<?= \Yii::$app->request->BaseUrl ?>/informe-tecnico-financiero/index?PasoCriticoID=<?= $desembolso->ID ?>"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Informe técnico</span></a></li>

                                        <li><a href="<?= \Yii::$app->request->BaseUrl ?>/itf/gastos-pendientes"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Rendición de gastos pendientes</span></a></li>
                                        <li><a href="<?= \Yii::$app->request->BaseUrl ?>/itf/gastos-definitivos"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Rendición de gastos definitivos</span></a></li>
                                        <li><a href="<?= \Yii::$app->request->BaseUrl ?>/itf/libro-banco"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Libro banco</span></a></li>

                                    </ul>
                                </li>
                                <?php $i++; ?>
                            <?php } ?>

                        </ul>
                    </li>
                    <!--
                    <li class="hasChild">
                        <a href="javascript:;"><i class="fa fa-file-text-o"></i> <span>Certificacion Presupuestal</span></a>
                        <ul class="acc-menu">
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/orden-servicio"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Solicitud de certificacion</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/orden-compra"><i class="fa fa-list-alt"></i>&nbsp;Nota de certificacion</a></li>
                        </ul>
                    </li>


                    <li class="hasChild">
                        <a href="javascript:;"><i class="fa fa-file-text-o"></i> <span>Adquisiciones</span></a>
                        <ul class="acc-menu">
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/orden-servicio"><i class="fa fa-sticky-note-o"></i>&nbsp;<span>Orden de servicio</span></a></li>
                            <li><a href="<?= \Yii::$app->request->BaseUrl ?>/orden-compra"><i class="fa fa-list-alt"></i>&nbsp;Orden de compra</a></li>
                            <li><a href=""><i class="fa fa-bar-chart"></i>&nbsp;Conformidad de adquisicones</a></li>
                            <li><a href=""><i class="fa fa-search"></i>&nbsp;Informe de compras</a></li>
                            <li><a href=""><i class="fa fa-usd"></i>&nbsp;Reporte de pagos de proveedores</a></li>
                        </ul>
                    </li>
                    -->

                    <?php } ?>

                <?php }elseif (\Yii::$app->user->identity->rol==2 ){ ?>
                    <li class="open  hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Proyecto</span></a>
                        <ul class="acc-menu">
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/proyecto"><i class="fa fa-info"></i>&nbsp;<span>Listado de proyectos</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/evaluacion-proyecto"><i class="fa fa-info"></i>&nbsp;<span>Evaluación de proyectos</span></a></li>
                            <!--<li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/ejecutado-proyecto"><i class="fa fa-info"></i>&nbsp;<span>Seguimiento de Proyectos</span></a></li>-->
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/evaluacion-proyecto/aprobacion-pa"><i class="fa fa-info"></i>&nbsp;<span>Evaluación del PAC</span></a></li>

                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/panel/proyectos"><i class="fa fa-info"></i>&nbsp;<span>Mapa de proyectos</span></a></li>
                            <!--<li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/certificacion-presupuestal"><i class="fa fa-info"></i>&nbsp;<span>Certificación Presupuestal</span></a></li>-->
                        </ul>
                    </li>
                    <li class="open  hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Ejecución</span></a>
                        <ul class="acc-menu">
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/requerimiento/uafsi"><i class="fa fa-info"></i>&nbsp;<span>Requerimientos</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/compromiso-presupuestal/uafsi"><i class="fa fa-info"></i>&nbsp;<span>Compromisos presupuestales</span></a></li>
                        </ul>
                    </li>
                    <li class="open  hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Re Programación</span></a>
                        <ul class="acc-menu">
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/re-programacion-recurso/uafsi"><i class="fa fa-info"></i>&nbsp;<span>Listado recurso</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/re-programacion-tarea/uafsi"><i class="fa fa-info"></i>&nbsp;<span>Listado tarea</span></a></li>
                        </ul>
                    </li>

                    <li class="hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Reportes BI</span></a>
                        <ul class="acc-menu">
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/tecnico-uafsi"><i class="fa fa-info"></i>&nbsp;<span>Tecnicos UAFSI</span></a></li>
                        </ul>
                    </li>

                    <?php if(\Yii::$app->user->id==1119){ ?>
                    <li class="hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Reportes dinámicos</span></a>
                        <ul class="acc-menu">
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/dinamico"><i class="fa fa-info"></i>&nbsp;<span>Información general</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/colaboradora"><i class="fa fa-info"></i>&nbsp;<span>Colaboradora</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/ambito"><i class="fa fa-info"></i>&nbsp;<span>Ámbito</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/dinamico-recursos"><i class="fa fa-info"></i>&nbsp;<span>Recursos</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/experimento-evento"><i class="fa fa-info"></i>&nbsp;<span>Experimento y evento</span></a></li>
                        </ul>
                    </li>
                    <li class="hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Reportes gráficos</span></a>
                        <ul class="acc-menu">
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/listado-region"><i class="fa fa-info"></i>&nbsp;<span>Proyecto por región</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/estacion-experimental-grafico"><i class="fa fa-info"></i>&nbsp;<span>Estación experimental</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/eventos-experimentos-grafico"><i class="fa fa-info"></i>&nbsp;<span>Experimento y evento</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/mensual-financiero-grafico"><i class="fa fa-info"></i>&nbsp;<span>Mensual financiero</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/mensual-financiero-grafico-proyecto"><i class="fa fa-info"></i>&nbsp;<span>Mensual financiero Proyecto</span></a></li>
                        </ul>
                    </li>
                    <?php } ?>

                    <li class="hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>ADMINISTRADOR</span></a>
                        <ul class="acc-menu">
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/rol"><i class="fa fa-info"></i>&nbsp;<span>Gestión de Rol</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/usuario"><i class="fa fa-info"></i>&nbsp;<span>Usuarios</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/usuario-rol"><i class="fa fa-info"></i>&nbsp;<span>Permisos</span></a></li>
                        </ul>
                    </li>

                    <li class="hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Otros Compomentes</span></a>
                        <ul class="acc-menu">
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/compromiso-presupuestal-reporte"><i class="fa fa-info"></i>&nbsp;<span>Compromiso Presupuestal</span></a></li>
                        </ul>
                    </li>

                <?php }elseif (\Yii::$app->user->identity->rol==5){ ?>
                    <li class="open  hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Proyecto</span></a>
                        <ul class="acc-menu" style="display: block;">
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/evaluacion-proyecto"><i class="fa fa-info"></i>&nbsp;<span>Evaluación de proyectos</span></a></li>
                            <!-- <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/ejecutado-proyecto"><i class="fa fa-info"></i>&nbsp;<span>Seguimiento de proyectos</span></a></li>  -->
                            <!-- <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/evaluacion-proyecto/aprobacion-pa"><i class="fa fa-info"></i>&nbsp;<span>Evaluación del PAC</span></a></li> -->

                            <!--<li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/certificacion-presupuestal"><i class="fa fa-info"></i>&nbsp;<span>Certificación Presupuestal</span></a></li>-->
                        </ul>
                    </li>

                    <li class="open  hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Requerimiento</span></a>
                        <ul class="acc-menu" style="display: block;">
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/requerimiento/eeaa"><i class="fa fa-info"></i>&nbsp;<span>Listado de requerimientos </span></a></li>
                        </ul>
                    </li>
                <?php }elseif (\Yii::$app->user->identity->rol==3){ ?>
                    <li class="open  hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Certificacion presupuestal</span></a>
                        <ul class="acc-menu" style="display: block;">
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/certificacion-presupuestal"><i class="fa fa-info"></i>&nbsp;<span>Emitir certificación presupuestal</span></a></li>
                        </ul>
                    </li>
                <?php }elseif (\Yii::$app->user->identity->rol==8){ ?>
                    <li class="open  hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Proyecto</span></a>
                        <ul class="acc-menu" style="display: block;">
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/evaluacion-proyecto/aprobacion-pa"><i class="fa fa-info"></i>&nbsp;<span>Evaluación del PAC</span></a></li>
                        </ul>
                    </li>
                <?php }elseif (\Yii::$app->user->identity->rol==10){ ?>
                    <li class="open  hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Adquisición</span></a>
                        <ul class="acc-menu" style="display: block;">

                        </ul>
                    </li>
                <?php }elseif (\Yii::$app->user->identity->rol==16){ ?>
                    <li class="hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Proyectos</span></a>
                        <ul class="acc-menu">
                            <!--<li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/usuario-proyecto/index"><i class="fa fa-info"></i>&nbsp;<span>Asignar Proyecto</span></a></li>-->
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/listado-proyecto"><i class="fa fa-info"></i>&nbsp;<span>Listado de proyectos</span></a></li>

                        </ul>
                    </li>
                    <li class="hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Reportes dinámicos</span></a>
                        <ul class="acc-menu">
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/dinamico"><i class="fa fa-info"></i>&nbsp;<span>Información general</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/colaboradora"><i class="fa fa-info"></i>&nbsp;<span>Colaboradora</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/ambito"><i class="fa fa-info"></i>&nbsp;<span>Ámbito</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/dinamico-recursos"><i class="fa fa-info"></i>&nbsp;<span>Recursos</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/experimento-evento"><i class="fa fa-info"></i>&nbsp;<span>Experimento y evento</span></a></li>
                        </ul>
                    </li>
                    <li class="hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Reportes gráficos</span></a>
                        <ul class="acc-menu">
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/listado-region"><i class="fa fa-info"></i>&nbsp;<span>Proyecto por región</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/estacion-experimental-grafico"><i class="fa fa-info"></i>&nbsp;<span>Estación experimental</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/eventos-experimentos-grafico"><i class="fa fa-info"></i>&nbsp;<span>Experimento y evento</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/mensual-financiero-grafico"><i class="fa fa-info"></i>&nbsp;<span>Mensual financiero</span></a></li>
                        </ul>
                    </li>
                <?php }elseif (\Yii::$app->user->identity->rol==11){ ?>
                    <li class="hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Proyectos</span></a>
                        <ul class="acc-menu">
                            <!--<li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/usuario-proyecto/index"><i class="fa fa-info"></i>&nbsp;<span>Asignar Proyecto</span></a></li>-->
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/evaluacion-proyecto"><i class="fa fa-info"></i>&nbsp;<span>Listado de proyectos</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/requerimiento/uafsi"><i class="fa fa-info"></i>&nbsp;<span>Requerimientos</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/compromiso-presupuestal/uafsi"><i class="fa fa-info"></i>&nbsp;<span>Compromiso presupuestal</span></a></li>
                        </ul>
                    </li>
                    <li class="hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Reportes dinámicos</span></a>
                        <ul class="acc-menu">
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/dinamico"><i class="fa fa-info"></i>&nbsp;<span>Información General</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/colaboradora"><i class="fa fa-info"></i>&nbsp;<span>Colaboradora</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/ambito"><i class="fa fa-info"></i>&nbsp;<span>Ambito</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/dinamico-recursos"><i class="fa fa-info"></i>&nbsp;<span>Recursos</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/experimento-evento"><i class="fa fa-info"></i>&nbsp;<span>Experimento y Evento</span></a></li>
                        </ul>
                    </li>
                    <li class="hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Reportes gráficos</span></a>
                        <ul class="acc-menu">
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/listado-region"><i class="fa fa-info"></i>&nbsp;<span>Proyecto por región</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/estacion-experimental-grafico"><i class="fa fa-info"></i>&nbsp;<span>Estación experimental</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/eventos-experimentos-grafico"><i class="fa fa-info"></i>&nbsp;<span>Experimento y Evento</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/mensual-financiero-grafico"><i class="fa fa-info"></i>&nbsp;<span>Mensual financiero</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/mensual-financiero-grafico-proyecto"><i class="fa fa-info"></i>&nbsp;<span>Mensuals financiero Proyecto</span></a></li>
                        </ul>
                    </li>
                    <li class="hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Reportes BI</span></a>
                        <ul class="acc-menu">
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/reporte/tecnico-uafsi"><i class="fa fa-info"></i>&nbsp;<span>Tecnicos UAFSI</span></a></li>
                        </ul>
                    </li>
                <?php }elseif (\Yii::$app->user->identity->rol==12){ ?>
                    <li class="open  hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Compromisos Presupuestal</span></a>
                        <ul class="acc-menu" style="display: block;">
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/compromiso-presupuestal/administracion"><i class="fa fa-info"></i>&nbsp;<span>Pendientes</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/compromiso-presupuestal/administracion-aprobado"><i class="fa fa-info"></i>&nbsp;<span>Aprobados</span></a></li>
                        </ul>
                    </li>
                <?php }elseif (\Yii::$app->user->identity->rol==13){ ?>
                    <li class="open  hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Compromisos Presupuestal</span></a>
                        <ul class="acc-menu" style="display: block;">
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/compromiso-presupuestal/contabilidad"><i class="fa fa-info"></i>&nbsp;<span>Listado</span></a></li>
                        </ul>
                    </li>
                <?php }elseif (\Yii::$app->user->identity->rol==14){ ?>
                    <li class="open  hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Compromisos Presupuestal</span></a>
                        <ul class="acc-menu" style="display: block;">
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/compromiso-presupuestal/especialista-administrativo"><i class="fa fa-info"></i>&nbsp;<span>Listado</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/compromiso-presupuestal/especialista-administrativo-aprobado"><i class="fa fa-info"></i>&nbsp;<span>Aprobados</span></a></li>
                        </ul>
                    </li>
                    <li class="hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Conformidad</span></a>
                        <ul class="acc-menu" style="display: block;">
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/conformidad-pago/listado-general"><i class="fa fa-info"></i>&nbsp;<span>Listado</span></a></li>
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/conformidad-pago/listado-conformidad-aprobado"><i class="fa fa-info"></i>&nbsp;<span>Aprobados</span></a></li>
                        </ul>
                    </li>

                <?php }elseif (\Yii::$app->user->identity->rol==15){ ?>
                    <li class="open  hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Compromisos Presupuestal</span></a>
                        <ul class="acc-menu" style="display: block;">
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/compromiso-presupuestal/uafsi"><i class="fa fa-info"></i>&nbsp;<span>Listado</span></a></li>
                        </ul>
                    </li>

                <?php }elseif (\Yii::$app->user->identity->rol==18){ ?>
                    <li class="open  hasChild">
                        <a href="javascript:;"><i class="fa fa-check-square-o"></i> <span>Comprobante</span></a>
                        <ul class="acc-menu" style="display: block;">
                            <li class=""><a href="<?= \Yii::$app->request->BaseUrl ?>/comprobante"><i class="fa fa-info"></i>&nbsp;<span>Listado</span></a></li>
                        </ul>
                    </li>
                <?php } ?>

            </ul>
        </nav>

        <?= $content ?>

    </div>
    <div class="modal fade" id="modal-ctrl-contrasena" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Cambiar contraseña</h4>
                </div>
                <div class="modal-body-main">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-ctrl-contrasena-forzado" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>-->
                    <h4 class="modal-title">Cambiar contraseña</h4>
                </div>
                <div class="modal-body-main">
                </div>
            </div>
        </div>
    </div>
    <script>
        $('body').on('click', '.btn-guardar-contrasena', function (e) {
            var UsuarioID = <?= \Yii::$app->user->id ?>;
            $('#modal-ctrl-contrasena .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/usuario/cambiar-contrasena?ID='+UsuarioID+'');
            $('#modal-ctrl-contrasena').modal('show');
        });
    </script>


    <?php if(!$usuario->CodigoVerificacion){?>
    <script>
        //var CodigoProyecto = $(this).attr('data-codigo-proyecto');
        $('#modal-ctrl-contrasena-forzado .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/usuario/cambiar-contrasena?ID=<?= $usuario->ID ?>');
        $('#modal-ctrl-contrasena-forzado').modal('show');
    </script>
    <?php }?>
    <?php if (Yii::$app->session->hasFlash('CambioContrasena')): ?>
    <script>
        bootbox.alert('Se ha cambiado la contraseña');
    </script>
    <?php endif; ?>
    <script>

        $('body').on('click', '#btn-guardar-contrasena', function (e) {
            e.preventDefault();

            if($('#contrasena').val()!=$('#re-contrasena').val())
            {
                bootbox.alert('¡Las contraseñas no son idénticas!');
                return false;
            }
            else
            {
                var $btn = $(this);
                $btn.attr('disabled', 'disabled');
                var isValid = formParsleyUsuario.validate();
                if (isValid) {
                    $('#frmContrasena').submit();
                }else{
                    $btn.removeAttr('disabled');
                }
                $btn.removeAttr('disabled');
            }
        });
    </script>

    <div id="modal-ampliacion" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <h4>Estimados postulantes se les comunica que el sistema se mantendrá abierto hasta las 8am del 15 de setiembre del 2016 </h4>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <div class="modal fade" id="modal-ctrl-internet" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="modal-header">
                        <h4 class="modal-title">CONEXIÓN</h4>
                    </div>
                    <div class="modal-body-main">
                        <div class="conectividad"></div>
                        <div class="modal-footer pie-conexion none">
                            <button type="submit" class="btn btn-success" data-dismiss="modal"><i class="fa fa-check"></i>&nbsp;SEGUIR NAVEGANDO</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).on('ready',function () {
            // $('.datepicker').datepicker(defaultDatePicker());
            //$('#modal-ampliacion').modal('show');
            console.log(navigator.onLine);

        });

        function explode(){
            // window.location.replace("<?= \Yii::$app->request->BaseUrl ?>/site/logout");
        }

        var conecta = setInterval(function(){

            internet(navigator.onLine,0);

        },500);

        function internet(conexion,pop){
            if(conexion == true){
                $('.net').addClass('conectado');
                $('.net').removeClass('desconectado');
                $('.net').html('<i class="fa fa-podcast " aria-hidden="true"></i> Conectado');

                $('#modal-ctrl-internet .modal-body-main .conectividad').html('<h5>Su conexión ha sido reestrablecida.</h5>');
                $('#modal-ctrl-internet .modal-body-main .pie-conexion').removeClass('none');

            } else if(conexion == false) {
                $('#modal-ctrl-internet .modal-body-main .pie-conexion').addClass('none');
                $('.net').addClass('desconectado');
                $('.net').removeClass('conectado');
                $('.net').html('<i class="fa fa-user-times" aria-hidden="true"></i> No tiene conexión');
                if(pop == 1){
                    $('#modal-ctrl-internet .modal-body-main .conectividad').html('<h5>Ocurrio un error en la conectividad de internet, verifique su conexión o comuniquese con el administrador de red.</h5>');
                    $('#modal-ctrl-internet').modal('show');
                    // bootbox.alert('Conexión a internet perdida, dentro de 5 segundos se cerrará sesión.');

                }
                // setTimeout(explode, 5000);
                window.clearInterval(conecta);
                setInterval(function(){
                    internet(navigator.onLine,0);
                },1000);

            }
        }



        $('body').tooltip({ selector: '[data-toggle="tooltip"]' });
        $('#sidebar ul li a').click(function () {
            _pageLoadingStart();
        });
        document.body.onclick = function (e) {
            if (e.ctrlKey) {
                _pageLoadingEnd();
            }
        }
        /*
        function prueba_notificacion() {
            if (Notification) {
            if (Notification.permission !== "granted") {
            Notification.requestPermission()
            }
            var title = "Xitrus"
            var extra = {
            icon: "http://xitrus.es/imgs/logo_claro.png",
            body: "Notificación de prueba en Xitrus"
            }
            var noti = new Notification( title, extra)
            noti.onclick = {
            // Al hacer click
            }
            noti.onclose = {
            // Al cerrar
            }
            setTimeout( function() { noti.close() }, 10000)
            }
        }
        prueba_notificacion();
        */
    </script>

    <style type="text/css">
        .none{
            display: none;
        }
        .conectado{
            color: rgb(23, 252, 23);
            padding-top: 10px;
            text-transform: uppercase;
        }
        .desconectado{
            color: red;
            padding-top: 10px;
            text-transform: uppercase;
        }


        .parpadea {
          animation-name: parpadeo;
          animation-duration: 3s;
          animation-timing-function: linear;
          animation-iteration-count: infinite;

          -webkit-animation-name:parpadeo;
          -webkit-animation-duration: 3s;
          -webkit-animation-timing-function: linear;
          -webkit-animation-iteration-count: infinite;
        }

        @-moz-keyframes parpadeo{
          0% { opacity: 1.0; }
          50% { opacity: 0.0; }
          100% { opacity: 1.0; }
        }

        @-webkit-keyframes parpadeo {
          0% { opacity: 1.0; }
          50% { opacity: 0.0; }
           100% { opacity: 1.0; }
        }

        @keyframes parpadeo {
          0% { opacity: 1.0; }
           50% { opacity: 0.0; }
          100% { opacity: 1.0; }
        }
    </style>
<?php // $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
