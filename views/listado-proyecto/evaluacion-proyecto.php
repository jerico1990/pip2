<?php
    $descripcionSituacion="";
   switch ($situacionProyecto) {
    case 0:
        $descripcionSituacion="Iniciado";
        break;
    case 1:
        $descripcionSituacion="Aprobado";
        break;
    case 2:
        $descripcionSituacion="Pendiente";
        break;
    case 3:
        $descripcionSituacion="Observado";
        break;
    }
    
?>
<div id="page-content" style="min-height: 885px;">
    <div id="wrap">
        <div id="page-heading">
            <h1></h1>
        </div>
        <div class="container">
                    
            <style>
                #page-heading {
                    display: none;
                }

                .tab-content {
                    background-color: #fff;
                }

                .panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 1px solid #a8a8ab !important;
                }

                .panel-gray .panel-body {
                    border-top: none !important;
                }

                .panel-heading {
                    height: auto !important;
                }
            </style>
            <h3>Estado del Proyecto: <span><b><?= $descripcionSituacion ?></b></span></h3>
            <div class="tab-container">
                <ul class="nav nav-tabs">
                    <!--<li id="li-poaf" ><a href="#tab-seguimiento" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Seguimiento</a></li>-->
                    <li id="li-poaf" class="active"><a href="#tab-informacion-general" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Información General</a></li>
                    <li id="li-poaf"><a href="#tab-marco-logico" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Marco Lógico</a></li>
                    <li id="li-poaf" ><a href="#tab-recursos" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Recursos</a></li>
                    <li id="li-poaf" ><a href="#tab-tarea" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Tareas</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane " id="tab-seguimiento">
                        <?php $observacionOR=0; ?>
                        <?php $observacionAR=0; ?>
                        <?php foreach($ObservacionesRecursos as $ObservacionRecurso){
                                if($ObservacionRecurso->Estado==1)
                                {
                                    $observacionOR++;
                                }
                                elseif($ObservacionRecurso->Estado==3)
                                {
                                    $observacionAR++;
                                }
                        }?>
                        
                        <?php if($observacionOR>0){ ?>
                            <span style="color: red">Existen observaciones pendientes en Recursos, Total: <?= $observacionOR ?> .</span>
                        <?php } ?>
                        <?php if($observacionAR>0){ ?>
                            <span style="color: green">El proyecto contiene <?= $observacionAR ?> aprobaciones de recursos cerradas.</span>
                        <?php } ?>
                        <br>
                        <?php $observacionOT=0; ?>
                        <?php $observacionAT=0; ?>
                        <?php foreach($ObservacionesTareas as $ObservacionTarea){
                                if($ObservacionTarea->Estado==1)
                                {
                                    $observacionOT++;
                                }
                                elseif($ObservacionTarea->Estado==3)
                                {
                                    $observacionAT++;
                                }
                        }?>
                        
                        <?php if($observacionOT>0){ ?>
                            <span style="color: red">Existen observaciones pendientes en Tareas, Total: <?= $observacionOT ?> .</span>
                        <?php } ?>
                        <?php if($observacionAT>0){ ?>
                            <span style="color: green">El proyecto contiene <?= $observacionAT ?> aprobaciones de tareas cerradas.</span>
                        <?php } ?>
                        <br><br>
                        <?php if(\Yii::$app->user->identity->Rol==5 && $situacionProyecto==2 && $seguimientosCount==0){?>
                            <button class="btn btn-primary btn-aprobar-poa" ><i class="fa fa-check"></i>&nbsp;Aprobar POA</button>
                            <button class="btn btn-primary btn-observar-poa" >&nbsp;Observar POA</button>
                        <?php }elseif(\Yii::$app->user->identity->Rol==2 && $situacionProyecto==2 && $seguimientosCount==1){ ?>
                            <button class="btn btn-primary btn-aprobar-poa" ><i class="fa fa-check"></i>&nbsp;Aprobar POA</button>
                            <button class="btn btn-primary btn-observar-poa" >&nbsp;Observar POA</button>
                        <?php } ?>
                    </div>
                    <div class="tab-pane active" id="tab-informacion-general">
                        <div id="container-poaf">
                            <div id="container-informacion-general"></div>
                            
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-marco-logico">
                        <div id="container-poaf">
                            <div id="container-marco-logico"></div>
                        </div>
                    </div>
                    <div class="tab-pane " id="tab-recursos">
                        <div id="container-poaf-recursos">
                            <style>
                                #container-poaf-recursos .input-number {
                                    text-align: right;
                                    min-width: 72px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }

                                #container-poaf-recursos .input-number-sm {
                                    text-align: right;
                                    min-width: 52px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }

                                #container-poaf-recursos .input-number-md {
                                    text-align: right;
                                    min-width: 82px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }

                                #container-poaf-recursos table-act > tbody > tr > td:nth-child(1) {
                                    text-align: center;
                                }

                                #container-poaf-recursos .table-act > tbody > tr > td:nth-child(2) {
                                    min-width: 320px !important;
                                }

                                #container-poaf-recursos .table-act > tbody > tr > td:nth-child(3) {
                                    min-width: 100px !important;
                                }

                                #container-poaf-recursos .table > tbody > .tr-header > td {
                                    text-align: center;
                                    font-size: 12px;
                                    font-weight: bold;
                                    padding: 1px !important;
                                    vertical-align: middle !important;
                                    /*border: 1px solid #cfcfd0;
                                    background-color: #f0f0f1;*/
                                    border: 1px solid #c0c0c0;
                                    background-color: #e3e3e3;
                                    min-width: 75px !important;
                                }

                                #container-poaf-recursos h4.panel-title {
                                    font-size: 16px;
                                }

                                #container-poaf-recursos .tr-comp td, .tr-proy td {
                                    font-weight: bold;
                                }

                                #container-poaf-recursos .table-condensed td {
                                    padding: 1px !important;
                                }

                                #container-poaf-recursos td, #table-container input {
                                    font-size: 13px !important;
                                }

                                #container-poaf-recursos .form-control[readonly] {
                                    background-color: #f2f3f4;
                                }
                                #container-informacion-general #page-content{
                                    margin-left: 0px;
                                    padding-bottom:0px;
                                }
                                #container-informacion-general #wrap > .container
                                {
                                    padding: 0px;
                                }
                                
                                #container-marco-logico #page-content{
                                    margin-left: 0px;
                                    padding-bottom:0px;
                                }
                                #container-marco-logico #wrap > .container
                                {
                                    padding: 0px;
                                }
                                
                                
                            </style>
                            <br>

                            <div id="table-container-recursos">
                            </div>
                            <br>
                            <div class="text-center">
                                <a target="_blank" class="btn btn-inverse-alt" href="<?= Yii::$app->getUrlManager()->createUrl('recursos-poa/descargar-poa?codigo='.$informacion->Codigo.'') ?>">
                                    <i class="fa fa-download"></i>&nbsp;Descargar Cronograma Físico
                                </a>
                                
                                <a target="_blank" class="btn btn-inverse-alt" href="<?= Yii::$app->getUrlManager()->createUrl('recursos-poa/descargar-poa-financiero?codigo='.$informacion->Codigo.'') ?>">
                                    <i class="fa fa-download"></i>&nbsp;Descargar Cronograma Financiero
                                </a>
                            </div> 
                            <script>
                                var _proyectoRecurso = new Object();
                                $(document).ready(function () {
                                    var investigadorID=<?= $investigadorID ?>;
                                    
                                    //$("#container-informacion-general .btn-actv-tarea,.btn-enviar-evaluacion-poa,.fa-edit,.fa-remove").hide();
                                    
                                    $('#container-informacion-general').html('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>');
                                    $('#container-informacion-general').load('<?= \Yii::$app->request->BaseUrl ?>/informacion-general/index?investigador='+investigadorID);
                                    $("#container-informacion-general input,textarea,select").prop('disabled', true);
                                    
                                    
                                    $('#container-marco-logico').html('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>');
                                    $('#container-marco-logico').load('<?= \Yii::$app->request->BaseUrl ?>/marco-logico/index?investigadorID='+investigadorID);
                                    $("#container-marco-logico input,textarea,select").prop("disabled", true);
                                    $("#container-marco-logico .btn-edit-ml-finproyecto").prop("disabled", true);
                                    
                                    
                                    $('#table-container-recursos').html('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>');
                                    getProyectoRecurso().done(function(json) {
                                        _proyectoRecurso = json;
                                        _drawTableRecurso();
                                    });
                                });
                                
                                $('.btn-descargar-poa').click(function (e) {
                                    var $btn = $(this);
                                    var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                                    $.ajax({
                                        url: '<?= Yii::$app->getUrlManager()->createUrl('recursos-poa/descargar-poa') ?>',
                                        type: 'GET',
                                        async: false,
                                        data: {codigo:'<?= $informacion->Codigo ?>',_csrf: toke},
                                        success: function(data){
                                           
                                        }
                                    });
                                });
                                
                                function getProyectoRecurso() {
                                    var url = "<?= \Yii::$app->request->BaseUrl ?>/recursos-poa/recursos-json?codigo=<?= $informacion->Codigo ?>";
                                    return $.getJSON( url );
                                }

                                function _drawTableRecurso() {
                                    //console.log(_proyecto.Componentes);
                                    var situacionProyecto=<?= $situacionProyecto ?>;
                                    var situacionRecurso=<?= $situacionRecurso ?>;
                                    var html = '';
                                    var cantmeses = _proyectoRecurso.Cronogramas;
                                    for (var i_comp = 0; i_comp < _proyectoRecurso.Componentes.length; i_comp++) {
                                        var comp = _proyectoRecurso.Componentes[i_comp];
                                        html += '<div class="panel panel-gray"><div class="panel-heading">'
                                                + '<h4 class="panel-title">'
                                                + (i_comp + 1) + '. Objetivo específico - ' + comp.Nombre
                                                //+ '</a>'
                                                + '</h4></div>';
                                        html += '<div class="panel-body">';
                                        html += '<div class="table-responsive">';
                                        html += '<table class="table table-bordered table-condensed table-act">';
                                        html += '<tbody>';
                                        for (var i_act = 0; i_act < comp.Actividades.length; i_act++)
                                        {
                                            var acutotMF = 0.00;
                                            var acutotMFs = 0.00;
                                            var subacutotCat = 0.00;
                                            var subacutotCats = 0.00;
                                            var acuTotal = 0.00;
                                            var act = comp.Actividades[i_act];
                                            var TotalActividad=0;
                                            for (var i_are = 0; i_are < act.ActRubroElegibles.length; i_are++) {
                                                var are = act.ActRubroElegibles[i_are];
                                                if (are.AreSubCategorias.length) {
                                                    for (var i_arescat = 0; i_arescat < are.AreSubCategorias.length; i_arescat++) {
                                                        var arescat = are.AreSubCategorias[i_arescat];
                                                        if (arescat) {
                                                            TotalActividad=TotalActividad+(arescat.CostoUnitario*arescat.MetaFisica);
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            html += '<tr class="tr-header">';
                                            html += '<td>#</td><td>Actividad</td><td>Unidad de medida</td><td>Costo unitario</td>'
                                                    + '<td>Meta física</td><td>Total</td>';
                                            // Cronogramas
                                            
                                            for (var i_crono_act = 0; i_crono_act < cantmeses; i_crono_act++) {
                                                html += '<td>' + act.Cronogramas[i_crono_act].MesDescripcion + '</td>';
                                            }
                                            // Sumatorias
                                            html += '</tr>';
                                           
                                            html += '<tr class="tr-act-mf info" data-act-id="' + act.ID + '">';
                                            html += '<td>' + (i_comp + 1) + '.' + (i_act + 1) + '</td><td><b>' + act.Nombre + '</b></td><td>' + act.UnidadMedida+'</td><td><input type="text" readonly="" class="input-number form-control" value="'+(TotalActividad/act.MetaFisica)+'"></td>'
                                                    + '<td align="center">' + act.MetaFisica + '</td><td><input type="text" readonly="" class="input-number form-control" value="'+TotalActividad+'"></td>';
                                            for (var i_crono_act = 0; i_crono_act < cantmeses; i_crono_act++) {
                                                html += '<td></td>';
                                                //Max
                                                acutotMF += act.Cronogramas[i_crono_act].PoafMetaFisica;
                                                //End Max
                                            }
                                            html += '</tr>';
                                            html += '</tr>';
                                            
                                            // Rubros Elegibles
                                            html += '<tr>';
                                            html += '<td colspan="6"></td>';
                                            html += '</tr>';
                                            for (var i_are = 0; i_are < act.ActRubroElegibles.length; i_are++) {
                                                var are = act.ActRubroElegibles[i_are];
                                                var TotalRubroElegible=0;
                                                if (are.AreSubCategorias.length) {
                                                    for (var i_arescat = 0; i_arescat < are.AreSubCategorias.length; i_arescat++) {
                                                        var arescat = are.AreSubCategorias[i_arescat];
                                                        if (arescat) {
                                                            TotalRubroElegible=TotalRubroElegible+(arescat.CostoUnitario*arescat.MetaFisica);
                                                        }
                                                    }
                                                }
                                                
                                                html += '<tr class="tr-sub2-tot success">';
                                                html += '<td></td><td>' + are.RubroElegible.Nombre + '</td><td>' 
                                                        + '</td><td>'
                                                        + '</td><td>'
                                                        + '<td><input type="text" class="form-control input-number" readonly="" value="' + TotalRubroElegible + '" /></td>';
                                                for (var i_crono_are = 0; i_crono_are < cantmeses; i_crono_are++) {
                                                    html += '<td></td>';
                                                }
                                              
                                                //
                                                html += '</tr>';
                                                // Subcategorías
                                                if (are.AreSubCategorias.length) {
                                                    for (var i_arescat = 0; i_arescat < are.AreSubCategorias.length; i_arescat++) {
                                                        var arescat = are.AreSubCategorias[i_arescat];
                                                        if (arescat) {
                                                            html += '<tr class="tr-subact warning">';
                                                            html += '<td align="center">';
                                                            var alertaRecurso="red";
                                                            var  metaFisicaRecurso1=0;
                                                            for (var i_crono_arescat1 = 0; i_crono_arescat1 < cantmeses; i_crono_arescat1++) {
                                                                metaFisicaRecurso1=parseFloat(metaFisicaRecurso1)+parseFloat(arescat.Cronogramas[i_crono_arescat1].MetaFisica);
                                                            }
                                                           
                                                            if (parseFloat(arescat.MetaFisica)==parseFloat(metaFisicaRecurso1)) {
                                                                alertaRecurso="black";
                                                            }
                                                            
                                                            var color='black';
                                                            if (arescat.Situacion==3) {
                                                                color='red';
                                                            }
                                                            
                                                            var perfil='<?= $seguimientosCount ?>';
                                                            
                                                            var rol=<?= \Yii::$app->user->identity->Rol ?>;
                                                            if (situacionRecurso==2 && perfil==0 && rol==5) {
                                                                html += '<span style="color:'+color+'"  class="fa fa-search generar-observacion-recurso" data-recurso-id="'+ arescat.ID +'"></span>';
                                                            }
                                                            else if (situacionRecurso==2 && perfil==1 && rol==2) {
                                                                html += '<span style="color:'+color+'" class="fa fa-search generar-observacion-recurso" data-recurso-id="'+ arescat.ID +'"></span>';
                                                            }else if (situacionRecurso==3 && arescat.Situacion==3) {
                                                                html +='<span data-toggle="tooltip" title="'+arescat.Observacion+'"><i class="fa fa-eye"></i></span>';
                                                            }
                                                            
                                                            if (arescat.ObservacionSituacionID==3) {
                                                                alertaRecurso='green';
                                                            }
                                                            html += '</td><td style="color:'+alertaRecurso+'">' + arescat.Nombre + '</td><td>' + arescat.UnidadMedida
                                                                    + '</td><td><input type="text" class="form-control input-number" readonly="" value="' + arescat.CostoUnitario + '" />'
                                                                    + '</td><td><input type="text" style="color:'+alertaRecurso+'" class="form-control input-number-sm recurso-meta-fisica" data-meta-fisica="'+ arescat.MetaFisica +'" data-recurso-id="'+ arescat.ID +'" readonly="" value="' + arescat.MetaFisica + '" />'
                                                                    + '</td><td><input type="text" class="form-control input-number" readonly="" value="'+(arescat.CostoUnitario*arescat.MetaFisica)+'" /></td>';
                                                            for (var i_crono_arescat = 0; i_crono_arescat < cantmeses; i_crono_arescat++) {
                                                                    
                                                                if (arescat.Cronogramas[i_crono_arescat]) {
                                                                    html += '<td><input readonly type="text"'
                                                                        + 'data-recurso-id="'+ arescat.ID +'"'
                                                                        + 'data-meta-fisica="'+ arescat.MetaFisica +'"'
                                                                        + 'data-cronograma-recurso-id="'+ arescat.Cronogramas[i_crono_arescat].ID +'" class="form-control success input-number recurso_'+arescat.ID+' btn-save-cronograma-recurso" value="'
                                                                        + arescat.Cronogramas[i_crono_arescat].MetaFisica + '" /></td>';
                                                                    //Max
                                                                    //End Max
                                                                }
                                                            }
                                                        }
                                                        
                                                        //end Max
                                                        html += '</tr>';
                                                    }
                                                }
                                            }
                                        }
                                        // Totales por componente
                                        html += '</tbody>';
                                        html += '</table>';
                                        html += '</div>';
                                        html += '</div>';
                                        html += '</div>';
                                    }
                                       
                                    $('#table-container-recursos').html(html);
                                    //
                                    $('.table-responsive').css('max-height', $(window).height() * 0.60);
                                    $('.input-number').inputmask("decimal", {
                                        radixPoint: ".",
                                        groupSeparator: ",",
                                        groupSize: 3,
                                        digits: 2,
                                        integerDigits: 7,
                                        autoGroup: true,
                                        allowPlus: false,
                                        allowMinus: true,
                                        placeholder: ''
                                    }).click(function () {
                                        $(this).select();
                                    });
                                    
                                }

                                
                            </script>
                        </div>
                    </div>
                    <div class="tab-pane " id="tab-tarea">
                        <div id="container-poaf-tareas">
                            <style>
                                #container-poaf-tareas .input-number {
                                    text-align: right;
                                    min-width: 72px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }

                                #container-poaf-tareas .input-number-sm {
                                    text-align: right;
                                    min-width: 52px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }

                                #container-poaf-tareas .input-number-md {
                                    text-align: right;
                                    min-width: 82px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }
                                
                                #container-poaf-tareas table-act > tbody > tr > td:nth-child(1) {
                                    text-align: center;
                                }

                                #container-poaf-tareas .table-act > tbody > tr > td:nth-child(3) {
                                    min-width: 320px !important;
                                }

                                #container-poaf-tareas .table-act > tbody > tr > td:nth-child(2) {
                                    min-width: 100px !important;
                                }
                                
                                #container-poaf-tareas .table-act > tbody > tr > td:nth-child(4) {
                                    min-width: 100px !important;
                                }
                                #container-poaf-tareas .table-act > tbody > tr > td:nth-child(5) {
                                    min-width: 100px !important;
                                }
                                #container-poaf-tareas .table-act > tbody > tr > td:nth-child(6) {
                                    min-width: 80px !important;
                                }
                                #container-poaf-tareas .table-act > tbody > tr > td:nth-child(7) {
                                    min-width: 100px !important;
                                }
                                
                                #container-poaf-tareas .table > tbody > .tr-header > td {
                                   min-width: 50px;
                                    text-align: center;
                                    font-size: 12px;
                                    font-weight: bold;
                                    padding: 1px !important;
                                    vertical-align: middle !important;
                                    border: 1px solid #c0c0c0;
                                    background-color: #e3e3e3;
                                }

                                #container-poaf-tareas h4.panel-title {
                                    font-size: 16px;
                                }

                                #container-poaf-tareas .tr-comp td, .tr-proy td {
                                    font-weight: bold;
                                }

                                #container-poaf-tareas .table-condensed td {
                                    padding: 1px !important;
                                }

                                #container-poaf-tareas td, #table-container input {
                                    font-size: 13px !important;
                                }

                                #container-poaf-tareas .form-control[readonly] {
                                    background-color: #f2f3f4;
                                }
                                
                                .mes
                                {
                                    min-width: 50px !important;
                                }
                            </style>
                            <br>
                            <div id="table-container-tareas">
                            </div>
                            <br>
                            <div class="text-center">
                                <a class="btn btn-inverse-alt" href="<?= Yii::$app->getUrlManager()->createUrl('recursos-tarea/descargar-tarea?codigo='.$informacion->Codigo.'') ?>">
                                    <i class="fa fa-download"></i>&nbsp;Descargar Cronograma Técnico
                                </a>
                            </div> 
                            <script>
                                
                                        
                                var _proyectoTarea = new Object();
                                $(document).ready(function () {
                                    $('#table-container-tareas').html('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>');
                                    getProyectoTarea().done(function(json) {
                                        _proyectoTarea = json;
                                        _drawTableTarea();
                                    });
                                });
                                
                                function getProyectoTarea() {
                                    // var url = "<?= \Yii::$app->request->BaseUrl ?>/../json/recurso.json";
                                    var url = "<?= \Yii::$app->request->BaseUrl ?>/recursos-tarea/tareas-json?codigo=<?= $informacion->Codigo ?>";;
                                    return $.getJSON( url );
                                }
                                function _drawTableTarea() {
                                    var situacionTarea=<?= $situacionTarea ?>;
                                    var situacionProyecto=<?= $situacionProyecto ?>;
                                    var html = '';
                                    var cantmeses = _proyectoTarea.Cronogramas.length;
                                  
                                    for (var i_comp = 0; i_comp < _proyectoTarea.Componentes.length; i_comp++) {
                                        var comp = _proyectoTarea.Componentes[i_comp];
                                       
                                        html += '<div class="panel panel-gray"><div class="panel-heading">'
                                                + '<span class="pull-left"><h4 class="panel-title">'
                                                + (i_comp + 1) + '. Objetivo específico - ' + comp.Nombre
                                                + '</h4></span><span class="pull-right">Peso: '+comp.Peso+'</span></div>';
                                        html += '<div class="panel-body">';
                                        html += '<div class="table-responsive">';
                                        html += '<table class="table table-bordered table-condensed table-act">';
                                        html += '<tbody>';
                                        for (var i_act = 0; i_act < comp.Actividades.length; i_act++)
                                        {
                                            var acutotMF = 0.00;
                                            var acutotMFs = 0.00;
                                            var subacutotCat = 0.00;
                                            var subacutotCats = 0.00;
                                            var acuTotal = 0.00;
                                            var act = comp.Actividades[i_act];
                                            html += '<tr class="tr-header">';
                                            html += '<td>Peso</td><td>#</td><td>Actividad/Tarea</td><td>Unidad de medida</td><td>Meta física</td>';
                                            // Cronogramas
                                            for (var i_crono_act = 0; i_crono_act < cantmeses; i_crono_act++) {
                                                html += '<td class="mes">' + act.Cronogramas[i_crono_act].MesDescripcion + '</td>';
                                            }
                                            // Sumatorias
                                            html += '<tr class="tr-act-mf info" data-act-id="' + act.ID + '">';
                                            html += '<td align="center">'+act.Peso+'</td><td>' + (i_comp + 1) + '.' + (i_act + 1) + '</td>'
                                                    + '<td><b>' + act.Nombre + '</b></td><td>' + act.UnidadMedida
                                                    + '</td><td>'+act.MetaFisica
                                                    + '</td>';
                                            for (var i_crono_act = 0; i_crono_act < cantmeses; i_crono_act++) {
                                                html += '<td></td>';
                                                //Max
                                                acutotMF += act.Cronogramas[i_crono_act].PoafMetaFisica;
                                                //End Max
                                            }
                                            //
                                            html += '</tr>';
                                            html += '</tr>';
                                            // Rubros Elegibles
                                            html += '<tr>';
                                            html += '<td colspan="5"></td>';
                                            html += '</tr>';
                                            for (var i_are = 0; i_are < act.Tareas.length; i_are++) {
                                                var are = act.Tareas[i_are];
                                                var alertaRecurso="red";
                                                var  metaFisicaTarea1=0;
                                                for (var i_crono_are = 0; i_crono_are < cantmeses; i_crono_are++) {
                                                    metaFisicaTarea1=parseFloat(metaFisicaTarea1)+parseFloat(are.Cronogramas[i_crono_are].MetaFisica);
                                                }
                                                if (parseFloat(are.MetaFisica)==parseFloat(metaFisicaTarea1)) {
                                                    alertaRecurso="black";
                                                }
                                                
                                                html += '<tr class="tr-sub2-tot success" data-tarea-id="'+i_are+'"  data-meta-fisica="'+are.Cantidad+'">';
                                                html += '<td align="center">';
                                                var color='black';
                                                if (are.Situacion==3) {
                                                    color='red';
                                                }
                                                var perfil='<?= $seguimientosCount ?>';
                                                var rol=<?= \Yii::$app->user->identity->Rol ?>;
                                                if (situacionTarea==2 && perfil==0 && rol==5) {
                                                    html += '<span style="color:'+color+'" class="fa fa-search  generar-observacion-tarea" data-tarea-id="'+ are.ID +'"></span>'
                                                }
                                                else if (situacionTarea==2 && perfil==1 && rol==2) {
                                                    html += '<span style="color:'+color+'" class="fa fa-search  generar-observacion-tarea" data-tarea-id="'+ are.ID +'"></span>'
                                                }else if (situacionTarea==3 && are.Situacion==3) {
                                                    html +='<span data-toggle="tooltip" title="'+are.Observacion+'"><i class="fa fa-eye"></i></span>';
                                                }
                                                            
                                                html +='</td><td>' + (i_comp + 1) + '.' + (i_act + 1) + '.' + (i_are + 1) + '</td><td style="color:'+alertaRecurso+';">' + are.Descripcion + '</td><td>'+are.UnidadMedida+'</td><td><input type="text" style="color:'+alertaRecurso+';" data-tarea-id="'+are.ID+'" data-meta-fisica="'+are.MetaFisica+'" class="form-control tarea-meta-fisica" readonly value="' + are.MetaFisica+'" >'
                                                       
                                                for (var i_crono_are = 0; i_crono_are < cantmeses; i_crono_are++) {
                                                    
                                                    var cronogramaTarea=are.Cronogramas[i_crono_are];
                                                    html += '<td><input type="text" readonly data-meta-fisica="'+are.MetaFisica+'" data-tarea-id="'+are.ID+'" class="form-control input-number tarea_'+are.ID+' btn-save-cronograma-tarea input-sub2-total"  '
                                                            + 'data-cronograma-tarea-id="' + cronogramaTarea.ID + '" value="'+cronogramaTarea.MetaFisica
                                                            + '" /></td>';
                                                }
                                                html += '</tr>';
                                                
                                            }
                                        }
                                        html += '</tbody>';
                                        html += '</table>';
                                        html += '</div>';
                                        //html += '</div>';
                                        html += '</div>';
                                        html += '</div>';
                                    }
                                    $('#table-container-tareas').html(html);
                                    
                                    /*
                                    if (situacionProyecto!=0) {
                                        $("input,textarea,select").prop("disabled", true);
                                        $(".btn-actv-tarea,.btn-enviar-evaluacion-poa,.fa-edit,.fa-remove").hide();
                                    }*/
                                    /*
                                    // Totales por proyecto
                                    html += '<div class="panel panel-default">';
                                    html += '<div class="panel-body">';
                                    html += '<div class="table-responsive">';
                                    html += '<table class="table table-bordered table-condensed">';
                                    html += '<tbody>';
                                    html += '<tr class="tr-header">';
                                    html += '<td>#</td>'
                                            //+ '<td></td><td></td><td></td><td></td>'
                                            + '<td style="min-width:100px;"></td><td>Total S/.</td>';
                                    // Cronogramas
                                    for (var i_crono_proy = 0; i_crono_proy < cantmeses; i_crono_proy++) {
                                        html += '<td>Mes ' + _proyecto.Cronogramas[i_crono_proy].Mes + '</td>';
                                    }
                                    // Sumatorias
                                    html += '<td></td>';
                                    //
                                    html += '</tr>';
                                    html += '<tr class="tr-proy">';
                                    html += '<td></td>'
                                            + '<td>Total Proyecto</td>'
                                            //+ '</td><td>'
                                            //+ '</td><td>'
                                            //+ '</td><td>'
                                            + '<td><input type="text" class="form-control input-number input-number-md" readonly="" value="' + _proyecto.Total + '" /></td>';
                                    for (var i_crono_proy = 0; i_crono_proy < cantmeses; i_crono_proy++) {
                                        html += '<td><input type="text" class="form-control input-number input-number-md" readonly="" '
                                                + ' data-crono-proy-id="' + _proyecto.Cronogramas[i_crono_proy].ID
                                                + '" value="'
                                                + _proyecto.Cronogramas[i_crono_proy].PoafMetaFinanciera + '" /></td>';
                                    }
                                    // Sumatorias
                                    html += '<td></td>';
                                    //
                                    html += '</tr>';
                                    html += '</tbody>';
                                    html += '</table>';
                                    html += '</div>';
                                    html += '</div>';
                                    html += '</div>';

                                    $('#table-container').html(html);
                                    //
                                    $('.table-responsive').css('max-height', $(window).height() * 0.60);
                                     */
                                    $('.input-number').inputmask("decimal", {
                                        radixPoint: ".",
                                        groupSeparator: ",",
                                        groupSize: 3,
                                        digits: 2,
                                        integerDigits: 7,
                                        autoGroup: true,
                                        allowPlus: false,
                                        allowMinus: true,
                                        placeholder: ''
                                    }).click(function () {
                                        $(this).select();
                                    });
                                   
                                }
                            </script>
                        </div>

                   
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal-ctrl-recurso" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Detalle de la observación</h4>
                        </div>
                        <div class="modal-body-main">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal-loading" class="modal fade modal-loading in" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-hidden="false" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content-no-shadow modal-content">
            <div class="text-center"><i class="fa fa-spinner fa-pulse fa-fw" style="color:#16a085;font-size: 100px;"></i></div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-ctrl-observacion" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Observación</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>
