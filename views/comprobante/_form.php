<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => \Yii::$app->request->BaseUrl.'/comprobante/crear',
        'options'           => [
            'id'            => 'frmComprobante',
            'enctype'       =>'multipart/form-data',
        ]
    ]
); ?>
    
    <div class="modal-body">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-2 control-label">Codigo Proyecto:</label>
                <div class="col-sm-10">
                    <select class="form-control" name="ComprobantePago[CodigoProyecto]">
                        <?php foreach ($CodigoProyectoLista as $lst): ?>
                            <option value="<?php echo $lst->username; ?>"><?php echo $lst->username; ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Documento:</label>
                <div class="col-sm-10">
                    <!-- <p>Adjuntar los comprobantes de pago</p> -->
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="input-group">
                            <div class="form-control uneditable-input" data-trigger="fileinput">
                                <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                <span class="fileinput-filename">
                                Seleccione archivo...
                                </span>
                            </div>
                            <span class="input-group-addon btn btn-default btn-file">
                                <span class="fileinput-new">Seleccionar</span>
                                <span class="fileinput-exists">Cambiar</span>
                                <input type="file" onchange="return Imagen(this);" id="file-cv-administrativo" name="ComprobantePago[Archivo]">
                            </span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Eliminar</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Descripción:</label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="ComprobantePago[Descripcion]"></textarea>
                </div>
            </div>
        </div>
    </div>
    
        
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button id="btn-save-conformidad-servicios" type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>

<script>
    var $form = $('#frmConformidadPago');
    var formParsleyConformidadPago = $form.parsley(defaultParsleyForm());

</script>