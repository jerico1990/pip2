<?php if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start(); ?>
<link href="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
<script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/bootstrap-table/bootstrap-table.min.js"></script>
<script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/bootstrap-table/bootstrap-table-es-SP.min.js"></script>
<link href="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/form-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">

<link rel="stylesheet" media="all" type="text/css" href="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/timepicker/jquery-ui-timepicker-addon.css" />

<div id="page-content" style="min-height: 885px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Programaci&oacute;n experimientos</h1>
        </div>
        <div class="container">
            <div class="panel panel-primary">
                <div id="Divnotificaciones"></div>
				<div class="panel-body">
                    <div id="tblListaToolbar">
                        <button id="btn-crear-experimento" class="btn btn-primary" data-codigo-proyecto="<?= $CodigoProyecto ?>">Generar Experimento</button>
					</div>
                    <table id="tblLista" data-height="450">
						<thead>
							<tr>
								<th data-field="Item" data-width="35" data-align="center">N°</th>
								<th data-field="Titulo" data-align="left">Titulo</th>
                                <th data-field="CodigoPOA" data-width="80" data-align="center">Objetivo /<br/> Actividad</th>
								<th data-field="LugarDesarrollo" data-width="150" data-align="left">Lugar<br/>desarrollo</th>	
								<th data-field="FechaInicio" data-width="90" data-align="center">Fecha<br/>inicio</th>	
								<th data-field="FechaFin" data-width="90" data-align="center">Fecha<br/>T&eacute;rmino</th>	
								<th data-field="Probabilidad" data-width="90" data-align="center">Probabilidad<br/>termino plazo</th>	
								<th data-formatter="tblLista_editar" data-events="tblLista_events" data-width="35" data-align="center">&nbsp;</th>	
								<th data-formatter="tblLista_eliminar" data-events="tblLista_events" data-width="35" data-align="center">&nbsp;</th>
							</tr>
						</thead>
					</table>
				</div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-ctrl-crear-experimento" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Experimentos</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer" style="margin-top: 0px;">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button id="btn-guardar-experimento" type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
            </div>
        </div>
    </div>
</div>
<script>
	window.tblLista_events = {
        'click .tblLista-editar': function (e, value, row, index) {
            var CodigoProyecto = $('#btn-crear-experimento').data('codigo-proyecto');
            AjaxSendForm('<?= \Yii::$app->request->BaseUrl ?>/programacion-experimento-new/editar'
                , "#modal-ctrl-crear-experimento .modal-body"
                , {'CodigoProyecto':CodigoProyecto, 'IdProgramacionTecnica':row['IdProgramacionTecnica']}
                , false
                , function(estado, datos, msj){
                    $('#modal-ctrl-crear-experimento').modal('hide');
                    if(estado == 1){
                        alerta_sistema('#Divnotificaciones','success', msj);
                        cargar_grilla();
                    } else {
                        alerta_sistema('#Divnotificaciones','danger', msj);
                    }
                }
            );
            $('#modal-ctrl-crear-experimento').modal('show');
            $('#btn-guardar-experimento').removeAttr('disabled');
        },
        'click .tblLista-eliminar': function (e, value, row, index) {
            $.ajax({
                url: '<?= Yii::$app->getUrlManager()->createUrl('/programacion-experimento-new/anular'); ?>',
                type: 'POST',
                async: false,
                data: { 
                    ProgramacionTecnica: { 'IdProgramacionTecnica': row['IdProgramacionTecnica'] }, 
                    _csrf: '<?=Yii::$app->request->getCsrfToken()?>' 
                },
                success: function(result){
                    result = JSON.parse(result);
                    if(result['Estado'] == 1){
                        alerta_sistema('#Divnotificaciones','success', result['Msj']);
                        cargar_grilla();
                    } else {
                        alerta_sistema('#Divnotificaciones','danger', result['Msj']);
                    }
                }
            });
        }
    };
	
	function tblLista_editar(value, row, index) {
		return '<a class="tblLista-editar" href="#"><span class="fa fa-pencil"></span></a>';
	}
	function tblLista_eliminar(value, row, index) {
		return '<a class="tblLista-eliminar" href="#"><span class="fa fa-trash"></span></a>';
	}
	
    function cargar_grilla(){
        $.ajax({
            url:'<?= \Yii::$app->request->BaseUrl ?>/programacion-experimento-new/lista-experimentos-json',
            async:true,
            data:{},
            contentType: "application/json",
            beforeSend:function()
            {
                $('#tblLista').bootstrapTable('showLoading');
            },
            success:function(result)
            {
                //console.log(result);
                //result = JSON.parse(result);
                //console.log(result.length);
                $('#tblLista').bootstrapTable('load', result);
            },
            error:function(){   
                alert('Error al realizar el proceso de busqueda.');
            },
            complete: function (){
                $('#tblLista').bootstrapTable('hideLoading');
            }
        });        
    }
	$(document).ready(function(){

        $('#tblLista').bootstrapTable({
            cache: false,
            striped: true,
            pagination: false,
            search: true,
			classes: 'table table-condensed',
            toolbar: '#tblListaToolbar'
		});
		
		$('#modal-ctrl-crear-experimento').modal({ backdrop:'static', keyboard:false, show:false });
		
		cargar_grilla();

        $('#btn-crear-experimento').click(function(){
            //alert('btn-crear-experimento : click ');
            var CodigoProyecto = $(this).data('codigo-proyecto');
            AjaxSendForm('<?= \Yii::$app->request->BaseUrl ?>/programacion-experimento-new/nuevo'
                , "#modal-ctrl-crear-experimento .modal-body"
                , {'CodigoProyecto':CodigoProyecto}
                , false
                , function(estado, datos, msj){
                    $('#modal-ctrl-crear-experimento').modal('hide');
                    if(estado == 1){
                        alerta_sistema('#Divnotificaciones','success', msj);
                        cargar_grilla();
                    } else {
                        alerta_sistema('#Divnotificaciones','danger', msj);
                    }

                }
            );
            $('#modal-ctrl-crear-experimento').modal('show');
            $('#btn-guardar-experimento').removeAttr('disabled');
        });
	});

    function AjaxSendForm(url, placeholder,data, append, callback_postsave = null) {
        append = (append | false); 
        $.ajax({
            cache: false,
            type: 'GET',
            url: url,
            data: data,
            beforeSend: function() {
                // setting a timeout
                $(placeholder).empty()
                    .append('<div class="loading"><i class="fa fa-spinner fa-pulse fa-5x fa-fw"></i></div>');
            },
            success: function(data) {
                $(placeholder).html("");
                if (append) {
                    $(placeholder).append(data);
                } else {
                    $(placeholder).html(data);
                    if(callback_postsave != null){
                        postSave_event = callback_postsave;
                    }
                }
            },
            error: function(xhr) { // if error occured
                alert("Error occured.please try again");
                $(placeholder).append(xhr.statusText + xhr.responseText);
            },
            complete: function() { $(".loading").remove(); },
            dataType: 'html'
        });
    }
    
</script>
