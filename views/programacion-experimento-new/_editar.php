<?php
use yii\helpers;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>



<?php $form = ActiveForm::begin(
    [
        'action'            => '',
        'options'           => [
            'enctype'       => 'multipart/form-data',
            'id'            => 'frmExperimiento',
            'class'         => 'form-horizontal'
        ]
    ]
); ?>    
    <input type="hidden" id="hddCP" value="<?= $CodigoProyecto ?>">
    <input type="hidden" id="hddIdProgramacion" value="<?= $experimento['IdProgramacionTecnica'] ?>">
    <div class="form-group">
        <label class="col-sm-2 col-md-2 col-lg-2 control-label">Objetivo:<span class="f_req">*</span></label>
        <div class="col-sm-9col-sm-10 col-md-10 col-lg-10">
            <select class="form-control input-sm" name="ProgramacionTecnica[ComponenteID]" id="experimento-componenteid" onchange="Actividad(this.value);" required>
                <option value="">[SELECCIONE]</option>
                <?php foreach($componentes as $componente){ ?>
                    <option value="<?= $componente->ID ?>"><?= $componente->Correlativo ?>.- <?= $componente->Nombre ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 col-md-2 col-lg-2 control-label">Actividad:<span class="f_req">*</span></label>
        <div class="col-sm-9col-sm-10 col-md-10 col-lg-10">
            <select class="form-control input-sm" name="ProgramacionTecnica[ActividadID]" id="experimento-actividadid" required>
                <option value="">[SELECCIONE]</option>
            </select>
        </div>
    </div>
	<div class="form-group">
        <label class="col-sm-2 col-md-2 col-lg-2 control-label">Titulo:<span class="f_req">*</span></label>
		<div class="col-sm-10 col-md-10 col-lg-10">
            <input type="text" class="form-control input-sm" name="ProgramacionTecnica[Titulo]" id="experimento-titulo" required "/>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 col-md-2 col-lg-2 control-label">Tipología:<span class="f_req">*</span></label>
        <div class="col-sm-10 col-md-10 col-lg-10">
            <select class="form-control input-sm" name="ProgramacionTecnica[Tipologia]" id="experimento-tipologia" required>
                <option value="">[SELECCIONE]</option>
                <option value="1">Experimentos de investigación de cultivos priorizado</option>
                <option value="2">Experimentos de investigación de crianzas priorizadas</option>
                <option value="3">Experimentos biotecnológicos.</option>
                <option value="4">Accesiones mantenidas en los Bancos de Germoplasmas (indicar en observaciones la cantidad)</option>
                <option value=5>Experimentos de investigación en cambio climático</option>
                <option value="6">Análisis socio-económicos, de mercados, TT y sistemas de producción</option>
                <option value="7">Experimentos en investigación en post-cosecha</option>
                <option value="8">Variedad liberada / lanzamiento de cultivar mejorado</option>
                <option value="9">Tecnología liberada / lanzamiento de tecnología productiva</option>
                <option value="10">Experimentos incluyan una publicación</option>
            </select>
        </div>
    </div>
	<div class="form-group">
        <label class="col-sm-2 col-md-2 col-lg-2 control-label">Descripci&oacute;n:<span class="f_req">*</span></label>
		<div class="col-sm-10 col-md-10 col-lg-10">
            <textarea class="form-control input-sm" name="ProgramacionTecnica[Descripcion]" id="experimento-descripcion" required ></textarea>
        </div>
    </div>
	<div class="form-group">
        <label class="col-sm-2 col-md-2 col-lg-2 control-label">Lugar:<span class="f_req">*</span></label>
		<div class="col-sm-10 col-md-10 col-lg-10">
            <input type="text" class="form-control input-sm" name="ProgramacionTecnica[Lugar]" id="experimento-lugar" required />
        </div>
    </div>
	<div class="form-group">
        <label class="col-sm-2 col-md-2 col-lg-2 control-label">Fecha Inicio:<span class="f_req">*</span></label>
		<div class="col-sm-2 col-md-2 col-lg-2">
            <div class="input-group input-group-sm">
                <input type="text" class="form-control fechas" name="ProgramacionTecnica[FechaInicio]" id="experimento-fechainicio" required maxlength="10" />
                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
            </div>
        </div>
        <label class="col-sm-1 col-md-1 col-lg-1 control-label">Fin:<span class="f_req">*</span></label>
		<div class="col-sm-2 col-md-2 col-lg-2">
            <div class="input-group input-group-sm">
                <input type="text" class="form-control fechas" name="ProgramacionTecnica[FechaFin]" id="experimento-fechafin" required maxlength="10" />    
                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
            </div>
        </div>
        <label class="col-sm-3 col-md-3 col-lg-3 control-label">Probabilidad de termino en plazo:<span class="f_req">*</span></label>
		<div class="col-sm-2 col-md-2 col-lg-2">
            <select class="form-control input-sm" name="ProgramacionTecnica[Probabilidad]" id="experimento-probabilidad" required>
                <option value="">[SELECCIONE]</option>
                <option value=1>Nula</option>
                <option value=2>Baja</option>
                <option value=3>Media</option>
                <option value=4>Alta</option>
            </select>
        </div>
    </div>
	<div class="form-group" style="margin-bottom: 0px;">
        <label class="col-sm-2 col-md-2 col-lg-2 control-label">Observaci&oacute;n:</label>
		<div class="col-sm-10 col-md-10 col-lg-10">
            <textarea class="form-control input-sm" name="ProgramacionTecnica[Observacion]" id="experimento-observacion"></textarea>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">
    var formParsleyViatico = $('#frmExperimiento').parsley(defaultParsleyForm2()); 
    var postSave_event = null; // setear esta variable con la funcion que se ejecutara al guardar. function (estado, data, mensaje)

    function Actividad(valor, selectValue = null) {
        var ComponenteID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= Yii::$app->getUrlManager()->createUrl('termino-referencia/actividades'); ?>',
            type: 'POST',
            async: false,
            data: {ComponenteID:ComponenteID,_csrf: toke},
            success: function(data){
                $("#experimento-actividadid").html(data);
                if(selectValue != null)
                    $("#experimento-actividadid").val(selectValue);
            }
        });
    }

    $('#experimento-titulo, #experimento-descripcion, #experimento-lugar, #experimento-observacion').filter_input_alfanumericos_validate();
    $('#experimento-fechainicio, #experimento-fechafin').filter_input_fecha_validate();

    $('#experimento-fechainicio').datepicker({
        language: 'es',
        format: 'dd/mm/yyyy',
        autoclose: true
    }).on('changeDate', function (ev) {
        $('#experimento-fechafin').datepicker('setStartDate', ev.date)
    });
    $('#experimento-fechafin').datepicker({
        language: 'es',
        format: 'dd/mm/yyyy',
        autoclose: true
    }).on('changeDate', function (ev) {
        $('#experimento-fechainicio').datepicker('setEndDate', ev.date);
    });
    $(document).off('click').on('click','#btn-guardar-experimento',function(){
        if(formParsleyViatico.validate()) {    
            $(this).attr('disabled', 'disabled');
            var datos = { 
                ProgramacionTecnica: { 
                    'IdProgramacionTecnica': $('#hddIdProgramacion').val(),
                    'ComponenteID': $('#experimento-componenteid').val(),
                    'ActividadID': $('#experimento-actividadid').val(),
                    'Titulo': $('#experimento-titulo').val(),
                    'IntroduccionDescripcion': $('#experimento-descripcion').val(),
                    'LugarDesarrollo': $('#experimento-lugar').val(),
                    'FechaInicio': $('#experimento-fechainicio').datepicker('getFormattedDate','yyyy-mm-dd'),
                    'FechaFin': $('#experimento-fechafin').datepicker('getFormattedDate','yyyy-mm-dd'),
                    'Probabilidad': $('#experimento-probabilidad').val(),
                    'Tipologia': $('#experimento-tipologia').val(),
                    'ObservacionInvestigador': $('#experimento-observacion').val()
                },
                _csrf : $('input:hidden[name="_csrf"]').val()
            };
            
            $.ajax({
                url: '<?= Yii::$app->getUrlManager()->createUrl('programacion-experimento-new/registrar'); ?>',
                data: datos,
                type: 'POST',
                beforeSend:function(){
                },
                success:function(result)
                {
                    result = JSON.parse(result);
                    if(postSave_event != null){
                        postSave_event(result['Estado'], result['Data'], result['Msj']);
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso de busqueda.');
                },
                complete: function (){
                }
            });
        }
        
    });
    <?php if($experimento['IdProgramacionTecnica'] > 0){  ?>
        $("#experimento-componenteid").val('<?= $experimento['ComponenteID'] ?>');
        Actividad('<?= $experimento['ComponenteID'] ?>', '<?= $experimento['ActividadID'] ?>');
        $("#experimento-titulo").val('<?= $experimento['Titulo'] ?>');

        $("#experimento-descripcion").html('<?= preg_replace("[\n|\r|\n\r]", '<br />', $experimento['IntroduccionDescripcion']) ?>');
        $("#experimento-lugar").val('<?= $experimento['LugarDesarrollo'] ?>');
        $("#experimento-fechainicio").datepicker('setDate', '<?= $experimento['FechaInicio'] ?>');
        $("#experimento-fechafin").datepicker('setDate', '<?= $experimento['FechaFin'] ?>');
        $("#experimento-probabilidad").val('<?= $experimento['Probabilidad'] ?>');
        $("#experimento-tipologia").val('<?= $experimento['Tipologia'] ?>');
        $("#experimento-observacion").html('<?= preg_replace("[\n|\r|\n\r]", '<br />', $experimento['ObservacionInvestigador']) ?>');

    <?php } ?>
</script>