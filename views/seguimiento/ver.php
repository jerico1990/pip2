<div id="page-content" style="min-height: 754px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Seguimiento del Proyecto</h1>
        </div>
        <div class="container">
            <style>
                .vertical{
                    vertical-align: middle;
                }   
                .panel .panel-body {
                    padding: 10px !important;
                }

                .panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 1px solid #a8a8ab !important;
                }

                .panel-gray .panel-body {
                    border-top: none !important;
                }

                .btn-sm {
                    padding: 5px 10px !important;
                    line-height: 1.5 !important;
                }

                .input-comp-nombre {
                    text-transform: uppercase;
                }

                .tbl-act > tbody > .tr-header > td {
                    text-align: center;
                    font-size: 12px;
                    font-weight: bold;
                    padding: 5px !important;
                    vertical-align: middle !important;
                    /*border: 1px solid #cfcfd0;
                    background-color: #f0f0f1;*/
                    border: 1px solid #c0c0c0;
                    background-color: #e3e3e3;
                }

                /*.tbl-act .tr-item td {
                    padding: 2px !important;
                }*/
                input.form-control, select.form-control {
                    /*height: 28px !important;*/
                }

                .tbl-act .tr-act .input-nombre {
                    min-width: 320px !important;
                }

                .tbl-act .tr-act .input-unidadmedida {
                    min-width: 100px !important;
                }

                .input-number {
                    min-width: 92px !important;
                }

                .btn-add-re {
                    float: right;
                }

                /*.tbl-act > tbody > tr > td:nth-child(2) {
                    min-width: 40px !important;
                    text-align: center !important;
                }*/
                .text-left {
                    text-align: left !important;
                    font-weight: bold;
                }

                .tr-comp input, .tr-proy input {
                    font-weight: bold;
                }

                .th-proy {
                    min-width: 630px !important;
                }
            </style>

            <div class="panel panel-primary">
                <!-- <div class="panel-heading">
                    <h4>OBJETIVO</h4>
                </div> -->
                <div class="panel-body">
                    <div class="row">
                        <div class="container">
                            <div class="table-responsive" style="max-height: 800px;">
                                <hr>
                                <table class="table table-bordered table-condensed tbl-act">
                                    <tbody>
                                        <tr class="tr-header">
                                            <td class="text-center">Estación</td>
                                            <td class="text-center">Solicitud</td>
                                            <td class="text-center">Memo</td>
                                            <td class="text-center">Tipo</td>
                                            <td class="text-center">Fecha Inicio</td>
                                            <!-- <td class="text-center">Fecha Fin</td> -->
                                        </tr>
                                        <tr>
                                            <td class="text-center"> <?php echo $segui['EstacionNombre'] ?></td>
                                            <td class="text-center"> <?php echo $segui['SolicitudCodigo'] ?></td>
                                            <td class="text-center"> <?php echo $segui['SolicitudMemorando'] ?></td>
                                            <td class="text-center"> <?php echo $segui['OrdenTipo'] ?></td>
                                            <td class="text-center"> <?php echo $segui['OrdenFecha'] ?></td>
                                            
                                        </tr>
                                    </tbody>
                                </table>

                                <hr>
                                <table class="table table-bordered table-condensed tbl-act">
                                    <tbody>
                                        <tr class="tr-header">
                                            <td class="text-center">Nº</td>
                                            <td class="text-center">Actividad</td>
                                            <td class="text-center">Responsable</td>
                                            <td class="text-center">Fecha Inicio</td>
                                            <td class="text-center">Fecha Fin</td>
                                            <td class="text-center">Días</td>
                                            <td class="text-center">Alerta</td>
                                        </tr>
                                        
                                        <tr>
                                            <td class="text-center">1</td>
                                            <td class="text-left">Realiza el requerimiento</td>
                                            <td class="text-left">IRP</td>
                                            <td class="text-center"><?php echo Yii::$app->tools->dateFormat($requerimiento->FechaSolicitud) ?></td>
                                            <td class="text-center"><?php echo Yii::$app->tools->dateFormat($requerimiento->FechaSolicitud) ?></td>
                                            <td class="text-center"><?php echo trim(explode(' ',strip_tags(Yii::$app->tools->RestarFechas($requerimiento->FechaSolicitud,$requerimiento->FechaSolicitud),'<ul><li><p>'))[0]);  ?></td>
                                            <td class="text-center" style="background: <?php echo Yii::$app->tools->colorSeg(1) ?>"></td>
                                        </tr>


                                        <tr>
                                            <td class="text-center">2</td>
                                            <td class="text-left">Evalua requerimiento</td>
                                            <td class="text-left">Director EEA</td>
                                            <?php if(!empty($segui['DirectorEEA'])):?>
                                                <td class="text-center"><?php echo Yii::$app->tools->valoreVacion(explode('=', $segui['DirectorEEA'])[0]); ?></td>
                                                <td class="text-center"><?php echo Yii::$app->tools->valoreVacion(explode('=', $segui['DirectorEEA'])[1]) ?></td>
                                                <td class="text-center"><?php echo Yii::$app->tools->vacioDias(explode('=', $segui['DirectorEEA'])[2]) ?></td>
                                            <?php else: ?>
                                                <td class="text-center"></td>
                                                <td class="text-center"></td>
                                                <td class="text-center"></td>
                                            <?php endif ?>
                                            <td class="text-center" style="background: <?php echo Yii::$app->tools->colorSeg($segui['DirectorEEA_Color']) ?>"></td>
                                        </tr>


                                        <tr>
                                            <td class="text-center">3</td>
                                            <td class="text-left">Genera la solicitud de compromiso</td>
                                            <td class="text-left">IRP</td>
                                            <td class="text-center"><?php echo Yii::$app->tools->dateFormat($cabecera->FechaRegistro) ?></td>
                                            <td class="text-center"><?php echo Yii::$app->tools->dateFormat($cabecera->FechaRegistro) ?></td>
                                            <td class="text-center"><?php echo trim(explode(' ',strip_tags(Yii::$app->tools->RestarFechas($cabecera->FechaRegistro,$cabecera->FechaRegistro),'<ul><li><p>'))[0]);  ?></td>
                                            <td class="text-center" style="background: <?php echo Yii::$app->tools->colorSeg(1) ?>"></td>
                                        </tr>



                                        <?php $esp = Yii::$app->tools->seguimiento($CodigoProyecto,$ID,2); ?>
                                        <tr>
                                            <td class="text-center">4</td>
                                            <td class="text-left">Evalua la elegibilidad de la solicitud de compromiso</td>
                                            <td class="text-left">Especialista Técnico <?php echo ' - '.$esp['username'] ?></td>
                                            <?php if(!empty($segui['EspecialistaUAFSI'])):?>
                                                <td class="text-center"><?php echo Yii::$app->tools->valoreVacion(explode('=', $segui['EspecialistaUAFSI'])[0]) ?></td>
                                                <td class="text-center"><?php echo Yii::$app->tools->valoreVacion(explode('=', $segui['EspecialistaUAFSI'])[1]) ?></td>
                                                <td class="text-center"><?php echo Yii::$app->tools->vacioDias(explode('=', $segui['EspecialistaUAFSI'])[2]) ?></td>
                                            <?php else: ?>
                                                <td class="text-center"></td>
                                                <td class="text-center"></td>
                                                <td class="text-center"></td>
                                            <?php endif ?>

                                            <td class="text-center" style="background: <?php echo Yii::$app->tools->colorSeg($segui['EspecialistaUAFSI_Color']) ?>"></td>
                                        </tr>

                                        <?php $seg = Yii::$app->tools->seguimiento($CodigoProyecto,$ID,15); ?>
                                        <tr>
                                            <td class="text-center">5</td>
                                            <td class="text-left">Evalua la elegibilidad de la solicitud de compromiso</td>
                                            <td class="text-left">Especialista S&E <?php echo ' - '.$seg['username'] ?></td>
                                            <?php if(!empty($segui['EspecialistaPacUAFSI'])):?>
                                                <td class="text-center"><?php echo Yii::$app->tools->valoreVacion(explode('=', $segui['EspecialistaPacUAFSI'])[0]) ?></td>
                                                <td class="text-center"><?php echo Yii::$app->tools->valoreVacion(explode('=', $segui['EspecialistaPacUAFSI'])[1]) ?></td>
                                                <td class="text-center"><?php echo Yii::$app->tools->vacioDias(explode('=', $segui['EspecialistaPacUAFSI'])[2]) ?></td>
                                            <?php else: ?>
                                                <td class="text-center"></td>
                                                <td class="text-center"></td>
                                                <td class="text-center"></td>
                                            <?php endif ?>
                                            <td class="text-center" style="background: <?php echo Yii::$app->tools->colorSeg($segui['EspecialistaPacUAFSI_Color']) ?>"></td>
                                        </tr>

                                        <?php $jefe = Yii::$app->tools->seguimiento($CodigoProyecto,$ID,11); ?>
                                        <tr>
                                            <td class="text-center">6</td>
                                            <td class="text-left">Evalua la elegibilidad de la solicitud de compromiso</td>
                                            <td class="text-left">Jefe UAFSI <?php echo ' - '.$jefe['username'] ?></td>
                                            <?php if(!empty($segui['JefeUAFSI'])):?>
                                                <td class="text-center"><?php echo Yii::$app->tools->valoreVacion(explode('=', $segui['JefeUAFSI'])[0]) ?></td>
                                                <td class="text-center"><?php echo Yii::$app->tools->valoreVacion(explode('=', $segui['JefeUAFSI'])[1]) ?></td>
                                                <td class="text-center"><?php echo Yii::$app->tools->vacioDias(explode('=', $segui['JefeUAFSI'])[2]) ?></td>
                                            <?php else: ?>
                                                <td class="text-center"></td>
                                                <td class="text-center"></td>
                                                <td class="text-center"></td>
                                            <?php endif ?>
                                            <td class="text-center" style="background: <?php echo Yii::$app->tools->colorSeg($segui['JefeUAFSI_Color']) ?>"></td>
                                        </tr>

                                        <?php $jefe = Yii::$app->tools->seguimiento($CodigoProyecto,$ID,12); ?>
                                        <tr>
                                            <td class="text-center">7</td>
                                            <td class="text-left">Evalua la elegibilidad de la solicitud de compromiso</td>
                                            <td class="text-left">Jefe UA <?php echo ' - '.$jefe['username'] ?></td>
                                            <?php if(!empty($segui['jefeAdministrativo'])):?>
                                                <td class="text-center"><?php echo Yii::$app->tools->valoreVacion(explode('=', $segui['jefeAdministrativo'])[0]) ?></td>
                                                <td class="text-center"><?php echo Yii::$app->tools->valoreVacion(explode('=', $segui['jefeAdministrativo'])[1]) ?></td>
                                                <td class="text-center"><?php echo Yii::$app->tools->vacioDias(explode('=', $segui['jefeAdministrativo'])[2]) ?></td>
                                            <?php else: ?>
                                                <td class="text-center"></td>
                                                <td class="text-center"></td>
                                                <td class="text-center"></td>
                                            <?php endif ?>
                                            <td class="text-center" style="background: <?php echo Yii::$app->tools->colorSeg($segui['jefeAdministrativo_Color']) ?>"></td>
                                        </tr>

                                        <?php $jefe = Yii::$app->tools->seguimiento($CodigoProyecto,$ID,13); ?>
                                        <tr>
                                            <td class="text-center">8</td>
                                            <td class="text-left">Evalua la elegibilidad de la solicitud de compromiso</td>
                                            <td class="text-left">Jefe de Contabilidad <?php echo ' - '.$jefe['username'] ?></td>
                                            <?php if(!empty($segui['JefeContabilidad'])):?>
                                                <td class="text-center"><?php echo Yii::$app->tools->valoreVacion(explode('=', $segui['JefeContabilidad'])[0]) ?></td>
                                                <td class="text-center"><?php echo Yii::$app->tools->valoreVacion(explode('=', $segui['JefeContabilidad'])[1]) ?></td>
                                                <td class="text-center"><?php echo Yii::$app->tools->vacioDias(explode('=', $segui['JefeContabilidad'])[2]) ?></td>
                                            <?php else: ?>
                                                <td class="text-center"></td>
                                                <td class="text-center"></td>
                                                <td class="text-center"></td>
                                            <?php endif ?>
                                            <td class="text-center" style="background: <?php echo Yii::$app->tools->colorSeg($segui['JefeContabilidad_Color']) ?>"></td>
                                        </tr>

                                        <?php $jefe = Yii::$app->tools->seguimiento($CodigoProyecto,$ID,14); ?>
                                        <tr>
                                            <td class="text-center">9</td>
                                            <td class="text-left">Evalua la elegibilidad de la solicitud de compromiso</td>
                                            <td class="text-left">Especialista administrativo <?php echo ' - '.$jefe['username'] ?></td>
                                            <?php if(!empty($segui['EspecialistaContabilidad'])):?>
                                                <td class="text-center"><?php echo Yii::$app->tools->valoreVacion(explode('=', $segui['EspecialistaContabilidad'])[0]) ?></td>
                                                <td class="text-center"><?php echo Yii::$app->tools->valoreVacion(explode('=', $segui['EspecialistaContabilidad'])[1]) ?></td>
                                                <td class="text-center"><?php echo Yii::$app->tools->vacioDias(explode('=', $segui['EspecialistaContabilidad'])[2]) ?></td>
                                            <?php else: ?>
                                                <td class="text-center"></td>
                                                <td class="text-center"></td>
                                                <td class="text-center"></td>
                                            <?php endif ?>
                                            <td class="text-center" style="background: <?php echo Yii::$app->tools->colorSeg($segui['EspecialistaContabilidad_Color']) ?>"></td>
                                        </tr>

                                        <tr>
                                            <td class="text-center">10</td>
                                            <td class="text-left">Conformidad del requerimiento</td>
                                            <td class="text-left">Especialista administrativo <?php echo ' - '.$jefe['username'] ?></td>
                                            <?php if(!empty($segui['EspecialistaContabilidad'])):?>
                                                <td class="text-center"><?php echo Yii::$app->tools->valoreVacion(explode('=', $segui['EspecialistaContabilidad'])[0]) ?></td>
                                                <td class="text-center"><?php echo Yii::$app->tools->valoreVacion(explode('=', $segui['EspecialistaContabilidad'])[1]) ?></td>
                                                <td class="text-center"><?php echo Yii::$app->tools->vacioDias(explode('=', $segui['EspecialistaContabilidad'])[2]) ?></td>
                                            <?php else: ?>
                                                <td class="text-center"></td>
                                                <td class="text-center"></td>
                                                <td class="text-center"></td>
                                            <?php endif ?>
                                            <td class="text-center" style="background: <?php echo Yii::$app->tools->colorSeg($segui['EspecialistaContabilidad_Color']) ?>"></td>
                                        </tr>

                                        <tr>
                                            <td class="text-center">11</td>
                                            <td class="text-left">Generar devengado</td>
                                            <td class="text-left">Especialista administrativo</td>
                                            <?php if(!empty($segui['SiafDevengado'])):?>
                                                <td class="text-center"><?php echo Yii::$app->tools->valoreVacion(explode('=', $segui['SiafDevengado'])[0]) ?></td>
                                                <td class="text-center"><?php echo Yii::$app->tools->valoreVacion(explode('=', $segui['SiafDevengado'])[1]) ?></td>
                                                <td class="text-center"><?php echo Yii::$app->tools->vacioDias(explode('=', $segui['SiafDevengado'])[2]) ?></td>
                                            <?php else: ?>
                                                <td class="text-center"></td>
                                                <td class="text-center"></td>
                                                <td class="text-center"></td>
                                            <?php endif ?>
                                            <td class="text-center" style="background: <?php echo Yii::$app->tools->colorSeg($segui['SiafDevengado_Color']) ?>"></td>
                                        </tr>

                                        <tr>
                                            <td class="text-center">12</td>
                                            <td class="text-left">Generar Girado</td>
                                            <td class="text-left">Especialista administrativo</td>
                                            <?php if(!empty($segui['SiafGirado'])):?>
                                                <td class="text-center"><?php echo Yii::$app->tools->valoreVacion(explode('=', $segui['SiafGirado'])[0]) ?></td>
                                                <td class="text-center"><?php echo Yii::$app->tools->valoreVacion(explode('=', $segui['SiafGirado'])[1]) ?></td>
                                                <td class="text-center"><?php echo Yii::$app->tools->vacioDias(explode('=', $segui['SiafGirado'])[2]) ?></td>
                                            <?php else: ?>
                                                <td class="text-center"></td>
                                                <td class="text-center"></td>
                                                <td class="text-center"></td>
                                            <?php endif ?>
                                            <td class="text-center" style="background: <?php echo Yii::$app->tools->colorSeg($segui['SiafGirado_Color']) ?>"></td>
                                        </tr>

                                        <tr>
                                            <td class="text-center">12</td>
                                            <td class="text-left">Carga de comprobante de pago</td>
                                            <td class="text-left">Especialista administrativo</td>
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                            <td class="text-center"></td>
                                            <td class="text-center" style="background: <?php echo Yii::$app->tools->colorSeg(0) ?>"></td>
                                        </tr>


                                    </tbody>
                                </table>
                            </div>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>


