<?php if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start(); ?>
<link href="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
<script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/bootstrap-table/bootstrap-table.min.js"></script>
<script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/bootstrap-table/bootstrap-table-es-SP.min.js"></script>
<link href="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/form-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">

<link rel="stylesheet" media="all" type="text/css" href="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/timepicker/jquery-ui-timepicker-addon.css" />

<div id="page-content" style="min-height: 885px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Ejecuci&oacute;n experimientos</h1>
        </div>
        <div class="container">
            <div class="panel panel-primary">
                <div id="Divnotificaciones"></div>
				<div class="panel-body">
                    <div id="tblListaToolbar">
                        <button id="btn-crear-ejecucion" class="btn btn-primary" data-codigo-proyecto="<?= $CodigoProyecto ?>">Generar ejecuci&oacute;n</button>
					</div>
                    <table id="tblLista" data-height="450">
						<thead>
							<tr>
								<th data-field="Item" data-width="35" data-align="center">N°</th>
								<th data-field="Titulo" data-align="left" data-halign="center">Titulo</th>
                                <th data-field="FechaInicio" data-width="90" data-align="center">Fecha<br/>inicio</th>  
                                <th data-field="ResponsableTecnico" data-width="300" data-halign="center">Responsable T&eacute;cnico</th>
								<th data-field="Costo" data-width="90" data-align="right" data-halign="center">Costo</th>	
								<th data-formatter="tblLista_editar" data-events="tblLista_events" data-width="35" data-align="center">&nbsp;</th>	
								<th data-formatter="tblLista_eliminar" data-events="tblLista_events" data-width="35" data-align="center">&nbsp;</th>
							</tr>
						</thead>
					</table>
				</div>
            </div>
        </div>
    </div>
</div>
<script>
	window.tblLista_events = {
        'click .tblLista-editar': function (e, value, row, index) {
            $('body').append('<form id="frmEditar" method="post" action="<?= \Yii::$app->request->BaseUrl ?>/ejecucion-experimento-new/editar"><input type="hidden" name="IdEjecucionTecnica" value="'+row['IdProgramacionTecnica']+'"/><input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>"/></form>');
            $('#frmEditar').submit();
        },
        'click .tblLista-eliminar': function (e, value, row, index) {
            $.ajax({
                url: '<?= Yii::$app->getUrlManager()->createUrl('/ejecucion-experimento-new/anular'); ?>',
                type: 'POST',
                async: false,
                data: { 
                    ProgramacionTecnica: { 'IdProgramacionTecnica': row['IdProgramacionTecnica'] }, 
                    _csrf: '<?=Yii::$app->request->getCsrfToken()?>' 
                },
                success: function(result){
                    result = JSON.parse(result);
                    if(result['Estado'] == 1){
                        alerta_sistema('#Divnotificaciones','success', result['Msj']);
                        cargar_grilla();
                    } else {
                        alerta_sistema('#Divnotificaciones','danger', result['Msj']);
                    }
                }
            });
        }
    };
	
	function tblLista_editar(value, row, index) {
		return '<a class="tblLista-editar" href="#"><span class="fa fa-pencil"></span></a>';
	}
	function tblLista_eliminar(value, row, index) {
		return '<a class="tblLista-eliminar" href="#"><span class="fa fa-trash"></span></a>';
	}
	
    function cargar_grilla(){
        $.ajax({
            url:'<?= \Yii::$app->request->BaseUrl ?>/ejecucion-experimento-new/lista-experimentos-json',
            async:true,
            data:{},
            contentType: "application/json",
            beforeSend:function()
            {
                $('#tblLista').bootstrapTable('showLoading');
            },
            success:function(result)
            {
                result = JSON.parse(result);
                $('#tblLista').bootstrapTable('load', result);
            },
            error:function(){   
                alert('Error al realizar el proceso de busqueda.');
            },
            complete: function (){
                $('#tblLista').bootstrapTable('hideLoading');
            }
        });        
    }
	$(document).ready(function(){

        $('#tblLista').bootstrapTable({
            cache: false,
            striped: true,
            pagination: false,
            search: true,
			classes: 'table table-condensed',
            toolbar: '#tblListaToolbar'
		});
		
		cargar_grilla();

        $('#btn-crear-ejecucion').click(function(){
            var formulario = $('<form method="post" action="<?= \Yii::$app->request->BaseUrl ?>/ejecucion-experimento-new/nuevo"></form>')
            .append('<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>"/>')
            .append('<input type="hidden" name="CodigoProyecto" value="'+$(this).data('codigo-proyecto')+'"/>');

            $('body').append(formulario);
            formulario.submit();
        });
	});
</script>
