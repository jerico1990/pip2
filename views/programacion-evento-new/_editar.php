<?php
use yii\helpers;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>



<?php $form = ActiveForm::begin(
    [
        'action'            => '',
        'options'           => [
            'enctype'       => 'multipart/form-data',
            'id'            => 'frmEvento',
            'class'         => 'form-horizontal'
        ]
    ]
); ?>    
    <input type="hidden" id="hddCP" value="<?= $CodigoProyecto ?>">
    <input type="hidden" id="hddIdProgramacion" value="<?= $experimento['IdProgramacionTecnica'] ?>">
    <div class="form-group">
        <label class="col-sm-2 col-md-2 col-lg-2 control-label">Objetivo:<span class="f_req">*</span></label>
        <div class="col-sm-9col-sm-10 col-md-10 col-lg-10">
            <select class="form-control input-sm" name="ProgramacionTecnica[ComponenteID]" id="evento-componenteid" onchange="Actividad(this.value);" required>
                <option value="">[SELECCIONE]</option>
                <?php foreach($componentes as $componente){ ?>
                    <option value="<?= $componente->ID ?>"><?= $componente->Correlativo ?>.- <?= $componente->Nombre ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 col-md-2 col-lg-2 control-label">Actividad:<span class="f_req">*</span></label>
        <div class="col-sm-9col-sm-10 col-md-10 col-lg-10">
            <select class="form-control input-sm" name="ProgramacionTecnica[ActividadID]" id="evento-actividadid" required>
                <option value="">[SELECCIONE]</option>
            </select>
        </div>
    </div>
	<div class="form-group">
        <label class="col-sm-2 col-md-2 col-lg-2 control-label">Titulo:<span class="f_req">*</span></label>
		<div class="col-sm-10 col-md-10 col-lg-10">
            <input type="text" class="form-control input-sm" name="ProgramacionTecnica[Titulo]" id="evento-titulo" required "/>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 col-md-2 col-lg-2 control-label">Tipología:<span class="f_req">*</span></label>
        <div class="col-sm-10 col-md-10 col-lg-10">
            <select class="form-control input-sm" name="ProgramacionTecnica[Tipologia]" id="evento-tipologia" required>
                <option value="">[SELECCIONE]</option>
                <option value="1">Experimentos de investigación de cultivos priorizado</option>
                <option value="2">Experimentos de investigación de crianzas priorizadas</option>
                <option value="3">Experimentos biotecnológicos.</option>
                <option value="4">Accesiones mantenidas en los Bancos de Germoplasmas (indicar en observaciones la cantidad)</option>
                <option value=5>Experimentos de investigación en cambio climático</option>
                <option value="6">Análisis socio-económicos, de mercados, TT y sistemas de producción</option>
                <option value="7">Experimentos en investigación en post-cosecha</option>
                <option value="8">Variedad liberada / lanzamiento de cultivar mejorado</option>
                <option value="9">Tecnología liberada / lanzamiento de tecnología productiva</option>
                <option value="10">Experimentos incluyan una publicación</option>
            </select>
        </div>
    </div>
	<div class="form-group">
        <label class="col-sm-2 col-md-2 col-lg-2 control-label">Descripci&oacute;n:<span class="f_req">*</span></label>
		<div class="col-sm-10 col-md-10 col-lg-10">
            <textarea class="form-control input-sm" name="ProgramacionTecnica[Descripcion]" id="evento-descripcion" required ></textarea>
        </div>
    </div>
	<div class="form-group">
        <label class="col-sm-2 col-md-2 col-lg-2 control-label">Lugar:<span class="f_req">*</span></label>
		<div class="col-sm-10 col-md-10 col-lg-10">
            <input type="text" class="form-control input-sm" name="ProgramacionTecnica[Lugar]" id="evento-lugar" required />
        </div>
    </div>
	<div class="form-group">
        <label class="col-sm-2 col-md-2 col-lg-2 control-label">Fecha Inicio:<span class="f_req">*</span></label>
		<div class="col-sm-2 col-md-2 col-lg-2">
            <div class="input-group input-group-sm">
                <input type="text" class="form-control fechas" name="ProgramacionTecnica[FechaInicio]" id="evento-fechainicio" required maxlength="10" />
                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
            </div>
        </div>
        <label class="col-sm-1 col-md-1 col-lg-1 control-label">Fin:<span class="f_req">*</span></label>
		<div class="col-sm-2 col-md-2 col-lg-2">
            <div class="input-group input-group-sm">
                <input type="text" class="form-control fechas" name="ProgramacionTecnica[FechaFin]" id="evento-fechafin" required maxlength="10" />    
                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
            </div>
        </div>
        <label class="col-sm-2 col-md-2 col-lg-2 control-label">Beneficiario:<span class="f_req">*</span></label>
		<div class="col-sm-3 col-md-3 col-lg-3">
            <input type="text" class="form-control input-sm" name="ProgramacionTecnica[Beneficiario]" id="evento-Beneficiario" required />
        </div>
    </div>
	<div class="form-group" style="margin-bottom: 0px;">
        <label class="col-sm-2 col-md-2 col-lg-2 control-label">Observaci&oacute;n:</label>
		<div class="col-sm-10 col-md-10 col-lg-10">
            <textarea class="form-control input-sm" name="ProgramacionTecnica[Observacion]" id="evento-observacion"></textarea>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<script type="text/javascript">
    var formParsleyViatico = $('#frmEvento').parsley(defaultParsleyForm2()); 
    var postSave_event = null; // setear esta variable con la funcion que se ejecutara al guardar. function (estado, data, mensaje)

    function Actividad(valor, selectValue = null) {
        var ComponenteID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= Yii::$app->getUrlManager()->createUrl('termino-referencia/actividades'); ?>',
            type: 'POST',
            async: false,
            data: {ComponenteID:ComponenteID,_csrf: toke},
            success: function(data){
                $("#evento-actividadid").html(data);
                if(selectValue != null)
                    $("#evento-actividadid").val(selectValue);
            }
        });
    }

    $('#evento-titulo, #evento-descripcion, #evento-lugar, #evento-observacion, #evento-Beneficiario').filter_input_alfanumericos_validate();
    $('#evento-fechainicio, #evento-fechafin').filter_input_fecha_validate();

    $('#evento-fechainicio').datepicker({
        language: 'es',
        format: 'dd/mm/yyyy',
        autoclose: true
    }).on('changeDate', function (ev) {
        $('#evento-fechafin').datepicker('setStartDate', ev.date)
    });
    $('#evento-fechafin').datepicker({
        language: 'es',
        format: 'dd/mm/yyyy',
        autoclose: true
    }).on('changeDate', function (ev) {
        $('#evento-fechainicio').datepicker('setEndDate', ev.date);
    });
    $(document).off('click').on('click','#btn-guardar-evento',function(){
        if(formParsleyViatico.validate()) {    
            $(this).attr('disabled', 'disabled');
            var datos = { 
                ProgramacionTecnica: { 
                    'IdProgramacionTecnica': $('#hddIdProgramacion').val(),
                    'ComponenteID': $('#evento-componenteid').val(),
                    'ActividadID': $('#evento-actividadid').val(),
                    'Titulo': $('#evento-titulo').val(),
                    'IntroduccionDescripcion': $('#evento-descripcion').val(),
                    'LugarDesarrollo': $('#evento-lugar').val(),
                    'FechaInicio': $('#evento-fechainicio').datepicker('getFormattedDate','yyyy-mm-dd'),
                    'FechaFin': $('#evento-fechafin').datepicker('getFormattedDate','yyyy-mm-dd'),
                    'Probabilidad': $('#evento-probabilidad').val(),
                    'Tipologia': $('#evento-tipologia').val(),
                    'ObservacionInvestigador': $('#evento-observacion').val(),
                    'Beneficiario': $('#evento-Beneficiario').val()
                },
                _csrf : $('input:hidden[name="_csrf"]').val()
            };
            
            $.ajax({
                url: '<?= Yii::$app->getUrlManager()->createUrl('programacion-evento-new/registrar'); ?>',
                data: datos,
                type: 'POST',
                beforeSend:function(){
                },
                success:function(result)
                {
                    result = JSON.parse(result);
                    if(postSave_event != null){
                        postSave_event(result['Estado'], result['Data'], result['Msj']);
                    }
                },
                error:function(){
                    alert('Error al realizar el proceso de busqueda.');
                },
                complete: function (){
                }
            });
        }
        
    });
    <?php if($experimento['IdProgramacionTecnica'] > 0){  ?>
        $("#evento-componenteid").val('<?= $experimento['ComponenteID'] ?>');
        Actividad('<?= $experimento['ComponenteID'] ?>', '<?= $experimento['ActividadID'] ?>');
        $("#evento-titulo").val('<?= $experimento['Titulo'] ?>');

        $("#evento-descripcion").html('<?= preg_replace("[\n|\r|\n\r]", '<br />', $experimento['IntroduccionDescripcion']) ?>');
        $("#evento-lugar").val('<?= $experimento['LugarDesarrollo'] ?>');
        $("#evento-fechainicio").datepicker('setDate', '<?= $experimento['FechaInicio'] ?>');
        $("#evento-fechafin").datepicker('setDate', '<?= $experimento['FechaFin'] ?>');
        $("#evento-probabilidad").val('<?= $experimento['Probabilidad'] ?>');
        $("#evento-tipologia").val('<?= $experimento['Tipologia'] ?>');
        $("#evento-observacion").html('<?= preg_replace("[\n|\r|\n\r]", '<br />', $experimento['ObservacionInvestigador']) ?>');
        $("#evento-Beneficiario").val('<?= $experimento['Beneficiario'] ?>');
    <?php } ?>
</script>