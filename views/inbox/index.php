<div id="page-content" style="min-height: 1342px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Seguimiento del Proyecto</h1>
        </div>
        <div class="container">
            <table class="table">
                <thead>
                    <th>Etapa</th>
                    <th>Situacion</th>
                    <th>Acciones</th>
                </thead>
                <tbody>
                <?php
                    foreach($inboxs as $inbox){
                        echo '<tr>';
                            echo '<td>'.$inbox->getEtapa($inbox->Etapa).'</td>';
                            echo '<td>'.$inbox->getSituacion($inbox->Situacion).'</td>';
                            echo '<td>'.$inbox->getAcciones($inbox->Etapa,$inbox->Situacion).'</td>';
                        echo '</tr>';
                    }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $(document).on('click','.enviar_evaluacion_poa',function (e) {
        e.preventDefault();
        var $btn = $(this)
        bootboxConfirm($btn,
            'Está seguro de enviar a evaluar el POA?',
            'Enviar',
            function () {
                var epID = $btn.attr('role-id');
                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                $.post('<?= \Yii::$app->request->BaseUrl ?>/entidad-participante/deletentidades/', { id: epID, _csrf: toke }, function (data) {
                        var jx = JSON.parse(data);
                    if (jx.Success == true) {
                        cargarColaboradoras();
                    }
                });
            });
    });
</script>