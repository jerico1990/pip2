<?php
use app\models\Actividad;
use app\models\ActRubroElegible;
use app\models\CronogramaAreSubCategoria;
use app\models\CronogramaProyecto;
use app\models\MarcoLogicoActividad;
use app\models\AreSubCategoria;


/** Error reporting */
error_reporting(E_ALL);

/** Include path **/
ini_set('include_path', ini_get('include_path').';../web/PHPExcel/Classes/');
//require(__DIR__ . '/../../web/PHPExcel/Classes/');
/** PHPExcel */
include 'PHPExcel.php';

/** PHPExcel_Writer_Excel2007 */
include 'PHPExcel/Writer/Excel2007.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
$sheet = $objPHPExcel->getActiveSheet();

$Meses=[];
$objPHPExcel->setActiveSheetIndex(0);
$letras=[1=>'A',2=>'B',3=>'C',4=>'D',5=>'E',6=>'F',7=>'G',8=>'H',9=>'I',10=>'J',11=>'K',12=>'L',13=>'M',
         14=>'N',15=>'O',16=>'P',17=>'Q',18=>'R',19=>'S',20=>'T',21=>'U',22=>'V',23=>'W',24=>'X',25=>'Y',
         26=>'Z',27=>'AA',28=>'AB',29=>'AC',30=>'AD',31=>'AE',32=>'AF',33=>'AG',34=>'AH',35=>'AI',36=>'AJ',37=>'AK',38=>'AL',39=>'AM',
         40=>'AN',41=>'AO',42=>'AP',43=>'AQ',44=>'AR',45=>'AS',46=>'AT',47=>'AU',48=>'AV',49=>'AW',50=>'AX',51=>'AY',
         52=>'AZ',53=>'BA',54=>'BB',55=>'BC',56=>'BD',57=>'BE',58=>'BF',59=>'BG',60=>'BH',61=>'BI',61=>'BJ',62=>'BK',63=>'BL',64=>'BM',
         65=>'BN',66=>'BO',67=>'BP',68=>'BQ',69=>'BR',70=>'BS',71=>'BT',72=>'BU',73=>'BV',74=>'BW',75=>'BX',76=>'BY',
         77=>'BZ',];

$sheet->SetCellValue('A1', '');
$sheet->SetCellValue('B1', '');
$sheet->SetCellValue('C1', 'Detalle');
$sheet->SetCellValue('C5', 'Partida');
$sheet->SetCellValue('D5', 'Subpartida');
$sheet->SetCellValue('E5', 'Código Matriz');
$sheet->SetCellValue('F5', 'Específica');
$sheet->SetCellValue('G5', 'Detalle Gasto');
$sheet->SetCellValue('H5', 'Unidad');
$sheet->SetCellValue('I5', 'Cantidad');
$sheet->SetCellValue('J5', 'Costo Unitario');
$sheet->SetCellValue('K5', 'Total (del Proyecto Adjudicado)');

$sheet->getRowDimension('1')->setRowHeight(30);

$sheet->getColumnDimension('A')->setWidth(15);

$styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));

  
/*
$sheet->getColumnDimension('A')->setAutoSize(true);
$sheet->getColumnDimension('B')->setAutoSize(true);
$sheet->getColumnDimension('C')->setAutoSize(true);
$sheet->getColumnDimension('D')->setAutoSize(true);
$sheet->getColumnDimension('E')->setAutoSize(true);
$sheet->getColumnDimension('F')->setAutoSize(true);
$sheet->getColumnDimension('G')->setAutoSize(true);
$sheet->getColumnDimension('H')->setAutoSize(true);
$sheet->getColumnDimension('I')->setAutoSize(true);
*/
$sheet->getStyle('A1:K3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle('A1:K3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


foreach(CronogramaProyecto::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->all() as $mes)
{
    array_push ( $Meses , 'Mes '.$mes->Mes );
}

$L=12;
$c=0;
foreach($componentes as $componente)
{
    $countActividad=0;
    $i=0;
    $actividades=Actividad::find()->where('ComponenteID=:ComponenteID and Estado=1',[':ComponenteID'=>$componente->ID])->orderBy('Correlativo asc')->all();
    foreach($actividades as $actividad)
    {
        $sheet->SetCellValue($letras[$L+$countActividad+$i].'3', $actividad->Correlativo);
        $sheet->mergeCells($letras[$L+$countActividad+$i].'3:'.$letras[$L+$countActividad+$i+1].'3');
        $sheet->SetCellValue($letras[$L+$countActividad+$i].'4', $actividad->Nombre);
        $sheet->mergeCells($letras[$L+$countActividad+$i].'4:'.$letras[$L+$countActividad+$i+1].'4');
        $sheet->SetCellValue($letras[$L+$countActividad+$i].'5', 'Cant');
        $sheet->SetCellValue($letras[$L+$countActividad+$i+1].'5', 'S/.');
        $sheet->getStyle('A1:'.$letras[$L+$countActividad+$i+1].'4')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);    
        /*
        $recursos=  AreSubCategoria::find()
                    ->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')
                    ->where('ActRubroElegible.ActividadID=:ActividadID',[':ActividadID'=>$actividad->ID])
                    ->all();
        
       
                        
        $r=6;
        foreach($recursos as $recurso)
        {
            $sheet->SetCellValue($letras[1].$r, $recurso->MetaFisica);
            $sheet->SetCellValue($letras[1].$r, $recurso->Total);
            
            
            
            $sheet->SetCellValue($letras[$L+$countActividad+$i].$r, $recurso->MetaFisica);
            $sheet->SetCellValue($letras[$L+$countActividad+$i+1].$r, $recurso->Total);
            $r=$r+$c+$i+1;
        }
        */
        
        $countActividad++;
        $i++;
    }
    
    $sheet->SetCellValue($letras[$L].'1', $componente->Correlativo);
    $sheet->mergeCells($letras[$L].'1:'.$letras[$L+$countActividad*2-1].'1');
    $sheet->SetCellValue($letras[$L].'2', $componente->Nombre);
    $sheet->mergeCells($letras[$L].'2:'.$letras[$L+$countActividad*2-1].'2');
    $L=$L+$countActividad*2;
    $c++;
}



$recursos=Yii::$app->db->createCommand("
                        SELECT 
                                convert(NVARCHAR(200),RubroElegible.ID)+'.'+convert(NVARCHAR(200),RubroElegible.Nombre) Rubro,
                                AreSubCategoria.Nombre,
                                AreSubCategoria.Especifica,AreSubCategoria.Detalle,
                                convert(NVARCHAR(200),ActRubroElegible.RubroElegibleID)+'.'+convert(NVARCHAR(200),AreSubCategoria.Codificacion)+'.' Codificacion,
                                AreSubCategoria.Codificacion Codificacion2,
                                AreSubCategoria.UnidadMedida,
                                sum(AreSubCategoria.MetaFisica) Cant,
                                AreSubCategoria.CostoUnitario,
                                sum(AreSubCategoria.MetaFisica)*AreSubCategoria.CostoUnitario AS Total
                        FROM AreSubCategoria
                        INNER JOIN ActRubroElegible ON ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID
                        INNER JOIN RubroElegible ON RubroElegible.ID=ActRubroElegible.RubroElegibleID
                        INNER JOIN Actividad ON Actividad.ID=ActRubroElegible.ActividadID
                        INNER JOIN Componente ON Componente.ID=Actividad.ComponenteID
                        WHERE Componente.PoaID=".$poa->ID." and Actividad.Estado=1 and Componente.Estado=1
                        GROUP BY AreSubCategoria.Especifica,AreSubCategoria.Detalle,RubroElegible.ID,RubroElegible.Nombre,ActRubroElegible.RubroElegibleID,AreSubCategoria.Codificacion,AreSubCategoria.Nombre,AreSubCategoria.UnidadMedida,AreSubCategoria.CostoUnitario
                        ORDER BY RubroElegible.ID ASC,Codificacion2 ASC
                        ")
                        ->queryAll();
$re=6;
$c=0;
foreach($recursos as $recurso)
{
    $sheet->SetCellValue($letras[3].$re, $recurso['Rubro']);
    $sheet->SetCellValue($letras[4].$re, $recurso['Nombre']);
    $sheet->SetCellValue($letras[5].$re, $recurso['Codificacion']);
    $sheet->SetCellValue($letras[6].$re, $recurso['Especifica']);
    $sheet->SetCellValue($letras[7].$re, $recurso['Detalle']);
    $sheet->SetCellValue($letras[8].$re, $recurso['UnidadMedida']);
    $sheet->SetCellValue($letras[9].$re, $recurso['Cant']);
    $sheet->SetCellValue($letras[10].$re, $recurso['CostoUnitario']);
    $sheet->SetCellValue($letras[11].$re, $recurso['Total']);
        $bandera=explode(".",$recurso['Codificacion']);
        /*
        $recursosActividades=  AreSubCategoria::find()
                    ->select('AreSubCategoria.*,ActRubroElegible.RubroElegibleID')
                    ->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')
                    ->innerJoin('Actividad','Actividad.ID=ActRubroElegible.ActividadID')
                    ->innerJoin('Componente','Componente.ID=Actividad.ComponenteID')
                    ->innerJoin('Poa','Poa.ID=Componente.PoaID')
                    ->where('Poa.ID=:ID and ActRubroElegible.RubroElegibleID=:RubroElegibleID and AreSubCategoria.Codificacion=:Codificacion',
                            [':ID'=>$poa->ID,':RubroElegibleID'=>1,':Codificacion'=>1])
                    ->OrderBy('Componente.Correlativo ASC,Actividad.Correlativo ASC')
                    ->all();*/
        
                    //echo '<pre>';print_r($recursosActividades);die;
        $recursosActividades=Yii::$app->db->createCommand("
                        SELECT 
                            Componente.Correlativo,
                            Actividad.Correlativo,
                            ISNULL((SELECT TOP 1 AreSubCategoria.MetaFisica FROM AreSubCategoria 
                            INNER JOIN ActRubroElegible ON ActRubroElegible.ID = AreSubCategoria.ActRubroElegibleID
                            INNER JOIN Actividad a ON a.ID = ActRubroElegible.ActividadID
                            WHERE a.Estado=1 AND a.ID=Actividad.ID AND Codificacion=$bandera[1] AND ActRubroElegible.RubroElegibleID = $bandera[0]),0) MetaFisica,
                            ISNULL((SELECT TOP 1 AreSubCategoria.MetaFisica*AreSubCategoria.CostoUnitario FROM AreSubCategoria 
                            INNER JOIN ActRubroElegible ON ActRubroElegible.ID = AreSubCategoria.ActRubroElegibleID
                            INNER JOIN Actividad a ON a.ID = ActRubroElegible.ActividadID
                            WHERE a.Estado=1 AND a.ID=Actividad.ID AND Codificacion=$bandera[1] AND ActRubroElegible.RubroElegibleID = $bandera[0]),0) Total
                        FROM Actividad
                        INNER JOIN Componente ON Componente.ID=Actividad.ComponenteID
                        WHERE Componente.PoaID=".$poa->ID." AND Componente.Estado = 1 AND Actividad.Estado = 1
                        ")
                        ->queryAll();
        $r=6;
        $b=0;
        
            foreach($recursosActividades as $rec)
            {
                //echo '<pre>';print_r($rec);die;
                    $sheet->SetCellValue($letras[(12+$b)].($r+$c), $rec['MetaFisica']);
                    $sheet->SetCellValue($letras[(13+$b)].($r+$c), $rec['Total']);

                $b=$b+2;
                
                //$r=$r+$re;
            }
        
        $c++;
    $re++;
}
$highestColumm = $objPHPExcel->setActiveSheetIndex(0)->getHighestColumn();
$sheet->getStyle('A1:'.$highestColumm.($re-1))->applyFromArray($styleArray);
$sheet->getStyle('E6:E'.($re-1))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('F2F2F2');

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="CronogramaFinanciero.xls"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
