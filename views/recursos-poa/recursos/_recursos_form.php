<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'recursos-poa/recursos-crear?investigadorID='.$investigador,
        'options'           => [
            'autocomplete'  => 'off',
            'id'            => 'frmRecursos'
        ]
    ]
); ?>
    <input type="hidden" name="ActRubroElegible[ActividadID]" id="actividadid" value="<?= $idactividad ?>">
    <input type="hidden" name="AreSubCategoria[Codificacion]" id="codificacion" value="<?= $resultado["Codificacion"] ?>" >
    <div class="modal-body">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label">Partida Específica <span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <select class="form-control" name="ActRubroElegible[RubroElegibleID]" required="" onchange="AreSubCategoria($(this).val())">
                        <option value="">[SELECCIONE]</option>
                        <?php foreach ($rubro as $value): ?>
                            <option value="<?= $value->ID ?>"  <?= ($value->ID==$resultado["ID"])?'selected':'';?>><?= $value->Nombre ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
	    <!--
	    <div class="form-group">
                <label class="col-sm-3 control-label">Recurso </label>
                <div class="col-sm-9">
		    <select class="form-control" name="ActRubroElegible[AreSubCategoriaID]" id="aresubcategoriaid" onchange="GetDatosRecurso($(this).val())">
			<option value="">[SELECCIONE]</option>
		    </select>
                </div>
            </div>
	    -->
	    <!--
            <div class="form-group">
                <label class="col-sm-3 control-label">Rubro Elegible: Unidad de Medida <span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="ActRubroElegible[UnidadMedida]">
                    
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Rubro Elegible: Costo Unitario <span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="number" class="form-control" name="ActRubroElegible[CostoUnitario]">
                   
                </div>
            </div>-->
	    <div class="form-group">
                <label class="col-sm-3 control-label">Tarea </label>
                <div class="col-sm-9">
		    <select class="form-control" name="AreSubCategoria[TareaID]" id="tareaid" >
			<option value>Seleccionar</option>
			<?php foreach($tareas as $tarea){ ?>
			<option value="<?= $tarea->ID ?>"><?= $tarea->Descripcion ?></option>
			<?php } ?>
		    </select>
                </div>
            </div>
	    
            <hr>

            <div class="form-group">
                <label class="col-sm-3 control-label">Sub Partida <span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="AreSubCategoria[Nombre]" id="nombre" <?= !empty($resultado["Nombre"])?'disabled':'' ?>><?= $resultado["Nombre"] ?></textarea>
                    <!--<h6 class="pull-left count-message"></h6>-->
                </div>
            </div>
	    <div class="form-group">
                <label class="col-sm-3 control-label">Específica <span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="AreSubCategoria[Especifica]" id="especifica"  <?= !empty($resultado["Especifica"])?'disabled':'' ?>><?= $resultado["Especifica"] ?></textarea>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Costo Unitario <span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text"   class="form-control" name="AreSubCategoria[CostoUnitario]" id="costo-unitario" value="<?= $resultado["CostoUnitario"] ?>" <?= !empty($resultado["CostoUnitario"])?'disabled':'' ?>>
                    <!--<h6 class="pull-left count-message"></h6>-->
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Unidad de medida <span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="AreSubCategoria[UnidadMedida]" id="unidad-medida" value="<?= $resultado["UnidadMedida"] ?>" <?= !empty($resultado["UnidadMedida"])?'disabled':'' ?>>
                    <!--<h6 class="pull-left count-message"></h6>-->
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Meta Fisica <span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" onKeyPress="return NumCheck(event,this);" class="form-control" name="AreSubCategoria[MetaFisica]" >
                    <!--<h6 class="pull-left count-message"></h6>-->
                </div>
            </div>
	    <div class="form-group">
                <label class="col-sm-3 control-label">Colaborador <span class="f_req">*</span></label>
                <div class="col-sm-9">
		    <select name="AreSubCategoria[EntidadParticipanteID]" class="form-control" id="entidad-participante-id" requerid >
			<option value=>Seleccionar</option>
			<?php foreach($entidadesParticipantes as $entidadParticipante){ ?>
                <?php if($entidadParticipante->RazonSocial == 'PNIA' || $entidadParticipante->RazonSocial == 'PROGRAMA NACIONAL DE INNOVACION AGRARIA- PNIA'): ?>
    			    <option value="<?= $entidadParticipante->ID ?>" <?= ($entidadParticipante->ID==$resultado["EntidadParticipanteID"])?'selected':'';?> ><?= $entidadParticipante->RazonSocial ?></option>
                <?php endif ?>
			<?php } ?>
		    </select>
                    <!--<h6 class="pull-left count-message"></h6>-->
                </div>
            </div>
	    <div class="form-group">
                <label class="col-sm-3 control-label">Fondo <span class="f_req">*</span></label>
                <div class="col-sm-9">
		    <select name="AreSubCategoria[FondoEntidadParticipanteID]" id="fondo-entidad-participante-id" class="form-control" >
			<option value=>Seleccionar</option>
			<option value="1" <?= ("1"==$resultado["FondoEntidadParticipanteID"])?'selected':'';?>>Monetario</option>
			<!-- <option value="2" <?= ("2"==$resultado["FondoEntidadParticipanteID"])?'selected':'';?>>No Monetario</option> -->
		    </select>
                    <!--<h6 class="pull-left count-message"></h6>-->
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button id="btn-save-recurso-p" type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>


<script>
    var $form = $('#frmRecursos');
    var formParsleyRecursos = $form.parsley(defaultParsleyForm());


    $(document).ready(function () {
        $('.count-message').each(function () {
            var $this = $(this);
            var $divformgroup = $this.closest('div.form-group');
            var $txt = $divformgroup.find('textarea, input[type="text"]');
            var text_max = parseInt($txt.attr('maxlength'));

            $txt.keyup(function () {
                var text_length = $txt.val().length;
                var text_remaining = text_max - text_length;
                $this.html(text_remaining + ' caracteres restantes');
            });
        });


    });
    
    function AreSubCategoria(valor) {
	var investigadorid='<?= $investigador; ?>';
	var toke = '<?=Yii::$app->request->getCsrfToken()?>';
	$.get('<?= \Yii::$app->request->BaseUrl ?>/recursos-poa/get-recurso', { valor: valor ,investigadorid:investigadorid,_csrf: toke }, function (data) {
	    
	    $('#aresubcategoriaid').html(data);
	});
    }
    /*
    function GetDatosRecurso(valor) {
	var investigadorid='<?= $investigador; ?>';
	var toke = '<?=Yii::$app->request->getCsrfToken()?>';
	$.get('<?= \Yii::$app->request->BaseUrl ?>/recursos-poa/get-datos-recurso', { valor: valor ,investigadorid:investigadorid,_csrf: toke }, function (data) {
                var jx = JSON.parse(data);
                if (jx.Success == true) {
                    
                    $('#nombre').val(jx.Nombre);
                    $('#especifica').val(jx.Especifica);
		    $('#costo-unitario').val(jx.CostoUnitario);
		    $('#unidad-medida').val(jx.UnidadMedida);
		    $('#entidad-participante-id').val(jx.EntidadParticipanteID);
		    $('#fondo-entidad-participante-id').val(jx.FondoEntidadParticipanteID);
		    $('#codificacion').val(jx.Codificacion);
		    
                }
            });
    }
    */
</script>