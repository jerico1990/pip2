<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
?>

    <div class="modal-body">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label">Rubro Elegible:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="hidden" name="" value="<?php echo $ActividadID ?>" id="requerimiento-actividadid-filtro">
                    <select class="form-control" id="orden-rubroelegible-filtro">
                        <option>[SELECCIONE]</option>
                        <?php foreach($rubrosElegibles as $rubroElegible){ ?>
                            <option value="<?= $rubroElegible->ID ?>"><?= $rubroElegible->ID ?> <?= $rubroElegible->Nombre ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            
            <!-- <div class="form-group">
                <label class="col-sm-3 control-label">Actividad:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <select class="form-control" name="Requerimiento[ActividadID]" id="requerimiento-actividadid-filtro" onchange="Recurso($(this).val())">
                        
                    </select>
                </div>
            </div> -->
            
            <div class="form-group">
                <div class="col-sm-3">
                    <a class="btn btn-primary seleccionar_poa" data-act-idd="<?= $ActividadID?>" >Seleccionar </a>
                    <a class="btn btn-primary nuevo" data-act-idd="<?= $ActividadID ?>" >Nuevo </a>
                </div>
            </div>
            
            <div class="table-responsive">
                <table class="codificaciones display table table-bordered table-striped table-condensed table-hover" cellspacing="0" >
                    <thead>
                        <tr>
                            <th>Código Matriz</th>
                            <th>Recurso</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>

<script>
   
    var tblresultjs1={
        "order": [[ 0, "desc" ]],
        "language": {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta lista",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":    "Último",
                "sNext":    "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        //"destroy": true
    };

    $(document).ready(function() {
        var CodigoProyecto="<?= $CodigoProyecto ?>";
        $.ajax({
            url:'<?= Url::toRoute('recursos-poa/listado-codificacion?CodigoProyecto='.$CodigoProyecto.'&RubroElegibleID=') ?>',
            async:false,
            data:{},
            beforeSend:function()
            {
                //$('.panel-default').LoadingOverlay("show");
            },
            success:function(result)
            {
                //$("#poas").destroy();//
                $(".codificaciones tbody").html(result);
                // $('.codificaciones').DataTable(tblresultjs1);
                //tblresultjs=$('#example').DataTable(configDTjs);
                //$('.panel-default').LoadingOverlay("hide",true);
            },
            error:function(){
                //$('.panel-default').LoadingOverlay("hide",true);
                alert('Error al realizar el proceso de busqueda.');
            }
        });
    });
    
    $('body').on('click', '.pintar_poa', function (e) {
        e.preventDefault();
        var id=$(this).attr('id');
        $('.fila').each(function(){
            var td=$(this).children();
            td.removeAttr('style');
        });
        $('#'+id).addClass('fila');
        $('#'+id+" td").css('background','yellow');
    });

    // $('body').on('change', '#orden-rubroelegible-filtro', function (e) {
    // $('body').on('change', '#orden-rubroelegible-filtro', function (e) {
    $('#orden-rubroelegible-filtro').change(function(e){
        e.preventDefault();
        var ComponenteID=$(this).val();
        console.log(ComponenteID);
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        
        var tblresultjs2={
            "order": [[ 0, "desc" ]],
            "language": {
                "sProcessing":    "Procesando...",
                "sLengthMenu":    "Mostrar _MENU_ registros",
                "sZeroRecords":   "No se encontraron resultados",
                "sEmptyTable":    "Ningún dato disponible en esta lista",
                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":   "",
                "sSearch":        "Buscar:",
                "sUrl":           "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":    "Último",
                    "sNext":    "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            "destroy":true
        };
        
        var CodigoProyecto="<?= $CodigoProyecto ?>";
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: location.origin+'/pip2/web/orden/listado-codificacion?CodigoProyecto='+CodigoProyecto+'&RubroElegibleID='+$(this).val()+'&ActividadID='+$('#requerimiento-actividadid-filtro').val()+'&_csrf='+toke,
            async:false,
            success:function(result)
            {
                    // $(".poas").destroy();//
                    $('.codificaciones').DataTable(tblresultjs2).destroy();
                    // console.log(result);
                    //$(".poas tbody").html('');
                    $(".codificaciones tbody").html(result);
                    // $('.codificaciones').DataTable(tblresultjs2);
                    
                    //tblresultjs=$('#example').DataTable(configDTjs);
                    $('.panel-default').LoadingOverlay("hide",true);
            }
        });
    });

    


</script>