<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
	'action'            => 'recursos-poa/recursos-actualizar?id='.$id.'&actv='.$actv.'&investigador='.$investigador,
        'options'           => [
            'autocomplete'  => 'off',
            'id'            => 'frmRecursos'
        ]
    ]
); ?>

<input type="hidden" name="ActRubroElegible[ActividadID]" id="actividadid" value="<?= $actv ?>">
<input type="hidden" name="Codificacion" id="codificacion" value="<?= $recurso->Codificacion ?>" >
    <div class="modal-body">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label">Partida Específica <span class="f_req">*</span></label>
                <div class="col-sm-9">
                	<select class="form-control" name="ActRubroElegible[RubroElegibleID]" required="">
                        <option value="">[SELECCIONE]</option>
                        <?php foreach ($rubrosElegibles as $value): ?>
                            <option value="<?php echo $value->ID ?>" <?= ($value->ID==$recurso->RubroElegibleID)?'selected':'';?>><?php echo $value->Nombre ?></option>
                        <?php endforeach ?>
                	</select>
                </div>
            </div>
    	    <div class="form-group">
                <label class="col-sm-3 control-label">Tarea </label>
                <div class="col-sm-9">
        		    <select class="form-control" name="AreSubCategoria[TareaID]" id="tareaid" >
            			<option value>Seleccionar</option>
            			<?php foreach($tareas as $tarea){ ?>
                            <option value="<?= $tarea->ID ?>" <?= ($tarea->ID==$recurso->TareaID)?'selected':'';?>><?= $tarea->Descripcion ?></option>
            			<?php } ?>
        		    </select>
                </div>
            </div>
	    <!--
            <div class="form-group">
                <label class="col-sm-3 control-label">Rubro Elegible: Unidad de Medida <span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="ActRubroElegible[UnidadMedida]">
                    
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Rubro Elegible: Costo Unitario <span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="number" class="form-control" name="ActRubroElegible[CostoUnitario]">
                </div>
            </div>-->
            <hr>

            <div class="form-group">
                <label class="col-sm-3 control-label">Sub Partida <span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="Nombre"><?= $recurso->Nombre ?></textarea>
                    <!--<h6 class="pull-left count-message"></h6>-->
                </div>
            </div>
	    
	    <div class="form-group">
                <label class="col-sm-3 control-label">Específica <span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="Especifica"><?= $recurso->Especifica ?></textarea>
                    <!--<h6 class="pull-left count-message"></h6>-->
                </div>
            </div>
	    
	    
            <div class="form-group">
                <label class="col-sm-3 control-label">Costo Unitario <span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control"   name="CostoUnitario" value="<?= $recurso->CostoUnitario ?>" >
                    <!--<h6 class="pull-left count-message"></h6>-->
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Unidad de medida <span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="UnidadMedida" value="<?= $recurso->UnidadMedida ?>">
                    <!--<h6 class="pull-left count-message"></h6>-->
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Meta Fisica <span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" onKeyPress="return NumCheck(event,this);" class="form-control" name="AreSubCategoria[MetaFisica]" value="<?= $recurso->MetaFisica ?>">
                    <!--<h6 class="pull-left count-message"></h6>-->
                </div>
            </div>
	    <div class="form-group">
                <label class="col-sm-3 control-label">Colaborador <span class="f_req">*</span></label>
                <div class="col-sm-9">
		    <select name="EntidadParticipanteID" class="form-control" >
			<option value=>Seleccionar</option>
			<?php foreach($entidadesParticipantes as $entidadParticipante){ ?>
                <?php if($entidadParticipante->RazonSocial == 'PNIA' || $entidadParticipante->RazonSocial == 'PROGRAMA NACIONAL DE INNOVACION AGRARIA- PNIA' ): ?>
                    <option value="<?= $entidadParticipante->ID ?>" <?= ($entidadParticipante->ID==$recurso->EntidadParticipanteID)?'selected':''; ?>><?= $entidadParticipante->RazonSocial ?></option>
                <?php endif ?>
                
			    
			<?php } ?>
		    </select>
                    <!--<h6 class="pull-left count-message"></h6>-->
                </div>
            </div>
	    <div class="form-group">
                <label class="col-sm-3 control-label">Fondo <span class="f_req">*</span></label>
                <div class="col-sm-9">
		    <select name="FondoEntidadParticipanteID" class="form-control" >
			<option value=>Seleccionar</option>
			<option value="1" <?= ($recurso->FondoEntidadParticipanteID==1)?'selected':''; ?> >Monetario</option>
			<!-- <option value="2" <?= ($recurso->FondoEntidadParticipanteID==2)?'selected':''; ?>>No Monetario</option> -->
		    </select>
                    <!--<h6 class="pull-left count-message"></h6>-->
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button id="btn-save-recurso-p" type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>


<script>
    var $form = $('#frmRecursos');
    var formParsleyRecursos = $form.parsley(defaultParsleyForm());

    $(document).ready(function () {
        
        $('.count-message').each(function () {
            var $this = $(this);
            var $divformgroup = $this.closest('div.form-group');
            var $txt = $divformgroup.find('textarea, input[type="text"]');
            var text_max = parseInt($txt.attr('maxlength'));

            $txt.keyup(function () {
                var text_length = $txt.val().length;
                var text_remaining = text_max - text_length;
                $this.html(text_remaining + ' caracteres restantes');
            });
        });


    });
    

</script>