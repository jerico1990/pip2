<?php if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start(); ?>
<div id="page-content" style="min-height: 885px;">
    <div id="wrap">
        <div id="page-heading">
            <h1></h1>
        </div>
        <div class="container">
                    
            <style>
                #page-heading {
                    display: none;
                }

                .tab-content {
                    background-color: #fff;
                }

                .panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 1px solid #a8a8ab !important;
                }

                .panel-gray .panel-body {
                    border-top: none !important;
                }

                .panel-heading {
                    height: auto !important;
                }
            </style>
    
            <div class="tab-container">
                <ul class="nav nav-tabs">
                    <li id="li-poaf" class="active"><a href="#tab-poaf" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Recursos</a></li>
                    <!-- <li id="li-pc"><a href="#container-pc" data-toggle="tab"><i class="fa fa-pencil"></i>&nbsp;Pasos Críticos</a></li> -->
                </ul>
                <div class="tab-content">

                    <div class="tab-pane active" id="tab-poaf">
                        <div id="container-poaf">

                            <style>
                                #container-poaf .input-number,#container-poaf .success {
                                    text-align: right;
                                    min-width: 72px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }

                                .btn-save-cronograma-recurso{
                                    /*text-align: right;
                                    min-width: 72px !important;
                                    font-size: 13px !important;*/
                                    height: 22px !important;
                                }

                                #container-poaf .input-number-sm {
                                    text-align: right;
                                    min-width: 52px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }

                                #container-poaf .input-number-md {
                                    text-align: right;
                                    min-width: 82px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }

                                #container-poaf table-act > tbody > tr > td:nth-child(1) {
                                    text-align: center;
                                }

                                #container-poaf .table-act > tbody > tr > td:nth-child(3) {
                                    min-width: 320px !important;
                                }

                                #container-poaf .table-act > tbody > tr > td:nth-child(4) {
                                    min-width: 100px !important;
                                }

                                #container-poaf .table > tbody > .tr-header > td {
                                    text-align: center;
                                    font-size: 12px;
                                    font-weight: bold;
                                    padding: 1px !important;
                                    vertical-align: middle !important;
                                    /*border: 1px solid #cfcfd0;
                                    background-color: #f0f0f1;*/
                                    border: 1px solid #c0c0c0;
                                    background-color: #e3e3e3;
                                    min-width: 75px !important;
                                }

                                #container-poaf h4.panel-title {
                                    font-size: 16px;
                                }

                                #container-poaf .tr-comp td, .tr-proy td {
                                    font-weight: bold;
                                }

                                #container-poaf .table-condensed td {
                                    padding: 1px !important;
                                }

                                #container-poaf td, #table-container input {
                                    font-size: 13px !important;
                                }

                                #container-poaf .form-control[readonly] {
                                    background-color: #f2f3f4;
                                }
                            </style>
                            
                            <a class="btn btn-primary presupuesto " id="hidens2">Control de presupuesto</a>
                            <br>
                            <br>
                            <div id="table-container">
                            </div>

                            <br>
                            <div class="text-center hide" id="hidens">
                                <?php /*if(\Yii::$app->user->identity->Rol==2){?>
                                <button class="btn btn-primary btn-aprobar-poa" ><i class="fa fa-check"></i>&nbsp;Aprobar POA</button>
                                <button class="btn btn-primary btn-observar-poa" ><i class="fa fa-check"></i>&nbsp;Observar POA</button>
                                <?php }*/ ?>
                                <?php /*if(\Yii::$app->user->identity->SituacionProyecto==1){?>
                                <button class="btn btn-primary btn-reprogramacion-poa"><i class="fa fa-check"></i>&nbsp;Reprogramación de recursos</button>
                                <?php }*/ ?>
                                <button class="btn btn-primary btn-enviar-evaluacion-poa" ><i class="fa fa-check"></i>&nbsp;Validar programación de recursos</button>
                                <a target="_blank" class="btn btn-inverse-alt" href="<?= Yii::$app->getUrlManager()->createUrl('recursos-poa/descargar-poa?codigo='.$usuario->username.'') ?>">
                                    <i class="fa fa-download"></i>&nbsp;Descargar Cronograma Físico
                                </a>
                                
                                <a target="_blank" class="btn btn-inverse-alt" href="<?= Yii::$app->getUrlManager()->createUrl('recursos-poa/descargar-poa-financiero?codigo='.$usuario->username.'') ?>">
                                    <i class="fa fa-download"></i>&nbsp;Descargar Cronograma Financiero
                                </a> 
                            </div> 
                            <!-- <div class="text-center">
                                <button class="btn btn-primary btn-save-poaf"><i class="fa fa-check"></i>&nbsp;Guardar</button>
                                <a class="btn btn-inverse-alt" href="/SLFC/PasoCritico/DescargarPC_PlanOperativoDetallado">
                                    <i class="fa fa-download"></i>&nbsp;Descargar
                                </a>
                            </div> -->

                            <script>
                                var _proyecto = new Object();

                                $(document).ready(function () {
                                    $('#table-container').html('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>');
                                    getProyecto().done(function(json) {
                                        _proyecto = JSON.parse(JSON.stringify(json));
                                        _drawTable();
                                        $('#hidens').removeClass('hide');
                                        $('#hidens2').removeClass('hide');
                                    })
                                    // .always(function() {
                                    //     alert( "finished" );
                                    // });
                                });
                                
                                $('.btn-descargar-poa').click(function (e) {
                                    var $btn = $(this);
                                    var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                                    $.ajax({
                                        url: '<?= Yii::$app->getUrlManager()->createUrl('recursos-poa/descargar-poa') ?>',
                                        type: 'GET',
                                        async: false,
                                        data: {codigo:'<?= $usuario->username ?>',_csrf: toke},
                                        success: function(data){
                                           
                                        }
                                    });
                                });
                                
                                $('.btn-aprobar-poa').click(function (e) {
                                    var $btn = $(this);
                                    bootbox.confirm({
                                        title: '<span style="text-align:center">PNIA - CONFIRMACIÓN</span>',
                                        message: 'Está seguro de aprobar el poa del proyecto?.',
                                        buttons: {
                                            'cancel': {
                                                label: 'CANCELAR'
                                            },
                                            'confirm': {
                                                label: 'APROBAR POA'
                                            }
                                        },
                                        callback: function (confirmed) {
                                            if (confirmed) {
                                                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                                                $.ajax({
                                                    url: '<?= Yii::$app->getUrlManager()->createUrl('recursos-poa/aprobar-poa') ?>',
                                                    type: 'GET',
                                                    async: false,
                                                    data: {aprobar:1,codigo:'<?= $usuario->username ?>',_csrf: toke},
                                                    success: function(data){
                                                       
                                                    }
                                                });
                                            } 
                                        }
                                    });
                                });

                                $('body').on('click', '.btn-aprobar-recursos', function (e) {
                                    var $this = $(this);
                                    var ObservacionID=$this.attr('data-id');
                                    bootbox.confirm({
                                        title: '<span style="text-align:center">PNIA - OBSERVACIÓN</span>',
                                        message: 'Está seguro de aprobar la observación ?.',
                                        buttons: {
                                            'cancel': {
                                                label: 'CANCELAR'
                                            },
                                            'confirm': {
                                                label: 'APROBAR OBSERVACIÓN'
                                            }
                                        },
                                        callback: function (confirmed) {
                                            if (confirmed) {
                                                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                                                $.ajax({
                                                    url: '<?= Yii::$app->getUrlManager()->createUrl('recursos-poa/aprobar-observacion') ?>',
                                                    type: 'GET',
                                                    async: false,
                                                    data: {ObservacionID:ObservacionID,_csrf: toke},
                                                    success: function(data){
                                                       
                                                    }
                                                });
                                            } 
                                        }
                                    });
                                });
                                
                                
                                function getNum(val) {
                                    if (val == '0.00' || val == '') {
                                         return 0;
                                    }
                                    var val2 = val.replace(',','');
                                    return val2;
                                }
                                $('body').on('focus', '.btn-save-cronograma-recurso', function (e) {
                                    var $this = $(this);
                                    $this.css('background', 'white');
                                });
                                
                                $('body').on('change', '.btn-save-cronograma-recurso', function (e) {
                                    var $this = $(this);
                                    var ID=$this.attr('data-cronograma-recurso-id');
                                    var RecursoID=$this.attr('data-recurso-id');
                                    var RecursoMetaFisica=parseFloat($this.attr('data-meta-fisica'));
                                    
                                    suma=0;
                                    $('.recurso_'+RecursoID).each(function(){
                                        suma = suma+parseFloat(getNum($(this).val()));
                                    });
                                    /*
                                    if (RecursoID==8214) {
                                        suma=0.00;
                                        console.log(RecursoID);
                                        $('.recurso_'+RecursoID).each(function(){
                                            suma = suma+parseFloat(getNum($(this).val()));
                                            console.log(parseFloat($(this).val()));
                                        });
                                        console.log(suma.toFixed(2));
                                    }
                                    */
                                    
                                    if (RecursoMetaFisica<suma.toFixed(2)) {
                                        bootbox.alert('La cantidad supera a la meta fisica general.');
                                        $this.val('0.00');
                                        return false;
                                    }
                                    if (parseFloat(RecursoMetaFisica)==suma.toFixed(2))
                                    {
                                        $('.recurso_alerta_'+RecursoID).css("color", "black");
                                    }
                                    else
                                    {
                                        $('.recurso_alerta_'+RecursoID).css("color", "red");
                                    }
                                    
                                    var MetaFisica=$this.val();
                                    $.ajax({
                                        cache: false,
                                        type: 'GET',
                                        url: '<?= \Yii::$app->request->BaseUrl ?>/recursos-poa/grabar-meta-cronograma-recurso',
                                        data: {ID:ID,MetaFisica:MetaFisica},
                                        success: function (data) {
                                            var jx = JSON.parse(data);
                                            if (jx.Success == true) {
                                            //if (data.Success) {
                                                $this.css('background', 'green');
                                                //loadPoaf();
                                                _pageLoadingEnd();
                                            } else {
                                                //toastr.error(data.Error);
                                            }
                                        },
                                        error: function () {
                                            _pageLoadingEnd();
                                        }
                                    });
                                    return true;
                                });

                                $('body').on('change', '.input-poaf-crono-act-mfis', function (e) {
                                    e.preventDefault();
                                    $(this).attr('changed', 'changed');
                                });
                                
                                
                                $('body').on('click', '.btn-enviar-evaluacion-poa', function (e) {
                                    e.preventDefault();
                                    var $this = $(this);
                                    var codigo="<?= $usuario->username ?>";
                                    var error='';
                                    
                                    $('.recurso-meta-fisica').each(function(){
                                        
                                        var suma=0;
                                        var RecursoID=$(this).attr('data-recurso-id');
                                        var RecursoMetaFisica=parseFloat($(this).attr('data-meta-fisica'));
                                        $('.recurso_'+RecursoID).each(function(){
                                            suma = suma+parseFloat(getNum($(this).val()));
                                            
                                        });
                                        
                                        if (RecursoMetaFisica.toFixed(2)>suma.toFixed(2)) {
                                            
                                            error=error+'a';
                                        }
                                    });
                                    
                                    
                                    if (error!='') {
                                        bootbox.alert('Falta programar algunos recurso');
                                        return false;
                                    }
                                    bootbox.alert('Su programación de recursos, esta correcta');
                                    
                                });

                                $('body').on('click', '.input-sub-total', function (e) {
                                    e.preventDefault();
                                    var $this = $(this);
                                    var acumusubtotal = 0.00;
                                    $this.closest('.tr-sub-tot').find('.input-sub-total').each(function () {
                                        var _valor = parseFloat($(this).inputmask('unmaskedvalue'));
                                        if (!isNaN(_valor)) {
                                            acumusubtotal += _valor;
                                        }
                                    });
                                    $this.closest('.tr-sub-tot').find('.input-act-tot-mfs').val(acumusubtotal);
                                });

                                $('body').on('click', '.input-sub2-total', function (e) {

                                    e.preventDefault();
                                    var $this = $(this);
                                    var acumusubtotal = 0.00;
                                    $this.closest('.tr-sub2-tot').find('.input-sub2-total').each(function () {

                                        var _valor = parseFloat($(this).inputmask('unmaskedvalue'));
                                        if (!isNaN(_valor)) {
                                            acumusubtotal += _valor;
                                        }
                                    });
                                    $this.closest('.tr-sub2-tot').find('.input-act-tot-mfss').val(acumusubtotal);
                                });

                                $('body').on('change', '.input-poaf-crono-arescat-mfin', function (e) {
                                    e.preventDefault();
                                    $(this).attr('changed', 'changed');
                                    
                                });
                                
                                function getProyecto() {
                                    var url = "<?= \Yii::$app->request->BaseUrl ?>/recursos-poa/recursos-json?codigo=<?= $usuario->username ?>";
                                    return $.getJSON( url );
                                }

                                function _drawTable() {
                                   console.time('Test performance');
                                    var situacionRecurso=<?= \Yii::$app->user->identity->SituacionRecurso ?>;
                                    var html = '';
                                    var cantmeses = _proyecto.Cronogramas;
                                    for (var i_comp = 0; i_comp < _proyecto.Componentes.length; i_comp++) {
                                        var comp = _proyecto.Componentes[i_comp];
                                        
                                        html += '<div class="panel panel-gray">';
                                        html += '<div class="panel-heading">';
                                        html += '<h4 class="title-obj panel-title">';
                                        html += (i_comp + 1) + '. Objetivo específico - ' + comp.Nombre ;
                                        html += '</h4></div>';
                                        
                                        html += '<div class="panel-heading">';
                                        html += '<h4 class="panel-title">';
                                        html += 'Total de Objetivo: <span class="input-number" style="font-size:15px !important">'+comp.TotalObjetivo+'</span>' ;
                                        html += '</h4></div>';
                                        
                                        html += '<div class="panel-body">';
                                        html += '<div class="table-responsive">';
                                        html += '<table class="table table-bordered table-condensed table-act">';
                                        html += '<tbody>';
                                        for (var i_act = 0; i_act < comp.Actividades.length; i_act++)
                                        {
                                            var act = comp.Actividades[i_act];
                                            
                                            var TotalActividad=0;
                                            for (var i_are = 0; i_are < act.ActRubroElegibles.length; i_are++) {
                                                var are = act.ActRubroElegibles[i_are];
                                                if (are.AreSubCategorias.length) {
                                                    for (var i_arescat = 0; i_arescat < are.AreSubCategorias.length; i_arescat++) {
                                                        var arescat = are.AreSubCategorias[i_arescat];
                                                        if (arescat) {
                                                            TotalActividad=TotalActividad+(arescat.CostoUnitario*arescat.MetaFisica);
                                                        }
                                                    }
                                                }
                                            }
                                                
                                            html += '<tr class="tr-header">';
                                            html += '<td>#</td><td>Código Matriz</td><td>Actividad</td><td>Específica</td><td>Unidad de medida</td><td>Costo unitario</td><td>Meta física</td><td>Total</td>';
                                            var m=1;
                                            for (var i_crono_act = 0; i_crono_act < cantmeses.length; i_crono_act++) {
                                                var compAa = _proyecto.Cronogramas[i_crono_act];
                                                html += '<td> Mes ' + compAa.MesDescripcion + '</td>';
                                                m++;
                                            }
                                            html += '</tr>';

                                            html += '<tr class="tr-act-mf info" data-act-id="' + act.ID + '">';
                                            html += '<td>' + (i_comp + 1) + '.' + (i_act + 1) + '</td><td></td><td><b>' + act.Nombre + '</b></td><td></td><td>' + act.UnidadMedida+'</td><td><input type="text" readonly="" class="input-number form-control" value="'+(TotalActividad/act.MetaFisica)+'"></td>'
                                                    + '<td align="center">' + act.MetaFisica + '</td><td><input type="text" readonly="" class="input-number form-control" value="'+TotalActividad+'"></td>';
                                                    html += '<td colspan="'+m+'"></td>';
                                            html += '</tr>';
                                            
                                            // Rubros Elegibles
                                            html += '<tr>';
                                            if (act.ID) {
                                                html += '<td colspan="7"><a href="javascript:void(0);" id="btn-add-recursos"  data-act-idd="'+ act.ID +'" style="margin: 5px;" class="btn btn-primary btn-sm btn-actv-rb"><i class="fa fa-plus"></i>&nbsp;Agregar Recursos</a> </td>';
                                            }
                                            html += '</tr>';
                                            for (var i_are = 0; i_are < act.ActRubroElegibles.length; i_are++) {
                                                
                                                var are = act.ActRubroElegibles[i_are];
                                                var TotalRubroElegible=0;
                                                if (are.AreSubCategorias.length) {
                                                    for (var i_arescat = 0; i_arescat < are.AreSubCategorias.length; i_arescat++) {
                                                        var arescat = are.AreSubCategorias[i_arescat];
                                                        if (arescat) {
                                                            TotalRubroElegible=TotalRubroElegible+(arescat.CostoUnitario*arescat.MetaFisica);
                                                        }
                                                    }
                                                }
                                                html += '<tr class="tr-sub2-tot success">';
                                                html += '<td></td>';
                                                html += '<td class="text-left" colspan="2">' + are.RubroElegible.Nombre + '</td>'
                                                html += '<td colspan="4"></td>'
                                                html += '<td><input type="text" class="form-control input-number" readonly="" value="'+TotalRubroElegible+'" /></td>';
                                                //html += '<td colspan="'+cantmeses.length+'"></td>';
                                                var m=1;
                                                for (var i_crono_act = 0; i_crono_act < cantmeses.length; i_crono_act++) {
                                                    var compAa = _proyecto.Cronogramas[i_crono_act];
                                                    html += '<td> Mes ' + compAa.MesDescripcion + '</td>';
                                                    m++;
                                                }
                                                html += '</tr>';
                                                // Subcategorías
                                                if (are.AreSubCategorias.length) {
                                                    for (var i_arescat = 0; i_arescat < are.AreSubCategorias.length; i_arescat++) {
                                                        var arescat = are.AreSubCategorias[i_arescat];
                                                        if (arescat) {
                                                            html += '<tr class="tr-subact warning"><td align="center">';
                                                            if (arescat.Situacion==0) {
                                                                html += '<a href="#" class="btn-edit-recurso" data-toggle="tooltip" title="" data-act-idd="'+act.ID+'" data-id="'+arescat.ID+'" data-original-title="Editar"><i class="fa fa-edit fa-lg"></i></a>'
                                                                      + '<a href="#" class="btn-remove-recurso" data-toggle="tooltip" title="Eliminar" data-act-idd="'+act.ID+'" data-id="'+arescat.ID+'"><i class="fa fa-remove fa-lg"></i></a>';
                                                            }
                                                            
                                                            if (situacionProyecto==3 && arescat.Situacion==3) {
                                                                html += '<a href="#" class="btn-edit-recurso" data-toggle="tooltip" title="" data-act-idd="'+act.ID+'" data-id="'+arescat.ID+'" data-original-title="Editar"><i class="fa fa-edit fa-lg"></i></a>';
                                                                
                                                                html +='<span data-toggle="tooltip" title="'+arescat.Observacion+'"><i class="fa fa-eye"></i></span>'
                                                                     +' <a href="#" data-toggle="tooltip" data-id="'+arescat.ObservacionID+'" class="btn-aprobar-recursos"><i class="fa fa-check"></i></a>';
                                                            }
                                                            var alertaRecurso="red";
                                                            var  metaFisicaRecurso1=0;
                                                            for (var i_crono_arescat1 = 0; i_crono_arescat1 < cantmeses.length; i_crono_arescat1++) {
                                                                if (arescat.Cronogramas[i_crono_arescat1].MetaFisica!=null) {
                                                                    metaFisicaRecurso1=parseFloat(metaFisicaRecurso1)+parseFloat(arescat.Cronogramas[i_crono_arescat1].MetaFisica);
                                                                }
                                                            }
                                                           
                                                            if (parseFloat(arescat.MetaFisica).toFixed(2)==parseFloat(metaFisicaRecurso1).toFixed(2)) {
                                                                alertaRecurso="black";
                                                            }

                                                            var deshabilitar="disabled";
                                                            
                                                            if ((arescat.Situacion==0 || arescat.Situacion==3) && (situacionRecurso==0 || situacionRecurso==3)) {
                                                                var deshabilitar="";
                                                            }

                                                            html += '</td><td>' + arescat.Matriz + '</td><td style="color:'+alertaRecurso+'" class="recurso_alerta_'+arescat.ID+'">' + arescat.Nombre + '</td><td>'+arescat.Especifica+'</td><td>' + arescat.UnidadMedida
                                                                    + '</td><td><input type="text" class="form-control input-number" readonly="" value="' + arescat.CostoUnitario + '" />'
                                                                    + '</td><td><input type="text" style="color:'+alertaRecurso+'" class="recurso_alerta_'+arescat.ID+' form-control  input-number-sm recurso-meta-fisica" data-meta-fisica="'+ arescat.MetaFisica +'" data-recurso-id="'+ arescat.ID +'" readonly="" value="' + arescat.MetaFisica + '" />'
                                                                    
                                                                    + '</td><td><input type="text" class="form-control input-number" readonly="" value="'+(arescat.CostoUnitario*arescat.MetaFisica)+'" /></td>';
                                                            for (var i_crono_arescat = 0; i_crono_arescat < cantmeses.length; i_crono_arescat++) {
                                                                var deshabilitar="disabled";
                                                                //if ((arescat.Cronogramas[i_crono_arescat].Situacion==0 || arescat.Cronogramas[i_crono_arescat].Situacion==3) && (situacionRecurso==0 || situacionRecurso==3)) {
                                                                if ((arescat.Cronogramas[i_crono_arescat].Situacion==0 || arescat.Cronogramas[i_crono_arescat].Situacion==3)) {
                                                                    deshabilitar="";
                                                                }
                                                                
                                                                if (arescat.Cronogramas[i_crono_arescat]) {
                                                                    var tot = 0.00;
                                                                    if(arescat.Cronogramas[i_crono_arescat].MetaFisica != '.00'){
                                                                        tot = arescat.Cronogramas[i_crono_arescat].MetaFisica;
                                                                    }
                                                                    var compAa = _proyecto.Cronogramas[i_crono_arescat];
                                                                    
                                                                    html += '<td><input '+deshabilitar+' title="Mes '+compAa.MesDescripcion+'" type="text"'
                                                                        + 'data-recurso-id="'+ arescat.ID +'"'
                                                                        + 'data-meta-fisica="'+ arescat.MetaFisica +'"'
                                                                        + 'data-cronograma-recurso-id="'+ arescat.Cronogramas[i_crono_arescat].ID +'"'
                                                                        +'class="form-control recurso_'+arescat.ID+' btn-save-cronograma-recurso"'
                                                                        +'value="'+ tot + '" /></td>';
                                                                }
                                                            }
                                                        }
                                                        html += '</tr>';
                                                    }
                                                }
                                            }
                                        }
                                        // Totales por componente
                                        html += '</tbody>';
                                        html += '</table>';
                                        html += '</div>';
                                        html += '</div>';
                                        html += '</div>';
                                    }
                                    
                                    $('#table-container').html(html);
                                    if (situacionRecurso!=0) {
                                        $(".btn-actv-rb").hide();
                                    }
                                    
                                    $('.table-responsive').css('max-height', $(window).height() * 0.60);
                                    validarNumeros();

                                    console.timeEnd('Test performance');

                                }
                            </script>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal-ctrl-recurso" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Recursos</h4>
                        </div>
                        <div class="modal-body-main">
                        </div>
                    </div>
                </div>
            </div>

            <div id="modal-add-pc" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body-main">
                        </div>
                    </div>
                </div>
            </div>

            <div id="modal-create-indpc" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Indicador de paso crítico</h4>
                        </div>
                        <div class="modal-body-main">
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="modal fade" id="modal-ctrl-codificacion" tabindex="-1" role="dialog" >
                <div class="modal-dialog modal-lg" >
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Recursos</h4>
                        </div>
                        <div class="modal-body-main">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="modal-control-presupuesto" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Consolidado por clasificador por monto</h4>
                        </div>
                        <div class="modal-body-main">
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>







<script type="text/javascript">
    
    $('body').on('click', '.presupuesto', function (e) {
        e.preventDefault();
        var CodigoProyecto = '<?= $informacionGeneral->Codigo ?>';
        $('#modal-control-presupuesto .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/recursos-poa/presupuesto?CodigoProyecto='+CodigoProyecto);
        $('#modal-control-presupuesto').modal('show');
    });
    
    
    $('body').on('click', '.btn-remove-recurso', function (e) {
        e.preventDefault();
        var $this = $(this);
        bootboxConfirmEliminar($this,
            'Está seguro de eliminar el recurso?',
            function () {
                var id = $this.attr('data-id');
                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                $.get('<?= \Yii::$app->request->BaseUrl ?>/recursos-poa/delete-recurso', { id: id ,_csrf: toke }, function (data) {
                    var jx = JSON.parse(data);
                    if (jx.Success == true) {
                        $("#div-presupuesto").html('');
                        getProyecto().done(function(json) {
                            _proyecto = json;
                            _drawTable();
                        });
                    }
                });
            });
    });
    
    $('body').on('click', '.btn-edit-recurso', function (e) {
        e.preventDefault();
        var idActv = $(this).attr('data-act-idd');
        var idInvestigador = <?= $investigadorID ?>;
        var id=$(this).attr('data-id');
        $('#modal-ctrl-recurso .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/recursos-poa/recursos-actualizar?id='+id+'&actv='+idActv+'&investigador='+idInvestigador);
        $('#modal-ctrl-recurso').modal('show');
    });

    $('body').on('click', '#btn-save-recurso-p', function (e) {
        
        e.preventDefault();
        
        var $btn = $(this);
        $btn.attr('disabled', 'disabled');
        var isValid = formParsleyRecursos.validate();
        if (isValid) {
            sendFormNoMessage($('#frmRecursos'), $(this), function (msg) {
                if (msg.incorrecto) {
                    bootbox.alert(msg.incorrecto);
                }
                
                $("#div-presupuesto").html('');
                getProyecto().done(function(json) {
                    _proyecto = json;
                    _drawTable();
                });
                $("#modal-ctrl-recurso").modal('hide');
                _pageLoadingEnd();
		
            });
        } else {
            console.log("b");
        }
    });
    
    $('body').on('click', '.nuevo', function (e) {
        e.preventDefault();
        var idActv = $(this).attr('data-act-idd');
        var idInvestigador = <?= $investigadorID ?>;
        $('#modal-ctrl-codificacion').modal('hide');
        $('#modal-ctrl-recurso .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/recursos-poa/recursos?id='+idActv+'&investigador='+idInvestigador);
        $('#modal-ctrl-recurso').modal('show');
    });
    
    $('body').on('click', '.seleccionar_poa', function (e) {
        e.preventDefault();
        var idActv = $(this).attr('data-act-idd');
        var investigadorid = <?= $investigadorID ?>;
        $('#modal-ctrl-codificacion').modal('hide');
        
        
        $('.fila').each(function(){
            var id=$(this).attr('data-id');
            
            var rubroelegibleid=$(this).attr('data-rubro-elegible');
            var codificacion=$(this).attr('data-codificacion');
            console.log(rubroelegibleid);
            console.log(codificacion);
            var toke = '<?=Yii::$app->request->getCsrfToken()?>';
            
            
            $.get('<?= \Yii::$app->request->BaseUrl ?>/recursos-poa/get-datos-recurso', { valor: ''+rubroelegibleid+'.'+codificacion+'' ,investigadorid:investigadorid,_csrf: toke }, function (data) {
                var jx = JSON.parse(data);
                if (jx.Success == true) {
                    $('#modal-ctrl-recurso .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/recursos-poa/recursos?id='+idActv+'&investigador='+investigadorid+'&valor='+''+rubroelegibleid+'.'+codificacion+'');
                    $('#modal-ctrl-recurso').modal('show');
                    
                    $('#modal-ctrl-recurso #nombre').val(jx.Nombre);
                    $('#modal-ctrl-recurso #especifica').val(jx.Especifica);
                    $('#modal-ctrl-recurso #costo-unitario').val(jx.CostoUnitario);
                    $('#modal-ctrl-recurso #unidad-medida').val(jx.UnidadMedida);
                    $('#modal-ctrl-recurso #entidad-participante-id').val(jx.EntidadParticipanteID);
                    $('#modal-ctrl-recurso #fondo-entidad-participante-id').val(jx.FondoEntidadParticipanteID);
                    $('#modal-ctrl-recurso #codificacion').val(jx.Codificacion);
                }
            });
        });
    });
    
    
    var situacionProyecto=<?= \Yii::$app->user->identity->SituacionProyecto ?>;
    if (situacionProyecto==1 || situacionProyecto==2) {
        $(".btn-enviar-evaluacion-poa").hide();
    }
    
    $('body').on('click', '.btn-actv-rb', function (e) {
        e.preventDefault();
        var idActv = $(this).attr('data-act-idd');
        var idInvestigador = <?= $investigadorID ?>;
        
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        var CodigoProyecto="<?= $informacionGeneral->Codigo ?>";
        $('#modal-ctrl-codificacion .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/recursos-poa/codificacion?CodigoProyecto='+CodigoProyecto+'&ActividadID='+idActv+'&_csrf='+toke);
        $('#modal-ctrl-codificacion').modal('show');
    });
    
</script>




<!--


<div id="modal-loading" class="modal fade modal-loading in" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-hidden="false" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content-no-shadow modal-content">
            <div class="text-center"><i class="fa fa-spinner fa-pulse fa-fw" style="color:#16a085;font-size: 100px;"></i></div>
        </div>
    </div>
</div>
-->