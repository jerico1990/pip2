<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<div id="page-content" style="min-height: 1342px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Carga rapida</h1>
        </div>
        
        <div class="container">
<?php $form = ActiveForm::begin(
    [
        'options'           => [
            'enctype'       =>'multipart/form-data',
        ]
    ]); ?>
    <div class="modal-body">
        <div class="form-horizontal">
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Objetivo:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <select class="form-control" name="ActRubroElegible[ComponenteID]" id="terminoreferencia-componenteid" onchange="Actividad($(this).val())" required>
                        <option>[SELECCIONE]</option>
                        
                        <?php foreach($componentes as $componente){ ?>
                            <option value="<?= $componente->ID ?>"> <?= $componente->Correlativo."-".$componente->Nombre ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Actividad:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <select class="form-control" name="ActRubroElegible[ActividadID]" id="terminoreferencia-actividadid" onchange="Recurso($(this).val())" required>
                        
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Documento:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input class="form-control" type="file" name="file">
                </div>
            </div>
            <div class="form-group">
                1:Personal y Servicios<br>
                2:Pasajes y viáticos<br>
                3:Insumos y materiales<br>
                4:Bienes y Equipos<br>
                5:Capacitación, divulgación y comunicaciones<br>
                6:Imprevistos<br>
            </div>
            
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<?php
    $actividades=Yii::$app->getUrlManager()->createUrl('recursos-poa/actividades');
?>
<script>
    
    function Actividad(valor) {
        var ComponenteID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $actividades ?>',
            type: 'POST',
            async: false,
            data: {ComponenteID:ComponenteID,_csrf: toke},
            success: function(data){
               $( "#terminoreferencia-actividadid" ).html( data );
            }
        });
    }
    
</script>