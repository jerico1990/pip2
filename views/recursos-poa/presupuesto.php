<style>
    .table > tbody > .tr-header > td {
        text-align: center;
        font-size: 14px;
        font-weight: bold;
        padding: 1px !important;
        vertical-align: middle !important;
        /*border: 1px solid #cfcfd0;
        background-color: #f0f0f1;*/
        border: 1px solid #c0c0c0;
        background-color: #e3e3e3;
        min-width: 75px !important;
    }
</style>
<div class="modal-body">
    <div class="form-horizontal">
        <div class="table-responsive">
            <table class="table table-bordered table-condensed table-act">
                <tbody>
                    <tr class="tr-header">
                        <td>Rubro</td>
                        <td>% Base Concurso</td>
                        <td>S/ Monto límite base</td>
                        <td>% Programado del Proyecto</td>
                        <td>S/ Monto programado del proyecto</td>
                    </tr>
                    <?php $tot=0; ?>
                    <?php foreach($resultados as $resultado){ ?>
                    <tr>
                        <td><?= $resultado["Nombre"] ?></td>
                        <td class="text-right"><?= $resultado["LimiteSegunBaseConcurso"] ?></td>
                        <td class="text-right"><?= number_format($resultado["TotalSegunBase"], 2, '.', ' ')  ?></td>
                        <?php if($resultado["LimiteSegunBaseConcurso"] < $resultado["LimiteActual"]){ ?>
                            <td class="text-right fondo_red" ><?= number_format($resultado["LimiteActual"], 2, '.', ' ')  ?> </td>
                            <td class="text-right fondo_red"><?= number_format($resultado["TotalRubro"], 2, '.', ' ') ?></td>
                        <?php } else { ?>
                        <td class="text-right"><?= number_format($resultado["LimiteActual"], 2, '.', ' ')  ?> </td>
                        <td class="text-right"><?= number_format($resultado["TotalRubro"], 2, '.', ' ') ?></td>
                        <?php } ?>
                    </tr>
                    <?php $tot=$tot+$resultado["TotalRubro"]; ?>
                    <?php } ?>
                    <tr>
                        <td class="text-right" colspan="4">Total:</td>
                        <td class="text-right">S/ <?= number_format($tot, 2, '.', ' ') ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <h4>Consolidado de Objetivos</h4>
    <div class="form-horizontal">
        <div class="table-responsive">
            <table class="table table-bordered table-condensed table-act">
                <tbody>
                    <tr class="tr-header">
                        <td>#</td>
                        <td>Nombre</td>
                        <td width="100">Monto</td>
                    </tr>
                    <?php $tot=0; ?>
                    <?php foreach($totalObjetivo as $obj){ ?>
                    <tr>
                        <td><?= $obj["Correlativo"] ?></td>
                        <td class="text-left"><?= $obj["Nombre"] ?></td>
                        <td class="input-number"><?= number_format($obj["Total"], 2, '.', ' ')  ?></td>
                    </tr>
                    <?php $tot=$tot+$obj["Total"]; ?>
                    <?php } ?>
                    <tr>
                        <td class="text-right" colspan="2">Total:</td>
                        <td class="text-right">S/ <?= number_format($tot, 2, '.', ' ') ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    
</div>