<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<div id="page-content" style="min-height: 1342px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Carga rapida</h1>
        </div>
        
        <div class="container">
<?php $form = ActiveForm::begin(
    [
        'options'           => [
            'enctype'       =>'multipart/form-data',
        ]
    ]); ?>
    <div class="modal-body">
        <div class="form-horizontal">
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Objetivo:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <select class="form-control" name="ActRubroElegible[ComponenteID]" id="terminoreferencia-componenteid" onchange="Actividad($(this).val())" required>
                        <option>[SELECCIONE]</option>
                        
                        <?php foreach($componentes as $componente){ ?>
                            <option value="<?= $componente->ID ?>"> <?= $componente->Correlativo."-".$componente->Nombre ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Actividad:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <select class="form-control" name="ActRubroElegible[ActividadID]" id="terminoreferencia-actividadid" onchange="Recurso($(this).val())" required>
                        
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Clasificador:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <select class="form-control" name="ActRubroElegible[RubroElegibleID]" required="">
                        <option value="">[SELECCIONE]</option>
                        <option value="1">Personal y Servicios</option>
                        <option value="2">Pasajes y viáticos</option>
                        <option value="3">Insumos y materiales</option>
                        <option value="4">Bienes y Equipos</option>
                        <option value="5">Capacitación, divulgación y comunicaciones</option>
                        <option value="6">Imprevistos</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Documento:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input class="form-control" type="file" name="file">
                </div>
            </div>
            
            <div class="form-group">
                <button type="button" id="agregar" class="btn btn-primary">Agregar</button><br><br>
                <table class="table borderless table-hover" id="detalle_tabla" border="0">
                    <thead>
                        <tr>
                            <th>Nombre Recurso</th>
                            <th>Específica</th>
                            <th>Costo Unitario</th>
                            <th>Unidad de medida</th>
                            <th>Meta Fisica</th>
                            <th width="22">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr id='detalle_1'></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<?php
    $actividades=Yii::$app->getUrlManager()->createUrl('recursos-poa/actividades');
    $recursos=Yii::$app->getUrlManager()->createUrl('termino-referencia/recursos');
?>
<script>
    
    function Actividad(valor) {
        var ComponenteID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $actividades ?>',
            type: 'POST',
            async: false,
            data: {ComponenteID:ComponenteID,_csrf: toke},
            success: function(data){
               $( "#terminoreferencia-actividadid" ).html( data );
            }
        });
    }
    
    function Recurso(valor) {
        var ActividadID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $recursos ?>',
            type: 'POST',
            async: false,
            data: {ActividadID:ActividadID,_csrf: toke},
            success: function(data){
               $( "#terminoreferencia-areasubcategoriaid" ).html( data );
            }
        });
    }
    
    
    detalle=1;
    $("#agregar").click(function(){
	//var proveedores=$('input[name=\'SolicitudCompromisoPresupuestal[Proveedores][]\']').length;
	
	var error = '';
	
	if (error != '') {
	    $("#error_fuente_origen").html(error);
            return false;
	}
	else
        {
	    var option = null;
            $('#detalle_'+detalle).html(
                                            '<td>'+
                                                '<input class="form-control" type="text" name="AreSubCategoria[Recursos][]"  >'+
					    '</td>'+
					    '<td>'+
                                                '<input class="form-control" type="text" name="AreSubCategoria[Especificas][]">'+
					    '</td>'+
					    '<td>'+
                                                '<input class="form-control" type="text" name="AreSubCategoria[CostosUnitarios][]">'+
					    '</td>'+
                                            '<td>'+
                                                '<input class="form-control" type="text" name="AreSubCategoria[UnidadesMedidas][]">'+
					    '</td>'+
                                            '<td>'+
                                                '<input class="form-control" type="text" name="AreSubCategoria[MetasFisicas][]">'+
					    '</td>'+
					    '<td>'+
						'<span class="eliminar fa fa-remove fa-lg" >'+
						'</span>'+
					    '</td>');
            $('#detalle_tabla').append('<tr id="detalle_'+(detalle+1)+'"></tr>');
            detalle++;
            return true;
        }
    });
    
    $("#detalle_tabla").on('click','.eliminar',function(){
        var r = confirm("Estas seguro de Eliminar?");
        var mensaje = '';
        if (r == true) {
            id=$(this).children().val();
            if (id) {
                
                $(this).parent().parent().remove();
            }
            else
            {
                $(this).parent().parent().remove();    
            }   
            mensaje = "Se elimino el Registro Correctamente";
        } 
    });
</script>