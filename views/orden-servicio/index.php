<div id="page-content" style="min-height: 754px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Orden de Servicio</h1>
        </div>
        <div class="container">
            
            <!-- ORDENES -->
            <style>
                .panel .panel-body {
                    padding: 10px !important;
                }

                .panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 1px solid #a8a8ab !important;
                }

                .panel-gray .panel-body {
                    border-top: none !important;
                }

                .btn-sm {
                    padding: 5px 10px !important;
                    line-height: 1.5 !important;
                }

                .input-comp-nombre {
                    text-transform: uppercase;
                }

                .tbl-act > tbody > .tr-header > td {
                    text-align: center;
                    font-size: 12px;
                    font-weight: bold;
                    padding: 5px !important;
                    vertical-align: middle !important;
                    /*border: 1px solid #cfcfd0;
                    background-color: #f0f0f1;*/
                    border: 1px solid #c0c0c0;
                    background-color: #e3e3e3;
                }

                /*.tbl-act .tr-item td {
                    padding: 2px !important;
                }*/
                input.form-control, select.form-control {
                    /*height: 28px !important;*/
                }

                .tbl-act .tr-act .input-nombre {
                    min-width: 320px !important;
                }

                .tbl-act .tr-act .input-unidadmedida {
                    min-width: 100px !important;
                }

                .input-number {
                    min-width: 92px !important;
                }

                .btn-add-re {
                    float: right;
                }

                .tbl-act > tbody > tr > td:nth-child(2) {
                    min-width: 40px !important;
                    text-align: center !important;
                }

                .tr-comp input, .tr-proy input {
                    font-weight: bold;
                }

                .th-proy {
                    min-width: 630px !important;
                }
            </style>

             <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4>Orden de Servicio</h4>
                </div>
                <div class="panel-body">

                    <div id="div-colaboradoras-table-main">
	                    <button type="button" id="btn-add-colaboradora" class="btn btn-midnightblue-alt"><i class="fa fa-plus"></i>&nbsp;<span>Crear Orden de Servicio</span></button>
	                    <br><br>
	                    <div id="div-colaboradoras-table">
		                    <div class="table-responsive">
							    <table class="table table-bordered table-condensed table-striped table-hover">
							        <thead>
							            <tr>
							                <th>Acciones</th>
							                <th>Razon Social</th>
							                <th>RUC</th>
							                <th>Tipo</th>
							                <th>Fecha de creación</th>
							            </tr>
							        </thead>
									<tbody>
							        	<tr>
							                <td class="td-acciones" colspan="5">
												No hay ordenes generadas							                    
							                </td>
							            </tr>
							        </tbody>
							    </table>
							</div>
						</div>
	                </div>

                </div>
            </div>
            <!-- END ORDENES -->
        </div>
    </div>
</div>

