<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'entidad-participante/crear',
        'options'           => [
            'autocomplete'  => 'off',
            'id'            => 'frmColaboradora'
        ]
    ]
); ?>

    <div class="panel panel-sky">
        <div class="panel-heading">
            <h4>Aspectos Generales</h4>
            <div class="btn-heading">
                <a id="btn-back-colaboradora" href="javascript:void(0);" class="btn btn-midnightblue"><i class="fa fa-arrow-left"></i>&nbsp;Regresar</a>
            </div>
        </div>
        <div class="panel-body">
            <div id="container-resp"></div>
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-2 control-label">N° RUC <span class="f_req">*</span></label>
                    <div class="col-sm-3">
                        <input class="form-control" id="Ruc" onKeyPress="return soloNumeros(event)"  maxlength="11" minlength="11" name="EntidadParticipante[Ruc]" required="" type="text" onfocusout="RUC($(this).val())" value="<?= $Model->Ruc ?>">
                    </div>
                    <label class="col-sm-2 control-label">Nombre o Razón Social <span class="f_req">*</span></label>
                    <div class="col-sm-5">
                        <input class="form-control" id="RazonSocial" name="EntidadParticipante[RazonSocial]" required="" type="text" value="<?= $Model->RazonSocial ?>" disabled >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Iniciales o Siglas <span class="f_req">*</span></label>
                    <div class="col-sm-3">
                        <input class="form-control text-uppercase" id="Siglas" maxlength="20" name="EntidadParticipante[Siglas]" required="" type="text" value="<?= $Model->Siglas ?>">
                        <h6 class="pull-left count-message"></h6>
                    </div>
                    <label class="col-sm-2 control-label">Tipo de Institución <span class="f_req">*</span></label>
                    <div class="col-sm-5">
                        <select class="form-control" data-val="true" data-val-number="The field TipoInstitucionID must be a number." data-val-required="El campo TipoInstitucionID es obligatorio." id="TipoInstitucionID" name="EntidadParticipante[TipoInstitucionID]" required="required">
                            <option value="">[ SELECCIONE ]</option>
                            <?php foreach($TipoInstitucion as $institucion){?>
                                <option value="<?= $institucion->ID ?>" <?= $Model->TipoInstitucionID == $institucion->ID ? 'selected':'' ?>><?= $institucion->Nombre ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Régimen <span class="f_req">*</span></label>
                    <div class="col-sm-6" data-content-group="">
                        <label><input <?php echo $Model->Regimen == 'Publico' ? 'checked' : '' ?> id="Regimen" name="EntidadParticipante[Regimen]" type="radio" value="Publico"> Público</label>
                        <label><input <?php echo $Model->Regimen == 'Privado' ? 'checked' : '' ?> id="Regimen" name="EntidadParticipante[Regimen]" type="radio" value="Privado"> Privado</label>
                        <label><input <?php echo $Model->Regimen == 'Persona Natural' ? 'checked' : '' ?> id="Regimen" name="EntidadParticipante[Regimen]" type="radio" value="Persona Natural"> Persona Natural</label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Procedencia <span class="f_req">*</span></label>
                    <div class="col-sm-2" data-content-group="">
                        <div class="radio block"><label><input <?php echo $Model->Procedencia == 'Nacional' ? 'checked' : '' ?> id="Procedencia" name="EntidadParticipante[Procedencia]" type="radio" value="Nacional"> Nacional</label></div>
                    </div>
                    <label class="col-sm-3 control-label">Fecha de creación o constitución <span class="f_req">*</span></label>
                    <div class="col-sm-2">
                        <input class="form-control fechas" id="FechaCreacion" name="EntidadParticipante[FechaCreacion]" required="" type="text" value="<?= !empty($Model->FechaCreacion)?Yii::$app->tools->dateFormat($Model->FechaCreacion,'d/m/Y'): date('d/m/Y') ?>" data-parsley-required="true">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Domicilio Fiscal  <span class="f_req">*</span></label>
                    <div class="col-sm-7">
                        <input class="form-control" id="DomicilioFiscal" maxlength="250" name="EntidadParticipante[DomicilioFiscal]" required="" type="text" value="<?= $Model->DomicilioFiscal ?>" disabled>
                        <!--<h6 class="pull-left count-message"></h6>-->
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Teléfono <span class="f_req">*</span></label>
                    <div class="col-sm-3">
                        <input class="form-control" id="Telefono" onKeyPress="return soloNumeros(event)" maxlength="20" name="EntidadParticipante[Telefono]" required="" type="text" value="<?= $Model->Telefono ?>">
                        <!--<h6 class="pull-left count-message"></h6>-->
                    </div>
                    <label class="col-sm-2 control-label">Correo electrónico <span class="f_req">*</span></label>
                    <div class="col-sm-5">
                        <input class="form-control" data-parsley-type="email" id="Email" maxlength="50" name="EntidadParticipante[Email]" required="" type="text" value="<?= $Model->Email ?>">
                        
                    </div>
                </div>
                <div class="form-group">
                    
                </div>
                <!--
                <div class="form-group">
                    <label class="col-sm-3 control-label">Página web </label>
                    <div class="col-sm-6">
                        <input class="form-control" id="PaginaWeb" maxlength="100" name="EntidadParticipante[PaginaWeb]" type="text" value="<?= $Model->PaginaWeb ?>">
                        <h6 class="pull-left count-message"></h6>
                    </div>
                </div>
                -->
            </div>
        </div>
    </div>


    <div class="panel panel-sky">
        <div class="panel-heading">
            <h4>Fondo Recibido o Administrado</h4>
        </div>
        <div class="panel-body">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Aporte Monetario S/.</label>
                    <div class="col-sm-4">
                        <input class="form-control input-number" id="AporteMonetario" onKeyPress="return soloNumeros(event)" maxlength="10" name="EntidadParticipante[AporteMonetario]" required="" type="text" value="<?= $Model->AporteMonetario ?>">
                        <!--<h6 class="pull-left count-message"></h6>-->
                    </div>
                    <label class="col-sm-2 control-label">Aporte No Monetario S/.</label>
                    <div class="col-sm-4">
                        <input class="form-control input-number" id="AporteNoMonetario" onKeyPress="return soloNumeros(event)" maxlength="10" name="EntidadParticipante[AporteNoMonetario]" required="" type="text" value="<?= $Model->AporteNoMonetario ?>">
                        <!--<h6 class="pull-left count-message"></h6>-->
                    </div>
                    
                </div>
            </div>
        </div>
    </div>


    <div class="panel panel-sky">
        <div class="panel-heading">
            <h4>Representantes Legales </h4>
        </div>
        <div class="panel-body">
            <button type="button" id="btn-add-replegal-colaboradora" class="btn btn-midnightblue-alt hidden" data-id="<?= $Model->ID ?>"><i class="fa fa-plus"></i>&nbsp;<span>Agregar Representante Legal</span></button>
            <br><br>
            <div id="div-tablereplegal-colaboradora">
                <div class="alert alert-info">
                    <i class="fa fa-info"></i>&nbsp;&nbsp;No se registraron representantes legales.
                </div>
            </div>
        </div>
    </div>
    <div class="panel-footer text-center">
        <button type="button" id="btn-save-colaboradora" class="btn-primary btn"><i class="fa fa-check"></i>&nbsp;Guardar</button>
        <button type="button" id="btn-cancelar-colaboradora" class="btn-default btn"><i class="fa fa-check"></i>&nbsp;Cancelar</button>
    </div>
<?php ActiveForm::end(); ?>


<div id="modal-replegal-colaboradora" class="modal fade modal-replegal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Representante Legal</h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>

<script>
    
    var SituacionProyecto=<?= \Yii::$app->user->identity->SituacionProyecto ?>;
    if (SituacionProyecto!=0) {
        $("input,textarea,select").prop("disabled", true);
        $("#frmColaboradora button,#frmColaboradora a").hide();
    }
            
    $('#btn-back-colaboradora, #btn-cancelar-colaboradora').click(function () {
        cargarColaboradoras();
    });

    var formParsleyColaboradora = $('#frmColaboradora').parsley(defaultParsleyForm());

    $(document).ready(function () {
        var ds = $("#btn-add-replegal-colaboradora").attr('data-id');
        if(ds != ''){
            $('#btn-add-replegal-colaboradora').removeClass('hidden');
            cargarRepresentantesLegalesColaboradora(ds);
            $('#frmColaboradora').attr('action','entidad-participante/actualizaentidades?id='+ds);
        }

        $('input[name="EntidadParticipante[Ruc]"]').inputmask('integer');
        $('input[name="EntidadParticipante[Ruc]"]').css('text-align', 'left');
        // $('#frmColaboradora #FechaCreacion').datepicker(defaultDatePicker());
        $('input[name="EntidadParticipante[MontoFinanciado]"]').inputmask('integer');
        $('input[name="EntidadParticipante[MontoFinanciado]"]').css('text-align', 'left');

        $('.count-message').each(function () {
            var $this = $(this);
            var $divformgroup = $this.closest('div.form-group');
            var $txt = $divformgroup.find('textarea, input[type="text"]');
            var text_max = parseInt($txt.attr('maxlength'));

            $txt.keyup(function () {
                var text_length = $txt.val().length;
                var text_remaining = text_max - text_length;
                $this.html(text_remaining + ' caracteres restantes');
            });
        });



        $('.fechas').datepicker({
            language: 'es',
            format: 'dd/mm/yyyy',
            autoclose: true,
            //startDate: fechainicio_date,
            //endDate: fechafin_date_max
        }).on('changeDate', function (selected) {
            // calcularDuracionProyecto();
        });

        validarNumeros();

    });

    $('#btn-save-colaboradora').click(function (e) {
        e.preventDefault();
        var isValid = formParsleyColaboradora.validate();
        if (isValid) {
            sendForm($('#frmColaboradora'), $(this), function (eve) {
                console.log(eve);
                $('#btn-add-replegal-colaboradora').removeClass('hidden');
                $('#btn-add-replegal-colaboradora').attr('data-id',eve.EntidadID);
            });
        }
    });

    $(document).on('click','#btn-add-replegal-colaboradora',function () {
        $('.modal-replegal .modal-body').html('');
        var s = $(this).attr('data-id');
        
        if(s != ''){
            $('#modal-replegal-colaboradora .modal-body').load('<?= \Yii::$app->request->BaseUrl ?>/entidad-participante/representantelegalcrear?epID='+s);
        }else{
        }
            // $('#modal-replegal-colaboradora .modal-body').load('<?= \Yii::$app->request->BaseUrl ?>/entidad-participante/representantelegalcrear');

        $('#modal-replegal-colaboradora').modal('show');
    });

    


    function RUC(valor) {
        $('#RazonSocial').val('');
        $('#DomicilioFiscal').val('');
        $('#Telefono').val('');
        
        $.get('<?= \Yii::$app->request->BaseUrl ?>/orden/servicio/', { valor: valor}, function (data) {
            var jx = JSON.parse(data);
            if (jx==1) {
                alert("RUC no se encuentra");
            }
            else
            {
                var razon = jx.razonSocial;
                var domicilioLegal = jx.domicilioLegal;
                var telefono1 = jx.telefono1;

                $('#DomicilioFiscal').val(domicilioLegal.trim());
                $('#Telefono').val(telefono1.trim());
                $('#RazonSocial').val(razon.trim());
            }
            
        });
    }


    // $('.btn-remove-replegal').click(function (e) {
    // $(document).on('click','.btn-remove-replegal',function (e) {
    //     console.log(' ');
    //     e.preventDefault();
    //     var $this = $(this)
    //     bootboxConfirmEliminar($this,
    //         'Está seguro de eliminar el representante legal seleccionado?',
    //         function () {
    //             var repID = $this.attr('role-id');
    //             $.get('<?= \Yii::$app->request->BaseUrl ?>/entidad-participante/delete-resentante', { id: repID }, function (data) {
    //                 console.log(data);
    //                 var jx = JSON.parse(data);
    //                 console.log(jx)
    //                 if (jx.Success) {
    //                     console.log(jx.id);
    //                     cargarRepresentantesLegalesColaboradora(jx.id);
    //                 }
    //             });
    //         });
    // });

    // // $('#btn-save-replegal').click(function (e) {
    // $(document).on('click','#btn-save-replegal',function (e) {
    //     e.preventDefault();
    //     var isValid = formParsleyRepLegal.validate();
    //     if (isValid) {
    //         sendForm($('#frmRepresentanteLegal'), $(this), function (data) {
    //             $('#modal-replegal-colaboradora').modal('hide');   
    //             cargarRepresentantesLegalesColaboradora(data.id);
    //         });
    //     }
    // });


    function cargarRepresentantesLegalesColaboradora(x) {
        $('#div-tablereplegal-colaboradora').html('<i class="fa fa-spinner fa-pulse fa-5x fa-fw" style="color:#16a085"></i>');
        $('.modal-replegal').modal('hide');
        $('#div-tablereplegal-colaboradora').load('<?= \Yii::$app->request->BaseUrl ?>/entidad-participante/representanteslegales?epID='+x);
    }

</script>