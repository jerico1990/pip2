<?php if(!empty($EntidadParticipante)){?>
<div class="table-responsive">
    <table class="table table-bordered table-condensed table-striped table-hover tbl-act">
        <thead>
            <tr class="tr-header">
                <th>Acciones</th>
                <th>Institución</th>
                <th>RUC</th>
                <th>Tipo</th>
                <th>Fecha de creación</th>
            </tr>
        </thead>
        <tbody>
        	<?php foreach($EntidadParticipante as $Entidades){?>
	            <tr class="">
	                <td class="td-acciones">
	                    <a href="#" class="btn-edit-entidadcolaboradora" data-toggle="tooltip" title="" role-id="<?= $Entidades->EntidadID ?>" data-original-title="Editar"><i class="fa fa-edit fa-lg"></i></a>&nbsp;
	                    <a href="#" class="btn-remove-entidadcolaboradora" data-toggle="tooltip" title="" role-id="<?= $Entidades->EntidadID ?>" data-original-title="Eliminar"><i class="fa fa-remove fa-lg"></i></a>
	                </td>
	                <td><?= $Entidades->RazonSocial ?></td>
	                <td><?= $Entidades->Ruc ?></td>
	                <td><?= $Entidades->Nombre ?></td>
	                <td><?= Yii::$app->tools->dateFormat($Entidades->FechaCreacion) ?></td>
	            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<?php }else{ ?>
    <div class="alert alert-info">
        <i class="fa fa-info"></i>&nbsp;&nbsp;No se registraron registros.
    </div>
<?php } ?>

<script>
    var SituacionProyecto=<?= \Yii::$app->user->identity->SituacionProyecto ?>;
	    if (SituacionProyecto!=0) {
		$("#frmColaboradora input,#frmColaboradora textarea,#frmColaboradora select").prop("disabled", true);
		$(".btn-remove-entidadcolaboradora").hide();
	    }
</script>

