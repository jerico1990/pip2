
<?php if(!empty($RepresentanteLegal)){?>

<div class="table-responsive">
    <table class="table table-bordered table-condensed table-hover tbl-act">
        <thead>
            <tr class="tr-header">
                <th>Acciones</th>
                <th>Nombres y Apellidos</th>
                <th>N° Documento</th>
                <th>Cargo</th>
                <th>Teléfono</th>
                <th>Correo Electrónico</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($RepresentanteLegal as $legal){?>
                <tr>
                    <td class="td-acciones">
                        <a href="#" class="btn-edit-replegal" data-toggle="tooltip" title="Editar" role-id="<?= $legal->Codigo ?>"><i class="fa fa-edit fa-lg"></i></a>&nbsp;
                        <a href="#" class="btn-remove-replegal" data-toggle="tooltip" title="Eliminar" role-id="<?= $legal->Codigo ?>"><i class="fa fa-remove fa-lg"></i></a>
                    </td>
                    <td><?= $legal->ApellidoPaterno." ".$legal->ApellidoMaterno." ".$legal->Nombre ?></td>
                    <td><?= $legal->NroDocumento ?></td>
                    <td><?= $legal->Cargo ?></td>
                    <td><?= $legal->Telefono ?></td>
                    <td><?= $legal->Email ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>

<?php }else{ ?>
    <div class="alert alert-info">
        <i class="fa fa-info"></i>&nbsp;&nbsp;No se registraron representantes legales.
    </div>
<?php } ?>


<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

    $(document).on('click','.btn-edit-replegal',function (e) {
        e.preventDefault();
        var repID = $(this).attr('role-id');
        var s = $('#btn-add-replegal-colaboradora').attr('data-id');
        $('#modal-replegal-colaboradora .modal-body').load('<?= \Yii::$app->request->BaseUrl ?>/entidad-participante/representantelegalcrear?epID=' + s + '&id=' + repID);
        $('#modal-replegal-colaboradora').modal('show');
    });

    $('.btn-remove-replegal').click(function (e) {
    // $(document).on('click','.btn-remove-replegal',function (e) {
        console.log(' ');
        e.preventDefault();
        var $this = $(this)
        bootboxConfirmEliminar($this,
            'Está seguro de eliminar el representante legal seleccionado?',
            function () {
                var repID = $this.attr('role-id');
                $.get('<?= \Yii::$app->request->BaseUrl ?>/entidad-participante/delete-resentante', { id: repID }, function (data) {
                    console.log(data);
                    var jx = JSON.parse(data);
                    console.log(jx)
                    if (jx.Success) {
                        console.log(jx.id);
                        cargarRepresentantesLegalesColaboradora(jx.id);
                    }
                });
            });
    });
    
    
</script>