<div id="page-content" style="min-height: 934px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Entidades Participantes</h1>
        </div>
        <div class="container">
            
			<div class="panel panel-primary">
			    <div class="panel-heading">
			        <h4>Entidades Participantes</h4>
			        <div class="options">
                        <a class="panel-collapse" href="#">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
			    </div>
			    <div class="panel-body">
			        <div id="div-equipogestion-main">
			            <div class="tab-pane" id="tab-colaboradoras">
			                <div id="div-colaboradoras-table-main">
			                    <button type="button" id="btn-add-colaboradora" class="btn btn-midnightblue-alt"><i class="fa fa-plus"></i>&nbsp;<span>Agregar Entidad Colaboradora</span></button>
			                    <br><br>
			                    <div id="div-colaboradoras-table"></div>
			                </div>
			                <div id="div-colaboradoras-create" class="hidden"></div>
			            </div>

			        </div>
			    </div>
			</div>

			<div id="modal-fondorecibido" class="modal fade" tabindex="-1" role="dialog">
			    <div class="modal-dialog">
			        <div class="modal-content">
			            <div class="modal-header">
			                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
			                <h4 class="modal-title">Fondo Recibido o Administrado</h4>
			            </div>
			            <div class="modal-body">
			            </div>
			        </div>
			    </div>
			</div>


			<!-- <div class="alert alert-warning">
			    
			    <strong>IMPORTANTE: Para adjuntar un archivo se debe seguir los siguientes pasos:</strong>
			    <ol>
			        <li>Descargar el formato en caso exista, utilizando la opción &nbsp;<a href="javascript:void(0);"><i class="fa fa-download"></i>&nbsp;Descargar formato</a></li>
			        <li>Llenar el formato descargado, imprimirlo, firmarlo y escaneárlo.</li>
			        <li>Adjuntar el archivo escaneado, utilizando la opción &nbsp;<a href="javascript:void(0);"><i class="fa fa-cloud-upload fa-lg"></i>&nbsp;Adjuntar archivo</a></li>
			    </ol>
			    
			</div> -->

        </div>
    </div>
</div>
<!--
<div id="modal-loading" class="modal fade modal-loading " tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content-no-shadow modal-content">
            <div class="text-center"><i class="fa fa-spinner fa-pulse fa-fw" style="color:#16a085;font-size: 100px;"></i></div>
        </div>
    </div>
</div>
-->

	<script>
	    var situacionProyecto=<?= \Yii::$app->user->identity->SituacionProyecto ?>;
	    if (situacionProyecto!=0) {
		$("input,textarea,select").prop("disabled", true);
		$(".container button,.container a").hide();
	    }
	    $(document).ready(function () {
	        $('[data-toggle="tooltip"]').tooltip();
		cargarColaboradoras();
	    });

        $('#btn-add-colaboradora').click(function () {
            $('#div-colaboradoras-table-main').addClass('hidden');
            $('#div-colaboradoras-create').html('<i class="fa fa-spinner fa-pulse fa-5x fa-fw" style="color:#16a085"></i>');
            $('#div-colaboradoras-create').load('<?= \Yii::$app->request->BaseUrl ?>/entidad-participante/entidadescrear');
            $('#div-colaboradoras-create').removeClass('hidden');
        });


        function cargarColaboradoras() {
            $('#div-colaboradoras-create').addClass('hidden');
            $('#div-colaboradoras-table').html('<i class="fa fa-spinner fa-pulse fa-5x fa-fw" style="color:#16a085"></i>');
            $('#div-colaboradoras-table').load('<?= \Yii::$app->request->BaseUrl ?>/entidad-participante/entidadeslistado');
            $('#div-colaboradoras-table-main').removeClass('hidden');
        }

        function cargarRepresentantesLegales() {
            $('.bootbox-confirm').modal('hide');
            $('.modal-replegal').modal('hide');
            $('.modal-replegal .modal-body').html('');

            if (typeof cargarRepresentantesLegalesProponente == 'function') {
                cargarRepresentantesLegalesProponente();
            }
            if (typeof cargarRepresentantesLegalesDemandante == 'function') {
                cargarRepresentantesLegalesDemandante();
            }
            if (typeof cargarRepresentantesLegalesColaboradora == 'function') {
                cargarRepresentantesLegalesColaboradora();
            }

            //cargarRepresentantesLegalesProponente();
            //cargarRepresentantesLegalesDemandante();
            //cargarRepresentantesLegalesColaboradora();
        }


        $(document).on('click','.btn-edit-entidadcolaboradora',function (e) {
        	e.preventDefault();
	        var epID = $(this).attr('role-id');
	        // alert(epID);
	        $('#div-colaboradoras-table-main').addClass('hidden');
	        $('#div-colaboradoras-create').html('<i class="fa fa-spinner fa-pulse fa-5x fa-fw" style="color:#16a085"></i>');
	        $('#div-colaboradoras-create').load('<?= \Yii::$app->request->BaseUrl ?>/entidad-participante/entidadescrear?id=' + epID);
	        $('#div-colaboradoras-create').removeClass('hidden');
	    });



	    $(document).on('click','.btn-remove-entidadcolaboradora',function (e) {
	        e.preventDefault();
	        var $btn = $(this)
	        bootboxConfirmEliminar($btn,
	            'Está seguro de eliminar la entidad participante?',
	            function () {
	                var epID = $btn.attr('role-id');
	                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
	                $.post('<?= \Yii::$app->request->BaseUrl ?>/entidad-participante/deletentidades/', { id: epID, _csrf: toke }, function (data) {
	                	var jx = JSON.parse(data);
	                    if (jx.Success == true) {
	                        cargarColaboradoras();
	                    }
	                });
	            });
	    });

    </script>    
