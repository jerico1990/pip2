<?php
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
?>

<?php $form = ActiveForm::begin(
    [
        'action'    => empty($Modelor->Codigo) ? 'entidad-participante/crearrepresentante' : 'entidad-participante/actualizarepresentante?id='.$Modelor->Codigo,
        'options'   => [
            'autocomplete'  => 'off',
            'id'            => 'frmRepresentanteLegal'
         ]
    ]
); ?>
    <div class="form-horizontal">
        <div id="container-resp"></div>
        <div class="form-group">
            <label class="col-sm-4 control-label">N° DNI <span class="f_req">*</span></label>
            <div class="col-sm-8">
                <input class="form-control" id="Persona_NroDocumento" onfocusout="DNI($(this).val())" maxlength="8" minlength="8" name="Persona[NroDocumento]" required="" type="text" value="<?php echo $Modelor->NroDocumento ?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label">Nombres <?php echo $Modelor->Codigo ?><span class="f_req">*</span></label>
            <div class="col-sm-8">
                <input class="form-control" id="Persona_Nombre" name="Persona[Nombre]" required="" type="text" value="<?php echo $Modelor->Nombre ?>" disabled>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label">Apellido Paterno <span class="f_req">*</span></label>
            <div class="col-sm-8">
                <input class="form-control" id="Persona_ApellidoPaterno" name="Persona[ApellidoPaterno]" required="" type="text" value="<?php echo $Modelor->ApellidoPaterno ?>" disabled>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label">Apellido Materno <span class="f_req">*</span></label>
            <div class="col-sm-8">
                <input class="form-control" id="Persona_ApellidoMaterno" name="Persona[ApellidoMaterno]" required="" type="text" value="<?php echo $Modelor->ApellidoMaterno ?>" disabled>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label">
                Cargo que desempeña
                <span class="f_req">*</span>
            </label>
            <div class="col-sm-8">
                <input class="form-control text-uppercase" id="Cargo" name="RepresentanteLegal[Cargo]" required="" type="text" value="<?php echo $Modelor->Cargo ?>">
                <input name="RepresentanteLegal[EntidadParticipanteID]" type="hidden" value="<?php echo $Id ?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label">Teléfono <span class="f_req">*</span></label>
            <div class="col-sm-8">
                <input class="form-control" id="Persona_Telefono" maxlength="20" onKeyPress="return soloNumeros(event)" name="Persona[Telefono]" required="" type="text" value="<?php echo $Modelor->Telefono ?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label">Correo Electrónico <span class="f_req">*</span></label>
            <div class="col-sm-8">
                <input class="form-control text-uppercase" data-parsley-type="email" id="Persona_Email" name="Persona[Email]" required="" type="text" value="<?php echo $Modelor->Email ?>">
            </div>
        </div>
        <hr>
        <div class="form-inline text-right">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <button id="btn-save-replegal" type="button" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
        </div>
    </div>
<?php ActiveForm::end(); ?>


<script>
    var formParsleyRepLegal = $('#frmRepresentanteLegal').parsley(defaultParsleyForm());

    $(document).ready(function () {
        $('#frmRepresentanteLegal [required=""]').attr('data-parsley-required', 'true');
        //$('#frmRepresentanteLegal [name="Persona[NroDocumento]"]').inputmask('integer');
        $('#frmRepresentanteLegal [name="Persona[NroDocumento]"]').css('text-align', 'left');
    });

    $('#btn-save-replegal').click(function (e) {
        e.preventDefault();
        var isValid = formParsleyRepLegal.validate();
        if (isValid) {
            sendForm($('#frmRepresentanteLegal'), $(this), function (e) {
                $('#modal-replegal-c').modal('hide');   
                cargarRepresentantesLegalesColaboradora(<?php echo $Id ?>);
            });
        }
    });

    $('#frmRepresentanteLegal [name="Persona[NroDocumento]"]')._toNumeric();


    function DNI(valor) {
        $('#Persona_Nombre').val('');
        $('#Persona_ApellidoPaterno').val('');
        $('#Persona_ApellidoMaterno').val('');

        $.get('<?= \Yii::$app->request->BaseUrl ?>/viatico/servicio-reniec/', { valor: valor}, function (data) {
            var jx = JSON.parse(data);
            if (jx.message !='Ok') {
                alert("DNI no se encuentra");
                $('#btn-save-replegal').attr('disabled','disabled');
            }
            else
            {
                if(jx.error != 1){
                    $('#btn-save-replegal').removeAttr('disabled');
                    var name = jx.data.nombres;
                    var paterno = jx.data.apellidoPaterno;
                    var materno = jx.data.apellidoMaterno;
                    $('#Persona_Nombre').val(name.trim());
                    $('#Persona_ApellidoPaterno').val(paterno.trim());
                    $('#Persona_ApellidoMaterno').val(materno.trim());
                }
            }
            
        });
    }

</script>