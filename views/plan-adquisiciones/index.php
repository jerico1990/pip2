<div id="page-content" style="min-height: 754px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Plan de Adquisición <?php echo $anio ?></h1>
        </div>
        <div class="container">
            
            <!-- OBJETIVOS -->
            <style>
                .panel .panel-body {
                    padding: 10px !important;
                }

                .panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 1px solid #a8a8ab !important;
                }

                .panel-gray .panel-body {
                    border-top: none !important;
                }

                .btn-sm {
                    padding: 5px 10px !important;
                    line-height: 1.5 !important;
                }


                .tbl-act > tbody > .tr-header > td {
                    text-align: center;
                    font-size: 12px;
                    font-weight: bold;
                    padding: 5px !important;
                    vertical-align: middle !important;
                    /*border: 1px solid #cfcfd0;
                    background-color: #f0f0f1;*/
                    border: 1px solid #c0c0c0;
                    background-color: #e3e3e3;
                }

                /*.tbl-act .tr-item td {
                    padding: 2px !important;
                }*/
                input.form-control, select.form-control {
                    /*height: 28px !important;*/
                }

                .tbl-act .tr-act .input-nombre {
                    min-width: 320px !important;
                }

                .tbl-act .tr-act .input-unidadmedida {
                    min-width: 100px !important;
                }

                .input-number {
                    min-width: 92px !important;
                }

                .btn-add-re {
                    float: right;
                }

                /*.tbl-act > tbody > tr > td:nth-child(2) {
                    min-width: 40px !important;
                    text-align: center !important;
                }*/
                .left-bold{
                    text-align: left !important;
                    font-weight: bold;
                }
                .tr-comp input, .tr-proy input {
                    font-weight: bold;
                }

                .th-proy {
                    min-width: 630px !important;
                }

                select.form-control.categoria {
                    width: 250px;
                }
            </style>

            <!-- <div class="text-left">
                <a class="btn btn-inverse-alt" id="volvers" href="plan-adquisiciones2/listado">
                    <i class="fa fa-arrow-left"></i>&nbsp;Volver
                </a>
            </div> -->
            <br>    

            <div class="panel panel-primary">
                <!-- <div class="panel-heading">
                    <h4>OBJETIVO</h4>
                </div> -->
                <div class="panel-body">
                    <div class="row">
                        <div id="contendo_preview">
                            <div class="col-sm-12">
                                <div id="table-container">
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="text-center">
                                    <?php if($Situacion == 0): ?>
                                        <button class="btn btn-primary btn-save-poaf"><i class="fa fa-check"></i>&nbsp;Previsualizar</button>
                                        <a class="btn btn-inverse-alt" id="volverss" href="<?= \Yii::$app->request->BaseUrl ?>/plan-adquisiciones/listado">
                                            <i class="fa fa-arrow-left"></i>&nbsp;Volver
                                        </a>
                                    <?php endif ?>

                                    <!-- <a href="javascript:void(0);" class="btn btn-inverse-alt" onclick="loadPoaf();">
                                        <i class="fa fa-refresh"></i>&nbsp;Actualizar
                                    </a>
                                    <a class="btn btn-inverse-alt" href="/SLFC/PasoCritico/DescargarPC_PlanOperativoDetallado">
                                        <i class="fa fa-download"></i>&nbsp;Descargar
                                    </a> -->
                                </div>
                            </div>
                        </div>
                        <div id="result_preview" class="hidden">
                            
                        </div>
                    </div>

                </div>
            </div>

            <!-- END OBJETIVOS -->
        </div>
    </div>
</div>




<div class="modal fade" id="modal-ctrl-objetivo" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Recursos</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>


<div id="modal-componentes-actv" class="modal fade modal-replegal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Objetivos - Actividades</h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>


<script>
    var _proyecto = new Object();

    $(document).ready(function () {
        $('#table-container').html('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>');
        getProyecto().done(function(json) {
            _proyecto = json;
            _drawTable();
        });
    });

    $(document).on('click','.add-component',function(e){
        e.preventDefault();
        var r = $(this).parent().parent().parent().attr('data-recurso-id');
        var x = $('#ubicacion_'+r).val()
        $('#modal-componentes-actv .modal-body').load('<?= \Yii::$app->request->BaseUrl ?>/plan-adquisiciones/componente-form?recurso='+r,{valor:x}, function(response, status, xhr) {
            if (status == "error") {
                var msg = "Error!, algo ha sucedido: ";
                alert(msg + xhr.status + " " + xhr.statusText);
            }
        });
        $('#modal-componentes-actv').modal('show');
    });

    $('body').on('change','.objetivo-select',function(e){
        e.preventDefault();
        var dd = $(this).val();
        // var xx = $(this).parent().parent().attr('id');
        // var f = xx.split("_")[1];
        $.ajax({
            cache: false,
            type: 'GET',
            url: '<?= \Yii::$app->request->BaseUrl ?>/plan-adquisiciones/objetivos',
            data: 'componente='+dd,
            success: function (data) {
                $('.Actividades').html(data);
            },
            error: function () {
                _pageLoadingEnd();
            }
        });
    });

    var arrObjs = [];

    $('body').on('click','#agregar_ob',function(e){
        e.preventDefault();
        var arrVacios = [];
        var isValid = formParsleyColaboradora.validate();
        if (isValid) {
            var obj = $('.objetivo-select').val();
            var act = $('.Actividades').val();

            $('.longitud td').each(function(){
                var c = $(this).attr('data-comp-id');
                var a = $(this).attr('data-actv-id');
                if(a == act){
                    console.log(a);
                    arrVacios.push({
                        ID: 1
                    });
                }
            });
            
            var arrTod = [];

            if(arrVacios.length == 0){
                $.ajax({
                    cache: false,
                    type: 'GET',
                    contentType: 'application/json;charset=utf-8',
                    dataType: 'json',
                    data: 'componente='+obj +'&actividad='+act,
                    url: '<?= \Yii::$app->request->BaseUrl ?>/plan-adquisiciones/objetivos',
                    success: function (data) {
                        var longitud = $('.longitud').length;
                        if(data.Error == 0){
                            var ac = '<tr id="a_'+longitud+'" class="longitud">';
                                ac += '<td><a href="#" class="btn-remove" data-toggle="tooltip" title="" data-id="'+longitud+'" data-original-title="Eliminar"><i class="fa fa-remove fa-lg"></i></a></td>';
                                ac += '<td class="filas" data-comp-id="'+ data.ComponenteID +'" data-comp-corelativo="'+ data.ComponenteCorelativo +'"><strong> Objetivo ' + data.ComponenteCorelativo + ':</strong> ' + data.Componente + '</td>';
                                ac += '<td class="filas" data-actv-id="'+ data.ActividadID +'" data-actv-corelativo="'+ data.ActividadCorrelativo +'"><strong> Actividad ' + data.ActividadCorrelativo + ':</strong> ' + data.Actividad  + '</td>';
                            ac += '</tr>';
                            $('#contenido_obj').append(ac);
                            var t = $('#recurso').val();
                            arrObjs.push({
                                recurso: t,
                                ubica: 'Objetivo: '+data.ComponenteCorelativo+', Actividad: '+data.ComponenteCorelativo+'.'+data.ActividadCorrelativo,
                                componenteID:       data.ComponenteID,
                                correlativoComp:    data.ComponenteCorelativo,
                                componente:         data.Componente,
                                actividadID:        data.ActividadID,
                                actividad:          data.Actividad,
                                correlativoActv:    data.ActividadCorrelativo
                            });

                            // if()
                            // arrObjs.push({
                            //     recurso : arrTod
                            // });
                        }
                    },
                    error: function () {
                        _pageLoadingEnd();
                    }
                });
            }
        }
    });


    $(document).on('click','#btn-save-colaboradora',function (e) {
        e.preventDefault();
        var arrVacios = [];
        var temp1 = [];
        var temp2 = [];
        var cc = '';
        var ac = '';
        var isValid = formParsleyColaboradora.validate();
        if (isValid) {
            var t = $('#recurso').val();

            // $('.longitud .filas').each(function(k,v){
            //     var com  = $(v).data('comp-id');
            //     var actv = $(v).data('actv-id');
            //     if( !!com){
            //         var comCorrela  = $(v).data('comp-corelativo');
            //         var componente  = $(v).data('comp-corelativo');
            //         temp1.push({
            //             componenteID: com,
            //             correlativoComp: comCorrela,
            //             componente: componente
            //         });
            //     }

            //     if( !!actv ){
            //         var actvCorrela  = $(v).data('actv-corelativo');
            //         var actividad  = $(v).data('actv-id');

            //         temp2.push({
            //             actividadID: actv,
            //             correlativoActv: actvCorrela,
            //             actividad: actividad
            //         });
            //     }

            //     console.log($(v).data('comp-id'));
            //     var total = temp1.concat(temp2);
            //     // arrObjs.push({
            //     //     total
            //     // });
            //     // console.log(v.attr('data-comp-id'));
            //     // console.log(v.attr('data-comp-id'));
            // });

            console.log(arrObjs);
            var g = '';
            $(arrObjs).each(function(k,v){

                if(v.recurso == t){
                    g += v.ubica+",";

                    arrVacios.push({
                        recurso: t,
                        componenteID:       v.componenteID,
                        correlativoComp:    v.correlativoComp,
                        componente:         v.componente,
                        actividadID:        v.actividadID,
                        actividad:          v.actividad,
                        correlativoActv:    v.correlativoActv
                    });
                }
            });
            $('.ubicacion_'+t).html('<a href="#" class="add-component">'+g+'</a>');
            $('#ubicacion_'+t).val(JSON.stringify(arrVacios));
            // arrObjs = [];
            $('#modal-componentes-actv').modal('hide');
        }
    });

    $(document).on('click','.btn-remove',function(e){
        e.preventDefault();
        var s = $(this).attr('data-id');
        var t = $('#recurso').val()
        var f = jQuery.parseJSON($('#json').val());
            // console.log(s)
        $(f).each(function(k,v){
            console.log(k);
            if(k == s){
                // arrObjs = [];
                $('#a_'+s).remove();
                removeItemFromArr( f, k );
                removeItemFromArr( arrObjs, t );
                console.log(f)
                
                $('#ubicacion_'+t).val(JSON.stringify(f));
            }
        });
    });

    function removeItemFromArr ( arr, item ) {
        var i = arr.indexOf( item );
        arr.splice( i, 1 );
    }


    $('.btn-save-poaf').click(function (e) {
        e.preventDefault();
        _pageLoadingStart();
        var arrPoafMFIS = [];
        var arrPoafMFIN = [];
        var arrChecked = [];
        var arrChecked2 = [];
        var arrCabecera = [];

        $('.paccheck:checked').each(function () {
            var $this = $(this);
            var s = $this.parent().siblings('td').children('select').val();
            var rub = $this.parent().siblings('td').children('select').attr('data-rubro');
            var codi = $this.parent().siblings('td').children('select').attr('data-codifica');
            var id = $this.attr('data-id-recurso');
            var rubro = $this.attr('data-id-rubro');
            var sd = arrCabecera.map(function (element) {return element.INDICADOR;}).indexOf(s);
            var xid = arrChecked2.map(function (element) {return element.ID;}).indexOf(id);

            if(sd == -1){
                arrCabecera.push({
                    INDICADOR: s
                });
            }

            if(xid == -1){
                arrChecked.push({
                    ID: rub,
                    CODI: codi,
                    DES: s
                });
            }
        });

        // $('.categoria[changed="changed"]').removeAttr('changed');
        console.log(arrChecked.length);
        if (arrChecked.length) {
            var toke = '<?=Yii::$app->request->getCsrfToken()?>';
            var _obj = {
                PrevisualizacionPac: arrChecked,
                PrevisualizacionCab: arrCabecera,
                Anio               : <?php echo $anio ?>,
                _csrf: toke
            };
            $.ajax({
                cache: false,
                type: 'POST',
                url: '<?= \Yii::$app->request->BaseUrl ?>/plan-adquisiciones/preview',
                data: (_obj),
                beforeSend: function() {
                   // _pageLoadingStart();
                },
                success: function (data) {
                    // _pageLoadingEnd();
                    // $('.modal-backdrop').remove(); 
                    var ck  = $('.paccheck:checked').length;
                    var car = $('.categoria[changed="changed"]').length;
                    if(ck == car){
                        $('#contendo_preview').addClass('hidden');
                        $('#result_preview').html(data);
                        $('#result_preview').removeClass('hidden');
                    }else{
                        bootbox.alert('Debe de seleccionar la categoria y marcar el check.');
                    }
                },
                error: function () {
                    _pageLoadingEnd();
                }
            });
        } else {
            bootbox.alert('Debe de seleccionar una categoria');
        }
    });



    $('body').on('change', '.categoria', function (e) {
        e.preventDefault();
        $(this).attr('changed', 'changed');
        $(this).parent().siblings('td').children('.paccheck').attr('checked','checked');
    });
    

    function getProyecto() {
        // var url = "<?= \Yii::$app->request->BaseUrl ?>/../json/pa.json";
        var url = "<?= \Yii::$app->request->BaseUrl ?>/plan-adquisiciones/recursos-json-pa?anio=<?php echo $anio ?>";
        return $.getJSON( url );
    }

    function _drawTable() {
        var html = '';
        html += '<table class="table table-bordered table-condensed ">';
        html += '<tbody>';
        // console.log(_proyecto.Categoria);

        for (var i_act = 0; i_act < _proyecto.Rubro.length; i_act++)
        {
            var act = _proyecto.Rubro[i_act];
            html += '<tr class="tr-header info">';
            html += '<td>'+(i_act + 1)+'</td><td><b>'+ act.Nombre +'</b></td><td>Categoría</td>'
            html += '<td></td>';
            html += '</tr>';
            if (act.Recursos.length) {
                for (var i_are = 0; i_are < act.Recursos.length; i_are++) {
                    var are = act.Recursos[i_are];
                    // console.log(are);
                    html += '<tr class="tr-sub2-tot warning">';
                    // (i_act + 1)+'.'+(i_are+1)
                    html += '<td>'+are.Codificacion+'</td><td>' + are.Nombre + '</td>'
                    + '<td><select class="form-control categoria" data-rubro="' + are.Rubro + '" data-codifica="' + are.Codifica + '" data-id-categoria=""><option value="">[SELECCCIONE]</option>';
                        $(_proyecto.Categoria).each(function(k,v){
                            html +='<option value="'+v.ID+'">'+v.Nombre+'</option>';
                        });
                    html += '<td><input type="checkbox" class="form-control paccheck" data-id-recurso="' + are.ID + '" name="costo" data-id-rubro="' + act.ID + '" style="height: 16px !important;display:none" value="' + are.ID + '"></td>'
                    html += '</tr>';
                }
            }else{
                html += '<tr class="tr-sub2-tot warning">';
                html += '<td colspan="4">No existen recursos</td>'
                html += '</tr>';
            }
        }
        html += '</tbody>';
        html += '</table>';
        $('#table-container').html(html);
        $('.table-responsive').css('max-height', $(window).height() * 0.60);
        
    }
</script>
<!-- <div id="modal-loading" class="modal fade modal-loading" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="text-center"><i class="fa fa-spinner fa-pulse fa-fw" style="color:#16a085;font-size: 100px;"></i></div>
        </div>
    </div>
</div> -->