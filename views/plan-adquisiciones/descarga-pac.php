<?php
use app\models\Actividad;
use app\models\ActRubroElegible;
use app\models\CronogramaAreSubCategoria;
use app\models\CronogramaProyecto;
use app\models\MarcoLogicoActividad;


/** Error reporting */
error_reporting(E_ALL);

/** Include path **/
ini_set('include_path', ini_get('include_path').';../web/PHPExcel/Classes/');
//require(__DIR__ . '/../../web/PHPExcel/Classes/');
/** PHPExcel */
include 'PHPExcel.php';

/** PHPExcel_Writer_Excel2007 */
include 'PHPExcel/Writer/Excel2007.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
$sheet = $objPHPExcel->getActiveSheet();

$sheet->setTitle('PAC');

$Meses=[];
$objPHPExcel->setActiveSheetIndex(0);

// ------------------------------------------------
    $bakgroundColor = array(
        'cabeza' => array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'c5c5c5')
                )
            ),
        'cuerpo' => array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '848484')
                )
            ),
        'bordes' => array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                )
            ),
    );  // Background

    $styleArray = array(
       'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN
            )
        )
    );  // Bordes

    $fontColor = array(
        'font'  => array(
            'bold'  => true,
            'color' => array('rgb' => 'FFFFFF'),
            // 'size'  => 15,
            // 'name'  => 'Verdana'
        )
    ); // Fuente

    $FontBold = array(
        'font'  => array(
            'bold'  => true
        )
    );  // Fuente Negrita

    $alineacionVeticalH = array(
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        )
    );  // Alineacion

// ------------------------------------------------

    $sheet->getStyle('A1:L9')->applyFromArray($alineacionVeticalH);
    $sheet->getStyle('A1:A5')->applyFromArray($bakgroundColor['cabeza']);
    $sheet->getStyle('A1:A5')->applyFromArray($FontBold);


    $sheet->setCellValueByColumnAndRow(0, 1, "Plan de Adquisiciones - Proyecto de Investigación y Transferencia de Tecnología del INIA");
    $sheet->getRowDimension('1')->setRowHeight(20);

    // --------------------------------
    $sheet->setCellValueByColumnAndRow(0, 2, "Código del Proyecto: ". $codigo);
    $sheet->getRowDimension('2')->setRowHeight(20);

    // -----------------------------------

    $sheet->setCellValueByColumnAndRow(0, 3, "Título del Proyecto: ". $titulo);
    $sheet->getRowDimension('3')->setRowHeight(20);

    // -----------------------------------

    $sheet->setCellValueByColumnAndRow(0, 4, "Responsable del Proyecto: ". $investigador);
    $sheet->getRowDimension('4')->setRowHeight(20);

    // -----------------------------------

    $sheet->setCellValueByColumnAndRow(0, 5, "Período comprendido para este Plan de Adquisiciones: Semetre 1 y 2 AÑO ". $anio);
    $sheet->getRowDimension('5')->setRowHeight(20);

    // --------------------------------------------------------

    $sheet->getStyle('A7:L9')->applyFromArray($fontColor);
    $sheet->getStyle('A7:L9')->applyFromArray($bakgroundColor['cuerpo']);
    $sheet->getStyle('A7:L9')->applyFromArray($styleArray);
    $sheet->getStyle('A7:L9')->getAlignment()->setWrapText(true); 

    $sheet->getRowDimension('7')->setRowHeight(30);
    $sheet->getRowDimension('8')->setRowHeight(30);
    $sheet->getRowDimension('9')->setRowHeight(65);

    $sheet->mergeCells('A7:'."A8");
    $sheet->SetCellValue('A7', 'Nº de Referencia');
    $sheet->getColumnDimension('A')->setWidth(11);

    $sheet->mergeCells('B7:'."B8");
    $sheet->SetCellValue('B7', 'Descripción Detallada');
    $sheet->getColumnDimension('B')->setWidth(16);


    $sheet->mergeCells('C7:'."C8");
    $sheet->SetCellValue('C7', 'Unidad de Medida');
    $sheet->getColumnDimension('C')->setWidth(18);

    $sheet->mergeCells('D7:'."D8");
    $sheet->SetCellValue('D7', 'Cantidad');
    $sheet->getColumnDimension('D')->setWidth(12);

    $sheet->mergeCells('E7:'."E8");
    $sheet->SetCellValue('E7', 'CODIFICACIÓN SEGÚN MATRIZ');
    $sheet->getColumnDimension('E')->setWidth(23);

    $sheet->mergeCells('F7:'."F8");
    $sheet->SetCellValue('F7', 'Costo Estimado de la Adquisición o Contratación (S/)');
    $sheet->getColumnDimension('F')->setWidth(32);

    $sheet->mergeCells('G7:'."G8");
    $sheet->SetCellValue('G7', 'Método de Adquisición o Contratación(CP o Evaluación de CV)');
    $sheet->getColumnDimension('G')->setWidth(27);

    $sheet->mergeCells('H7:'."I7");  // Combinar columna
    $sheet->SetCellValue('H7', 'Fuente de Financiamiento');
    $sheet->getColumnDimension('H')->setWidth(15);
    $sheet->getColumnDimension('I')->setWidth(15);

    $sheet->mergeCells('J7:'."J8");
    $sheet->SetCellValue('J7', 'Fechas Estimadas');
    $sheet->getColumnDimension('J')->setWidth(19);


    $sheet->mergeCells('K7:'."K8");
    $sheet->SetCellValue('K7', 'Comentarios u Observaciones');
    $sheet->getColumnDimension('K')->setWidth(18);

    $sheet->mergeCells('L7:'."L9");
    $sheet->SetCellValue('L7', 'Observaciones');
    $sheet->getColumnDimension('L')->setWidth(17);


    $sheet->SetCellValue('H8', 'PNIA');
    $sheet->SetCellValue('I8', 'Otros');
    $sheet->SetCellValue('J8', 'Inicio de la Adquisición o Contratación');

    // -------------------------------------------------------------

    $sheet->SetCellValue('A9', 'Número de Referencia del PAC');
    $sheet->SetCellValue('B9', 'detalle de los rubros a adquirir');
    $sheet->SetCellValue('C9', 'millares, kilos, toneladas, frasco');
    $sheet->SetCellValue('D9', 'número de unidade');
    $sheet->SetCellValue('E9', 'Indicar la identificación en la Matriz');
    $sheet->SetCellValue('F9', 'debe consignarse el monto consignado en el POA');
    $sheet->SetCellValue('G9', 'esta puede ser Comparación de Precios (CP) o Evaluación de CV');
    $sheet->mergeCells('H9:'."I9");  // Combinar columna
    $sheet->SetCellValue('H9', 'debe consignar el monto que corresponda al PNIA o Colaboradores');
    $sheet->SetCellValue('J9', 'fecha en la que se Contratará el servicio o se va a adquirir los bienes');
    $sheet->SetCellValue('K9', 'Indicar la identificación con el objetivo-actividad');



$highestColumm = $objPHPExcel->setActiveSheetIndex(0)->getHighestColumn();
$sheet->mergeCells('A1:'.$highestColumm."1");
$sheet->getStyle('A1:'.$highestColumm.'1')->applyFromArray($styleArray);

$sheet->mergeCells('A2:'.$highestColumm."2");
$sheet->getStyle('A2:'.$highestColumm.'2')->applyFromArray($styleArray);

$sheet->mergeCells('A3:'.$highestColumm."3");
$sheet->getStyle('A3:'.$highestColumm.'3')->applyFromArray($styleArray);

$sheet->mergeCells('A4:'.$highestColumm."4");
$sheet->getStyle('A4:'.$highestColumm.'4')->applyFromArray($styleArray);

$sheet->mergeCells('A5:'.$highestColumm."5");
$sheet->getStyle('A5:'.$highestColumm.'5')->applyFromArray($styleArray);

$sheet->mergeCells('J7:'."J8");

$sheet->getStyle('A11:L99')->getAlignment()->setWrapText(true); 


// echo '<pre>';
// print_r($detallepa);
// echo '</pre>';
// die();

$ca = ''; $i=0; $x=0; $ii=10; $ss=1;
foreach($detallepa as $value):
    
    if($value['Categoria'] != $ca):
        $sheet->SetCellValue('A'.$ii, ''.($i+1).'');

        $sheet->mergeCells('B'.$ii.":".$highestColumm.$ii);
        $sheet->SetCellValue('B'.$ii, strtoupper($value['Categoria']));
        $sheet->getStyle('A'.$ii.":".$highestColumm.$ii)->applyFromArray($bakgroundColor['cabeza']);
        $sheet->getStyle('A'.$ii.":".$highestColumm.$ii)->getFont()->setBold(true);
        $sheet->getStyle('A'.$ii.":".$highestColumm.$ii)->applyFromArray($styleArray);

        $ca = $value['Categoria']; $i++;$x=0;
        $sa = $ii ++;
    endif;

    // echo (($i).".".($x+1));

    $sheet->getStyle('A'.($sa+1).":".$highestColumm.($sa+1))->applyFromArray($bakgroundColor['bordes']);
    $sheet->getStyle('A'.($sa+1).":".$highestColumm.($sa+1))->applyFromArray($alineacionVeticalH);
    $sheet->getStyle('A'.($sa+1).":".$highestColumm.($sa+1))->applyFromArray($styleArray);

    
    $sheet->SetCellValue('A'.($sa+1), ($i).".".($x+1) );
    $sheet->SetCellValue('B'.($sa+1), $value['Nombre']);
    $sheet->SetCellValue('C'.($sa+1), $value['UnidadMedida']);
    $sheet->SetCellValue('D'.($sa+1), $value['Cantidad']);
    $sheet->SetCellValue('E'.($sa+1), $value['Codificacion']);
    $sheet->SetCellValue('F'.($sa+1), $value['Cantidad']*$value['CostoUnitario']);
    $sheet->SetCellValue('G'.($sa+1), $value['MetodoAdquisicion']);
    $sheet->SetCellValue('H'.($sa+1), $value['RazonSocial'] == 'PNIA'? $value['Cantidad']*$value['CostoUnitario']: '-' );
    $sheet->SetCellValue('I'.($sa+1), $value['RazonSocial'] != 'PNIA'? $value['CostoUnitario']: '-');
    $sheet->SetCellValue('J'.($sa+1), $value['FechaInicioPA']);
    $sheet->SetCellValue('K'.($sa+1), $value['NumeracionPAC']);
    // if(!empty($value['NumeracionPAC'])){
    //     foreach (json_decode($value['NumeracionPAC']) as $key => $valuex):
    //         $sheet->SetCellValue('K'.($sa+1), "Objetivo:".$valuex->correlativoComp.' - Actividad: '.$valuex->correlativoComp.'.'.$valuex->correlativoActv.', ');
    //     endforeach;
    // }
    $sheet->SetCellValue('L'.($sa+1), $value['Observaciones']);
    $ii++;
    // $ss++;
    $sa++;
    $x++;
endforeach;


// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="PAC-'.$codigo.'-'.$anio.'.xls"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
