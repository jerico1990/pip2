<div id="page-content" style="min-height: 754px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Plan de Adquisición <?php echo $anio; ?></h1>
        </div>
        <div class="container">
            
            <!-- OBJETIVOS -->
            <style>
                .panel .panel-body {
                    padding: 10px !important;
                }

                .panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 1px solid #a8a8ab !important;
                }

                .panel-gray .panel-body {
                    border-top: none !important;
                }

                .btn-sm {
                    padding: 5px 10px !important;
                    line-height: 1.5 !important;
                }

                .input-comp-nombre {
                    text-transform: uppercase;
                }

                .tbl-act > tbody > .tr-header > td {
                    text-align: center;
                    font-size: 12px;
                    font-weight: bold;
                    padding: 5px !important;
                    vertical-align: middle !important;
                    /*border: 1px solid #cfcfd0;
                    background-color: #f0f0f1;*/
                    border: 1px solid #c0c0c0;
                    background-color: #e3e3e3;
                }

                /*.tbl-act .tr-item td {
                    padding: 2px !important;
                }*/
                input.form-control, select.form-control {
                    /*height: 28px !important;*/
                }

                .tbl-act .tr-act .input-nombre {
                    min-width: 320px !important;
                }

                .tbl-act .tr-act .input-unidadmedida {
                    min-width: 100px !important;
                }

                .input-number {
                    min-width: 92px !important;
                }

                .btn-add-re {
                    float: right;
                }

                /*.tbl-act > tbody > tr > td:nth-child(2) {
                    min-width: 40px !important;
                    text-align: center !important;
                }*/
                .text-left {
                    text-align: left !important;
                    font-weight: bold;
                }

                .tr-comp input, .tr-proy input {
                    font-weight: bold;
                }

                .th-proy {
                    min-width: 630px !important;
                }
            </style>

            <div class="panel panel-primary">
                <!-- <div class="panel-heading">
                    <h4>OBJETIVO</h4>
                </div> -->
                <div class="panel-body">
                    <div class="row">
                        <div class="container">
                            <!-- <a href="<?= \Yii::$app->request->BaseUrl ?>/plan-adquisiciones/listado" id="volvers">VOLVER</a> -->
                            <div class="text-left">
                                <a class="btn btn-inverse-alt" id="volvers" href="<?= \Yii::$app->request->BaseUrl ?>/plan-adquisiciones/listado">
                                    <i class="fa fa-arrow-left"></i>&nbsp;Volver
                                </a>
                            </div>
                            <hr>

                            <?php if(!empty($detallepa)): ?>
                                <div class="table-responsive" style="max-height: 800px;">
                                    <table class="table table-bordered table-condensed tbl-act">
                                        <tbody>
                                            <tr class="tr-header">
                                                <td rowspan="2" class="text-center">Nro de Referencia</td>
                                                <td rowspan="2" class="text-center">Descripción Detallada</td>
                                                <td rowspan="2" class="text-center">Unidad de Medida</td>
                                                <td rowspan="2" class="text-center">Cantidad</td>
                                                <td rowspan="2" class="text-center">Codificación Segun Matriz</td>
                                                <td rowspan="2" class="text-center">Costo Estimado de la <br>Adquisición o Contratación (S/.)</td>
                                                <td rowspan="2" class="text-center">Método de Adquisición o Contratación</td>
                                                <td colspan="2" class="text-center">Fuente Financiamiento</td>
                                                <td rowspan="2" class="text-center">Fecha Estimada</td>
                                                <td rowspan="2" class="text-center">Objetivo - Actividad</td>
                                                <td rowspan="2" class="text-center">Observaciones</td>
                                            </tr>
                                            <tr class="tr-header">
                                                <td class="text-center">PNIA</td>
                                                <td class="text-center">Otro</td>
                                            </tr>

                                            <!-- <tr class="tr-header">
                                                <td class="text-center">1</td>
                                                <td class="text-left" colspan="10">Bienes</td>
                                            </tr> -->
                                            <?php 
                                            $ca = '';
                                            $i=0;$x=0; foreach ($detallepa as $value): ?>
                                                    <?php if($value['Categoria'] != $ca): ?>
                                                        <tr class="tr-header">
                                                            <td class="text-center">
                                                                <?php echo ($i+1); ?>
                                                            </td>
                                                            <td class="text-left" colspan="11"><?php echo strtoupper($value['Categoria']) ?></td>
                                                        </tr>
                                                    <?php $ca = $value['Categoria']; $i++;$x=0; ?>
                                                    <?php endif ?>

                                                    <tr data-recurso-id="<?php echo $value['ID']; ?>" id="<?php echo $value['ID']; ?>" data-categoria-id="<?php echo $value['Codigo']; ?>">
                                                        <td class="text-center">
                                                            <?php if($Situacion == 0): ?>
                                                                <a href="#" class="btn-remove-recurso" data-toggle="tooltip" title="" data-id="<?php echo $value['ID']; ?>" data-original-title="Eliminar"><i class="fa fa-remove fa-lg"></i></a>
                                                            <?php endif; ?>
                                                               
                                                            <?php if($value['Situacion'] == 3){ ?>
                                                                <a href="javascript:void(0);" class="btn-edit-are" data-toggle="tooltip" title="" data-original-title="Detalle"><i class="fa fa-search"></i></a>
                                                            <?php } ?>

                                                            <?php echo ($i).".".($x+1);  ?>
                                                            <input type="hidden" class="correlativo" name="correlativo" value="<?php echo ($i).".".($x+1);  ?>">

                                                        </td>
                                                        
                                                        <td class="nombrePac">
                                                            <?php if($Situacion == 0): ?>
                                                                <textarea class="form-control" style="width: 150px"><?php echo $value['Nombre'] ?></textarea>
                                                            <?php else: ?>
                                                                <?php echo $value['Nombre'] ?>
                                                            <?php endif; ?>
                                                        </td>

                                                        <td class="text-center unidadPac"><?php echo $value['UnidadMedida'] ?></td>
                                                        
                                                        <td class="text-center">
                                                            <?php if($Situacion == 0): ?>
                                                                <input type="text" name="cantidad" value="<?php echo $value['Cantidad'] ?>" class="form-control input-cantidad input-number">
                                                            <?php else: ?>
                                                                <?php echo $value['Cantidad'] ?>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td class="text-center">
                                                            <?php echo $value['Codificacion'] ?>
                                                            <input type="hidden" class="codificacion" name="codificacion" value="<?php echo $value['Codificacion'] ?>">
                                                        </td>
                                                        <!-- <td class="text-center">-</td> -->
                                                        <td class="text-center costoPac input-number"><?php echo $value['Cantidad']*$value['CostoUnitario'] ?></td>
                                                        <td class="text-center">
                                                            <?php if($Situacion == 0): ?>
                                                            <select class="form-control select-metodo" style="width: 80px;" name="metodo" <?php echo trim($value['MetodoAdquisicion']) != ''?'changed="changed"':'' ?> >
                                                                <option value="" <?php echo $value['MetodoAdquisicion'] == ''?'selected':'' ?> >[SELECCIONE]</option>
                                                                <option value="CP" <?php echo trim($value['MetodoAdquisicion']) == 'CP'?'selected':'' ?> >CP</option>
                                                                <option value="CV" <?php echo trim($value['MetodoAdquisicion']) == 'CV'?'selected':'' ?>>CV</option>
                                                                <option value="CV" <?php echo trim($value['MetodoAdquisicion']) == 'CD'?'selected':'' ?>>CD</option>
                                                            </select>
                                                            <?php else: ?>
                                                                <?php echo $value['MetodoAdquisicion'] ?>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td class="text-center input-number"><?php echo ($value['RazonSocial'] == 'PNIA' || $value['RazonSocial'] = 'PROGRAMA NACIONAL DE INNOVACION AGRARIA- PNIA') ? $value['Cantidad']*$value['CostoUnitario'] : '-' ?></td>
                                                        <td class="text-center"><?php echo '-' ?></td>
                                                        <td class="text-center">
                                                            <?php if($Situacion == 0): ?>
                                                            <select class="form-control select-fecha" name="metodo" <?php echo trim($value['FechaInicioPA']) != ''?'changed="changed"':'' ?> style="width: 100px;">
                                                                <option>[SELECCIONE]</option>
                                                                <option value="ENE" <?php echo trim($value['FechaInicioPA']) == 'ENE'?'selected':'' ?> >Enero</option>
                                                                <option value="FEB" <?php echo trim($value['FechaInicioPA']) == 'FEB'?'selected':'' ?> >Febrero</option>
                                                                <option value="MAR" <?php echo trim($value['FechaInicioPA']) == 'MAR'?'selected':'' ?> >Marzo</option>
                                                                <option value="ABR" <?php echo trim($value['FechaInicioPA']) == 'ABR'?'selected':'' ?> >Abril</option>
                                                                <option value="MAY" <?php echo trim($value['FechaInicioPA']) == 'MAY'?'selected':'' ?> >Mayo</option>
                                                                <option value="JUN" <?php echo trim($value['FechaInicioPA']) == 'JUN'?'selected':'' ?> >Junio</option>
                                                                <option value="JUL" <?php echo trim($value['FechaInicioPA']) == 'JUL'?'selected':'' ?> >Julio</option>
                                                                <option value="AGO" <?php echo trim($value['FechaInicioPA']) == 'AGO'?'selected':'' ?> >Agosto</option>
                                                                <option value="SEP" <?php echo trim($value['FechaInicioPA']) == 'SEP'?'selected':'' ?> >Septiembre</option>
                                                                <option value="OCT" <?php echo trim($value['FechaInicioPA']) == 'OCT'?'selected':'' ?> >Octubre</option>
                                                                <option value="NOV" <?php echo trim($value['FechaInicioPA']) == 'NOV'?'selected':'' ?> >Noviembre</option>
                                                                <option value="DIC" <?php echo trim($value['FechaInicioPA']) == 'DIC'?'selected':'' ?> >Diciembre</option>
                                                            </select>
                                                            <?php else: ?>
                                                                <?php echo $value['FechaInicioPA'] ?>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td class="text-center">
                                                            <?php if(!empty($value['NumeracionPAC'])): ?>
                                                                <input type="hidden" class="jsonubica" name="jsonubicacion" id="ubicacion_<?php echo $value['ID']; ?>" value='<?php echo $value['NumeracionPAC'] ?>''>
                                                                <?php /*foreach (json_decode($value['NumeracionPAC']) as $key => $valuex): ?>
                                                                    <?php 
                                                                    echo "Objetivo:".$valuex->correlativoComp.'- Actividad: '.$valuex->correlativoComp.'.'.$valuex->correlativoActv.', ';*/ ?>
                                                                <?php echo $value['NumeracionPAC'] ?>
                                                            <?php endif ?>
                                                        </td>
                                                        <td>
                                                            <?php if($Situacion == 0): ?>
                                                                <textarea class="observa" id="obs"><?php echo $value['Observaciones'] ?></textarea>
                                                            <?php else: ?>
                                                                <?php echo $value['Observaciones'] ?>
                                                            <?php endif; ?>
                                                        </td>
                                                    </tr>
                                                    
                                            <?php $x++;endforeach ?>
                                        </tbody>
                                    </table>
                                </div>
                                <hr>
                                <div class="col-sm-12">
                                    <div class="text-center">
                                        <?php if($Situacion == 0): ?>
                                           <button class="btn btn-primary btn-save-envia" id="envia" data-id="<?php echo $Pac; ?>" ><i class="fa fa-check"></i>&nbsp;Enviar PAC</button>
                                           <button class="btn btn-primary btn-save-poaf" id="guardar"><i class="fa fa-check"></i>&nbsp;Guardar</button>
                                        <?php endif; ?>
                                        <a class="btn btn-inverse-alt" href="<?= \Yii::$app->request->BaseUrl ?>/plan-adquisiciones/descargar-pa?codigo=<?php echo $Codigo ?>&anio=<?php echo $anio ?>">
                                            <i class="fa fa-download"></i>&nbsp;Descargar
                                        </a>
                                    </div>
                                </div>
                            <?php else: ?>
                                <div class="alert alert-info">
                                    <i class="fa fa-info"></i>&nbsp;&nbsp;El plan de adquisiciones no ha sido creada.
                                </div>
                            <?php endif ?>

                            <style type="text/css">
                                .vertical{
                                    vertical-align: middle;
                                }
                            </style>
                            <script type="text/javascript">
                                

                            $(document).on('click','#envia',function (e) {
                                msg = 'Esta seguro que desea enviar el PA';
                                var vID = $(this).attr('data-id');
                                bootBoxConfirm(msg,function(){
                                    $.ajax({
                                        cache: false,
                                        type: 'POST',
                                        // contentType: 'application/json;charset=utf-8',
                                        // dataType: 'json',
                                        url: '<?= \Yii::$app->request->BaseUrl ?>/plan-adquisiciones/save-send',
                                        data: 'id='+vID,
                                        success: function (data) {
                                            location.href ="<?= \Yii::$app->request->BaseUrl ?>/plan-adquisiciones/detalle-plan-adquisicion?anio=<?php echo $anio ?>";
                                        },
                                        error: function () {
                                            _pageLoadingEnd();
                                        }
                                    });
                                });
                            });


                                $(document).on('click','.btn-save-poaf',function (e) {
                                    e.preventDefault();
                                    _pageLoadingStart();
                                    var arrCategoria = [];
                                    var arrMetodo = [];
                                    var arrFecha = [];
                                    var arrVacios = [];
                                    var arrObjetivo = [];
                                    var arrVacios = [];
                                    var arrObserva = [];
                                    var arrCorrelativo = [];
                                    var arrCodificacion = [];

                                    var arrDescripcion = [];
                                    var arrUnidad = [];
                                    var arrCosto = [];

                                    var proceso = $(this).attr('id');

                                    $('.input-cantidad').each(function () {
                                        var $this = $(this);
                                        var aa = $this.parent().parent().attr('data-recurso-id');
                                        var caa = $this.parent().parent().attr('data-categoria-id');
                                        // console.log(aa);

                                        var valor = parseFloat($this.inputmask('unmaskedvalue'));
                                        // console.log(valor);
                                        if (!isNaN(valor) || aa == '' || caa == '') {
                                            arrCategoria.push({
                                                ID: aa,
                                                CAT: caa,
                                                CANT: valor
                                            });
                                        }else{
                                            arrVacios.push({
                                                VACIO: 1
                                            })
                                        }
                                    });
                                    
                                    $('.select-fecha[changed="changed"]').each(function () {
                                        var $this = $(this);
                                        // console.log($this);
                                        var aa = $this.parent().parent().attr('data-recurso-id');
                                        var caa = $this.parent().parent().attr('data-categoria-id');

                                        var valor = ($this.inputmask('unmaskedvalue'));
                                        // console.log(valor);
                                        if (valor != '') {
                                            arrFecha.push({
                                                ID: aa,
                                                CAT: caa,
                                                FEC: valor
                                            });
                                        }
                                    });


                                    $('.select-metodo[changed="changed"]').each(function () {
                                        var $this = $(this);
                                        var aa = $this.parent().parent().attr('data-recurso-id');
                                        var caa = $this.parent().parent().attr('data-categoria-id');
                                        var valor = ($this.inputmask('unmaskedvalue'));
                                        if (valor != '') {
                                            arrMetodo.push({
                                                ID: aa,
                                                CAT: caa,
                                                MET: valor
                                            });
                                        }
                                    });

                                    $('.nombrePac').each(function () {
                                        var $this = $(this);
                                        var aa = $this.parent().attr('data-recurso-id');
                                        var caa = $this.parent().attr('data-categoria-id');

                                        var valor = $this.children().val();
                                        console.log(valor);
                                        if (valor != '') {
                                            arrDescripcion.push({
                                                ID: aa,
                                                CAT: caa,
                                                NOM: valor
                                            });
                                        }
                                    });

                                    $('.unidadPac').each(function () {
                                        var $this = $(this);
                                        var aa = $this.parent().attr('data-recurso-id');
                                        var caa = $this.parent().attr('data-categoria-id');

                                        var valor = $this.text();
                                        if (valor != '') {
                                            arrUnidad.push({
                                                ID: aa,
                                                CAT: caa,
                                                UNID: valor
                                            });
                                        }
                                    });
                                    
                                    $('.costoPac').each(function () {
                                        var $this = $(this);
                                        var aa = $this.parent().attr('data-recurso-id');
                                        var caa = $this.parent().attr('data-categoria-id');
                                        var valor = $this.text();
                                        if (valor != '') {
                                            arrCosto.push({
                                                ID: aa,
                                                CAT: caa,
                                                COSTO: valor
                                            });
                                        }
                                    });


                                    $('.jsonubica').each(function () {
                                        var $this = $(this);
                                        var aax = $this.val();
                                        var aa = $this.parent().parent().attr('data-recurso-id');
                                        var caa = $this.parent().parent().attr('data-categoria-id');
                                        console.log(aa);
                                        // var valor = ($this.inputmask('unmaskedvalue'));
                                        if (aa != '') {
                                            arrObjetivo.push({
                                                ID: aa,
                                                OBJ: aax,
                                                CAT: caa
                                            });
                                        }
                                    });


                                    $('.correlativo').each(function () {
                                        var $this = $(this);
                                        var corela = $this.val();
                                        var aa = $this.parent().parent().attr('data-recurso-id');
                                        var caa = $this.parent().parent().attr('data-categoria-id');
                                        if (corela != '') {
                                            arrCorrelativo.push({
                                                ID: aa,
                                                COR: corela,
                                                CAT: caa
                                            });
                                        }
                                    });

                                    $('.codificacion').each(function () {
                                        var $this = $(this);
                                        var codifica = $this.val();
                                        var aa = $this.parent().parent().attr('data-recurso-id');
                                        var caa = $this.parent().parent().attr('data-categoria-id');
                                        if (codifica != '') {
                                            arrCodificacion.push({
                                                ID: aa,
                                                CODI: codifica,
                                                CAT: caa
                                            });
                                        }
                                    });

                                    $('.observa').each(function () {
                                        var $this = $(this);
                                        var obs = $this.val();
                                        var aa = $this.parent().parent().attr('data-recurso-id');
                                        var caa = $this.parent().parent().attr('data-categoria-id');
                                        console.log(obs);
                                        // var valor = ($this.inputmask('unmaskedvalue'));
                                        if (obs != '') {
                                            arrObserva.push({
                                                ID: aa,
                                                OBS: obs,
                                                CAT: caa
                                            });
                                        }
                                    });

                                    // $('.correlativo').each(function () {
                                        //     var $this = $(this);
                                        //     var corela = $this.val();
                                        //     var aa = $this.parent().parent().attr('data-recurso-id');
                                        //     var caa = $this.parent().parent().attr('data-categoria-id');
                                        //     if (corela != '') {
                                        //         arrCorrelativo.push({
                                        //             ID: aa,
                                        //             COR: corela,
                                        //             CAT: caa
                                        //         });
                                        //     }
                                        // });


                                        
                                        // console.log(arrFecha);
                                        // $('.select-fecha[changed="changed"]').removeAttr('changed');
                                        // $('.select-metodo[changed="changed"]').removeAttr('changed');
                                    
                                    $('.select-fecha').each(function (e,y) {
                                        var $this = $(this);
                                        if($this.attr('changed') != 'changed'){
                                            arrVacios.push({
                                                VACIO: 1
                                            });
                                        }
                                    });

                                    $('.select-metodo').each(function (e,y) {
                                        var $this = $(this);
                                        if($this.attr('changed') != 'changed'){
                                            arrVacios.push({
                                                VACIO: 1
                                            });
                                        }
                                    });

                                    console.log(arrDescripcion);
                                    // console.log(arrVacios.length);
                                    if(arrVacios.length != 0){
                                        bootbox.alert('Debe de seleccionar todo los campos');
                                    }else{
                                        if (arrMetodo.length || arrFecha.length || arrCategoria.length) {
                                            var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                                            var _obj = {
                                                Metodo      : JSON.stringify(arrMetodo),
                                                Fecha       : JSON.stringify(arrFecha),
                                                Categoria   : JSON.stringify(arrCategoria),
                                                Objetivo    : JSON.stringify(arrObjetivo),
                                                Observa     : JSON.stringify(arrObserva),
                                                Correlativo : JSON.stringify(arrCorrelativo),
                                                Anio        : <?php echo $anio ?>,
                                                Codificacion : JSON.stringify(arrCodificacion),
                                                Descripcion   : JSON.stringify(arrDescripcion),
                                                UnidadMedida  : JSON.stringify(arrUnidad),
                                                CostoUnitario : JSON.stringify(arrCosto),
                                                _csrf       : toke,
                                                Proceso     : proceso
                                            };

                                            // console.log(_obj);
                                            // console.log(JSON.stringify(_obj));
                                            var msg = '';
                                            var ref;
                                            if(proceso == 'envia'){
                                                msg = 'Esta seguro que desea enviar el PA';
                                            }else{
                                                msg = 'Esta seguro que desea guardar el PA';
                                            }
                                            bootBoxConfirm(msg,function(){
                                                $.ajax({
                                                    cache: false,
                                                    type: 'POST',
                                                    // contentType: 'application/json;charset=utf-8',
                                                    // dataType: 'json',
                                                    url: '<?= \Yii::$app->request->BaseUrl ?>/plan-adquisiciones/save',
                                                    data: (_obj),
                                                    success: function (data) {
                                                        // location.href ="<?= \Yii::$app->request->BaseUrl ?>/plan-adquisiciones/detalle-plan-adquisicion?anio=<?php echo $anio ?>";
                                                    },
                                                    error: function () {
                                                        _pageLoadingEnd();
                                                    }
                                                });
                                            });
                                            
                                        } else {
                                            bootbox.alert('Debe agregar un recurso');
                                            _pageLoadingEnd();
                                        }
                                    }
                                });
                                
                                $('body').on('change', '.select-metodo', function (e) {
                                    e.preventDefault();
                                    $(this).attr('changed', 'changed');
                                });

                                $('body').on('change', '.select-fecha', function (e) {
                                    e.preventDefault();
                                    $(this).attr('changed', 'changed');
                                });

                                $('.btn-remove-recurso').click(function(){
                                    var s = $(this).attr('data-id');
                                    bootBoxConfirm('Esta seguro que desea eliminar recurso del PA',function(){
                                        $.ajax({
                                            cache: false,
                                            type: 'POST',
                                            url: '<?= \Yii::$app->request->BaseUrl ?>/plan-adquisiciones/delete',
                                            data: 'recurso='+s,
                                            success: function (data) {
                                                $('#'+s).remove();
                                                // location.reload();
                                            },
                                            error: function () {
                                                _pageLoadingEnd();
                                            }
                                        });
                                    });
                                });


                                $('body').on('change', '.btn-save-cronograma-recurso', function (e) {
                                    var $this = $(this);
                                    var ID=$this.attr('data-cronograma-recurso-id');
                                    var RecursoID=$this.attr('data-recurso-id');
                                    var RecursoMetaFisica=$this.attr('data-meta-fisica');
                                    
                                    suma=0;
                                    $('.recurso_'+RecursoID).each(function(){
                                        suma = suma+parseFloat(getNum($(this).val()));
                                    });
                                    
                                    if (RecursoMetaFisica<suma) {
                                        bootbox.alert('La cantidad supera a la meta fisica general.');
                                        $this.val('0.00');
                                        return false;
                                    }
                                    if (RecursoMetaFisica==suma)
                                    {
                                        $('.recurso_alerta_'+RecursoID).css("color", "black");
                                    }
                                    else
                                    {
                                        $('.recurso_alerta_'+RecursoID).css("color", "red");
                                    }
                                    
                                    var MetaFisica=$this.val();
                                    $.ajax({
                                        cache: false,
                                        type: 'GET',
                                        url: '<?= \Yii::$app->request->BaseUrl ?>/recursos-poa/grabar-meta-cronograma-recurso',
                                        data: {ID:ID,MetaFisica:MetaFisica},
                                        success: function (data) {
                                            if (data.Success) {
                                                loadPoaf();
                                                _pageLoadingEnd();
                                            } else {
                                                //toastr.error(data.Error);
                                            }
                                        },
                                        error: function () {
                                            _pageLoadingEnd();
                                        }
                                    });
                                    return true;
                                });

                                $('.input-number').inputmask("decimal", {
                                    radixPoint: ".",
                                    groupSeparator: ",",
                                    groupSize: 3,
                                    digits: 2,
                                    integerDigits: 7,
                                    autoGroup: true,
                                    allowPlus: false,
                                    allowMinus: true,
                                    placeholder: ''
                                }).click(function () {
                                    $(this).select();
                                });

                            </script>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>


