<style type="text/css">
    select.form-control.categoria {
        width: 250px;
    }
</style>
<div class="container">
    <div class="text-left">
        <a class="btn btn-inverse-alt" id="volvers" href="#">
            <i class="fa fa-arrow-left"></i>&nbsp;Volver
        </a>
    </div>

	<hr>


    <div class="table-responsive" style="max-height: 800px;">
        <table class="table table-bordered table-condensed tbl-act">
            <tbody>
                <tr class="tr-header">
                    <td rowspan="2" class="text-center">Nro de Referencia</td>
                    <td rowspan="2" class="text-center">Descripción Detallada</td>
                    <td rowspan="2" class="text-center">Unidad de Medida</td>
                    <td rowspan="2" class="text-center">Cantidad</td>
                    <td rowspan="2" class="text-center">Codificación Segun Matriz</td>
                    <td rowspan="2" class="text-center">Costo Estimado de la <br>Adquisición o Contratación (S/.)</td>
                    <td rowspan="2" class="text-center">Método de Adquisición o Contratación</td>
                    <td colspan="2" class="text-center">Fuente Financiamiento</td>
                    <td rowspan="2" class="text-center">Fecha Estimada</td>
                    <td width="10" rowspan="2" class="text-center">Objetivo - Actividad</td>
                    <td rowspan="2" class="text-center">Observaciones</td>
                </tr>
                <tr class="tr-header">
                    <td class="text-center">PNIA</td>
                    <td class="text-center">Otro</td>
                </tr>
 
                <?php if(!empty($cab)): ?>
                    <?php 
                    $i=1; foreach ($cab as $value):?>
                            <tr style="background: #e2e2e2;">
                                <td><?php echo ($i); ?></td>
                                <td colspan="11" class="left-bold"><?php echo $value['nombre']; ?></td>
                            </tr>
                            <?php $ix=1; if(!empty($info)): ?>
                                <?php  foreach ($info as $valuex):  ?>
                                    <?php if($value['id'] == $valuex['DES']): ?>
                                        <tr data-recurso-id="<?php echo $valuex['ID']; ?>" id="<?php echo $valuex['ID']; ?>" data-categoria-id="<?php echo $value['id']; ?>">
                                            <td>
                                                <a href="#" class="btn-remove-recurso" data-toggle="tooltip" title="" data-id="<?php echo $valuex['ID']; ?>" data-original-title="Eliminar">
                                                    <i class="fa fa-remove fa-lg"></i>
                                                </a>
                                                <?php echo ($i).".".($ix);  ?>
                                                <input type="hidden" class="correlativo" name="correlativo" value="<?php echo ($i).".".($ix);  ?>">
                                            </td>

                                            <td class="nombrePac"><textarea class="form-control" style="width: 150px"><?php echo $valuex['Nombre'] ?></textarea></td>
                                            <td class="text-center unidadPac"><?php echo $valuex['UnidadMedida'] ?></td>
                                            <td>
                                                <input class="form-control input-cantidad input-number input-number" type="text" name="cantidad">
                                            </td>
                                            <td class="text-center">
                                                <?php echo $valuex['RUB'] ?>
                                                <input type="hidden" class="codificacion" name="codificacion" value="<?php echo $valuex['RUB'] ?>">
                                            </td>
                                            <td class="text-center costoPac input-number"><?php echo $valuex['CostoUnitario'] ?></td>
                                            <td>
                                                <select class="form-control select-metodo" style="width: 80px;" name="metodo" data-recursos-id="<?php echo $valuex['ID']; ?>" required>
                                                    <option>[SELECCIONE]</option>
                                                    <option value="CP">Comparación de precios</option>
                                                    <option value="CV">Comparación de CV</option>
                                                    <option value="CD">Compra Directa</option>
                                                </select>
                                            </td>
                                            <td class="text-center input-number">
                                                <?php //echo $valuex['Fuente']== 'PNIA'?$valuex['MontoFuente']:'-' ?>
                                                <?php echo $valuex['Fuente']== 'PNIA'?$valuex['CostoUnitario']:'-' ?>
                                            </td>
                                            <td class="text-center">
                                                <?php echo $valuex['Fuente']!= 'PNIA'?$valuex['MontoFuente']:'-' ?>
                                            </td>
                                            <td>
                                                <select class="form-control select-fecha" style="width: 100px;" name="fecha" data-recursox-id="<?php echo $valuex['ID']; ?>" required>
                                                    <option>[SELECCIONE]</option>
                                                    <option value="ENE">Enero</option>
                                                    <option value="FEB">Febrero</option>
                                                    <option value="MAR">Marzo</option>
                                                    <option value="ABR">Abril</option>
                                                    <option value="MAY">Mayo</option>
                                                    <option value="JUN">Junio</option>
                                                    <option value="JUL">Julio</option>
                                                    <option value="AGO">Agosto</option>
                                                    <option value="SEP">Septiembre</option>
                                                    <option value="OCT">Octubre</option>
                                                    <option value="NOV">Noviembre</option>
                                                    <option value="DIC">Diciembre</option>
                                                </select>
                                            </td>
                                            <td class="text-center">
                                                <?php  echo $valuex['seg'] ?>
                                                <input type="hidden" class="jsonubica" name="jsonubicacion" id="ubicacion_<?php echo $valuex['seg']; ?>" value="<?php echo $valuex['seg']; ?>">
                                                <!-- <div class="ubicacion_<?php echo $valuex['ID']; ?>">
                                                    <a href="#" class="btn btn-info add-component">Agregar<br> Componente</a>
                                                </div> -->
                                            </td>
                                            <td><textarea name="observaciones" class="observa" id="obs"></textarea></td>
                                        </tr>
                                    <?php $ix++;endif ?>
                                <?php endforeach ?>
                            <?php endif ?>

                    <?php $i++;$ix=1;endforeach ?>
                <?php endif ?>

            </tbody>
        </table>
    </div>
	<hr>
	<div class="col-sm-12">
	    <div class="text-center">
            <?php if($Situacion == 0): ?>
	           <button class="btn btn-primary btn-save-poaf"><i class="fa fa-check"></i>&nbsp;Guardar y Procesar</button>
            <?php endif; ?>
	        <!-- <a class="btn btn-inverse-alt" href="">
                <i class="fa fa-download"></i>&nbsp;Descargar
            </a> -->
	    </div>
	</div>


	<style type="text/css">
		.vertical{
			vertical-align: middle;
		}
	</style>

	<script type="text/javascript">
		$('#volvers').click(function(e){
            e.preventDefault();
	        $('#result_preview').addClass('hidden');
	        $('#result_preview').html('');
	        $('#contendo_preview').removeClass('hidden');
	    });

        

    	$('.btn-save-poaf').click(function (e) {
            e.preventDefault();
            _pageLoadingStart();
            var arrCategoria = [];
            var arrMetodo = [];
            var arrFecha = [];
            var arrObjetivo = [];
            var arrVacios = [];
            var arrObserva = [];
            var arrCorrelativo = [];
            var arrCodificacion = [];

            var arrDescripcion = [];
            var arrUnidad = [];
            var arrCosto = [];
         
            $('.input-cantidad').each(function () {
                var $this = $(this);
                var aa = $this.parent().parent().attr('data-recurso-id');
                var caa = $this.parent().parent().attr('data-categoria-id');
                var valor = parseFloat($this.inputmask('unmaskedvalue'));
                if (!isNaN(valor) || aa == '' || caa == '') {
                    arrCategoria.push({
                        ID: aa,
                        CAT: caa,
                        CANT: valor
                    });
                }else{
                	arrVacios.push({
                		VACIO: 1
                	})
                }
            });
            
            $('.select-fecha[changed="changed"]').each(function () {
                var $this = $(this);
                var aa = $this.parent().parent().attr('data-recurso-id');
                var caa = $this.parent().parent().attr('data-categoria-id');

                var valor = ($this.inputmask('unmaskedvalue'));
                if (valor != '') {
                    arrFecha.push({
                    	ID: aa,
                    	CAT: caa,
                        FEC: valor
                    });
                }
            });

            $('.nombrePac').each(function () {
                var $this = $(this);
                var aa = $this.parent().attr('data-recurso-id');
                var caa = $this.parent().attr('data-categoria-id');

                var valor = $this.children().val();
                if (valor != '') {
                    arrDescripcion.push({
                        ID: aa,
                        CAT: caa,
                        NOM: valor
                    });
                }
            });

            $('.unidadPac').each(function () {
                var $this = $(this);
                var aa = $this.parent().attr('data-recurso-id');
                var caa = $this.parent().attr('data-categoria-id');

                var valor = $this.text();
                if (valor != '') {
                    arrUnidad.push({
                        ID: aa,
                        CAT: caa,
                        UNID: valor
                    });
                }
            });
            
            $('.costoPac').each(function () {
                var $this = $(this);
                var aa = $this.parent().attr('data-recurso-id');
                var caa = $this.parent().attr('data-categoria-id');
                var valor = $this.text();
                if (valor != '') {
                    arrCosto.push({
                        ID: aa,
                        CAT: caa,
                        COSTO: valor
                    });
                }
            });

            $('.select-metodo[changed="changed"]').each(function () {
                var $this = $(this);
                var aa = $this.parent().parent().attr('data-recurso-id');
                var caa = $this.parent().parent().attr('data-categoria-id');
                var valor = ($this.inputmask('unmaskedvalue'));
                if (valor != '') {
                    arrMetodo.push({
                    	ID: aa,
                    	CAT: caa,
                        MET: valor
                    });
                }
            });

            $('.jsonubica').each(function () {
                var $this = $(this);
                var aax = $this.val();
                var aa = $this.parent().parent().attr('data-recurso-id');
                var caa = $this.parent().parent().attr('data-categoria-id');
                if (aa != '') {
                    arrObjetivo.push({
                        ID: aa,
                        OBJ: aax,
                        CAT: caa
                    });
                }
            });

            $('.observa').each(function () {
                var $this = $(this);
                var obs = $this.val();
                var aa = $this.parent().parent().attr('data-recurso-id');
                var caa = $this.parent().parent().attr('data-categoria-id');
                if (obs != '') {
                    arrObserva.push({
                        ID: aa,
                        OBS: obs,
                        CAT: caa
                    });
                }else{
                    arrVacios.push({
                        VACIO: 1
                    })
                }
            });

            $('.correlativo').each(function () {
                var $this = $(this);
                var corela = $this.val();
                var aa = $this.parent().parent().attr('data-recurso-id');
                var caa = $this.parent().parent().attr('data-categoria-id');
                if (corela != '') {
                    arrCorrelativo.push({
                        ID: aa,
                        COR: corela,
                        CAT: caa
                    });
                }
            });

            $('.codificacion').each(function () {
                var $this = $(this);
                var codifica = $this.val();
                var aa = $this.parent().parent().attr('data-recurso-id');
                var caa = $this.parent().parent().attr('data-categoria-id');
                if (codifica != '') {
                    arrCodificacion.push({
                        ID: aa,
                        CODI: codifica,
                        CAT: caa
                    });
                }
            });
            
            $('.select-fecha').each(function (e,y) {
                var $this = $(this);
                if($this.attr('changed') != 'changed'){
                    arrVacios.push({
                        VACIO: 1
                    });
                }
            });

            $('.select-metodo').each(function (e,y) {
                var $this = $(this);
                if($this.attr('changed') != 'changed'){
                    arrVacios.push({
                        VACIO: 1
                    });
                }
            });

            if(arrVacios.length != 0){
            	bootbox.alert('Debe de seleccionar todo los campos');
            }else{
    	        if (arrMetodo.length || arrFecha.length || arrCategoria.length) {
    	            var toke = '<?=Yii::$app->request->getCsrfToken()?>';
    	            var _obj = {
    	                Metodo 		: JSON.stringify(arrMetodo),
    	                Fecha 		: JSON.stringify(arrFecha),
    	                Categoria 	: JSON.stringify(arrCategoria),
                        Observa     : JSON.stringify(arrObserva),
                        Objetivo    : JSON.stringify(arrObjetivo),
                        Correlativo : JSON.stringify(arrCorrelativo),
                        Codificacion : JSON.stringify(arrCodificacion),

                        Descripcion   : JSON.stringify(arrDescripcion),
                        UnidadMedida  : JSON.stringify(arrUnidad),
                        CostoUnitario : JSON.stringify(arrCosto),

                        Anio        : <?php echo $anio ?>,
                        Proceso     : '',
    	                _csrf: toke
    	            };

                    console.log(_obj);

                    bootBoxConfirm('Esta seguro que desea enviar el PA', function(){
                        // var jsonString = JSON.stringify(_obj);
                        $.ajax({
                            url: '<?= \Yii::$app->request->BaseUrl ?>/plan-adquisiciones/save',
                            type: 'POST',
                            data: _obj,
                            success: function (data) {
                                location.href ="<?= \Yii::$app->request->BaseUrl ?>/plan-adquisiciones/detalle-plan-adquisicion?anio=<?php echo $anio ?>";
                            },
                            error: function () {
                                _pageLoadingEnd();
                            }
                        });
                  
                    });
                    
    	        } else {
    	            _pageLoadingEnd();
    	        }
            }
        });

        $('body').on('change', '.select-metodo', function (e) {
            e.preventDefault();
            $(this).attr('changed', 'changed');
        });

        $('body').on('change', '.select-fecha', function (e) {
            e.preventDefault();
            $(this).attr('changed', 'changed');
        });


        $('.btn-remove-recurso').click(function(e){
            e.preventDefault();
            var s = $(this).attr('data-id');
            // console.log(s);
            $('#'+s).remove();
        });

        $('.input-number').inputmask("decimal", {
            radixPoint: ".",
            groupSeparator: ",",
            groupSize: 3,
            digits: 2,
            integerDigits: 7,
            autoGroup: true,
            allowPlus: false,
            allowMinus: true,
            placeholder: ''
        }).click(function () {
            $(this).select();
        });
	</script>
	
</div>