<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'entidad-participante/crear',
        'options'           => [
            'autocomplete'  => 'off',
            'id'            => 'frmColaboradora'
        ]
    ]
); ?>


    <div class="form-horizontal">
        <div class="form-group">
            <label class="col-sm-2 control-label">Componentes</label>
            <div class="col-sm-4">
                <input type="hidden" name="recurso" id="recurso" value="<?php echo $recurso ?>">
                <select class="form-control objetivo-select" required="required">
                    <option  value="00">[SELECCIONE]</option>
                    <?php foreach ($Componente as $xx): ?>
                        <option value="<?php echo $xx->ID ?>"><?php echo "OBJ-".$xx->Correlativo .': '.$xx->Nombre ?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <label class="col-sm-2 control-label">Actividades</label>
            <div class="col-sm-4">
                <select class="form-control Actividades" required="required">
                    <option  value="00">[SELECCIONE]</option>
                </select>
            </div>
        </div>
    </div>

    <div class="text-left" style="padding-bottom: 5px;">
        <!-- <a class="btn btn-midnightblue-alt" id="agregar_ob" href="">
            <i class="fa fa-plus"></i>&nbsp;Añadir
        </a> -->
        <button type="submit" class="btn btn-midnightblue-alt" id="agregar_ob"><i class="fa fa-plus"></i>&nbsp;Añadir</button>
    </div>

    <hr>
    <table class="table table-bordered table-condensed tbl-act">
        <tr class="tr-header">
            <td class="text-center">#</td>
            <td class="text-center">Objetivo</td>
            <td class="text-center">Actividades</td>
        </tr>
        <tbody id="contenido_obj">
        </tbody>
    </table>   

    <div class="panel-footer text-center">
        <input type="hidden" name="json" id="json" value='<?php echo $json ?>'>
        <button type="button" id="btn-save-colaboradora" class="btn-primary btn"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>


<script>

    var formParsleyColaboradora = $('#frmColaboradora').parsley(defaultParsleyForm());

    var fx = $('#json').val();

    $(document).ready(function(){
        // console.log(f);
        if(fx != ''){
            var f = jQuery.parseJSON($('#json').val());
            $(f).each(function(k,v){
                // console.log(v);
                var ac = '<tr id="a_'+k+'" class="longitud">';
                    ac += '<td><a href="#" class="btn-remove" data-toggle="tooltip" title="" data-id="'+k+'" data-original-title="Eliminar"><i class="fa fa-remove fa-lg"></i></a></td>';
                    ac += '<td data-comp-id="'+ v.componenteID +'" data-comp-corelativo="'+ v.correlativoComp +'"><strong> Objetivo ' + v.correlativoComp + ':</strong> ' + v.componente + '</td>';
                    ac += '<td data-actv-id="'+ v.actividadID +'" data-actv-corelativo="'+ v.correlativoActv +'"><strong> Actividad ' + v.correlativoActv + ':</strong> ' + v.actividad  + '</td>';
                ac += '</tr>';
                $('#contenido_obj').append(ac);
            });
        }

        
    });

</script>