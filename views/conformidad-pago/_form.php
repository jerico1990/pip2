<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'conformidad-pago/crear?CodigoProyecto='.$CodigoProyecto,
        'options'           => [
            'id'            => 'frmConformidadPago',
            'enctype'       =>'multipart/form-data',
        ]
    ]
); ?>
    <input type="hidden" name="ConformidadPago[CodigoProyecto]" value="<?php echo $CodigoProyecto ?>">
    <div class="modal-body">
        <div id="loading" style="position: absolute;z-index: 1;margin-left: 34%;"></div>
        <div class="form-horizontal">
            <p id="devengado" style="color: red;font-weight: bold;text-align: center;"></p>
            <div class="form-group">
                <label class="col-sm-2 control-label">SIAF<span class="f_req">*</span></label>
                <div class="col-sm-10">
                    <select class="form-control" name="ConformidadPago[OrdenID]" id="tipo-siaf" required>
                        <option value>Seleccionar</option>
                        <?php foreach($ordenes as $orden){ ?>
                            <option value="<?= $orden['ID'] ?>"><?= 'Orden: '.str_pad($orden['Correlativo'], 3, "0", STR_PAD_LEFT).'-'.$orden['Annio'].'/MEMO: '.$orden['Memorando'].'/'.$orden['Sigla'].'/'.$orden['SIAF'] ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Nro Documento:<span class="f_req">*</span></label>
                <div class="col-sm-10">
                    <input type="text" id="correlativo" placeholder="Correlativo del documento" class="form-control" name="ConformidadPago[DocumentoCorrelativo]" required>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Mes<span class="f_req">*</span></label>
                <div class="col-sm-10">
                    <select class="form-control" name="Entregable" id="tipo" required>
                        <option value>Seleccionar</option>
                        <option value="1-<?php echo date('Y'); ?>">Enero-<?php echo date('Y'); ?></option>
                        <option value="2-<?php echo date('Y'); ?>">Febrero-<?php echo date('Y'); ?></option>
                        <option value="3-<?php echo date('Y'); ?>">Marzo-<?php echo date('Y'); ?></option>
                        <option value="4-<?php echo date('Y'); ?>">Abril-<?php echo date('Y'); ?></option>
                        <option value="5-<?php echo date('Y'); ?>">Mayo-<?php echo date('Y'); ?></option>
                        <option value="6-<?php echo date('Y'); ?>">Junio-<?php echo date('Y'); ?></option>
                        <option value="7-<?php echo date('Y'); ?>">Julio-<?php echo date('Y'); ?></option>
                        <option value="8-<?php echo date('Y'); ?>">Agosto-<?php echo date('Y'); ?></option>
                        <option value="9-<?php echo date('Y'); ?>">Septiembre-<?php echo date('Y'); ?></option>
                        <option value="10-<?php echo date('Y'); ?>">Octubre-<?php echo date('Y'); ?></option>
                        <option value="11-<?php echo date('Y'); ?>">Noviembre-<?php echo date('Y'); ?></option>
                        <option value="12-<?php echo date('Y'); ?>">Diciembre-<?php echo date('Y'); ?></option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Nº<span class="f_req">*</span></label>
                <div class="col-sm-10">
                    <input type="text" placeholder="Ejem: E-09" class="form-control" id="nroDocumento" name="ConformidadPago[NDocumento]" required>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-2 control-label">Monto<span class="f_req">*</span></label>
                <div class="col-sm-10">
                    <input type="text" class="form-control input-number" placeholder="" id="Monto" name="ConformidadPago[Monto]" required>
                </div>
            </div>


            <div class="form-group">
                <label class="col-sm-2 control-label">Concepto<span class="f_req">*</span></label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="ConformidadPago[Concepto]" id="concepto" required></textarea>
                </div>
            </div>

        </div>
    </div>
    
        
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button id="btn-save-conformidad-servicios" type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>

<script>
    var $form = $('#frmConformidadPago');
    var formParsleyConformidadPago = $form.parsley(defaultParsleyForm());
    validarNumeros()

</script>