<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

    $form = ActiveForm::begin(
    [
        // 'action'            => 'conformidad-pago/crear-conformidad-servicio?ID='.$OrdenID,
        // 'action' => empty($resultados) ? 'conformidad-pago/crear-conformidad-servicio?ID='.$OrdenID : 'conformidad-pago/update-conformidad-servicio?OrdenID='.$OrdenID.'&id='.$resultados['ID'],

        'action'            => empty($resultados['ID'])?'conformidad-pago/crear-conformidad-servicio?ID='.$OrdenID.'&edit=1'.'&code='.$Code :'conformidad-pago/crear-conformidad-servicio?ID='.$OrdenID.'&edit=1',

        'options'           => [
            'id'            => 'frmDocumentosConformidad',
            'enctype'       =>'multipart/form-data',
        ]
    ]
); ?>
    <input type="hidden" name="ConformidadPago[CodigoProyecto]" value="<?php echo $CodigoProyecto ?>">
    <div class="modal-body">
        <div class="form-horizontal">
            <?php if(!empty($resultados) AND $resultados['Obs']): ?>
                    <div class="alert alert-danger" style="text-align:justify">
                        <strong>Observación: </strong> <?php echo $resultados['Obs'] ?>
                    </div>
            <?php else: ?>
                <div class="alert alert-warning" style="text-align:justify">
                    <strong>NOTA:</strong> Adjuntar todo el sustento firmado para poder ser validado.
                </div>
            <?php endif; ?>
            <div class="form-group">
                <label class="col-sm-3 control-label">Anexo 06<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <?php if($bloqueado == 0): ?>
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="input-group">
                                <input class="form-control" type="hidden" name="DetalleConformidadPago[ID]" value="<?php echo $Code ?>">
                                <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                                <div class="form-control uneditable-input" data-trigger="fileinput">
                                    <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                    <span class="fileinput-filename">
                                    <?php if(!empty($resultados)): ?>
                                        <?php if($resultados['Anexo06']): ?>
                                            <?php echo $resultados['Anexo06'] ?>
                                        <?php else: ?>
                                            Seleccione archivo...
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    </span>
                                </div>
                                <span class="input-group-addon btn btn-default btn-file">
                                    <span class="fileinput-new">Seleccionar</span>
                                    <span class="fileinput-exists">Cambiar</span>

                                    <input type="file" onchange="return Imagen(this);" id="file-anexo06" name="DetalleConformidadPago[Anexo06Archivo]" required>
                                    <input class="form-control" type="hidden" name="DetalleConformidadPago[Anexo06]" value="">

                                </span>
                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Eliminar</a>
                            </div>
                        </div>
                        <br>
                    <?php endif; ?>
                    <?php if(!empty($resultados)): ?>
                        <?php if($resultados['Anexo06']): ?>
                            <a href="<?= \Yii::$app->request->BaseUrl ?>/conformidad/<?php echo $resultados['Anexo06'] ?>" target="_blank"><i class="fa fa-download"></i>&nbsp;Descargar adjunto</a>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Orden Compra<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <?php if($bloqueado == 0): ?>
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="input-group">
                                <div class="form-control uneditable-input" data-trigger="fileinput">
                                    <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                    <span class="fileinput-filename">
                                    <?php if(!empty($resultados)): ?>
                                        <?php if($resultados['OrdenCompra']): ?>
                                            <?php echo $resultados['OrdenCompra'] ?>
                                        <?php else: ?>
                                            Seleccione archivo...
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    </span>
                                </div>
                                <span class="input-group-addon btn btn-default btn-file">
                                    <span class="fileinput-new">Seleccionar</span>
                                    <span class="fileinput-exists">Cambiar</span>
                                    <input type="file" onchange="return Imagen(this);" id="file-cv-administrativo" name="DetalleConformidadPago[OrdenCompraArchivo]" required>
                                    <input class="form-control" type="hidden" name="DetalleConformidadPago[OrdenCompra]" value="">
                                    <input class="form-control" type="hidden" name="DetalleConformidadPago[TipoGasto]" value="<?php echo $tipo ?>">
                                </span>
                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Eliminar</a>
                            </div>
                        </div>
                        <br>
                    <?php endif; ?>
                    <?php if(!empty($resultados)): ?>
                        <?php if($resultados['OrdenCompra']): ?>
                            <a href="<?= \Yii::$app->request->BaseUrl ?>/conformidad/<?php echo $resultados['OrdenCompra'] ?>" target="_blank"><i class="fa fa-download"></i>&nbsp;Descargar adjunto</a>
                        <?php endif; ?>
                    <?php endif; ?>

                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Conformidad segun Especificaciones<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <?php if($bloqueado == 0): ?>
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="input-group">
                                <div class="form-control uneditable-input" data-trigger="fileinput">
                                    <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                    <span class="fileinput-filename">
                                    <?php if(!empty($resultados)): ?>
                                        <?php if($resultados['ConformidadEspecificaciones']): ?>
                                            <?php echo $resultados['ConformidadEspecificaciones'] ?>
                                        <?php else: ?>
                                            Seleccione archivo...
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    </span>
                                </div>
                                <span class="input-group-addon btn btn-default btn-file">
                                    <span class="fileinput-new">Seleccionar</span>
                                    <span class="fileinput-exists">Cambiar</span>
                                    <input type="file" onchange="return Imagen(this);" id="file-cv-administrativo" name="DetalleConformidadPago[ConformidadEspecificacionesArchivo]" required>
                                    <input class="form-control" type="hidden" name="DetalleConformidadPago[ConformidadEspecificaciones]" value="">
                                </span>
                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Eliminar</a>
                            </div>
                        </div>
                        <br>
                    <?php endif; ?>
                    <?php if(!empty($resultados)): ?>
                        <?php if($resultados['ConformidadEspecificaciones']): ?>
                            <a href="<?= \Yii::$app->request->BaseUrl ?>/conformidad/<?php echo $resultados['ConformidadEspecificaciones'] ?>" target="_blank"><i class="fa fa-download"></i>&nbsp;Descargar adjunto</a>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Factura<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <?php if($bloqueado == 0): ?>
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="input-group">
                                <div class="form-control uneditable-input" data-trigger="fileinput">
                                    <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                    <span class="fileinput-filename">
                                    <?php if(!empty($resultados)): ?>
                                        <?php if($resultados['Factura']): ?>
                                            <?php echo $resultados['Factura'] ?>
                                        <?php else: ?>
                                            Seleccione archivo...
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    </span>
                                </div>
                                <span class="input-group-addon btn btn-default btn-file">
                                    <span class="fileinput-new">Seleccionar</span>
                                    <span class="fileinput-exists">Cambiar</span>
                                    <input type="file" onchange="return Imagen(this);" id="file-cv-administrativo" name="DetalleConformidadPago[FacturaArchivo]" required>
                                    <input class="form-control" type="hidden" name="DetalleConformidadPago[Factura]" value="">
                                </span>
                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Eliminar</a>
                            </div>
                        </div>
                        <br>
                    <?php endif; ?>
                    <?php if(!empty($resultados)): ?>
                        <?php if($resultados['Factura']): ?>
                            <a href="<?= \Yii::$app->request->BaseUrl ?>/conformidad/<?php echo $resultados['Factura'] ?>" target="_blank"><i class="fa fa-download"></i>&nbsp;Descargar adjunto</a>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Otros Documentos<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <?php if($bloqueado == 0): ?>
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="input-group">
                                <div class="form-control uneditable-input" data-trigger="fileinput">
                                    <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                    <span class="fileinput-filename">
                                    <?php if(!empty($resultados)): ?>
                                        <?php if($resultados['Otros']): ?>
                                            <?php echo $resultados['Otros'] ?>
                                        <?php else: ?>
                                            Seleccione archivo...
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    </span>
                                </div>
                                <span class="input-group-addon btn btn-default btn-file">
                                    <span class="fileinput-new">Seleccionar</span>
                                    <span class="fileinput-exists">Cambiar</span>
                                    <input type="file" onchange="return Imagen(this);" id="file-cv-administrativo" name="DetalleConformidadPago[OtrosArchivo]" required>
                                    <input class="form-control" type="hidden" name="DetalleConformidadPago[Otros]" value="">
                                </span>
                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Eliminar</a>
                            </div>
                        </div>
                        <br>
                    <?php endif; ?>
                    <?php if(!empty($resultados)): ?>
                        <?php if($resultados['Otros']): ?>
                            <a href="<?= \Yii::$app->request->BaseUrl ?>/conformidad/<?php echo $resultados['Otros'] ?>" target="_blank"><i class="fa fa-download"></i>&nbsp;Descargar adjunto</a>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>

        </div>
    </div>

    
        
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <?php if($bloqueado == 0): ?>
            <button id="btn-save-conformidad-servicios" type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
        <?php endif; ?>
    </div>
<?php ActiveForm::end(); ?>

<script>
    var $form = $('#frmDocumentosConformidad');
    var formParsleyConformidadPago = $form.parsley(defaultParsleyForm());

</script>