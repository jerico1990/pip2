
<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay_progress.min.js"></script>
<script src=" //cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>


<div id="page-content" style="min-height: 1342px;">
    
    <div id="wrap">
        <div id="page-heading">
            <h1>Conformidades Anexo 06 (Aprobados) </h1>
        </div>
        
           
        <div class="container">
            <!-- <div class="form-group">
                <button class="btn btn-crear-requerimiento" >Generar requerimiento</button>
            </div> -->
            <div class="table-responsive">
                <table id="proyectos" class="display table table-bordered table-striped table-condensed table-hover" cellspacing="0" >
                    <thead>
                        <tr>
                            <th>Código Proyecto</th>
                            <th>OS/OC</th>
                            <th>Proveedor</th>
                            <th width="100">Concepto</th>
                            <th>Monto</th>
                            <th>Mes</th>
                            <th>Siaf</th>
                            <th>Situación</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<?php 
if(\Yii::$app->user->identity->rol == 2 || \Yii::$app->user->identity->rol == 11){ 
    $requerimiento = Yii::$app->getUrlManager()->createUrl('requerimiento/actualizar-requerimiento');
}

if(\Yii::$app->user->identity->rol == 13 || \Yii::$app->user->identity->rol == 14){ 
    $requerimiento = Yii::$app->getUrlManager()->createUrl('requerimiento/actualizar-requerimiento-contabilidad');
} 
$compro=Yii::$app->getUrlManager()->createUrl('compromiso-presupuestal/generar-compromiso-anual');
?>

<div class="modal fade" id="modal-ctrl-requerimiento" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Requerimiento</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-ctrl-generar-pago" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Conformidad</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-ctrl-desaprobar" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Desaprobar</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<script>
    var tblresultjs;
    var configDTjs={
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[20, 50, 100, -1], [20, 50, 100, "Todos"]],
            "language": {
                "sProcessing":    "Procesando...",
                "sLengthMenu":    "Mostrar _MENU_ registros",
                "sZeroRecords":   "No se encontraron resultados",
                "sEmptyTable":    "Ningún dato disponible en esta lista",
                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":   "",
                "sSearch":        "Buscar:",
                "sUrl":           "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":    "Último",
                    "sNext":    "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
    };
    $(document).ready(function() {
        $.ajax({
            url:'<?= \Yii::$app->request->BaseUrl ?>/conformidad-pago/listado-conformidad-ok',
            async:false,
            data:{},
            beforeSend:function()
            {
                $('.panel-default').LoadingOverlay("show");
            },
            success:function(result)
            {
                    //$tblresultjs.destroy();//
                    $("#proyectos tbody").html(result);
                    $('#proyectos').DataTable(configDTjs);
                    //tblresultjs=$('#example').DataTable(configDTjs);
                    $('.panel-default').LoadingOverlay("hide",true);
            },
            error:function(){
                $('.panel-default').LoadingOverlay("hide",true);
                alert('Error al realizar el proceso de busqueda.');
            }
        });
    });


    $('body').on('click', '.btn-generar-documento', function (e) {
        e.preventDefault();
        var CodigoProyecto = $(this).attr('data-codigo-proyecto');
        var tipoGasto = $(this).attr('data-gasto');
        var OrdenID = $(this).attr('data-id');
        var edit = $(this).attr('data-edit');
        var bloq = $(this).attr('data-bloq');
        var IDCor = $(this).attr('data-corr');
        $('#modal-ctrl-generar-pago .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/conformidad-pago/ver?ID='+OrdenID+'&tipo='+tipoGasto+'&CodigoProyecto='+CodigoProyecto+'&edit='+edit+'&bloq='+bloq+'&Code='+IDCor);
        $('#modal-ctrl-generar-pago').modal('show');
    });
</script>