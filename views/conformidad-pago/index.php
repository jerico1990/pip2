
<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay_progress.min.js"></script>
<script src=" //cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>


<div id="page-content" style="min-height: 1342px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Conformidad de pago</h1>
        </div>
        
        <div class="container">
            <div class="form-group">
                <button data-codigo-proyecto="<?= $CodigoProyecto ?>" class="btn btn-primary btn-crear-conformidad-servicios">Generar Conformidad de pago</button>
                <!--<a class="btn " href="documentos/3_MODELO_CONFORMIDAD_SERVICIOS.docx">Plantilla de conformidad de servicios</a>-->
            </div>
            <div class="table-responsive">
                <table id="proyectos" class="display table table-bordered table-striped table-condensed table-hover" cellspacing="0" >
                    <thead>
                        <tr>
                            <th>Tipo</th>
                            <th>Memorando</th>
                            <th>Siaf</th>
                            <th>Mes</th>
                            <th>Monto</th>
                            <th>Situación</th>
                            <th>Formato</th>
                            <th>Fecha registro</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-ctrl-conformidad-servicios" tabindex="-1" role="dialog">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Conformidad de pago</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-ctrl-generar-pago" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Conformidad</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    
</style>

<script>
    function js_buscar()
    {
       
    }
    var tblresultjs;
        var configDTjs={
            "order": [[ 2, "desc" ]],
                "language": {
                    "sProcessing":    "Procesando...",
                    "sLengthMenu":    "Mostrar _MENU_ registros",
                    "sZeroRecords":   "No se encontraron resultados",
                    "sEmptyTable":    "Ningún dato disponible en esta lista",
                    "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":   "",
                    "sSearch":        "Buscar:",
                    "sUrl":           "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":    "Último",
                        "sNext":    "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
        };
    $(document).ready(function() {
        var CodigoProyecto="<?= $CodigoProyecto ?>";
        $.ajax({
            url:'conformidad-pago/lista-conformidades-servicios?CodigoProyecto='+CodigoProyecto,
            async:false,
            data:{},
            beforeSend:function()
            {
                $('.panel-default').LoadingOverlay("show");
            },
            success:function(result)
            {
                    //$tblresultjs.destroy();//
                    $("#proyectos tbody").html(result);
                    $('#proyectos').DataTable(configDTjs);
                    //tblresultjs=$('#example').DataTable(configDTjs);
                    $('.panel-default').LoadingOverlay("hide",true);
            },
            error:function(){
                $('.panel-default').LoadingOverlay("hide",true);
                alert('Error al realizar el proceso de busqueda.');
            }
        });
        
    } );
    
    $('body').on('click', '.btn-crear-conformidad-servicios', function (e) {
        e.preventDefault();
        var CodigoProyecto = $(this).attr('data-codigo-proyecto');

        AjaxSendForm('<?= \Yii::$app->request->BaseUrl ?>/conformidad-pago/crear', "#modal-ctrl-conformidad-servicios .modal-body-main", {'CodigoProyecto':CodigoProyecto}, false);
        // $('#modal-ctrl-conformidad-servicios .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/conformidad-pago/crear?CodigoProyecto='+CodigoProyecto);
        $('#modal-ctrl-conformidad-servicios').modal('show');
    });


    $('body').on('click', '.btn-generar-documento', function (e) {
        e.preventDefault();
        var CodigoProyecto = $(this).attr('data-codigo-proyecto');
        var tipoGasto = $(this).attr('data-gasto');
        var OrdenID = $(this).attr('data-id');
        var edit = $(this).attr('data-edit');
        var bloq = $(this).attr('data-bloq');
        var IDCor = $(this).attr('data-corr');
        console.log(IDCor);
        $('#modal-ctrl-generar-pago .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/conformidad-pago/ver?ID='+OrdenID+'&tipo='+tipoGasto+'&CodigoProyecto='+CodigoProyecto+'&edit='+edit+'&bloq='+bloq+'&Code='+IDCor);
        $('#modal-ctrl-generar-pago').modal('show');
    });

    $('body').on('click', '.btn-remove', function (e) {
        e.preventDefault();
        var $this = $(this);
        bootboxConfirmEliminar($this,
        'Está seguro de eliminar el registro?',
        function () {
            var id = $this.attr('data-remove-id');
            var toke = '<?=Yii::$app->request->getCsrfToken()?>';
            $.get('/pip2/web/conformidad-pago/delete', { ID: id ,_csrf: toke }, function (data) {
                var jx = JSON.parse(data);
                if (jx.Success == true) {
                    location.reload();
                }
            });
        });
    });

    $(document).on('click','.btn-enviar-aprobar',function(e){
        e.preventDefault();
        var ID=$(this).attr('data-id');
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        bootBoxConfirm('Esta seguro que desea enviar la conformidad',function(){
           $.ajax({
                url: 'conformidad-pago/enviar',
                type: 'GET',
                async: false,
                data: {ID:ID,_csrf: toke},
                success: function (data) {
                    location.reload();
                },
                error: function () {
                    _pageLoadingEnd();
                }
            }); 
        });
    });


    $(document).on('change','#tipo-siaf',function(e){
        e.preventDefault();
        var idOrden = $(this).val().split('-')[0];
        // var ID=$(this).attr('data-id');
        console.log(idOrden);
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: 'conformidad-pago/validar-siaft',
            type: 'POST',
            dataType:'json',
            data: {ID:idOrden,_csrf: toke},
            beforeSend: function(){
                $("#loading").html("");
                $('#devengado').text();
                $("#loading").append('<div class="loading"><i class="fa fa-spinner fa-pulse fa-5x fa-fw"></i></div>');
            },
            success: function (data) {
                console.log(data);
                $("#loading").html("");
                if((data.Saldo == ".0000" || data.Saldo == 0) && data.ID != ''){
                    $('#devengado').text('NO EXISTE SALDO PARA DEVENGAR');
                    $('#btn-save-conformidad-servicios,#correlativo,#tipo,#nroDocumento,#concepto').attr('disabled','disabled');
                    $('#Monto').attr('disabled','disabled');
                    $('#Monto').val(0.00);
                }else{
                    $('#devengado').text('');
                    $('#Monto').val(data.Saldo);
                    $('#Monto').removeAttr('disabled','disabled');
                    $('#btn-save-conformidad-servicios,#correlativo,#tipo,#nroDocumento,#concepto').removeAttr('disabled','disabled');
                }
            },
            error: function () {
                _pageLoadingEnd();
            }
        });

    });

    var i = 0;
    function AjaxSendForm(url, placeholder,data, append) {
        // var data = $(form).serialize();
        append = (append === undefined ? false : true); 
        $.ajax({
            type: 'GET',
            url: url,
            data: data,
            beforeSend: function() {
                // setting a timeout
                $(placeholder).html("");
                $(placeholder).append('<div class="loading"><i class="fa fa-spinner fa-pulse fa-5x fa-fw"></i></div>');
                i++;
            },
            success: function(data) {
                $(placeholder).html("");
                if (append) {
                    $(placeholder).append(data);
                } else {
                    $(placeholder).html(data);
                }
            },
            error: function(xhr) { // if error occured
                alert("Error occured.please try again");
                $(placeholder).append(xhr.statusText + xhr.responseText);
            },
            complete: function() {
                i--;
                if (i <= 0) {
                    $(".loading").remove();
                }
            },
            dataType: 'html'
        });
    }




</script>