<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

    $form = ActiveForm::begin(
    [
        'action'            => 'desaprobar?CodigoProyecto='.$CodigoProyecto,
        'options'           => [
            'id'            => 'frmDesaprobar',
            'enctype'       =>'multipart/form-data',
        ]
    ]
); ?>
    <div class="modal-body">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label">Observación<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="hidden" name="ConformidadPago[ID]" value="<?php echo $ID ?>">
                    <textarea class="form-control" name="ConformidadPago[Observacion]" required></textarea>
                </div>
            </div>
        </div>
    </div>
    
        
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button id="btn-save-conformidad-des" type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>

<script>
    var $form = $('#frmDesaprobar');
    var formParsleyDesaprobar = $form.parsley(defaultParsleyForm());

    // sendFormNoMessage


    $('body').on('click', '#btn-save-conformidad-des', function (e) {
        e.preventDefault();
        
        var $btn = $(this);
        $btn.attr('disabled', 'disabled');
        var isValid = formParsleyDesaprobar.validate();
        if (isValid) {
            sendFormNoMessage($('#frmDesaprobar'), $(this), function (msg) {
                
                location.reload();
                
        
            });
        } else {
        }
    });


</script>