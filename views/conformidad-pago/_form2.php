<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'conformidad-pago/crear?CodigoProyecto='.$CodigoProyecto,
        'options'           => [
            'id'            => 'frmConformidadPago',
            'enctype'       =>'multipart/form-data',
        ]
    ]
); ?>
    <input type="hidden" name="ConformidadPago[CodigoProyecto]" value="<?php echo $CodigoProyecto ?>">
    <div class="modal-body">
        <div class="form-horizontal">
        <!--
            <div class="form-group">
                <label class="col-sm-3 control-label">Memorando<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="ConformidadPago[Memorando]" required>
                </div>
            </div>-->
            <div class="form-group">
                <label class="col-sm-1 control-label">SIAF<span class="f_req">*</span></label>
                <div class="col-sm-11">
            <select class="form-control" name="ConformidadPago[OrdenID]" id="tipo" required>
            <option value>Seleccionar</option>
            <?php foreach($ordenes as $orden){ ?>
                <option value="<?= $orden['ID'] ?>"><?= 'Orden: '.str_pad($orden['Correlativo'], 3, "0", STR_PAD_LEFT).'-'.$orden['Annio'].'/MEMO: '.$orden['Memorando'].'/'.$orden['Sigla'].'/'.$orden['SIAF'] ?></option>
            <?php } ?>
            </select>
                </div>
            </div>
        </div>
    </div>
    
        
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button id="btn-save-conformidad-servicios" type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>

<script>
    var $form = $('#frmConformidadPago');
    var formParsleyConformidadPago = $form.parsley(defaultParsleyForm());

</script>