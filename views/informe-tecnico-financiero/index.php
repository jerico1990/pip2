<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Actividad;
use app\models\EjecucionMetaFisicaFinanciera;
use app\models\EjecucionFinancieraColaboradora;

?>
<div id="page-content" style="min-height: 1342px;">
<div id="wrap">
    <div id="page-heading">
        <h1>Informe técnico financiero</h1>
    </div>
    <div class="container">
        <style>
                #page-heading {
                    display: none;
                }

                .tab-content {
                    background-color: #fff;
                }

                .panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 1px solid #a8a8ab !important;
                }

                .panel-gray .panel-body {
                    border-top: none !important;
                }

                .panel-heading {
                    height: auto !important;
                }
                .tbl-act > tbody > .tr-header > td {
                text-align: center;
                font-size: 12px;
                font-weight: bold;
                padding: 5px !important;
                vertical-align: middle !important;
                /*border: 1px solid #cfcfd0;
                background-color: #f0f0f1;*/
                border: 1px solid #c0c0c0;
                background-color: #e3e3e3;
                }
            </style>
        <?php $form = ActiveForm::begin([
            'action'            => '../informe-tecnico-financiero/index?PasoCriticoID='.$PasoCriticoID,
            'options'           => [
                'enctype'       =>'multipart/form-data',
                'id'            =>'frmITF'
            ]
        ]); ?>
            <!--<div class="panel panel-primary">
                <div class="panel-body">-->
                    <div class="form-horizontal">
                            <div class="tab-container">
                                <ul class="nav nav-tabs">
                                    <li id="li-poaf" class="active"><a href="#tab-1" data-toggle="tab"><i class="fa fa-home"></i>&nbsp;Inf. General</a></li>
                                    <li id="li-poaf" class="detallet"><a href="#tab-2" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Ejecución</a></li>
                                    <li id="li-poaf" class="detallet"><a href="#tab-3" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Hito</a></li>
                                    <li id="li-poaf" class="detallet"><a href="#tab-7" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Salvaguardas</a></li>
                                    <li id="li-poaf" class="detallet"><a href="#tab-4" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Lecciones Aprendidas</a></li>
                                    <li id="li-poaf" class="detallet"><a href="#tab-5" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Conclusión y Recomendación</a></li>
                                    <li id="li-poaf" class="detallet"><a href="#tab-6" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Anexos</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab-1">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Fecha de informe:<span class="f_req">*</span></label>
                                            <div class="col-sm-3">
                                                <input type="date" class="form-control" name="InformeTecnicoFinanciero[FechaInforme]" value="<?= ($model->FechaInforme)?date('Y-m-d',strtotime($model->FechaInforme)):'';?>" required>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Fecha de desembolso del PNIA:<span class="f_req">*</span></label>
                                            <div class="col-sm-3">
                                                <input type="date" class="form-control" name="InformeTecnicoFinanciero[FechaDesembolsoPNIA]" value="<?= ($model->FechaDesembolsoPNIA)?date('Y-m-d',strtotime($model->FechaDesembolsoPNIA)):'';?>" required>
                                            </div>
                                            <label class="col-sm-2 control-label">Monto desembolsado:<span class="f_req">*</span></label>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" name="InformeTecnicoFinanciero[MontoDesembolso]" value="<?= $model->MontoDesembolso ?>" required>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <hr>
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <b>Resumen ejecutivo del proyecto </b>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <textarea  class="form-control" name="InformeTecnicoFinanciero[ResumenEjecutivo]" required><?= $model->ResumenEjecutivo ?></textarea>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <button type="submit" class="btn btn-primary" id="btn-guardar-requerimientos"><i class="fa fa-check"></i>&nbsp;Guardar</button>
                                                <a class="btn btn-default itf" target="_blank" href="imprimir-itf?PasoCriticoID=<?= $PasoCriticoID ?>"><span class="fa fa-cloud-download"></span> Descargar ITF</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane " id="tab-2">
                                        <hr>
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <b>Ejecución física y financiera </b>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <div class="table-responsive">
                                                    <?php foreach($componentes as $componente ){ ?>
                                                    <table class="table borderless tbl-act">
                                                        <tbody >
                                                            <tr class="tr-header">
                                                                <td>#</td>
                                                                <td>Nombre</td>
                                                            </tr>
                                                            <tr>
                                                                <td><?= 'O.'.$componente->Correlativo ?></td>
                                                                <td><b><?= $componente->Nombre ?></b></td>
                                                            </tr>
                                                        <?php //$actividades=Actividad::find()->where('ComponenteID=:ComponenteID',[':ComponenteID'=>$componente->ID])->all();
                                                                $actividades = Actividad::find()
                                                                                        ->select('Actividad.ID,Actividad.ComponenteID,Actividad.Nombre,Actividad.CostoUnitario,Actividad.Correlativo')
                                                                                        ->innerJoin('ActRubroElegible','ActRubroElegible.ActividadID=Actividad.ID')
                                                                                        ->innerJoin('AreSubCategoria','AreSubCategoria.ActRubroElegibleID=ActRubroElegible.ID')
                                                                                        ->innerJoin('CronogramaAreSubCategoria','CronogramaAreSubCategoria.AreSubCategoriaID=AreSubCategoria.ID')
                                                                                        ->where('Actividad.Estado=:Estado and Actividad.ComponenteID=:ComponenteID and CronogramaAreSubCategoria.MetaFisica!=0',[':Estado' => 1, ':ComponenteID' => $componente->ID])
                                                                                        ->andWhere(['between', 'CronogramaAreSubCategoria.Mes', $pasoCritico->MesInicio, $pasoCritico->MesFin])
                                                                                        ->groupBy('Actividad.ID,Actividad.Correlativo,Actividad.ComponenteID,Actividad.Nombre,Actividad.CostoUnitario')
                                                                                        ->orderBy('Actividad.Correlativo asc')
                                                                                        ->all();
                                                        ?>
                                                        <?php foreach($actividades as $actividad){ ?>
                                                            <?php $detalle=EjecucionMetaFisicaFinanciera::find()->where('ActividadID=:ActividadID and InformeTecnicoFinancieroID=:InformeTecnicoFinancieroID',[':ActividadID'=>$actividad->ID,':InformeTecnicoFinancieroID'=>$model->ID])->one(); ?>
                                                            <tr>
                                                                <td><?= 'A.'.$componente->Correlativo.'.'.$actividad->Correlativo ?></td>
                                                                <td><?= $actividad->Nombre ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <input type="hidden" name="InformeTecnicoFinanciero[EjecucionMetaFisicaFinancieraActividadesIDs][]" value="<?= $actividad->ID; ?>">
                                                                    <input type="hidden" name="InformeTecnicoFinanciero[EjecucionMetaFisicaFinancieraIDs][]" <?= ($detalle)?'value='.$detalle->ID.'':''; ?>>
                                                                </td>
                                                                <td><textarea  class="form-control" name="InformeTecnicoFinanciero[EjecucionMetaFisicaFinancieraDescripciones][]" ><?= ($detalle)?$detalle->Descripcion:''; ?></textarea></td>
                                                            </tr>
                                                        <?php } ?>
                                                        </tbody>
                                                    </table>
                                                    <?php } ?>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                        <hr>
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <b>Ejecución financiera de aportes de entidades colaboradoras </b>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <textarea  class="form-control" name="InformeTecnicoFinanciero[EjecucionFinancieraColaboradora]" required><?= $model->EjecucionFinancieraColaboradora ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <div class="table-responsive">
                                                    <table class="table borderless tbl-act" id="detalle">
                                                        <tbody >
                                                            <tr class="tr-header">
                                                                <td>Entidad colaboradora</td>
                                                                <td>Tipo de Aporte  (Monetario/No Monetario)</td>
                                                                <td>Objetivo/Actividad</td>
                                                                <td>Meta Total Programada (S/.)</td>
                                                                <td>Meta Semestral Ejecutada (S/.)</td>
                                                                <td>% avance</td>
                                                                <td>Detalle del aporte</td>
                                                            </tr>
                                                            <?php $totalx=0; ?>
                                                            <?php $totaly=0; ?>
                                                            <?php $i=1; ?>
                                                            <?php foreach($colaboradoras as $colaboradora){ ?>
                                                                <?php $ejecucionColaboradora=EjecucionFinancieraColaboradora::find()->where('ColaboradoraID=:ColaboradoraID and InformeTecnicoFinancieroID=:InformeTecnicoFinancieroID',[':ColaboradoraID'=>$colaboradora->ID,':InformeTecnicoFinancieroID'=>$model->ID])->one(); ?>
                                                                <?php if($ejecucionColaboradora){ ?>
                                                                <input type="hidden" name="InformeTecnicoFinanciero[ColaboradoraIDs][]" value="<?= $colaboradora->ID ?>"><tr>
                                                                <input type="hidden" name="InformeTecnicoFinanciero[EjecucionColaboradoraIDs][]" <?= ($ejecucionColaboradora)?'value='.$ejecucionColaboradora->ID.'':''; ?>><tr>
                                                                    <td><?= $colaboradora->RazonSocial ?></td>
                                                                    <td>
                                                                        <select class="form-control" name="InformeTecnicoFinanciero[ColaboradoraTipos][]">
                                                                            <option value>Seleccionar</option>
                                                                            <option value=1 <?= ($ejecucionColaboradora && $ejecucionColaboradora->TipoAporte==1)?'selected':'';?>>Monetario</option>
                                                                            <option value=2 <?= ($ejecucionColaboradora && $ejecucionColaboradora->TipoAporte==2)?'selected':'';?>>No monetario</option>
                                                                        </select>
                                                                    </td>
                                                                    <td><textarea class="form-control" name="InformeTecnicoFinanciero[ColaboradoraActividadesDetalles][]"><?= $ejecucionColaboradora->ActividadDetalle ?></textarea></td>
                                                                    <td><input type="text" class="form-control contadorx" name="InformeTecnicoFinanciero[ColaboradoraMetasProgramadas][]" value="<?= ($ejecucionColaboradora)?$ejecucionColaboradora->MetaTotalProgramado:'0'; ?>"></td>
                                                                    <td><input type="text" class="form-control contadory" name="InformeTecnicoFinanciero[ColaboradoraMetasEjecutadas][]" value="<?= ($ejecucionColaboradora)?$ejecucionColaboradora->MetaSemestralEjecutada:'0'; ?>"></td>
                                                                    <td><input type="text" class="form-control " name="InformeTecnicoFinanciero[ColaboradoraAvances][]" value="<?= ($ejecucionColaboradora)?$ejecucionColaboradora->Avance:''; ?>"></td>
                                                                    <td><textarea class="form-control" name="InformeTecnicoFinanciero[ColaboradoraDetalles][]"><?= ($ejecucionColaboradora)?$ejecucionColaboradora->DetalleAporte:''; ?></textarea></td>
                                                                </tr>
                                                                <?php if($ejecucionColaboradora){ ?>
                                                                <?php $totalx=$totalx+$ejecucionColaboradora->MetaTotalProgramado; ?>
                                                                <?php $totaly=$totaly+$ejecucionColaboradora->MetaSemestralEjecutada; ?>
                                                                <?php } ?>
                                                                <?php $i++ ?>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                               <td class="text-right"><b>Total: </b></td>
                                                               <td></td>
                                                               <td></td>
                                                               <td id="totalx">S/ <?= round(($totalx/$i),2) ?></td>
                                                               <td id="totaly">S/ <?= round(($totaly/$i),2) ?></td>
                                                               <td></td>
                                                               <td></td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="tab-pane " id="tab-3">
                                        <hr>
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <b>Título del Hito:</b>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <textarea  class="form-control" name="InformeTecnicoFinanciero[TituloHito]" required><?= $model->TituloHito ?></textarea>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <b>Descripción del Hito:</b>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <textarea  class="form-control" name="InformeTecnicoFinanciero[DescripcionHito]" required><?= $model->DescripcionHito ?></textarea>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <b>Periodo del Hito:</b>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Fecha inicio:<span class="f_req">*</span></label>
                                            <div class="col-sm-3">
                                                <input type="date" class="form-control" name="InformeTecnicoFinanciero[PeriodoInicioHito]" value="<?= $model->PeriodoInicioHito ?>" required>
                                            </div>
                                            <label class="col-sm-2 control-label">Monto termino:<span class="f_req">*</span></label>
                                            <div class="col-sm-3">
                                                <input type="date" class="form-control" name="InformeTecnicoFinanciero[PeriodoFinHito]" value="<?= $model->PeriodoFinHito ?>" required>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <b>Se cumplió el hito:</b>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <textarea  class="form-control" name="InformeTecnicoFinanciero[DescripcionCumplio]" required><?= $model->DescripcionCumplio ?></textarea>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <b>Ejecución financiera para alcanzar el hito:</b>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <textarea  class="form-control" name="InformeTecnicoFinanciero[EjecucionFinanciera]" required><?= $model->EjecucionFinanciera ?></textarea>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <b>Observaciones: sugerencias para mejorar el logro de hitos:</b>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <textarea  class="form-control" name="InformeTecnicoFinanciero[Observacion]" required><?= $model->Observacion ?></textarea>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <b>Logros adicionales al hito:</b>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <textarea  class="form-control" name="InformeTecnicoFinanciero[Logros]"  required><?= $model->Logros ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane " id="tab-4">
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <b>Lecciones aprendidas:</b>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <textarea  class="form-control" name="InformeTecnicoFinanciero[LeccionAprendida]" required><?= $model->LeccionAprendida ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane " id="tab-5">
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <b>Conclusiones:</b>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <textarea  class="form-control" name="InformeTecnicoFinanciero[Conclusion]" required><?= $model->Conclusion ?></textarea>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <b>Recomendaciones:</b>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <textarea  class="form-control" name="InformeTecnicoFinanciero[Recomendacion]" required><?= $model->Recomendacion ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane " id="tab-6">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Anexo 1 (Ejecución Física):<span class="f_req">*</span></label>
                                            <div class="col-sm-3">
                                                <!--<span class='fa fa-cloud-download'></span> Descargar</a>-->
                                                <?php if(!$model->Anexo1){ ?>
                                                <a class="btn btn-default btn-adjuntar" data-tipo=1 href="#"><span class="fa fa-cloud-upload"></span> Subir</a>
                                                <a class="btn btn-default" data-tipo=1 href="#"><span class="fa fa-cloud-download"></span> Descargar</a>
                                                <?php }else{ ?>
                                                <a target="_blank" class="btn btn-default" href="../informetecnicofinanciero/<?= $model->Anexo1 ?>" ><span class='fa fa-cloud-download'></span> Descargar documento</a>
                                                <a href='#' class='btn btn-danger btn-remove-documento btn' data-tipo=1 data-id="<?= $model->ID ?>"><i class='fa fa-remove fa-lg'></i></a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Anexo 2 (Ejecución Financiera):<span class="f_req">*</span></label>
                                            <div class="col-sm-3">
                                                <?php if(!$model->Anexo2){ ?>
                                                <a class="btn btn-default btn-adjuntar" data-tipo=2 href="#"><span class="fa fa-cloud-upload"></span> Subir</a>
                                                <a class="btn btn-default" data-tipo=2 href="#"><span class="fa fa-cloud-download"></span> Descargar</a>
                                                <?php }else{ ?>
                                                <a target="_blank" class="btn btn-default" href="../informetecnicofinanciero/<?= $model->Anexo2 ?>" ><span class='fa fa-cloud-download'></span> Descargar documento</a>
                                                <a href='#' class='btn btn-danger btn-remove-documento btn' data-tipo=2 data-id="<?= $model->ID ?>"><i class='fa fa-remove fa-lg'></i></a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Anexo 3 (Consolidado Fis-Fin):<span class="f_req">*</span></label>
                                            <div class="col-sm-3">
                                                <?php if(!$model->Anexo3){ ?>
                                                <a class="btn btn-default btn-adjuntar" data-tipo=3 href="#"><span class="fa fa-cloud-upload"></span> Subir</a>
                                                <a class="btn btn-default" data-tipo=3 href="#"><span class="fa fa-cloud-download"></span> Descargar</a>
                                                <?php }else{ ?>
                                                <a target="_blank" class="btn btn-default" href="../informetecnicofinanciero/<?= $model->Anexo3 ?>" ><span class='fa fa-cloud-download'></span> Descargar documento</a>
                                                <a href='#' class='btn btn-danger btn-remove-documento btn' data-tipo=3 data-id="<?= $model->ID ?>"><i class='fa fa-remove fa-lg'></i></a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Anexo 4 (Consolidado Avance):<span class="f_req">*</span></label>
                                            <div class="col-sm-3">
                                                <?php if(!$model->Anexo4){ ?>
                                                <a class="btn btn-default btn-adjuntar" data-tipo=4 href="#"><span class="fa fa-cloud-upload"></span> Subir</a>
                                                <a class="btn btn-default" data-tipo=4 href="#"><span class="fa fa-cloud-download"></span> Descargar</a>
                                                <?php }else{ ?>
                                                <a target="_blank" class="btn btn-default" href="../informetecnicofinanciero/<?= $model->Anexo4 ?>" ><span class='fa fa-cloud-download'></span> Descargar documento</a>
                                                <a href='#' class='btn btn-danger btn-remove-documento btn' data-tipo=4 data-id="<?= $model->ID ?>"><i class='fa fa-remove fa-lg'></i></a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Anexo 5 (Rend. G. definitivos):<span class="f_req">*</span></label>
                                            <div class="col-sm-3">
                                                <?php if(!$model->Anexo5){ ?>
                                                <a class="btn btn-default btn-adjuntar" data-tipo=5 href="#"><span class="fa fa-cloud-upload"></span> Subir</a>
                                                <a class="btn btn-default" data-tipo=5 href="#"><span class="fa fa-cloud-download"></span> Descargar</a>
                                                <?php }else{ ?>
                                                <a target="_blank" class="btn btn-default" href="../informetecnicofinanciero/<?= $model->Anexo5 ?>" ><span class='fa fa-cloud-download'></span> Descargar documento</a>
                                                <a href='#' class='btn btn-danger btn-remove-documento btn' data-tipo=5 data-id="<?= $model->ID ?>"><i class='fa fa-remove fa-lg'></i></a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Anexo 6 (Rend. gastos pendients):<span class="f_req">*</span></label>
                                            <div class="col-sm-3">
                                                <?php if(!$model->Anexo6){ ?>
                                                <a class="btn btn-default btn-adjuntar" data-tipo=6 href="#"><span class="fa fa-cloud-upload"></span> Subir</a>
                                                <a class="btn btn-default" data-tipo=6 href="#"><span class="fa fa-cloud-download"></span> Descargar</a>
                                                <?php }else{ ?>
                                                <a target="_blank" class="btn btn-default" href="../informetecnicofinanciero/<?= $model->Anexo6 ?>" ><span class='fa fa-cloud-download'></span> Descargar documento</a>
                                                <a href='#' class='btn btn-danger btn-remove-documento btn' data-tipo=6 data-id="<?= $model->ID ?>"><i class='fa fa-remove fa-lg'></i></a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Anexo 7 (Estado de cuenta):<span class="f_req">*</span></label>
                                            <div class="col-sm-3">
                                                <?php if(!$model->Anexo7){ ?>
                                                <a class="btn btn-default btn-adjuntar" data-tipo=7 href="#"><span class="fa fa-cloud-upload"></span> Subir</a>
                                                <a class="btn btn-default" data-tipo=7 href="#"><span class="fa fa-cloud-download"></span> Descargar</a>
                                                <?php }else{ ?>
                                                <a target="_blank" class="btn btn-default" href="../informetecnicofinanciero/<?= $model->Anexo7 ?>" ><span class='fa fa-cloud-download'></span> Descargar documento</a>
                                                <a href='#' class='btn btn-danger btn-remove-documento btn' data-tipo=7 data-id="<?= $model->ID ?>"><i class='fa fa-remove fa-lg'></i></a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Anexo 8 (Libro Banos):<span class="f_req">*</span></label>
                                            <div class="col-sm-3">
                                                <?php if(!$model->Anexo8){ ?>
                                                <a class="btn btn-default btn-adjuntar" data-tipo=8 href="#"><span class="fa fa-cloud-upload"></span> Subir</a>
                                                <a class="btn btn-default" data-tipo=8 href="#"><span class="fa fa-cloud-download"></span> Descargar</a>
                                                <?php }else{ ?>
                                                <a target="_blank" class="btn btn-default" href="../informetecnicofinanciero/<?= $model->Anexo8 ?>" ><span class='fa fa-cloud-download'></span> Descargar documento</a>
                                                <a href='#' class='btn btn-danger btn-remove-documento btn' data-tipo=8 data-id="<?= $model->ID ?>"><i class='fa fa-remove fa-lg'></i></a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <!--
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Anexo 9:<span class="f_req">*</span></label>
                                            <div class="col-sm-3">
                                                <?php if(!$model->Anexo9){ ?>
                                                <a class="btn btn-default btn-adjuntar" data-tipo=9 href="#"><span class="fa fa-cloud-upload"></span> Subir</a>
                                                <a class="btn btn-default" data-tipo=9 href="#"><span class="fa fa-cloud-download"></span> Descargar</a>
                                                <?php }else{ ?>
                                                <a target="_blank" class="btn btn-default" href="../informetecnicofinanciero/<?= $model->Anexo9 ?>" ><span class='fa fa-cloud-download'></span> Descargar documento</a>
                                                <a href='#' class='btn btn-danger btn-remove-documento btn' data-tipo=9 data-id="<?= $model->ID ?>"><i class='fa fa-remove fa-lg'></i></a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Anexo 10:<span class="f_req">*</span></label>
                                            <div class="col-sm-3">
                                                <?php if(!$model->Anexo10){ ?>
                                                <a class="btn btn-default btn-adjuntar" data-tipo=10 href="#"><span class="fa fa-cloud-upload"></span> Subir</a>
                                                <a class="btn btn-default" data-tipo=10 href="#"><span class="fa fa-cloud-download"></span> Descargar</a>
                                                <?php }else{ ?>
                                                <a target="_blank" class="btn btn-default" href="../informetecnicofinanciero/<?= $model->Anexo10 ?>" ><span class='fa fa-cloud-download'></span> Descargar documento</a>
                                                <a href='#' class='btn btn-danger btn-remove-documento btn' data-tipo=10 data-id="<?= $model->ID ?>"><i class='fa fa-remove fa-lg'></i></a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Anexo 11:<span class="f_req">*</span></label>
                                            <div class="col-sm-3">
                                                <?php if(!$model->Anexo11){ ?>
                                                <a class="btn btn-default btn-adjuntar11" data-tipo=11 href="#"><span class="fa fa-cloud-upload"></span> Subir</a>
                                                <a class="btn btn-default" data-tipo=11 href="#"><span class="fa fa-cloud-download"></span> Descargar</a>
                                                <?php }else{ ?>
                                                <a target="_blank" class="btn btn-default" href="../informetecnicofinanciero/<?= $model->Anexo11 ?>" ><span class='fa fa-cloud-download'></span> Descargar documento</a>
                                                <a href='#' class='btn btn-danger btn-remove-documento btn' data-tipo=11 data-id="<?= $model->ID ?>"><i class='fa fa-remove fa-lg'></i></a>
                                                <?php } ?>
                                            </div>
                                        </div>-->
                                    </div>
                                        
                                    <div class="tab-pane " id="tab-7">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <b>Lista de acciones para mitigar efectos ambientales / Considerando los aspectos e impactos identificados previamente.</b>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                Describa las medidas socio ambientales implementadas para el proyecto en este periodo:  manejo de aguas, residuos sólidos, manejo de plagas, manejo de agroquímicos, sustancias peligrosas, manejo de suelos, reforestación y deforestación, etc:
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <div class="table-responsive">
                                                    <table class="table borderless tbl-act" id="detalle2">
                                                        <tbody class="detalleo_">
                                                            <tr class="tr-header">
                                                                <td class="col-md-11" >Descripcion</td>
                                                                <td class="col-md-1"><a class="btn btn-primary agregar"><i class="fa"></i> Agregar</a></td>
                                                            </tr>
                                                            <?php foreach($medidas as $medida){ ?>
                                                            <tr>
                                                                <td style="padding-left: 0px !important;">
                                                                    <textarea class="form-control col-md-11"  name="InformeTecnicoFinanciero[MedidasSocioAmbientales][]"><?= $medida->Descripcion ?></textarea>
                                                                    <input type="hidden" name="InformeTecnicoFinanciero[MedidasSocioAmbientalesIDs][]" value="<?= $medida->ID ?>">
                                                                </td>
                                                                <td><a href="javascript:;" class="eliminar"><i class="fa fa-times" aria-hidden="true"></i></a></td>
                                                            </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <hr>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                Producción de residuos sólidos expresada en Kilogramos:
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <b>Residuos NO Peligrosos:</b>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php $bandera=$pasoCritico->MesInicio; ?>
                                            <?php for($i=1;$i<=($pasoCritico->MesFin-$pasoCritico->MesInicio+1);$i++){ ?>
                                            <div class="col-sm-2">
                                                <input type="text" name="InformeTecnicoFinanciero[NPMes<?= $i ?>]" class="form-control" placeholder="Mes <?= $bandera ?>" value="<?= $model->{'NPMes'.$i} ?>">
                                                <input type="hidden" name="InformeTecnicoFinanciero[NPMesID<?= $i ?>]" class="form-control" value="<?= $bandera ?>">
                                            </div>
                                            <?php $bandera++; ?>
                                            <?php } ?>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <b>Residuos Peligrosos:</b>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php $bandera=$pasoCritico->MesInicio; ?>
                                            <?php for($i=1;$i<=($pasoCritico->MesFin-$pasoCritico->MesInicio+1);$i++){ ?>
                                            <div class="col-sm-2">
                                                <input type="text" name="InformeTecnicoFinanciero[PMes<?= $i ?>]" class="form-control" placeholder="Mes <?= $bandera ?>" value="<?= $model->{'PMes'.$i} ?>">
                                                <input type="hidden" name="InformeTecnicoFinanciero[PMesID<?= $i ?>]" class="form-control" value="<?= $bandera ?>">
                                            </div>
                                            <?php $bandera++; ?>
                                            <?php } ?>
                                        </div>
                                        <hr>  
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                Describa el manejo de residuos sólidos comunes y de residuos tóxicos peligrosos:
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <textarea  class="form-control" name="InformeTecnicoFinanciero[DescribirResiduosPeligrosos]" ><?= $model->DescribirResiduosPeligrosos ?></textarea>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                Consumos de combustibles / Galones (Gln):
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php $bandera=$pasoCritico->MesInicio; ?>
                                            <?php for($i=1;$i<=($pasoCritico->MesFin-$pasoCritico->MesInicio+1);$i++){ ?>
                                            <div class="col-sm-2">
                                                <input type="text" name="InformeTecnicoFinanciero[CCMes<?= $i ?>]" class="form-control" placeholder="Mes <?= $bandera ?>" value="<?= $model->{'CCMes'.$i} ?>">
                                                <input type="hidden" name="InformeTecnicoFinanciero[CCMesID<?= $i ?>]" class="form-control" value="<?= $bandera ?>">
                                            </div>
                                            <?php $bandera++; ?>
                                            <?php } ?>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                Consumo de agua / Metros cubicos (m3):
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <textarea  class="form-control" name="InformeTecnicoFinanciero[DescribirConsumoAgua]" ><?= $model->DescribirConsumoAgua ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php $bandera=$pasoCritico->MesInicio; ?>
                                            <?php for($i=1;$i<=($pasoCritico->MesFin-$pasoCritico->MesInicio+1);$i++){ ?>
                                            <div class="col-sm-2">
                                                <input type="text" name="InformeTecnicoFinanciero[CAMes<?= $i ?>]" class="form-control" placeholder="Mes <?= $bandera ?>" value="<?= $model->{'CAMes'.$i} ?>">
                                                <input type="hidden" name="InformeTecnicoFinanciero[CAMesID<?= $i ?>]" class="form-control" value="<?= $bandera ?>">
                                            </div>
                                            <?php $bandera++; ?>
                                            <?php } ?>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                Seguridad y Salud Ocupacional:
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <textarea  class="form-control" name="InformeTecnicoFinanciero[SeguridadOcupacional]" ><?= $model->SeguridadOcupacional ?></textarea>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                Liste las comunidades, distritos, asociaciones, agrupaciones, distritos involucrados en el proyecto para este périodo:
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <textarea  class="form-control" name="InformeTecnicoFinanciero[ListaInvolucrados]" ><?= $model->ListaInvolucrados ?></textarea>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                Liste los talleres y capacitaciones o actividades participativas sociales en este periodo:
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <div class="table-responsive">
                                                    <table class="table borderless tbl-act" id="participativa">
                                                        <tbody class="participativa_">
                                                            <tr class="tr-header">
                                                                <td class="col-md-1" >Fecha</td>
                                                                <td class="col-md-3" >Tema</td>
                                                                <td class="col-md-1" >Numero de personas</td>
                                                                <td class="col-md-2" >Comunidad, localidad</td>
                                                                <td class="col-md-2" >Lugar</td>
                                                                <td class="col-md-1" >Tiempo del evento en MIN</td>
                                                                <td class="col-md-1" >(**)Horas Hombre (HH)</td>
                                                                <td class="col-md-1"><a class="btn btn-primary agregar_participativa"><i class="fa"></i> Agregar</a></td>
                                                            </tr>
                                                            <?php foreach($participativas as $participativa){ ?>
                                                            <tr>
                                                                <td style="padding-left: 0px !important;">
                                                                    <input type="date" class="form-control col-md-1"  name="InformeTecnicoFinanciero[ParticipativaFechas][]" value="<?= date('Y-m-d',strtotime($participativa->Fecha)) ?>">
                                                                    <input type="hidden" name="InformeTecnicoFinanciero[ParticipativaIDs][]" value="<?= $participativa->ID ?>">
                                                                </td>
                                                                <td style="padding-left: 0px !important;">
                                                                    <textarea class="form-control col-md-1"  name="InformeTecnicoFinanciero[ParticipativaTemas][]"><?= $participativa->Tema ?></textarea>
                                                                </td>
                                                                <td style="padding-left: 0px !important;">
                                                                    <input type="number" class="form-control col-md-1"  name="InformeTecnicoFinanciero[ParticipativaNumerosPersonas][]" value="<?= $participativa->NumeroPersonas ?>">
                                                                </td>
                                                                <td style="padding-left: 0px !important;">
                                                                    <textarea class="form-control col-md-1"  name="InformeTecnicoFinanciero[ParticipativaComunidades][]"><?= $participativa->Comunidad ?></textarea>
                                                                </td>
                                                                <td style="padding-left: 0px !important;">
                                                                    <textarea class="form-control col-md-1"  name="InformeTecnicoFinanciero[ParticipativaLugares][]"><?= $participativa->Lugar ?></textarea>
                                                                </td>
                                                                <td style="padding-left: 0px !important;">
                                                                    <input type="text" class="form-control col-md-1"  name="InformeTecnicoFinanciero[ParticipativaTiempos][]" value="<?= $participativa->Tiempo ?>">
                                                                </td>
                                                                <td style="padding-left: 0px !important;">
                                                                    <input type="text" class="form-control col-md-1"  name="InformeTecnicoFinanciero[ParticipativaHorasHombres][]" value="<?= $participativa->HorasHombre ?>">
                                                                </td>
                                                                <td><a href="javascript:;" class="eliminar"><i class="fa fa-times" aria-hidden="true"></i></a></td>
                                                            </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                Liste los incidentes socio ambientales ocurridos:
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <div class="table-responsive">
                                                    <table class="table borderless tbl-act" id="incidencia">
                                                        <tbody class="incidencia_">
                                                            <tr class="tr-header">
                                                                <td class="col-md-2" >Incidente/Accidente</td>
                                                                <td class="col-md-1" >Fecha</td>
                                                                <td class="col-md-2" >Medidas correctivas adoptadas</td>
                                                                <td class="col-md-2" >Medidas preventivas adoptadas</td>
                                                                <td class="col-md-2" >Numero de personas involucradas</td>
                                                                <td class="col-md-1" >Con daño a la persona (si /no)</td>
                                                                <td class="col-md-1" >Costo en S/. Pérdida</td>
                                                                <td class="col-md-1"><a class="btn btn-primary agregar_incidencia"><i class="fa"></i> Agregar</a></td>
                                                            </tr>
                                                            <?php foreach($incidencias as $incidencia){ ?>
                                                            <tr>
                                                                <td style="padding-left: 0px !important;">
                                                                    <select class="form-control col-md-1"  name="InformeTecnicoFinanciero[IncidenciaTipos][]">
                                                                    <option value>Seleccionar</option>
                                                                    <option value="1" <?= ($incidencia->Tipo=="1")?'selected':'';?>>Incidencia</option>
                                                                    <option value="2" <?= ($incidencia->Tipo=="2")?'selected':'';?>>Accidente</option>
                                                                    </select>
                                                                    <input type="hidden" name="InformeTecnicoFinanciero[IncidenciaIDs][]" value="<?= $incidencia->ID ?>">
                                                                </td>
                                                                <td style="padding-left: 0px !important;">
                                                                    <input type="date" class="form-control col-md-1"  name="InformeTecnicoFinanciero[IncidenciaFechas][]" value="<?= date('Y-m-d',strtotime($incidencia->Fecha)) ?>">
                                                                </td>
                                                                <td style="padding-left: 0px !important;">
                                                                    <textarea class="form-control col-md-1"  name="InformeTecnicoFinanciero[IncidenciaMedidasCorrectivas][]"><?= $incidencia->MedidaCorrectiva ?></textarea>
                                                                </td>
                                                                <td style="padding-left: 0px !important;">
                                                                    <textarea class="form-control col-md-1"  name="InformeTecnicoFinanciero[IncidenciaMedidasPreventivas][]"><?= $incidencia->MedidaPreventiva ?></textarea>
                                                                </td>
                                                                <td style="padding-left: 0px !important;">
                                                                    <input type="number" class="form-control col-md-1"  name="InformeTecnicoFinanciero[IncidenciaNumerosInvolucrados][]" value="<?= $incidencia->NumeroInvolucrados ?>">
                                                                </td>
                                                                <td style="padding-left: 0px !important;">
                                                                    <select class="form-control col-md-1"  name="InformeTecnicoFinanciero[IncidenciaDanosPersonas][]">
                                                                        <option value>Seleccionar</option>
                                                                        <option value="1" <?= ($incidencia->DanoPersona=="1")?'selected':'';?>>Si</option>
                                                                        <option value="2" <?= ($incidencia->DanoPersona=="2")?'selected':'';?>>No</option>
                                                                    </select>
                                                                </td>
                                                                <td style="padding-left: 0px !important;">
                                                                    <input type="text" class="form-control col-md-1"  name="InformeTecnicoFinanciero[IncidenciaCostos][]" value="<?= $incidencia->Costo ?>">
                                                                </td>
                                                                <td>
                                                                    <a href="javascript:;" class="eliminar"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                                </td>
                                                            </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                Equidad Social:
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <div class="table-responsive">
                                                    <table class="table borderless tbl-act" id="equidadinclusion">
                                                        <tbody class="equidadinclusion_">
                                                            <tr class="tr-header">
                                                                <td class="col-md-1" >Equidad/Inclusión</td>
                                                                <td class="col-md-4" >Descripción</td>
                                                                <td class="col-md-2" >Hombres</td>
                                                                <td class="col-md-2" >Mujeres</td>
                                                                <td class="col-md-1"><a class="btn btn-primary agregar_equidadinclusion"><i class="fa"></i> Agregar</a></td>
                                                            </tr>
                                                            <?php foreach($equidadesinclusiones as $equidadinclusion){ ?>
                                                            <tr>
                                                                <td style="padding-left: 0px !important;">
                                                                    <select class="form-control col-md-1"  name="InformeTecnicoFinanciero[EquidadInclusionTipos][]">
                                                                    <option value>Seleccionar</option>
                                                                    <option value="1" <?= ($equidadinclusion->Tipo=="1")?'selected':'';?>>Equidad</option>
                                                                    <option value="2" <?= ($equidadinclusion->Tipo=="2")?'selected':'';?>>Inclusión</option>
                                                                    </select>
                                                                    <input type="hidden" name="InformeTecnicoFinanciero[EquidadInclusionIDs][]" value="<?= $equidadinclusion->ID ?>">
                                                                </td>
                                                                <td style="padding-left: 0px !important;">
                                                                    <textarea class="form-control col-md-1"  name="InformeTecnicoFinanciero[EquidadInclusionDescripciones][]"><?= $equidadinclusion->Descripcion ?></textarea>
                                                                </td>
                                                                <td style="padding-left: 0px !important;">
                                                                    <input type="number" class="form-control col-md-1"  name="InformeTecnicoFinanciero[EquidadInclusionHombres][]" value="<?= $equidadinclusion->Hombres ?>">
                                                                </td>
                                                                <td style="padding-left: 0px !important;">
                                                                    <input type="number" class="form-control col-md-1"  name="InformeTecnicoFinanciero[EquidadInclusionMujeres][]" value="<?= $equidadinclusion->Mujeres ?>">
                                                                </td>
                                                                <td>
                                                                    <a href="javascript:;" class="eliminar"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                                </td>
                                                            </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        
                    </div>
                    <!--
                </div>
            </div>-->
        <?php ActiveForm::end(); ?>
    </div>
</div>

<div class="modal fade" id="modal-adjuntar" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Adjuntar Documento</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>


<script>
    
    var $form = $('#frmITF');
    var formParsleyITF = $form.parsley(defaultParsleyForm());
    $('body').on('click', '.btn-adjuntar', function (e) {
        e.preventDefault();
        var PasoCriticoID=<?= $PasoCriticoID ?>;
        var Tipo=$(this).attr('data-tipo');
        $('#modal-adjuntar .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/informe-tecnico-financiero/adjuntar-documento?PasoCriticoID='+PasoCriticoID+'&Tipo='+Tipo);
        $('#modal-adjuntar').modal('show');
    });
    
    $('body').on('click', '.btn-remove-documento', function (e) {
        e.preventDefault();
        var $this = $(this);
        
        bootboxConfirmEliminar($this,
        '¿Está seguro de eliminar el anexo?',
        function () {
            var id = $this.attr('data-id');
            var Tipo =$this.attr('data-tipo');
            var PasoCriticoID=<?= $PasoCriticoID ?>;
            var toke = '<?=Yii::$app->request->getCsrfToken()?>';
            $.get('<?= \Yii::$app->request->BaseUrl ?>/informe-tecnico-financiero/eliminar-adjunto', { id: id,Tipo:Tipo,PasoCriticoID:PasoCriticoID ,_csrf: toke }, function (data) {
                var jx = JSON.parse(data);
                if (jx.Success == true) {
                    location.reload();
                }
            });
        });
    });
    $('#detalle').on('blur','.contadorx',function(){
        contar(1);
    });
    $('#detalle').on('blur','.contadory',function(){
        contar(2);
    });
    function contar(tipo){
        var total_total = 0.00;
        var i=1;
        
        if (tipo==1) {
            
            $('.contadorx').each(function(x,y){
                TotSuma = $(this).val();
                total_total = parseFloat(getNum(TotSuma)) + parseFloat(total_total) ;
                i++;
            });
            $('#totalx').text( "S/."+ number_format((total_total/i),2) );
        }
        else if (tipo==2) {
            $('.contadory').each(function(x,y){
                TotSuma = $(this).val();
                total_total = parseFloat(getNum(TotSuma)) + parseFloat(total_total) ;
                i++;
            });
            $('#totaly').text( "S/."+ number_format((total_total/i),2) );
        }
        
        
    }
    
    function Suma(x,y,elemento) {
        if (elemento && x>y) {
            $(elemento).val('0.00');
            contar();
            return false;
        }
        
        
        return true;
    }
    
    function getNum(val) {
       if (val == '0.00' || val == '') {
            return 0;
       }
       var val2 = val.replace(',','');
       return val2;
    }
    
    function number_format(amount, decimals) {
        amount += '';
        amount = parseFloat(amount.replace(/[^0-9\.]/g, ''));
        decimals = decimals || 0;
        if (isNaN(amount) || amount === 0) 
            return parseFloat(0).toFixed(decimals);
        amount = '' + amount.toFixed(decimals);
        var amount_parts = amount.split('.'),
            regexp = /(\d+)(\d{3})/;
        while (regexp.test(amount_parts[0]))
            amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');
        return amount_parts.join('.');
    }
    
    $('body').on('click', '.agregar_participativa', function (e) {
	var error = '';
	if (error != '') {
            return false;
	}
	else
        {
	    var option = null;
            var html=
                    '<tr class="participativa_t" >'+
                        '<td style="padding-left: 0px !important;">'+
                            '<input type="date" class="form-control col-md-1"  name="InformeTecnicoFinanciero[ParticipativaFechas][]" >'+
                        '</td>'+
                        '<td style="padding-left: 0px !important;">'+
                            '<textarea class="form-control col-md-1"  name="InformeTecnicoFinanciero[ParticipativaTemas][]"></textarea>'+
                        '</td>'+
                        '<td style="padding-left: 0px !important;">'+
                            '<input type="number" class="form-control col-md-1"  name="InformeTecnicoFinanciero[ParticipativaNumerosPersonas][]" >'+
                        '</td>'+
                        '<td style="padding-left: 0px !important;">'+
                            '<textarea class="form-control col-md-1"  name="InformeTecnicoFinanciero[ParticipativaComunidades][]"></textarea>'+
                        '</td>'+
                        '<td style="padding-left: 0px !important;">'+
                            '<textarea class="form-control col-md-1"  name="InformeTecnicoFinanciero[ParticipativaLugares][]"></textarea>'+
                        '</td>'+
                        '<td style="padding-left: 0px !important;">'+
                            '<input type="text" class="form-control col-md-1"  name="InformeTecnicoFinanciero[ParticipativaTiempos][]">'+
                        '</td>'+
                        '<td style="padding-left: 0px !important;">'+
                            '<input type="text" class="form-control col-md-1"  name="InformeTecnicoFinanciero[ParticipativaHorasHombres][]">'+
                        '</td>'+
                        '<td>'+
                            '<a href="javascript:;" class="eliminar"><i class="fa fa-times" aria-hidden="true"></i></a>'+
                        '</td>'+
                    '</tr>';
                    
            $('.participativa_').append(html);
            $('#css').removeAttr('style');
            
            return true;
        }
    });
    
    $('body').on('click', '.agregar_incidencia', function (e) {
	var error = '';
	if (error != '') {
            return false;
	}
	else
        {
	    var option = null;
            var html=
                    '<tr class="incidencia_t" >'+
                        '<td style="padding-left: 0px !important;">'+
                            '<select class="form-control col-md-1"  name="InformeTecnicoFinanciero[IncidenciaTipos][]">'+
                                '<option value>Seleccionar</option>'+
                                '<option value="1">Incidencia</option>'+
                                '<option value="2">Accidente</option>'+
                            '</select>'+
                        '</td>'+
                        '<td style="padding-left: 0px !important;">'+
                            '<input type="date" class="form-control col-md-1"  name="InformeTecnicoFinanciero[IncidenciaFechas][]">'+
                        '</td>'+
                        '<td style="padding-left: 0px !important;">'+
                            '<textarea class="form-control col-md-1"  name="InformeTecnicoFinanciero[IncidenciaMedidasCorrectivas][]"></textarea>'+
                        '</td>'+
                        '<td style="padding-left: 0px !important;">'+
                            '<textarea class="form-control col-md-1"  name="InformeTecnicoFinanciero[IncidenciaMedidasPreventivas][]"></textarea>'+
                        '</td>'+
                        '<td style="padding-left: 0px !important;">'+
                            '<input type="number" class="form-control col-md-1"  name="InformeTecnicoFinanciero[IncidenciaNumerosInvolucrados][]">'+
                        '</td>'+
                        '<td style="padding-left: 0px !important;">'+
                            '<select class="form-control col-md-1"  name="InformeTecnicoFinanciero[IncidenciaDanosPersonas][]">'+
                                '<option value>Seleccionar</option>'+
                                '<option value="1">Si</option>'+
                                '<option value="2">No</option>'+
                            '</select>'+
                        '</td>'+
                        '<td style="padding-left: 0px !important;">'+
                            '<input type="text" class="form-control col-md-1"  name="InformeTecnicoFinanciero[IncidenciaCostos][]">'+
                        '</td>'+
                        '<td>'+
                            '<a href="javascript:;" class="eliminar"><i class="fa fa-times" aria-hidden="true"></i></a>'+
                        '</td>'+
                    '</tr>';
                    
            $('.incidencia_').append(html);
            $('#css').removeAttr('style');
           
            return true;
        }
    });
    $('body').on('click', '.agregar_equidadinclusion', function (e) {
        
	var error = '';
	if (error != '') {
            return false;
	}
	else
        {
	    var option = null;
            var html=
                    '<tr class="detalleo" >'+
                        '<td style="padding-left: 0px !important;">'+
                            '<select class="form-control col-md-1"  name="InformeTecnicoFinanciero[EquidadInclusionTipos][]">'+
                                '<option value>Seleccionar</option>'+
                                '<option value="1" >Equidad</option>'+
                                '<option value="2" >Inclusión</option>'+
                            '</select>'+
                        '</td>'+
                        '<td style="padding-left: 0px !important;">'+
                            '<textarea class="form-control col-md-3"  name="InformeTecnicoFinanciero[EquidadInclusionDescripciones][]"></textarea>'+
                        '</td>'+
                        '<td style="padding-left: 0px !important;">'+
                            '<input type="number" class="form-control col-md-1"  name="InformeTecnicoFinanciero[EquidadInclusionHombres][]">'+
                        '</td>'+
                        '<td style="padding-left: 0px !important;">'+
                            '<input type="number" class="form-control col-md-1"  name="InformeTecnicoFinanciero[EquidadInclusionMujeres][]">'+
                        '</td>'+
                        '<td>'+
                            '<a href="javascript:;" class="eliminar"><i class="fa fa-times" aria-hidden="true"></i></a>'+
                        '</td>'+
                    '</tr>';
                    
            $('.equidadinclusion_').append(html);
            $('#css').removeAttr('style');
            
            return true;
        }
    });
    
    $('body').on('click', '.agregar', function (e) {
        
	var error = '';
	if (error != '') {
            return false;
	}
	else
        {
	    var option = null;
            var html=
                    '<tr class="detalleo" >'+
                        '<td>'+
                            '<textarea class="form-control col-md-11"  name="InformeTecnicoFinanciero[MedidasSocioAmbientales][]"></textarea>'+
                        '</td>'+
                        '<td>'+
                            '<a href="javascript:;" class="eliminar"><i class="fa fa-times" aria-hidden="true"></i></a>'+
                        '</td>'+
                    '</tr>';
                    
            $('.detalleo_').append(html);
            $('#css').removeAttr('style');
            
            return true;
        }
    });
     
    $("#detalle2").on('click','.eliminar',function(){
        
        var r = confirm("Estas seguro de Eliminar?");
        var mensaje = '';
        if (r == true) {
            id=$(this).children().val();
            if (id) {
                
                $(this).parent().parent().remove();
            }
            else
            {
                $(this).parent().parent().remove();    
            }   
            mensaje = "Se elimino el Registro Correctamente";
            
        } 
    });
</script>