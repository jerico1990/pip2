<div id="page-content" style="min-height: 934px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Presupuesto</h1>
        </div>
        <div class="container">
                    

            <style>
                .panel .panel-body {
                    padding: 10px !important;
                }

                .panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 1px solid #a8a8ab !important;
                }

                .panel-gray .panel-body {
                    border-top: none !important;
                }

                .btn-sm {
                    padding: 5px 10px !important;
                    line-height: 1.5 !important;
                }

                .input-comp-nombre {
                    text-transform: uppercase;
                }

                .tbl-act > tbody > .tr-header > td {
                    text-align: center;
                    font-size: 12px;
                    font-weight: bold;
                    padding: 1px !important;
                    vertical-align: middle !important;
                    /*border: 1px solid #cfcfd0;
                    background-color: #f0f0f1;*/
                    border: 1px solid #c0c0c0;
                    background-color: #e3e3e3;
                }

                .tbl-act .tr-item td {
                    padding: 2px !important;
                }

                /*.tbl-act .tr-item .form-control {
                    height: 28px !important;
                }*/

                input.form-control, select.form-control {
                    height: 28px !important;
                }

                .tbl-act .tr-act .input-nombre {
                    min-width: 320px !important;
                }

                .tbl-act .tr-act .input-unidadmedida {
                    min-width: 100px !important;
                }

                .input-number {
                    min-width: 92px !important;
                }

                .btn-add-re {
                    float: right;
                }

                .tbl-act > tbody > tr > td:nth-child(2) {
                    min-width: 40px !important;
                    text-align: center !important;
                }

                .tr-comp input, .tr-proy input {
                    font-weight: bold;
                }

                .th-proy {
                    min-width: 630px !important;
                }

                /*.div-tbl-comp {
                    height: 350px;
                }*/

                /*.panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 2px solid #a8a8ab !important;
                }*/

                /*.tr-re, .tr-are {
                    background-color: #e5fbe5;
                }

                .tr-scat, .tr-arescat {
                    background-color: #fcf3d0;
                }*/
            </style>


            <div class="alert alert-info" style="padding: 10px;">
                <dl class="dl-horizontal" style="margin-bottom: 0;">
                    <dt>Nombre del Proyecto:</dt>
                    <dd>ADAPTACIÓN DE LA PLANTA DE LLANTÉN (Plantago major - Plantagináceas) A UN SISTEMA DE CULTIVO AGROINDUSTRIAL PARA LA PRODUCCIÓN DE CÁPSULAS Y HOJAS SECAS EN LA REGION DE QUILLABAMBA – CUZCO, PARA EL MERCADO DE MEDICINAS NATURALES EN EUROPA.  </dd>
                    <dt>Entidad proponente:</dt>
                    <dd>Asociación de Productores Naturandes de Echarate - Cusco</dd>
                </dl>
            </div>


            <div class="row">
                <div class="col-sm-12">
                    <a href="javascript:void(0);" id="btn-add-comp" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>&nbsp;Agregar Objetivo</a>
                    <!-- <a href="javascript:void(0);" id="btn-ctrl-pre" class="btn btn-success btn-sm"><i class="fa fa-check-square-o"></i>&nbsp;Control de presupuesto</a> -->
                    <!-- <a href="javascript:void(0);" id="btn-ctrl-apo" class="btn btn-success btn-sm"><i class="fa fa-check-square"></i>&nbsp;Control de aportes</a> -->
                    <!-- <a href="javascript:void(0);" id="btn-upl-cartacomp" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#modal-cartacompromiso"><i class="fa fa-upload"></i>&nbsp;Carta de compromiso</a> -->
                    <!-- <a href="javascript:void(0);" id="btn-reset" class="btn btn-danger btn-sm hidden" style="float:right"><i class="fa fa-refresh"></i>&nbsp;Reset Presupuesto</a> -->
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-12">
                    <div id="div-presupuesto">

                    <!-- <table class="table-responsive" border="1" cellspacing="0" cellpadding="0">
                        <tr> 
                            <td rowspan="3"><p>Países Europeos</p></td>
                            <td><p>España</p></td>
                        </tr>
                        <tr> 
                            <td><p>Francia</p></td>
                        </tr>
                        <tr> 
                            <td><p>Reino Unido</p></td>
                        </tr>
                    </table> -->


                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal-edit-are" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body-main">

                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal-rubros-pred" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">RUBROS ELEGIBLES PREDEFINIDOS</h4>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" id="hdf-cid">
                            <button id="btn-add-actividad-honorarios" data-re-id="1" class="btn btn-primary form-control">HONORARIOS</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal-ctrl-pre" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">RESUMEN DE PRESUPUESTO POR PARTIDAS</h4>
                        </div>
                        <div class="modal-body">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal-ctrl-objetivo" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">OBJETIVO</h4>
                        </div>
                        <div class="modal-body">
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal-cartacompromiso" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">CARTA DE COMPROMISO</h4>
                        </div>
                        <div class="modal-body">
                            <a href="/SLFC/Descarga/DescargarAnexo_CartaCompromiso_Colaboradora" class="btn btn-inverse btn-sm"><i class="fa fa-download"></i>&nbsp;Descargar formato</a>
                            <br><br>
                            <table class="table table-bordered table-condensed table-hover">
                                <thead>
                                    <tr>
                                        <th>Entidad</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Eco Export SAC</td>
                                        <td>
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <span class="btn btn-midnightblue-alt btn-file btn-sm">
                                                    <span class="fileinput-new"><i class="fa fa-plus"></i>&nbsp;Seleccionar archivo</span>
                                                    <span class="fileinput-exists"><i class="fa fa-ban"></i>&nbsp;Cambiar</span>
                                                    <input type="file" id="file-283">
                                                </span>
                                                <span class="fileinput-filename"></span>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="javascript:void(0);" class="btn btn-primary btn-file-cartacompromiso btn-sm" role-id="283"><i class="fa fa-cloud-upload fa-lg"></i>&nbsp;Adjuntar archivo</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <span class="btn btn-midnightblue-alt btn-file btn-sm">
                                                    <span class="fileinput-new"><i class="fa fa-plus"></i>&nbsp;Seleccionar archivo</span>
                                                    <span class="fileinput-exists"><i class="fa fa-ban"></i>&nbsp;Cambiar</span>
                                                    <input type="file" id="file-1877">
                                                </span>
                                                <span class="fileinput-filename"></span>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="javascript:void(0);" class="btn btn-primary btn-file-cartacompromiso btn-sm" role-id="1877"><i class="fa fa-cloud-upload fa-lg"></i>&nbsp;Adjuntar archivo</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <span class="btn btn-midnightblue-alt btn-file btn-sm">
                                                    <span class="fileinput-new"><i class="fa fa-plus"></i>&nbsp;Seleccionar archivo</span>
                                                    <span class="fileinput-exists"><i class="fa fa-ban"></i>&nbsp;Cambiar</span>
                                                    <input type="file" id="file-2522">
                                                </span>
                                                <span class="fileinput-filename"></span>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="javascript:void(0);" class="btn btn-primary btn-file-cartacompromiso btn-sm" role-id="2522"><i class="fa fa-cloud-upload fa-lg"></i>&nbsp;Adjuntar archivo</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <span class="btn btn-midnightblue-alt btn-file btn-sm">
                                                    <span class="fileinput-new"><i class="fa fa-plus"></i>&nbsp;Seleccionar archivo</span>
                                                    <span class="fileinput-exists"><i class="fa fa-ban"></i>&nbsp;Cambiar</span>
                                                    <input type="file" id="file-2524">
                                                </span>
                                                <span class="fileinput-filename"></span>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="javascript:void(0);" class="btn btn-primary btn-file-cartacompromiso btn-sm" role-id="2524"><i class="fa fa-cloud-upload fa-lg"></i>&nbsp;Adjuntar archivo</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="alert alert-warning">
                                <strong>IMPORTANTE: Para adjuntar un archivo se debe seguir los siguientes pasos:</strong>
                                <ol>
                                    <li>Descargar el formato en caso exista, utilizando la opción &nbsp;<a href="javascript:void(0);"><i class="fa fa-download"></i>&nbsp;Descargar formato</a></li>
                                    <li>Llenar el formato descargado, imprimirlo, firmarlo y escaneárlo.</li>
                                    <li>Adjuntar el archivo escaneado, utilizando la opción &nbsp;<a href="javascript:void(0);"><i class="fa fa-cloud-upload fa-lg"></i>&nbsp;Adjuntar archivo</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>



<script>
    $(document).ready(function () {
        //$('#modal-ampliacion').modal('show');
    });
    $('body').tooltip({ selector: '[data-toggle="tooltip"]' });
    $('#sidebar ul li a').click(function () {
        _pageLoadingStart();
    });
    document.body.onclick = function (e) {
        if (e.ctrlKey) {
            _pageLoadingEnd();
        }
    }
</script>




<script>
    var _proyecto = new Object();
    var _$rubrosElegibles = $();

    $(document).ready(function () {
        getProyecto().done(function(json) {
            _proyecto = json;
            drawTable();
        });
        // buildSelectRubrosElegibles();
        setWhenReady();
    });

    // Botones
        $('#btn-reset').click(function (e) {
            e.preventDefault();
            bootboxConfirm($(this), 'Está seguro de volver a generar el presupuesto?', 'Volver a generar'
                    , function () {
                        window.location = '/SLFC/Presupuesto/ResetPresupuesto';
                    });
        });

        $('#btn-ctrl-pre').click(function (e) {
            e.preventDefault();
            $('#modal-ctrl-pre .modal-body').load('/SLFC/Presupuesto/GetControlPresupuesto');
            $('#modal-ctrl-pre').modal('show');
        });

        $('#btn-add-comp').click(function (e) {
            e.preventDefault();
            $('#modal-ctrl-objetivo .modal-body').load('<?= \Yii::$app->request->BaseUrl ?>/plan-operativo/objetivoform');
            $('#modal-ctrl-objetivo').modal('show');
        });

        $('.btn-file-cartacompromiso').click(function () {
            var roleID = $(this).attr('role-id');
            var formdata = new FormData();
            var fileInput = document.getElementById('file-' + roleID);
            if (fileInput.files.length) {
                for (i = 0; i < fileInput.files.length; i++) {
                    formdata.append(fileInput.files[i].name, fileInput.files[i]);
                }
                var xhr = new XMLHttpRequest();
                xhr.open('POST', '/SLFC/Presupuesto/UploadCartaCompromisoColaboradora?id=' + roleID);
                xhr.send(formdata);
                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.status == 200) {
                        console.log(xhr);
                        var op = JSON.parse(xhr.responseText);
                        if (op.Success) {
                            bootbox.alert('El archivo se cargó correctamente');
                        } else {
                            console.error(op.Error);
                        }
                    }
                }
            } else {
                bootbox.alert('Debe seleccionar un archivo.');
            }
        });

        $('body').on('change', '.input-comp-nombre', function (e) {
            e.preventDefault();
            var $this = $(this);
            $.post('/SLFC/Presupuesto/UpdateComponenteNombre'
                , { id: $this.attr('data-comp-id'), nombre: $this.val() || '' }
                , function () {
                });
        });

        $('body').on('change', '.tr-act .input-nombre', function (e) {
            e.preventDefault();
            var $this = $(this);
            var actID = $this.closest('tr').attr('data-act-id');
            $.post('/SLFC/Presupuesto/UpdateActividadNombre'
                , { id: actID, nombre: $this.val() || '' }
                , function () {
                });
        });

        $('body').on('change', '.tr-act .input-unidadmedida', function (e) {
            e.preventDefault();
            var $this = $(this);
            var actID = $this.closest('tr').attr('data-act-id');
            $.post('/SLFC/Presupuesto/UpdateActividadUnidadMedida'
                , { id: actID, um: $this.val() || '' }
                , function () {
                });
        });

        $('body').on('change', '.tr-act .input-metafisica', function (e) {
            e.preventDefault();
            _pageLoadingStart();
            var $this = $(this);
            var $tr = $this.closest('tr');
            $.post('/SLFC/Presupuesto/UpdateActividadMetaFisica'
                , { id: $tr.attr('data-act-id'), mf: $this.inputmask('unmaskedvalue') || 0, cid: $tr.attr('data-comp-id'), pid: _proyecto.ID }
                , function (data) {
                    if (data) {
                        updateActividad(data.Actividad, data.Componente, data.Proyecto);
                        _pageLoadingEnd();
                    }
                });
        });

        $('body').on('change', '.tr-arescat .input-costounitario', function (e) {
            e.preventDefault();
            _pageLoadingStart();
            var $this = $(this);
            var $tr = $this.closest('tr');
            $.post('/SLFC/Presupuesto/UpdateAreSubCategoriaCostoUnitario'
                , { id: $tr.attr('data-arescat-id'), cu: $this.inputmask('unmaskedvalue') || 0, aid: $tr.attr('data-act-id'), cid: $tr.attr('data-comp-id'), pid: _proyecto.ID }
                , function (data) {
                    if (data) {
                        updateActividad(data.Actividad, data.Componente, data.Proyecto);
                        _pageLoadingEnd();
                    }
                });
        });

        $('body').on('change', '.tr-arescat .input-metafisica', function (e) {
            e.preventDefault();
            _pageLoadingStart();
            var $this = $(this);
            var $tr = $this.closest('tr');
            $.post('/SLFC/Presupuesto/UpdateAreSubCategoriaMetaFisica'
                , { id: $tr.attr('data-arescat-id'), mf: $this.inputmask('unmaskedvalue') || 0, aid: $tr.attr('data-act-id'), cid: $tr.attr('data-comp-id'), pid: _proyecto.ID }
                , function (data) {
                    if (data) {
                        updateActividad(data.Actividad, data.Componente, data.Proyecto);
                        _pageLoadingEnd();
                    }
                });
        });

        $('body').on('change', '.tr-arescat .input-costounitario', function (e) {
            e.preventDefault();
            _pageLoadingStart();
            var $this = $(this);
            var $tr = $this.closest('tr');
            $.post('/SLFC/Presupuesto/UpdateAreSubCategoriaCostoUnitario'
                , {
                    id: $tr.attr('data-arescat-id'),
                    cu: $this.inputmask('unmaskedvalue') || 0,
                    aid: $tr.attr('data-act-id'),
                    cid: $tr.attr('data-comp-id'),
                    pid: _proyecto.ID
                }
                , function (data) {
                    if (data) {
                        updateActividad(data.Actividad, data.Componente, data.Proyecto);
                        _pageLoadingEnd();
                    }
                });
        });

        $('body').on('change', '.tr-are .input-unidadmedida', function (e) {
            e.preventDefault();
            var $this = $(this);
            var areID = $this.closest('tr').attr('data-are-id');
            $.post('/SLFC/Presupuesto/UpdateActRubroElegibleUnidadMedida'
                , { id: areID, um: $this.val() || '' }
                , function () {
                });
        });

        $('body').on('change', '.tr-arescat .input-unidadmedida', function (e) {
            e.preventDefault();
            var $this = $(this);
            var arescatID = $this.closest('tr').attr('data-arescat-id');
            $.post('/SLFC/Presupuesto/UpdateAreSubCategoriaUnidadMedida'
                , { id: arescatID, um: $this.val() || '', pid: _proyecto.ID }
                , function () {
                });
        });

        $('body').on('change', '[data-apo-are-id].input-apo-monetario', function (e) {
            e.preventDefault();
            _pageLoadingStart();
            var $this = $(this);
            var $tr = $this.closest('tr');
            $.post('/SLFC/Presupuesto/UpdateAporteActRubroElegibleMonetario'
               , {
                   id: $this.attr('data-apo-are-id'),
                   m: $this.inputmask('unmaskedvalue') || 0,
                   aid: $tr.attr('data-act-id'),
                   cid: $tr.attr('data-comp-id'),
                   pid: _proyecto.ID
               }
               , function (data) {
                   if (data) {
                       updateActividad(data.Actividad, data.Componente, data.Proyecto);
                       _pageLoadingEnd();
                   }
               });
        });

        $('body').on('change', '[data-apo-are-id].input-apo-nomonetario', function (e) {
            e.preventDefault();
            _pageLoadingStart();
            var $this = $(this);
            var $tr = $this.closest('tr');
            $.post('/SLFC/Presupuesto/UpdateAporteActRubroElegibleNoMonetario'
               , {
                   id: $this.attr('data-apo-are-id'),
                   nm: $this.inputmask('unmaskedvalue') || 0,
                   aid: $tr.attr('data-act-id'),
                   cid: $tr.attr('data-comp-id'),
                   pid: _proyecto.ID
               }
               , function (data) {
                   if (data) {
                       updateActividad(data.Actividad, data.Componente, data.Proyecto);
                       _pageLoadingEnd();
                   }
               });
        });

        $('body').on('change', '[data-apo-arescat-id].input-apo-monetario', function (e) {
            e.preventDefault();
            _pageLoadingStart();
            var $this = $(this);
            var $tr = $this.closest('tr');
            $.post('/SLFC/Presupuesto/UpdateAporteAreSubCategoriaMonetario'
               , {
                   id: $this.attr('data-apo-arescat-id'),
                   m: $this.inputmask('unmaskedvalue') || 0,
                   aid: $tr.attr('data-act-id'),
                   cid: $tr.attr('data-comp-id'),
                   pid: _proyecto.ID
               }
               , function (data) {
                   if (data) {
                       updateActividad(data.Actividad, data.Componente, data.Proyecto);
                       _pageLoadingEnd();
                   }
               });
        });

        $('body').on('change', '[data-apo-arescat-id].input-apo-nomonetario', function (e) {
            e.preventDefault();
            _pageLoadingStart();
            var $this = $(this);
            var $tr = $this.closest('tr');
            $.post('/SLFC/Presupuesto/UpdateAporteAreSubCategoriaNoMonetario'
               , {
                   id: $this.attr('data-apo-arescat-id'),
                   nm: $this.inputmask('unmaskedvalue') || 0,
                   aid: $tr.attr('data-act-id'),
                   cid: $tr.attr('data-comp-id'),
                   pid: _proyecto.ID
               }
               , function (data) {
                   if (data) {
                       updateActividad(data.Actividad, data.Componente, data.Proyecto);
                       _pageLoadingEnd();
                   }
               });
        });

        $('body').on('change', '.select-are', function (e) {
            e.preventDefault();
            _pageLoadingStart();
            var $this = $(this);
            var $tr = $this.closest('tr');
            $.post('/SLFC/Presupuesto/ChangeRubroElegible'
                , {
                    areid: $tr.attr('data-are-id'),
                    reid: $this.val(),
                    aid: $tr.attr('data-act-id'),
                    cid: $tr.attr('data-comp-id'),
                    pid: _proyecto.ID
                }
                , function (data) {
                    if (data) {
                        updateActividad(data.Actividad, data.Componente, data.Proyecto);
                        _pageLoadingEnd();
                    }
                });
        });

        $('body').on('click', '.btn-edit-are', function (e) {
            e.preventDefault();
            var areID = $(this).closest('tr').attr('data-are-id');
            $('#modal-edit-are .modal-body-main').load('/SLFC/Presupuesto/GetAreSubCategoriaByActRubroElegible', { id: areID });
            $('#modal-edit-are').modal('show');
        });

        $('body').on('click', '.btn-add-are', function (e) {
            e.preventDefault();
            var $tr = $(this).closest('tr');
            $.post('/SLFC/Presupuesto/AddActRubroElegible'
                   , {
                       aid: $tr.attr('data-act-id')
                   }
                   , function (data) {
                       if (data.Success) {
                           _pageLoadingStart();
                           location.reload();
                       }
                   });
        });

        $('body').on('click', '.btn-remove-are', function (e) {
            e.preventDefault();
            var $this = $(this);
            var $tr = $this.closest('tr');
            bootboxConfirmEliminar($this, 'Está seguro de eliminar el rubro? Se eliminarán todos los datos asociados a él.'
                , function () {
                    $.post('/SLFC/Presupuesto/RemoveActRubroElegible'
                   , {
                       id: $tr.attr('data-are-id')
                   }
                   , function (data) {
                       if (data.Success) {
                           _pageLoadingStart();
                           location.reload();
                       }
                   });
                });
        });

        $('body').on('click', '.btn-add-act', function (e) {
            e.preventDefault();
            var cid = $(this).attr('data-comp-id');
            $.post('/SLFC/Presupuesto/AddActividad'
                   , {
                       cid: cid
                   }
                   , function (data) {
                       if (data.Success) {
                           _pageLoadingStart();
                           location.reload();
                       }
                   });
        });

        $('body').on('click', '.btn-remove-act', function (e) {
            e.preventDefault();
            var $this = $(this)
            var $tr = $this.closest('tr');
            bootboxConfirmEliminar($this, 'Está seguro de eliminar la actividad? se eliminarán todos los datos asociados a ella.'
                , function () {
                    $.post('/SLFC/Presupuesto/RemoveActividad'
                   , {
                       id: $tr.attr('data-act-id')
                   }
                   , function (data) {
                       if (data.Success) {
                           _pageLoadingStart();
                           location.reload();
                       }
                   });
                });
        });

        // $('body').on('click', '#btn-add-comp', function (e) {
        //     e.preventDefault();
        //     $.post('/SLFC/Presupuesto/AddComponente'
        //         , function (data) {
        //             if (data.Success) {
        //                 _pageLoadingStart();
        //                 location.reload();
        //             }
        //         });
        // });

        $('body').on('click', '.btn-remove-comp', function (e) {
            e.preventDefault();
            var $this = $(this);
            var roleID = $this.attr('role-id');
            bootboxConfirmEliminar($this, 'Está seguro de eliminar el componente? Se eliminarán todos los datos asociados a él.'
                , function () {
                    $.post('/SLFC/Presupuesto/RemoveComponente'
                   , {
                       id: roleID
                   }
                   , function (data) {
                       if (data.Success) {
                           _pageLoadingStart();
                           location.reload();
                       }
                   });
                });
        });

        $('body').on('click', '#btn-add-actividad-honorarios', function (e) {
            e.preventDefault();
            var $btn = $(this);
            $btn.attr('disabled', 'disabled');
            var cid = $('#modal-rubros-pred #hdf-cid').val();
            if (cid) {
                $.post('/SLFC/Presupuesto/AddActividadForHonorarios?cid=' + cid, function (data) {
                    _pageLoadingStart();
                    location.reload();
                });
            }
        });

        //btn-add-act-pred
        $('body').on('click', '.btn-add-act-pred', function (e) {
            e.preventDefault();
            var cid = $(this).attr('data-comp-id');
            $('#modal-rubros-pred #hdf-cid').val(cid);
            $('#modal-rubros-pred').modal('show');
        });

    function editare_update(areID, aid, cid, pid) {
        $('#modal-edit-are .modal-body-main').load('/SLFC/Presupuesto/GetAreSubCategoriaByActRubroElegible', { id: areID });
        $.post('/SLFC/Presupuesto/UpdateProyecto'
               , {
                   aid: aid,
                   cid: cid,
                   pid: pid
               }
               , function (data) {
                   console.log(data);
                   if (data) {
                       updateActividad(data.Actividad, data.Componente, data.Proyecto);
                   }
               });
    }

    function updateActividad(actividad, componente, proyecto) {
        var $tr_act = $('.tr-act[data-act-id="' + actividad.ID + '"]');
        $tr_act.find('.input-total').val(actividad.Total.toFixed(2));
        $tr_act.find('.input-costounitario').val(actividad.CostoUnitario.toFixed(2));
        $tr_act.find('.input-totalfinanciamientopnia').val(actividad.TotalFinanciamientoPnia.toFixed(2));
        $tr_act.find('.input-totalfinanciamientoalianza').val(actividad.TotalFinanciamientoAlianza.toFixed(2));
        for (var i_act_apo = 0; i_act_apo < actividad.Aportes.length; i_act_apo++) {
            var act_aporte = actividad.Aportes[i_act_apo];
            $tr_act.find('input[data-apo-act-id="' + act_aporte.ID + '"].input-apo-monetario')
                .val(act_aporte.Monetario.toFixed(2));
            $tr_act.find('input[data-apo-act-id="' + act_aporte.ID + '"].input-apo-nomonetario')
                .val(act_aporte.NoMonetario.toFixed(2));
        }
        for (var i_are = 0; i_are < actividad.Tareas.length; i_are++) {
            var actrubroelegible = actividad.Tareas[i_are];
            var $tr_are = $('.tr-are[data-are-id="' + actrubroelegible.ID + '"]');
            $tr_are.find('.input-costounitario').val(actrubroelegible.CostoUnitario.toFixed(2));
            $tr_are.find('.input-total').val(actrubroelegible.Total.toFixed(2));
            $tr_are.find('.input-totalfinanciamientopnia').val(actrubroelegible.TotalFinanciamientoPnia.toFixed(2));
            $tr_are.find('.input-totalfinanciamientoalianza').val(actrubroelegible.TotalFinanciamientoAlianza.toFixed(2));
            for (var i_are_apo = 0; i_are_apo < actrubroelegible.Aportes.length; i_are_apo++) {
                var are_aporte = actrubroelegible.Aportes[i_are_apo];
                $tr_are.find('input[data-apo-are-id="' + are_aporte.ID + '"].input-apo-monetario')
                    .val(are_aporte.Monetario.toFixed(2));
                $tr_are.find('input[data-apo-are-id="' + are_aporte.ID + '"].input-apo-nomonetario')
                    .val(are_aporte.NoMonetario.toFixed(2));
            }
            for (var i_arescat = 0; i_arescat < actrubroelegible.AreSubCategorias.length; i_arescat++) {
                var aresubcategoria = actrubroelegible.AreSubCategorias[i_arescat];
                var $tr_arescat = $('.tr-arescat[data-arescat-id="' + aresubcategoria.ID + '"]');
                $tr_arescat.find('.input-total').val(aresubcategoria.Total.toFixed(2));
                $tr_arescat.find('.input-totalfinanciamientopnia').val(aresubcategoria.TotalFinanciamientoPnia.toFixed(2));
                $tr_arescat.find('.input-totalfinanciamientoalianza').val(aresubcategoria.TotalFinanciamientoAlianza.toFixed(2));
                for (var i_arescat_apo = 0; i_arescat_apo < aresubcategoria.Aportes.length; i_arescat_apo++) {
                    var arescat_aporte = aresubcategoria.Aportes[i_arescat_apo];
                    $tr_arescat.find('input[data-apo-arescat-id="' + arescat_aporte.ID + '"].input-apo-monetario')
                        .val(arescat_aporte.Monetario.toFixed(2));
                    $tr_arescat.find('input[data-apo-arescat-id="' + arescat_aporte.ID + '"].input-apo-nomonetario')
                        .val(arescat_aporte.NoMonetario.toFixed(2));
                }
            }
        }

        var $tr_comp = $('.tr-comp[data-comp-id="' + componente.ID + '"]');
        $tr_comp.find('.input-total').val(componente.Total.toFixed(2));
        $tr_comp.find('.input-totalfinanciamientopnia').val(componente.TotalFinanciamientoPnia.toFixed(2));
        $tr_comp.find('.input-totalfinanciamientoalianza').val(componente.TotalFinanciamientoAlianza.toFixed(2));
        for (var i_comp_apo = 0; i_comp_apo < componente.Aportes.length; i_comp_apo++) {
            var comp_aporte = componente.Aportes[i_comp_apo];
            $tr_comp.find('input[data-apo-comp-id="' + comp_aporte.ID + '"].input-apo-monetario')
                .val(comp_aporte.Monetario.toFixed(2));
            $tr_comp.find('input[data-apo-comp-id="' + comp_aporte.ID + '"].input-apo-nomonetario')
                .val(comp_aporte.NoMonetario.toFixed(2));
        }

        var $tr_proy = $('.tr-proy[data-proy-id="' + proyecto.ID + '"]');
        $tr_proy.find('.input-total').val(proyecto.Total.toFixed(2));
        $tr_proy.find('.input-totalfinanciamientopnia').val(proyecto.TotalFinanciamientoPnia.toFixed(2));
        $tr_proy.find('.input-totalfinanciamientoalianza').val(proyecto.TotalFinanciamientoAlianza.toFixed(2));
        for (var i_proy_apo = 0; i_proy_apo < proyecto.Aportes.length; i_proy_apo++) {
            var proy_aporte = proyecto.Aportes[i_proy_apo];
            $tr_proy.find('input[data-apo-proy-id="' + proy_aporte.ID + '"].input-apo-monetario')
                .val(proy_aporte.Monetario.toFixed(2));
            $tr_proy.find('input[data-apo-proy-id="' + proy_aporte.ID + '"].input-apo-nomonetario')
                .val(proy_aporte.NoMonetario.toFixed(2));
        }

        $('.input-number').each(function () {
            var valor = parseFloat($(this).val());
            //$(this).val(parseFloat(valor.toFixed(2)));
            if (valor < 0) {
                $(this).css('color', 'red');
                $(this).css('border-color', 'red');
            }
        });

        var trproytotalpnia = parseFloat($('.tr-proy .input-totalfinanciamientopnia').first().inputmask('unmaskedvalue'));
        console.log(trproytotalpnia);
        if (trproytotalpnia > 196000) {
            $('.tr-proy .input-totalfinanciamientopnia').first().css('color', 'red');
        } else {
            $('.tr-proy .input-totalfinanciamientopnia').first().css('color', '');
        }
    }

    function setWhenReady() {
        $('.input-number').each(function () {
            var valor = parseFloat($(this).val());
            //$(this).val(parseFloat($(this).val()).toFixed(2));
            $(this).val(parseFloat(valor.toFixed(2)));
            if (valor < 0) {
                $(this).css('color', 'red');
                $(this).css('border-color', 'red');
            }
        });
        $('.input-number').inputmask("decimal", {
            radixPoint: ".",
            groupSeparator: ",",
            groupSize: 3,
            digits: 2,
            integerDigits: 7,
            autoGroup: true,
            allowPlus: false,
            allowMinus: true,
            placeholder: ''
        }).click(function () {
            $(this).select();
        });

        $('[data-toggle="tooltip]').tooltip();

        //$('.table-responsive').css('max-height', $(window).height() * 0.45);
        $('.table-responsive').css('max-height', $(window).height() * 0.60);

        //
        $('.select-are:not(:last) option[value="9"]').remove();
        $('.select-are:not([disabled]) option[value="1"]:not(:selected)').remove();
        var $areh = $('.select-are option[value="1"]:selected');
        if ($areh.length) {
            $('a.btn-add-act-pred').remove();
            var arehactid = $areh.closest('tr').attr('data-act-id');
            $('tr[data-act-id="' + arehactid + '"] .input-nombre').attr('disabled', 'disabled');
            $('tr[data-act-id="' + arehactid + '"].tr-act .input-metafisica').attr('disabled', 'disabled');
            $('tr[data-act-id="' + arehactid + '"] .select-are').attr('disabled', 'disabled');
            $('tr[data-act-id="' + arehactid + '"] a:not(.btn-remove-act)').remove();
        }

        var $divGP = $('.panel-comp:last');
        $divGP.find('a').remove();
        $divGP.find('select, input').attr('disabled', 'disabled');

        var trproytotalpnia = parseFloat($('.tr-proy .input-totalfinanciamientopnia').first().inputmask('unmaskedvalue'));
        console.log(trproytotalpnia);
        if (trproytotalpnia > 196000) {
            $('.tr-proy .input-totalfinanciamientopnia').first().css('color', 'red');
        }
    }

    function drawTable() {
        // console.log(_proyecto);
        var _conta_aporte_aplica = 0;
        for (var i_comp = 0; i_comp < _proyecto.Componentes.length; i_comp++) {
            var componente = _proyecto.Componentes[i_comp];
            var $panel = $('<div class="panel panel-gray panel-comp"></div>');
            var $panelheading = $('<div class="panel-heading">Objetivo ' + (i_comp + 1)
                + '<div class="btn-heading">'
                // + '<a href="javascript:void(0);" data-comp-id="' + componente.ID + '" class="btn-add-act-pred btn btn-sm btn-info">'
                // + '<i class="fa fa-plus"></i> Actividad predefinida</a>'
                + '<a href="javascript:void(0);" class="btn btn-sm btn-midnightblue-alt btn-remove-comp" role-id="' + componente.ID + '">'
                + '<i class="fa fa-remove"></i> Eliminar Objetivo</a></div></div>');
            var $panelbody = $('<div class="panel-body"></div>');
            var $forminputcomp = $('<div class="form-horizontal">'
            + '<div class="form-group"><label class="col-sm-1 control-label">Objetivo:</label>'
            + '<div class="col-sm-6"><textarea data-comp-id="' + componente.ID + '" '
            + 'class="form-control input-comp-nombre"  required="" placeholder="Ingrese Objetivo...">' + componente.Nombre+'</textarea></div>' 

            + '<div class="col-sm-2"><a href="javascript:void(0);" class="btn btn-sm btn-info btn-add-act"'
            + ' data-comp-id="' + componente.ID + '" style="width: 100%">'
            + '<i class="fa fa-plus"></i>&nbsp;Actividad</a></div></div>');

            // $panelbody.append($forminputcomp);

            var $div_table = $('<div class="table-responsive div-tbl-comp"></div>');

            var $table = $('<table class="tbl-act table"><tbody></tbody></table>');

                var $tr_thead_0 = getTR_Header();
                // $tr_thead_0.append(getTD());
                // $tr_thead_0.append(getTD());
                $tr_thead_0.append(getTD().attr('colspan', '2').text('Objetivo'));
                $tr_thead_0.append(getTD().text('Unidad de medida'));
                $tr_thead_0.append(getTD().text('Meta Física'));
                $tr_thead_0.append(getTD().text('Medios de verificación'));
                $tr_thead_0.append(getTD().text('Supuestos'));
                // console.log($tr_thead_0);
                $table.append($tr_thead_0);
                


                /*
                <table class="table-responsive" border="1" cellspacing="0" cellpadding="0">
                    <tr> 
                        <td rowspan="3"><p>Países Europeos</p></td>
                        <td><p>España</p></td>
                    </tr>
                    <tr> 
                        <td><p>Francia</p></td>
                    </tr>
                    <tr> 
                        <td><p>Reino Unido</p></td>
                    </tr>
                </table>
                */


                var totIndi = componente.Indicadores.length;
                var _x = 0;
                // var $tr_act00 = getTR_Item().addClass('tr-act').addClass('info').attr({ 'data-comp-id': componente.ID});
                // $tr_act00.append(getTD().attr('rowspan', totIndi).append(getInputText().addClass('input-objetivo').val(componente.Nombre).attr('placeholder', 'Ingrese Objetivo..')));
            
            for (var i_act0 = 0; i_act0 < componente.Indicadores.length; i_act0++) {
                var indicadorx = componente.Indicadores[i_act0];
                var $tr_act00 = getTR_Item().addClass('tr-act').addClass('warning').attr({ 'data-comp-id': componente.ID,'data-act-id': indicadorx.ID});
                if(_x == 0){
                    $tr_act00.append(getTD());
                    $tr_act00.append(getTD('style="text-align:left !important"').attr('rowspan', totIndi).append(componente.Nombre));
                }else{
                    $tr_act00.append(getTD('style="text-align:left !important"'));
                }
                $tr_act00.append(getTD('style="text-align:center !important"').append(indicadorx.IndicadorUnidadMedida));
                $tr_act00.append(getTD('style="text-align:center !important"').append(indicadorx.IndicadorMeta));
                $tr_act00.append(getTD('style="text-align:center !important"').append(indicadorx.MedioVerificacion));
                $tr_act00.append(getTD('style="text-align:center !important"').append(indicadorx.Supuestos));
                $table.append($tr_act00);
                _x++;
            }
                // $table.append($tr_act00);
            
            

            var $tr_thead_t = getTR_Item();
                $tr_thead_t.append(getTD().html('<div style="padding:10px;"></div>'));
                $table.append($tr_thead_t);


           
            for (var i_act = 0; i_act < componente.Actividades.length; i_act++) {
                _conta_aporte_aplica = 0;
                var actividad = componente.Actividades[i_act];
                var $tr_thead_1 = getTR_Header().attr({ 'data-comp-id': componente.ID, 'data-act-id': actividad.ID });
                // $tr_thead_1.append(getTD().attr('rowspan', '3'));
                // $tr_thead_1.append(getTD().attr('rowspan', '3'));
                $tr_thead_1.append(getTD().attr('colspan', '2').text('Actividad'));
                $tr_thead_1.append(getTD().text('Unidad de medida'));
                $tr_thead_1.append(getTD().text('Meta Física'));
                $tr_thead_1.append(getTD().text('Medios de verificación'));
                $tr_thead_1.append(getTD().text('Supuestos'));
                    // $tr_thead_1.append(getTD().attr('colspan', '2').text('Fuente de financiamiento'));
                    // $tr_thead_1.append(getTD().attr('colspan', actividad.Aportes.length * 2).text('Aporte de la alianza (S/.)'));
                
                var $tr_thead_2 = getTR_Header().attr({ 'data-comp-id': componente.ID, 'data-act-id': actividad.ID });
                var $tr_thead_3 = getTR_Header().attr({ 'data-comp-id': componente.ID, 'data-act-id': actividad.ID });
                    // $tr_thead_2.append(getTD().attr('rowspan', '2').text('PNIA'));
                    // $tr_thead_2.append(getTD().attr('rowspan', '2').text('Alianza'));
                    
                    // for (var i_act_apo = 0; i_act_apo < actividad.Aportes.length; i_act_apo++) {
                    //     var act_aporte = actividad.Aportes[i_act_apo];
                    //     var conta_aporte_aplica = 0;
                    //     if (aplicaCandadoMonetario(act_aporte.EntidadParticipante.Tipo, 1)) {
                    //         $tr_thead_3.append(getTD().text('M.'));
                    //         conta_aporte_aplica++;
                    //         _conta_aporte_aplica++;
                    //     }
                    //     if (aplicaCandadoMonetario(act_aporte.EntidadParticipante.Tipo, 0)) {
                    //         $tr_thead_3.append(getTD().text('N. M.'));
                    //         conta_aporte_aplica++;
                    //         _conta_aporte_aplica++;
                    //     }
                    //     $tr_thead_2.append(getTD().attr('colspan', conta_aporte_aplica)
                    //         .text(act_aporte.EntidadParticipante.Siglas ? act_aporte.EntidadParticipante.Siglas : '-'));
                    // }
                $table.append($tr_thead_1);
                $table.append($tr_thead_2);
                $table.append($tr_thead_3);

                // console.log(actividad.Indicadores);

                var totA = actividad.Indicadores.length;
                var _y = 0;
                // $table.append($tr_act);
                for (var i_acta = 0; i_acta < actividad.Indicadores.length; i_acta++) {
                    var $tr_act = getTR_Item().addClass('tr-act').addClass('info').attr({ 'data-comp-id': componente.ID, 'data-act-id': actividad.ID });
                    console.log(_y);
                    var indi_act = actividad.Indicadores[i_acta];
                    
                    
                    if(_y == 0){
                        $tr_act.append(getTD('style="text-align:center"').html('<a href="javascript:void(0);" data-toggle="tooltip" title="Eliminar" class="btn-remove-act">'
                            + '<i class="fa fa-remove"></i></a>   <b>' + (i_comp + 1) + '.' + (i_act + 1) + '</b>'));
                        $tr_act.append(getTD('style="text-align:left !important"').attr('rowspan', totA).append(actividad.Nombre));
                    }else{
                        $tr_act.append(getTD('style="text-align:left !important"'));
                    }
                    _y++;
                    // $tr_act.append(getTD('style="text-align:center"').html('<a href="javascript:void(0);" data-toggle="tooltip" title="Eliminar" class="btn-remove-act">'
                    //     + '<i class="fa fa-remove"></i></a>'));
                    // console.log(indi_act);
                    $tr_act.append(getTD('style="text-align:center !important"').append(indi_act.IndicadorUnidadMedida));
                    $tr_act.append(getTD('style="text-align:center !important"').append(indi_act.IndicadorMeta));
                    $tr_act.append(getTD('style="text-align:center !important"').append(indi_act.MedioVerificacion));
                    $tr_act.append(getTD('style="text-align:center !important"').append(indi_act.Supuestos));
                    $table.append($tr_act);
                }
                    // $tr_act.append(getTD().append(getInputTextNumber().addClass('input-total').val(actividad.Total).attr('readonly', '')));
                    // $tr_act.append(getTD().append(getInputTextNumber().addClass('input-totalfinanciamientopnia').val(actividad.TotalFinanciamientoPnia).attr('readonly', '')));
                    // $tr_act.append(getTD().append(getInputTextNumber().addClass('input-totalfinanciamientoalianza').val(actividad.TotalFinanciamientoAlianza).attr('readonly', '')));
                    // for (var i_act_apo = 0; i_act_apo < actividad.Aportes.length; i_act_apo++) {
                    //     var act_aporte = actividad.Aportes[i_act_apo];
                    //     if (aplicaCandadoMonetario(act_aporte.EntidadParticipante.Tipo, 1)) {
                    //         $tr_act.append(getTD().append(getInputTextNumber().addClass('input-apo-monetario').val(act_aporte.Monetario)
                    //             .attr({ 'data-apo-act-id': act_aporte.ID, 'readonly': '' })));
                    //     }
                    //     if (aplicaCandadoMonetario(act_aporte.EntidadParticipante.Tipo, 0)) {
                    //         $tr_act.append(getTD().append(getInputTextNumber().addClass('input-apo-nomonetario').val(act_aporte.NoMonetario)
                    //             .attr({ 'data-apo-act-id': act_aporte.ID, 'readonly': '' })));
                    //     }
                    // }
                

                // var $tr_re = getTR_Item().addClass('tr-re success').attr({ 'data-comp-id': componente.ID, 'data-act-id': actividad.ID });
                // $tr_re.append(getTDvacios(2));
                // $tr_re.append(getTD().append('Tareas<a href="javascript:void(0);"'
                //     + ' class="btn-add-are btn-add-re btn btn-success btn-xs">'
                //     + '<i class="fa fa-plus"></i> Agregar Tarea</a>'));
                //     // console.log(_conta_aporte_aplica);
                //     // $tr_re.append(getTDvacios(6 + _conta_aporte_aplica));
                // $table.append($tr_re);

                // for (var i_are = 0; i_are < actividad.Tareas.length; i_are++) {
                //     var actrubroelegible = actividad.Tareas[i_are];
                //     var areEsPredifinida = (actrubroelegible.RubroElegible.TipoRubroElegibleID == 2);
                //     var $tr_are = getTR_Item().addClass('tr-are success').attr({ 'data-comp-id': componente.ID, 'data-act-id': actividad.ID, 'data-are-id': actrubroelegible.ID });
                //     // $tr_are.append(getTD());
                //     $tr_are.append(getTD('style="text-align:right"').html('<b>' + (i_comp + 1) + '.' + (i_act + 1) + '.' + (i_are + 1) + '</b>'));
                //     var $td_are_actions = getTD().html('<a href="javascript:void(0);" class="btn-remove-are" '
                //         + 'data-toggle="tooltip" title="Eliminar"><i class="fa fa-remove"></i></a>');
                //     if (!areEsPredifinida) {
                //         $td_are_actions.append('&nbsp;&nbsp;<a href="javascript:void(0);" class="btn-edit-are" '
                //         + 'data-toggle="tooltip" title="Detalle"><i class="fa fa-search"></i></a>')
                //             .attr({ 'data-comp-id': componente.ID, 'data-act-id': actividad.ID, 'data-are-id': actrubroelegible.ID });
                //     }
                //     $tr_are.append($td_are_actions);
                //         // console.log(actrubroelegible);
                //     $tr_are.append(getTD().append(getInputText().addClass('input-unidadmedida').val(actrubroelegible.Nombre).attr('placeholder', 'Ingrese Tareas')));
                //         // $tr_are.append(getTD().append(_$rubrosElegibles.clone().val(actrubroelegible.RubroElegibleID).addClass('select-are')));

                //         // if (!areEsPredifinida) {
                //         //     $tr_are.append(getTD().append(getInputText().addClass('input-unidadmedida').val(actrubroelegible.UnidadMedida).attr('placeholder', 'Ingrese unidad de medida..')));
                //         //     $tr_are.append(getTD().append(getInputTextNumber().addClass('input-costounitario').val(actrubroelegible.CostoUnitario).attr('readonly', '')));
                //         // } else {
                //         //     $tr_are.append(getTDvacios(2));
                //         // }
                //         // $tr_are.append(getTD());
                //         // $tr_are.append(getTD().append(getInputTextNumber().addClass('input-total').val(actrubroelegible.Total).attr('readonly', '')));
                //         // $tr_are.append(getTD().append(getInputTextNumber().addClass('input-totalfinanciamientopnia').val(actrubroelegible.TotalFinanciamientoPnia).attr('readonly', '')));
                //         // $tr_are.append(getTD().append(getInputTextNumber().addClass('input-totalfinanciamientoalianza').val(actrubroelegible.TotalFinanciamientoAlianza).attr('readonly', '')));
                //         // for (var i_are_apo = 0; i_are_apo < actrubroelegible.Aportes.length; i_are_apo++) {
                //         //     var are_aporte = actrubroelegible.Aportes[i_are_apo];
                //         //     if (aplicaCandadoMonetario(are_aporte.EntidadParticipante.Tipo, 1)) {
                //         //         if (areEsPredifinida) {
                //         //             $tr_are.append(getTD().append(getInputTextNumber().addClass('input-apo-monetario').val(are_aporte.Monetario)
                //         //                 .attr({ 'data-apo-are-id': are_aporte.ID, 'readonly': '' })));
                //         //         } else {
                //         //             $tr_are.append(getTD().append(getInputTextNumber().addClass('input-apo-monetario').val(are_aporte.Monetario)
                //         //                 .attr({ 'data-apo-are-id': are_aporte.ID })));
                //         //         }
                //         //     }
                //         //     if (aplicaCandadoMonetario(are_aporte.EntidadParticipante.Tipo, 0)) {
                //         //         if (areEsPredifinida) {
                //         //             $tr_are.append(getTD().append(getInputTextNumber().addClass('input-apo-nomonetario').val(are_aporte.NoMonetario)
                //         //                 .attr({ 'data-apo-are-id': are_aporte.ID, 'readonly': '' })));
                //         //         } else {
                //         //             $tr_are.append(getTD().append(getInputTextNumber().addClass('input-apo-nomonetario').val(are_aporte.NoMonetario)
                //         //                 .attr({ 'data-apo-are-id': are_aporte.ID })));
                //         //         }
                //         //     }
                //         // }
                //     $table.append($tr_are);

                //         // if (areEsPredifinida) {
                //         //     var $tr_scat = getTR_Item().addClass('tr-scat warning');
                //         //     $tr_scat.append(getTDvacios(2));
                //         //     $tr_scat.append(getTD().text('Sub Categorías '));
                //         //     $tr_scat.append(getTDvacios(6 + _conta_aporte_aplica));
                //         //     $table.append($tr_scat);

                //         //     for (var i_arescat = 0; i_arescat < actrubroelegible.AreSubCategorias.length; i_arescat++) {
                //         //         var aresubcategoria = actrubroelegible.AreSubCategorias[i_arescat];
                //         //         var $tr_arescat = getTR_Item().addClass('tr-arescat warning').attr({
                //         //             'data-comp-id': componente.ID, 'data-act-id': actividad.ID, 'data-are-id': actrubroelegible.ID, 'data-arescat-id': aresubcategoria.ID
                //         //         });
                //         //         $tr_arescat.append(getTDvacios(2));
                //         //         $tr_arescat.append(getTD().append(getInputText().addClass('input-nombre').val(aresubcategoria.Nombre)));
                //         //         $tr_arescat.append(getTD().append(getInputText().addClass('input-unidadmedida').val(aresubcategoria.UnidadMedida).attr('placeholder', 'Ingrese unidad de medida..')));
                //         //         //
                //         //         var fcID = parseInt($('#fc-id').val());
                //         //         //
                //         //         if (aresubcategoria.Nombre == 'Coordinador General' && fcID != 5) {
                //         //             $tr_arescat.append(getTD().append(getInputTextNumber().addClass('input-costounitario').val(aresubcategoria.CostoUnitario).attr('readonly', '')));
                //         //             $tr_arescat.append(getTD().append(getInputTextNumber().addClass('input-metafisica').val(aresubcategoria.MetaFisica).attr('readonly', '')));
                //         //         } else {
                //         //             $tr_arescat.append(getTD().append(getInputTextNumber().addClass('input-costounitario').val(aresubcategoria.CostoUnitario)));
                //         //             $tr_arescat.append(getTD().append(getInputTextNumber().addClass('input-metafisica').val(aresubcategoria.MetaFisica)));
                //         //         }
                                
                //         //         $tr_arescat.append(getTD().append(getInputTextNumber().addClass('input-total').val(aresubcategoria.Total).attr('readonly', '')));
                //         //         $tr_arescat.append(getTD().append(getInputTextNumber().addClass('input-totalfinanciamientopnia').val(aresubcategoria.TotalFinanciamientoPnia).attr('readonly', '')));
                //         //         $tr_arescat.append(getTD().append(getInputTextNumber().addClass('input-totalfinanciamientoalianza').val(aresubcategoria.TotalFinanciamientoAlianza).attr('readonly', '')));
                //         //         // for (var i_arescat_apo = 0; i_arescat_apo < aresubcategoria.Aportes.length; i_arescat_apo++) {
                //         //         //     var arescat_aporte = aresubcategoria.Aportes[i_arescat_apo];
                //         //         //     if (aplicaCandadoMonetario(arescat_aporte.EntidadParticipante.Tipo, 1)) {
                //         //         //         $tr_arescat.append(getTD().append(getInputTextNumber().addClass('input-apo-monetario').val(arescat_aporte.Monetario)
                //         //         //             .attr({ 'data-apo-arescat-id': arescat_aporte.ID })));
                //         //         //     }
                //         //         //     if (aplicaCandadoMonetario(arescat_aporte.EntidadParticipante.Tipo, 0)) {
                //         //         //         $tr_arescat.append(getTD().append(getInputTextNumber().addClass('input-apo-nomonetario').val(arescat_aporte.NoMonetario)
                //         //         //             .attr({ 'data-apo-arescat-id': arescat_aporte.ID })));
                //         //         //     }
                //         //         // }
                //         //         $table.append($tr_arescat);
                //         //     }
                //         // }
                // }

                $div_table.append($table);
                $panelbody.append($div_table);
            }

            // TOTAL Componente
            // var $tr_thead_1 = getTR_Header();
            // $tr_thead_1.append(getTD().attr('rowspan', '3'));
            // $tr_thead_1.append(getTD().attr('rowspan', '3'));
            // $tr_thead_1.append(getTD().attr({ 'rowspan': '3', 'colspan': '4' }));
            // $tr_thead_1.append(getTD().attr('rowspan', '3').text('Total'));
            // $tr_thead_1.append(getTD().attr('colspan', '2').text('Fuente de financiamiento'));
            // $tr_thead_1.append(getTD().attr('colspan', componente.Aportes.length * 2).text('Aporte de la alianza (S/.)'));
            // var $tr_thead_2 = getTR_Header();
            // var $tr_thead_3 = getTR_Header();
            // $tr_thead_2.append(getTD().attr('rowspan', '2').text('PNIA'));
            // $tr_thead_2.append(getTD().attr('rowspan', '2').text('Alianza'));
            // for (var i_comp_apo = 0; i_comp_apo < componente.Aportes.length; i_comp_apo++) {
            //     var comp_aporte = componente.Aportes[i_comp_apo];
            //     var conta_aporte_aplica = 0;
            //     if (aplicaCandadoMonetario(comp_aporte.EntidadParticipante.Tipo, 1)) {
            //         $tr_thead_3.append(getTD().text('M.'));
            //         conta_aporte_aplica++;
            //         _conta_aporte_aplica++;
            //     }
            //     if (aplicaCandadoMonetario(comp_aporte.EntidadParticipante.Tipo, 0)) {
            //         $tr_thead_3.append(getTD().text('N. M.'));
            //         conta_aporte_aplica++;
            //         _conta_aporte_aplica++;
            //     }
            //     $tr_thead_2.append(getTD().attr('colspan', conta_aporte_aplica)
            //         .text(comp_aporte.EntidadParticipante.Siglas ? comp_aporte.EntidadParticipante.Siglas : '-'));
            // }
            // $table.append($tr_thead_1);
            // $table.append($tr_thead_2);
            // $table.append($tr_thead_3);

            // var $tr_comp = getTR_Item().addClass('tr-comp').attr({ 'data-comp-id': componente.ID });
            // $tr_comp.append(getTDvacios(2));
            // $tr_comp.append(getTD().html('<b>COSTO TOTAL DEL COMPONENTE</b>').attr('colspan', '4'));
            // $tr_comp.append(getTD().append(getInputTextNumber().addClass('input-total').val(componente.Total).attr('readonly', '')));
            // $tr_comp.append(getTD().append(getInputTextNumber().addClass('input-totalfinanciamientopnia').val(componente.TotalFinanciamientoPnia).attr('readonly', '')));
            // $tr_comp.append(getTD().append(getInputTextNumber().addClass('input-totalfinanciamientoalianza').val(componente.TotalFinanciamientoAlianza).attr('readonly', '')));
            // for (var i_comp_apo = 0; i_comp_apo < componente.Aportes.length; i_comp_apo++) {
            //     var comp_aporte = componente.Aportes[i_comp_apo];
            //     if (aplicaCandadoMonetario(comp_aporte.EntidadParticipante.Tipo, 1)) {
            //         $tr_comp.append(getTD().append(getInputTextNumber().addClass('input-apo-monetario').val(comp_aporte.Monetario)
            //             .attr({ 'readonly': '', 'data-apo-comp-id': comp_aporte.ID })));
            //     }
            //     if (aplicaCandadoMonetario(comp_aporte.EntidadParticipante.Tipo, 0)) {
            //         $tr_comp.append(getTD().append(getInputTextNumber().addClass('input-apo-nomonetario').val(comp_aporte.NoMonetario)
            //             .attr({ 'readonly': '', 'data-apo-comp-id': comp_aporte.ID })));
            //     }
            // }
            // $table.append($tr_comp);
            //

            $panel.append($panelheading);
            $panel.append($panelbody);

            $('#div-presupuesto').append($panel);
        }

        // TOTAL PROYECTO
        // _conta_aporte_aplica = 0;
        // var $panel_proy = $('<div class="panel panel-gray"></div>');
        // var $panelbody_proy = $('<div class="panel-body"></div>');
        // var $div_table_proy = $('<div class="table-responsive div-tbl-proy"></div>');
        // var $table_proy = $('<table class="tbl-act table"><tbody></tbody></table>');
        // var $tr_thead_1_proy = getTR_Header();
        // $tr_thead_1_proy.append(getTD().attr('rowspan', '3'));
        // $tr_thead_1_proy.append(getTD().attr('rowspan', '3'));
        // $tr_thead_1_proy.append(getTD().attr({ 'rowspan': '3', 'colspan': '4' }).addClass('th-proy'));
        // $tr_thead_1_proy.append(getTD().attr('rowspan', '3').text('Total'));
        // $tr_thead_1_proy.append(getTD().attr('colspan', '2').text('Fuente de financiamiento'));
        // // $tr_thead_1_proy.append(getTD().attr('colspan', _proyecto.Aportes.length * 2).text('Aporte de la alianza (S/.)'));
        // var $tr_thead_2_proy = getTR_Header();
        // var $tr_thead_3_proy = getTR_Header();
        // $tr_thead_2_proy.append(getTD().attr('rowspan', '2').text('PNIA'));
        // $tr_thead_2_proy.append(getTD().attr('rowspan', '2').text('Alianza'));
        // for (var i_proy_apo = 0; i_proy_apo < _proyecto.Aportes.length; i_proy_apo++) {
        //     var proy_aporte = _proyecto.Aportes[i_proy_apo];
        //     var conta_aporte_aplica = 0;
        //     if (aplicaCandadoMonetario(proy_aporte.EntidadParticipante.Tipo, 1)) {
        //         $tr_thead_3_proy.append(getTD().text('M.'));
        //         conta_aporte_aplica++;
        //         _conta_aporte_aplica++;
        //     }
        //     if (aplicaCandadoMonetario(proy_aporte.EntidadParticipante.Tipo, 0)) {
        //         $tr_thead_3_proy.append(getTD().text('N. M.'));
        //         conta_aporte_aplica++;
        //         _conta_aporte_aplica++;
        //     }
        //     $tr_thead_2_proy.append(getTD().attr('colspan', conta_aporte_aplica)
        //         .text(proy_aporte.EntidadParticipante.Siglas ? proy_aporte.EntidadParticipante.Siglas : '-'));
        // }
        // $table_proy.append($tr_thead_1_proy);
        // $table_proy.append($tr_thead_2_proy);
        // $table_proy.append($tr_thead_3_proy);
        // var $tr_proy = getTR_Item().addClass('tr-proy').attr({ 'data-proy-id': _proyecto.ID });
        // $tr_proy.append(getTDvacios(2));
        // $tr_proy.append(getTD().html('<b>COSTO TOTAL DEL PROYECTO</b>').attr('colspan', '4'));
        // $tr_proy.append(getTD().append(getInputTextNumber().addClass('input-total').val(_proyecto.Total).attr('readonly', '')));
        // $tr_proy.append(getTD().append(getInputTextNumber().addClass('input-totalfinanciamientopnia').val(_proyecto.TotalFinanciamientoPnia).attr('readonly', '')));
        // $tr_proy.append(getTD().append(getInputTextNumber().addClass('input-totalfinanciamientoalianza').val(_proyecto.TotalFinanciamientoAlianza).attr('readonly', '')));
        // for (var i_proy_apo = 0; i_proy_apo < _proyecto.Aportes.length; i_proy_apo++) {
        //     var proy_aporte = _proyecto.Aportes[i_proy_apo];
        //     if (aplicaCandadoMonetario(proy_aporte.EntidadParticipante.Tipo, 1)) {
        //         $tr_proy.append(getTD().append(getInputTextNumber().addClass('input-apo-monetario').val(proy_aporte.Monetario)
        //             .attr({ 'readonly': '', 'data-apo-comp-id': proy_aporte.ID, 'data-apo-proy-id': proy_aporte.ID })));
        //     }
        //     if (aplicaCandadoMonetario(proy_aporte.EntidadParticipante.Tipo, 0)) {
        //         $tr_proy.append(getTD().append(getInputTextNumber().addClass('input-apo-nomonetario').val(proy_aporte.NoMonetario)
        //             .attr({ 'readonly': '', 'data-apo-comp-id': proy_aporte.ID, 'data-apo-proy-id': proy_aporte.ID })));
        //     }
        // }
        // $table_proy.append($tr_proy);
        // $div_table_proy.append($table_proy);
        // $panelbody_proy.append($div_table_proy);
        // $panel_proy.append($panelbody_proy);
        // $('#div-presupuesto').append($panel_proy);
    }

    function getProyecto() {
        var url = "<?= \Yii::$app->request->BaseUrl ?>/../de.json";
        return $.getJSON( url );
        // $.ajax({
        //     url:'http://localhost/pip2/de.json',
        //     dataType:'json',
        //     success:function(data){
        //         // que = jQuery.parseJSON(data);
        //         // console.log(data.Componentes.length);
        //         // _proyecto =  data;
        //         que = data;
        //     }   
        // });
        //         console.log(que);
        // return que;
    }

    function getRubrosElegibles() {
        return jQuery.parseJSON($('#jsonRubrosElegibles').val());
    }

    function buildSelectRubrosElegibles() {
        _$rubrosElegibles = $('<select class="form-control"></select>').css('padding-top', '3px');
        $.each(getRubrosElegibles(), function (key, value) {
            _$rubrosElegibles
                .append($("<option></option>")
                           .attr("value", value.ID)
                           .text(value.Nombre));
        });
    }

    function aplicaCandadoMonetario(tipoEntidad, tipoMonetario) {
        // tipoEntidad:     { 1: proponente, 2: colaboradora }
        // tipoMonetario:   { 1: monetario, 0: no monetario }
        var fcID = parseInt($('#fc-id').val());
        switch (fcID) {
            case 1: {   // Servicios de Extensión Agraria
                if (tipoEntidad == 1) {         // Proponente
                    return tipoMonetario == 1 ? true : false;
                } else if (tipoEntidad == 3) {  // Colaboradora
                    return tipoMonetario == 1 ? true : false;
                }
            } break;
            case 3: {   // Investigación Adaptativa
                if (tipoEntidad == 1) {         // Proponente
                    return tipoMonetario == 1 ? true : false;
                } else if (tipoEntidad == 3) {  // Colaboradora
                    return true;
                }
            } break;
            case 4: {   // Desarrollo de Empresas Semilleristas
                if (tipoEntidad == 1) {         // Proponente
                    return tipoMonetario == 1 ? true : false;
                } else if (tipoEntidad == 3) {  // Colaboradora
                    return tipoMonetario == 1 ? true : false;
                }
            } break;
            case 5: {   // Capacitación por Competencias
                if (tipoEntidad == 1) {         // Proponente
                    return tipoMonetario == 1 ? true : false;
                } else if (tipoEntidad == 3) {  // Colaboradora
                    return true;
                }
            } break;
            default:
        }
        return false;
    }

    //
    function getTR_Header() {
        return $('<tr class="tr-header"></tr>');
    }
    function getTR_Item() {
        return $('<tr class="tr-item"></tr>');
    }

    function getTH() {
        return $('<th></th>');
    }

    function getTD(s = '') {
        if(s != '' || s != undefined){
            return $('<td '+s+'></td>');
        }else{
            return $('<td></td>');
        }
    }

    function getTDvacios(q) {
        var html = '';
        for (var i = 0; i < q; i++) {
            html += '<td>&nbsp;</td>';
        }
        return html;
    }

    function getInputText() {
        return $('<input type="text" class="form-control" />');
    }

    function getTextArea(){
        return $('<textarea class="form-control"></textarea')
    }

    function getInputTextNumber() {
        return $('<input type="text" class="form-control input-number" />');
    }
</script>