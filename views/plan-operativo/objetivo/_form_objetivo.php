<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<style>
    #container-poaf .input-number {
        text-align: right;
        min-width: 72px !important;
        height: 22px !important;
        font-size: 13px !important;
    }

    #container-poaf .input-number-sm {
        text-align: right;
        min-width: 52px !important;
        height: 22px !important;
        font-size: 13px !important;
    }

    #container-poaf .input-number-md {
        text-align: right;
        min-width: 82px !important;
        height: 22px !important;
        font-size: 13px !important;
    }

    #container-poaf table-act > tbody > tr > td:nth-child(1) {
        text-align: center;
    }

    #container-poaf .table-act > tbody > tr > td:nth-child(2) {
        min-width: 320px !important;
    }

    #container-poaf .table-act > tbody > tr > td:nth-child(3) {
        min-width: 100px !important;
    }

    #container-poaf .table > tbody > .tr-header > td {
        text-align: center;
        font-size: 12px;
        font-weight: bold;
        padding: 1px !important;
        vertical-align: middle !important;
        /*border: 1px solid #cfcfd0;
        background-color: #f0f0f1;*/
        border: 1px solid #c0c0c0;
        background-color: #e3e3e3;
        min-width: 75px !important;
    }

    #container-poaf h4.panel-title {
        font-size: 16px;
    }

    #container-poaf .tr-comp td, .tr-proy td {
        font-weight: bold;
    }

    #container-poaf .table-condensed td {
        padding: 1px !important;
    }

    #container-poaf td, #table-container input {
        font-size: 13px !important;
    }

    #container-poaf .form-control[readonly] {
        background-color: #f2f3f4;
    }
</style>

    <input data-val="true" data-val-number="The field ID must be a number." data-val-required="El campo ID es obligatorio." id="ID" name="ID" type="hidden" value="0">
    <input data-val="true" data-val-number="The field ProyectoID must be a number." data-val-required="El campo ProyectoID es obligatorio." id="ProyectoID" name="ProyectoID" type="hidden" value="80">    
    <div class="modal-body">



<div class="tab-container">
    <ul class="nav nav-tabs">
        <li id="li-poaf" class="active"><a href="#atab-act" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Objetivo</a></li>
        <li id="li-pc"><a href="#xindicadores-tab" data-toggle="tab"><i class="fa fa-pencil"></i>&nbsp;Indicadores</a></li>
    </ul>
    <div class="tab-content">

        <div class="tab-pane active" id="atab-act">
            <?php $form = ActiveForm::begin(
                [
                    'action'            => empty($model->ID) ? 'plan-operativo/objetivo-crear' : 'plan-operativo/actualiza-objetivo?id='.$model->ID,
                    'options'           => [
                        'autocomplete'  => 'off',
                        'id'            => 'frmObjetivoProyecto'
                    ]
                ]
            ); ?>
                <div id="container-resp"></div>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nombre Objetivo <span class="f_req">*</span></label>
                        <div class="col-sm-10">
                            <textarea class="form-control" cols="20" id="IndicadorTipo" name="Componente[Nombre]" required="" rows="2"><?php echo $model->Nombre ?></textarea>
                            <input type="hidden" class="componenteId" name="<?php echo $model->ID; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Peso <span class="f_req">*</span></label>
                        <div class="col-sm-4">
                            <input  type="text" onKeyPress="return NumCheck(event, this)" class="form-control input-number" data-val="true" data-val-number="The field Peso must be a number." data-val-required="El campo Peso es obligatorio." id="Peso" name="Componente[Peso]" required="" value="<?php echo round($model->Peso, 2) ?>">
                            <!-- <input class="form-control" type="text" id="Peso" name="Componente[Peso]" required="" value="<?php echo $model->Peso ?>"> -->
                            
                        </div>
                    </div>
                
                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button> -->
                        <button id="" type="submit" class="btn btn-primary btn-save-objetivoproyecto"><i class="fa fa-check"></i>&nbsp;<?php echo ($model->ID)?'Actualizar Objetivo':'Continuar'; ?></button>
                    </div>
                </div>

            <?php ActiveForm::end(); ?>
        </div>

        <div class="tab-pane" id="xindicadores-tab">
            <?php $forms = ActiveForm::begin(
                [
                    'action'            => 'plan-operativo/objetivo-indicadores-crear',
                    'options'           => [
                        'autocomplete'  => 'off',
                        'id'            => 'frmObjetivoProyectoIndicador'
                    ]
                ]
            ); ?>
            <div class="alert alert-info indica">
                <i class="fa fa-info"></i>&nbsp;&nbsp;No tiene indicadores registrados.
            </div>
            <div class="form-horizontal hidden" id="indicador-form">
                <p><strong>INDICADORES</strong></p>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Descripcion <span class="f_req">*</span></label>
                    <div class="col-sm-10">
                        <input type="hidden" name="MarcoLogicoComponente[ComponenteID]" value="<?php echo $model->ID; ?>" id="componente-indicador-id">
                        <textarea class="form-control" cols="20" id="IndicadorDescripcion" name="MarcoLogicoComponente[IndicadorDescripcion]" required="" rows="2"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Unidad de Medida <span class="f_req">*</span></label>
                    <div class="col-sm-4">
                        <input type="textarea" class="form-control" data-val="true" name="MarcoLogicoComponente[IndicadorUnidadMedida]" required="" value=""  id="IndicadorUnidadMedida">
                    </div>
                    <label class="col-sm-2 control-label">Meta <span class="f_req">*</span></label>
                    <div class="col-sm-4">
                        <input class="form-control input-number" onKeyPress="return NumCheck(event, this)" data-val="true" data-val-number="The field IndicadorMeta must be a number." data-val-required="El campo IndicadorMeta es obligatorio." id="IndicadorMeta" name="MarcoLogicoComponente[IndicadorMeta]" required="" type="text" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Medio Verificación <span class="f_req">*</span></label>
                    <div class="col-sm-4">
                        <textarea class="form-control" cols="20" id="MedioVerificacion" name="MarcoLogicoComponente[MedioVerificacion]" required="" rows="2"></textarea>
                    </div>
                    <label class="col-sm-2 control-label">Supuestos <span class="f_req">*</span></label>
                    <div class="col-sm-4">
                        <textarea class="form-control" cols="20" id="Supuestos"  name="MarcoLogicoComponente[Supuestos]" required="" rows="2"></textarea>
                    </div>
                </div>
                
                <button id="btn-save-indicador" type="submit" class="btn btn-primary"> Guardar Indicador</button>
                <hr>
                
                    <div id="table_indica_obj">
                    </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>






<script>
    var $form = $('#frmObjetivoProyecto');
    var $formI = $('#frmObjetivoProyectoIndicador');
    var formParsleyObjetivoProyecto = $form.parsley(defaultParsleyForm());
    var formParsleyObjetivoProyecto1 = $formI.parsley(defaultParsleyForm());
    
    $(document).ready(function () {
        // alert('s');
        var como = <?php echo ($model->ID)?$model->ID:0; ?>;//$(".componenteId").val();
        // alert(como);
        if(como != ''){
            $('.indica').css('display','none');
            $('#indicador-form').removeClass('hidden');
        }

        // $('.count-message').each(function () {
        //     var $this = $(this);
        //     var $divformgroup = $this.closest('div.form-group');
        //     var $txt = $divformgroup.find('textarea, input[type="text"]');
        //     var text_max = parseInt($txt.attr('maxlength'));

        //     $txt.keyup(function () {
        //         var text_length = $txt.val().length;
        //         var text_remaining = text_max - text_length;
        //         $this.html(text_remaining + ' caracteres restantes');
        //     });
        // });

        // alert(como);
        cargarIndicadoresObjetivo(como);

    });
    

    function cargarIndicadoresObjetivo(x) {
        $('#table_indica_obj').load('<?= \Yii::$app->request->BaseUrl ?>/plan-operativo/lista-indicadores-objetivo?ComponenteID='+x);
    }
</script>