
<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay_progress.min.js"></script>
<script src=" //cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>


<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<div id="page-content" style="min-height: 1342px;">
    
    <div id="wrap">
        <div id="page-heading">
            <h1>Rendición de encargo</h1>
        </div>
        
           
        <div class="container">
             <div class="form-group">
                <button class="btn btn-crear-rendicion-encargo" data-codigo-proyecto="<?= $CodigoProyecto ?>">Generar rendición encargo</button>
            </div>
            <div id="container-resp"> <?= Yii::$app->session->getFlash('error'); ?></div>
            <div class="table-responsive">
                <table id="cajachica" class="display table table-bordered table-striped table-condensed table-hover" cellspacing="0" >
                    <thead>
                        <tr>
                            <th style="display:none;">ID</th>
                            <th>N° rendición</th>
                            <th>Fecha registro</th>
                            <th>Solicitante encargo</th>
                            <th>Total</th>
                            <th>Situación</th>
                            <th>Formato rendición</th>
                            <th>Acciones</th>
                            <!--
                            <th>Acciones</th>
                            -->
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
          <a class="btn" href="<?= Yii::$app->getUrlManager()->createUrl('requerimiento-caja-chica/excel?CodigoProyecto='.$CodigoProyecto.'') ?>">Descargar Excel</a>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-ctrl-rendicion-encargo" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Rendición encargo</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>
    
<script>

    function js_buscar()
    {
       
    }
    var tblresultjs;
        var configDTjs={
            "order": [[ 1, "desc" ]],
                "language": {
                    "sProcessing":    "Procesando...",
                    "sLengthMenu":    "Mostrar _MENU_ registros",
                    "sZeroRecords":   "No se encontraron resultados",
                    "sEmptyTable":    "Ningún dato disponible en esta lista",
                    "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":   "",
                    "sSearch":        "Buscar:",
                    "sUrl":           "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":    "Último",
                        "sNext":    "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
        };
    $(document).ready(function() {
        var CodigoProyecto="<?= $CodigoProyecto ?>";
        $.ajax({
            url:'rendicion-encargo/lista?CodigoProyecto='+CodigoProyecto,
            async:false,
            data:{},
            beforeSend:function()
            {
                $('.panel-default').LoadingOverlay("show");
            },
            success:function(result)
            {
                    //$tblresultjs.destroy();//
                    $("#cajachica tbody").html(result);
                    $('#cajachica').DataTable(configDTjs);
                    //tblresultjs=$('#example').DataTable(configDTjs);
                    $('.panel-default').LoadingOverlay("hide",true);
            },
            error:function(){
                $('.panel-default').LoadingOverlay("hide",true);
                alert('Error al realizar el proceso de busqueda.');
            }
        });
    } );
    
    $('body').on('click', '.btn-crear-rendicion-encargo', function (e) {
        e.preventDefault();
        var CodigoProyecto = $(this).attr('data-codigo-proyecto');
        $('#modal-ctrl-rendicion-encargo .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/rendicion-encargo/crear?CodigoProyecto='+CodigoProyecto);
        $('#modal-ctrl-rendicion-encargo').modal('show');
    });
    
    $('body').on('click', '.btn-edit-rendicion-encargo', function (e) {
        e.preventDefault();
        var ID = $(this).attr('data-id');
        $('#modal-ctrl-rendicion-encargo .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/rendicion-encargo/actualizar?ID='+ID);
        $('#modal-ctrl-rendicion-encargo').modal('show');
    });


      $('body').on('click', '.btn-remove', function (e) {
        e.preventDefault();
        var $this = $(this);
        bootboxConfirmEliminar($this,
           'Está seguro de eliminar la rendición de encargo?',
            function () {
                var id = $this.attr('data-id');
                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                $.get('<?= \Yii::$app->request->BaseUrl ?>/rendicion-encargo/eliminar', { id: id ,_csrf: toke }, function (data) {
                    var jx = JSON.parse(data);
                    if (jx.Success == true ) {
                        location.reload();
                    }
                });
            });
    });
      
      
    $('body').on('click', '#btn-guardar-rendicion-encargo', function (e) {
        e.preventDefault();
        var $btn = $(this);
        var total=0;
        
        var isValid = formParsleyfrmRendicionEncargo.validate();
        var tbienes=0;
        var tservicios=0;
        bienes=parseFloat(bienes);
        servicios=parseFloat(servicios);
        
        $('#detalle .importe').each(function(x,y){
            tipo=$(this).parent().siblings('td').find('.tipo').val();
            if (tipo==1) {
                tbienes=tbienes+parseFloat($(this).val());
            }
            else if (tipo==2) {
                tservicios=tservicios+parseFloat($(this).val());
            }
        });
        
        if (isValid) {
            if (tbienes>bienes) {
                bootbox.alert('Ha superado el monto máximo de bienes que ha sido asignado para ese encargo');
                $btn.removeAttr('disabled');
                return false;
            }
            else if (tservicios>servicios) {
                bootbox.alert('Ha superado el monto máximo de servicios que ha sido asignado para ese encargo');
                $btn.removeAttr('disabled');
                return false;
            }
            $('#frmRendicionEncargo').submit();
            return true; 
            
        }else{
            return false;
            $btn.removeAttr('disabled');
        }
        return false;
        $btn.removeAttr('disabled');
    });
    
    
    function getNum(val) {
       if (val == '0.00' || val == '') {
            return 0;
       }
       var val2 = val.replace(',','');
       return val2;
    }

    function number_format(amount, decimals) {
        amount += '';
        amount = parseFloat(amount.replace(/[^0-9\.]/g, ''));
        decimals = decimals || 0;
        if (isNaN(amount) || amount === 0) 
            return parseFloat(0).toFixed(decimals);
        amount = '' + amount.toFixed(decimals);
        var amount_parts = amount.split('.'),
            regexp = /(\d+)(\d{3})/;
        while (regexp.test(amount_parts[0]))
            amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');
        return amount_parts.join('.');
    }
</script>