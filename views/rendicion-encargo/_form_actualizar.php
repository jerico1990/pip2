<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'rendicion-encargo/actualizar?ID='.$ID,
        'options'           => [
            'enctype'       =>'multipart/form-data',
            'id'            => 'frmRendicionEncargo',
            
        ]
    ]
); ?>
    <input type="hidden" name="RendicionEncargo[CodigoProyecto]" value="<?= $rendicionEncargo->CodigoProyecto ?>">
    <div class="modal-body">
        <div class="form-horizontal">
            <div class="tab-container">
                <ul class="nav nav-tabs">
                    <li id="li-poaf" class="active"><a href="#tab-rendicion-encargo" data-toggle="tab"><i class="fa fa-home"></i>&nbsp;Rendición encargo</a></li>
                    <li id="li-poaf" ><a href="#tab-rendicion-informe" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Informe</a></li>
                    <li id="li-poaf" ><a href="#tab-rendicion-declaracion-jurada" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Declaración jurada</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-rendicion-encargo">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Encargo:<span class="f_req">*</span></label>
                            <div class="col-sm-6">
                                <input type="hidden" name="RendicionEncargo[EncargoID]" value="<?= $rendicionEncargo->EncargoID ?>">
                                <select  class="form-control" id="rendicion-encargo-id" onchange="Encargo(this)" disabled>
                                    <option value>Seleccionar</option>
                                    <?php foreach($encargos as $encargo){ ?>
                                    <option value="<?= $encargo->ID ?>" <?= ($encargo->ID=$rendicionEncargo->EncargoID)?'selected':'';?>><?= str_pad($encargo->Correlativo, 3, "0", STR_PAD_LEFT)  ."-".$encargo->Annio ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Bienes:<span class="f_req">*</span></label>
                            <div class="col-sm-4">
                                <input class="form-control" id="bienes" value="<?= $encar->Bienes ?>" disabled>
                            </div>
                            <label class="col-sm-2 control-label">Servicios:<span class="f_req">*</span></label>
                            <div class="col-sm-4">
                                <input class="form-control" id="servicios" value="<?= $encar->Servicios ?>" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <a class="btn btn-primary" id="agregar-rendicion">Agregar registro</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12" style="max-width: 1200px !important;overflow-x: auto ">
                                <table class="table borderless table-hover" id="detalle">
                                    <thead>
                                        <tr>
                                            <th width="250">Tipo</th>
                                            <th width="250">Fecha</th>
                                            <th width="250">Tipo de documento</th>
                                            <th width="250">Clase</th>
                                            <th width="250">N° documento</th>
                                            <th width="150">RUC</th>
                                            <th width="100">Proveedor</th>
                                            <th width="250">Detalle de gasto</th>
                                            <th width="50">Importe</th>
                                            <th width="22">&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody id="detalle_rendicion">
                                        <?php $a=1;?>
                                        <?php foreach($detalles as $detalle){ ?>
                                        <tr>
                                            <input type="hidden" name="RendicionEncargo[IDs][]" value="<?= $detalle->ID ?>">
                                            <td>
                                                <select style="width:150px" name="RendicionEncargo[Tipos][]" class="form-control tipo">
                                                    <option value>Seleccionar</option>
                                                    <option value="1" <?= ($detalle->Tipo=="1")?'selected':'';?>>Bienes</option>
                                                    <option value="2" <?= ($detalle->Tipo=="2")?'selected':'';?>>Servicios</option>
                                                </select>
                                            </td>
                                            <td ><input style="width:150px" class="form-control" name="RendicionEncargo[Fechas][]" type="date" value="<?= date('Y-m-d',strtotime($detalle->FechaDocumento)) ?>"></td>
                                            <td >
                                                <select style="width:150px" name="RendicionEncargo[TiposDocumentos][]" class="form-control">
                                                    <option value>Seleccionar</option>
                                                    <option value="1" <?= ($detalle->TipoDocumento==1)?'selected':'';?>>FACTURA</option>
                                                    <option value="2" <?= ($detalle->TipoDocumento==2)?'selected':'';?>>RECIBO POR HONORARIOS</option>
                                                    <option value="3" <?= ($detalle->TipoDocumento==3)?'selected':'';?>>BOLETA DE VENTA</option>
                                                    <option value="4" <?= ($detalle->TipoDocumento==4)?'selected':'';?>>NOTA DE CREDITO</option>
                                                    <option value="5" <?= ($detalle->TipoDocumento==5)?'selected':'';?>>NOTA DE DEBITO</option>
                                                    <option value="6" <?= ($detalle->TipoDocumento==6)?'selected':'';?>>BOLETO DE VIAJE INTERPROVINCIAL</option>
                                                    <option value="7" <?= ($detalle->TipoDocumento==7)?'selected':'';?>>VOUCHER</option>
                                                    <option value="8" <?= ($detalle->TipoDocumento==8)?'selected':'';?>>TICKET</option>
                                                    <option value="9" <?= ($detalle->TipoDocumento==9)?'selected':'';?>>COMPROBANTE DE GASTO</option>
                                                    <option value="10" <?= ($detalle->TipoDocumento==10)?'selected':'';?>>DECLARACION JURADA</option>
                                                </select>
                                            </td>
                                            <td ><input style="width:150px" name="RendicionEncargo[Clases][]" class="form-control" type="text" value="<?= $detalle->ClaseDocumento ?>"></td>
                                            <td ><input style="width:150px" name="RendicionEncargo[NumerosDocumentos][]" class="form-control" type="text" value="<?= $detalle->NumeroDocumento ?>"></td>
                                            <td ><input style="width:150px" name="RendicionEncargo[Rucs][]" class="form-control" type="text" value="<?= $detalle->RUC ?>" onblur="Proveedor(this,<?= $a ?> )"></td>
                                            <td ><input  style="width:150px" class="form-control" type="text" value="<?= $detalle->Proveedor ?>" id="proveedor_<?= $a ?>" disabled></td>
                                            <input name="RendicionEncargo[Proveedores][]" class="form-control" type="hidden" id="proveedora_<?= $a ?>" value="<?= $detalle->Proveedor ?>">
                                            <td ><input style="width:150px" name="RendicionEncargo[DetallesGastos][]" class="form-control" type="text" value="<?= $detalle->DetalleGasto ?>"></td>
                                            <td ><input style="width:150px" name="RendicionEncargo[Importes][]" class="form-control importe" type="text" value="<?= $detalle->Importe ?>"></td>
                                            <td>
                                                <a href="javascript:;" class="eliminar"><input type="hidden" value="<?= $detalle->ID ?>"><i class="fa fa-remove fa-lg" aria-hidden="true"></i></a>
                                            </td>
                                            
                                        </tr>
                                        <?php $a++;?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-rendicion-informe">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Motivo del encargo:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <textarea name="RendicionEncargo[InformeMotivoEncargo]" class="form-control" required><?= $rendicionEncargo->InformeMotivoEncargo ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Detallar actividades:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <textarea name="RendicionEncargo[InformeDetallarActividadesEncargo]" class="form-control" required><?= $rendicionEncargo->InformeDetallarActividadesEncargo ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Detallar logros:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <textarea name="RendicionEncargo[InformeDetallarLogrosEncargo]" class="form-control" required><?= $rendicionEncargo->InformeDetallarLogrosEncargo ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-rendicion-declaracion-jurada">
                        
                        <div class="form-group">
                            <div class="col-sm-3">
                                <a class="btn btn-primary" id="agregar-declaracion">Agregar registro</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12" style="max-width: 1200px !important;overflow-x: auto ">
                                <table class="table borderless table-hover" id="declaracion">
                                    <thead>
                                        <tr>
                                            <th width="250">Fecha</th>
                                            <th width="250">Detalle</th>
                                            <th width="50">Importe</th>
                                            <th width="22">&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody id="detalle_declaracion">
                                        <?php $c=1;?>
                                        <?php foreach($declaracionDetalles as $declaracionDetalle){ ?>
                                        <tr>
                                            <input type="hidden" name="RendicionEncargo[IDDeclaracions][]" value="<?= $declaracionDetalle->ID ?>">
                                            <td ><input class="form-control" name="RendicionEncargo[DeclaracionFechas][]" type="date" value="<?= date('Y-m-d',strtotime($declaracionDetalle->Fecha)) ?>"></td>
                                            <td ><textarea class="form-control" name="RendicionEncargo[DeclaracionDetalles][]" required><?= $declaracionDetalle->Detalle ?></textarea></td>
                                            <td ><input class="form-control" onKeyPress="return soloNumeros(event);" type="text" value="<?= $declaracionDetalle->Importe ?>"   name="RendicionEncargo[DeclaracionImportes][]" required></td>
                                            <td>
                                                <a href="javascript:;" class="eliminarDe"><input type="hidden" value="<?= $declaracionDetalle->ID ?>"><i class="fa fa-remove fa-lg" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>
                                        <?php $c++;?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
       
        <button type="submit" class="btn btn-primary" id="btn-guardar-rendicion-encargo"><i class="fa fa-check"></i>&nbsp;Guardar</button>
       
    </div>
<?php ActiveForm::end(); ?>

<?php
$eliminarDetalle = Yii::$app->getUrlManager()->createUrl('rendicion-encargo/eliminar-detalle');
$eliminarDetalleDecl = Yii::$app->getUrlManager()->createUrl('rendicion-encargo/eliminar-detalle-declaracion');
?>
<style type="text/css">
    .none{
        display: none;
    }
    .block{
        display: block;
    }

    .ui-draggable, .ui-droppable {
      background-position: top;
    }
</style>



<script>
    
    var $form = $('#frmRendicionEncargo');
    var formParsleyfrmRendicionEncargo = $form.parsley(defaultParsleyForm());
    var b=<?= ($a)?$a:0; ?>;
    
    
    
    function Proveedor(elemento,correlativo) {
        $.get('<?= \Yii::$app->request->BaseUrl ?>/orden/servicio/', { valor: $(elemento).val()}, function (data) {
            console.log(data);
            var jx = JSON.parse(data);
            if (jx==1) {
                bootbox.alert('RUC no se encuentra');
            }
            else
            {
                $('#proveedor_'+correlativo).val(jx.razonSocial);
                $('#proveedora_'+correlativo).val(jx.razonSocial);
                
            }
        });
    }
    
    var bienes=<?= ($encar->Bienes)?$encar->Bienes:0; ?>;
    var servicios=<?= ($encar->Servicios)?$encar->Servicios:0; ?>;
    
    
    function Encargo(elemento) {
        $.get('<?= \Yii::$app->request->BaseUrl ?>/rendicion-encargo/datos', { valor: $(elemento).val()}, function (data) {
            var jx = JSON.parse(data);
            if (jx.Success == true) {
                bienes=jx.Bienes;
                servicios=jx.Servicios;
                $('#bienes').val(bienes);
                $('#servicios').val(servicios);
            }
        });
    }
    
    
    $('#agregar-rendicion').click(function (e) {
        e.preventDefault();
        encargoid=$('#rendicion-encargo-id').val();
        if (encargoid=='') {
            bootbox.alert('Debe seleccionar un encargo');
            return false;
        }
        $.get('<?= \Yii::$app->request->BaseUrl ?>/rendicion-encargo/datos', { valor: encargoid}, function (data) {
            var jx = JSON.parse(data);
            if (jx.Success == true) {
                bienes=jx.Bienes;
                servicios=jx.Servicios;
            }
        });
        
        
        var gtml ='<tr>'+
                '<td>'+
                    '<select style="width:150px" name="RendicionEncargo[Tipos][]" class="tipo form-control" required>'+
                        '<option value>Seleccionar</option>'+
                        '<option value="1">Bienes</option>'+
                        '<option value="2">Servicios</option>'+
                    '</select>'+
                '</td>'+
                '<td width="250">'+
                    '<input class="form-control" style="width:150px" type="date" name="RendicionEncargo[Fechas][]" required>'+
                '</td>'+
                '<td>'+
                    '<select style="width:150px" name="RendicionEncargo[TiposDocumentos][]" class="form-control" required>'+
                        '<option value>Seleccionar</option>'+
                        '<option value="1">FACTURA</option>'+
                        '<option value="2">RECIBO POR HONORARIOS</option>'+
                        '<option value="3">BOLETA DE VENTA</option>'+
                        '<option value="4">NOTA DE CREDITO</option>'+
                        '<option value="5">NOTA DE DEBITO</option>'+
                        '<option value="6">BOLETO DE VIAJE INTERPROVINCIAL</option>'+
                        '<option value="7">VOUCHER</option>'+
                        '<option value="8">TICKET</option>'+
                        '<option value="9">COMPROBANTE DE GASTO</option>'+
                        '<option value="10">DECLARACION JURADA</option>'+
                    '</select>'+
                '</td>'+
                '<td>'+
                    '<input class="form-control" style="width:150px" type="text" name="RendicionEncargo[Clases][]" required>'+
                '</td>'+
                '<td>'+
                    '<input class="form-control" style="width:150px" type="text" name="RendicionEncargo[NumerosDocumentos][]" required>'+
                '</td>'+
                '<td>'+
                    '<input class="form-control" style="width:150px" onKeyPress="return soloNumeros(event);" maxlength="11" type="text" name="RendicionEncargo[Rucs][]"  id="rucs_'+b+'" onblur="Proveedor(this,'+b+')" required>'+
                '</td>'+
                '<td>'+
                    '<input class="form-control" style="width:150px"  type="text" id="proveedor_'+b+'"  disabled>'+
                    '<input class="form-control" style="width:150px" type="hidden" name="RendicionEncargo[Proveedores][]"  id="proveedora_'+b+'" required>'+
                '</td>'+
                '<td>'+
                    '<input class="form-control" style="width:150px" type="text" name="RendicionEncargo[DetallesGastos][]"  required>'+
                '</td>'+
                '<td>'+
                    '<input class="form-control importe input-number" maxlength="6" style="width:150px" type="text"   name="RendicionEncargo[Importes][]" required>'+
                '</td>'+
                '<td>'+
                    '<a href="javascript:;" class="eliminar"><i class="fa fa-remove fa-lg" aria-hidden="true"></i></a>'+
                '</td>'+
            '</tr>';
        $('#detalle_rendicion').append(gtml);
        b++;
        validarNumeros();
    });
    var c=<?= ($c)?$c:0;?>;
    $('#agregar-declaracion').click(function (e) {
        e.preventDefault();
        
        var gtml ='<tr>'+
                '<td>'+
                    '<input class="form-control"  type="date" name="RendicionEncargo[DeclaracionFechas][]" required>'+
                '</td>'+
                '<td>'+
                    '<textarea class="form-control" name="RendicionEncargo[DeclaracionDetalles][]" required></textarea>'+
                '</td>'+
                '<td>'+
                    '<input class="form-control" onKeyPress="return soloNumeros(event);" type="text"   name="RendicionEncargo[DeclaracionImportes][]" required>'+
                '</td>'+
                '<td>'+
                    '<a href="javascript:;" class="eliminarDe"><i class="fa fa-remove fa-lg" aria-hidden="true"></i></a>'+
                '</td>'+
            '</tr>';
        $('#detalle_declaracion').append(gtml);
        c++;
    });
    
    $('body').on('click', '.eliminar', function (e) {
        var r = confirm("Estas seguro de Eliminar?");
        var mensaje = '';
        if (r == true) {
            id=$(this).children().val();
            console.log(id);
            
            if (id) {
                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                $.ajax({
                    url: '<?= $eliminarDetalle ?>',
                    type: 'POST',
                    async: false,
                    data: {id:id,_csrf: toke,},
                    success: function (data) {
                        
                    },
                    error: function () {
                        _pageLoadingEnd();
                    }
                });
                $(this).parent().parent().remove();
            }
            else
            {
                $(this).parent().parent().remove();    
            }   
            mensaje = "Se elimino el Registro Correctamente";
        } 
    });
    
    $('body').on('click', '.eliminarDe', function (e) {
        var r = confirm("Estas seguro de Eliminar?");
        var mensaje = '';
        if (r == true) {
            id=$(this).children().val();
            console.log(id);
            
            if (id) {
                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                $.ajax({
                    url: '<?= $eliminarDetalleDecl ?>',
                    type: 'POST',
                    async: false,
                    data: {id:id,_csrf: toke,},
                    success: function (data) {
                        
                    },
                    error: function () {
                        _pageLoadingEnd();
                    }
                });
                $(this).parent().parent().remove();
            }
            else
            {
                $(this).parent().parent().remove();    
            }   
            mensaje = "Se elimino el Registro Correctamente";
        } 
    });
</script>

