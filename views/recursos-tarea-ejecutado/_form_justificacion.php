<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'recursos-tarea-ejecutado/justificacion-tarea?cronogramatareaid='.$cronogramatareaid.'&CodigoProyecto='.$CodigoProyecto,
        'options'           => [
	    'enctype'       =>'multipart/form-data',
            'autocomplete'  => 'off',
            'id'            => 'frmJustificacion'
        ]
    ]
); ?>
    <div class="modal-body">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label">Descripción<span class="f_req">*</span></label>
                <div class="col-sm-9">
		    <textarea class="form-control" name="JustificacionTarea[Descripcion]" <?= ($CronogramaTarea->SituacionEjecucion==15)?'disabled':''; ?> required><?= $model->Descripcion ?></textarea>
                </div>
            </div>
	    
	    <div class="form-group">
		<label class="col-sm-3 control-label">Evidencia:<span class="f_req">*</span></label>
		<div class="col-sm-9">
		    <?php if($CronogramaTarea->SituacionEjecucion!=15){ ?>
		    <div class="fileinput fileinput-new" data-provides="fileinput">
			<div class="input-group">
			    <div class="form-control uneditable-input" data-trigger="fileinput">
				<i class="fa fa-file fileinput-exists"></i>&nbsp;
				<span class="fileinput-filename">
				    <?php if(!empty($model)): ?>
                                        <?php if($model->Evidencia): ?>
                                            <?php echo $model->NombreArchivo ?>
                                        <?php else: ?>
                                            Seleccione archivo...
                                        <?php endif; ?>
                                    <?php endif; ?>
				</span>
			    </div>
			    <span class="input-group-addon btn btn-default btn-file">
				<span class="fileinput-new">Seleccionar</span>
				<span class="fileinput-exists">Cambiar</span>
				<input type="file" onchange="return Imagen(this);" id="file-cv-administrativo" name="JustificacionTarea[Archivo]">
			    </span>
			    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Eliminar</a>
			</div>
		    </div>
		    <?php } ?>
		    <?php if(!empty($model)): ?>
                        <?php if($model->Evidencia): ?>
                            <a href="justificaciontarea/<?php echo $model->Evidencia ?>" target="_blank"><i class="fa fa-download"></i>&nbsp;Descargar adjunto</a>
                        <?php endif; ?>
                    <?php endif; ?>
		</div>
	    </div>
	    <hr>
	    <div class="form-group">
                <h3>Observación</h3>
		<?php foreach($Observaciones as $Observacion){ ?>
		<div class="col-sm-12">
		    <textarea class="form-control" disabled><?= $Observacion->Observacion ?></textarea>
                </div>
		<?php } ?>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	<?php if($CronogramaTarea->SituacionEjecucion!=15){ ?>
        <button id="btn-guardar" name="JustificacionTarea[y01]" type="submit" value="1" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
	<button id="btn-enviar" name="JustificacionTarea[y01]" type="submit" value="2" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Enviar</button>
	<?php } ?>
    </div>
<?php ActiveForm::end(); ?>


<script>
    var $form = $('#frmJustificacion');
    var formParsleyJustificacion = $form.parsley(defaultParsleyForm());
</script>