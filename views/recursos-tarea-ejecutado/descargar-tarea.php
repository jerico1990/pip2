<?php
use app\models\Actividad;
use app\models\CronogramaTarea;
use app\models\Tarea;
use app\models\CronogramaProyecto;

/** Error reporting */
error_reporting(E_ALL);

/** Include path **/
ini_set('include_path', ini_get('include_path').';../web/PHPExcel/Classes/');
//require(__DIR__ . '/../../web/PHPExcel/Classes/');
/** PHPExcel */
require_once 'PHPExcel.php';

/** PHPExcel_Writer_Excel2007 */
include 'PHPExcel/Writer/Excel2007.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
$sheet = $objPHPExcel->getActiveSheet();
// Set properties
// Add some data
/*
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Hello');
$objPHPExcel->getActiveSheet()->SetCellValue('B2', 'world!');
$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Hello');
$objPHPExcel->getActiveSheet()->SetCellValue('D2', 'world!');
*/
// Rename sheet

$objPHPExcel->getActiveSheet()->setTitle('Tareas');

$Meses=[];
$objPHPExcel->setActiveSheetIndex(0);

$sheet->setCellValueByColumnAndRow(1, 1, "TAREAS");

$sheet->getStyle('B1')->getAlignment()->applyFromArray(
    array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    )
);
$sheet->getRowDimension('1')->setRowHeight(20);
$sheet->getStyle('A1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('c5c5c5');
$sheet->getStyle('A1')->getFont()->setBold(true);

$styleArray = array(
      'borders' => array(
          'allborders' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
          )
      )
  );  // Bordes


// -----------------------------------------------------------------------------
$sheet->SetCellValue('A4', ($proyecto->Avance/100));
$sheet->SetCellValue('B3', '#');
$sheet->SetCellValue('C3', 'Peso %');
$sheet->SetCellValue('D3', '');
$sheet->SetCellValue('E3', '');
$sheet->SetCellValue('F3', '');
$sheet->SetCellValue('G3', '');
$sheet->SetCellValue('H3', 'OE/Act/Recur');
$sheet->SetCellValue('I3', 'Unidad Medida');
$sheet->SetCellValue('J3', 'MetaFisica');
$sheet->getRowDimension('3')->setRowHeight(30);
$sheet->getColumnDimension('B')->setAutoSize(true);
$sheet->getColumnDimension('C')->setAutoSize(true);
$sheet->getColumnDimension('D')->setAutoSize(true);
$sheet->getColumnDimension('E')->setAutoSize(true);
$sheet->getColumnDimension('F')->setAutoSize(true);
$sheet->getColumnDimension('G')->setAutoSize(true);
$sheet->getColumnDimension('H')->setAutoSize(true);
$sheet->getColumnDimension('I')->setAutoSize(true);
$sheet->getColumnDimension('J')->setAutoSize(true);
$sheet->getStyle('B3:J3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle('B3:J3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


foreach(CronogramaProyecto::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->all() as $mes)
{
    array_push ( $Meses , 'Mes '.$mes->Mes );
}

$lastColumn = $sheet->getHighestColumn();
$lastColumn++;
foreach(CronogramaProyecto::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->all() as $mes)
{
    $sheet->SetCellValue($lastColumn.'3', 'Mes '.$mes->Mes);
    $sheet->mergeCells($lastColumn.'3:'.(++$lastColumn).'3');
    $lastColumn++;
}


$highestColumm = $objPHPExcel->setActiveSheetIndex(0)->getHighestColumn();
$sheet->mergeCells('B1:'.$highestColumm."1");  // Combinar columna
$sheet->getStyle('B1:'.$highestColumm.'1')->applyFromArray($styleArray);
$sheet->getStyle('K3:'.$highestColumm."3")->getAlignment()->applyFromArray(
    array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // alineacion horizontal
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER, // alineacion vertical
    )
);
$sheet->getStyle('B3:'.$highestColumm."3")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('c5c5c5'); // Color de fondo
$sheet->getStyle('B3:'.$highestColumm."3")->getFont()->setBold(true);  // Negrita
$sheet->getStyle('B3:'.$highestColumm."3")->applyFromArray($styleArray); // Celdas


$i=4;
foreach($componentes as $componente)
{
    $sheet->SetCellValue('B'.$i, ''.$componente->Correlativo.'');
    $sheet->SetCellValue('C'.$i, ''.($componente->Peso/100).'');
    $sheet->SetCellValue('D'.$i, ''.($componente->Avance/100).'');
    $sheet->SetCellValue('E'.$i, '');
    $sheet->SetCellValue('F'.$i, '');
    $sheet->SetCellValue('G'.$i, '');
    $sheet->SetCellValue('H'.$i, 'O.E. '.$componente->Nombre);
    $sheet->SetCellValue('I'.$i, '');
    $sheet->SetCellValue('J'.$i, $componente->Cantidad);
    $sheet->getStyle('B'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    
    $sheet->getStyle('C'.$i.':E'.$i)->getNumberFormat()->applyFromArray( 
        array( 
            'code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE_00
        )
    );
    // Perzonalizacion
    $sheet->getStyle('B'.$i.':'.$highestColumm.$i)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('c5c5c5');
    $sheet->getStyle('B'.$i.':'.$highestColumm.$i)->getAlignment()->applyFromArray(
        array(
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
        )
    );
    $sheet->getStyle('B'.$i.':'.$highestColumm.$i)->applyFromArray($styleArray);
    // End Perzonalizacion
    
    $ComponenteColumna='K';
    foreach(CronogramaProyecto::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->all() as $mes)
    {
        $sheet->SetCellValue($ComponenteColumna.$i, 'PRO');
        $sheet->SetCellValue(++$ComponenteColumna.$i, 'EJE');
        $ComponenteColumna++;
    }
    
    $actividades=Actividad::find()->where('ComponenteID=:ComponenteID',[':ComponenteID'=>$componente->ID])->orderBy('Correlativo asc')->all();
    $a=1;
    $variableSuma=0;
    foreach($actividades as $actividad)
    {
        $sheet->SetCellValue('B'.($a+$i), ''.$componente->Correlativo.'.'.$actividad->Correlativo.'' );
        $sheet->SetCellValue('C'.($a+$i), '');
        $sheet->SetCellValue('D'.($a+$i), '');
        $sheet->SetCellValue('E'.($a+$i), ''.($actividad->Peso/100).'');
        $sheet->SetCellValue('F'.($a+$i), ''.($actividad->Avance/100).'');
        $sheet->SetCellValue('G'.($a+$i), '');
        $sheet->SetCellValue('H'.($a+$i), 'Act. '.$actividad->Nombre);
        $sheet->SetCellValue('I'.($a+$i), '');
        $sheet->SetCellValue('J'.($a+$i), $actividad->Cantidad);
        $sheet->getStyle('K'.($a+$i))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        // Perzonalizacion
        $sheet->getStyle('B'.($a+$i).':'.$highestColumm.($a+$i))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ddf4fa');
        $sheet->getStyle('B'.($a+$i).':'.$highestColumm.($a+$i))->getAlignment()->applyFromArray(
            array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );
        $sheet->getStyle('E'.($a+$i).':F'.($a+$i))->getNumberFormat()->applyFromArray( 
            array( 
                'code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE_00
            )
        );
        $variableSuma=$variableSuma+($actividad->Peso/100);
    
        $sheet->getStyle('B'.($a+$i).':'.$highestColumm.($a+$i))->applyFromArray($styleArray);
        // End Perzonalizacion

        $tareas=Tarea::find()
                ->where('Tarea.ActividadID=:ActividadID',[':ActividadID'=>$actividad->ID])
                ->all();
        $r=1;
        foreach($tareas as $tarea)
        {
            $sheet->SetCellValue('B'.($a+$i+$r), '' );
            $sheet->SetCellValue('C'.($a+$i+$r), '');
            $sheet->SetCellValue('D'.($a+$i+$r), '');
            $sheet->SetCellValue('E'.($a+$i+$r), '');
            $sheet->SetCellValue('F'.($a+$i+$r), ''.($tarea->ActividadPonderado/100).'');
            $sheet->SetCellValue('G'.($a+$i+$r), ''.($tarea->Peso/100).'');
            $sheet->SetCellValue('H'.($a+$i+$r), 'Tareas. '.$tarea->Descripcion);
            
            $sheet->SetCellValue('I'.($a+$i+$r), $tarea->UnidadMedida);
            $sheet->getStyle('I'.($a+$i+$r))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $sheet->SetCellValue('J'.($a+$i+$r), $tarea->MetaFisica);
            $sheet->getStyle('J'.($a+$i+$r))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            // Perzonalizacion
            $sheet->getStyle('B'.($a+$i+$r).':'.$highestColumm.($a+$i+$r))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('fcf3d0');
            $sheet->getStyle('B'.($a+$i+$r).':'.$highestColumm.($a+$i+$r))->getAlignment()->applyFromArray(
                array(
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                )
            );
            
            $sheet->getStyle('F'.($a+$i+$r).':G'.($a+$i+$r))->getNumberFormat()->applyFromArray( 
                array( 
                    'code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE_00
                )
            );
            
            $sheet->getStyle('B'.($a+$i+$r).':'.$highestColumm.($a+$i+$r))->applyFromArray($styleArray);

            $sheet->getStyle('K'.($a+$i+$r).':'.$highestColumm.($a+$i+$r))->getAlignment()->applyFromArray(
                array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // alineacion horizontal
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER, // alineacion vertical
                )
            );

            // End Perzonalizacion

            $cronogramaTareas=CronogramaTarea::find()->where('TareaID=:TareaID',[':TareaID'=>$tarea->ID])->all();
            $cronogramaTareasMetaFisica=[];
            /*foreach($cronogramaTareas as $cronogramaTarea)
            {
                array_push ( $cronogramaTareasMetaFisica , $cronogramaTarea->MetaFisica );
            }
            $sheet->fromArray($cronogramaTareasMetaFisica,null,'K'.($a+$i+$r));
            */
            $TareaColumna='K';
            foreach($cronogramaTareas as $cronogramaTarea)
            {
                $sheet->SetCellValue($TareaColumna.($a+$i+$r), $cronogramaTarea->MetaFisica);
                $sheet->SetCellValue(++$TareaColumna.($a+$i+$r), $cronogramaTarea->MetaFisicaEjecutada);
                $TareaColumna++;
            }
            
            $sheet->getStyle('K'.($a+$i+$r))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            
            $r++;
        }
        $a=$r+$a;
    }
    $sheet->setCellValue('E'.$i,''.$variableSuma.'');
    $i=$i+$a;
}
$sheet->mergeCells('A4:A'.($i-1).'');
$sheet->getStyle('A4:A'.($i-1))->getAlignment()->applyFromArray(
                    array(
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                    )
                );
$sheet->getStyle('A4:A'.($i-1))->getNumberFormat()->applyFromArray( 
                    array( 
                        'code' => PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE_00
                    )
                );

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="EjecucionTécnica.xls"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
