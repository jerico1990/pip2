<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use app\models\DetalleGastosGenerales;
?>
<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay_progress.min.js"></script>
<script src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>

<div id="" style="min-height: 754px;">
    <div id="wrap">

        <div class="container">
            
            <!-- OBJETIVOS -->
            <style>
                .has-error {
                    border: 1px solid #ff0e0e;
                }
                .help-block {
                    color: red;
                }
                .panel .panel-body {
                    padding: 10px !important;
                }

                .panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 1px solid #a8a8ab !important;
                }

                .panel-gray .panel-body {
                    border-top: none !important;
                }

                .btn-sm {
                    padding: 5px 10px !important;
                    line-height: 1.5 !important;
                }

                .input-comp-nombre {
                    text-transform: uppercase;
                }

                .tbl-act > thead > .tr-header > th {
                    text-align: center;
                    font-size: 12px;
                    font-weight: bold;
                    padding: 5px !important;
                    vertical-align: middle !important;
                    /*border: 1px solid #cfcfd0;
                    background-color: #f0f0f1;*/
                    border: 1px solid #c0c0c0;
                    background-color: #e3e3e3;
                }

                /*.tbl-act .tr-item td {
                    padding: 2px !important;
                }*/
                input.form-control, select.form-control {
                    /*height: 28px !important;*/
                    margin-bottom: 10px;
                }

                .tbl-act .tr-act .input-nombre {
                    min-width: 320px !important;
                }

                .tbl-act .tr-act .input-unidadmedida {
                    min-width: 100px !important;
                }

                .input-number {
                    min-width: 92px !important;
                }

                .btn-add-re {
                    float: right;
                }

                .text-left {
                    text-align: left !important;
                    font-weight: bold;
                }

                .tr-comp input, .tr-proy input {
                    font-weight: bold;
                }

                .th-proy {
                    min-width: 630px !important;
                }

                #css th{
                    text-align: right;
                }
            </style>

            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="row">
                        <div class="container">
                            <?php $form = ActiveForm::begin(
                                [
                                    'action' => empty($model) ? 'gastos-generales/crear-gasto?CodigoProyecto='.$CodigoProyecto : 'gastos-generales/editar-gasto?CodigoProyecto='.$CodigoProyecto.'&id='.$model->ID,
                                    'id'=> 'frmGastos'
                                ]
                            ); ?>

                            <div class="form-horizontal">
    
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Paso Crítico:<span class="f_req">*</span></label>
                                    <div class="col-sm-10">
                                        <input type="hidden" name="GastosGenerales[CodigoProyecto]" value="<?php echo $CodigoProyecto ?>" />
                                        <input type="hidden" name="GastosGenerales[ID]" value="<?php echo !empty($model) ?$model->ID :'' ?>">
                                        <select class="form-control" id="dni" name="GastosGenerales[PasoCritico]" required>
                                            <?php if ($obs == 0 ): ?>
                                                <option value="1" <?php echo !empty($model)? $model->PasoCritico == 1?'selected':''  :'' ?> >1</option>
                                            <?php else: ?>
                                                <option value="2" <?php echo !empty($model) ?$model->PasoCritico == 2?'selected':'':'' ?> >2</option>
                                            <?php endif ?>
                                        </select>
                                    </div>
                                </div>

                                <fieldset class="scheduler-border">
                                    <legend class="scheduler-border">Comprobante de pago</legend>
                                    <div class="control-group">
                                        <label class="col-sm-1 control-label">Nº:<span class="f_req">*</span></label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control" type="text" id="Nro" name="GastosGenerales[NumeroUnidadEjecutora]" required  placeholder="001-2016" value="<?php echo !empty($model) ?$model->NumeroUnidadEjecutora :'' ?>" />
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="col-sm-1 control-label">Fecha:<span class="f_req">*</span></label>
                                        <div class="col-sm-5">
                                            <input type="date" class="form-control" type="text" id="Nro" name="GastosGenerales[FechaUnidadEjecutora]" required value="<?php echo !empty($model) ? Yii::$app->tools->dateFormat($model->FechaUnidadEjecutora,'Y-m-d') :'' ?>" />
                                        </div>
                                    </div>
                                </fieldset>

                                <fieldset class="scheduler-border">
                                    <legend class="scheduler-border">Documento de Gasto</legend>
                                    <div class="control-group">
                                        <label class="col-sm-1 control-label">Fecha:<span class="f_req">*</span></label>
                                        <div class="col-sm-4">
                                            <input type="date" class="form-control" type="text" id="Nro" name="GastosGenerales[FechaDocumentoGasto]" required value="<?php echo !empty($model) ? Yii::$app->tools->dateFormat($model->FechaDocumentoGasto,'Y-m-d') :'' ?>"/>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="col-sm-2 control-label">Tipo Solicitud:<span class="f_req">*</span></label>
                                        <div class="col-sm-5">
                                            <select class="form-control" id="tipo" name="GastosGenerales[TipoDocumentoGasto]" required>
                                                <option value="">Seleccione</option>
                                                <?php foreach ($TipoGasto as $gasto) { ?>
                                                    <option value="<?php echo $gasto->ID ?>" <?php echo !empty($model) ?$model->TipoDocumentoGasto == $gasto->ID ?'selected':'':'' ?> ><?php echo $gasto->Nombre ?></option>
                                                <?php } ?>
                                                <option value="7">Gastos Bancarios</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="control-group">
                                        <label class="col-sm-1 control-label">Clase:<span class="f_req">*</span></label>
                                        <div class="col-sm-4">
                                            <select class="form-control" id="clase" name="GastosGenerales[ClaseDocumentoGasto]" required>
                                                <option value="">Seleccione</option>
                                                <?php foreach ($TipoDocumento as $doc) { ?>
                                                    <option value="<?php echo $doc->ID ?>" <?php echo !empty($model) ?$model->ClaseDocumentoGasto == $doc->ID ?'selected':'':'' ?> ><?php echo $doc->Nombre ?></option>
                                                <?php } ?>
                                                
                                            </select>
                                        </div>
                                        <label class="col-sm-2 control-label">Serie:</label>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control" type="text" id="Serie" name="GastosGenerales[NumeroSerieDocumentoGasto]" maxlength="4" onKeyPress="return soloNumeros(event);" placeholder="001"  value="<?php echo !empty($model) ?$model->NumeroSerieDocumentoGasto :'' ?>" />
                                        </div>
                                        <label class="col-sm-1 control-label">Nro:</label>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control" type="text" id="Nro" name="GastosGenerales[NroDocumentoGasto]" maxlength="9"  placeholder="032-2016" value="<?php echo !empty($model) ?$model->NroDocumentoGasto :'' ?>" />
                                        </div>

                                        <div style="display: none" class="otrodoc">
                                            <label class="col-sm-1 control-label">Otro:</label>
                                            <div class="col-sm-11">
                                                <input type="text" class="form-control" id="Otro" name="GastosGenerales[ClaseOtro]" value="<?php echo !empty($model) ?$model->ClaseOtro :'' ?>" />
                                            </div>  
                                        </div>

                                    </div>


                                    <div class="control-group" id="viatico-select" style="display: none">
                                        <label class="col-sm-1 control-label">Integrante:<span class="f_req">*</span></label>
                                        <div class="col-sm-11">
                                            <select name="Viatico[PersonaID]" class="form-control" onchange="Persona(this)">
                                                <option value>Seleccionar</option>
                                                <?php foreach($integrantes as $integrante){?>
                                                <option value="<?= $integrante->ID ?>"><?= $integrante->NroDocumento.'-'.$integrante->Nombre.' '.$integrante->ApellidoPaterno.' '.$integrante->ApellidoMaterno ?></option>
                                                <?php }?>
                                            </select>
                                        </div>
                                    </div>

                                </fieldset>

                                <fieldset class="scheduler-border">
                                    <legend class="scheduler-border">Comisionado</legend>
                                    <div class="control-group">
                                        <label class="col-sm-2 control-label">Nº Documento:<span class="f_req">*</span></label>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" type="text" id="NroRuc" name="GastosGenerales[RucProveedor]"  onKeyPress="return soloNumeros(event);" minlength="11" maxlength="11" onfocusout="RUC($(this).val())" required  value="<?php echo !empty($model) ?$model->RucProveedor :'' ?>" />
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="col-sm-2 control-label">Razón Social:<span class="f_req">*</span></label>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" disabled="disabled" type="text" id="razonsocial" name="" value="<?php echo !empty($model) ?$model->RazonSocialProveedor :'' ?>" />
                                            <input type="hidden" class="form-control" id="razonsocial2" name="GastosGenerales[RazonSocialProveedor]" required value="<?php echo !empty($model) ?$model->RazonSocialProveedor :'' ?>" />
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="col-sm-2 control-label">Concepto:<span class="f_req">*</span></label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" type="text" id="Nro" name="GastosGenerales[ConceptoProveedor]" required value="<?php echo !empty($model) ?$model->ConceptoProveedor :'' ?>" />
                                        </div>
                                    </div>
                                </fieldset>

                                <fieldset class="scheduler-border">
                                    <legend class="scheduler-border">Recursos</legend>
                                    <a href="#" class="btn btn-info add_rubro">Agregar Rubro</a>
                                    <div class="recursos_data"></div>

                                    <?php if(!empty($model)){ ?>
                                        <table class="table tabla-recursos2" id="remove_01">
                                            <thead>
                                                <th>MATRIZ</th>
                                                <th>POA</th>
                                                <th>MES</th>
                                                <th>CANTIDAD</th>
                                                <th>COSTO</th>
                                                <th>Total</th>
                                                <th></th>
                                                <th><a href="" class="delete" data-id="01" >X</a></th>
                                            </thead>
                                            <tbody>
                                                <?php $detalle = DetalleGastosGenerales::find()->where('GastosGeneralesID=:GastosGeneralesID',[':GastosGeneralesID'=>$model->ID])->all(); 
                                                    // print_r($detalle);
                                                    $i = 0;
                                                    foreach($detalle as $dett):
                                                ?>
                                                    <tr id="del_<?php echo $i;  ?>">
                                                        <td><?php echo $dett->CodigoMatriz ?> <input type="hidden" name="GastosGenerales[CodigoMatriz2][]" value="<?php echo $dett->CodigoMatriz ?>" ></td>
                                                        <td><?php echo $dett->ObjetivoActividad ?> <input type="hidden" name="GastosGenerales[ObjetivoActividad][]" value="<?php echo $dett->ObjetivoActividad ?>" > </td>
                                                        <td><?php echo Yii::$app->tools->DescripcionMes($dett->Mes) ?> <input type="hidden" name="GastosGenerales[Mes][]" value="<?php echo $dett->Mes ?>" > </td>
                                                        <td><?php echo $dett->MetaFisica ?> <input type="hidden" name="GastosGenerales[MetaFisica][]" class="form-control input-number" value="<?php echo $dett->MetaFisica ?>"> </td>
                                                        <td><?php echo $dett->CostoUnitario ?> <input type="hidden" name="GastosGenerales[CostoUnitario][]" class="form-control input-number" value="<?php echo $dett->CostoUnitario ?>"> <input type="hidden" name="GastosGenerales[Codigos][]"></td>
                                                        <td><?php echo $dett->MetaFisica * $dett->CostoUnitario ?></td>
                                                        <td><a href="" class="deletex" data-id="<?php echo $i;  ?>" >X</a></td>
                                                    </tr>

                                                    <div class="recursos_datax"></div>
                                                <?php $i++; endforeach; ?>
                                            </tbody>
                                        </table>
                                    <?php } ?>

                                </fieldset>

                                <div class="modal-footer">
                                    <a href="#" class="btn btn-default" data-dismiss="modal">Cerrar</a>
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
                                </div>

                                <?php ActiveForm::end(); ?>
                                
                                <?php
                                    $actividades= Yii::$app->getUrlManager()->createUrl('gastos-generales/actividades');
                                    $recursos=Yii::$app->getUrlManager()->createUrl('gastos-generales/recursos');
                                    $recursosRubro = Yii::$app->getUrlManager()->createUrl('gastos-generales/recursos-rubro');
                                    $codificacion = Yii::$app->getUrlManager()->createUrl('gastos-generales/recursos-codifica');
                                ?>
                            </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>







<script type="text/javascript">
    
    $('#tipo').change(function(){
        var tipo = $(this).val();
        $('#NroRuc').val('');
        $('#razonsocial').val('');
        $('#razonsocial2').val('');

        if(tipo == 4 || tipo == 5 || tipo == 6){  // Viatico
            $('#viatico-select').show();
            $('#NroRuc').attr("onfocusout","DNI($(this).val())");
            $('#NroRuc').attr("maxlength","8");
            $('#NroRuc').attr("minlength","8");
        }else{
            $('#viatico-select').hide();
            $('#NroRuc').attr("onfocusout","RUC($(this).val())");
            $('#NroRuc').attr("maxlength","11");
            $('#NroRuc').attr("minlength","11");
        }
    });


    function Persona(elemento) {
        $('#NroRuc').val('');
        $('#razonsocial').val('');
        $('#razonsocial2').val('');
        
        $.get('<?= \Yii::$app->request->BaseUrl ?>/recursos-humanos/integrantes/', { CodigoProyecto: '<?= $CodigoProyecto ?>',Tipo:1,ID:$(elemento).val()}, function (data) {
            var jx = JSON.parse(data);
            // console.log(jx);
            if (jx.Success==true) {
                $('#NroRuc').val(jx.NroDocumento);
                $('#razonsocial').val(jx.Nombres + ' '+ jx.Apellidos);
                $('#razonsocial2').val(jx.Nombres + ' '+ jx.Apellidos);
            }
        });
    }

    $(document).on('click','.generar-obj',function(e){
        e.preventDefault();
        var arrVacios = [];

        var poa ='';
        $('.chekear:checked').each(function () {
            var obj = $(this).parent().parent().attr('data-obj');
            var act = $(this).parent().parent().attr('data-act');
            poa += obj+'.'+act+',';
            arrVacios.push({
                ID: poa
            });
        });

        if(arrVacios.length){
            $('.chekear:not(:checked)').each(function () {
                var thisx = $(this);
                $('#tabla_contenido').css('display','none');
                // var id = $(this).parent().parent().css('display','none');
                var id = $(this).parent().parent().remove();
                // console.log(id);
            });
            $('#tabla_contenido2 .objAct').val(poa);
        }else{
            bootbox.alert('Debe de seleccionar un item');
        }
    });

    var $form = $('#frmGastos');
    var formParsleyViatico = $form.parsley(defaultParsleyForm());


    $('body').on('change', '#Recurso', function (e) {
        e.preventDefault();
        var CorrelativoMatriz = $('option:selected', this).attr('data-matriz');
        console.log(CorrelativoMatriz);
        $('#correlativo-matriz').val(CorrelativoMatriz);
    });


    $('body').on('change', '#clase', function (e) {
        e.preventDefault();
        var id = $(this).val();
        if(id == 16){
            $(".otrodoc").show();
        }else{
            $(".otrodoc").hide();
        }

        // $('#correlativo-matriz').val(CorrelativoMatriz);
    });


    
    function RUC(valor) {
        $('#razonsocial').val('');
        
        $.get('<?= \Yii::$app->request->BaseUrl ?>/servicio/sunat', { valor: valor}, function (data) {
            // console.log(data);
            var jx = JSON.parse(data);
            if (jx==1) {
                bootbox.alert('RUC no se encuentra');
            }
            else
            {
                $.trim($('#razonsocial').val(jx.razonSocial));
                $.trim($('#razonsocial2').val(jx.razonSocial));
            }
            
        });
    }

    function DNI(valor) {
        $('#razonsocial').val('');
        $.get('<?= \Yii::$app->request->BaseUrl ?>/servicio/reniec', { valor: valor}, function (data) {
            // console.log(data);
            var jx = JSON.parse(data);
            if (jx==1) {
                bootbox.alert('DNI no se encuentra');
            }
            else
            {
                var name = jx.data.nombres;
                var paterno = jx.data.apellidoPaterno;
                var materno = jx.data.apellidoMaterno;
                $.trim($('#razonsocial').val(name.trim()+' '+paterno.trim()+' '+materno.trim() ) );
                $.trim($('#razonsocial2').val(name.trim()+' '+paterno.trim()+' '+materno.trim() ));
            }
            
        });
    }


    $('body').on('click', '.agregar-recurso', function (e) {
        e.preventDefault();
        var arrVacios = [];
        var arrDatos = [];
        var poa ='';
        $('.chekear:checked').each(function () {
            var obj = $(this).parent().parent().attr('data-obj');
            var act = $(this).parent().parent().attr('data-act');
            var codifica = $(this).parent().parent().attr('data-codifica');
            poa += obj+'.'+act+',';

            var mes = $(this).parent().siblings('.rubro').data('mes');
            var numes = $(this).parent().siblings('.rubro').data('numes');
            // var meta = $(this).parent().siblings('.meta').data('meta');
            var meta = $(this).parent().siblings('.meta').find('.meta-fisica').val();
            var costo = $(this).parent().siblings('.costo').find('.meta-costo').val();
            // var costo = $(this).parent().siblings('.costo').data('costo');
            arrVacios.push({
                ID: 1
            });
            
            arrDatos.push({
                POA: poa,
                Mes: mes,
                Meta: meta,
                Costo: costo,
                Codifica : codifica,
                NuMes : numes
            });
        });

        if(arrVacios.length){
            console.log($('.tabla-recursos').length);
            var tot = $('.tabla-recursos').length;
            var xhtml = '<table class="table tabla-recursos" id="remove_'+tot+'">'
                xhtml += '<thead>'
                xhtml +=    '<th>MATRIZ</th>'
                xhtml +=    '<th>POA</th>'
                xhtml +=    '<th>MES</th>'
                xhtml +=    '<th>CANTIDAD</th>'
                xhtml +=    '<th>COSTO</th>'
                xhtml +=    '<th>Total</th>'
                xhtml +=    '<th></th>'
                xhtml +=    '<th><a href="" class="delete" data-id="'+tot+'" >X</a></th>'
                xhtml += '</thead>'
                xhtml += '<tbody>'
                $(arrDatos).each(function (e,v) {
                    xhtml +=    '<tr id="del_'+tot+e+'">'
                    xhtml +=        '<td>'+v.Codifica+' <input type="hidden" name="GastosGenerales[CodigoMatriz2][]" value="'+v.Codifica+'" ></td>'
                    xhtml +=        '<td>'+poa+' <input type="hidden" name="GastosGenerales[ObjetivoActividad][]" value="'+poa+'" > </td>'
                    xhtml +=        '<td>'+v.Mes+' <input type="hidden" name="GastosGenerales[Mes][]" value="'+v.NuMes+'" > </td>'
                    xhtml +=        '<td>'+v.Meta+' <input type="hidden" name="GastosGenerales[MetaFisica][]" class="form-control input-number" value="'+v.Meta+'"> </td>'
                    xhtml +=        '<td>'+v.Costo+' <input type="hidden" name="GastosGenerales[CostoUnitario][]" class="form-control input-number" value="'+v.Costo+'"> <input type="hidden" name="GastosGenerales[Codigos][]"></td>'
                    var costo = v.Costo.replace(",", "");
                    var meta = v.Meta.replace(",", "");
                    xhtml +=        '<td>'+( meta * costo)+'</td>'
                    xhtml +=        '<td><a href="" class="deletex" data-id="'+tot+e+'" >X</a></td>'
                    xhtml +=    '</tr>'
                });
                xhtml += '</tbody>'
                xhtml += '</table>';
            $(".recursos_data").append(xhtml);
            $("#modal-ctrl-requerimiento").show();
            $('#modal-matriz').modal('hide');
        }else{
            bootbox.alert('Debe de seleccionar un item');
        }
    });


    $('body').on('click','.delete',function(e){
        e.preventDefault();
        var id = $(this).data('id');
        $('#remove_'+id).remove();
    });

    $('body').on('click','.deletex',function(e){
        e.preventDefault();
        var id = $(this).data('id');
        $('#del_'+id).remove();
    });

    validarNumeros();
</script>
    