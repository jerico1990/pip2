<div class="panel panel-primary">
    <div class="panel-body">
        <div class="row">
            <div class="container">
                <div class="form-horizontal">

                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border">Recursos</legend>
                        <div class="control-group">
                            <label class="col-sm-2 control-label">Rubro Elegible:<span class="f_req">*</span></label>
                            <div class="col-sm-10">
                                <select type="text" class="form-control" id="Matriz" name="GastosGenerales[CodigoMatriz]" required>
                                    <option>Seleccione</option>
                                    <?php foreach ($codificacion as $rubro) { ?>
                                        <option value="<?php echo $rubro['Codificacion2'] ?>"><?php echo $rubro['Codificacion2'] ?></option>
                                    <?php } ?>
                                </select>
                                <!-- <input type="hidden" id="correlativo-rubro" name="GastosGenerales[CorrelativoRubroElegible]" /> -->
                            </div>
                        </div>
                        <div id="tabla_contenido2">
                            <div class="control-group">
                                <label class="col-sm-2 control-label">Objetivos y actividades:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="ObjetivoActividad" class="form-control objAct" disabled required="required">
                                    <input type="hidden" name="GastosGenerales[ObjetivoActividad]" class="objAct">
                                </div>
                            </div>
                        </div>

                        
                        <div class="carga-loading" style="display: none;">
                            <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                        </div>

                        <div id="carga-data"></div>
                    </fieldset>


                     <div class="modal-footer">
                        <a href="#" class="btn btn-default btn-cerrar" >Cerrar</a>
                        <button type="submit" class="btn btn-primary agregar-recurso"><i class="fa fa-check"></i>&nbsp;Agregar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    
    $('body').on('change', '#Matriz', function (e) {
        e.preventDefault();
        var ComponenteID=$(this).val();
        var Code = ComponenteID.split('.');
        var i = 0;
        $.ajax({
            url: '<?= \Yii::$app->request->BaseUrl ?>/gastos-generales/recursos-codifica?Codificacion='+Code[1]+'&CodigoProyecto=<?php echo $CodigoProyecto ?>&rubro='+Code[0],
            type: 'GET',
            beforeSend: function() {
                // setting a timeout
                // $("#carga-loading").addClass('loading');
                $('#carga-data').html("");
                $(".carga-loading").show();
                i++;
            },
            success: function(data){
                $('#carga-data').html(data);
            },
            error: function(xhr) { // if error occured
                alert("Error occured.please try again");
                $(".carga-loading").append(xhr.statusText + xhr.responseText);
                $(".carga-loading").hide();
            },
            complete: function() {
                i--;
                if (i <= 0) {
                    // $(".carga-loading").removeClass('loading');
                    $(".carga-loading").hide();
                }
            },
            dataType: 'html'
        });
    }); 
</script>