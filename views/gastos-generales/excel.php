<?php

/** Error reporting */
error_reporting(E_ALL);

/** Include path **/
ini_set('include_path', ini_get('include_path').';../web/PHPExcel/Classes/');
//require(__DIR__ . '/../../web/PHPExcel/Classes/');
/** PHPExcel */
include 'PHPExcel.php';

/** PHPExcel_Writer_Excel2007 */
include 'PHPExcel/Writer/Excel2007.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
$sheet = $objPHPExcel->getActiveSheet();

$sheet->setTitle('Gastos');


$objPHPExcel->setActiveSheetIndex(0);

$sheet->SetCellValue('H3', '#');


if(empty($CodigoProyecto)){
    $sheet->SetCellValue('B3', 'Codigo Proyecto');
}


$sheet->SetCellValue('C3', '#');
// $sheet->SetCellValue('B3', 'Paso Critico');
$sheet->SetCellValue('D3', 'Fecha');
$sheet->SetCellValue('E3', 'Nº');
$sheet->SetCellValue('F3', 'Fecha Documento');
$sheet->SetCellValue('G3', 'Tipo');
$sheet->SetCellValue('H3', 'Clase Documento');
$sheet->SetCellValue('I3', 'Serie Documento');
$sheet->SetCellValue('J3', 'Nº Documento');
$sheet->SetCellValue('K3', 'RUC');
$sheet->SetCellValue('L3', 'Razon Social');
$sheet->SetCellValue('M3', 'Concepto');
// $sheet->SetCellValue('N3', 'Correlativo Objetivo');
// $sheet->SetCellValue('O3', 'Objetivo');
// $sheet->SetCellValue('P3', 'Correlativo Actividad');
// $sheet->SetCellValue('Q3', 'Actividad');
$sheet->SetCellValue('N3', 'Codigo Matriz');
$sheet->SetCellValue('O3', 'Objetivo - Actividad');
$sheet->SetCellValue('P3', 'Mes');
$sheet->SetCellValue('Q3', 'Cantidad');
$sheet->SetCellValue('R3', 'Costo Unitario');
$sheet->SetCellValue('S3', 'Total');
// $sheet->SetCellValue('W3', 'Nº Cheque');

$i=4;
foreach($resultados as $resultado)
{
    if(empty($CodigoProyecto)){
        $sheet->SetCellValue('B'.$i, $resultado["CodigoProyecto"]);
    }
    $sheet->SetCellValue('C'.$i, ($i-3));
    // $sheet->SetCellValue('B'.$i, $resultado["PasoCritico"]);
    // $sheet->SetCellValue('C'.$i, $resultado["CodigoProyecto"]);
    $sheet->SetCellValue('D'.$i, Yii::$app->tools->dateFormat($resultado["FechaUnidadEjecutora"]) );
    $sheet->SetCellValue('E'.$i, $resultado["NumeroUnidadEjecutora"]);
    $sheet->SetCellValue('F'.$i, Yii::$app->tools->dateFormat($resultado["FechaDocumentoGasto"]));
    $sheet->SetCellValue('G'.$i, $resultado["TipoDocumentoGasto"]);
    $sheet->SetCellValue('H'.$i, $resultado["ClaseDocumentoGasto"]);
    $sheet->SetCellValue('I'.$i, $resultado["NumeroSerieDocumentoGasto"]);
    $sheet->SetCellValue('J'.$i, $resultado["NroDocumentoGasto"]);
    $sheet->SetCellValue('K'.$i, $resultado["RucProveedor"]);
    $sheet->SetCellValue('L'.$i, $resultado["RazonSocialProveedor"]);
    $sheet->SetCellValue('M'.$i, $resultado["ConceptoProveedor"]);
    // $sheet->SetCellValue('N'.$i, $resultado["CorrelativoObjetivo"]);
    // $sheet->SetCellValue('O'.$i, $resultado["Objetivo"]);
    // $sheet->SetCellValue('P'.$i, $resultado["CorrelativoActividad"]);
    // $sheet->SetCellValue('Q'.$i, $resultado["Actividad"]);
    $sheet->SetCellValue('N'.$i, $resultado["CodigoMatriz"]);
    $sheet->SetCellValue('O'.$i, $resultado["ObjetivoActividad"]);
    $sheet->SetCellValue('P'.$i, Yii::$app->tools->DescripcionMes($resultado["Mes"]) );
    $sheet->SetCellValue('Q'.$i, $resultado["MetaFisica"]);
    $sheet->SetCellValue('R'.$i, $resultado["CostoUnitario"]);
    $sheet->SetCellValue('S'.$i, ($resultado["MetaFisica"]*$resultado["CostoUnitario"]) );
    // $sheet->SetCellValue('W'.$i, $resultado["NroCheque"]);
    $i++;
}
    




// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="GastosDetallado.xls"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
