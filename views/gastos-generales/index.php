
<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay_progress.min.js"></script>
<script src=" //cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>

<?php 

// echo '<pre>';print_r($gasto);die;
if(!empty($gasto)){
    switch ($gasto->Situacion) {
        case 1:
            $gastar = 1;
            $estado = '';
            $observa = '';
        break;
        case 2:
            $gastar = 2;
            $estado = 'EN PROCESO';
            $observa = '';
        break;
        case 3:
            $gastar = 3;
            $estado = '';
            $observa = '';
        break;
        case 4:
            $gastar = 4;
            $estado = 'OBSERVADO';
            $observa = isset($observa->Observacion)?$observa->Observacion:'';
        break;
        case 5:
            $gastar = 5;
            $estado = 'CERRADO';
            $observa = '';
        break;
    }
}else{
    $gastar = 1;
    $estado = '';
    $observa = '';
}

?>

<div id="page-content" style="min-height: 1342px;">
    
    <div id="wrap">
        <div id="page-heading">
            <h1>Carga de ITF </h1>
        </div>
        
        <div class="container panel-default">
            <?php if ($gastar != 5): ?>
                    <h3><?php echo $estado ?></h3>
                    <h4><?php echo $observa ?></h4>
                    <h4 class=""><strong> El total ingresado es de : <?php echo ' S/.'.number_format($total['tot'], 2, '.', '') ?></strong> </h4>
                    <div class="form-group">
                        <?php if($gastar==1 || $gastar==3 || $gastar==4){ ?>
                            <a href="#" class="btn btn-primary btn-crear-requerimiento" data-codigo-proyecto="">Generar Gastos</a>
                        <?php } ?>
                        
                        <a class="btn btn-info" href="<?= Yii::$app->getUrlManager()->createUrl('gastos-generales/excel?CodigoProyecto='.$CodigoProyecto.'') ?>">Reporte Excel</a>
                        
                        <?php if($gastar==1 || $gastar==3){ ?>
                            <a class="btn btn-success" href="#" id="enviar">ENVIAR</a>
                        <?php } ?>

                        <?php if($gastar==4){ ?>
                            <a class="btn btn-success" href="#" id="enviar-observacion">VOLVER ENVIAR</a>
                            <hr>
                                Sustento:
                                <textarea class="form-control" required id="Observacion-Gasto"></textarea>
                            <hr>
                        <?php } ?>
                        <!-- <a class="btn btn-default" href="<?= Yii::$app->getUrlManager()->createUrl('gastos-generales/carga-total') ?>">Carga Excel</a> -->
                        <br>
                    </div>
                    <div class="table-responsive">
                        <table id="proyectos" class="display table table-bordered table-striped table-condensed table-hover" cellspacing="0" >
                            <thead>
                                <tr>
                                    <th>Numero Comprobante de pago</th>
                                    <th>Tipo Documento Gasto</th>
                                    <th>Clase Documento</th>
                                    <th>Total</th>
                                    <!-- <th></th> -->
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
            <?php else: ?>
                <h3>Se ah completado el llenado de información</h3>
            <?php endif ?>
        </div>
    </div>
</div>

<style type="text/css">
    .none{
        display: none;
    }
    .block{
        display: block;
    }

    .modal-body-main .loading{
        text-align: center;
    }

    .loading{
        margin: 40px;
    }
</style>

<div class="modal fade" id="modal-ctrl-requerimiento"  data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Gastos Detalle</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-matriz"  data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button> -->
                <h4 class="modal-title">Rubro Elegible</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<script>
    function js_buscar()
    {
       
    }
    var tblresultjs;
    var configDTjs={
        "order": [[ 0, "desc" ]],
        "language": {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta lista",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":    "Último",
                "sNext":    "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        destroy: true
    };

    $(document).ready(function() {
        var CodigoProyecto="";
        $.ajax({
            url:'gastos-generales/lista-gastos?CodigoProyecto='+CodigoProyecto,
            async:false,
            data:{},
            beforeSend:function()
            {
                $('.panel-default').LoadingOverlay("show");
            },
            success:function(result)
            {
                    //$tblresultjs.destroy();//
                    $("#proyectos tbody").html(result);
                    $('#proyectos').DataTable(configDTjs);
                    //tblresultjs=$('#example').DataTable(configDTjs);
                    $('.panel-default').LoadingOverlay("hide",true);
            },
            error:function(){
                $('.panel-default').LoadingOverlay("hide",true);
                alert('Error al realizar el proceso de busqueda.');
            }
        });
        
    } );
    

    var i = 0;
    $('body').on('click', '.btn-crear-requerimiento', function (e) {
        e.preventDefault();
        var CodigoProyecto = $(this).attr('data-codigo-proyecto');
        AjaxSendForm('<?= \Yii::$app->request->BaseUrl ?>/gastos-generales/crear', "#modal-ctrl-requerimiento .modal-body-main", {'CodigoProyecto':CodigoProyecto}, false);
        $('#modal-ctrl-requerimiento').modal('show');
    });


    $('body').on('click', '.btn-editar-requerimiento', function (e) {
        e.preventDefault();
        var CodigoProyecto = $(this).attr('data-codigo-proyecto');
        var codigo = $(this).attr('data-id');
        AjaxSendForm('<?= \Yii::$app->request->BaseUrl ?>/gastos-generales/crear', "#modal-ctrl-requerimiento .modal-body-main", {'CodigoProyecto':CodigoProyecto,'id':codigo}, false);
        $('#modal-ctrl-requerimiento').modal('show');
    });


    $('body').on('click', '.btn-delete', function (e) {
        e.preventDefault();
        var $this = $(this);
        bootboxConfirmEliminar($this,
            'Está seguro de eliminar el registro?',
            function () {
                var id = $this.attr('data-id');
                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                $.get('<?= \Yii::$app->request->BaseUrl ?>/gastos-generales/eliminar?id=', { id: id ,_csrf: toke }, function (data) {
                    var jx = JSON.parse(data);
                    if (jx.Success == true) {
                        location.reload();
                    }
                });
            });
    });


    var i = 0;
    function AjaxSendForm(url, placeholder,data, append) {
        // var data = $(form).serialize();
        append = (append === undefined ? false : true); 
        $.ajax({
            type: 'GET',
            url: url,
            data: data,
            beforeSend: function() {
                // setting a timeout
                $(placeholder).html("");
                $(placeholder).append('<div class="loading"><i class="fa fa-spinner fa-pulse fa-5x fa-fw"></i></div>');
                i++;
            },
            success: function(data) {
                $(placeholder).html("");
                if (append) {
                    $(placeholder).append(data);
                } else {
                    $(placeholder).html(data);
                }
            },
            error: function(xhr) { // if error occured
                alert("Error occured.please try again");
                $(placeholder).append(xhr.statusText + xhr.responseText);
            },
            complete: function() {
                i--;
                if (i <= 0) {
                    $(".loading").remove();
                }
            },
            dataType: 'html'
        });
    }


    $('body').on('click', '.add_rubro', function (e){
        e.preventDefault();
        var rub = $(this);

        var CodigoProyecto = $(this).attr('data-codigo-proyecto');
        AjaxSendForm('<?= \Yii::$app->request->BaseUrl ?>/gastos-generales/matriz-crear', "#modal-matriz .modal-body-main", {'CodigoProyecto':CodigoProyecto}, false);
        $("#modal-ctrl-requerimiento").hide();
        $('#modal-matriz').modal('show');
        
    });


    $('body').on('click', '.btn-cerrar', function (e){
        e.preventDefault();
        $("#modal-ctrl-requerimiento").show();
        $('#modal-matriz').modal('hide');
    });


    $('body').on('click', '#enviar', function (e) {
        e.preventDefault();
        var $this = $(this);
        bootBoxConfirm('Está seguro de enviar el registro?',function(){
           $.ajax({
                type: 'GET',
                url: '<?= \Yii::$app->request->BaseUrl ?>/gastos-generales/enviar-gastos',
                // data: {ID:ID,_csrf: toke},
                beforeSend: function() {
                    $("#proyectos tbody").html("");
                    $('.panel-default').LoadingOverlay("show");
                },
                success: function(data) {
                    // $("#proyectos tbody").html(data);
                    // $('#proyectos').DataTable(configDTjs);
                    location.reload();
                    // $('.panel-default').LoadingOverlay("hide",true);
                },
                error: function(xhr) { 
                    alert("Error occured.please try again");
                    console.log(xhr.statusText + xhr.responseText);
                },
                complete: function() {
                    // $('.panel-default').LoadingOverlay("hide",true);
                },
                // dataType: 'html'


            }); 
        });
    });


    $('body').on('click', '#enviar-observacion', function (e) {
        e.preventDefault();
        var $this = $(this);
        var obs = $('#Observacion-Gasto').val();
        if(obs != ''){
            bootBoxConfirm('Está seguro de enviar el registro?',function(){
                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                $.ajax({
                    type: 'POST',
                    url: '<?= \Yii::$app->request->BaseUrl ?>/gastos-generales/enviar-gastos-obs',
                    data: {obs:obs,_csrf: toke},
                    beforeSend: function() {
                        $("#proyectos tbody").html("");
                        $('.panel-default').LoadingOverlay("show");
                    },
                    success: function(data) {
                        // $("#proyectos tbody").html(data);
                        // $('#proyectos').DataTable(configDTjs);
                        location.reload();
                        // $('.panel-default').LoadingOverlay("hide",true);
                    },
                    error: function(xhr) { 
                        alert("Error occured.please try again");
                        console.log(xhr.statusText + xhr.responseText);
                    },
                    complete: function() {
                        // $('.panel-default').LoadingOverlay("hide",true);
                    },
                    // dataType: 'html'
                }); 
            });
        }else{
            alert('Debe de llenar el sustento');
        }

    });
    

</script>