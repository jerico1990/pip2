<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use app\models\DetalleGastosGenerales;
?>
<style type="text/css">
    .tbl-act > thead > .tr-header > th {
        text-align: center;
        font-size: 12px;
        font-weight: bold;
        padding: 5px !important;
        vertical-align: middle !important;
        border: 1px solid #c0c0c0;
        background-color: #e3e3e3;
    }
</style>
<?php //echo '<pre>'; print_r($gastos) ; die()?>
<?php if(!empty($gastos)): ?>

	<?php 
	switch ($gastos->Situacion) {
		case 2: 
		?>
			<a href="#" class="btn btn-primary generar-aprobacion-itf">APROBAR</a>
			<a href="#" class="btn btn-primary generar-observacion-itf" data-recurso-id="">OBSERVAR</a>
			<h4><?php echo !empty($observacion)? "<strong>SUSTENTO:</strong> ".$observacion->RespuestaObservacion: '' ?></h4>
		<?php break; 
			case 4: 
		?>
			<h2>OBSERVADO</h2>
			<p><?php echo !empty($observacion)? $observacion->Observacion: '' ?></p>
			<p><?php echo !empty($observacion)? "SUSTENTO: ".$observacion->RespuestaObservacion: '' ?></p>
		<?php break; ?>
	<?php } ?>
	<!-- <h5><strong>FALTA ENVIAR PARA APROBACIÓN</strong></h5> -->
<?php endif ?>

<hr>

<?php $total = 0; foreach ($resultados as $rest): ?>
	<div class="panel panel-gray panel-comp">
		<div class="panel-heading"> Total : <?php echo $rest['tot'] ?></div>
		<div class="panel-body">
			<div class="table-responsive div-tbl-comp">
				<table class="tbl-act table">
					<tbody>
						<tr class="tr-header">
							<td colspan="3">CP</td>
							<td>Clase</td>
							<td>Tipo</td>
							<td>Total</td>
							<!-- <td>Observación</td> -->
						</tr>
							<tr class="tr-item tr-act warning">
								<td style="text-align:left !important" colspan="3"><?php echo $rest['NumeroUnidadEjecutora'] ?></td>
								<td style="text-align:center !important"><?php echo $rest['Tipo'] ?></td>
								<td style="text-align:center !important"><?php echo $rest['Clase'] ?></td>
								<td style="text-align:center !important"><?php echo $rest['tot'] ?></td>
								<!-- <td style="text-align:center !important"><span style="color:red;cursor: pointer"  class="fa fa-search generar-observacion-itf" data-recurso-id="<?php echo $rest['ID'] ?>"></span></td> -->
							</tr>
							<?php $detalle = DetalleGastosGenerales::find()->where('GastosGeneralesID=:GastosGeneralesID',[':GastosGeneralesID'=>$rest['ID']])->all();  ?>
							<?php if (!empty($detalle)): ?>
								<tr class="tr-header">
									<td>MATRIZ</td>
									<td>POA</td>
									<td>MES</td>
									<td>CANTIDAD</td>
									<td>COSTO</td>
									<td>Total</td>
								</tr>
								<?php
			                        $i = 0;
			                        foreach($detalle as $dett):
					            ?>
									<tr class="tr-item tr-act info">
										<td style="text-align:center;width: 80px;"><?php echo !empty($rest['CodigoMatriz']) ? $rest['CodigoMatriz']: $dett->CodigoMatriz ?></td>
										<td style="text-align:left !important"><?php echo $dett->ObjetivoActividad ?></td>
										<td style="text-align:center !important"><?php echo Yii::$app->tools->DescripcionMes($dett->Mes) ?></td>
										<td style="text-align:center !important"><?php echo $dett->MetaFisica ?></td>
										<td style="text-align:center !important"><?php echo $dett->CostoUnitario ?></td>
										<td style="text-align:center !important"><?php echo $dett->MetaFisica * $dett->CostoUnitario ?></td>
									</tr>
								<?php $i++; endforeach; ?>
							<?php endif ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
<?php $total = $total+$rest['tot']; endforeach ?>

<h2>total: <?php echo $total ?></h2>
<a class="btn btn-info" href="<?= Yii::$app->getUrlManager()->createUrl('gastos-generales/excel?CodigoProyecto='.$CodigoProyecto.'') ?>">Reporte Excel</a>

<script type="text/javascript">
    validarNumeros();


</script>