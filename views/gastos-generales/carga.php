<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<div id="page-content" style="min-height: 1342px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Carga rapida</h1>
        </div>
        
        <div class="container">
<?php $form = ActiveForm::begin(
    [
        'options'           => [
            'enctype'       =>'multipart/form-data',
        ]
    ]); ?>
    <div class="modal-body">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label">Documento:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input class="form-control" type="file" name="file">
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

