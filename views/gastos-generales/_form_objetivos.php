<style type="text/css">
    .tbl-act > thead > .tr-header > th {
        text-align: center;
        font-size: 12px;
        font-weight: bold;
        padding: 5px !important;
        vertical-align: middle !important;
        border: 1px solid #c0c0c0;
        background-color: #e3e3e3;
    }
</style>

<table class="table tbl-act">
    <thead>
        <tr class="tr-header">
                <th>Recurso</th>
                <th>Cantidad</th>
                <th>Precio Unitario</th>
                <th>Total</th>
            </tr>
    </thead>
    <tbody>
        <tr>
            <?php 
            $esp = $resultados['Especifica'] != '-'? $resultados['Especifica'].' - ' : '';
            $det = $resultados['Detalle'] != '-'? $resultados['Detalle'].' - '  :'';
            $nom = $resultados['Nombre'] != '-'? $resultados['Nombre'].' - '  : '';
            ?>
            <td><?php echo $esp.$det.$nom ?></td>
            <td class="input-number"><?php echo $resultados['Cantidad'] ?></td>
            <td class="input-number"><?php echo $resultados['CostoUnitario'] ?></td>
            <td class="input-number"><?php echo $resultados['Total'] ?></td>
        </tr>
    </tbody>
</table>
<hr>

<table class="table tbl-act">
    <thead>
        <tr class="tr-header">
                <th>#</th>
                <th>Componente - Actividad</th>
                <th>Mes</th>
                <th>Cantidad</th>
                <th>Costo Unitario</th>
                <th>Total</th>
            </tr>
    </thead>
    <tbody>
        <?php $i=1; foreach ($detalle as $det): ?>
            <tr data-id="<?php echo $i; ?>" data-obj="<?php echo $det['Componente'] ?>" data-act="<?php echo $det['Actividad'] ?>" data-codifica="<?php echo $rubro.'.'.$codifica ?>">
                <td><input type="checkbox" name="chekear" class="chekear"></td>
                <td><?php echo $det['Componente'].'.'.$det['Actividad'] ?> <input type="hidden" name="GastosGenerales[ObjetivoActividad][]" value="<?php echo $det['Componente'].'.'.$det['Actividad'] ?>"></td>
                
                <td class="rubro" data-mes="<?php echo Yii::$app->tools->DescripcionMes($det['Mes']) ?>" data-numes="<?php echo $det['Mes'] ?>"><?php echo Yii::$app->tools->DescripcionMes($det['Mes']) ?> <input type="hidden" name="GastosGenerales[Mes][]" value="<?php echo $det['Mes'] ?>"></td>
                
                <td class="meta" data-meta="<?php echo $det['MetaFisica'] ?>" ><input type="text" name="GastosGenerales[MetaFisica][]" class="form-control input-number meta-fisica" value="<?php echo $det['MetaFisica'] ?>"></td>
                
                <td class="costo" data-costo="<?php echo $det['CostoUnitario'] ?>" ><input type="text" name="GastosGenerales[CostoUnitario][]" class="form-control input-number meta-costo" value="<?php echo $det['CostoUnitario'] ?>"> 
                    <input type="hidden" name="GastosGenerales[Codigos][]">
                </td>
                <td class="input-number"><?php echo $det['MetaFisica']*$det['CostoUnitario'] ?></td>
            </tr>
        <?php $i++; endforeach ?>
    </tbody>
</table>

<a href="#" class="btn btn-info generar-obj">Generar</a>
<hr>
<div class="alert alert-warning" style="text-align:justify">
    <strong>NOTA:</strong> Seleccionar dando click en en  checkbox para identificar los recursos y meses que se ejecutara.
</div>

<script type="text/javascript">
    validarNumeros();
</script>