<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<?php $form = ActiveForm::begin(
    [
        'action'            => \Yii::$app->request->BaseUrl.'/usuario-proyecto/crear',
        'options'           => [
            'enctype'       =>'multipart/form-data',
            'id'            => 'frmAdjuntar',
        ]
    ]
); ?>
<div class="modal-body">
    <div class="form-horizontal">
        <div class="form-group field-usuarioproyecto-usuarioid">
            <label class="control-label" for="usuarioproyecto-usuarioid">Usuario ID</label>
            <select id="usuarioproyecto-usuarioid" class="form-control" name="UsuarioProyecto[UsuarioID]">
                <option value>Seleccionar</option>
                <?php foreach($usuarios as $usuario){ ?>
                    <option value="<?= $usuario->ID ?>"><?= $usuario->username ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group field-usuarioproyecto-codigoproyecto">
            <label class="control-label" for="usuarioproyecto-codigoproyecto">Codigo Proyecto</label>
            <select id="usuarioproyecto-codigoproyecto" class="form-control" name="UsuarioProyecto[CodigoProyecto]">
                <option value>Seleccionar</option>
                <?php foreach($codigos as $codigo){ ?>
                    <option value="<?= $codigo->Codigo ?>"><?= $codigo->Codigo ?></option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button id="btn-save-adjuntar" type="submit" class="btn btn-primary">Guardar</button>
        
    </div>
</div>
<?php ActiveForm::end(); ?>

<script>
    var formParsleyAdjuntar = $('#frmAdjuntar').parsley(defaultParsleyForm());
    /*$('#btn-save-adjuntar').click(function (e) {
        e.preventDefault();
        var isValid = formParsleyAdjuntar.validate();
        if (isValid) {
            sendForm($('#frmAdjuntar'), $(this), function (eve) {
                console.log(eve);
            });
        }
    });*/
</script>
