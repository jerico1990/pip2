<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UsuarioProyecto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usuario-proyecto-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="form-group field-usuarioproyecto-usuarioid">
        <label class="control-label" for="usuarioproyecto-usuarioid">Usuario ID</label>
        <select id="usuarioproyecto-usuarioid" class="form-control" name="UsuarioProyecto[UsuarioID]">
            <option value>Seleccionar</option>
            <?php foreach($usuarios as $usuario){ ?>
                <option value="<?= $usuario->ID ?>"><?= $usuario->username ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="form-group field-usuarioproyecto-codigoproyecto">
        <label class="control-label" for="usuarioproyecto-codigoproyecto">Codigo Proyecto</label>
        <select id="usuarioproyecto-codigoproyecto" class="form-control" name="UsuarioProyecto[CodigoProyecto]">
            <option value>Seleccionar</option>
            <?php foreach($codigos as $codigo){ ?>
                <option value="<?= $codigo->Codigo ?>"><?= $codigo->Codigo ?></option>
            <?php } ?>
        </select>
    </div>
   
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
