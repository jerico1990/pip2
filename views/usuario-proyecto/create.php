<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UsuarioProyecto */

?>
<div id="page-content" style="min-height: 754px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Asignar proyecto </h1>
        </div>
        <div class="container">
            <div class="usuario-proyecto-create">
            
                <h1><?= Html::encode($this->title) ?></h1>
            
                <?= $this->render('_form', [
                    'model' => $model,
                    'usuarios'=> $usuarios,
                    'codigos'=> $codigos
                ]) ?>
            
            </div>
        </div>
    </div>
</div>