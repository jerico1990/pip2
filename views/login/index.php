<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<img class="img-responsive" src="<?= \Yii::$app->request->BaseUrl ?>/recursos/img/cabecera.jpg" width="100%">
<div class="mycenter">
    <h1 class="text-center title">Sistema de Programación,Ejecución,Seguimiento y Control de los Proyectos del PIP2</h1>
    <div class="mycenter-body">
        <style>
            #modal-comunicado p, #modal-comunicado li {
                text-align: left !important;
            }
        </style>
        <div class="panel panel-success">
            <?php $form = ActiveForm::begin(
                [
                    'options' => [
                        'class' => 'form-horizontal',
                        'style' => 'margin-bottom: 0px !important;'
                     ]
                ]
            ); ?>
                <div class="panel-body" style="border-radius: 0px;">
                    <div class="alert alert-dismissable alert-danger" style="display: none">
                        Usuario y/o clave incorrectas.
                    </div>
                    <h4 class="text-center" style="margin-bottom: 25px;">Inicia sesión </h4> 
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control" id="Usuario" name="LoginForm[username]" placeholder="Usuario" value="" maxlength="1000">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <!-- <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input type="password" class="form-control" id="Clave" name="LoginForm[password]" placeholder="Contraseña" maxlength="50">
                            </div> -->

                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                                <input type="password" class="form-control" id="Clave" name="LoginForm[password]" placeholder="Contraseña" maxlength="50">
                                <div class="input-group-addon"><i class="fa fa-eye-slash" aria-hidden="true"></i></div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <!-- <a href="#" class="pull-left btn btn-link" style="padding-left:0">¿Olvidó su contraseña?</a> -->
                    <div class="pull-right">
                        <button id="btnLogin" class="btn btn-success"><i class="fa fa-sign-in"></i>&nbsp;&nbsp;Iniciar Sesión</button>
                    </div>
                </div>
            <?php ActiveForm::end(); ?>
            <br>
            <div class="row">
                <div class="col-sm-12">
                    <div class="alert alert-warning" style="text-align:justify">
                        <strong>NOTA:</strong> El sistema es solo válido para los proyectos del PIP2.
                    </div>
                </div>
            </div>
        </div>
        <div id="modal-comunicado" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <img class="img-responsive" src="<?= \Yii::$app->request->BaseUrl ?>/recursos/img/cierreplazo.jpg">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if (Yii::$app->session->hasFlash('usuarioincorrecto')): ?>
<script>
    $('.alert').show();
</script>
<?php endif; ?>

<script>
    $('#btnLogin').click(function (e) {
        e.preventDefault();
        usuario=$('#Usuario').val();
        clave=$('#Clave').val();
        if ($.trim(usuario)=='' || $.trim(clave)=='') {
            $('.alert').show();
            return false;
        }
        else
        {
            $(this).text('...Iniciando Sesión');
            //$(this).attr('disabled', 'disabled');    
        }
        $('#w0').submit();
    });
</script>