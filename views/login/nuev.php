<?php
  use yii\helpers\Html;
  use yii\bootstrap\ActiveForm;
  $action = Yii::$app->getUrlManager()->createUrl('login/by-pass');
  $form = ActiveForm::begin(
        [
             'action'            => $action,
             'options'           => [
                 'autocomplete'  => 'off',
                 'id'            => 'inicia'
             ]
        ]
  );
?>
  <input type="hidden" name="LoginForm[username]" value="<?php echo $usuario ?>">
  <input type="hidden" name="LoginForm[password]" value="<?php echo $password ?>">
  <input type="hidden" name="url"  value="<?php echo $url ?>">
<?php ActiveForm::end(); ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
<script>
  $(document).on('ready readyAgain', function() {
    $('#inicia').submit();
  });
  $(document).trigger('readyAgain');
</script>
