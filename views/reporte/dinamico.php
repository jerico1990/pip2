<!--<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>-->
<script src="https://cdn.flexmonster.com/2.3/flexmonster.js"></script>

<div id="page-content" >
    <div id="wrap">
        <div id="page-heading">
            <h1>Información general</h1>
        </div>
        <div class="container">
	    <div class="panel panel-primary">
		<div class="panel-body">
		    <div class="form-horizontal">
			<div id="pivot-container"></div>
		    </div>
		</div>
	    </div>
	</div>
    </div>
</div>
<script type="text/javascript">
$("#pivot-container").flexmonster({
    componentFolder: "",
    licenseKey: "Z7WP-XI8A2U-191E1A-2E5F36-5A6H1U-5L3X19-3U565U-4H2G06-0H5R57-5T17",
    width: "100%",
    height: 800,
    toolbar: true,
    global: {
        localization: "loc/es.json"
    },
    report: {
        dataSource: {
            dataSourceType: "json",
            filename: 'estructura-dinamico',
        },
        "slice": {
            "rows": [ 
            {
                "uniqueName": "ProgramaTransversal"
            }],

            "columns": [
            {
                "uniqueName": "ProgramaTransversal"
            }],

            "measures": [{
                "uniqueName": "MontoPNIA"
            },
            {
                "uniqueName": "Cantidad"
            }],
        },
        options: {
            viewType: "grid",
            configuratorActive: false,
            configuratorButton: false,
            configuratorMatchHeight: false,
            showAggregations: false,
            editing: false,
            showDrillThroughConfigurator: false,
            showOutdatedDataAlert: false
        },
        formats: [{
          name: "Cantidad",
          currencySymbol: "$",
          currencySymbolAlign: "left",
          thousandsSeparator: ",",
          decimalPlaces: 2
        }],
    }
});

</script>

<style type="text/css">
    
    #fm-tab-connect,#fm-tab-open,#fm-tab-save,#fm-tab-options,#fm-tab-format,#fm-tab-export-image,#fm-tab-export-csv,#fm-tab-export-html,#fm-tab-charts-bar-horizontal,#fm-tab-charts-line,#fm-tab-charts-scatter,#fm-tab-charts-bar-stack,#fm-tab-charts-bar-line,#fm-tab-charts-multiple {
        display: none !important;
    }

</style>