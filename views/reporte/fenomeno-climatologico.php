
<div id="page-content" >
    <div id="wrap">
        <div id="page-heading">
            <h1>Fenómenos climatológicos</h1>
        </div>
        <div class="container">
	    <div class="panel panel-primary">
		<div class="panel-body">
		    <div class="form-horizontal">
			<div class="table-responsive">
			    <table class="table table-bordered">
				<thead>
				    <tr>
					<th class="text-center"><small>Tipo Impacto</small></th>
					<th><small>Impacto</small></th>
					<th width="15%" class="text-center"><small>N° de Número de daños reportados</small></th>
					<th align="center">#</th>
				    </tr>
				</thead>
				<tbody>
				    <?php foreach($climatologicos as $climatologico){?>
				    <tr>
					<td align="center"><?= $climatologico["ID"] ?></td>
					<td><?= $climatologico["Nomb"] ?></td>
					<td align="center"><?= $climatologico["cantidad"] ?></td>
					<td class="text-center">
					    <a href="#" class="open-Modal btn btn-default btn-sm btn-proyecto" data-toggle="modal" data-target="#myModal" data-tipo-impacto="<?= $climatologico["ID"] ?>">
						<i class="fa fa-search-plus"></i>
					    </a>
					</td>
				    </tr>
				    <?php } ?>
				</tbody>
			    </table>
			    <table class="table table-bordered">
				<thead>
				    <tr>
					<th class="text-center"><small>Unidad Operativa</small></th>
					<th><small>Nro total de Proyectos</small></th>
					<th width="15%" class="text-center"><small>Nro de Proyecto afectados</small></th>
					<th align="center">%</th>
				    </tr>
				</thead>
				<tbody>
				    <?php foreach($unidadesEjecutorias as $unidadeEjecutoria){?>
				    <?php $var=(($unidadeEjecutoria["afectados"]*100)/$unidadeEjecutoria["cantidad"]); ?>
				    <?php $semaforo='black'; ?>
				    
				    <?php if($var>25 and $var<=50){ $semaforo='orange'; }?>
				    <?php if($var>50 and $var<=100){ $semaforo='red'; }?>
				    <tr>
					<td align="center"><?= $unidadeEjecutoria["Nombre"] ?></td>
					<td><?= $unidadeEjecutoria["cantidad"] ?></td>
					<td align="center"><?= $unidadeEjecutoria["afectados"] ?></td>
					<td class="text-center" style="color: <?= $semaforo?>">
					    <?= number_format($var, 2, '.', ' ') ?>
					</td>
				    </tr>
				    <?php } ?>
				</tbody>
			    </table>
			</div>
		    </div>
		</div>
	    </div>
	</div>
    </div>
</div>
<div class="modal fade" id="modal-ctrl-fenomeno" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
	<div class="modal-content">
	    <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
		
	    </div>
	    <div class="modal-body-main">
		<div class="table-responsive">
		    <div class="cuerpo"></div>
		</div>
	    </div>
	</div>
    </div>
</div>
<script>
    $('body').on('click', '.btn-proyecto', function (e) {
	e.preventDefault();
	var tipoimpacto=$(this).attr('data-tipo-impacto');
	$.ajax({
            url:'../reporte/fenomeno-climatologico-r1',
            async:false,
	    type:'GET',
            data:{TipoImpactoID:tipoimpacto},
            beforeSend:function()
            {
                
            },
            success:function(result)
            {
                    
                $(".cuerpo").html(result);
            },
            error:function(){
                alert('Error al realizar el proceso de busqueda.');
            }
        });
	
	$('#modal-ctrl-fenomeno').modal('show');
    });
</script>