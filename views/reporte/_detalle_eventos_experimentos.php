

  
    
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>

<link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css" rel="stylesheet">
    
    
<div class="container">
    <div class="table-responsive">
        <table class="table table-striped display table-bordered dt-responsive nowrap" cellspacing="0" width="100%" id="listado">
            <thead >
                <tr class="tr-header">
                    <th>Código Proyecto</th>
                    <th>Título Proyecto</th>
                    <th>Eventos</th>
                    <th>Experimentos</th>
                    <!-- <th>Razon Social</th> -->
                </tr>
            </thead>
            <tbody id="table_indica_obj">
                <?php $i=1; ?>
                <?php foreach ($eventos_experimentos as $evento_experimento): ?>
                    <tr>
                        <td><?php echo $evento_experimento['Codigo'] ?></td>
                        <td><?php echo substr($evento_experimento['TituloProyecto'],0,80) ?></td>
                        <td><?php echo $evento_experimento['eventos'] ?></td>
                        <td><?php echo $evento_experimento['experimentos'] ?></td>
                    </tr>
                <?php $i++; ?>
                <?php endforeach ?>
            </tbody>
        </table>
        <br>
        <a href="<?= Yii::$app->getUrlManager()->createUrl('reporte/descargar-detalle-eventos-experimentos?id='.$id.'&tipo='.$tipo) ?>" class="btn btn-primary">Descargar </a><br><br>
    </div>
</div>



<script type="text/javascript">
    $(document).ready(function() {
        $('#listado').DataTable();
    } );
</script>
