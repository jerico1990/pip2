 
      
    <link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/elecciones/css/styles.css"/>  
   
<section class="main-content">
    <div class="content-stats">
        <h1 class="main-title"></h1>
        <p class="data-source"></p>
        <p><strong>Selecciona las porciones del gráfico correspondiente a cada partido político.</strong></p>
        <div class="box-map">
            <div id="mapPeru" data-config="{'url':'<?= \Yii::$app->request->BaseUrl ?>/elecciones/data.json','type':'presidencial'}"></div>
        </div>
        <div class="box-positions"></div>
        <div class="clear"></div>
    </div>
</section>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <!-- Librería JS Digital Analytix-->
    <script language="JavaScript1.3" src="http://b.scorecardresearch.com/c2/6906602/ct.js"></script>
    <!-- END Librería JS Digital Analytix-->
    <script src="<?= \Yii::$app->request->BaseUrl ?>/elecciones/js/highmaps.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/elecciones/js/mapPE.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/elecciones/js/circle-progress.js"></script>
    <script src="<?= \Yii::$app->request->BaseUrl ?>/elecciones/js/flash-electoral.js"></script>