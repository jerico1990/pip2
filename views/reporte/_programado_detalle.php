
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<div id="container-pie"></div>


<script type="text/javascript">

$(document).ready(function () {

    // Build the chart
    Highcharts.chart('container-pie', {
        chart: {
            type: 'pie'
        },
        title: {
            text: 'Consolidado de programa'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [
            <?php foreach ($programa as $prog): ?>
                {
                    name: "<?php echo $prog['Nombre'].': Total ('.$prog['Total'].')' ?>",
                    y: <?php echo $prog['Total'] ?>
                }, 
            <?php endforeach ?>
            ]
        }]
    });
});

</script>



<hr>
<div class="container">
    <table class="table table-hover tbl-act">
        <thead >
            <tr class="tr-header">
                <th>Programa</th>
                <th>Proyecto</th>
                <th>Cultivo</th>
                <th>Monto PNIA (S/)</th>
                <!-- <th>Razon Social</th> -->
            </tr>
        </thead>
        <tbody id="table_indica_obj">
            <?php foreach ($estacion as $estac): ?>
                <tr>
                    <td><?php echo $estac['Nombre'] ?></td>
                    <td><?php echo $estac['Codigo'] ?></td>
                    <td><?php echo $estac['Cultivo'] ?></td>
                    <td class="input-number"><?php echo $estac['AporteMonetario'] ?></td>
                    <!-- <td><?php echo $estac['RazonSocial'] ?></td> -->
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>                    
</div>



<script type="text/javascript">
    // validarNumeros();
</script>