<script src="//code.highcharts.com/highcharts.js"></script> 
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<div id="page-content" style="min-height: 1342px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Consolidado</h1>
        </div>
        <div class="container">
            <style>
                .div-label-left b {
                    margin-bottom: 10px;
                    font-size: 18px;
                }
                .count-message {
                    color: #2460AA;
                    margin-top: 5px;
                }
            </style>
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <div class="form-horizontal">
                            <div class="form-group div-label-left row-border">
                                <div class="col-sm-6">
                                    <!-- <b>Proyecto </b> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <!-- <div class="col-sm-4">
                                    <label class="control-label">Rubro Elegible <span class="f_req">*</span></label>
                                    <select class="form-control">
                                        <option>[SELECCIONE]</option>
                                        <?php foreach ($rubro as $value): ?>
                                            <option value="<?php echo $value->ID ?>"><?php echo $value->Nombre ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div> -->
                                <div class="col-sm-4">
                                    <label class="control-label">Consolidado <span class="f_req">*</span></label>
                                    <select class="form-control consolidado">
                                        <option value="0">[SELECCIONE]</option>
                                        <option value="1">Total</option>
                                        <option value="2">Proyecto</option>
                                    </select>
                                </div>
                                <div class="col-sm-4" id="proy1" style="display: none;">
                                    <label class="control-label">Proyectos <span class="f_req">*</span></label>
                                    <select class="form-control proyectos" name="proyectos" id="proyectos">
                                        <option>[SELECCIONE]</option>
                                        <?php foreach ($users as $user): ?>
                                            <option value="<?php echo $user->ID ?>"><?php echo $user->username ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="col-sm-4" id="cola1" style="display: none;">
                                    <label class="control-label">Colaborador <span class="f_req">*</span></label>
                                    <select class="form-control" id="colaborador">
                                        <option>[SELECCIONE]</option>
                                        <!-- <?php foreach ($entidad as $ent): ?>
                                            <option value="<?php echo $ent->ID ?>"><?php echo $ent->RazonSocial ?></option>
                                        <?php endforeach ?> -->
                                    </select>
                                </div>

                                <div class="col-sm-12" style="margin-top: 10px;">
                                    <label class="control-label"></label>
                                    <input type="submit" class="btn btn-primary" name="filtrar" id="filtrar">
                                </div>

                            </div>
                        </div>

                        <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

                        <table id="datatable" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Rubro Elegible</th>
                                    <th>Inversión S/.</th>
                                </tr>
                            </thead>
                            <tbody class="datatablex">
                                <?php foreach ($totalRubro as $rub): ?>
                                    <tr>
                                        <th><?php echo $rub['Nombre'] ?></th>
                                        <td><?php echo $rub['total'] ?></td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>


                    </div>
                </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    
    $('.consolidado').change(function(){
        var id = $(this).val();
        if(id == 1 || id == 0){
            $('#proy1').css('display','none')
            $('#cola1').css('display','none')
        }else if(id == 2){
            $('#proy1').css('display','block')
            $('#cola1').css('display','block')
        }
    });

    $('.proyectos').change(function(){
        var id = $(this).val();
        
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: 'proyecto-entidades',
            type: 'POST',
            async: false,
            data: {proyectos:id,_csrf: toke},
            success: function(data){
                // console.log(data);
               $( "#colaborador" ).html( data );
            }
        });
    });
    

    $(document).on('ready',function(){
        draw_chart_one();
    });

    var draw_chart_one = function(){

        var chart = new Highcharts.chart('container', {
            data: {
                table: 'datatable'
            },
            chart: {
                type: 'column'
            },
            title: {
                text: 'Consolidado de información'
            },
            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Total'
                }
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        this.point.y + ' ' + this.point.name.toLowerCase();
                }
            }
        });
    }


    $(document).on('click','#filtrar',function(){
        filtrar_reporte_fechas();
    });

    var filtrar_reporte_fechas = function(){
        var _proy  = $('#proyectos').val();
        var _cola = $('#colaborador').val();

        $.ajax({
            url: 'consolidado-bid-filtro?proyectos='+_proy+'&colaborador='+_cola,
            type: 'GET',
            success: function(data){
                // console.log(data);
               $( ".datatablex" ).html('');
               $( ".datatablex" ).html( data );
               draw_chart_one();
            }
        });

        // Envia perticion JSON
        // var _graphics = $.getJSON( 'consolidado-bid-filtro?proyectos='+_proy+'&colaborador='+_cola , function() {  
        //     $('#grafico-uno').fadeIn();
        // })
        // .done(function( json ) {   
        //     console.log(json);
        //     // Muestra los datos en la grafica
        //     draw_chart_one();
        // })
        // .fail(function( _graphics, textStatus, error ) { 
        //     console.log(_graphics);
        //     console.log(textStatus);
        //     console.log(error);
        //     // accion ante un error
        // });

    }


</script>