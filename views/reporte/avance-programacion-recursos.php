<?php
use app\models\Actividad;
use app\models\ActRubroElegible;
use app\models\CronogramaAreSubCategoria;
use app\models\CronogramaProyecto;
use app\models\MarcoLogicoActividad;


/** Error reporting */
error_reporting(E_ALL);

/** Include path **/
ini_set('include_path', ini_get('include_path').';../web/PHPExcel/Classes/');
//require(__DIR__ . '/../../web/PHPExcel/Classes/');
/** PHPExcel */
include 'PHPExcel.php';

/** PHPExcel_Writer_Excel2007 */
include 'PHPExcel/Writer/Excel2007.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
$sheet = $objPHPExcel->getActiveSheet();

$sheet->setTitle('POA');

$Meses=[];
$objPHPExcel->setActiveSheetIndex(0);

$sheet->SetCellValue('A1', 'Codigo');
$sheet->SetCellValue('B1', 'Especifica');
$sheet->SetCellValue('C1', 'Detalle especifica');
$sheet->SetCellValue('D1', 'Código matriz');
$sheet->SetCellValue('E1', 'Total');
$sheet->SetCellValue('F1', 'Terminado');
$sheet->SetCellValue('G1', 'Falta');


$i=2;
foreach($recursos as $recurso)
{
   
    $sheet->SetCellValue('A'.$i, ''.$recurso['Codigo'].'');
    $sheet->SetCellValue('B'.$i, ''.$recurso['Nombre'].'');
    $sheet->SetCellValue('C'.$i, ''.$recurso['Especifica'].'');
    $sheet->SetCellValue('D'.$i, ''.$recurso['RubroElegibleID'].'.'.$recurso['Codificacion'].'');
    $sheet->SetCellValue('E'.$i, ''.$recurso['Total'].'');
    $sheet->SetCellValue('F'.$i, ''.$recurso['Terminado'].'');
    $sheet->SetCellValue('G'.$i, ''.$recurso['Falta'].'');
    $i++;
}




// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="AvanceProgramadoRecurso.xls"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
