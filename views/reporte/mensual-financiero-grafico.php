<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay_progress.min.js"></script>
<script src=" //cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<div id="page-content" >
    <div id="wrap">
        <div id="page-heading">
            <h1>Mensual Financiero</h1>
        </div>
        <div class="container">
	    <div class="panel panel-primary">
		<div class="panel-body">
		    <div class="form-horizontal">
			<div class="form-group">
			    <label class="col-sm-2 control-label">Estación experimental:<span class="f_req">*</span></label>
			    <div class="col-sm-8">
				<select  class="form-control" id="estacion-experimental-id" name="Reporte[EstacionExperimentalID]">
				    <option value>Seleccionar</option>
				    <?php foreach($estaciones as $estacion){ ?>
				    <option value="<?= $estacion->ID ?>"><?= $estacion->Nombre ?></option>
				    <?php } ?>
				</select>
			    </div>
			</div>
		    </div>
		</div>
		<br>
		<div class="panel-body">
		    <div class="form-horizontal">
			<div id="container-mensual-financiero"></div>
		    </div>
		</div>
		<br>
		<br>
		<div class="panel-body">
			<div id="dvData2"><i class="loading fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div>
			<!-- <a href="descargar-mensual-financiero" class="btn btn-primary" id="btnExport" style="margin-bottom: 10px;">Exportar Excel</a> -->
			<a href="#" class="btn btn-primary" id="btnExport2" style="margin-bottom: 10px;">Exportar Excel</a>
		    <div class="form-horizontal">
			<div class="table-responsive">
			

				<div id="dvData">
				    <table class="estaciones display table table-bordered table-striped table-condensed table-hover" cellspacing="0" >
						<thead>
						    <tr>
							<th>Estación</th>
							<?php for($i=1;$i<37;$i++){ ?>
							<th><?= Yii::$app->tools->DescripcionMes($i) ?></th>
							<?php }?>
							<th>Total</th>
						    </tr>
						</thead>
						<tbody></tbody>
				    </table>
				</div>

			</div>
		    </div>
		</div>
	    </div>
	</div>
    </div>
</div>


<script type="text/javascript">

	$("#btnExport2").click(function (e) {
	    // window.open('data:application/vnd.ms-excel,' + $('#dvData').html());
	     window.open('data:application/vnd.ms-excel,' + encodeURIComponent($('#dvData').html()));
	    e.preventDefault();
	});

    $(document).ready(function() {
    var configDTjs={
	"paging": false,
        "order": [[ 1, "desc" ]],
            "language": {
                "sProcessing":    "Procesando...",
                "sLengthMenu":    "Mostrar _MENU_ registros",
                "sZeroRecords":   "No se encontraron resultados",
                "sEmptyTable":    "Ningún dato disponible en esta lista",
                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":   "",
                "sSearch":        "Buscar:",
                "sUrl":           "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":    "Último",
                    "sNext":    "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
    };
    
    function Grilla(estacion) {
		$.ajax({
            url:'mensual-financiero-grilla-json',
            data:{estacion:estacion},
            beforeSend:function()
            {
            	$("#dvData2").show();
            	$(".estaciones tbody").html('');
                $('.panel-default').LoadingOverlay("show");
            },
            success:function(result)
            {	    //$("#estaciones").destroy();
            	$("#dvData2").hide();
				$(".estaciones tbody").html(result);
				$(".estaciones").destroy();
				$('.estaciones').DataTable(configDTjs);
				$('.panel-default').LoadingOverlay("hide",true);
            },
            error:function(){
                $('.panel-default').LoadingOverlay("hide",true);
                alert('Error al realizar el proceso de busqueda.');
            }
        });
    }
    function Barras(estacion) 
    {
		$.getJSON('mensual-financiero-grafico-json',{estacion:estacion}, function(data) {
			Highcharts.chart('container-mensual-financiero', {
			    chart: {
				type: 'column'
			    },
			    title: {
				text: 'Reporte mensual financiero'
			    },
			    subtitle: {
				text: 'Fuente: PIP2'
			    },
			    xAxis: {
				type: 'category',
				labels: {
				    rotation: -45,
				    style: {
					fontSize: '13px',
					fontFamily: 'Verdana, sans-serif'
				    }
				}
			    },
			    yAxis: {
				min: 0,
				title: {
				    text: 'S/. (miles)'
				}
			    },
			    legend: {
				enabled: false
			    },
			    tooltip: {
				pointFormat: '<b>{point.y:.1f} Soles</b>'
			    },
			    series: [{
				name: 'Population',
				data: data,
				dataLabels: {
				    enabled: true,
				    rotation: -90,
				    color: '#FFFFFF',
				    align: 'right',
				    format: '{point.y:.1f}', // one decimal
				    y: 10, // 10 pixels down from the top
				    style: {
					fontSize: '13px',
					fontFamily: 'Verdana, sans-serif'
				    }
				}
			    }]
			});
		    
		});
    }
    var estacion=null;
    Barras(estacion);
    Grilla(estacion);

$('body').on('change', '#estacion-experimental-id', function (e) {
    e.preventDefault();
    Barras($(this).val());
    Grilla($(this).val());
});
    });
</script>



<style type="text/css">
	#dvData2{
		background: rgb(244, 244, 244);
	    position: absolute;
	    width: 77%;
	    height: 20%;
	}
	.loading{
	    text-align: center;
	    margin-left: 45%;
	    font-size: 40px;
	    margin-top: 5%;
	}
</style>