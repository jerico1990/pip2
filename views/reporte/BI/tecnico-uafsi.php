<div id="page-content" >
    <div id="wrap">
        <div id="page-heading">
            <h1>Reporte BI</h1>
        </div>
        <div class="container">
	    <div class="panel panel-primary">
		<div class="panel-body">
			<!-- <div class="video">
			    <iframe  src="https://app.powerbi.com/view?r=eyJrIjoiNTVkYmI0YTItZTQ2Ny00YzA2LWFlNDctODZlNjNiM2Q3MGY5IiwidCI6IjE2N2M0ZGUyLWMzMDItNGU4ZS1hODU0LTEyYjcwZmRhMjJlZSIsImMiOjR9"  allowfullscreen></iframe>
			</div> -->


			<div id="frame" class="embed-responsive embed-responsive-16by9">
			    <iframe style="width: 100%;height: 640px" src="https://app.powerbi.com/view?r=eyJrIjoiNTVkYmI0YTItZTQ2Ny00YzA2LWFlNDctODZlNjNiM2Q3MGY5IiwidCI6IjE2N2M0ZGUyLWMzMDItNGU4ZS1hODU0LTEyYjcwZmRhMjJlZSIsImMiOjR9" class="embed-responsive-item" frameborder="0" allowfullscreen></iframe>
			</div>

		</div>
	    </div>
	</div>
    </div>
</div>

<style type="text/css">
	
	.frame {
	   position: relative;
	   /*padding-bottom: 56.25%;*/
	   overflow: hidden;
	}

	.frame iframe
	 {
	    position: absolute;
	    display: block;
	    top: 0;
	    left: 0;
	    width: 100%;
	    height: 100%;
	}



</style>