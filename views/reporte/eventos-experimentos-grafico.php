
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>


<div id="page-content" >
    <div id="wrap">
        <div id="page-heading">
            <h1>Experimentos y eventos</h1>
        </div>
        <div class="container">
		    <div class="panel panel-primary">
				<div class="panel-body">
				    <div class="form-horizontal">
					<div id="container-pie"></div>
				    </div>
				</div>

				<hr>
				<div class="container">
					<table class="table table-hover tbl-act">
					    <thead >
					    	<tr class="tr-header">
						    <th>Estación Experimental</th>
						    <th>Eventos</th>
						    <th>Experimentos</th>
						    <th>N° Proyectos</th>
						    <th>Detalle</th>
					    	</tr>
					    </thead>
					    <tbody id="table_indica_obj">
					        <?php $eve=0;$exp=0;$pro=0; foreach ($estacion as $estac): ?>
						    <tr>
							<td><?php echo $estac['Estacion'] ?></td>
							<td class="text-center"><?php echo $estac['Evento'] ?></td>
							<td class="text-center"><?php echo $estac['Experimento'] ?></td>
							<td class="text-center"><?php echo $estac['CantProyectos'] ?></td>
							<td> 
							    <a href="#" class="ver_detalle" data-id="<?php echo $estac['ID'] ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
							</td>
						    </tr>
						    <?php $pro=$pro+$estac["CantProyectos"]; ?>
						    <?php $eve=$eve+$estac["Evento"]; ?>
						    <?php $exp=$exp+$estac["Experimento"]; ?>
					        <?php endforeach ?>
						    <tr>
							<td></td>
							<td class="text-center"><?php echo $eve ?></td>
							<td class="text-center"><?php echo $exp ?></td>
							<td class="text-center"><?php echo $pro ?></td>
							<td><a href="#" class="ver_detalle_total"><i class="fa fa-eye" aria-hidden="true"></i></td>
						    </tr>
					    </tbody>
					</table>					
				</div>


		    </div>
		</div>
    </div>
</div>


<script type="text/javascript">

$(document).ready(function () {

    // Build the chart
    Highcharts.chart('container-pie', {
        chart: {
            type: 'pie'
        },
        title: {
            text: 'Consolidado'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [
            <?php foreach ($total as $tot): ?>
                {
                    name: "Eventos <?php echo ': Total ('.$tot['Evento'].')' ?>",
                    y: <?php echo $tot['Evento'] ?>
                }, 
                {
                    name: "Experimentos <?php echo ': Total ('.$tot['Experimento'].')' ?>",
                    y: <?php echo $tot['Experimento'] ?>
                }
            <?php endforeach ?>
            ]
        }]
    });
});

</script>
<div class="modal fade" id="modal-detalle-reporte" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Detalle</h4>
		
            </div>
            <div class="modal-body-main">
		
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('body').on('click', '.ver_detalle', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        $('#modal-detalle-reporte .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/reporte/detalle-eventos-experimentos?id='+id+'&tipo=1');
        $('#modal-detalle-reporte').modal('show');
    });
    
    $('body').on('click', '.ver_detalle_total', function (e) {
        e.preventDefault();
        $('#modal-detalle-reporte .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/reporte/detalle-eventos-experimentos?id=&tipo=2');
        $('#modal-detalle-reporte').modal('show');
    });
</script>

