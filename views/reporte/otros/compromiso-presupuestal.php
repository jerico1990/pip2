<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>


<div id="page-content" >
    <div id="wrap">
        <div id="page-heading">
            <h1>Compromiso Presupuestal</h1>
        </div>
        <div class="container">
	    <div class="panel panel-primary">
			<div class="panel-body">
				<div id="reporte-compromiso"></div>
			</div>
	    </div>
	</div>
    </div>
</div>

<style type="text/css">
	
	.frame {
	   position: relative;
	   /*padding-bottom: 56.25%;*/
	   overflow: hidden;
	}

	.frame iframe
	 {
	    position: absolute;
	    display: block;
	    top: 0;
	    left: 0;
	    width: 100%;
	    height: 100%;
	}

	#reporte-compromiso{
		height: 300px;
	}

</style>

<script type="text/javascript">
	Highcharts.chart('reporte-compromiso', {
	    chart: {
	        type: 'bar',
	        // height: '100%' // 16:9 ratio
	    },
	    title: {
	        text: 'Seguimiento de Compromiso Presupuestal'
	    },
	    // subtitle: {
	    //     text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
	    // },
	    xAxis: {
	        categories: [
	        <?php foreach ($seguimiento as $for){
	        	echo "'".$for['Codigo']."',";
	        }
	        ?>
	        ],
	        title: {
	            text: null
	        }
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Avance ',
	            align: 'high'
	        },
	        labels: {
	            overflow: 'justify'
	        }
	    },
	    // tooltip: {
	    //     valueSuffix: ' %'
	    // },
	    plotOptions: {
	        bar: {
	            dataLabels: {
	                enabled: true
	            }
	        }
	    },
	    legend: {
	        layout: 'vertical',
	        align: 'right',
	        verticalAlign: 'top',
	        x: -40,
	        y: 80,
	        floating: true,
	        borderWidth: 1,
	        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
	        shadow: true
	    },
	    credits: {
	        enabled: false
	    },
	    series: [{
	        name: 'Requerimiento',
	        data: [
	        <?php foreach ($seguimiento as $forxx){
	        	echo $forxx['Req'].",";
	        }
	        ?>
	        ]
	    }, {
	        name: 'Compromiso Anual',
	        data: [
	        <?php foreach ($seguimiento as $forx){
	        	echo $forx['Anual'].",";
	        }
	        ?>
	        ]
	    }, {
	        name: 'Compromiso Administrativo',
	        data: [
	        <?php foreach ($seguimiento as $forxs){
	        	echo $forxs['Admin'].",";
	        }
	        ?>
	        ]
	    }]
	});
</script>