<?php
use app\models\Actividad;
use app\models\ActRubroElegible;
use app\models\CronogramaAreSubCategoria;
use app\models\CronogramaProyecto;
use app\models\MarcoLogicoActividad;


/** Error reporting */
error_reporting(E_ALL);

/** Include path **/
ini_set('include_path', ini_get('include_path').';../web/PHPExcel/Classes/');
//require(__DIR__ . '/../../web/PHPExcel/Classes/');
/** PHPExcel */
include 'PHPExcel.php';

/** PHPExcel_Writer_Excel2007 */
include 'PHPExcel/Writer/Excel2007.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
$sheet = $objPHPExcel->getActiveSheet();

// print_r($unidadesoperativas);
// die();


$sheet->setTitle('Mensual Financiero');

$Meses=[];
$objPHPExcel->setActiveSheetIndex(0);

$sheet->setCellValueByColumnAndRow(0, 1, "Mensual Financiero");



$sheet->getRowDimension('1')->setRowHeight(20);




$sheet->SetCellValue('A3', 'Estacion');

for($ix=1;$ix<37;$ix++)
{
    array_push ( $Meses , Yii::$app->tools->DescripcionMes($ix) );
}
    array_push ( $Meses , 'Total Proyecto' );


$sheet->fromArray($Meses,null,'B3');

$highestColumm = $objPHPExcel->setActiveSheetIndex(0)->getHighestColumn();

// echo $highestColumm;
// die();

$sheet->getRowDimension('3')->setRowHeight(30);
$sheet->getStyle('A3:'.$highestColumm."3")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle('A3:'.$highestColumm."3")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);




$i=4;
foreach($unidadesoperativas as $unidad)
{

    $mes=1;
    $suma=0;
    $a=1;
    $tot[];
    $sheet->SetCellValue('A'.$i, ''.$unidad['Nombre'].'');
    for($i=0;$i<36;$i++)
    {
        $Total = (new \yii\db\Query())
            ->select('CronogramaAreSubCategoria.Mes,sum(CronogramaAreSubCategoria.MetaFisica*CronogramaAreSubCategoria.CostoUnitario) Total')
            ->from('CronogramaAreSubCategoria')
            ->innerJoin('AreSubCategoria','AreSubCategoria.ID=CronogramaAreSubCategoria.AreSubCategoriaID')
            ->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')
            ->innerJoin('Actividad','Actividad.ID=ActRubroElegible.ActividadID')
            ->innerJoin('Componente','Componente.ID=Actividad.ComponenteID')
            ->innerJoin('Poa','Poa.ID=Componente.PoaID')
            ->innerJoin('Proyecto','Proyecto.ID=Poa.ProyectoID')
            ->innerJoin('InformacionGeneral','InformacionGeneral.InvestigadorID=Proyecto.InvestigadorID')
            ->where('CronogramaAreSubCategoria.MetaFisica<>0 and InformacionGeneral.UnidadOperativaID=:UnidadOperativaID and CronogramaAreSubCategoria.Mes=:Mes',[':UnidadOperativaID'=>$unidad['ID'],':Mes'=>$mes])
            ->GroupBy('CronogramaAreSubCategoria.Mes')
            ->OrderBy('CronogramaAreSubCategoria.Mes asc, Total asc')
            ->one();

        if($Total && $mes==$Total['Mes'])
        {
            array_push ( $tot , $Total['Total'] );
            // echo '<td>'.$Total['Total'].'</td>';
            // $sheet->SetCellValue('B'.($a+$i), $Total['Total']);
        }
        else
        {
            array_push ( $tot , '' );
            // $sheet->SetCellValue('B'.($a+$i), '');
            // echo '<td></td>';
        }
        $mes++;
        $sheet->fromArray($tot,null,'B'.$i);
        $suma=$suma+$Total['Total'];
    }




    
    // End Perzonalizacion


    //$sheet = [['#','Objetivo Especifico '.$i.'']];
    //$objPHPExcel->setActiveSheetIndex($index);
    //$objPHPExcel->getActiveSheet()->fromArray($sheet,'A'.$i);
    //$index++;
    $i++;
}




// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="MensualFinanciero.xls"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;