<?php
use app\models\Actividad;
use app\models\ActRubroElegible;
use app\models\CronogramaAreSubCategoria;
use app\models\CronogramaProyecto;
use app\models\MarcoLogicoActividad;


/** Error reporting */
error_reporting(E_ALL);

/** Include path **/
ini_set('include_path', ini_get('include_path').';../web/PHPExcel/Classes/');
//require(__DIR__ . '/../../web/PHPExcel/Classes/');
/** PHPExcel */
include 'PHPExcel.php';

/** PHPExcel_Writer_Excel2007 */
include 'PHPExcel/Writer/Excel2007.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
$sheet = $objPHPExcel->getActiveSheet();

$sheet->setTitle('POA');

$Meses=[];
$objPHPExcel->setActiveSheetIndex(0);

$sheet->SetCellValue('A1', '#');
$sheet->SetCellValue('B1', 'Código');
$sheet->SetCellValue('C1', 'Título');
$sheet->SetCellValue('D1', 'Eventos');
$sheet->SetCellValue('E1', 'Experimentos');

$i=2;
foreach($eventos_experimentos as $evento_experimento)
{
   
    $sheet->SetCellValue('A'.$i, ($i-1));
    $sheet->SetCellValue('B'.$i, $evento_experimento['Codigo']);
    $sheet->SetCellValue('C'.$i, $evento_experimento['TituloProyecto']);
    $sheet->SetCellValue('D'.$i, $evento_experimento['eventos']);
    $sheet->SetCellValue('E'.$i, $evento_experimento['experimentos']);
    $i++;
}




// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="EventosExperimentos.xls"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
