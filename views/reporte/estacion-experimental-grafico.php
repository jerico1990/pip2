
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>

<script type="text/javascript">
    $(function () {
    // Create the chart
    Highcharts.chart('container-report', {
        chart: {
            type: 'pie'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: 'Fuente: Información del PIP2'
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '{point.name}: {point.y}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y} Subproyectos</b><br/>'
        },
        series: [{
            name: 'Sub Proyectos',
            colorByPoint: true,
            data: [
            <?php $i=1; foreach($estacion as $est): ?>
	            {
	                name: "<?php echo $est['Estacion'] ;?>",
	                y: <?php echo $est['Proyecto'];?>,
	                drilldown: 'Estacion<?php echo $i;?>'
	            }, 
            <?php $i++;endforeach; ?>
            ]
        }],
        drilldown: {
            series: [

            <?php 
            	$i=1;
            	foreach($cantUnidad as $unid): ?>
		            	{
		            	name: "<?php echo $unid['Estacion'];?>",
		                id: 'Estacion<?php echo $i;?>',
		                data: [
		                	<?php foreach ($unid['ProyectoCant'] as $proy) { ?>
		                    	["Proyectos - <?php echo substr($proy['Estacion'],0,40); ?>", <?php echo $proy['Proyectos']; ?>],
		                	<?php } ?>
		                ]
		            },
            <?php $i++;
            	endforeach; 
            ?>

            ]
        }
    });
});

    
</script>




<div id="page-content" >
    <div id="wrap">
        <div id="page-heading">
            <h1>Estación Experimental</h1>
        </div>
        <div class="container">
		    <div class="panel panel-primary">
				<div class="panel-body">
				    <div class="form-horizontal">
						<div id="container-report"></div>
				    </div>
				</div>

				<hr>
				<div class="container">
					<table class="table table-hover tbl-act">
					    <thead >
					    	<tr class="tr-header">
								<th>Estación Experimental</th>
						        <th>Nº Proyectos</th>
						        <th>Detalle</th>
					    	</tr>
					    </thead>
					    <tbody id="table_indica_obj">
					        <?php foreach ($estacion as $estac): ?>
					        	<tr>
								    <td><?php echo $estac['Estacion'] ?></td>
								    <td class="text-center"><?php echo $estac['Proyecto'] ?></td>
								    <td> <a href="#" class="ver_detalle" data-id="<?php echo $estac['ID'] ?>"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
					        	</tr>
					        <?php endforeach ?>
					    </tbody>
					</table>					
				</div>
		    </div>
		</div>
    </div>
</div>


<div class="modal fade" id="modal-detalle-reporte" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Detalle</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
	$('body').on('click', '.ver_detalle', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        $('#modal-detalle-reporte .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/reporte/detalle-programado?id='+id);
        $('#modal-detalle-reporte').modal('show');
    });
</script>