<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'termino-referencia/crear?CodigoProyecto='.$CodigoProyecto,
        'options'           => [
            'enctype'       =>'multipart/form-data',
            'id'            => 'frmTerminoReferencia',
            
        ]
    ]
); ?>
    <input type="hidden" name="TerminoReferencia[CodigoProyecto]" value="<?php echo $CodigoProyecto ?>">
    <div class="modal-body">
        <div class="form-horizontal">
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Tipo de Servicio:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <select class="form-control" name="TerminoReferencia[TipoServicio]">
                        <option>[SELECCIONE]</option>
                        <option value=1>Términos de referencias para consultoria</option>
                        <option value=2>Términos de referencias para no consultoria</option>
                        <option value=3>Especificaciones Técnicas</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Monto Total:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="TerminoReferencia[MontoTotal]" id="terminoreferencia-montototal">
                </div>
            </div>
            <!--
            <div class="form-group">
                <label class="col-sm-3 control-label">Duración:(en días)<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="TerminoReferencia[Duracion]" id="terminoreferencia-duracion">
                </div>
            </div>
            -->
            <div class="form-group">
                <label class="col-sm-3 control-label">Objetivo:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <select class="form-control" name="TerminoReferencia[ComponenteID]" id="terminoreferencia-componenteid" onchange="Actividad($(this).val())">
                        <option>[SELECCIONE]</option>
                        <?php foreach($componentes as $componente){ ?>
                            <option value="<?= $componente->ID ?>"><?= $componente->Nombre ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Actividad:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <select class="form-control" name="TerminoReferencia[ActividadID]" id="terminoreferencia-actividadid" onchange="Recurso($(this).val())">
                        
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Recurso:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <select class="form-control" name="TerminoReferencia[AreSubCategoriaID]" id="terminoreferencia-areasubcategoriaid">
                    </select>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Documento:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input class="form-control" type="file" name="TerminoReferencia[archivo]" id="terminoreferencia-archivo">
                </div>
            </div>
            
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>
<?php
    $actividades=Yii::$app->getUrlManager()->createUrl('termino-referencia/actividades');
    $recursos=Yii::$app->getUrlManager()->createUrl('termino-referencia/recursos');
?>
<script>
    var $form = $('#frmRequerimientos');
    var formParsleyRequerimientos = $form.parsley(defaultParsleyForm());
    function Actividad(valor) {
        var ComponenteID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $actividades ?>',
            type: 'POST',
            async: false,
            data: {ComponenteID:ComponenteID,_csrf: toke},
            success: function(data){
               $( "#terminoreferencia-actividadid" ).html( data );
            }
        });
    }
    
    function Recurso(valor) {
        var ActividadID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $recursos ?>',
            type: 'POST',
            async: false,
            data: {ActividadID:ActividadID,_csrf: toke},
            success: function(data){
               $( "#terminoreferencia-areasubcategoriaid" ).html( data );
            }
        });
    }
</script>