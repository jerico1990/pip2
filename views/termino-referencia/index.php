
<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay_progress.min.js"></script>
<script src=" //cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>


<div id="page-content" style="min-height: 1342px;">
    
    <div id="wrap">
        <div id="page-heading">
            <h1>Términos de referencia</h1>
        </div>
        
           
        <div class="container">
             <div class="form-group">
                <button class="btn btn-crear-termino" data-codigo-proyecto="<?= $CodigoProyecto ?>">Generar Término de Referencia</button>
                <a class="btn " href="documentos/1_MODELO_ESPECIFICACIONES_TECNICAS.docx">Plantilla de especificaciones técnicas</a>
                <a class="btn " href="documentos/1_MODELO_TR_CONSULTORIA.docx">Plantilla de T.R. Consultoria</a>
                <a class="btn " href="documentos/1_MODELO_TR_SERVICIOS_NO_CONSULTORIA.docx">Plantilla de T.R. No Consultoria</a>
            </div>
            <div class="table-responsive">
                <table id="proyectos" class="display table table-bordered table-striped table-condensed table-hover" cellspacing="0" >
                    <thead>
                        <tr>
                            <th>N° Término</th>
                            <th>Tipo de Servicio</th>
                            <th>Fecha Registro</th>
                            <th>Situación</th>
                            <th>Descargar</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-ctrl-termino" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Términos de referencia</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<script>
    function js_buscar()
    {
       
    }
    var tblresultjs;
        var configDTjs={
            "order": [[ 2, "desc" ]],
                "language": {
                    "sProcessing":    "Procesando...",
                    "sLengthMenu":    "Mostrar _MENU_ registros",
                    "sZeroRecords":   "No se encontraron resultados",
                    "sEmptyTable":    "Ningún dato disponible en esta lista",
                    "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":   "",
                    "sSearch":        "Buscar:",
                    "sUrl":           "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":    "Último",
                        "sNext":    "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
        };
    $(document).ready(function() {
        var CodigoProyecto="<?= $CodigoProyecto ?>";
        $.ajax({
            url:'termino-referencia/lista-terminos-referencias?CodigoProyecto='+CodigoProyecto,
            async:false,
            data:{},
            beforeSend:function()
            {
                $('.panel-default').LoadingOverlay("show");
            },
            success:function(result)
            {
                    //$tblresultjs.destroy();//
                    $("#proyectos tbody").html(result);
                    $('#proyectos').DataTable(configDTjs);
                    //tblresultjs=$('#example').DataTable(configDTjs);
                    $('.panel-default').LoadingOverlay("hide",true);
            },
            error:function(){
                $('.panel-default').LoadingOverlay("hide",true);
                alert('Error al realizar el proceso de busqueda.');
            }
        });
        
    } );
    
    $('body').on('click', '.btn-crear-termino', function (e) {
        e.preventDefault();
        var CodigoProyecto = $(this).attr('data-codigo-proyecto');
        $('#modal-ctrl-termino .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/termino-referencia/crear?CodigoProyecto='+CodigoProyecto);
        $('#modal-ctrl-termino').modal('show');
    });
    
    /*
    $('body').on('click', '#btn-guardar-requerimientos', function (e) {
        
        e.preventDefault();
        // alert('s');
        
        var $btn = $(this);
        $btn.attr('disabled', 'disabled');
        var isValid = formParsleyRequerimientos.validate();
        if (isValid) {
            sendFormNoMessage($('#frmRequerimientos'), $(this), function (msg) {
                
                $("#modal-ctrl-requerimiento").modal('hide');
                _pageLoadingEnd();
		
            });
        } else {
        }
    });
    */
    $('body').on('click', '.btn-edit-termino', function (e) {
        e.preventDefault();
        var id=$(this).attr('data-id');
        $('#modal-ctrl-termino .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/termino-referencia/actualizar?ID='+id);
        $('#modal-ctrl-termino').modal('show');
    });
    
    $('body').on('click', '.btn-remove', function (e) {
        e.preventDefault();
        var $this = $(this);
        bootboxConfirmEliminar($this,
        'Está seguro de eliminar el término de referencia?',
        function () {
            var id = $this.attr('data-id');
            var toke = '<?=Yii::$app->request->getCsrfToken()?>';
            $.get('<?= \Yii::$app->request->BaseUrl ?>/termino-referencia/eliminar', { id: id ,_csrf: toke }, function (data) {
                var jx = JSON.parse(data);
                if (jx.Success == true) {
                    
                }
            });
        });
    });
</script>