<div id="page-content" style="min-height: 754px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Relación de gastos definitivos: Registrar Nuevo</h1>
        </div>
        <div class="container">
            
            <!-- OBJETIVOS -->
            <style>
                .panel .panel-body {
                    padding: 10px !important;
                }

                .panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 1px solid #a8a8ab !important;
                }

                .panel-gray .panel-body {
                    border-top: none !important;
                }

                .btn-sm {
                    padding: 5px 10px !important;
                    line-height: 1.5 !important;
                }

                .input-comp-nombre {
                    text-transform: uppercase;
                }

                .tbl-act > tbody > .tr-header > td {
                    text-align: center;
                    font-size: 12px;
                    font-weight: bold;
                    padding: 5px !important;
                    vertical-align: middle !important;
                    /*border: 1px solid #cfcfd0;
                    background-color: #f0f0f1;*/
                    border: 1px solid #c0c0c0;
                    background-color: #e3e3e3;
                }

                /*.tbl-act .tr-item td {
                    padding: 2px !important;
                }*/
                input.form-control, select.form-control {
                    /*height: 28px !important;*/
                }

                .tbl-act .tr-act .input-nombre {
                    min-width: 320px !important;
                }

                .tbl-act .tr-act .input-unidadmedida {
                    min-width: 100px !important;
                }

                .input-number {
                    min-width: 92px !important;
                }

                .btn-add-re {
                    float: right;
                }

                /*.tbl-act > tbody > tr > td:nth-child(2) {
                    min-width: 40px !important;
                    text-align: center !important;
                }*/
                .text-left {
                    text-align: left !important;
                    font-weight: bold;
                }

                .tr-comp input, .tr-proy input {
                    font-weight: bold;
                }

                .th-proy {
                    min-width: 630px !important;
                }

            </style>

            <div class="panel panel-primary">
                <!-- <div class="panel-heading">
                    <h4>OBJETIVO</h4>
                </div> -->
                <div class="panel-body">
                    <div class="row">
                        <div class="container">

                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-1 control-label"><b>Actividad</b></label>
                                    <div class="col-sm-11">
                                        <select class="form-control">
                                            <option>[SELECCIONE]</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <h3>II. Presupuesto por rendir:</h3>
                            <table class="table table-bordered table-condensed tbl-act">
                                <tbody>
                                    <tr class="tr-header">
                                        <td class="text-center">Concepto</td>
                                        <td class="text-center">Total</td>
                                    </tr>
                                    <tr class="">
                                        <td class="text-left" style="width: 900px">Disponible</td>
                                        <td class="text-center"><input type="text" name="" class="form-control" disabled="disabled"></td>
                                    </tr>
                                    <tr class="">
                                        <td class="text-left" style="width: 900px">Rendido</td>
                                        <td class="text-center"><input type="text" name="" class="form-control" disabled="disabled"></td>
                                    </tr>
                                    <tr class="">
                                        <td class="text-left" style="width: 900px">Por rendir</td>
                                        <td class="text-center"><input type="text" name="" class="form-control" disabled="disabled"></td>
                                    </tr>
                                </tbody>
                            </table>



                            <div class="table-responsive" style="max-height: 800px;">
                                <table class="table table-bordered table-condensed tbl-act">
                                    <tbody>
                                        <tr class="tr-header">
                                            <td rowspan="2" class="text-center">Fecha</td>
                                            <td class="text-center" colspan="2">Referencia</td>
                                            <td class="text-center" colspan="2">Comprobante</td>
                                            <td class="text-center" colspan="2">Proveedor</td>
                                            <!-- <td rowspan="2" class="text-center">Codificación Segun Matriz</td> -->
                                            <td rowspan="2" class="text-center">Concepto</td>
                                            <td rowspan="2" class="text-center">Total</td>
                                        </tr>
                                        <tr class="tr-header">
                                            <td class="text-center">Tipo</td>
                                            <td class="text-center">Nro</td>

                                            <td class="text-center">Tipo</td>
                                            <td class="text-center">Nro</td>    

                                            <td class="text-center">RUC</td>
                                            <td class="text-center">Razón Social</td>

                                        </tr>

                                        <tr class="">
                                            <td class="text-left"><input type="date" class="form-control" name=""></td>
                                            <td class="text-center">
                                                <select class="form-control">
                                                    <option>[SELECCIONE]</option>
                                                </select>
                                            </td>
                                            <td class="text-center"><input type="text" class="form-control" name=""></td>
                                            <td class="text-center">
                                                <select class="form-control">
                                                    <option>[SELECCIONE]</option>
                                                </select>
                                            </td>
                                            <td class="text-center"><input type="text" class="form-control" name=""></td>
                                            <td class="text-center"><input type="text" class="form-control" name=""></td>
                                            <td class="text-center"><input type="text" class="form-control" name=""></td>
                                            <td class="text-center"><input type="text" class="form-control" name=""></td>
                                            <td class="text-center"><input type="text" class="form-control" name="" value="0"></td>
                                        </tr>
                                        <tr class="">
                                            <td class="text-left"><input type="date" class="form-control" name=""></td>
                                            <td class="text-center">
                                                <select class="form-control">
                                                    <option>[SELECCIONE]</option>
                                                </select>
                                            </td>
                                            <td class="text-center"><input type="text" class="form-control" name=""></td>
                                            <td class="text-center">
                                                <select class="form-control">
                                                    <option>[SELECCIONE]</option>
                                                </select>
                                            </td>
                                            <td class="text-center"><input type="text" class="form-control" name=""></td>
                                            <td class="text-center"><input type="text" class="form-control" name=""></td>
                                            <td class="text-center"><input type="text" class="form-control" name=""></td>
                                            <td class="text-center"><input type="text" class="form-control" name=""></td>
                                            <td class="text-center"><input type="text" class="form-control" name="" value="0"></td>
                                        </tr>
                                        <tr class="">
                                            <td class="text-left"><input type="date" class="form-control" name=""></td>
                                            <td class="text-center">
                                                <select class="form-control">
                                                    <option>[SELECCIONE]</option>
                                                </select>
                                            </td>
                                            <td class="text-center"><input type="text" class="form-control" name=""></td>
                                            <td class="text-center">
                                                <select class="form-control">
                                                    <option>[SELECCIONE]</option>
                                                </select>
                                            </td>
                                            <td class="text-center"><input type="text" class="form-control" name=""></td>
                                            <td class="text-center"><input type="text" class="form-control" name=""></td>
                                            <td class="text-center"><input type="text" class="form-control" name=""></td>
                                            <td class="text-center"><input type="text" class="form-control" name=""></td>
                                            <td class="text-center"><input type="text" class="form-control" name="" value="0"></td>
                                        </tr>
                                        <tr class="">
                                            <td class="text-left"><input type="date" class="form-control" name=""></td>
                                            <td class="text-center">
                                                <select class="form-control">
                                                    <option>[SELECCIONE]</option>
                                                </select>
                                            </td>
                                            <td class="text-center"><input type="text" class="form-control" name=""></td>
                                            <td class="text-center">
                                                <select class="form-control">
                                                    <option>[SELECCIONE]</option>
                                                </select>
                                            </td>
                                            <td class="text-center"><input type="text" class="form-control" name=""></td>
                                            <td class="text-center"><input type="text" class="form-control" name=""></td>
                                            <td class="text-center"><input type="text" class="form-control" name=""></td>
                                            <td class="text-center"><input type="text" class="form-control" name=""></td>
                                            <td class="text-center"><input type="text" class="form-control" name="" value="0"></td>
                                        </tr>
                                        <tr class="">
                                            <td class="text-left"><input type="date" class="form-control" name=""></td>
                                            <td class="text-center">
                                                <select class="form-control">
                                                    <option>[SELECCIONE]</option>
                                                </select>
                                            </td>
                                            <td class="text-center"><input type="text" class="form-control" name=""></td>
                                            <td class="text-center">
                                                <select class="form-control">
                                                    <option>[SELECCIONE]</option>
                                                </select>
                                            </td>
                                            <td class="text-center"><input type="text" class="form-control" name=""></td>
                                            <td class="text-center"><input type="text" class="form-control" name=""></td>
                                            <td class="text-center"><input type="text" class="form-control" name=""></td>
                                            <td class="text-center"><input type="text" class="form-control" name=""></td>
                                            <td class="text-center"><input type="text" class="form-control" name="" value="0"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>


                            <hr>

                            <div class="col-sm-12">
                                <div class="text-center">
                                    <a class="btn btn-inverse-alt" href="">
                                        <span class="fa fa-floppy-o"></span>&nbsp;Guardar Cambios
                                    </a>
                                    <a class="btn btn-primary" href="">
                                        <span class="fa fa-sign-out"></span>&nbsp;Finalizar
                                    </a>
                                </div>
                            </div>

                            <style type="text/css">
                                .vertical{
                                    vertical-align: middle;
                                }
                            </style>

                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>


