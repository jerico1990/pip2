<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<div id="page-content" style="min-height: 1342px;">
<div id="wrap">
    <div id="page-heading">
        <h1>Información General</h1>
    </div>
    <div class="container">
        <style>
            .div-label-left b {
                margin-bottom: 10px;
                font-size: 18px;
            }
            .count-message {
                color: #2460AA;
                margin-top: 5px;
            }
        </style>
    </div>
</div>
</div>