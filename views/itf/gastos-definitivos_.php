<div id="page-content" style="min-height: 754px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Gastos Definitivos</h1>
        </div>
        <div class="container">
            
            <!-- OBJETIVOS -->
            <style>
                .panel .panel-body {
                    padding: 10px !important;
                }

                .panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 1px solid #a8a8ab !important;
                }

                .panel-gray .panel-body {
                    border-top: none !important;
                }

                .btn-sm {
                    padding: 5px 10px !important;
                    line-height: 1.5 !important;
                }

                .input-comp-nombre {
                    text-transform: uppercase;
                }

                .tbl-act > tbody > .tr-header > td {
                    text-align: center;
                    font-size: 12px;
                    font-weight: bold;
                    padding: 5px !important;
                    vertical-align: middle !important;
                    /*border: 1px solid #cfcfd0;
                    background-color: #f0f0f1;*/
                    border: 1px solid #c0c0c0;
                    background-color: #e3e3e3;
                }

                /*.tbl-act .tr-item td {
                    padding: 2px !important;
                }*/
                input.form-control, select.form-control {
                    /*height: 28px !important;*/
                }

                .tbl-act .tr-act .input-nombre {
                    min-width: 320px !important;
                }

                .tbl-act .tr-act .input-unidadmedida {
                    min-width: 100px !important;
                }

                .input-number {
                    min-width: 92px !important;
                }

                .btn-add-re {
                    float: right;
                }

                /*.tbl-act > tbody > tr > td:nth-child(2) {
                    min-width: 40px !important;
                    text-align: center !important;
                }*/
                .text-left {
                    text-align: left !important;
                    font-weight: bold;
                }

                .tr-comp input, .tr-proy input {
                    font-weight: bold;
                }

                .th-proy {
                    min-width: 630px !important;
                }

            </style>

            <div class="panel panel-primary">
                <!-- <div class="panel-heading">
                    <h4>OBJETIVO</h4>
                </div> -->
                <div class="panel-body">
                    <div class="row">
                        <div class="container">

                            <!-- <div class="text-left">
                                <a class="btn btn-primary" id="volvers" href="/pip2/web/itf/nuevo-gasto-definitivo">
                                    <span class="fa fa-plus"></span>&nbsp;Añadir Nuevo
                                </a>
                            </div> -->
                            <hr>

                            <div class="table-responsive" style="max-height: 800px;">
                                <table class="table table-bordered table-condensed tbl-act">
                                    <tbody>
                                        <tr class="tr-header">
                                            <td rowspan="2" class="text-center">Nº</td>
                                            <td colspan="2" class="text-center">C/P Unidad Ejecutora</td>
                                            <td class="text-center" colspan="4">Documento de Gastos</td>
                                            <td class="text-center" colspan="3">Proveedor</td>
                                            <!-- <td rowspan="2" class="text-center">Codificación Segun Matriz</td> -->
                                            <td rowspan="2" class="text-center">Importe</td>
                                            <td rowspan="2" class="text-center" >Objetivo /Actividad</td>
                                            <td rowspan="2" class="text-center">Observaciones</td>

                                            <td rowspan="2" class="text-center"><em class="fa fa-cog"></em></td>
                                        </tr>
                                        <tr class="tr-header">

                                            <td class="text-center">Fecha</td>
                                            <td class="text-center">Nro</td>



                                            <td class="text-center">Fecha</td>
                                            <td class="text-center">Clase</td>
                                            <td class="text-center">Serie</td>
                                            <td class="text-center">Nro</td>

                                            <td class="text-center">RUC</td>
                                            <td class="text-center">Razón Social</td>
                                            <td class="text-center">Concepto</td>
                                        </tr>

                                        <?php if( empty($definitivoOrden) || empty($definitivoViatico)): ?>
                                        <tr class="">
                                            <td class="text-left" colspan="13">PRESUPUESTO TOTAL RENDIDO CON COMPROBANTES DE PAGO</td>
                                            <td class="text-center">0.00</td>
                                            <td class="text-center"></td>
                                        </tr>
                                        <?php else: ?>
                                            <?php //echo '<pre>';print_r($definitivoViatico); die(); ?>
                                            <?php $i=1; foreach ($definitivoViatico as $value): ?>
                                                <tr class="">
                                                    <td class="text-center"><?php echo $i ?></td>
                                                    <td class="text-center"><?php echo $value->Fecha ?></td>
                                                    <td class="text-center"><?php echo $value->ID.'-'.date('Y') ?></td>
                                                    <td class="text-center"><?php echo $value->FechaProceso ?></td>
                                                    <td class="text-center"><?php echo 'Viatico' ?></td>
                                                    <td class="text-center"><?php echo '-' ?></td>
                                                    <td class="text-center"><?php echo '-' ?></td>
                                                    <td class="text-center"><?php echo '-' ?></td>
                                                    <td class="text-center"><?php echo '-' ?></td>
                                                    <td class="text-center"><?php echo '-' ?></td>
                                                    <td class="text-center"><?php echo '-' ?></td>
                                                    <td class="text-center"><?php echo $value->CodigoMatriz ?></td>
                                                    <td class="text-center"><?php echo '-' ?></td>
                                                    <td class="text-center"><?php echo '-' ?></td>
                                                </tr>
                                            <?php $i++;endforeach ?>

                                            <?php  foreach ($definitivoOrden as $valuex): ?>
                                                <tr class="">
                                                    <td class="text-center"><?php echo $i ?></td>
                                                    <td class="text-center"><?php echo $valuex->Fecha ?></td>
                                                    <td class="text-center"><?php echo $valuex->ID.'-'.date('Y') ?></td>
                                                    <td class="text-center"><?php echo $valuex->FechaProceso ?></td>
                                                    <td class="text-center"><?php echo $valuex->TipoOrden == 3?'OS':$valuex->TipoOrden==1?'FACTURA':'RH' ?></td>
                                                    <td class="text-center"><?php echo '-' ?></td>
                                                    <td class="text-center"><?php echo '-' ?></td>
                                                    <td class="text-center"><?php echo '-' ?></td>
                                                    <td class="text-center"><?php echo '-' ?></td>
                                                    <td class="text-center"><?php echo '-' ?></td>
                                                    <td class="text-center"><?php echo '-' ?></td>
                                                    <td class="text-center"><?php echo $valuex->CodigoMatriz ?></td>
                                                    <td class="text-center"><?php echo '-' ?></td>
                                                    <td class="text-center"><?php echo '-' ?></td>
                                                </tr>
                                            <?php $i++;endforeach ?>

                                        <?php endif; ?>



                                    </tbody>
                                </table>
                            </div>


                            <hr>

                            <!-- <div class="col-sm-12">
                                <div class="text-center">
                                    <a class="btn btn-inverse-alt" href="">
                                        <i class="fa fa-download"></i>&nbsp;Descargar
                                    </a>
                                </div>
                            </div> -->

                            <style type="text/css">
                                .vertical{
                                    vertical-align: middle;
                                }
                            </style>

                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>


