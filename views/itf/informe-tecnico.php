<div id="page-content" style="min-height: 1342px;">
    <div id="wrap">
        <div id="page-heading">
        <h1>Paso Crítico</h1>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox-title">
                    <h5>Módulo: Seguimiento a Pasos Críticos - Informe Técnico: Registrar Nuevo</h5>
                </div>
                <div class="form-group">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>Nombre del Proyecto</td>
                                        <td colspan="3"><?= $informacion->TituloProyecto ?></td>
                                    </tr>
                                    <tr>
                                        <td width="20%">Inicio del PC</td>
                                        <td>31 de Diciembre de 1969</td><td width="20%">Término del PC</td>
                                        <td>31 de Mayo de 1970</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <label>I.- Cumplimiento de Paso Crítico</label>
                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th colspan="6"><small>Cumplimiento de Indicadores de Paso Critico Programados</small></th>
                                        </tr>
                                        <tr>
                                            <th class="text-center"><small>#</small></th>
                                            <th width="25%"><small>Descripción del Indicador</small></th>
                                            <th width="10%" class="text-center"><small>U.M.</small></th>
                                            <th width="15%" class="text-center"><small>Programado</small></th>
                                            <th width="15%" class="text-center"><small>Ejecutado</small></th>
                                            <th><small>Descripción del avance del cumplimiento del paso crítico</small></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center"><small>1</small></td>
                                            <td><small>Descripcion </small></td>
                                            <td class="text-center"><small>Unidad de Medida </small></td>
                                            <td>
                                                <input type="text" name="ProgramadoPC[1]" class="form-control input-sm" pattern="^\d+(\.\d{1,5})?$" title="Ingrese un número válido" readonly="" value="12.00">
                                            </td>
                                            <td>
                                                <input type="text" name="EjecutadoPC[1]" class="form-control input-sm" pattern="^\d+(\.\d{1,5})?$" title="Ingrese un número válido" maxlength="15" value="0.00" required="">
                                            </td>
                                            <td>
                                                <input type="text" name="ComentarioPC[1]" class="form-control input-sm" maxlength="100" value="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-center"><small>2</small></td>
                                            <td><small>Elaboracion de la linea de base</small></td>
                                            <td class="text-center"><small>Informe</small></td>
                                            <td>
                                                <input type="text" name="ProgramadoPC[4954]" class="form-control input-sm" pattern="^\d+(\.\d{1,5})?$" title="Ingrese un número válido" readonly="" value="1.00">
                                            </td>
                                            <td>
                                                <input type="text" name="EjecutadoPC[4954]" class="form-control input-sm" pattern="^\d+(\.\d{1,5})?$" title="Ingrese un número válido" maxlength="15" value="0.00" required="">
                                            </td>
                                            <td>
                                                <input type="text" name="ComentarioPC[4954]" class="form-control input-sm" maxlength="100" value="">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <label>II.- PARA EL CASO DE INCUMPLIMIENTO DE EJECUCION DEL PASO CRITICO</label>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label>Justificación al no cumplimiento del Paso Crítico</label>
                            <textarea name="JustificacionNoCumplimiento" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-9">
                            <label>El incumplimiento de indicadores de Paso Crítico afectará al subproyecto en cuanto al cumplimiento de objetivos?</label>
                        </div>
                        <div class="col-md-3">
                            <label for="checkbox-3" class="checkbox-inline">
                                <input type="radio" id="checkbox-3" name="IncumplimientoNro2Afecta" class="i-checks" value="1" style="position: absolute; opacity: 0;">
                                       Si
                            </label>
                            <label for="checkbox-4" class="checkbox-inline">
                                <input type="radio" id="checkbox-3" name="IncumplimientoNro2Afecta" class="i-checks" value="0" style="position: absolute; opacity: 0;"> No
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>