<div id="page-content" style="min-height: 754px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Libro Banco</h1>
        </div>
        <div class="container">
            
            <!-- OBJETIVOS -->
            <style>
                .panel .panel-body {
                    padding: 10px !important;
                }

                .panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 1px solid #a8a8ab !important;
                }

                .panel-gray .panel-body {
                    border-top: none !important;
                }

                .btn-sm {
                    padding: 5px 10px !important;
                    line-height: 1.5 !important;
                }

                .input-comp-nombre {
                    text-transform: uppercase;
                }

                .tbl-act > tbody > .tr-header > td {
                    text-align: center;
                    font-size: 12px;
                    font-weight: bold;
                    padding: 5px !important;
                    vertical-align: middle !important;
                    /*border: 1px solid #cfcfd0;
                    background-color: #f0f0f1;*/
                    border: 1px solid #c0c0c0;
                    background-color: #e3e3e3;
                }

                /*.tbl-act .tr-item td {
                    padding: 2px !important;
                }*/
                input.form-control, select.form-control {
                    /*height: 28px !important;*/
                }

                .tbl-act .tr-act .input-nombre {
                    min-width: 320px !important;
                }

                .tbl-act .tr-act .input-unidadmedida {
                    min-width: 100px !important;
                }

                .input-number {
                    min-width: 92px !important;
                }

                .btn-add-re {
                    float: right;
                }

                /*.tbl-act > tbody > tr > td:nth-child(2) {
                    min-width: 40px !important;
                    text-align: center !important;
                }*/
                .text-left {
                    text-align: left !important;
                    font-weight: bold;
                }

                .tr-comp input, .tr-proy input {
                    font-weight: bold;
                }

                .th-proy {
                    min-width: 630px !important;
                }

            </style>

            <div class="panel panel-primary">
                <!-- <div class="panel-heading">
                    <h4>OBJETIVO</h4>
                </div> -->
                <div class="panel-body">
                    <div class="row">
                        <div class="container">

                            <!-- <div class="text-left">
                                <a class="btn btn-primary" id="volvers" href="/pip2/web/itf/nuevo-gasto-definitivo">
                                    <span class="fa fa-plus"></span>&nbsp;Añadir Nuevo
                                </a>
                            </div> -->
                            <hr>

                            <div class="table-responsive" style="max-height: 800px;">
                                <table class="table table-bordered table-condensed tbl-act">
                                    <tbody>
                                        <tr class="tr-header">
                                            <td class="text-center">Nº</td>
                                            <td class="text-center" style="width: 78px;">Fecha</td>
                                            <td class="text-center">Documento</td>
                                            <td class="text-center">Girado a</td>
                                            <td class="text-center">Detalle</td>
                                            <td class="text-center">Debe</td>
                                            <td class="text-center">Haber</td>
                                            <td class="text-center">Saldo</td>

                                            <td class="text-center"><em class="fa fa-cog"></em></td>
                                        </tr>

                                        <?php if( empty($libro) ): ?>
                                        <tr class="">
                                            <td class="text-left" colspan="7">PRESUPUESTO TOTAL RENDIDO CON COMPROBANTES DE PAGO</td>
                                            <td class="text-center">0.00</td>
                                            <td class="text-center"></td>
                                        </tr>
                                        <?php else: ?>
                                            <?php //echo '<pre>';print_r($libro); die(); ?>
                                            <?php $siaf = '';$mont= 0;$i=1; $to = 0;foreach ($libro as $value): ?>
                                                <tr class="">
                                                
                                                <?php 
                                                if($siaf != $value->NUME_SIAF_COP):
                                                    $siaf = $value->NUME_SIAF_COP;
                                                    ?>
                                                    <td class="text-center"><?php echo $i ?></td>
                                                    <td class="text-center"><?php echo Yii::$app->tools->dateFormat($value->FECH_PAGO_COP) ?></td>
                                                    <td class="text-center"><?php echo 'CH. N°'.$value->NUM_CHEQ_CHE ?></td>
                                                    <td class="text-center"><?php echo $value->DESC_ANEX_ANX ?></td>
                                                    <td class="text-center"><?php echo $value->CONC_PAGO_COP ?></td>
                                                    <td class="text-center"><?php echo '' ?></td>
                                                    <td class="text-center"><?php echo '' ?></td>
                                                    <td class="text-center"><?php echo $value->MONT_COMP_COP ?></td>
                                                    <td class="text-center"><?php echo '' ?></td>
                                                </tr>
                                            <?php $i++; $to = $value->MONT_COMP_COP + $to;endif;  endforeach ?>
                                            <tr class="">
                                                <td class="text-left" colspan="7">PRESUPUESTO TOTAL RENDIDO CON COMPROBANTES DE PAGO</td>
                                                <td class="text-center"><?php echo number_format($to,2); ?></td>
                                                <td class="text-center"></td>
                                            </tr>
                                        <?php endif; ?>



                                    </tbody>
                                </table>
                            </div>


                            <hr>

                            <!-- <div class="col-sm-12">
                                <div class="text-center">
                                    <a class="btn btn-inverse-alt" href="">
                                        <i class="fa fa-download"></i>&nbsp;Descargar
                                    </a>
                                </div>
                            </div> -->

                            <style type="text/css">
                                .vertical{
                                    vertical-align: middle;
                                }
                            </style>

                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>


