<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'evento/crear?CodigoProyecto='.$CodigoProyecto,
        'options'           => [
            'enctype'       =>'multipart/form-data',
            'id'            => 'frmEvento'
            
        ]
    ]
); ?>
    <input type="hidden" name="Evento[CodigoProyecto]" value="<?= $CodigoProyecto ?>">
    <div class="modal-body">

        <div class="tab-container">
            <ul class="nav nav-tabs">
                <li id="li-poaf" class="active"><a href="#tab-act" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Evento</a></li>
                <li id="li-pc"><a href="#indicadores-tab" data-toggle="tab"><i class="fa fa-pencil"></i>&nbsp;Anexo</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab-act">
                    <div id="container-resp"></div>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Objetivos:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <select class="form-control" name="Evento[ComponenteID]" id="evento-componenteid" onchange="Actividad($(this).val())" required>
                                    <option>[SELECCIONE]</option>
                                    <?php foreach($componentes as $componente){ ?>
                                        <option value="<?= $componente->ID ?>"><?= $componente->Correlativo ?>.- <?= $componente->Nombre ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Actividad:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <select class="form-control" name="Evento[ActividadID]" id="evento-actividadid" onchange="Recurso($(this).val())" required>
                                    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Título evento:<span class="f_req">*</span></label>
                            <div class="col-sm-4">
                                <input class="form-control" type="text" name="Evento[Titulo]" required>
                            </div>
                            <label class="col-sm-2 control-label">Responsable del evento:<span class="f_req">*</span></label>
                            <div class="col-sm-3">
                                <input class="form-control" type="text" name="Evento[ResponsableEvento]" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tipología:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <select class="form-control" name="Evento[Tipologia]" required>
                                    <option>[SELECCIONE]</option>
                                    <option value=1>Transferencia</option>
                                    <option value=2>Capacitación</option>
                                    <option value=3>Taller de inicio</option>
                                    <option value=4>Taller de cierre</option>
                                    <option value=5>Capacitación y eventos</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Duración:(en horas)<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <input class="form-control input-number" type="text" name="Evento[Duracion]" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Beneficiario:(hombres)<span class="f_req">*</span></label>
                            <div class="col-sm-3">
                                <input class="form-control input-number" type="text" name="Evento[BeneficiarioHombres]" required>
                            </div>
                            <label class="col-sm-2 control-label">Beneficiario:(mujeres)<span class="f_req">*</span></label>
                            <div class="col-sm-4">
                                <input class="form-control input-number" type="text" name="Evento[BeneficiarioMujeres]" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Introducción:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="Evento[Introduccion]" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Material:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="Evento[Material]" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Métodos:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="Evento[Metodo]" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Resultados:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="Evento[Resultado]" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Conclusiones:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="Evento[Conclusion]" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Costo:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <input class="form-control input-number" type="text" name="Evento[Costo]" required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="indicadores-tab">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Anexos:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="input-group">
                                        <div class="form-control uneditable-input" data-trigger="fileinput">
                                            <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                            <span class="fileinput-filename">
                                            Seleccione archivo...
                                            </span>
                                        </div>
                                        <span class="input-group-addon btn btn-default btn-file">
                                            <span class="fileinput-new">Seleccionar</span>
                                            <span class="fileinput-exists">Cambiar</span>
                                            <input type="file" onchange="return Imagen(this);" id="file-cv-administrativo" name="Evento[Archivo]" required>
                                        </span>
                                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Eliminar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>
<?php
    $actividades=Yii::$app->getUrlManager()->createUrl('termino-referencia/actividades');
    $recursos=Yii::$app->getUrlManager()->createUrl('termino-referencia/recursos');
?>
<script>

    var $form = $('#frmEvento');
    // var $formI = $('#frmActividadComponenteIndicadores');
    var formParsleyEvento = $form.parsley(defaultParsleyForm());
    // var formParsleyActividadProyecto1 = $formI.parsley(defaultParsleyForm());


    function Actividad(valor) {
        var ComponenteID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $actividades ?>',
            type: 'POST',
            async: false,
            data: {ComponenteID:ComponenteID,_csrf: toke},
            success: function(data){
               $( "#evento-actividadid" ).html( data );
            }
        });
    }

    $(document).ready(function () {
        validarNumeros();
        var como = $("#actividad-indicador-id").val();
        // alert(como);
        if(como != ''){
            $('.indica').css('display','none');
            
            $('#indicador-form-actv').removeClass('hidden');
        }
        cargarIndicadoresActividad(como);
    });

    
    function cargarIndicadoresActividad(x) {
        $('.table_indica_actv').load('<?= \Yii::$app->request->BaseUrl ?>/marco-logico/lista-indicadores-actividad?ActividadID='+x);
    }


</script>