<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'evento/actualizar?ID='.$evento->ID,
        'options'           => [
            'enctype'       =>'multipart/form-data',
            
        ]
    ]
); ?>
    <input type="hidden" name="Evento[CodigoProyecto]" value="<?= $evento->CodigoProyecto ?>">
    <div class="modal-body">


    <div class="tab-container">
            <ul class="nav nav-tabs">
                <li id="li-poaf" class="active"><a href="#tab-act" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Evento</a></li>
                <li id="li-pc"><a href="#indicadores-tab" data-toggle="tab"><i class="fa fa-pencil"></i>&nbsp;Anexo</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab-act">
                    <div id="container-resp"></div>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Objetivo:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <select class="form-control" name="Evento[ComponenteID]" id="evento-componenteid" onchange="Actividad($(this).val())">
                                    <option>[SELECCIONE]</option>
                                    <?php foreach($componentes as $componente){ ?>
                                        <option value="<?= $componente->ID ?>" <?= ($componente->ID==$evento->ComponenteID)?'selected':'';?>><?= $componente->Correlativo ?>.- <?= $componente->Nombre ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Actividad:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <select class="form-control" name="Evento[ActividadID]" id="evento-actividadid" onchange="Recurso($(this).val())">
                                    <?php foreach($actividades as $actividad){ ?>
                                        <option value="<?= $actividad->ID ?>" <?= ($actividad->ID==$evento->ActividadID)?'selected':'';?>><?= $actividad->Nombre ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Título evento:<span class="f_req">*</span></label>
                            <div class="col-sm-4">
                                <input class="form-control" type="text" name="Evento[Titulo]" value="<?= $evento->Titulo ?>">
                            </div>
                            <label class="col-sm-2 control-label">Responsable del evento:<span class="f_req">*</span></label>
                            <div class="col-sm-3">
                                <input class="form-control" type="text" name="Evento[ResponsableEvento]" value="<?= $evento->ResponsableEvento ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tipología:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <select class="form-control" name="Evento[Tipologia]">
                                    <option>[SELECCIONE]</option>
                                    <option value=1  <?= ($evento->Tipologia==1)?'selected':'';?>>Transferencia</option>
                                    <option value=2  <?= ($evento->Tipologia==2)?'selected':'';?>>Capacitación</option>
                                    <option value=3  <?= ($evento->Tipologia==3)?'selected':'';?>>Taller de inicio</option>
                                    <option value=4  <?= ($evento->Tipologia==4)?'selected':'';?>>Taller de cierre</option>
                                    <option value=5  <?= ($evento->Tipologia==5)?'selected':'';?>>Capacitación y eventos</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Duración:(en horas)<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <input class="form-control input-number" type="text" name="Evento[Duracion]" value="<?= $evento->Duracion ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Beneficiario:(hombres)<span class="f_req">*</span></label>
                            <div class="col-sm-3">
                                <input class="form-control input-number" type="text" name="Evento[BeneficiarioHombres]" value="<?= $evento->BeneficiarioHombres ?>">
                            </div>
                            <label class="col-sm-2 control-label">Beneficiario:(mujeres)<span class="f_req">*</span></label>
                            <div class="col-sm-4">
                                <input class="form-control input-number" type="text" name="Evento[BeneficiarioMujeres]" value="<?= $evento->BeneficiarioMujeres ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Introducción:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="Evento[Introduccion]"><?= $evento->Introduccion ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Material:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="Evento[Material]"><?= $evento->Material ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Métodos:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="Evento[Metodo]"><?= $evento->Metodo ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Resultados:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="Evento[Resultado]"><?= $evento->Resultado ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Conclusiones:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="Evento[Conclusion]"><?= $evento->Conclusion ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Costo:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <input class="form-control input-number" type="text" name="Evento[Costo]" value="<?= $evento->Costo ?>">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="indicadores-tab">
                    <div class="form-horizontal">
                        <div class="alert alert-info indica">
                            <i class="fa fa-info"></i>&nbsp;&nbsp;No tiene indicadores registrados.
                        </div>
                        <div class="form-group">
                            
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Nombre:<span class="f_req">*</span></label>
                                <div class="col-sm-9">

                                    <input class="form-control" type="hidden" id="IDForm" name="DetalleSustento[Nombre]" value="<?php echo $evento->ID ?>">
                                    <input class="form-control" type="text" id="NombreDetalle" name="DetalleSustento[Nombre]" value="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Anexos:<span class="f_req">*</span></label>
                                <div class="col-sm-9">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="input-group">
                                            <div class="form-control uneditable-input" data-trigger="fileinput">
                                                <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                <span class="fileinput-filename">
                                                Seleccione archivo...
                                                </span>
                                            </div>
                                            <span class="input-group-addon btn btn-default btn-file">
                                                <span class="fileinput-new">Seleccionar</span>
                                                <span class="fileinput-exists">Cambiar</span>
                                                <input type="file" onchange="return Imagen(this);" id="file-cv-anexo" name="DetalleSustento[Documento]" required>
                                            </span>
                                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Eliminar</a>
                                        </div>
                                    </div>
                                    <br />
                                    <!-- <a href=""><i class="fa fa-download"></i>&nbsp;Descargar formato</a> -->
                                    &nbsp;&nbsp;&nbsp;
                                    <a href="#" id="btn-upload-anexos"><i class="fa fa-cloud-upload fa-lg"></i>&nbsp;Adjuntar Archivo</a>




                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


        
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>
<?php
    $actividades=Yii::$app->getUrlManager()->createUrl('termino-referencia/actividades');
    $recursos=Yii::$app->getUrlManager()->createUrl('termino-referencia/recursos');
?>
<script>
    function Actividad(valor) {
        var ComponenteID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $actividades ?>',
            type: 'POST',
            async: false,
            data: {ComponenteID:ComponenteID,_csrf: toke},
            success: function(data){
               $( "#evento-actividadid" ).html( data );
            }
        });
    }


    $(document).ready(function () {
        validarNumeros();
        var como = $("#actividad-indicador-id").val();
        // alert(como);
        if(como != ''){
            $('.indica').css('display','none');
            
            $('#indicador-form-actv').removeClass('hidden');
        }
        cargarIndicadoresActividad(como);
    });

    
    function cargarIndicadoresActividad(x) {
        $('.table_indica_actv').load('<?= \Yii::$app->request->BaseUrl ?>/marco-logico/lista-indicadores-actividad?ActividadID='+x);
    }


    document.getElementById('btn-upload-anexos').onclick = function () {
        var formdata = new FormData();
        var fileInput = document.getElementById('file-cv-anexo');
        if (fileInput.files.length) {
            for (i = 0; i < fileInput.files.length; i++) {
                console.log(fileInput.files[i].name);
                console.log(fileInput.files[i]);
                // formdata.append(fileInput.files[i].name, fileInput.files[i]);
                formdata.append('ValeProvisionalCaja_pdf', fileInput.files[i] );
            }
            formdata.append('Nombre', $('#NombreDetalle').val() );
            formdata.append('CodeID', $('#IDForm').val() );

            formdata.append('_csrf', '<?=Yii::$app->request->getCsrfToken()?>');
            console.log(formdata);
            var xhr = new XMLHttpRequest();
            xhr.open('POST', 'evento/subir-anexo');
            xhr.send(formdata);
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    console.log(xhr);
                    var op = JSON.parse(xhr.responseText);
                    if (op.Success) {
                        bootbox.alert('El archivo se cargó correctamente');
                    }
                }
            }
        } else {
            bootbox.alert('Debe seleccionar un archivo.');
        }
        return false;
    }



    
</script>