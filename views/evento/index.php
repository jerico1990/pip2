
<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay_progress.min.js"></script>
<script src=" //cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>


<div id="page-content" style="min-height: 1342px;">
    
    <div id="wrap">
        <div id="page-heading">
            <h1>Eventos</h1>
        </div>
        <div class="container">
            <div class="form-group">
                <button class="btn btn-primary btn-crear-evento" data-codigo-proyecto="<?= $CodigoProyecto ?>">Generar Evento</button>
            </div>
            <div class="table-responsive">
                <table id="eventos" class="display table table-bordered table-striped table-condensed table-hover" cellspacing="0" >
                    <thead>
                        <tr>
                            <th>Responsable evento</th>
                            <th>Título evento</th>
                            <th>Duración</th>
                            <th>Fecha registro</th>
                            <th>Documento</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-ctrl-evento" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Evento</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<script>
    function js_buscar()
    {
       
    }
    var tblresultjs;
        var configDTjs={
            "order": [[ 2, "desc" ]],
                "language": {
                    "sProcessing":    "Procesando...",
                    "sLengthMenu":    "Mostrar _MENU_ registros",
                    "sZeroRecords":   "No se encontraron resultados",
                    "sEmptyTable":    "Ningún dato disponible en esta lista",
                    "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":   "",
                    "sSearch":        "Buscar:",
                    "sUrl":           "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":    "Último",
                        "sNext":    "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
        };
    $(document).ready(function() {
        var CodigoProyecto="<?= $CodigoProyecto ?>";
        $.ajax({
            url:'evento/lista-eventos?CodigoProyecto='+CodigoProyecto,
            async:false,
            data:{},
            beforeSend:function()
            {
                $('.panel-default').LoadingOverlay("show");
            },
            success:function(result)
            {
                    //$tblresultjs.destroy();//
                    $("#eventos tbody").html(result);
                    $('#eventos').DataTable(configDTjs);
                    //tblresultjs=$('#example').DataTable(configDTjs);
                    $('.panel-default').LoadingOverlay("hide",true);
            },
            error:function(){
                $('.panel-default').LoadingOverlay("hide",true);
                alert('Error al realizar el proceso de busqueda.');
            }
        });
        
    } );
    
    $('body').on('click', '.btn-crear-evento', function (e) {
        e.preventDefault();
        var CodigoProyecto = $(this).attr('data-codigo-proyecto');
        $('#modal-ctrl-evento .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/evento/crear?CodigoProyecto='+CodigoProyecto);
        $('#modal-ctrl-evento').modal('show');
    });
    
    $('body').on('click', '.btn-edit-evento', function (e) {
        e.preventDefault();
        var id=$(this).attr('data-id');
        $('#modal-ctrl-evento .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/evento/actualizar?ID='+id);
        $('#modal-ctrl-evento').modal('show');
    });
    
    $('body').on('click', '.btn-remove', function (e) {
        e.preventDefault();
        var $this = $(this);
        bootboxConfirmEliminar($this,
            'Está seguro de eliminar el evento?',
            function () {
                var id = $this.attr('data-id');
                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                $.get('<?= \Yii::$app->request->BaseUrl ?>/evento/eliminar', { id: id ,_csrf: toke }, function (data) {
                    var jx = JSON.parse(data);
                    if (jx.Success == true) {
                        location.reload();
                    }
                });
            });
    });
</script>