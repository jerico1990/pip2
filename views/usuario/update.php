<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */

$this->title = 'Update Usuario: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div id="page-content" style="min-height: 754px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Crear Usuario </h1>
        </div>
        <div class="container">
            <div class="usuario-update">
            
                <h1><?= Html::encode($this->title) ?></h1>
            
                <?= $this->render('_form', [
                    'model' => $model,
                    'personas'=>$personas
                ]) ?>
            
            </div>
        </div>
    </div>
</div>
