<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usuario-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'username')->textInput() ?>
    
    <?= $form->field($model, 'password')->passwordInput() ?>

    <?= $form->field($model, 'CodigoVerificacion')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>
    
    <div class="form-group field-usuario-personaid">
        <label class="control-label" for="usuario-personaid">Persona Id</label>
        <select type="text" id="usuario-personaid" class="form-control" name="Usuario[PersonaID]">
            <option value>Seleccionar</option>
            <?php foreach($personas as $persona){ ?>
                <option value="<?= $persona->ID ?>" <?= ($persona->ID==$model->PersonaID)?'selected':'';?>><?= $persona->Nombre." ".$persona->ApellidoPaterno ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
