<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => \Yii::$app->request->BaseUrl.'/usuario/cambiar-contrasena?ID='.$usuario->ID,
        'options'           => [
            'enctype'       =>'multipart/form-data',
            'id'            => 'frmContrasena',
            
        ]
    ]
); ?>
    <div class="modal-body">
        <div class="form-horizontal">
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Contraseña:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" onblur="Password(this)" id="contrasena" name="Usuario[Contrasena]" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Recontraseña:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" id="re-contrasena" name="Usuario[Recontrasena]" required>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="modal-footer">
        <?php if($usuario->CodigoVerificacion){ ?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <?php }else{ ?>
        <a href="<?= \Yii::$app->request->BaseUrl ?>/site/logout" class="btn btn-default" >Salir</a>
        <?php } ?>
        <!--<a href="<?= \Yii::$app->request->BaseUrl ?>/site/logout" class="btn btn-default" >Salir</a>-->
        <button type="submit" class="btn btn-primary" id="btn-guardar-contrasena"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>


<style type="text/css">
    .none{
        display: none;
    }
    .block{
        display: block;
    }

    .ui-draggable, .ui-droppable {
      background-position: top;
    }
</style>



<script>
    var $form = $('#frmContrasena');
    var formParsleyUsuario = $form.parsley(defaultParsleyForm());
    function Password(elemento) {
        var longitud=$(elemento).val().length;
        console.log(longitud);
        if ($(elemento).val()!='' && longitud<6) {
            bootbox.alert('¡La contraseña no puede ser menor a 6 caracteres!');
            return false;
        }
    }
    
</script>

