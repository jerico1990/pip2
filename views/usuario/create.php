<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>


<?php $form = ActiveForm::begin(
    [
        'action'            => empty($model) ? 'usuario/create': 'usuario/update?id='.$model->ID,
        'options'           => [
            'enctype'       =>'multipart/form-data',
            'id'            =>'frmCrearUsuario'
        ]
    ]
); ?>
    <div class="modal-body">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label">Username:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="Usuario[username]" value="<?php echo !empty($model)?$model->username :'' ?>" required>
                    <input type="hidden" name="Usuario[status]" value="1">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Contraseña:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" name="Usuario[password]" value="<?php echo !empty($model)?$model->password :'' ?>" required>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Email:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="email" class="form-control" name="Usuario[email]" value="<?php echo !empty($model)?$model->email :'' ?>" required>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Persona:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                <?php if($model){
                    $person = $model->PersonaID;
                }else{
                    $person = '';
                }
                ?>
                    <select class="form-control" name="Usuario[PersonaID]" required>
                        <option value>Seleccione</option>
                        <?php foreach ($persona as $perso): ?>
                            <option value="<?php echo $perso->ID ?>" <?php echo $person==$perso->ID?'selected':'' ?>><?php echo $perso->ApellidoPaterno.' '.$perso->ApellidoMaterno.' '.$perso->Nombre ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>

        </div>
    </div>
    
    
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary" id="btn-guardar-requerimientos"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>


<style type="text/css">
    .none{
        display: none;
    }
    .block{
        display: block;
    }

    .ui-draggable, .ui-droppable {
      background-position: top;
    }
    .ui-widget-content{
        position: relative;
        z-index: 9999;
        width: 400px !important;
    }

    .tbl-act > thead > .tr-header > th {
        text-align: center;
        font-size: 12px;
        font-weight: bold;
        padding: 5px !important;
        vertical-align: middle !important;
        /*border: 1px solid #cfcfd0;
        background-color: #f0f0f1;*/
        border: 1px solid #c0c0c0;
        background-color: #e3e3e3;
    }

</style>



<script>
    var $form = $('#frmCrearUsuario');
    var frmCrearUsuario = $form.parsley(defaultParsleyForm());
    $('.datepicker').datepicker(defaultDatePicker());
    function Tipo(elemento) {
        console.log($(elemento).val());
        if ($(elemento).val()=='5') {
            $('.detallet').hide();
        }
        else
        {
            $('.detallet').show();
        }
    }
</script>

