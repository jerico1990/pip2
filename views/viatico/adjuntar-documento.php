<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => '../viatico/adjuntar-documento?ID='.$ID,
        'options'           => [
            'enctype'       =>'multipart/form-data',
            'id'            => 'frmViatico',
            
        ]
    ]
); ?>

    <div class="modal-body">
        <!-- <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label">Documento:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input class="form-control" onchange="return Imagen(this);" type="file" name="Viatico[archivo]" >
                    <input class="form-control" type="hidden" name="Viatico[Correlativo]" value="<?= $Viatico->Correlativo ?>">
                </div>
            </div>
        </div> -->


        <div class="form-group">
            <label class="col-sm-3 control-label">Documento:</label>
            <div class="col-sm-9">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="input-group">
                        <div class="form-control uneditable-input" data-trigger="fileinput">
                            <i class="fa fa-file fileinput-exists"></i>&nbsp;
                            <span class="fileinput-filename">
                            Seleccione archivo...
                            </span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                            <span class="fileinput-new">Seleccionar</span>
                            <span class="fileinput-exists">Cambiar</span>
                            <input type="file" onchange="return Imagen(this);" id="file-cv-administrativo" name="Viatico[archivo]">
                            <input class="form-control" type="hidden" name="Viatico[Correlativo]" value="<?= $Viatico->Correlativo ?>">
                        </span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Eliminar</a>
                    </div>
                </div>
                <!-- <br>
                <a href="/SLFC/Descarga/DescargarAnexo_CV_Administrativo"><i class="fa fa-download"></i>&nbsp;Descargar formato</a>
                &nbsp;&nbsp;&nbsp;
                <a href="#" id="btn-upload-cv-administrativo"><i class="fa fa-cloud-upload fa-lg"></i>&nbsp;Adjuntar Archivo</a> -->
            </div>
        </div>


    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>
<script>
    var $form = $('#frmViatico');
    var formParsleyViatico = $form.parsley(defaultParsleyForm());
</script>