<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

?>

<link href="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/form-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">


<!-- <link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css" /> -->
<link rel="stylesheet" media="all" type="text/css" href="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/timepicker/jquery-ui-timepicker-addon.css" />

<!-- <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.11.0/jquery-ui.min.js"></script> -->
<script type="text/javascript" src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/timepicker/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/timepicker/i18n/jquery-ui-timepicker-es.js"></script>

<div id="" style="min-height: 754px;">
    <div id="wrap">
        <!-- <div id="page-heading">
            <h1>Planilla de Viaticos </h1>
        </div> -->
        <div class="container">
            
            <!-- OBJETIVOS -->
            <style>
                .panel .panel-body {
                    padding: 10px !important;
                }

                .panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 1px solid #a8a8ab !important;
                }

                .panel-gray .panel-body {
                    border-top: none !important;
                }

                .btn-sm {
                    padding: 5px 10px !important;
                    line-height: 1.5 !important;
                }

                .input-comp-nombre {
                    text-transform: uppercase;
                }

                .tbl-act > thead > .tr-header > th {
                    text-align: center;
                    font-size: 12px;
                    font-weight: bold;
                    padding: 5px !important;
                    vertical-align: middle !important;
                    /*border: 1px solid #cfcfd0;
                    background-color: #f0f0f1;*/
                    border: 1px solid #c0c0c0;
                    background-color: #e3e3e3;
                }

                /*.tbl-act .tr-item td {
                    padding: 2px !important;
                }*/
                input.form-control, select.form-control {
                    /*height: 28px !important;*/
                }

                .tbl-act .tr-act .input-nombre {
                    min-width: 320px !important;
                }

                .tbl-act .tr-act .input-unidadmedida {
                    min-width: 100px !important;
                }

                .input-number {
                    min-width: 92px !important;
                }

                .btn-add-re {
                    float: right;
                }

                .text-left {
                    text-align: left !important;
                    font-weight: bold;
                }

                .tr-comp input, .tr-proy input {
                    font-weight: bold;
                }

                .th-proy {
                    min-width: 630px !important;
                }

                #css th{
                    text-align: right;
                }

            </style>
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="row">
                        <div class="container">
                            <?php $form = ActiveForm::begin(
                                [
                                    'action' => \Yii::$app->request->BaseUrl.'/viatico/crear-planilla?CodigoProyecto='.$CodigoProyecto,
                                    'id'=> 'frmViatico'
                                ]
                            ); ?>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    
                                    <label class="col-sm-2 control-label">DNI:<span class="f_req">*</span></label>
                                    <div class="col-sm-10">
                                        <select name="Viatico[PersonaID]" class="form-control" onchange="Persona(this)">
                                            <option value>Seleccionar</option>
                                            <?php foreach($integrantes as $integrante){?>
                                            <option value="<?= $integrante->ID ?>"><?= $integrante->NroDocumento.'-'.$integrante->Nombre.' '.$integrante->ApellidoPaterno.' '.$integrante->ApellidoMaterno ?></option>
                                            <?php }?>
                                        </select>
                                        <!--<input type="text" onfocusout="DNI($(this).val())" onKeyPress="return soloNumeros(event);" class="form-control" id="dni" maxlength="8" name="Viatico[Dni]" required>-->
                                        
                                        <input type="hidden" class="dni" maxlength="8" name="Viatico[Dni]">
                                        <input type="hidden" name="Viatico[CodigoProyecto]" value="<?php echo $CodigoProyecto ?>">
                                        <input type="hidden" name="Viatico[RequerimientoID]" value="<?php echo $RequerimientoID ?>">
                                        <input type="hidden" name="Viatico[DetallesRequerimientosIDs]" value="<?= $DetallesRequerimientosIDs ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nombre:<span class="f_req">*</span></label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control nombre" name="" disabled>
                                        <input type="hidden" class="nombre" name="Viatico[Nombre]">
                                    </div>
                                    <label class="col-sm-2 control-label">Apellido:<span class="f_req">*</span></label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control apellido" name="" disabled>
                                        <input type="hidden" class="apellido" name="Viatico[Apellido]">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Salida:<span class="f_req">*</span></label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control " id="salida_viatico" data-date-format="dd-mm-yyyy" name="Viatico[Salida]" required>
                                    </div>
                                    <label class="col-sm-2 control-label ">Regreso: <span class="f_req">*</span></label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control " id="regreso_viatico" name="Viatico[Regreso]" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nro. Dias:<span class="f_req">*</span></label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="nro-dias" onKeyPress="return soloNumeros(event)" name="Viatico[NroDias]" required>
                                    </div>
                                    <label class="col-sm-2 control-label">Horas asignadas: <span class="f_req">*</span></label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="horas-asig" onKeyPress="return soloNumeros(event)" name="Viatico[AsigHoras]" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Cargo/Función:<span class="f_req">*</span></label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control cargo"  disabled>
                                        <input type="hidden" class="cargo"  name="Viatico[Cargo]" required>
                                    </div>
                                </div>

                                <!-- <div class="form-group">
                                    <label class="col-sm-2 control-label">Regimen Laboral:<span class="f_req">*</span></label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="Viatico[Regimen]" e>
                                            <option>[SELECCIONE]</option>
                                            <option value="CAP">CAP</option>
                                            <option value="CAS">CAS</option>
                                            <option value="ST">S.T.</option>
                                        </select>
                                    </div>
                                </div> -->
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Plan de trabajo: <span class="f_req">*</span></label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" name="Viatico[PlanTrabajo]" required></textarea>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tipo de Viaje:<span class="f_req">*</span></label>
                                    <div class="col-sm-4">
                                        <select class="form-control" name="Viatico[TipoViaje]" required>
                                            <option value>[SELECCIONE]</option>
                                            <option value="1">Nacional</option>
                                            <option value="2">Local o Regional</option>
                                            <option value="3">Internacional</option>
                                        </select>
                                    </div>
                                    <label class="col-sm-2 control-label">Destino:<span class="f_req">*</span></label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control"  name="Viatico[Destino]" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Total:<span class="f_req">*</span></label>
                                    <div class="col-sm-4">
                                    <!-- input-number -->
                                        <input type="text" class="form-control " id="totales" name="" value="<?php echo $total->Total ?>" disabled>
                                    </div>
                                </div>
                                

                                
                                <div class="form-group">
                                    <div class="col-sm-10 col-md-offset-1">
                                        <div class="container">
                                            <!-- <button type="button" id="agregar" class="btn btn-primary">Agregar</button><br><br> -->
                                            <table class="table borderless table-hover tbl-act" id="detalle" >
                                                <thead>
                                                    <tr class="tr-header">
                                                        <th class="col-sm-2">Concepto</th>
                                                        <th class="col-sm-2">Importe</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="detalle">


                                                    <tr class="contador">
                                                        <td>
                                                            <select class="form-control col-sm-2" name="Viatico[Tipo][]">
                                                                <option value="Pasajes">Pasajes</option>
                                                            </select>
                                                        </td>
                                                        <td class="totalAp">
                                                            <input class="form-control col-sm-4 contadorx input-number" id="importe_1" type="text" name="Viatico[Importe][]" value="0">
                                                            <input type="hidden" name="Viatico[Codigos][]">
                                                        </td>
                                                    </tr>


                                                    <tr class="contador">
                                                        <td>
                                                            <select class="form-control col-sm-2" name="Viatico[Tipo][]">
                                                                <option value="Viaticos">Viáticos</option>
                                                            </select>
                                                        </td>
                                                        <td class="totalAp">
                                                            <input class="form-control col-sm-4 contadorx input-number" id="importe_2" type="text" name="Viatico[Importe][]" value="0">
                                                            <input type="hidden" name="Viatico[Codigos][]">
                                                        </td>
                                                    </tr>


                                                    <tr class="contador">
                                                        <td>
                                                            <select class="form-control col-sm-2" name="Viatico[Tipo][]">
                                                                <option value="Otros">Otros Gastos</option>
                                                            </select>
                                                        </td>
                                                        <td class="totalAp">
                                                            <input class="form-control col-sm-4 contadorx input-number" id="importe_3" type="text" name="Viatico[Importe][]" value="0" >
                                                            <input type="hidden" name="Viatico[Codigos][]">
                                                        </td>
                                                    </tr>



                                                    <!-- <tr id='detalle_1'>
                                                        <td colspan="3" style="text-align: center;font-weight: bold;">REGISTRAR DETALLE VIATICOS</td>
                                                    </tr> -->
                                                </tbody>
                                                <thead>
                                                    <tr id="css">
                                                        <th>Total:<input type="hidden" id="totviatico" name="Viatico[Monto]"></th>
                                                        <th id="total">S/.0.00</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                            <div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <!-- <div class="form-group">
                                    <label class="col-sm-2 control-label">FTE.FTO.:<span class="f_req">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="Viatico[FteFto]" required>
                                    </div>
                                </div> -->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Cuenta Bancaria:<span class="f_req">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="Viatico[CuentaBancaria]" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Lugar y Fecha:<span class="f_req">*</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="Viatico[LugarFecha]" required>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <a href="planilla-viaticos" class="btn btn-default" data-dismiss="modal">Cerrar</a>
                                <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
                            </div>

                            <?php ActiveForm::end(); ?>
                            
                            <?php
                                $actividades=Yii::$app->getUrlManager()->createUrl('requerimiento/actividades');
                                $recursos=Yii::$app->getUrlManager()->createUrl('requerimiento/recursos');
                            ?>

                            <style type="text/css">
                                .none{
                                    display: none;
                                }
                                .block{
                                    display: block;
                                }
                            </style>

                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>


<div class="modal fade" id="modal-ctrl-poa" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-lg" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">POA</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<script>
    $( function() {

        // $('#date_begin,#date_end').datetimepicker(); 
        // $( "#salida_viatico" ).datepicker( { dateFormat: 'dd-mm-yy' } )

        var startDateTextBox = $('#salida_viatico');
        var endDateTextBox = $('#regreso_viatico');

        startDateTextBox.datetimepicker({ 
            language: 'es',
            dateFormat: 'dd/mm/yy',
            beforeShowDay: $.datepicker.noWeekends,
            onClose: function(dateText, inst) {
                if (endDateTextBox.val() != '') {
                    var testStartDate = startDateTextBox.datetimepicker('getDate');
                    var testEndDate = endDateTextBox.datetimepicker('getDate');
                    if (testStartDate > testEndDate)
                        endDateTextBox.datetimepicker('setDate', testStartDate);
                }
                else {
                    endDateTextBox.val(dateText);
                }
            },
            onSelect: function (selectedDateTime){
                endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
            }
        });
        endDateTextBox.datetimepicker({ 
            language: 'es',
            dateFormat: 'dd/mm/yy',
            beforeShowDay: $.datepicker.noWeekends,
            onClose: function(dateText, inst) {
                if (startDateTextBox.val() != '') {
                    var testStartDate = startDateTextBox.datetimepicker('getDate');
                    var testEndDate = endDateTextBox.datetimepicker('getDate');
                    if (testStartDate > testEndDate)
                        startDateTextBox.datetimepicker('setDate', testEndDate);
                }
                else {
                    startDateTextBox.val(dateText);
                }
            },
            onSelect: function (selectedDateTime){
                startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
            }
        });
    });
</script>



<script>

    var $form = $('#frmViatico');
    var formParsleyViatico = $form.parsley(defaultParsleyForm());
    
    validarNumeros();



    $('body').on('click', '#agregar-poa', function (e) {
        e.preventDefault();
        var CodigoProyecto="<?= $CodigoProyecto ?>";

        $('#modal-ctrl-poa .modal-body-main').load('<?= Url::toRoute('requerimiento/poa?CodigoProyecto='.$CodigoProyecto,'http') ?>');
        $('#modal-ctrl-poa').modal('show');
    });



    $('body').on('click', '.seleccionar_poa', function (e) {
        e.preventDefault();
        $('.fila').each(function(){
            var id=$(this).attr('data-id');
            var componenteid=$(this).attr('data-componente');
            var actividadid=$(this).attr('data-actividad');
            var toke = '<?=Yii::$app->request->getCsrfToken()?>';
            $.get('<?= \Yii::$app->request->BaseUrl ?>/requerimiento/get-dato-poa', { id: id ,componenteid:componenteid,actividadid:actividadid,_csrf: toke }, function (data) {
                var jx = JSON.parse(data);
                if (jx.Success == true) {
                    // console.log(jx);
                    $('#requerimiento-componenteid').val(jx.componente)
                    $('#requerimiento-actividadid').val(jx.actividad)
                    $('#requerimiento-areasubcategoriaid').val(jx.recurso)
                    $('#requerimiento-codigo').val(jx.codigo)
                    
                    $('#requerimiento-componenteid-c').val(jx.componenteid)
                    $('#requerimiento-actividadid-c').val(jx.actividadid)
                    $('#requerimiento-areasubcategoriaid-c').val(jx.aresubcategoriaid)
                    $('#requerimiento-codigo-c').val(jx.codigo)
                    //$('#requerimiento-componenteid-c').val(data.componente)
                }
            });
            $('#modal-ctrl-poa').modal('hide');
        });
    });


    detalle=1;
    $('body').on('click', '#agregar', function (e) {
        var error = '';

        if (error != '') {
            $("#error_fuente_origen").html(error);
                return false;
        }
        else
        {
            var option = null;
            var con = $('.contador').length; +1
            // $('#detalle_'+detalle).html(
            
            var hh ='<tr class="contador"><td>'+
                    '<select class="form-control col-sm-2" name="Viatico[Tipo][]">'
                    +'<option value="">[SELECCIONE]</option>'
                    +'<option value="Pasajes">Pasajes</option>'
                    +'<option value="Viaticos">Viaticos</option>'
                    +'<option value="Otros">Otros Gastos</option>'
                    +'</select>'+
                '</td>'+
                '<td class="totalAp">'+
                    '<input class="form-control col-sm-4 contadorx input-number" id="importe_'+con+'" type="text" name="Viatico[Importe][]" required>'+
                '</td>'+
                '<td>'+
                    '<input type="hidden" name="Viatico[Codigos][]">'+
                    '<a href="javascript:;" class="eliminar"><i class="fa fa-times fa-2x" aria-hidden="true"></i></a>'+
                '</td></tr>';
            $('#detalle').append(hh);
            $('#detalle_1').remove();
            detalle++;
            
            $('#css').removeAttr('style');
            return true;
        }
    });

    
    $("#detalle").on('click','.eliminar',function(){
        var r = confirm("Estas seguro de Eliminar?");
        var mensaje = '';
        if (r == true) {
            id=$(this).children().val();
            if (id) {
                
                $(this).parent().parent().remove();
            }
            else
            {
                $(this).parent().parent().remove();    
            }   
            mensaje = "Se elimino el Registro Correctamente";
            contar();
        } 
    });
    

    $('#detalle').on('blur','.contadorx',function(){
        contar();
    });


    function contar(){
        var total_total = 0.00;
        $('.contadorx').each(function(x,y){
            TotSuma = $(this).val();
            total_total = parseFloat(getNum(TotSuma)) + parseFloat(total_total) ;
            Suma(total_total,parseFloat($('#totales').val()),this);
            
        });
        // console.log($('#totales').val());
        if (total_total>parseFloat($('#totales').val())) {
            return false;
        }
        $('#total').text( "S/."+ number_format(total_total,2) );
        $('#totviatico').val(number_format(total_total,2));
    }

    function Suma(x,y,elemento) {
        if (elemento && x>y) {
            $(elemento).val('0.00');
            contar();
            return false;
        }
        
        
        return true;
    }


    function getNum(val) {
       if (val == '0.00' || val == '') {
            return 0;
       }
       var val2 = val.replace(',','');
       return val2;
    }

    function number_format(amount, decimals) {
        amount += '';
        amount = parseFloat(amount.replace(/[^0-9\.]/g, ''));
        decimals = decimals || 0;
        if (isNaN(amount) || amount === 0) 
            return parseFloat(0).toFixed(decimals);
        amount = '' + amount.toFixed(decimals);
        var amount_parts = amount.split('.'),
            regexp = /(\d+)(\d{3})/;
        while (regexp.test(amount_parts[0]))
            amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');
        return amount_parts.join('.');
    }
    
    
    
    function POA(valor) {
        $.get('<?= \Yii::$app->request->BaseUrl ?>/orden/poa/', { valor: valor}, function (data) {
            var jx = JSON.parse(data);
            if (jx.Success == true) {
                $('#objetivo').val(jx.componente);
                $('#actividad').val(jx.actividad);
                $('#recurso').val(jx.recurso);
                $('#codigo-poa').val(jx.codigo);    
            }
            
        });
    }

    $('#regreso_viatico').change(function(){
        var salida = $('#salida_viatico').val();
        var regreso = $(this).val();
        var sal = salida.split('/');
        var reg = regreso.split('/');

        // console.log(sal[0]);
        // console.log(salida.split('/'));
        // console.log(regreso.split('/'));
        // console.log($(this).val());
        
        var fechaInicio = new Date(sal[1]+'/'+sal[0]+'/'+sal[2]).getTime();
        
        var fechaFin    = new Date(reg[1]+'/'+reg[0]+'/'+reg[2]).getTime();
        var diff = fechaFin - fechaInicio;
        
        $('#nro-dias').val( Math.round((diff/(1000*60*60*24))+1) );
        $('#horas-asig').val( Math.round((diff/(1000*60*60))+24) );
        // console.log(diff/(1000*60*60) );

    });
    
    function sumarDias(fecha, dias){
        fecha.setDate(fecha.getDate() + dias);
        return fecha;
    }
    function Persona(elemento) {
        $('.nombre').val('');
        $('.apellido').val('');
        $('.cargo').val('');
        $('.dni').val('');
        
        $.get('<?= \Yii::$app->request->BaseUrl ?>/recursos-humanos/integrantes/', { CodigoProyecto: '<?= $CodigoProyecto ?>',Tipo:1,ID:$(elemento).val()}, function (data) {
            var jx = JSON.parse(data);
            // console.log(jx);
            if (jx.Success==true) {
                $('.nombre').val(jx.Nombres);
                $('.apellido').val(jx.Apellidos);
                $('.cargo').val(jx.Cargo);
                $('.dni').val(jx.NroDocumento);
            }
        });
    }
    function DNI(valor) {
        $('.nombre').val('');
        $('.apellido').val('');

        $.get('<?= \Yii::$app->request->BaseUrl ?>/viatico/servicio-reniec/', { valor: valor}, function (data) {
            var jx = JSON.parse(data);
            // console.log(jx);
            if (jx.message!='Ok') {
                alert("DNI no se encuentra");
                console.log(message);
            }
            else
            {
                if(jx.error != 1){
                    var name = jx.data.nombres;
                    var paterno = jx.data.apellidoPaterno;
                    var materno = jx.data.apellidoMaterno;
                    $('.nombre').val(name.trim());
                    $('.apellido').val(paterno.trim()+' '+materno.trim());
                }
            }
            
        });
    }

</script>