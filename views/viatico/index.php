
<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay_progress.min.js"></script>
<script src=" //cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>


<div id="page-content" style="min-height: 1342px;">
    
    <div id="wrap">
        <div id="page-heading">
            <h1>Viáticos</h1>
        </div>
        
           
        <div class="container">
             <div class="form-group">
                
                <!-- <button class="btn btn-crear-requerimiento" data-codigo-proyecto="">Generar viatico</button> -->
                <a href="orden/crear-servicio-factura?CodigoProyecto=" class="btn btn-crear-requerimientoz" data-codigo-proyecto="">Generar viatico</a>
                
                
               <!-- <a class="btn " href="documentos/0_OFICIO_MODELO_REQUERIMIENTO.docx">Descargar plantilla</a>-->
                
                
                
            </div>
            <div class="table-responsive">
                <table id="proyectos" class="display table table-bordered table-striped table-condensed table-hover" cellspacing="0" >
                    <thead>
                        <tr>
                            <th>Tipo de Viaje</th>
                            <th>Destino</th>
                            <th>Salida</th>
                            <th>Regreso</th>
                            <th>Nro dias</th>
                            <!--<th>Acciones</th>-->
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-ctrl-requerimiento" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Requerimiento</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<script>
    function js_buscar()
    {
       
    }
    var tblresultjs;
    var configDTjs={
        "order": [[ 2, "desc" ]],
            "language": {
                "sProcessing":    "Procesando...",
                "sLengthMenu":    "Mostrar _MENU_ registros",
                "sZeroRecords":   "No se encontraron resultados",
                "sEmptyTable":    "Ningún dato disponible en esta lista",
                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":   "",
                "sSearch":        "Buscar:",
                "sUrl":           "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":    "Último",
                    "sNext":    "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
    };

    $(document).ready(function() {
        var CodigoProyecto="";
        $.ajax({
            url:'viatico/lista-ordenes?CodigoProyecto='+CodigoProyecto,
            async:false,
            data:{},
            beforeSend:function()
            {
                $('.panel-default').LoadingOverlay("show");
            },
            success:function(result)
            {
                    //$tblresultjs.destroy();//
                    $("#proyectos tbody").html(result);
                    $('#proyectos').DataTable(configDTjs);
                    //tblresultjs=$('#example').DataTable(configDTjs);
                    $('.panel-default').LoadingOverlay("hide",true);
            },
            error:function(){
                $('.panel-default').LoadingOverlay("hide",true);
                alert('Error al realizar el proceso de busqueda.');
            }
        });
        
    } );
    
    $('body').on('click', '.btn-crear-requerimiento', function (e) {
        e.preventDefault();
        var CodigoProyecto = $(this).attr('data-codigo-proyecto');
        $('#modal-ctrl-requerimiento .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/requerimiento/crear?CodigoProyecto='+CodigoProyecto);
        $('#modal-ctrl-requerimiento').modal('show');
    });


    $('body').on('click', '.btn-crear-requerimiento-servicio', function (e) {
        e.preventDefault();
        var CodigoProyecto = $(this).attr('data-codigo-proyecto');
        $('#modal-ctrl-requerimiento .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/orden/crear-servicio-factura?CodigoProyecto='+CodigoProyecto);
        $('#modal-ctrl-requerimiento').modal('show');
    });


    
    $('body').on('click', '#btn-guardar-requerimientos', function (e) {
        
        e.preventDefault();
        // alert('s');
        
        var $btn = $(this);
        $btn.attr('disabled', 'disabled');
        var isValid = formParsleyRequerimientos.validate();
        if (isValid) {
            sendFormNoMessage($('#frmRequerimientos'), $(this), function (msg) {
                
                $("#modal-ctrl-requerimiento").modal('hide');
                _pageLoadingEnd();
		
            });
        } else {
        }
    });

    $(document).on('change','.fileArchivo',function(e){
        e.preventDefault();
        var esto = $(this);
        var id = esto.data('id');
        var xid = esto.attr('id');
        var code = esto.data('code');
        var base = 'http://belserolivera.com/tesis/';
        uploadAjax("http://belserolivera.com/tesis/Dashboard/dashboard/subirArchivo",xid,id,base,code);
    });

    function uploadAjaxarchivo(url_base, idFile, semiruta,base) {
        var inputFileImage = document.getElementById(idFile);
        var file = inputFileImage.files[0];
        //console.log(file);
        var data = new FormData();
        data.append("archivo",file);
        data.append("data-ruta",semiruta);
        var url = url_base;

        $.ajax({
            url:url,
            type:'POST',
            contentType:false,
            data:data,
            processData:false,
            cache:false,
            dataType: "json",
            beforeSend: function() {
                console.log('desarrollo : belserolivera.com');
                $('#load-'+idFile+'').show();
                //$('#cod'+code).hide();
            },
            success:function(xdata){
                //console.log(xdata.ok);
                if(xdata.ok == true){
                     $('#blah-'+idFile+'').attr('src','../../uploads/'+semiruta+'/'+xdata.name);
                 }
                // return xdata;
            },
            complete: function() {
                $('#load-'+idFile+'').hide();
                // $('#cod'+code).show();
            }
        });
    }

</script>