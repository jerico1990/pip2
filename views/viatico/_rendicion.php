<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;


?>
<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay_progress.min.js"></script>
<script src=" //cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>

<div>
    <div id="wrap">
        <!-- <div id="page-heading">
            <h1>Planilla de Viaticos </h1>
        </div> -->
        <div class="container">
            
            <!-- OBJETIVOS -->
            <style>
                .panel .panel-body {
                    padding: 10px !important;
                }

                .panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 1px solid #a8a8ab !important;
                }

                .panel-gray .panel-body {
                    border-top: none !important;
                }

                .btn-sm {
                    padding: 5px 10px !important;
                    line-height: 1.5 !important;
                }

                .input-comp-nombre {
                    text-transform: uppercase;
                }

                .tr-header > td {
                    text-align: center;
                    font-size: 12px;
                    font-weight: bold;
                    padding: 5px !important;
                    vertical-align: middle !important;
                    /*border: 1px solid #cfcfd0;
                    background-color: #f0f0f1;*/
                    border: 1px solid #c0c0c0;
                    background-color: #e3e3e3;
                }

                /*.tbl-act .tr-item td {
                    padding: 2px !important;
                }*/
                input.form-control, select.form-control {
                    /*height: 28px !important;*/
                }

                .tbl-act .tr-act .input-nombre {
                    min-width: 320px !important;
                }

                .tbl-act .tr-act .input-unidadmedida {
                    min-width: 100px !important;
                }

                .input-number {
                    min-width: 92px !important;
                }

                .btn-add-re {
                    float: right;
                }

                .text-left {
                    text-align: left !important;
                    font-weight: bold;
                }

                .tr-comp input, .tr-proy input {
                    font-weight: bold;
                }

                .th-proy {
                    min-width: 630px !important;
                }
            </style>
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="row">
                        <div class="container">
                            <?php $form = ActiveForm::begin(
                                [
                                    'action' => \Yii::$app->request->BaseUrl.'/viatico/crear-rendicion?CodigoProyecto='.$CodigoProyecto,
                                    'id'	=> 'rendicionViatico'
                                ]
                            ); ?>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Planilla de Viático:<span class="f_req">*</span></label>
                                    <div class="col-sm-6">
                                        <select class="form-control" id="planilla-viatico" required>
                                            <option value="">[SELECCIONE]</option>
                                            <?php //print_r($viatico); ?>
                                            <?php foreach ($viatico as $valuex): ?>
                                                <option value="<?php echo $valuex->ID ?>"> <?php echo "Memo:".$valuex->Memorando." ".$valuex->Apellido.' '.$valuex->Nombre.' - Nº '.$valuex->Dni.' - SIAF:'.Yii::$app->administrativo->obtenerSiaf($valuex->Siaf,$valuex->Annio); ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Anticipo Recibido:<span class="f_req">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" onKeyPress="return soloNumeros(event)" class="form-control anticipo" value="" disabled>
                                        <input type="hidden" class="anticipo" name="RendicionViatico[AnticipoRecibido]" value="">

                                        <input type="hidden" id="viaticoid" name="RendicionViatico[ViaticoID]" value="">
                                        <input type="hidden" name="RendicionViatico[CodigoProyecto]" value="<?php echo $CodigoProyecto ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Gastos Sustentados:<span class="f_req">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text" class="sustentado form-control" onKeyPress="return soloNumeros(event)" name="" disabled>
                                        <input type="hidden" class="sustentado" name="RendicionViatico[GastoSustentado]" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Por Devolver/Reintegrar:<span class="f_req">*</span></label>
                                    <div class="col-sm-6">
                                        <input type="text"  onKeyPress="return soloNumeros(event)" class="devolucion form-control" name="" disabled>
                                        <input type="hidden" class="devolucion" name="RendicionViatico[Reintegrar]" required>
                                    </div>
                                </div>

                                
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="container">
                                            <button type="button" id="agregar" class="btn btn-primary" disabled>Agregar</button><br><br>
                                            <table class="table borderless table-hover" >
                                            	<thead>
                                                    <tr class="tr-header">
	                                                    <td rowspan="2">Nº</td>
	                                                    <td colspan="3">Documentos</td>
                                                        <td rowspan="2">RUC</td>
	                                                    <td rowspan="2">Proveedor</td>
	                                                    <td rowspan="2">Detalle</td>
                                                        <td rowspan="2">Importe</td>
	                                                    <td rowspan="2">Documento</td>
	                                                    <td rowspan="2"></td>
	                                                </tr>
	                                                <tr class="tr-header">
	                                                    <td>Fecha</td>
                                                        <td>Tipo</td>
	                                                    <td>Clase Nº</td>
	                                                </tr>
                                                </thead>
                                                

                                                <tbody id="detalle">
                                                    <tr id='detalle_1'>
                                                        <td colspan="9" style="text-align: center;font-weight: bold;">REGISTRAR DETALLE</td>
                                                    </tr>
                                                </tbody>
                                                <thead>
                                                    <tr id="css" style="display: none;">
                                                        <th colspan="6"></th>
                                                        <th>Total:</th>
                                                        <th id="total">S/.0.00</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                            <div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="modal-footer">
                                <a href="#" class="btn btn-default" data-dismiss="modal">Cerrar</a>
                                <button type="submit" class="btn btn-primary" id="btn-guardar-rendimiento-viatico"><i class="fa fa-check"></i>&nbsp;Guardar</button>
                            </div>

                            <?php ActiveForm::end(); ?>
                            
                            <?php
                                $actividades=Yii::$app->getUrlManager()->createUrl('requerimiento/actividades');
                                $recursos=Yii::$app->getUrlManager()->createUrl('requerimiento/recursos');
                            ?>

                            <style type="text/css">
                                .none{
                                    display: none;
                                }
                                .block{
                                    display: block;
                                }
                            </style>

                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>



<script>

	var $form = $('#rendicionViatico');
    var formParsleyRendicion = $form.parsley(defaultParsleyForm());
    var documento = '<?php echo $documento ?>';

    var jx = JSON.parse(documento);
    console.log(jx);
    // $('body').on('click', '#agregar', function (e) {
    $('#agregar').click(function(e){
        var error = '';

        if (error != '') {
            $("#error_fuente_origen").html(error);
                return false;
        }
        else
        {
            var option = null;
            // $('#detalle_'+detalle).html(
            var cont = $('.contador').length;

            var xhtm = '';
            $(jx).each(function(x,y){
                // console.log(x);
                // console.log(y);
                xhtm += '<option value="'+y.ID+'">'+y.Nombre+'</option>';
            })
            
            var hh ='<tr class="contador"><td>'+(cont+1)+'</td>'+
                '<td class="totalAp">'+
                    '<input class="form-control col-sm-4" type="date" name="RendicionViatico[Fecha][]" required >'+
                '</td>'+
                '<td class="totalAp">'+
                    '<select class="form-control" name="RendicionViatico[TipoDoc][]" required>'+xhtm+'</select>'+
                '</td>'+
                '<td class="totalAp">'+
                    '<input class="form-control col-sm-4" type="text" name="RendicionViatico[Clase][]" required>'+
                '</td>'+
                '<td class="totalAp">'+
                    '<input class="form-control col-sm-4 numero-ruc" maxlength="11" onKeyPress="return soloNumeros(event)"  data-id="'+(cont+1)+'" type="text" name="RendicionViatico[Ruc][]" required>'+
                '</td>'+
                '<td class="totalAp">'+
                    '<input class="form-control col-sm-4 proveedor_'+(cont+1)+'" type="text" name=""  disabled><input class="proveedor_'+(cont+1)+'" type="hidden" name="RendicionViatico[Proveedor][]" required>'+
                '</td>'+
                '<td class="totalAp">'+
                    '<input class="form-control col-sm-4" type="text" name="RendicionViatico[Detalle][]" required>'+
                '</td>'+


                '<td class="totalAp">'+
                    '<input type="file" class="form-control col-sm-4 fileArchivo" name="RendicionViatico[Detalle][]" data-code="1" data-id="3" id="audio-one">'+
                '</td>'+


                '<td class="totalAp">'+
                    '<input class="form-control col-sm-4 contadorx input-number" type="text" name="RendicionViatico[Importe][]"  required>'+
                '</td>'+
                '<td>'+
                    '<input type="hidden" name="RendicionViatico[Codigos][]">'+
                    '<a href="javascript:;" class="eliminar"><i class="fa fa-times fa-1x" aria-hidden="true"></i></a>'+
                '</td></tr>';
            $('#detalle').append(hh);
            $('#detalle_1').remove();
            detalle++;


            validarNumeros()

            $('#css').removeAttr('style');
            return true;
        }
    });

    
    $("#detalle").on('click','.eliminar',function(){
        var r = confirm("Estas seguro de Eliminar?");
        var mensaje = '';
        if (r == true) {
            id=$(this).children().val();
            if (id) {
                
                $(this).parent().parent().remove();
            }
            else
            {
                $(this).parent().parent().remove();    
            }   
            mensaje = "Se elimino el Registro Correctamente";
            contar();
        } 
    });

    
    $('#detalle').on('blur','.contadorx',function(){
        contar();
    });


    function contar(){
        var total_total = 0.00;
        $('.contadorx').each(function(x,y){
            // console.log(x);
            // console.log(y);

            TotSuma = $(this).val();
            total_total = parseFloat(getNum(TotSuma)) + parseFloat(total_total) ;
        });

        var total = number_format(total_total,2);
        $('#total').text( "S/."+ total );
        $('.sustentado').val(total);

        var anticipo = $('.anticipo').val();
        
        var dif = getNum(total) - getNum(anticipo);
        $('.devolucion').val(dif);
    }


    function getNum(val) {
       if (val == '0.00' || val == '') {
            return 0;
       }
       var val2 = val.replace(',','');
       return val2;
    }

    function number_format(amount, decimals) {
        amount += '';
        amount = parseFloat(amount.replace(/[^0-9\.]/g, ''));
        decimals = decimals || 0;
        if (isNaN(amount) || amount === 0) 
            return parseFloat(0).toFixed(decimals);
        amount = '' + amount.toFixed(decimals);
        var amount_parts = amount.split('.'),
            regexp = /(\d+)(\d{3})/;
        while (regexp.test(amount_parts[0]))
            amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');
        return amount_parts.join('.');
    }


    $('body').on('change','.numero-ruc',function(){
        var ss = $(this).attr('data-id');
        var valor = $(this).val();

        $('.proveedor_'+ss).val('');
        $.get('<?= \Yii::$app->request->BaseUrl ?>/orden/servicio/', { valor: valor}, function (data) {
            var jx = JSON.parse(data);
            if (jx==1) {
                bootbox.alert('RUC no se encuentra');            
            }
            else
            {
                $('.proveedor_'+ss).val(jx.razonSocial.trim());
            }
        });
    })


    $('body').on('change','#planilla-viatico',function(e){
        e.preventDefault();
        var $this = $(this);
        var cod = $(this).val();

        bootBoxConfirm('Esta seguro que desea cambiar de usuario para la rendición',function(){
            $('#agregar').removeAttr('disabled');
            $('#viaticoid').val(cod);
            $('#detalle').html('');
            $.get('<?= \Yii::$app->request->BaseUrl ?>/viatico/suma-rendicion-planilla/', { id: cod}, function (data) {
                $('.anticipo').val(data);
            });

        });

        
    });


    

    

</script>