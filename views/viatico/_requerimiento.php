<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'compromiso-presupuestal/crear?CodigoProyecto='.$CodigoProyecto,
        'options'           => [
            'id'            => 'frmCompromisoPresupuestal',
            'enctype'       =>'multipart/form-data',
        ]
    ]
); ?>
    <input type="hidden" name="CompromisoPresupuestal[Codigo]" value="<?php echo $CodigoProyecto ?>">
    <div class="modal-body">
        <div class="form-horizontal">
            <div class="form-group">
                <h2>Requerimiento Nº <?php echo $Requerimiento->Correlativo.'-'.$Requerimiento->Annio ?></h2>
            </div>
            <!--
            <div class="form-group">
                <label class="col-sm-1 control-label">Documento:<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <input class="form-control" type="file" name="CompromisoPresupuestal[archivo]" id="requerimiento-archivo">
                </div>
            </div>
            -->
        </div>
        <div class="form-horizontal">
            <div class="form-group">
                <button type="button" id="agregar_fuente" class="btn btn-primary">Agregar Planilla</button><br><br>
                <table class="table borderless table-hover" id="detalle_tabla" border="0">
                    <thead>
                        <tr>
                            <th class="col-sm-4">Planilla</th>
                            <th width="22">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr id='detalle_addr_1'></tr>
                    </tbody>
                </table>
            </div>
        </div>
        
            
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button id="btn-save-desembolso" type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>

<?php
    $correlativos=Yii::$app->getUrlManager()->createUrl('compromiso-presupuestal/correlativo-gasto');
?>
<script>
    
    var $form = $('#frmCompromisoPresupuestal');
    var formParsleyCompromisoPresupuestal = $form.parsley(defaultParsleyForm());
    detalle=1;
    $("#agregar_fuente").click(function(){
        var proveedores=$('input[name=\'CompromisoPresupuestal[Proveedores][]\']').length;
        var CodigoProyecto = '<?php echo $CodigoProyecto ?>';
        var error = '';
        if (error != '') {
            $("#error_fuente_origen").html(error);
                return false;
        }
        else
        {
            $.ajax({
                url:'viatico/carga-planilla?CodigoProyecto='+CodigoProyecto,
                async:false,
                data:{},
                beforeSend:function()
                {
                    $('.panel-default').LoadingOverlay("show");
                },
                success:function(result)
                {
                        var option = null;
                        $('#detalle_addr_'+detalle).html(
                            '<td>'+
                                '#'+
                            '</td>'+
                            '<td>'+
                                '<select class="form-control col-sm-4"  name="RequerimientoViatico[ViaticoID][]" id="compromisopresupuestal_correlativo_'+detalle+'">'+result+'</select>'+
                            '</td>');
                        $('#detalle_tabla').append('<tr id="detalle_addr_'+(detalle+1)+'"></tr>');
                        detalle++;
                        return true;
                },
                error:function(){
                    $('.panel-default').LoadingOverlay("hide",true);
                    alert('Error al realizar el proceso de busqueda.');
                }
            });
            
        }
    });
    
    $("#detalle_tabla").on('click','.eliminar',function(){
        var r = confirm("Estas seguro de Eliminar?");
        var mensaje = '';
        if (r == true) {
            id=$(this).children().val();
            if (id) {
                
                $(this).parent().parent().remove();
            }
            else
            {
                $(this).parent().parent().remove();    
            }   
            mensaje = "Se elimino el Registro Correctamente";
        } 
    });
    
    function Correlativo(valor,correlativo) {
        
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $correlativos ?>',
            type: 'POST',
            async: false,
            data: {TipoGasto:valor,_csrf: toke},
            success: function(data){
               $( "#compromisopresupuestal_correlativo_"+correlativo+"" ).html( data );
            }
        });
    }
</script>