
<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay_progress.min.js"></script>
<script src=" //cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>


<div id="page-content" style="min-height: 1342px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Solicitud de Pagos</h1>
        </div>
        
        <div class="container">
            <div class="form-group">
                <button data-codigo-proyecto="<?= $CodigoProyecto ?>" class="btn btn-crear-solicitud-pago">Solicitar Pago</button>
            </div>
            <div class="table-responsive">
                <table id="proyectos" class="display table table-bordered table-striped table-condensed table-hover" cellspacing="0" >
                    <thead>
                        <tr>
                            <th>Memorando</th>
                            <th>Fecha Registro</th>
                            <th>Monto total</th>
                            <th>Documento</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-ctrl-solicitud-pago" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width: 1200px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Solicitud de pago</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>


<script>
    function js_buscar()
    {
       
    }
    var tblresultjs;
        var configDTjs={
            "order": [[ 2, "desc" ]],
                "language": {
                    "sProcessing":    "Procesando...",
                    "sLengthMenu":    "Mostrar _MENU_ registros",
                    "sZeroRecords":   "No se encontraron resultados",
                    "sEmptyTable":    "Ningún dato disponible en esta lista",
                    "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":   "",
                    "sSearch":        "Buscar:",
                    "sUrl":           "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":    "Último",
                        "sNext":    "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
        };
    $(document).ready(function() {
        var CodigoProyecto="<?= $CodigoProyecto ?>";
        $.ajax({
            url:'compromiso-presupuestal/lista-compromisos?CodigoProyecto='+CodigoProyecto,
            async:false,
            data:{},
            beforeSend:function()
            {
                $('.panel-default').LoadingOverlay("show");
            },
            success:function(result)
            {
                    //$tblresultjs.destroy();//
                    $("#proyectos tbody").html(result);
                    $('#proyectos').DataTable(configDTjs);
                    //tblresultjs=$('#example').DataTable(configDTjs);
                    $('.panel-default').LoadingOverlay("hide",true);
            },
            error:function(){
                $('.panel-default').LoadingOverlay("hide",true);
                alert('Error al realizar el proceso de busqueda.');
            }
        });
        
    } );
    
    $('body').on('click', '.btn-crear-compromiso-presupuestal', function (e) {
        e.preventDefault();
        var CodigoProyecto = $(this).attr('data-codigo-proyecto');
        $('#modal-ctrl-compromiso-presupuestal .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/compromiso-presupuestal/crear?CodigoProyecto='+CodigoProyecto);
        $('#modal-ctrl-compromiso-presupuestal').modal('show');
    });
</script>