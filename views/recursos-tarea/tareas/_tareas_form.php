<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'recursos-tarea/tareas-crear',
        'options'           => [
            'autocomplete'  => 'off',
            'id'            => 'frmTarea'
        ]
    ]
); ?>
    <input type="hidden" name="Tarea[ActividadID]" value="<?php echo $idactividad ?>">
    <div class="modal-body">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label">Descripción <span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="Tarea[Descripcion]">
                    <!--<h6 class="pull-left count-message"></h6>-->
                </div>
            </div>
	    <!--
            <div class="form-group">
                <label class="col-sm-3 control-label">Peso <span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="number" class="form-control" name="Tarea[Peso]">
                    
                </div>
            </div>
	    -->
            <hr>

            <div class="form-group">
                <label class="col-sm-3 control-label">Unidad de medida <span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="Tarea[UnidadMedida]"></textarea>
                    <!--<h6 class="pull-left count-message"></h6>-->
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Meta Fisica <span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="number" class="form-control" name="Tarea[MetaFisica]">
                    <!--<h6 class="pull-left count-message"></h6>-->
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button id="btn-save-tarea" type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>


<script>
    var $form = $('#frmTarea');
    var formParsleyTarea = $form.parsley(defaultParsleyForm());

    $(document).ready(function () {
        $('.count-message').each(function () {
            var $this = $(this);
            var $divformgroup = $this.closest('div.form-group');
            var $txt = $divformgroup.find('textarea, input[type="text"]');
            var text_max = parseInt($txt.attr('maxlength'));

            $txt.keyup(function () {
                var text_length = $txt.val().length;
                var text_remaining = text_max - text_length;
                $this.html(text_remaining + ' caracteres restantes');
            });
        });
    });
</script>