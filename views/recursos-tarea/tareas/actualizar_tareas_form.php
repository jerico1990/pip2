<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
	'action'            => 'recursos-tarea/actualizar-tarea?id='.$id,
        'options'           => [
            'autocomplete'  => 'off',
            'id'            => 'frmTarea'
        ]
    ]
); ?>
    
    <div class="modal-body">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label">Descripción <span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="Tarea[Descripcion]" value="<?= $tarea->Descripcion ?>">
                    <!--<h6 class="pull-left count-message"></h6>-->
                </div>
            </div>
	    <!--
            <div class="form-group">
                <label class="col-sm-3 control-label">Peso <span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="number" class="form-control" name="Tarea[Peso]" value="<?= $tarea->Peso ?>">
                    
                </div>
            </div>
	    -->
            <hr>

            <div class="form-group">
                <label class="col-sm-3 control-label">Unidad de medida <span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="Tarea[UnidadMedida]"><?= $tarea->UnidadMedida ?></textarea>
                    <!--<h6 class="pull-left count-message"></h6>-->
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Meta Fisica <span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="number" class="form-control" name="Tarea[MetaFisica]" value="<?= $tarea->MetaFisica ?>">
                    <!--<h6 class="pull-left count-message"></h6>-->
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button id="btn-save-tarea" type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>


<script>
    var $form = $('#frmTarea');
    var formParsleyTarea = $form.parsley(defaultParsleyForm());

    $(document).ready(function () {
        $('.count-message').each(function () {
            var $this = $(this);
            var $divformgroup = $this.closest('div.form-group');
            var $txt = $divformgroup.find('textarea, input[type="text"]');
            var text_max = parseInt($txt.attr('maxlength'));

            $txt.keyup(function () {
                var text_length = $txt.val().length;
                var text_remaining = text_max - text_length;
                $this.html(text_remaining + ' caracteres restantes');
            });
        });


    });
    /*
    $(document).on('click','#btn-save-tarea',function (e) {
        e.preventDefault();
        var $btn = $(this);
        $btn.attr('disabled', 'disabled');
        var isValid = formParsleyTarea.validate();
        if (isValid) {
            sendFormNoMessage($('#frmTarea'), $(this), function (dato) {
                 $("#div-presupuesto").html('');
                getProyecto().done(function(json) {
                    _proyecto = json;
                    _drawTable();
                });
                $("#modal-ctrl-tarea").modal('hide');
                _pageLoadingEnd();


            });
        } else {
            $btn.removeAttr('disabled');
        }
    });*/
    
</script>