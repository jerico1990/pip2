<?php
use app\models\Actividad;
use app\models\CronogramaTarea;
use app\models\Tarea;
use app\models\CronogramaProyecto;

/** Error reporting */
error_reporting(E_ALL);

/** Include path **/
ini_set('include_path', ini_get('include_path').';../web/PHPExcel/Classes/');
//require(__DIR__ . '/../../web/PHPExcel/Classes/');
/** PHPExcel */
require_once 'PHPExcel.php';

/** PHPExcel_Writer_Excel2007 */
include 'PHPExcel/Writer/Excel2007.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
$sheet = $objPHPExcel->getActiveSheet();
// Set properties
// Add some data
/*
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Hello');
$objPHPExcel->getActiveSheet()->SetCellValue('B2', 'world!');
$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Hello');
$objPHPExcel->getActiveSheet()->SetCellValue('D2', 'world!');
*/
// Rename sheet

$objPHPExcel->getActiveSheet()->setTitle('Tareas');

$Meses=[];
$objPHPExcel->setActiveSheetIndex(0);

$sheet->setCellValueByColumnAndRow(0, 1, "TAREAS");

$sheet->getStyle('A1')->getAlignment()->applyFromArray(
    array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    )
);
$sheet->getRowDimension('1')->setRowHeight(20);
$sheet->getStyle('A1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('c5c5c5');
$sheet->getStyle('A1')->getFont()->setBold(true);

$styleArray = array(
      'borders' => array(
          'allborders' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
          )
      )
  );  // Bordes


// -----------------------------------------------------------------------------
$sheet->SetCellValue('A3', '#');
$sheet->SetCellValue('B3', 'OE/Act/Recur');
$sheet->SetCellValue('C3', 'Unidad Medida');
$sheet->SetCellValue('D3', 'MetaFisica');
//$sheet->SetCellValue('E3', 'Costo Unitario');
//$sheet->SetCellValue('F3', 'Total Proyecto');

$sheet->getRowDimension('3')->setRowHeight(30);
$sheet->getColumnDimension('A')->setAutoSize(true);
$sheet->getColumnDimension('B')->setAutoSize(true);
$sheet->getColumnDimension('C')->setAutoSize(true);
$sheet->getColumnDimension('D')->setAutoSize(true);
//$sheet->getColumnDimension('E')->setAutoSize(true);
//$sheet->getColumnDimension('F')->setAutoSize(true);
$sheet->getStyle('A3:D3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$sheet->getStyle('A3:D3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


foreach(CronogramaProyecto::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->all() as $mes)
{
    array_push ( $Meses , 'Mes '.$mes->Mes );
}

$sheet->fromArray($Meses,null,'E3');


$highestColumm = $objPHPExcel->setActiveSheetIndex(0)->getHighestColumn();
$sheet->mergeCells('A1:'.$highestColumm."1");  // Combinar columna
$sheet->getStyle('A1:'.$highestColumm.'1')->applyFromArray($styleArray);
$sheet->getStyle('E3:'.$highestColumm."3")->getAlignment()->applyFromArray(
    array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // alineacion horizontal
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER, // alineacion vertical
    )
);
$sheet->getStyle('A3:'.$highestColumm."3")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('c5c5c5'); // Color de fondo
$sheet->getStyle('A3:'.$highestColumm."3")->getFont()->setBold(true);  // Negrita
$sheet->getStyle('A3:'.$highestColumm."3")->applyFromArray($styleArray); // Celdas



// echo '<pre>';
// // print_r($objPHPExcel->setActiveSheetIndex(0)->toArray());
// print_r($highestColumm);
// echo '</pre>';
// die();



$i=4;
foreach($componentes as $componente)
{
    $sheet->SetCellValue('A'.$i, ''.$componente->Correlativo.'');
    $sheet->SetCellValue('B'.$i, 'O.E. '.$componente->Nombre);
    $sheet->SetCellValue('C'.$i, '');
    $sheet->SetCellValue('D'.$i, $componente->Cantidad);
    //$sheet->SetCellValue('E'.$i, $componente->CostoUnitario);
    //$sheet->getStyle('F'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $sheet->getStyle('A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    //$sheet->SetCellValue('F'.$i, ($componente->Cantidad*$componente->CostoUnitario));

    // Perzonalizacion
    $sheet->getStyle('A'.$i.':'.$highestColumm.$i)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('c5c5c5');
    $sheet->getStyle('A'.$i.':'.$highestColumm.$i)->getAlignment()->applyFromArray(
    array(
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
        )
    );
    $sheet->getStyle('A'.$i.':'.$highestColumm.$i)->applyFromArray($styleArray);
    // End Perzonalizacion

    $actividades=Actividad::find()->where('ComponenteID=:ComponenteID',[':ComponenteID'=>$componente->ID])->orderBy('Correlativo asc')->all();
    $a=1;
    foreach($actividades as $actividad)
    {
        $sheet->SetCellValue('A'.($a+$i), ''.$componente->Correlativo.'.'.$actividad->Correlativo.'' );
        $sheet->SetCellValue('B'.($a+$i), 'Act. '.$actividad->Nombre);
        $sheet->SetCellValue('C'.($a+$i), '');
        $sheet->SetCellValue('D'.($a+$i), $actividad->Cantidad);
        //$sheet->SetCellValue('E'.($a+$i), $actividad->CostoUnitario);
        //$sheet->SetCellValue('F'.($a+$i), ($actividad->Cantidad*$actividad->CostoUnitario));
        //$sheet->getStyle('F'.($a+$i))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A'.($a+$i))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        // Perzonalizacion
        $sheet->getStyle('A'.($a+$i).':'.$highestColumm.($a+$i))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ddf4fa');
        $sheet->getStyle('A'.($a+$i).':'.$highestColumm.($a+$i))->getAlignment()->applyFromArray(
        array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );
        $sheet->getStyle('A'.($a+$i).':'.$highestColumm.($a+$i))->applyFromArray($styleArray);
        // End Perzonalizacion

        $tareas=Tarea::find()
                ->where('Tarea.ActividadID=:ActividadID',[':ActividadID'=>$actividad->ID])
                ->all();
        $r=1;
        foreach($tareas as $tarea)
        {
            $sheet->SetCellValue('A'.($a+$i+$r), ''.$componente->Correlativo.'.'.$actividad->Correlativo.'.'.$r.'' );
            $sheet->SetCellValue('B'.($a+$i+$r), 'Tareas. '.$tarea->Descripcion);
            
            $sheet->SetCellValue('C'.($a+$i+$r), $tarea->UnidadMedida);
            $sheet->getStyle('C'.($a+$i+$r))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
            $sheet->SetCellValue('D'.($a+$i+$r), $tarea->MetaFisica);
            $sheet->getStyle('D'.($a+$i+$r))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            //$sheet->SetCellValue('E'.($a+$i+$r), '');
            //$sheet->getStyle('E'.($a+$i+$r))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            //$sheet->SetCellValue('F'.($a+$i+$r), '');
            //$sheet->getStyle('F'.($a+$i+$r))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


            // Perzonalizacion
            $sheet->getStyle('A'.($a+$i+$r).':'.$highestColumm.($a+$i+$r))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('fcf3d0');
            $sheet->getStyle('A'.($a+$i+$r).':'.$highestColumm.($a+$i+$r))->getAlignment()->applyFromArray(
            array(
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                )
            );
            $sheet->getStyle('A'.($a+$i+$r).':'.$highestColumm.($a+$i+$r))->applyFromArray($styleArray);

            $sheet->getStyle('E'.($a+$i+$r).':'.$highestColumm.($a+$i+$r))->getAlignment()->applyFromArray(
                array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // alineacion horizontal
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER, // alineacion vertical
                )
            );

            // End Perzonalizacion

            $cronogramaTareas=CronogramaTarea::find()->where('TareaID=:TareaID',[':TareaID'=>$tarea->ID])->all();
            $cronogramaTareasMetaFisica=[];
            foreach($cronogramaTareas as $cronogramaTarea)
            {
                array_push ( $cronogramaTareasMetaFisica , $cronogramaTarea->MetaFisica );
            }
            $sheet->fromArray($cronogramaTareasMetaFisica,null,'E'.($a+$i+$r));
            $sheet->getStyle('E'.($a+$i+$r))->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

            $r++;
        }
        $a=$r+$a;
    }
    //$sheet = [['#','Objetivo Especifico '.$i.'']];
    //$objPHPExcel->setActiveSheetIndex($index);
    //$objPHPExcel->getActiveSheet()->fromArray($sheet,'A'.$i);
    //$index++;
    $i=$i+$a;
}



// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Tareas.xls"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
