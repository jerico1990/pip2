<div id="page-content" style="min-height: 885px;">
    <div id="wrap">
        <div id="page-heading">
            <h1></h1>
        </div>
        <div class="container">
                    
            <style>
                #page-heading {
                    display: none;
                }

                .tab-content {
                    background-color: #fff;
                }

                .panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 1px solid #a8a8ab !important;
                }

                .panel-gray .panel-body {
                    border-top: none !important;
                }

                .panel-heading {
                    height: auto !important;
                }
            </style>

            <div class="tab-container">
                <ul class="nav nav-tabs">
                    <li id="li-poaf" class="active"><a href="#tab-poaf" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Tareas</a></li>
                    <!-- <li id="li-pc"><a href="#container-pc" data-toggle="tab"><i class="fa fa-pencil"></i>&nbsp;Pasos Críticos</a></li> -->
                </ul>
                <div class="tab-content">

                    <div class="tab-pane active" id="tab-poaf">
                        <div id="container-poaf">

                            <style>
                                #container-poaf .input-number {
                                    text-align: right;
                                    min-width: 72px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }

                                #container-poaf .input-number-sm {
                                    text-align: right;
                                    min-width: 52px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }

                                #container-poaf .input-number-md {
                                    text-align: right;
                                    min-width: 82px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }
                                
                                #container-poaf table-act > tbody > tr > td:nth-child(1) {
                                    text-align: center;
                                }

                                #container-poaf .table-act > tbody > tr > td:nth-child(3) {
                                    min-width: 320px !important;
                                }

                                #container-poaf .table-act > tbody > tr > td:nth-child(2) {
                                    min-width: 45px !important;
                                }
                                
                                #container-poaf .table-act > tbody > tr > td:nth-child(4) {
                                    min-width: 100px !important;
                                }
                                #container-poaf .table-act > tbody > tr > td:nth-child(5) {
                                    min-width: 100px !important;
                                }
                                #container-poaf .table-act > tbody > tr > td:nth-child(6) {
                                    min-width: 80px !important;
                                }
                                #container-poaf .table-act > tbody > tr > td:nth-child(7) {
                                    min-width: 100px !important;
                                }
                                #container-poaf .table > tbody > .tr-header > td {
                                    min-width: 60px;
                                    text-align: center;
                                    font-size: 12px;
                                    font-weight: bold;
                                    padding: 1px !important;
                                    vertical-align: middle !important;
                                    /*border: 1px solid #cfcfd0;
                                    background-color: #f0f0f1;*/
                                    border: 1px solid #c0c0c0;
                                    background-color: #e3e3e3;
                                }

                                #container-poaf h4.panel-title {
                                    font-size: 16px;
                                }

                                #container-poaf .tr-comp td, .tr-proy td {
                                    font-weight: bold;
                                }

                                #container-poaf .table-condensed td {
                                    padding: 1px !important;
                                }

                                #container-poaf td, #table-container input {
                                    font-size: 13px !important;
                                }

                                #container-poaf .form-control[readonly] {
                                    background-color: #f2f3f4;
                                }
                                
                                .mes
                                {
                                    min-width: 50px !important;
                                }
                            </style>

                            <br>

                            <div id="table-container">
                            </div>

                            <br>
                            <div class="text-center">
                                <?php /*if(\Yii::$app->user->identity->SituacionProyecto==1){?>
                                <button class="btn btn-primary btn-reprogramacion-tareas"><i class="fa fa-check"></i>&nbsp;Reprogramación de Tareas</button>
                                <?php }*/ ?>
                                <button class="btn btn-primary btn-enviar-tareas"><i class="fa fa-check"></i>&nbsp;Validar programación de Tareas</button>
                                <a class="btn btn-inverse-alt" href="<?= Yii::$app->getUrlManager()->createUrl('recursos-tarea/descargar-tarea?codigo='.$usuario->username.'') ?>">
                                    <i class="fa fa-download"></i>&nbsp;Descargar Cronograma Técnico
                                </a>
                            </div> 

                            <script>
                                var _proyecto = new Object();

                                $(document).ready(function () {
                                        $('#table-container').html('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>');
                                    getProyecto().done(function(json) {
                                        _proyecto = json;
                                        _drawTable();
                                    });

                                });

                                $('.btn-save-poaf').click(function (e) {
                                    e.preventDefault();
                                    _pageLoadingStart();
                                    var arrPoafMFIS = [];
                                    var arrPoafMFIN = [];
                                    $('.input-poaf-crono-act-mfis[changed="changed"]').each(function () {
                                        var $this = $(this);
                                        var valor = parseFloat($this.inputmask('unmaskedvalue'));
                                        if (!isNaN(valor)) {
                                            arrPoafMFIS.push({
                                                ID: $this.attr('data-crono-act-id'),
                                                PoafMetaFisica: valor
                                            });
                                        }
                                    });
                                    $('.input-poaf-crono-arescat-mfin[changed="changed"]').each(function () {
                                        var $this = $(this);
                                        var valor = parseFloat($this.inputmask('unmaskedvalue'));
                                        if (!isNaN(valor)) {
                                            arrPoafMFIN.push({
                                                ID: $this.attr('data-crono-arescat-id'),
                                                PoafMetaFinanciera: valor,
                                                Mes: $this.attr('data-crono-arescat-mes'),
                                                AreSubCategoriaID: $this.attr('data-crono-arescat-arescatid'),
                                            });
                                        }
                                    });
                                    $('.input-poaf-crono-act-mfis[changed="changed"]').removeAttr('changed');
                                    $('.input-poaf-crono-act-mfin[changed="changed"]').removeAttr('changed');
                                    if (arrPoafMFIS.length || arrPoafMFIN.length) {
                                        var _obj = {
                                            CronogramasActividad: arrPoafMFIS,
                                            CronogramasAreSubCategoria: arrPoafMFIN
                                        };
                                        $.ajax({
                                            cache: false,
                                            type: 'POST',
                                            contentType: 'application/json;charset=utf-8',
                                            dataType: 'json',
                                            url: '/SLFC/PasoCritico/SavePoaf',
                                            data: JSON.stringify(_obj),
                                            success: function (data) {
                                                if (data.Success) {
                                                    toastr.success('Se registró correctamente');
                                                    loadPoaf();
                                                    _pageLoadingEnd();
                                                } else {
                                                    toastr.error(data.Error);
                                                }
                                            },
                                            error: function () {
                                                _pageLoadingEnd();
                                            }
                                        });
                                    } else {
                                        _pageLoadingEnd();
                                    }
                                });
                                function getNum(val) {
                                    if (val == '0.00' || val == '') {
                                         return 0;
                                    }
                                    var val2 = val.replace(',','');
                                    return val2;
                                }
    
                                $('body').on('change', '.btn-save-cronograma-tarea', function (e) {
                                    var $this = $(this);
                                    var ID=$this.attr('data-cronograma-tarea-id');
                                    var TareaID=$this.attr('data-tarea-id');
                                    var TareaMetaFisica=parseFloat($this.attr('data-meta-fisica'));
                                    
                                    suma=0;
                                    $('.tarea_'+TareaID).each(function(){
                                        suma = suma+parseFloat(getNum($(this).val()));
                                    });
                                    console.log(suma.toFixed(2));
                                    if (TareaMetaFisica<suma.toFixed(2)) {
                                        bootbox.alert('La cantidad supera a la meta fisica general.');
                                        $this.val('0.00');
                                        return false;
                                    }
                                    if (parseFloat(TareaMetaFisica)==suma.toFixed(2))
                                    {
                                        $('.tarea_alerta_'+TareaID).css("color", "black");
                                    }
                                    else
                                    {
                                        $('.tarea_alerta_'+TareaID).css("color", "red");
                                    }
                                    var MetaFisica=$this.val();
                                    $.ajax({
                                        cache: false,
                                        type: 'GET',
                                        url: '<?= \Yii::$app->request->BaseUrl ?>/recursos-tarea/grabar-meta-cronograma-tarea',
                                        data: {ID:ID,MetaFisica:MetaFisica},
                                        success: function (data) {
                                            if (data.Success) {
                                                //toastr.success('Se registró correctamente');
                                                loadPoaf();
                                                _pageLoadingEnd();
                                            } else {
                                                //toastr.error(data.Error);
                                            }
                                        },
                                        error: function () {
                                            _pageLoadingEnd();
                                        }
                                    });
                                    return true;
                                });
                                
                                
                                $('body').on('change', '.btn-save-cronograma-actividad', function (e) {
                                    var $this = $(this);
                                    var ID=$this.attr('data-cronograma-actividad-id');
                                    var ActividadID=$this.attr('data-actividad-id');
                                    var ActividadMetaFisica=$this.attr('data-meta-fisica');
                                    suma=0;
                                    $('.actividad_'+ActividadID).each(function(){
                                        suma = suma+parseFloat(getNum($(this).val()));
                                    });
                                    
                                    if (parseFloat(ActividadMetaFisica)<suma) {
                                        bootbox.alert('La cantidad supera a la meta fisica general.');
                                        $this.val('0.00');
                                        return false;
                                    }
                                    if (parseFloat(ActividadMetaFisica)==suma.toFixed(2))
                                    {
                                        $('.actividad_alerta_'+ActividadID).css("color", "black");
                                    }
                                    else
                                    {
                                        $('.actividad_alerta_'+ActividadID).css("color", "red");
                                    }
                                    
                                    var MetaFisica=$this.val();
                                    $.ajax({
                                        cache: false,
                                        type: 'GET',
                                        url: '<?= \Yii::$app->request->BaseUrl ?>/recursos-tarea/grabar-meta-cronograma-actividad',
                                        data: {ID:ID,MetaFisica:MetaFisica},
                                        success: function (data) {
                                            var jx = JSON.parse(data);
                                            if (jx.Success == true) {
                                                $this.css('background', 'green');
                                            //if (data.Success) {
                                                //toastr.success('Se registró correctamente');
                                                //loadPoaf();
                                                _pageLoadingEnd();
                                            } else {
                                                //toastr.error(data.Error);
                                            }
                                        },
                                        error: function () {
                                            _pageLoadingEnd();
                                        }
                                    });
                                    return true;
                                });
                                
                                $('body').on('click', '.btn-enviar-tareas', function (e) {
                                    e.preventDefault();
                                    var $this = $(this);
                                    
                                    var error='';
                                    $('.tarea-meta-fisica').each(function(){
                                        var TareaID=$(this).attr('data-tarea-id');
                                        var TareaMetaFisica=parseFloat($(this).attr('data-meta-fisica'));
                                        var suma=0;
                                        $('.tarea_'+TareaID).each(function(){
                                            suma = suma+parseFloat(getNum($(this).val()));
                                        });
                                        if (TareaMetaFisica.toFixed(2)>suma.toFixed(2)) {
                                            error=error+'a';
                                        }
                                    });
                                    
                                    if (error!='') {
                                        bootbox.alert('Falta programar las tareas');
                                        return false;
                                    }
                                    bootbox.alert('Su programación de tareas, esta correcta');
                                    return true;
                                    
                                    
                                });
                                
                                $('body').on('change', '.input-poaf-crono-act-mfis', function (e) {
                                    e.preventDefault();
                                    $(this).attr('changed', 'changed');
                                    
                                });
                                
                                
                                $('body').on('click', '.btn-aprobar-tarea', function (e) {
                                    var $this = $(this);
                                    var ObservacionID=$this.attr('data-id');
                                    bootbox.confirm({
                                        title: '<span style="text-align:center">PNIA - OBSERVACIÓN</span>',
                                        message: 'Está seguro de aprobar la observación ?.',
                                        buttons: {
                                            'cancel': {
                                                label: 'CANCELAR'
                                            },
                                            'confirm': {
                                                label: 'APROBAR OBSERVACIÓN'
                                            }
                                        },
                                        callback: function (confirmed) {
                                            if (confirmed) {
                                                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                                                $.ajax({
                                                    url: '<?= Yii::$app->getUrlManager()->createUrl('recursos-tarea/aprobar-observacion') ?>',
                                                    type: 'GET',
                                                    async: false,
                                                    data: {ObservacionID:ObservacionID,_csrf: toke},
                                                    success: function(data){
                                                       
                                                    }
                                                });
                                            } 
                                        }
                                    });
                                });
                                
                                
                                $('body').on('click', '.input-sub-total', function (e) {
                                    e.preventDefault();
                                    var $this = $(this);
                                    var acumusubtotal = 0.00;
                                    $this.closest('.tr-sub-tot').find('.input-sub-total').each(function () {
                                        var _valor = parseFloat($(this).inputmask('unmaskedvalue'));
                                        if (!isNaN(_valor)) {
                                            acumusubtotal += _valor;
                                        }
                                    });
                                    $this.closest('.tr-sub-tot').find('.input-act-tot-mfs').val(acumusubtotal);
                                });

                                $('body').on('click', '.input-sub2-total', function (e) {

                                    e.preventDefault();
                                    var $this = $(this);
                                    var acumusubtotal = 0.00;
                                    $this.closest('.tr-sub2-tot').find('.input-sub2-total').each(function () {

                                        var _valor = parseFloat($(this).inputmask('unmaskedvalue'));
                                        if (!isNaN(_valor)) {
                                            acumusubtotal += _valor;
                                        }
                                    });
                                    $this.closest('.tr-sub2-tot').find('.input-act-tot-mfss').val(acumusubtotal);
                                    //$(this).attr('changed-poaf-crono-arescat-mf', '');
                                });

                                $('body').on('change', '.input-poaf-crono-arescat-mfin', function (e) {
                                    e.preventDefault();
                                    $(this).attr('changed', 'changed');
                                    
                                });


                                

                                

                                function getProyecto() {
                                    var url = "<?= \Yii::$app->request->BaseUrl ?>/recursos-tarea/tareas-json?codigo=<?= $usuario->username ?>";;
                                    return $.getJSON( url );
                                }

                                function _drawTable() {
                                   var situacionProyecto=<?= \Yii::$app->user->identity->SituacionProyecto ?>;
                                    var html = '';
                                    var cantmeses = _proyecto.Cronogramas.length;
                                    var situacionTarea=<?= \Yii::$app->user->identity->SituacionTarea ?>;
                                    for (var i_comp = 0; i_comp < _proyecto.Componentes.length; i_comp++) {
                                        var comp = _proyecto.Componentes[i_comp];
                                        // console.log(comp);
                                        html += '<div class="panel panel-gray"><div class="panel-heading">'
                                                + '<span class="pull-left"><h4 class="panel-title">'
                                                + (i_comp + 1) + '. Objetivo específico - ' + comp.Nombre
                                                + '</h4></span><span class="pull-right">Peso: '+comp.Peso+'</span></div>';
                                        html += '<div class="panel-body">';
                                        html += '<div class="table-responsive">';
                                        html += '<table class="table table-bordered table-condensed table-act">';
                                        html += '<tbody>';
                                        for (var i_act = 0; i_act < comp.Actividades.length; i_act++)
                                        {
                                            var act = comp.Actividades[i_act];
                                            html += '<tr class="tr-header">';
                                            html += '<td>Peso</td><td>#</td><td>Actividad/Tarea</td><td>Unidad de medida</td><td>Meta física</td>';
                                            // Cronogramas
                                            
                                            for (var i_crono_act = 0; i_crono_act < cantmeses; i_crono_act++) {
                                                html += '<td class="mes"> Mes ' + act.Cronogramas[i_crono_act].MesDescripcion + '</td>';
                                            }
                                            // Sumatorias
                                            
                                            html += '<tr class="tr-act-mf info" data-act-id="' + act.ID + '">';
                                            html += '<td align="center">'+act.Peso+'</td><td>' + (i_comp + 1) + '.' + (i_act + 1) + '</td>'
                                                    + '<td><b>' + act.Nombre + '</b></td><td>' + act.UnidadMedida
                                                    + '</td><td>'+act.MetaFisica+ '</td>';
                                            for (var i_crono_act = 0; i_crono_act < cantmeses; i_crono_act++) {
                                                var deshabilitar="disabled";
                                                //if ((act.Situacion==0 || act.Situacion==3) && (situacionTarea==0 || situacionTarea==3)) {
                                                if ((act.Situacion==0 || act.Situacion==3)) {
                                                    deshabilitar="";
                                                }
                                                html += '<td><input type="text" '+deshabilitar+' data-meta-fisica="'+act.MetaFisica+'" data-actividad-id="'+act.ID+'" class="form-control input-number actividad_'+act.ID+' btn-save-cronograma-actividad input-sub2-total"  '
                                                            + 'data-cronograma-actividad-id="' + act.Cronogramas[i_crono_act].ID + '" value="'+act.Cronogramas[i_crono_act].MetaFisica
                                                            + '" /></td>';
                                            }
                                            //
                                            html += '</tr>';
                                            html += '</tr>';
                                            
                                            // Rubros Elegibles
                                            html += '<tr>';
                                            html += '<td colspan="5"><a href="javascript:void(0);" id="btn-add-tareas" data-act-idd="'+ act.ID +'" style="margin: 5px;" class="btn btn-primary btn-sm btn-actv-tarea"><i class="fa fa-plus"></i>&nbsp;Agregar Tarea</a> </td>';
                                            html += '</tr>';
                                            for (var i_are = 0; i_are < act.Tareas.length; i_are++) {
                                                var are = act.Tareas[i_are];
                                                var alertaTarea="red";
                                                var  metaFisicaTarea1=0;
                                                for (var i_crono_are = 0; i_crono_are < cantmeses; i_crono_are++) {
                                                    if (are.Cronogramas[i_crono_are].MetaFisica!=null) {
                                                        metaFisicaTarea1=parseFloat(metaFisicaTarea1)+parseFloat(are.Cronogramas[i_crono_are].MetaFisica);
                                                    }
                                                }
                                                
                                                if (parseFloat(are.MetaFisica).toFixed(2)==parseFloat(metaFisicaTarea1).toFixed(2)) {
                                                    alertaTarea="black";
                                                }
                                                
                                                
                                                html += '<tr class="tr-sub2-tot success" data-tarea-id="'+i_are+'"  data-meta-fisica="'+are.MetaFisica+'">';
                                                html += '<td align="center">';
                                                if (are.Situacion==0) {
                                                html += '<a href="#" class="btn-edit-tarea" data-toggle="tooltip" title="" data-act-idd="'+act.ID+'" data-id="'+are.ID+'" data-original-title="Editar"><i class="fa fa-edit fa-lg"></i></a>'
                                                        + '<a href="#" class="btn-remove-tarea" data-toggle="tooltip" title="Eliminar" data-act-idd="'+act.ID+'" data-id="'+are.ID+'"><i class="fa fa-remove fa-lg"></i></a>';
                                                }
                                                
                                                if (situacionProyecto==3 && are.Situacion==3) {
                                                    html += '<a href="#" class="btn-edit-tarea" data-toggle="tooltip" title="" data-act-idd="'+act.ID+'" data-id="'+are.ID+'" data-original-title="Editar"><i class="fa fa-edit fa-lg"></i></a>';
                                                    html +='<span data-toggle="tooltip" title="'+are.Observacion+'"><i class="fa fa-eye"></i></span>'
                                                         +' <a href="#" data-toggle="tooltip" data-id="'+are.ObservacionID+'" class="btn-aprobar-tarea"><i class="fa fa-check"></i></a>';
                                                                     
                                                }
                                                
                                                html +='</td><td>' + (i_comp + 1) + '.' + (i_act + 1) + '.' + (i_are + 1) + '</td><td style="color:'+alertaTarea+'" class="tarea_alerta_'+are.ID+'">' + are.Descripcion + '</td><td>'+are.UnidadMedida+'</td><td><input type="text" style="color:'+alertaTarea+';" data-tarea-id="'+are.ID+'" data-meta-fisica="'+are.MetaFisica+'" class="tarea_alerta_'+are.ID+' form-control tarea-meta-fisica input-number" readonly value="' + are.MetaFisica+'" >'
                                                for (var i_crono_are = 0; i_crono_are < cantmeses; i_crono_are++) {
                                                    var deshabilitar="disabled";
                                                    if ((are.Cronogramas[i_crono_are].Situacion==0 || are.Cronogramas[i_crono_are].Situacion==3)) {
                                                        deshabilitar="";
                                                    }
                                                    var cronogramaTarea=are.Cronogramas[i_crono_are];
                                                    html += '<td><input type="text" '+deshabilitar+' data-meta-fisica="'+are.MetaFisica+'" data-tarea-id="'+are.ID+'" class="form-control input-number tarea_'+are.ID+' btn-save-cronograma-tarea input-sub2-total"  '
                                                            + 'data-cronograma-tarea-id="' + cronogramaTarea.ID + '" value="'+cronogramaTarea.MetaFisica
                                                            + '" /></td>';
                                                }
                                                html += '</tr>';
                                                
                                            }
                                        }
                                       
                                        html += '</tbody>';
                                        html += '</table>';
                                        html += '</div>';
                                        html += '</div>';
                                        html += '</div>';
                                    }
                                    
                                    $('#table-container').html(html);
                                    
                                    
                                    if (situacionTarea!=0) {
                                        $(".btn-actv-tarea").hide();
                                    }
                                    
                                    $('.input-number').inputmask("decimal", {
                                        radixPoint: ".",
                                        groupSeparator: ",",
                                        groupSize: 3,
                                        digits: 2,
                                        integerDigits: 7,
                                        autoGroup: true,
                                        allowPlus: false,
                                        allowMinus: true,
                                        placeholder: ''
                                    }).click(function () {
                                        $(this).select();
                                    });
                                }
                            </script>
                        </div>

                    </div>

                </div>
            </div>


            <div class="modal fade" id="modal-ctrl-tarea" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Tarea</h4>
                        </div>
                        <div class="modal-body-main">
                        </div>
                    </div>
                </div>
            </div>


            <div id="modal-add-pc" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body-main">
                        </div>
                    </div>
                </div>
            </div>

            <div id="modal-create-indpc" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Indicador de paso crítico</h4>
                        </div>
                        <div class="modal-body-main">
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>







<script type="text/javascript">
    $('body').on('click', '.btn-actv-tarea', function (e) {
        e.preventDefault();
        var idActv = $(this).attr('data-act-idd');
        $('#modal-ctrl-tarea .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/recursos-tarea/tareas?id='+idActv);
        $('#modal-ctrl-tarea').modal('show');
    });
    
    $('body').on('click', '.btn-edit-tarea', function (e) {
        e.preventDefault();
        var idActv = $(this).attr('data-act-idd');
        var id=$(this).attr('data-id');
        $('#modal-ctrl-tarea .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/recursos-tarea/actualizar-tarea?id='+id);
        $('#modal-ctrl-tarea').modal('show');
    });
    
    $('body').on('click', '.btn-remove-tarea', function (e) {
        e.preventDefault();
        var $this = $(this);
        bootboxConfirmEliminar($this,
            'Está seguro de eliminar la tarea?',
            function () {
                var id = $this.attr('data-id');
                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                $.get('<?= \Yii::$app->request->BaseUrl ?>/recursos-tarea/delete-tarea', { id: id ,_csrf: toke }, function (data) {
                    var jx = JSON.parse(data);
                    if (jx.Success == true) {
                        
                    }
                });
            });
    });


    $(document).on('click','#btn-save-tarea',function (e) {
        e.preventDefault();
        var $btn = $(this);
        $btn.attr('disabled', 'disabled');
        var isValid = formParsleyTarea.validate();
        if (isValid) {
            sendFormNoMessage($('#frmTarea'), $(this), function (dato) {
                 $("#div-presupuesto").html('');
                getProyecto().done(function(json) {
                    _proyecto = json;
                    _drawTable();
                });
                $("#modal-ctrl-tarea").modal('hide');
                _pageLoadingEnd();
            });
        } else {
            $btn.removeAttr('disabled');
        }
    });
    var situacionProyecto=<?= \Yii::$app->user->identity->SituacionProyecto ?>;
    if (situacionProyecto==1 || situacionProyecto==2) {
        $(".btn-enviar-tareas").hide();
    }
    
</script>







<!--
<div id="modal-loading" class="modal fade modal-loading in" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-hidden="false" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content-no-shadow modal-content">
            <div class="text-center"><i class="fa fa-spinner fa-pulse fa-fw" style="color:#16a085;font-size: 100px;"></i></div>
        </div>
    </div>
</div>
-->