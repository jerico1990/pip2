<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'certificacion-presupuestal/crear',
        'options'           => [
            'id'            => 'frmCertificacionPresupuestal',
            'enctype'       =>'multipart/form-data',
        ]
    ]
); ?>
    <div class="modal-body">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-2 control-label">Memorando<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="CertificacionPresupuestal[Memorando]">
                </div>
		<label class="col-sm-2 control-label">Señor(a):<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="CertificacionPresupuestal[JefePnia]">
                </div>
            </div>
	    <div class="form-group">
                <label class="col-sm-2 control-label">Asunto<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="CertificacionPresupuestal[Asunto]">
                </div>
		<label class="col-sm-2 control-label">Referencia<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="CertificacionPresupuestal[Referencia]">
                </div>
            </div>
	    
	    <div class="form-group">
                <label class="col-sm-2 control-label">Meta Presupuestal<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="CertificacionPresupuestal[MetaPresupuestal]">
                </div>
		<label class="col-sm-2 control-label">Clasificación<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="CertificacionPresupuestal[Clasificacion]">
                </div>
		
            </div>
	    <div class="form-group">
		<label class="col-sm-2 control-label">Paso Critico<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <select class="form-control" name="CertificacionPresupuestal[PasoCriticoID]">
			<option></option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
			<option value="5">5</option>
			<option value="6">6</option>
		    </select>
                </div>
		<label class="col-sm-2 control-label">Año<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <select class="form-control" name="CertificacionPresupuestal[Annio]">
			<option></option>
			<option value="2016">2016</option>
			<option value="2017">2017</option>
			<option value="2018">2018</option>
			<option value="2019">2019</option>
		    </select>
                </div>
	    </div>
	    <div class="form-group">
                <label class="col-sm-2 control-label">Descripción<span class="f_req">*</span></label>
                <div class="col-sm-8">
                    <textarea class="form-control" name="CertificacionPresupuestal[Descripcion]"></textarea>
                </div>
            </div>
	    <div class="form-group">
                <button type="button" id="agregar" class="btn btn-primary">Agregar</button><br><br>
                <table class="table borderless table-hover" id="detalle_tabla" border="0">
                    <thead>
                        <tr>
                            <th class="col-sm-6">Cod. Proyecto</th>
                            <th class="col-sm-6">Monto a certificar</th>
                            <th width="22">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr id='detalle_1'></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button id="btn-save-desembolso" type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>

<?php
    $actividades=Yii::$app->getUrlManager()->createUrl('requerimiento/actividades');
    $recursos=Yii::$app->getUrlManager()->createUrl('requerimiento/recursos');
?>
<script>
    
    var $form = $('#frmCertificacionPresupuestal');
    var formParsleyCompromisoPresupuestal = $form.parsley(defaultParsleyForm());
    detalle=1;
    $("#agregar").click(function(){
	//var proveedores=$('input[name=\'SolicitudCompromisoPresupuestal[Proveedores][]\']').length;
	
	var error = '';
	
	if (error != '') {
	    $("#error_fuente_origen").html(error);
            return false;
	}
	else
        {
	    var option = null;
            $('#detalle_'+detalle).html(
                                            '<td>'+
                                                '<input class="form-control col-sm-6" type="text" name="CertificacionPresupuestal[Codigos][]"  >'+
					    '</td>'+
					    '<td>'+
                                                '<input class="form-control col-sm-6" type="text" name="CertificacionPresupuestal[Montos][]">'+
					    '</td>'+
					    
					    '<td>'+
						'<span class="eliminar icon-minus-sign" >'+
						'</span>'+
					    '</td>');
            $('#detalle_tabla').append('<tr id="detalle_'+(detalle+1)+'"></tr>');
            detalle++;
            return true;
        }
    });
    
    $("#detalle_tabla").on('click','.eliminar',function(){
        var r = confirm("Estas seguro de Eliminar?");
        var mensaje = '';
        if (r == true) {
            id=$(this).children().val();
            if (id) {
                
                $(this).parent().parent().remove();
            }
            else
            {
                $(this).parent().parent().remove();    
            }   
            mensaje = "Se elimino el Registro Correctamente";
        } 
    });
    
    function Actividad(valor,detalle) {
        var ComponenteID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $actividades ?>',
            type: 'POST',
            async: false,
            data: {ComponenteID:ComponenteID,_csrf: toke},
            success: function(data){
               $( "#actividad_"+detalle+"" ).html( data );
            }
        });
    }
    
    function Recurso(valor,detalle) {
        var ActividadID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $recursos ?>',
            type: 'POST',
            async: false,
            data: {ActividadID:ActividadID,_csrf: toke},
            success: function(data){
               $( "#recurso_"+detalle+"" ).html( data );
            }
        });
    }
</script>