<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'certificacion-presupuestal/actualizar-certificacion?ID='.$ID,
        'options'           => [
            'id'            => 'frmCertificacionPresupuestal',
            'enctype'       => 'multipart/form-data',
        ]
    ]
); ?>
    <div class="modal-body">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-2 control-label">Memorando<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="CertificacionPresupuestal[Memorando]" value="<?= $certificacion->Memorando ?>" disabled>
                </div>
		<label class="col-sm-2 control-label">Señor(a):<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="CertificacionPresupuestal[JefePnia]" value="<?= $certificacion->JefePnia ?>" disabled>
                </div>
            </div>
	    <div class="form-group">
                <label class="col-sm-2 control-label">Asunto<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="CertificacionPresupuestal[Asunto]" value="<?= $certificacion->Asunto ?>" disabled>
                </div>
		<label class="col-sm-2 control-label">Referencia<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="CertificacionPresupuestal[Referencia]" value="<?= $certificacion->Referencia ?>" disabled>
                </div>
            </div>
	    
	    <div class="form-group">
                <label class="col-sm-2 control-label">Meta Presupuestal<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="CertificacionPresupuestal[MetaPresupuestal]" value="<?= $certificacion->MetaPresupuestal ?>" disabled>
                </div>
		<label class="col-sm-2 control-label">Clasificación<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="CertificacionPresupuestal[Clasificacion]" value="<?= $certificacion->Clasificacion ?>" disabled>
                </div>
		
            </div>
	    <div class="form-group">
		<label class="col-sm-2 control-label">Paso Critico<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <select class="form-control" name="CertificacionPresupuestal[PasoCriticoID]" disabled>
			<option></option>
			<option value="1" <?= ($certificacion->PasoCriticoID==1)?'selected':'';?> >1</option>
			<option value="2" <?= ($certificacion->PasoCriticoID==2)?'selected':'';?> >2</option>
			<option value="3" <?= ($certificacion->PasoCriticoID==3)?'selected':'';?> >3</option>
			<option value="4" <?= ($certificacion->PasoCriticoID==4)?'selected':'';?> >4</option>
			<option value="5" <?= ($certificacion->PasoCriticoID==5)?'selected':'';?> >5</option>
			<option value="6" <?= ($certificacion->PasoCriticoID==6)?'selected':'';?> >6</option>
		    </select>
                </div>
		<label class="col-sm-2 control-label">Año<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <select class="form-control" name="CertificacionPresupuestal[Annio]" disabled>
			<option></option>
			<option value="2016" <?= ($certificacion->Annio=="2016")?'selected':'';?>>2016</option>
			<option value="2017" <?= ($certificacion->Annio=="2017")?'selected':'';?>>2017</option>
			<option value="2018" <?= ($certificacion->Annio=="2018")?'selected':'';?>>2018</option>
			<option value="2019" <?= ($certificacion->Annio=="2019")?'selected':'';?>>2019</option>
		    </select>
                </div>
	    </div>
	    <div class="form-group">
                <label class="col-sm-2 control-label">Descripción<span class="f_req">*</span></label>
                <div class="col-sm-8">
                    <textarea class="form-control" name="CertificacionPresupuestal[Descripcion]" disabled><?= $certificacion->Descripcion ?></textarea>
                </div>
            </div>
	    <div class="form-group">
                <table class="table borderless table-hover" id="detalle_tabla" border="0">
                    <thead>
                        <tr>
                            <th class="col-sm-6">Cod. Proyecto</th>
                            <th class="col-sm-6">Monto a certificar</th>
			    <th class="col-sm-6">Código de Certificación</th>
                            <th width="22">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
			<?php $i=1; ?>
			<?php foreach($detallesCertificaciones as $detalleCertificacion){ ?>
			    <tr id='detalle_<?= $i ?>'>
				<td><input disabled class="form-control col-sm-6" type="text" name="CertificacionPresupuestal[Codigos][]" value="<?= $detalleCertificacion->Codigo ?>"></td>
				<td><input disabled class="form-control col-sm-6" type="text" name="CertificacionPresupuestal[Montos][]"  value="<?= $detalleCertificacion->Monto ?>"></td>
				<td><input class="form-control col-sm-6" type="text" name="CertificacionPresupuestal[Certificaciones][]"  value="<?= $detalleCertificacion->CodigoCertificacion ?>"></td>
				    <input class="form-control col-sm-6" type="hidden" name="CertificacionPresupuestal[IDs][]"  value="<?= $detalleCertificacion->ID ?>">
			    </tr>
			<?php $i++; ?>
			<?php } ?>
                        <tr id='detalle_<?= $i ?>'></tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button id="btn-save-desembolso" type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>

<?php
    $actividades=Yii::$app->getUrlManager()->createUrl('requerimiento/actividades');
    $recursos=Yii::$app->getUrlManager()->createUrl('requerimiento/recursos');
?>
<script>
    
    var $form = $('#frmCertificacionPresupuestal');
    var formParsleyCompromisoPresupuestal = $form.parsley(defaultParsleyForm());
    detalle=<?= $i ?>;
    $("#agregar").click(function(){
	//var proveedores=$('input[name=\'SolicitudCompromisoPresupuestal[Proveedores][]\']').length;
	
	var error = '';
	
	if (error != '') {
	    $("#error_fuente_origen").html(error);
            return false;
	}
	else
        {
	    var option = null;
            $('#detalle_'+detalle).html(
                                            '<td>'+
                                                '<input class="form-control col-sm-6" type="text" name="CertificacionPresupuestal[Codigos][]"  >'+
					    '</td>'+
					    '<td>'+
                                                '<input class="form-control col-sm-6" type="text" name="CertificacionPresupuestal[Montos][]">'+
					    '</td>'+
					    
					    '<td>'+
						'<span class="eliminar icon-minus-sign" >'+
						'</span>'+
					    '</td>');
            $('#detalle_tabla').append('<tr id="detalle_'+(detalle+1)+'"></tr>');
            detalle++;
            return true;
        }
    });
    
    $("#detalle_tabla").on('click','.eliminar',function(){
        var r = confirm("Estas seguro de Eliminar?");
        var mensaje = '';
        if (r == true) {
            id=$(this).children().val();
            if (id) {
                
                $(this).parent().parent().remove();
            }
            else
            {
                $(this).parent().parent().remove();    
            }   
            mensaje = "Se elimino el Registro Correctamente";
        } 
    });
    
    function Actividad(valor,detalle) {
        var ComponenteID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $actividades ?>',
            type: 'POST',
            async: false,
            data: {ComponenteID:ComponenteID,_csrf: toke},
            success: function(data){
               $( "#actividad_"+detalle+"" ).html( data );
            }
        });
    }
    
    function Recurso(valor,detalle) {
        var ActividadID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $recursos ?>',
            type: 'POST',
            async: false,
            data: {ActividadID:ActividadID,_csrf: toke},
            success: function(data){
               $( "#recurso_"+detalle+"" ).html( data );
            }
        });
    }
</script>