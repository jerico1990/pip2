
<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay_progress.min.js"></script>
<script src=" //cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>


<div id="page-content" style="min-height: 1342px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Certificaciones Presupuestales</h1>
        </div>
        
        <div class="container">
            <div class="form-group">
                <button class="btn btn-crear-certificacion-presupuestal">Certificación Presupuestal</button>
            </div>
            <div class="table-responsive">
                <table id="proyectos" class="display table table-bordered table-striped table-condensed table-hover" cellspacing="0" >
                    <thead>
                        <?php if(\Yii::$app->user->identity->rol==2){ ?>
                        <tr>
                            <th>Plantilla</th>
                            <th>Situación</th>
                            <th>Memorando</th>
                            <th>Paso Critico</th>
                            <th>Monto total</th>
                            <th>Documento</th>
                            <th>Acciones</th>
                        </tr>
                        <?php } elseif (\Yii::$app->user->identity->rol==3){ ?>
                        <tr>
                            <th>Situación</th>
                            <th>Memorando</th>
                            <th>Paso Critico</th>
                            <th>Monto total</th>
                            <th>Documento</th>
                            <th>Acciones</th>
                        </tr>
                        <?php } ?>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
        
        
    </div>
</div>

<div class="modal fade" id="modal-ctrl-certificacion-presupuestal" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width: 800px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Certificación Presupuestal</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>


<script>
    function js_buscar()
    {
       
    }
    var tblresultjs;
        var configDTjs={
            "order": [[ 2, "desc" ]],
                "language": {
                    "sProcessing":    "Procesando...",
                    "sLengthMenu":    "Mostrar _MENU_ registros",
                    "sZeroRecords":   "No se encontraron resultados",
                    "sEmptyTable":    "Ningún dato disponible en esta lista",
                    "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":   "",
                    "sSearch":        "Buscar:",
                    "sUrl":           "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":    "Último",
                        "sNext":    "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
        };
    $(document).ready(function() {
       
        $.ajax({
            url:'certificacion-presupuestal/lista-certificaciones',
            async:false,
            data:{},
            beforeSend:function()
            {
                $('.panel-default').LoadingOverlay("show");
            },
            success:function(result)
            {
                    //$tblresultjs.destroy();//
                    $("#proyectos tbody").html(result);
                    $('#proyectos').DataTable(configDTjs);
                    //tblresultjs=$('#example').DataTable(configDTjs);
                    $('.panel-default').LoadingOverlay("hide",true);
            },
            error:function(){
                $('.panel-default').LoadingOverlay("hide",true);
                alert('Error al realizar el proceso de busqueda.');
            }
        });
        
    } );
    
    $('body').on('click', '.btn-crear-certificacion-presupuestal', function (e) {
        e.preventDefault();
        var CodigoProyecto = $(this).attr('data-codigo-proyecto');
        $('#modal-ctrl-certificacion-presupuestal .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/certificacion-presupuestal/crear');
        $('#modal-ctrl-certificacion-presupuestal').modal('show');
    });
    
    
    
    $('body').on('click', '.btn-remove', function (e) {
        e.preventDefault();
        var $this = $(this);
        bootboxConfirmEliminar($this,
            'Está seguro de eliminar la solicitud de certificación presupuestal?',
            function () {
                var id = $this.attr('data-id');
                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                $.get('<?= \Yii::$app->request->BaseUrl ?>/certificacion-presupuestal/eliminar', { id: id ,_csrf: toke }, function (data) {
                    var jx = JSON.parse(data);
                    if (jx.Success == true) {
                        
                    }
                });
            });
    });
    
    $('body').on('click', '.btn-confirmar-certificados', function (e) {
        e.preventDefault();
        var $this = $(this);
        var id = $this.attr('data-id');
        bootbox.confirm({
            title: '<span style="text-align:center">PNIA - CONFIRMACIÓN</span>',
            message: 'Está seguro de confirmar los códigos presupuestales?',
            buttons: {
                'cancel': {
                    label: 'CANCELAR'
                },
                'confirm': {
                    label: 'ACEPTAR'
                }
            },
            callback: function (confirmed) {
                if (confirmed) {
                    var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                    $.ajax({
                        url: '<?= Yii::$app->getUrlManager()->createUrl('certificacion-presupuestal/aprobar-certificacion-presupuestal') ?>',
                        type: 'GET',
                        async: false,
                        data: {id:id,_csrf: toke},
                        success: function(data){
                           
                        }
                    });
                } 
            }
        });
         
    });
    
    
    $('body').on('click', '.btn-confirmar', function (e) {
        e.preventDefault();
        var $this = $(this);
        var id = $this.attr('data-id');
        bootbox.confirm({
            title: '<span style="text-align:center">PNIA - CONFIRMACIÓN</span>',
            message: 'Está seguro de enviar la certificación presupuestal?',
            buttons: {
                'cancel': {
                    label: 'CANCELAR'
                },
                'confirm': {
                    label: 'ENVIAR'
                }
            },
            callback: function (confirmed) {
                if (confirmed) {
                    var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                    $.ajax({
                        url: '<?= Yii::$app->getUrlManager()->createUrl('certificacion-presupuestal/enviar-certificacion-presupuestal') ?>',
                        type: 'GET',
                        async: false,
                        data: {id:id,_csrf: toke},
                        success: function(data){
                           
                        }
                    });
                } 
            }
        });
         
    });
    
    
    $('body').on('click', '.btn-aprobar', function (e) {
        e.preventDefault();
        var $this = $(this);
        var id = $this.attr('data-id');
        bootbox.confirm({
            title: '<span style="text-align:center">PNIA - APROBAR</span>',
            message: 'Está seguro de aprobar el certificado presupuestal?',
            buttons: {
                'cancel': {
                    label: 'CANCELAR'
                },
                'confirm': {
                    label: 'APROBAR'
                }
            },
            callback: function (confirmed) {
                if (confirmed) {
                    var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                    $.ajax({
                        url: '<?= Yii::$app->getUrlManager()->createUrl('certificacion-presupuestal/aprobar-certificacion-presupuestal') ?>',
                        type: 'GET',
                        async: false,
                        data: {id:id,_csrf: toke},
                        success: function(data){
                           
                        }
                    });
                } 
            }
        });
         
    });
    $('body').on('click', '.btn-edit-certificados', function (e) {
        e.preventDefault();
        var id=$(this).attr('data-id');
        $('#modal-ctrl-certificacion-presupuestal .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/certificacion-presupuestal/actualizar-certificacion?ID='+id);
        $('#modal-ctrl-certificacion-presupuestal').modal('show');
    });
    
    $('body').on('click', '.btn-edit', function (e) {
        e.preventDefault();
        var id=$(this).attr('data-id');
        $('#modal-ctrl-certificacion-presupuestal .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/certificacion-presupuestal/actualizar?ID='+id);
        $('#modal-ctrl-certificacion-presupuestal').modal('show');
    });
</script>