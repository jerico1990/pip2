<?php if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start(); ?>
<link href="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
<script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/bootstrap-table/bootstrap-table.min.js"></script>
<script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/bootstrap-table/bootstrap-table-es-SP.min.js"></script>
<link href="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/form-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">

<link rel="stylesheet" media="all" type="text/css" href="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/timepicker/jquery-ui-timepicker-addon.css" />

<div id="page-content" style="min-height: 885px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Ejecuci&oacute;n indicadores</h1>
        </div>
        <div class="container">
            <div class="panel panel-primary">
                <div id="Divnotificaciones"></div>
				<div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Codigo Proyecto</label>
                            <div class="col-md-2">
                                <input type="text" id="txtCodigoProyecto" class="form-control input-sm"/>
                            </div>
                            <div class="col-md-2">
                                <button id="btnbuscar" class="btn btn-primary"><i class="fa fa-search"></i>&nbsp;Buscar</button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row-border">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div id="tblListaToolbar">
                                <button id="btnCrearEjecucion" class="btn btn-primary" data-codigo-proyecto="<?= $CodigoProyecto ?>" disabled="disabled"><i class="fa fa-plus"></i>&nbsp;Generar ejecuci&oacute;n</button>&nbsp;&nbsp;
                                <button id="btnEditarEjecucion" class="btn btn-primary" data-codigo-proyecto="<?= $CodigoProyecto ?>" disabled="disabled"><i class="fa fa-pencil"></i>&nbsp;Editar ejecuci&oacute;n</button>
        					</div>
                            <table id="tblLista" data-height="450">
        						<thead>
                                    <tr>
        							    <th data-field="Item" data-width="35" data-align="center">N°</th>
                                        <th data-field="IndicadorNombre" data-width="300" data-align="left" data-halign="center">Productos del Proyecto</th>
                                        <th data-formatter="tblLista_Cantidad_formatter" data-width="80" data-align="center">Cantidad<br/>(Eje / Pro)</th>
                                        <th data-field="Descripcion" data-align="left" data-halign="center">Descripci&oacute;n</th>  
                                        <th data-field="Probabilidad" data-formatter="tblLista_Probabilidad_formatter" data-width="120" data-align="center">Probabilidad</th>
                                        <th data-field="Observacion" data-align="left" data-halign="center">Observaci&oacute;n</th>
                                        <th data-formatter="tblLista_eliminar" data-events="tblLista_events" data-width="35" data-align="center">&nbsp;</th>
                                    </tr>    
        						</thead>
        					</table>
                        </div>
                    </div>
				</div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-ctrl-crear-experimento" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Indicadores</h4>
            </div>
            <div class="modal-body">
                <table id="tblEditar" data-height="400">
                    <thead>
                        <tr>
                            <th data-field="Item" data-width="35" data-align="center">N°</th>
                            <th data-field="IndicadorNombre" data-align="left" data-halign="center">Productos del Proyecto</th>
                            <th data-field="Ejecutado" data-formatter="tbl_fm_Ejecutado" data-width="80" data-align="center">Ejecutado</th>
                            <th data-field="Programado" data-formatter="tbl_fm_Programado" data-width="80" data-align="center">Programado</th>
                            <th data-field="Descripcion" data-formatter="tbl_fm_descripcion" data-width="200" data-halign="center">Descripci&oacute;n</th>
                            <th data-field="Probabilidad" data-formatter="tbl_fm_probabilidad" data-width="120" data-align="center">Probabilidad</th>
                            <th data-field="Observacion" data-formatter="tbl_fm_observacion" data-width="200" data-align="center">Observci&oacute;n</th>
                        </tr>    
                    </thead>
                </table>
            </div>
            <div class="modal-footer" style="margin-top: 0px;">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button id="btnguardarindicadores" type="button" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
            </div>
        </div>
    </div>
</div>
<script>
    function tbl_fm_Programado(value, row, index){
        return '<input type="text" id="programado'+index+'" class="form-control input-sm" value="'+value+'" class="form-control input-sm"/>';
    }
    function tbl_fm_Ejecutado(value, row, index){
        return '<input type="text" id="ejecutado'+index+'" class="form-control input-sm" value="'+value+'" class="form-control input-sm"/>';
    }
    function tblLista_Probabilidad_formatter(value, row, index){
        var rt = '';
        switch(value){
            case '1': rt='Nula'; break;
            case '2': rt='Baja'; break;
            case '3': rt='Media'; break;
            case '4': rt='Alta'; break;
        }
        return rt;
    }
    function tbl_fm_fechaprobable(value, row, index){
        return '<input type="text" id="fechaprobable'+index+'" class="form-control input-sm" value="'+value+'" class="form-control input-sm"/>';
    }
    function tbl_fm_descripcion(value, row, index){
        return '<textarea id="descripcion'+index+'" rows="2" cols="1" class="form-control input-sm"  maxlength="100">'+value+'</textarea>';
    }
    function tbl_fm_probabilidad(value, row, index){
        return '<select class="form-control input-sm" id="probabilidad'+index+'">'
            +'<option value="">[SELECCIONE]</option>'
            +'<option value=1 '+(value=='1' ? 'selected="selected"':'')+'>Nula</option>'
            +'<option value=2 '+(value=='2' ? 'selected="selected"':'')+'>Baja</option>'
            +'<option value=3 '+(value=='3' ? 'selected="selected"':'')+'>Media</option>'
            +'<option value=4 '+(value=='4' ? 'selected="selected"':'')+'>Alta</option>'
            +'</select>';
    }
    function tbl_fm_observacion(value, row, index){
        return '<textarea id="observacion'+index+'" rows="2" cols="1" class="form-control input-sm"  maxlength="100">'+value+'</textarea>';
    }
	window.tblLista_events = {
        'click .tblLista-eliminar': function (e, value, row, index) {
            $.ajax({
                url:'<?= \Yii::$app->request->BaseUrl ?>/ejecucion-indicadores-new/elimina-indicador-proyecto',
                type:'post',
                data:{ 
                    _csrf: '<?=Yii::$app->request->getCsrfToken()?>',
                    'ProyectoId': row['ProyectoID'],
                    'IndicadorId': row['IdIndicador']
                },
                beforeSend:function() {
                    $('#tblLista').bootstrapTable('showLoading');
                },
                success:function(result) {
                    result = JSON.parse(result);
                    if(result['Estado'] == 1){
                        $('#tblLista').bootstrapTable('remove', { field:'IdIndicador', values: [row['IdIndicador']] });
                        alerta_sistema('#Divnotificaciones','success', result['Msj']);
                    } else {
                        alerta_sistema('#Divnotificaciones','danger', result['Msj']);
                    }
                },
                error:function(){   
                    alert('Error al realizar el proceso de busqueda.');
                },
                complete: function () {
                    $('#tblLista').bootstrapTable('hideLoading');
                }
            });
        }
    };
	
	/*function tblLista_editar(value, row, index) {
		return '<a class="tblLista-editar" href="#"><span class="fa fa-pencil"></span></a>';
	}*/
	function tblLista_eliminar(value, row, index) {
		return '<a class="tblLista-eliminar" href="#"><span class="fa fa-trash"></span></a>';
	}
	function tblLista_Cantidad_formatter(value, row, index) {
        return row['Ejecutado']+' / '+row['Programado'];
    }
    function cargar_grilla(){
        if($('#txtCodigoProyecto').val() != '') {
            $.ajax({
                url:'<?= \Yii::$app->request->BaseUrl ?>/ejecucion-indicadores-new/lista-indicadores-por-proyecto',
                type:'post',
                data:{ 
                    _csrf: '<?=Yii::$app->request->getCsrfToken()?>',
                    'CodigoProyecto': $('#txtCodigoProyecto').val()
                },
                
                beforeSend:function() {
                    $('#tblLista').bootstrapTable('showLoading');
                },
                success:function(result) {
                    result = JSON.parse(result);
                    $('#tblLista').bootstrapTable('load', result);
                    if(result.length > 0) {
                        $('#btnCrearEjecucion').attr('disabled', 'disabled');
                        $('#btnEditarEjecucion').removeAttr('disabled');
                    } else {
                        $('#btnEditarEjecucion').attr('disabled', 'disabled');
                        $('#btnCrearEjecucion').removeAttr('disabled');
                    }
                },
                error:function(){   
                    alert('Error al realizar el proceso de busqueda.');
                },
                complete: function () {
                    $('#tblLista').bootstrapTable('hideLoading');
                }
            });    
        }    
    }
	$(document).ready(function(){

        $('#modal-ctrl-crear-experimento').modal({ backdrop:'static', keyboard:false, show:false });

        $('#tblLista').bootstrapTable({
            cache: false,
            striped: true,
            pagination: false,
            search: false,
			classes: 'table table-condensed',
            toolbar: '#tblListaToolbar'

		});
		$('#tblEditar').bootstrapTable({
            cache: false,
            striped: true,
            pagination: false,
            search: false,
            classes: 'table table-condensed',
        });

		cargar_grilla();

        $('#btnCrearEjecucion').click(function(){
            $('#modal-ctrl-crear-experimento').modal('show');
        });
        $('#btnEditarEjecucion').click(function(){
            $.ajax({
                url:'<?= \Yii::$app->request->BaseUrl ?>/ejecucion-indicadores-new/lista-indicadores',
                type:'post',
                data:{ _csrf: '<?=Yii::$app->request->getCsrfToken()?>', 'CodigoProyecto': $('#txtCodigoProyecto').val() },
                beforeSend:function() {
                    $('#tblEditar').bootstrapTable('showLoading');
                },
                success:function(result) {
                    result = JSON.parse(result);
                    $('#tblEditar').bootstrapTable('load', result);
                },
                error:function(){   
                    alert('Error al realizar el proceso de busqueda.');
                },
                complete: function () {
                    $('#tblEditar').bootstrapTable('hideLoading');
                }
            });
            $('#modal-ctrl-crear-experimento').modal('show');
        });   
        $('#btnbuscar').click(function(){
            cargar_grilla()
        });
        $('#btnguardarindicadores').click(function(){
            var dataGrid = $('#tblEditar').bootstrapTable('getData');
            var data = { save: false, completar: false, data : {
                    Indicadores: [],
                    CodigoProyecto: $('#txtCodigoProyecto').val(),
                    _csrf: '<?=Yii::$app->request->getCsrfToken()?>',
                } 
            };
                
            $.each(dataGrid, function(i, row){
                var vprogramado = $('#programado'+i).val();
                var vejecutado = $('#ejecutado'+i).val();
                var vdescripcion = $('#descripcion'+i).val();
                var vprobabilidad = $('#probabilidad'+i).val();
                var vobservacion = $('#observacion'+i).val();
                if(vdescripcion != '' || vprobabilidad != '' || vobservacion != '') {
                    data.save = true;
                    if(vdescripcion == '' || vprobabilidad == '' || vobservacion == '') { 
                        data.completar = true;
                    } else {
                        data.data.Indicadores[i] = {
                            'IndicadorId': row['IdIndicador'],
                            'Programado': vprogramado,
                            'Ejecutado': vejecutado,
                            'Descripcion': vdescripcion,
                            'Probabilidad': vprobabilidad,
                            'Observacion': vobservacion,
                        };
                    }
                }                
            });
            
            if (data.save) {
                if (data.completar) {
                    alert('debe completar la informacion');
                } else { 
                    $.ajax({
                        url: '<?= \Yii::$app->request->BaseUrl ?>/ejecucion-indicadores-new/registrar',
                        type: 'post',
                        data: data.data,
                        beforeSend: function() {
                            
                        },
                        success: function(result) {
                            result = JSON.parse(result);
                            if(result['Estado'] == 1){
                                alerta_sistema('#Divnotificaciones','success', result['Msj']);
                                cargar_grilla();
                            } else {
                                alerta_sistema('#Divnotificaciones','danger', result['Msj']);
                            }
                        },
                        error: function(){   
                            alerta_sistema('#Divnotificaciones','danger', 'Error al realizar el proceso de busqueda.');
                        },
                        complete: function () {
                            $('#modal-ctrl-crear-experimento').modal('hide');
                        }
                    });
                }
            }
        });
	});
</script>
