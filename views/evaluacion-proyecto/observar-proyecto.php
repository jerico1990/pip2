<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'observar-proyecto?ProyectoCodigo='.$ProyectoCodigo.'&NivelAprobacionCorrelativo='.$NivelAprobacionCorrelativo,
        'options'           => [
            'autocomplete'  => 'off',
            'id'            => 'frmSeguimientoObservado'
        ]
    ]
); ?>
<div class="modal-body">
    <div class="form-horizontal">
        <div class="form-group">
            <label class="col-sm-3 control-label">Observación <span class="f_req">*</span></label>
            <div class="col-sm-9">
                <textarea class="form-control" name="Seguimiento[Observacion]" required></textarea>
                <!--<h6 class="pull-left count-message"></h6>-->
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button id="btn-observacion-proyecto-p" type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Enviar</button>
    </div>
</div>


<?php ActiveForm::end(); ?>
<script>
    var formParsleySeguimiento = $('#frmSeguimientoObservado').parsley(defaultParsleyForm());
    
</script>