<div id="page-content" style="min-height: 885px;">
    <div id="wrap">
        <div id="page-heading">
            <h1></h1>
        </div>
        <div class="container">
                    
            <style>
                .panel .panel-body {
                    padding: 10px !important;
                }

                .panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 1px solid #a8a8ab !important;
                }

                .panel-gray .panel-body {
                    border-top: none !important;
                }

                .btn-sm {
                    padding: 5px 10px !important;
                    line-height: 1.5 !important;
                }

                .input-comp-nombre {
                    text-transform: uppercase;
                }

                .tbl-act > tbody > .tr-header > td {
                    text-align: center;
                    font-size: 12px;
                    font-weight: bold;
                    padding: 5px !important;
                    vertical-align: middle !important;
                    /*border: 1px solid #cfcfd0;
                    background-color: #f0f0f1;*/
                    border: 1px solid #c0c0c0;
                    background-color: #e3e3e3;
                }

                /*.tbl-act .tr-item td {
                    padding: 2px !important;
                }*/
                input.form-control, select.form-control {
                    /*height: 28px !important;*/
                }

                .tbl-act .tr-act .input-nombre {
                    min-width: 320px !important;
                }

                .tbl-act .tr-act .input-unidadmedida {
                    min-width: 100px !important;
                }

                .input-number {
                    min-width: 92px !important;
                }

                .btn-add-re {
                    float: right;
                }

                /*.tbl-act > tbody > tr > td:nth-child(2) {
                    min-width: 40px !important;
                    text-align: center !important;
                }*/
                .text-left {
                    text-align: left !important;
                    font-weight: bold;
                }

                .tr-comp input, .tr-proy input {
                    font-weight: bold;
                }

                .th-proy {
                    min-width: 630px !important;
                }
            </style>

            <h2>Plan de adquisiciones del proyecto <?php echo $codigos ?></h2>
            <div class="tab-container">
                <ul class="nav nav-tabs">
                        <li id="li-poaf" class="active"><a href="#tab-informacion-general" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Información General</a></li>
                        <?php foreach ($pac as $pacx): ?>
                            <li><a href="#<?php echo $pacx->Anio ?>" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;<?php echo $pacx->Anio ?></a></li>
                        <?php endforeach ?>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-informacion-general">
                        <div id="container-poaf">
                            
                            <p><?php echo $informacion->TituloProyecto ?></p>
                        </div>
                    </div>
                    <?php foreach ($pac as $pacxs): ?>
                        <?php 
                            $anio   = $pacxs->Anio;
                            $codigo = $pacxs->ProyectoID;
                            $situacionPac = $pacxs->Situacion;
                        ?>
                        <div class="tab-pane" id="<?php echo $pacxs->Anio ?>">
                            <div id="container-poaf-<?php echo $pacxs->Anio ?>">
                                <?php //print_r(\Yii::$app->user->identity->Rol) ?>
                                <?php //echo $situacionPac; ?>
                                <?php 
                                // if( (\Yii::$app->user->identity->Rol==2 && $situacionPac==2)  
                                // || ($situacionPac==1) 
                                // || (\Yii::$app->user->identity->Rol==8 && $situacionPac==1) 
                                // || (\Yii::$app->user->identity->Rol==5) ){
                                ?>

                                <h3>Estado del Plan de Adquisiciones 
                                <strong><?php echo strtoupper($pacxs->Descripcion); ?></strong></h3>
                                
                                <?php if( (\Yii::$app->user->identity->Rol==2 and $situacionPac==2) || (\Yii::$app->user->identity->Rol==8 and $situacionPac==1) ){?>
                                
                                <?php //if( (\Yii::$app->user->identity->Rol==8 and $situacionPac==1) ){?>
                                    <p>Verificar el plan de adquisicones <?php echo $anio; ?></p>
                                    <button class="btn btn-primary btn-aprobar-poa<?php echo $anio; ?>" ><i class="fa fa-check"></i>&nbsp;Aprobar PAC <?php echo $anio; ?></button>
                                    <!-- <button class="btn btn-primary btn-observar-poa" >&nbsp;Observar PAC</button> -->
                                <?php } ?>
                                <hr>
                                <div id="contenido_<?php echo $anio ?>">
                                </div>
                                <br>
                                <div class="col-sm-12">
                                    <div class="text-center">
                                        <a class="btn btn-inverse-alt" href="<?= \Yii::$app->request->BaseUrl ?>/plan-adquisiciones/descargar-pa?codigo=<?php echo $codigo ?>&anio=<?php echo $anio ?>">
                                            <i class="fa fa-download"></i>&nbsp;Descargar
                                        </a>
                                    </div>
                                </div>
                                <hr>

                            </div>
                        </div>
                        <script type="text/javascript">
                            $(document).on('ready',function(){
                                $('#contenido_<?php echo $anio ?>').load('<?= \Yii::$app->request->BaseUrl ?>/evaluacion-proyecto/seguimiento-pa?anio=<?php echo $anio ?>&codigo=<?php echo $codigos ?>');
                            });

                            $('body').on('change', '.central_<?php echo $anio;?>', function (e) {
                                e.preventDefault();
                                $(this).attr('changed', 'changed');
                            });

                            $('.btn-aprobar-poa<?php echo $anio; ?>').click(function (e) {
                            // $('body').on('click','btn-aprobar-poa<?php echo $anio; ?>',function(){
                                var arrVacios = [];
                                var arrCentral = [];
                                var $btn = $(this);

                                $('.central_<?php echo $anio;?>').each(function (e,y) {
                                    var $this = $(this);
                                    console.log(y);
                                    if($this.attr('changed') != 'changed'){
                                        arrVacios.push({
                                            VACIO: 1
                                        });
                                    }
                                });

                                $('.central_<?php echo $anio;?>[changed="changed"]').each(function () {
                                    var $this = $(this);
                                    var aa = $this.parent().parent().attr('data-recurso-id');
                                    var central = $this.val();
                                    arrCentral.push({
                                        ID: aa,
                                        CEN: central
                                    });
                                });

                                // console.log(JSON.stringify(arrCentral));

                                if(arrVacios.length != 0){
                                    bootbox.alert('Debe de seleccionar todo los campos');
                                }else{
                                    bootbox.confirm({
                                        title: '<span style="text-align:center">PNIA - CONFIRMACIÓN</span>',
                                        message: 'Está seguro de aprobar el PAC del proyecto?.',
                                        buttons: {
                                            'cancel': {
                                                label: 'CANCELAR'
                                            },
                                            'confirm': {
                                                label: 'APROBAR PAC'
                                            }
                                        },
                                        callback: function (confirmed) {
                                            if (confirmed) {
                                                var centra = JSON.stringify(arrCentral);

                                                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                                                <?php if( (\Yii::$app->user->identity->Rol==2) ){ ?>
                                                    $.ajax({
                                                        url: '<?= Yii::$app->getUrlManager()->createUrl('evaluacion-proyecto/aprobar-proyecto-pa') ?>',
                                                        type: 'GET',
                                                        async: false,
                                                        data: {aprobar:1,anio:'<?= $anio ?>',codigo:'<?= $codigo ?>',_csrf: toke},
                                                        success: function(data){
                                                            // $('.btn-aprobar-poa').attr('disabled');
                                                        }
                                                    });
                                                <?php } ?>

                                                <?php if( (\Yii::$app->user->identity->Rol==8) ){ ?>
                                                    $.ajax({
                                                        url: '<?= Yii::$app->getUrlManager()->createUrl('evaluacion-proyecto/aprobar-proyecto-pa-central') ?>',
                                                        type: 'POST',
                                                        async: false,
                                                        data: {"cental":centra,_csrf: toke,anio:'<?= $anio ?>',codigo:'<?= $codigo ?>'},
                                                        success: function(data){
                                                            $('.btn-aprobar-poa').attr('disabled');
                                                            // location.reload();
                                                        }
                                                    });
                                                <?php } ?>


                                                location.reload();


                                            } 
                                        }
                                    });
                                }
                                
                            });
                        </script>
                    <?php endforeach ?>

                </div>
            </div>


        </div>
    </div>
</div>

<div id="modal-loading" class="modal fade modal-loading in" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-hidden="false" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content-no-shadow modal-content">
            <div class="text-center"><i class="fa fa-spinner fa-pulse fa-fw" style="color:#16a085;font-size: 100px;"></i></div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-ctrl-observacion" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Recursos</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function(){
       $('.fa-search').popover(); 
    });
    $('[data-rel=tooltip]').tooltip();
      
</script>