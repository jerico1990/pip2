<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'observar-pac?RecursoID='.$RecursoID,
        'options'           => [
            'autocomplete'  => 'off',
            'id'            => 'frmObservarPac'
        ]
    ]
); ?>
<div class="modal-body">
    <div class="form-horizontal">
        <div class="form-group">
            <label class="col-sm-3 control-label">Observación <span class="f_req">*</span></label>
            <div class="col-sm-9">
                <textarea class="form-control" name="PacObservacion[Observacion]" required><?= $model->Observacion ?></textarea>
                <!--<h6 class="pull-left count-message"></h6>-->
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button id="btn-observacion-pac-p" type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Enviar</button>
    </div>
</div>


<?php ActiveForm::end(); ?>

<script>
    var formParsleyObservarPac = $('#frmObservarPac').parsley(defaultParsleyForm());
    
    $('#btn-observacion-pac-p').click(function () {
        
        var $btn = $(this);
        $btn.attr('disabled', 'disabled');
        var isValid = formParsleyObservarPac.validate();
        if (isValid) {
            sendForm($('#frmObservarPac'), $(this), function (eve) {
                $("#modal-ctrl-observacion").modal('hide');
            });
        }
    });
</script>