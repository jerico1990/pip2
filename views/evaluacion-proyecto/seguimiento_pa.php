<div class="table-responsive" style="max-height: 800px;">
    <table class="table table-bordered table-condensed tbl-act">
        <tbody>
            <tr class="tr-header">
                <td rowspan="2" class="text-center">Nro de Referencia</td>
                <td rowspan="2" class="text-center">Descripción Detallada</td>
                <td rowspan="2" class="text-center">Unidad de Medida</td>
                <td rowspan="2" class="text-center">Cantidad</td>
                <td rowspan="2" class="text-center">Codificación Segun Matriz</td>
                <td rowspan="2" class="text-center">Costo Estimado de la <br>Adquisición o Contratación (S/.)</td>
                <td rowspan="2" class="text-center">Método de Adquisición o Contratación</td>
                <td colspan="2" class="text-center">Fuente Financiamiento</td>
                <td rowspan="2" class="text-center">Fecha Estimada</td>
                <td rowspan="2" class="text-center">Objetivo - Actividad</td>
                <td rowspan="2" class="text-center">Observacion</td>
                <?php if(\Yii::$app->user->identity->rol==8){ ?>
                    <td rowspan="2" class="text-center"></td>
                <?php } ?>
            </tr>
            <tr class="tr-header">
                <td class="text-center">PNIA</td>
                <td class="text-center">Otro</td>
            </tr>

            <!-- <tr class="tr-header">
                <td class="text-center">1</td>
                <td class="text-left" colspan="10">Bienes</td>
            </tr> -->
            <?php //print_r($detallepa); ?>
            <?php if(!empty($detallepa)): ?>
                <?php 
                $ca = '';
                $i=0;$x=0; foreach ($detallepa as $value):  ?>
                        <?php if($value['Categoria'] != $ca): ?>
                                                        <tr class="tr-header">
                                                            <td class="text-center">
                                                                <?php echo ($i+1); ?>
                                                            </td>
                                                            <td class="text-left" colspan="11"><?php echo strtoupper($value['Categoria']) ?></td>
                                                        </tr>
                                                    <?php $ca = $value['Categoria']; $i++;$x=0; ?>
                                                    <?php endif ?>

                                                    <tr data-recurso-id="<?php echo $value['ID']; ?>" id="<?php echo $value['ID']; ?>" data-categoria-id="<?php echo $value['Codigo']; ?>">
                                                        <td class="text-center">
                                                            <?php if($Situacion == 0): ?>
                                                                <a href="#" class="btn-remove-recurso" data-toggle="tooltip" title="" data-id="<?php echo $value['ID']; ?>" data-original-title="Eliminar"><i class="fa fa-remove fa-lg"></i></a>
                                                            <?php endif; ?>
                                                               
                                                            <?php if($value['Situacion'] == 3){ ?>
                                                                <a href="javascript:void(0);" class="btn-edit-are" data-toggle="tooltip" title="" data-original-title="Detalle"><i class="fa fa-search"></i></a>
                                                            <?php } ?>

                                                            <?php echo ($i).".".($x+1);  ?>
                                                            <input type="hidden" class="correlativo" name="correlativo" value="<?php echo ($i).".".($x+1);  ?>">

                                                        </td>
                                                        
                                                        <td class="nombrePac">
                                                            <?php if($Situacion == 0): ?>
                                                                <textarea class="form-control" style="width: 150px"><?php echo $value['Nombre'] ?></textarea>
                                                            <?php else: ?>
                                                                <?php echo $value['Nombre'] ?>
                                                            <?php endif; ?>
                                                        </td>

                                                        <td class="text-center unidadPac"><?php echo $value['UnidadMedida'] ?></td>
                                                        
                                                        <td class="text-center">
                                                            <?php if($Situacion == 0): ?>
                                                                <input type="text" name="cantidad" value="<?php echo $value['Cantidad'] ?>" class="form-control input-cantidad input-number">
                                                            <?php else: ?>
                                                                <?php echo $value['Cantidad'] ?>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td class="text-center">
                                                            <?php echo $value['Codificacion'] ?>
                                                            <input type="hidden" class="codificacion" name="codificacion" value="<?php echo $value['Codificacion'] ?>">
                                                        </td>
                                                        <!-- <td class="text-center">-</td> -->
                                                        <td class="text-center costoPac input-number"><?php echo $value['Cantidad']*$value['CostoUnitario'] ?></td>
                                                        <td class="text-center">
                                                            <?php if($Situacion == 0): ?>
                                                            <select class="form-control select-metodo" style="width: 80px;" name="metodo" <?php echo trim($value['MetodoAdquisicion']) != ''?'changed="changed"':'' ?> >
                                                                <option value="" <?php echo $value['MetodoAdquisicion'] == ''?'selected':'' ?> >[SELECCIONE]</option>
                                                                <option value="CP" <?php echo trim($value['MetodoAdquisicion']) == 'CP'?'selected':'' ?> >CP</option>
                                                                <option value="CV" <?php echo trim($value['MetodoAdquisicion']) == 'CV'?'selected':'' ?>>CV</option>
                                                                <option value="CV" <?php echo trim($value['MetodoAdquisicion']) == 'CD'?'selected':'' ?>>CD</option>
                                                            </select>
                                                            <?php else: ?>
                                                                <?php echo $value['MetodoAdquisicion'] ?>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td class="text-center input-number"><?php echo ($value['RazonSocial'] == 'PNIA' || $value['RazonSocial'] = 'PROGRAMA NACIONAL DE INNOVACION AGRARIA- PNIA') ? $value['Cantidad']*$value['CostoUnitario'] : '-' ?></td>
                                                        <td class="text-center"><?php echo '-' ?></td>
                                                        <td class="text-center">
                                                            <?php if($Situacion == 0): ?>
                                                            <select class="form-control select-fecha" name="metodo" <?php echo trim($value['FechaInicioPA']) != ''?'changed="changed"':'' ?> style="width: 100px;">
                                                                <option>[SELECCIONE]</option>
                                                                <option value="ENE" <?php echo trim($value['FechaInicioPA']) == 'ENE'?'selected':'' ?> >Enero</option>
                                                                <option value="FEB" <?php echo trim($value['FechaInicioPA']) == 'FEB'?'selected':'' ?> >Febrero</option>
                                                                <option value="MAR" <?php echo trim($value['FechaInicioPA']) == 'MAR'?'selected':'' ?> >Marzo</option>
                                                                <option value="ABR" <?php echo trim($value['FechaInicioPA']) == 'ABR'?'selected':'' ?> >Abril</option>
                                                                <option value="MAY" <?php echo trim($value['FechaInicioPA']) == 'MAY'?'selected':'' ?> >Mayo</option>
                                                                <option value="JUN" <?php echo trim($value['FechaInicioPA']) == 'JUN'?'selected':'' ?> >Junio</option>
                                                                <option value="JUL" <?php echo trim($value['FechaInicioPA']) == 'JUL'?'selected':'' ?> >Julio</option>
                                                                <option value="AGO" <?php echo trim($value['FechaInicioPA']) == 'AGO'?'selected':'' ?> >Agosto</option>
                                                                <option value="SEP" <?php echo trim($value['FechaInicioPA']) == 'SEP'?'selected':'' ?> >Septiembre</option>
                                                                <option value="OCT" <?php echo trim($value['FechaInicioPA']) == 'OCT'?'selected':'' ?> >Octubre</option>
                                                                <option value="NOV" <?php echo trim($value['FechaInicioPA']) == 'NOV'?'selected':'' ?> >Noviembre</option>
                                                                <option value="DIC" <?php echo trim($value['FechaInicioPA']) == 'DIC'?'selected':'' ?> >Diciembre</option>
                                                            </select>
                                                            <?php else: ?>
                                                                <?php echo $value['FechaInicioPA'] ?>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td class="text-center">
                                                            <?php if(!empty($value['NumeracionPAC'])): ?>
                                                                <input type="hidden" class="jsonubica" name="jsonubicacion" id="ubicacion_<?php echo $value['ID']; ?>" value='<?php echo $value['NumeracionPAC'] ?>''>
                                                                <?php /*foreach (json_decode($value['NumeracionPAC']) as $key => $valuex): ?>
                                                                    <?php 
                                                                    echo "Objetivo:".$valuex->correlativoComp.'- Actividad: '.$valuex->correlativoComp.'.'.$valuex->correlativoActv.', ';*/ ?>
                                                                <?php echo $value['NumeracionPAC'] ?>
                                                            <?php endif ?>
                                                        </td>
                                                        <td>
                                                            <?php if($Situacion == 0): ?>
                                                                <textarea class="observa" id="obs"><?php echo $value['Observaciones'] ?></textarea>
                                                            <?php else: ?>
                                                                <?php echo $value['Observaciones'] ?>
                                                            <?php endif; ?>
                                                        </td>
                                                    </tr>

                <?php $x++;endforeach ?>
            <?php endif ?>
        </tbody>
    </table>
</div>


<script type="text/javascript">
    $('body').on('click', '.generar-observacion-recurso', function (e) {
        e.preventDefault();
        var recursoID=$(this).parent().parent().attr('data-recurso-id');
        console.log(recursoID);
        
        $('#modal-ctrl-observacion .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/evaluacion-proyecto/observar-pac?RecursoID='+recursoID);
        $('#modal-ctrl-observacion').modal('show');
    });


    


</script>


<div class="modal fade" id="modal-ctrl-observacion" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Recursos</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>