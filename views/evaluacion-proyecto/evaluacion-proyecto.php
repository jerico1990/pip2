<?php
    $descripcionSituacion="";
   switch ($situacionProyecto) {
    case 0:
        $descripcionSituacion="Iniciado";
        break;
    case 1:
        $descripcionSituacion="Aprobado";
        break;
    case 2:
        $descripcionSituacion="Pendiente";
        break;
    case 3:
        $descripcionSituacion="Observado";
        break;
    }
    
?>
<div id="page-content" style="min-height: 885px;">
    <div id="wrap">
        <div id="page-heading">
            <h1></h1>
        </div>
        <div class="container">
                    
            <style>
                #page-heading {
                    display: none;
                }

                .tab-content {
                    background-color: #fff;
                }

                .panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 1px solid #a8a8ab !important;
                }

                .panel-gray .panel-body {
                    border-top: none !important;
                }

                .panel-heading {
                    height: auto !important;
                }
            </style>
            <h3>Código Proyecto: <span><b><?= $informacion->Codigo ?></b></span>   - Estado del Proyecto: <span><b><?= $descripcionSituacion ?></b></span></h3>
            <div class="tab-container">
                <ul class="nav nav-tabs">
                    <li id="li-poaf" ><a href="#tab-seguimiento" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Seguimiento</a></li>
                    <li id="li-poaf" class="active"><a class="tab-informacion-general" href="#tab-informacion-general" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Información General</a></li>
                    <li id="li-poaf"><a class="tab-marco-logico" href="#tab-marco-logico" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Marco Lógico</a></li>
                    <li id="li-poaf"><a class="tab-recursos" href="#tab-recursos" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Recursos</a></li>
                    <li id="li-poaf"><a class="tab-tarea" href="#tab-tarea" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;P. Tareas</a></li>
                    <li id="li-poaf"><a class="tab-seguimiento-tarea" href="#tab-seguimiento-tarea" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Seguimiento Tareas</a></li>
                    <li id="li-poaf"><a class="tab-pasos-criticos" href="#tab-pasos-criticos" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Pasos Críticos</a></li>
                    <li id="li-poaf"><a class="tab-carga-itf" href="#tab-carga-itf" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Carga ITF</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane " id="tab-seguimiento">
                        <div id="container-poaf">
                            <?php if(\Yii::$app->user->identity->Rol==2){ /*?>
                            <button class="btn btn-primary btn-habilitar-recursos" ><i class="fa fa-check"></i>&nbsp;Habilitar Recursos</button><br><br>
                            <button class="btn btn-primary btn-deshabilitar-recursos" ><i class="fa fa-check"></i>&nbsp;Deshabilitar Recursos</button><br><br>
                            <button class="btn btn-primary btn-habilitar-cronograma-recursos" ><i class="fa fa-check"></i>&nbsp;Habilitar cronograma de Recursos</button><br><br>
                            <button class="btn btn-primary btn-deshabilitar-cronograma-recursos" ><i class="fa fa-check"></i>&nbsp;Deshabilitar cronograma de Recursos</button><br><br>
                            
                            
                            <button class="btn btn-primary btn-habilitar-tareas" ><i class="fa fa-check"></i>&nbsp;Habilitar Tareas</button><br><br>
                            <button class="btn btn-primary btn-deshabilitar-tareas" ><i class="fa fa-check"></i>&nbsp;Deshabilitar Tareas</button><br><br>
                            <button class="btn btn-primary btn-habilitar-cronograma-tareas" ><i class="fa fa-check"></i>&nbsp;Habilitar cronograma de Tareas</button><br><br>
                            <button class="btn btn-primary btn-deshabilitar-cronograma-tareas" ><i class="fa fa-check"></i>&nbsp;Deshabilitar cronograma de Tareas</button><br><br>
                            <?php*/ } ?>
                            
                            <?php $observacionOR=0; ?>
                            <?php $observacionAR=0; ?>
                            <?php foreach($ObservacionesRecursos as $ObservacionRecurso){
                                    if($ObservacionRecurso->Estado==1)
                                    {
                                        $observacionOR++;
                                    }
                                    elseif($ObservacionRecurso->Estado==3)
                                    {
                                        $observacionAR++;
                                    }
                            }?>
                            
                            <?php if($observacionOR>0){ ?>
                                <span style="color: red">Existen observaciones pendientes en Recursos, Total: <?= $observacionOR ?> .</span>
                            <?php } ?>
                            <?php if($observacionAR>0){ ?>
                                <span style="color: green">El proyecto contiene <?= $observacionAR ?> aprobaciones de recursos cerradas.</span>
                            <?php } ?>
                            <br>
                            <?php $observacionOT=0; ?>
                            <?php $observacionAT=0; ?>
                            <?php foreach($ObservacionesTareas as $ObservacionTarea){
                                    if($ObservacionTarea->Estado==1)
                                    {
                                        $observacionOT++;
                                    }
                                    elseif($ObservacionTarea->Estado==3)
                                    {
                                        $observacionAT++;
                                    }
                            }?>
                            
                            <?php if($observacionOT>0){ ?>
                                <span style="color: red">Existen observaciones pendientes en Tareas, Total: <?= $observacionOT ?> .</span>
                            <?php } ?>
                            <?php if($observacionAT>0){ ?>
                                <span style="color: green">El proyecto contiene <?= $observacionAT ?> aprobaciones de tareas cerradas.</span>
                            <?php } ?>
                            <br><br>
                            <?php if(\Yii::$app->user->identity->Rol==2){ ?>
                            <button class="btn btn-primary btn-habilitar" ><i class="fa fa-check"></i>&nbsp;Habilitar</button>
                            <?php } ?>
                            <?php if(\Yii::$app->user->identity->Rol==5 && $situacionProyecto==2 && $seguimientosCount==0){?>
                                <button class="btn btn-primary btn-aprobar-poa" ><i class="fa fa-check"></i>&nbsp;Aprobar POA</button>
                                <button class="btn btn-primary btn-observar-poa" >&nbsp;Observar POA</button>
                            <?php }elseif(\Yii::$app->user->identity->Rol==2 && $situacionProyecto==2 && $seguimientosCount==1){ ?>
                                <button class="btn btn-primary btn-aprobar-poa" ><i class="fa fa-check"></i>&nbsp;Aprobar POA</button>
                                <button class="btn btn-primary btn-observar-poa" >&nbsp;Observar POA</button>
                            <?php } ?>
                            <br><br>
                            <div class="panel-body">
                                <div class="col-sm-12">
                                    <div class="progress pos-rel" data-percent="<?= $proyecto->Avance ?>%" style="height: 20px;">
                                        <div class="progress-bar" style="width:<?= $proyecto->Avance ?>%;"><?= $proyecto->Avance ?>%</div>
                                    </div>
                                    <div class="pull-left">Avance fisico general del proyecto</div><div class="pull-right"><?= $proyecto->Avance ?>%</div>
                                </div>
                            </div>
                            <br><br>
                            <div class="panel-body">
                                <div class="col-sm-12">
                                    <div class="progress pos-rel" data-percent="<?= $EjecucionFechaActualTareas ?>%" style="height: 20px;">
                                        <div class="progress-bar" style="width:<?= $EjecucionFechaActualTareas ?>%;"><?= $EjecucionFechaActualTareas ?>%</div>
                                    </div>
                                    <div class="pull-left">Avance fisico a la fecha (<?php echo date('d-m-Y') ?>)</div><div class="pull-right"><?= $EjecucionFechaActualTareas ?>%</div>
                                </div>
                            </div>
                            <br><br>
                        </div>
                    </div>
                    <div class="tab-pane active" id="tab-informacion-general">
                        <div id="container-poaf">
                            <div id="container-informacion-general"></div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-marco-logico">
                        <div id="container-poaf">
                            <div id="container-marco-logico"></div>
                        </div>
                    </div>
                    <div class="tab-pane " id="tab-recursos">
                        <a class="btn btn-primary presupuesto" id="hidens2">Control de presupuesto</a>
                        <div id="container-poaf-recursos">
                            <style>
                                #container-poaf-recursos .input-number {
                                    text-align: right;
                                    min-width: 72px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }

                                #container-poaf-recursos .input-number-sm {
                                    text-align: right;
                                    min-width: 52px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }

                                #container-poaf-recursos .input-number-md {
                                    text-align: right;
                                    min-width: 82px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }

                                #container-poaf-recursos table-act > tbody > tr > td:nth-child(1) {
                                    text-align: center;
                                }

                                #container-poaf-recursos .table-act > tbody > tr > td:nth-child(2) {
                                   text-align: center;
                                }

                                #container-poaf-recursos .table-act > tbody > tr > td:nth-child(3) {
                                    min-width: 320px !important;
                                }

                                #container-poaf-recursos .table > tbody > .tr-header > td {
                                    text-align: center;
                                    font-size: 12px;
                                    font-weight: bold;
                                    padding: 1px !important;
                                    vertical-align: middle !important;
                                    /*border: 1px solid #cfcfd0;
                                    background-color: #f0f0f1;*/
                                    border: 1px solid #c0c0c0;
                                    background-color: #e3e3e3;
                                    min-width: 75px !important;
                                }

                                #container-poaf-recursos h4.panel-title {
                                    font-size: 16px;
                                }

                                #container-poaf-recursos .tr-comp td, .tr-proy td {
                                    font-weight: bold;
                                }

                                #container-poaf-recursos .table-condensed td {
                                    padding: 1px !important;
                                }

                                #container-poaf-recursos td, #table-container input {
                                    font-size: 13px !important;
                                }

                                #container-poaf-recursos .form-control[readonly] {
                                    background-color: #f2f3f4;
                                }
                                #container-informacion-general #page-content{
                                    margin-left: 0px;
                                    padding-bottom:0px;
                                }
                                #container-informacion-general #wrap > .container
                                {
                                    padding: 0px;
                                }
                                
                                #container-marco-logico #page-content{
                                    margin-left: 0px;
                                    padding-bottom:0px;
                                }
                                #container-marco-logico #wrap > .container
                                {
                                    padding: 0px;
                                }
                            </style>
                            <br>

                            <div id="table-container-recursos">
                            </div>
                            <br>
                            <div class="text-center">
                                <a target="_blank" class="btn btn-inverse-alt" href="<?= Yii::$app->getUrlManager()->createUrl('recursos-poa/descargar-poa?codigo='.$informacion->Codigo.'') ?>">
                                    <i class="fa fa-download"></i>&nbsp;Descargar Cronograma Físico
                                </a>
                                
                                <a target="_blank" class="btn btn-inverse-alt" href="<?= Yii::$app->getUrlManager()->createUrl('recursos-poa/descargar-poa-financiero?codigo='.$informacion->Codigo.'') ?>">
                                    <i class="fa fa-download"></i>&nbsp;Descargar Cronograma Financiero
                                </a>
                            </div> 
                            <script>
                                var _proyectoRecurso = new Object();
                                $(document).ready(function () {
                                    
                                    var investigadorID=<?= $investigadorID ?>;
                                    $('#container-informacion-general').html('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>');
                                    $('#container-informacion-general').load('<?= \Yii::$app->request->BaseUrl ?>/informacion-general/index?investigador='+investigadorID);
                                    $("#container-informacion-general input,#container-informacion-general textarea,#container-informacion-general select").prop("disabled", true);
                                    
                                    /*
                                    $('#container-marco-logico').html('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>');
                                    $('#container-marco-logico').load('<?= \Yii::$app->request->BaseUrl ?>/marco-logico/index?investigadorID='+investigadorID);
                                    $("#container-marco-logico input,textarea,select").prop("disabled", true);
                                    $("#container-marco-logico .btn-edit-ml-finproyecto").prop("disabled", true);
                                    
                                    
                                    $('#table-container-recursos').html('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>');
                                    getProyectoRecurso().done(function(json) {
                                        _proyectoRecurso = json;
                                        _drawTableRecurso();
                                    });*/
                                });
                                
                                $('.btn-descargar-poa').click(function (e) {
                                    var $btn = $(this);
                                    var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                                    $.ajax({
                                        url: '<?= Yii::$app->getUrlManager()->createUrl('recursos-poa/descargar-poa') ?>',
                                        type: 'GET',
                                        async: false,
                                        data: {codigo:'<?= $informacion->Codigo ?>',_csrf: toke},
                                        success: function(data){
                                           
                                        }
                                    });
                                });
                                
                                function getProyectoRecurso() {
                                    var url = "<?= \Yii::$app->request->BaseUrl ?>/recursos-poa/recursos-json?codigo=<?= $informacion->Codigo ?>";
                                    return $.getJSON( url );
                                }

                                function _drawTableRecurso() {
                                    var situacionProyecto=<?= $situacionProyecto ?>;
                                    var situacionRecurso=<?= $situacionRecurso ?>;
                                    var html = '';
                                    var cantmeses = _proyectoRecurso.Cronogramas;
                                    console.log(cantmeses);
                                    for (var i_comp = 0; i_comp < _proyectoRecurso.Componentes.length; i_comp++) {
                                        var comp = _proyectoRecurso.Componentes[i_comp];
                                        html += '<div class="panel panel-gray">';
                                        html += '<div class="panel-heading">';
                                        html += '<h4 class="title-obj panel-title">';
                                        html += (i_comp + 1) + '. Objetivo específico - ' + comp.Nombre ;
                                        html += '</h4></div>';
                                        
                                        html += '<div class="panel-heading">';
                                        html += '<h4 class="panel-title">';
                                        html += 'Total de Objetivo: <span class="input-number" style="font-size:15px !important">'+comp.TotalObjetivo+'</span>' ;
                                        html += '</h4></div>';
                                        
                                        html += '<div class="panel-body">';
                                        html += '<div class="table-responsive">';
                                        html += '<table class="table table-bordered table-condensed table-act">';
                                        html += '<tbody>';
                                        for (var i_act = 0; i_act < comp.Actividades.length; i_act++)
                                        {
                                            var act = comp.Actividades[i_act];
                                            var TotalActividad=0;
                                            for (var i_are = 0; i_are < act.ActRubroElegibles.length; i_are++) {
                                                var are = act.ActRubroElegibles[i_are];
                                                if (are.AreSubCategorias.length) {
                                                    for (var i_arescat = 0; i_arescat < are.AreSubCategorias.length; i_arescat++) {
                                                        var arescat = are.AreSubCategorias[i_arescat];
                                                        if (arescat) {
                                                            TotalActividad=TotalActividad+(arescat.CostoUnitario*arescat.MetaFisica);
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            html += '<tr class="tr-header">';
                                            html += '<td>#</td><td>Código Matriz</td><td>Actividad</td><td>Específica</td><td>Unidad de medida</td><td>Costo unitario</td><td>Meta física</td><td>Total</td>';
                                            var m=1;
                                            for (var i_crono_act = 0; i_crono_act < cantmeses.length; i_crono_act++) {
                                                var compAa = _proyectoRecurso.Cronogramas[i_crono_act];
                                                html += '<td> Mes '+ compAa.MesDescripcion +' </td>';
                                                m++;
                                            }
                                            html += '</tr>';
                                           
                                            html += '<tr class="tr-act-mf info" data-act-id="' + act.ID + '">';
                                            html += '<td>' + (i_comp + 1) + '.' + (i_act + 1) + '</td><td></td><td><b>' + act.Nombre + '</b></td><td></td><td>' + act.UnidadMedida+'</td><td><input type="text" readonly="" class="input-number form-control" value="'+(TotalActividad/act.MetaFisica)+'"></td>'
                                                    + '<td align="center">' + act.MetaFisica + '</td><td><input type="text" readonly="" class="input-number form-control" value="'+TotalActividad+'"></td>';
                                            html += '<td colspan="'+m+'"></td>';
                                            html += '</tr>';
                                            
                                            // Rubros Elegibles
                                            html += '<tr>';
                                            html += '<td colspan="7"></td>';
                                            html += '</tr>';
                                            for (var i_are = 0; i_are < act.ActRubroElegibles.length; i_are++) {
                                                var are = act.ActRubroElegibles[i_are];
                                                var TotalRubroElegible=0;
                                                if (are.AreSubCategorias.length) {
                                                    for (var i_arescat = 0; i_arescat < are.AreSubCategorias.length; i_arescat++) {
                                                        var arescat = are.AreSubCategorias[i_arescat];
                                                        if (arescat) {
                                                            TotalRubroElegible=TotalRubroElegible+(arescat.CostoUnitario*arescat.MetaFisica);
                                                        }
                                                    }
                                                }
                                                
                                                html += '<tr class="tr-sub2-tot success">';
                                                html += '<td></td><td class="text-left" colspan="2">' + are.RubroElegible.Nombre + '</td>'
                                                        + '<td colspan="4"></td>'
                                                        + '<td><input type="text" class="form-control input-number" readonly="" value="'+TotalRubroElegible+'" /></td>';
                                                        
                                                var m=1;
                                                for (var i_crono_act = 0; i_crono_act < cantmeses.length; i_crono_act++) {
                                                    var compAa = _proyectoRecurso.Cronogramas[i_crono_act];
                                                    html += '<td align="center"> Mes ' + compAa.MesDescripcion + '</td>';
                                                    m++;
                                                }
                                              
                                                //
                                                html += '</tr>';
                                                // Subcategorías
                                                if (are.AreSubCategorias.length) {
                                                    for (var i_arescat = 0; i_arescat < are.AreSubCategorias.length; i_arescat++) {
                                                        var arescat = are.AreSubCategorias[i_arescat];
                                                        if (arescat) {
                                                            html += '<tr class="tr-subact warning">';
                                                            html += '<td align="center">';
                                                            var alertaRecurso="red";
                                                            var  metaFisicaRecurso1=0;
                                                            for (var i_crono_arescat1 = 0; i_crono_arescat1 < cantmeses.length; i_crono_arescat1++) {
                                                                metaFisicaRecurso1=parseFloat(metaFisicaRecurso1)+parseFloat(arescat.Cronogramas[i_crono_arescat1].MetaFisica);
                                                            }
                                                           
                                                            if (parseFloat(arescat.MetaFisica).toFixed(2)==parseFloat(metaFisicaRecurso1).toFixed(2)) {
                                                                alertaRecurso="black";
                                                            }
                                                            
                                                            var color='black';
                                                            if (arescat.Situacion==3) {
                                                                color='red';
                                                            }
                                                            
                                                            var perfil='<?= $seguimientosCount ?>';
                                                            
                                                            var rol=<?= \Yii::$app->user->identity->Rol ?>;
                                                            if (situacionRecurso==2 && perfil==0 && rol==5) {
                                                                html += '<span style="color:'+color+'"  class="fa fa-search generar-observacion-recurso" data-recurso-id="'+ arescat.ID +'"></span>';
                                                            }
                                                            else if (situacionRecurso==2 && perfil==1 && rol==2) {
                                                                html += '<span style="color:'+color+'" class="fa fa-search generar-observacion-recurso" data-recurso-id="'+ arescat.ID +'"></span>';
                                                            }else if (situacionRecurso==3 && arescat.Situacion==3) {
                                                                html +='<span data-toggle="tooltip" title="'+arescat.Observacion+'"><i class="fa fa-eye"></i></span>';
                                                            }
                                                            
                                                            if (arescat.ObservacionSituacionID==3) {
                                                                alertaRecurso='green';
                                                            }
                                                            html += '</td><td>' + arescat.Matriz + '</td><td style="color:'+alertaRecurso+'" class="recurso_alerta_'+arescat.ID+'">' + arescat.Nombre + '</td><td>'+arescat.Especifica+'</td><td>' + arescat.UnidadMedida
                                                                    + '</td><td><input type="text" class="form-control input-number" readonly="" value="' + arescat.CostoUnitario + '" />'
                                                                    + '</td><td><input type="text" style="color:'+alertaRecurso+'" class="recurso_alerta_'+arescat.ID+' form-control  input-number-sm recurso-meta-fisica" data-meta-fisica="'+ arescat.MetaFisica +'" data-recurso-id="'+ arescat.ID +'" readonly="" value="' + arescat.MetaFisica + '" />'
                                                                    + '</td><td><input type="text" class="form-control input-number" readonly="" value="'+(arescat.CostoUnitario*arescat.MetaFisica)+'" /></td>';
                                                            for (var i_crono_arescat = 0; i_crono_arescat < cantmeses.length; i_crono_arescat++) {
                                                                    
                                                                if (arescat.Cronogramas[i_crono_arescat]) {
                                                                    html += '<td><input readonly type="text"'
                                                                        + 'data-recurso-id="'+ arescat.ID +'"'
                                                                        + 'data-meta-fisica="'+ arescat.MetaFisica +'"'
                                                                        + 'data-cronograma-recurso-id="'+ arescat.Cronogramas[i_crono_arescat].ID +'" class="form-control success input-number recurso_'+arescat.ID+' btn-save-cronograma-recurso" value="'
                                                                        + arescat.Cronogramas[i_crono_arescat].MetaFisica + '" /></td>';
                                                                    //Max
                                                                    //End Max
                                                                }
                                                            }
                                                        }
                                                        
                                                        //end Max
                                                        html += '</tr>';
                                                    }
                                                }
                                            }
                                        }
                                        // Totales por componente
                                        html += '</tbody>';
                                        html += '</table>';
                                        html += '</div>';
                                        html += '</div>';
                                        html += '</div>';
                                    }
                                       
                                    $('#table-container-recursos').html(html);
                                    //
                                    //$('.table-responsive').css('max-height', $(window).height() * 0.60);
                                    $('.input-number').inputmask("decimal", {
                                        radixPoint: ".",
                                        groupSeparator: ",",
                                        groupSize: 3,
                                        digits: 2,
                                        integerDigits: 7,
                                        autoGroup: true,
                                        allowPlus: false,
                                        allowMinus: true,
                                        placeholder: ''
                                    }).click(function () {
                                        $(this).select();
                                    });
                                    
                                }

                                
                            </script>
                        </div>
                    </div>
                    <div class="tab-pane " id="tab-tarea">
                        <div id="container-poaf-tareas">
                            <style>
                                #container-poaf-tareas .input-number {
                                    text-align: right;
                                    min-width: 72px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }

                                #container-poaf-tareas .input-number-sm {
                                    text-align: right;
                                    min-width: 52px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }

                                #container-poaf-tareas .input-number-md {
                                    text-align: right;
                                    min-width: 82px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }
                                
                                #container-poaf-tareas table-act > tbody > tr > td:nth-child(1) {
                                    text-align: center;
                                }

                                #container-poaf-tareas .table-act > tbody > tr > td:nth-child(3) {
                                    min-width: 320px !important;
                                }

                                #container-poaf-tareas .table-act > tbody > tr > td:nth-child(2) {
                                    min-width: 100px !important;
                                }
                                
                                #container-poaf-tareas .table-act > tbody > tr > td:nth-child(4) {
                                    min-width: 100px !important;
                                }
                                #container-poaf-tareas .table-act > tbody > tr > td:nth-child(5) {
                                    min-width: 100px !important;
                                }
                                #container-poaf-tareas .table-act > tbody > tr > td:nth-child(6) {
                                    min-width: 80px !important;
                                }
                                #container-poaf-tareas .table-act > tbody > tr > td:nth-child(7) {
                                    min-width: 100px !important;
                                }
                                
                                #container-poaf-tareas .table > tbody > .tr-header > td {
                                   min-width: 50px;
                                    text-align: center;
                                    font-size: 12px;
                                    font-weight: bold;
                                    padding: 1px !important;
                                    vertical-align: middle !important;
                                    border: 1px solid #c0c0c0;
                                    background-color: #e3e3e3;
                                }

                                #container-poaf-tareas h4.panel-title {
                                    font-size: 16px;
                                }

                                #container-poaf-tareas .tr-comp td, .tr-proy td {
                                    font-weight: bold;
                                }

                                #container-poaf-tareas .table-condensed td {
                                    padding: 1px !important;
                                }

                                #container-poaf-tareas td, #table-container input {
                                    font-size: 13px !important;
                                }

                                #container-poaf-tareas .form-control[readonly] {
                                    background-color: #f2f3f4;
                                }
                                
                                .mes
                                {
                                    min-width: 50px !important;
                                }
                            </style>
                            <br>
                            <div id="table-container-tareas">
                            </div>
                            <br>
                            <div class="text-center">
                                <a class="btn btn-inverse-alt" href="<?= Yii::$app->getUrlManager()->createUrl('recursos-tarea/descargar-tarea?codigo='.$informacion->Codigo.'') ?>">
                                    <i class="fa fa-download"></i>&nbsp;Descargar Cronograma Técnico
                                </a>
                            </div> 
                            <script>
                                var _proyectoTarea = new Object();
                                function getProyectoTarea() {
                                    // var url = "<?= \Yii::$app->request->BaseUrl ?>/../json/recurso.json";
                                    var url = "<?= \Yii::$app->request->BaseUrl ?>/recursos-tarea/tareas-json?codigo=<?= $informacion->Codigo ?>";;
                                    return $.getJSON( url );
                                }
                                function _drawTableTarea() {
                                    var situacionTarea=<?= $situacionTarea ?>;
                                    var situacionProyecto=<?= $situacionProyecto ?>;
                                    var html = '';
                                    var cantmeses = _proyectoTarea.Cronogramas.length;
                                  
                                    for (var i_comp = 0; i_comp < _proyectoTarea.Componentes.length; i_comp++) {
                                        var comp = _proyectoTarea.Componentes[i_comp];
                                       
                                        html += '<div class="panel panel-gray"><div class="panel-heading">'
                                                + '<span class="pull-left"><h4 class="panel-title">'
                                                + (i_comp + 1) + '. Objetivo específico - ' + comp.Nombre
                                                + '</h4></span><span class="pull-right">Peso: '+comp.Peso+'</span></div>';
                                        html += '<div class="panel-body">';
                                        html += '<div class="table-responsive">';
                                        html += '<table class="table table-bordered table-condensed table-act">';
                                        html += '<tbody>';
                                        for (var i_act = 0; i_act < comp.Actividades.length; i_act++)
                                        {
                                            var acutotMF = 0.00;
                                            var acutotMFs = 0.00;
                                            var subacutotCat = 0.00;
                                            var subacutotCats = 0.00;
                                            var acuTotal = 0.00;
                                            var act = comp.Actividades[i_act];
                                            html += '<tr class="tr-header">';
                                            html += '<td>Peso</td><td>#</td><td>Actividad/Tarea</td><td>Unidad de medida</td><td>Meta física</td>';
                                            // Cronogramas
                                            for (var i_crono_act = 0; i_crono_act < cantmeses; i_crono_act++) {
                                                html += '<td class="mes">' + act.Cronogramas[i_crono_act].MesDescripcion + '</td>';
                                            }
                                            // Sumatorias
                                            html += '<tr class="tr-act-mf info" data-act-id="' + act.ID + '">';
                                            html += '<td align="center">'+act.Peso+'</td><td>' + (i_comp + 1) + '.' + (i_act + 1) + '</td>'
                                                    + '<td><b>' + act.Nombre + '</b></td><td>' + act.UnidadMedida
                                                    + '</td><td>'+act.MetaFisica
                                                    + '</td>';
                                            for (var i_crono_act = 0; i_crono_act < cantmeses; i_crono_act++) {
                                                html += '<td></td>';
                                                //Max
                                                acutotMF += act.Cronogramas[i_crono_act].PoafMetaFisica;
                                                //End Max
                                            }
                                            //
                                            html += '</tr>';
                                            html += '</tr>';
                                            // Rubros Elegibles
                                            html += '<tr>';
                                            html += '<td colspan="5"></td>';
                                            html += '</tr>';
                                            for (var i_are = 0; i_are < act.Tareas.length; i_are++) {
                                                var are = act.Tareas[i_are];
                                                var alertaRecurso="red";
                                                var  metaFisicaTarea1=0;
                                                for (var i_crono_are = 0; i_crono_are < cantmeses; i_crono_are++) {
                                                    metaFisicaTarea1=parseFloat(metaFisicaTarea1)+parseFloat(are.Cronogramas[i_crono_are].MetaFisica);
                                                }
                                                if (parseFloat(are.MetaFisica)==parseFloat(metaFisicaTarea1)) {
                                                    alertaRecurso="black";
                                                }
                                                
                                                html += '<tr class="tr-sub2-tot success" data-tarea-id="'+i_are+'"  data-meta-fisica="'+are.Cantidad+'">';
                                                html += '<td align="center">';
                                                var color='black';
                                                if (are.Situacion==3) {
                                                    color='red';
                                                }
                                                var perfil='<?= $seguimientosCount ?>';
                                                var rol=<?= \Yii::$app->user->identity->Rol ?>;
                                                if (situacionTarea==2 && perfil==0 && rol==5) {
                                                    html += '<span style="color:'+color+'" class="fa fa-search  generar-observacion-tarea" data-tarea-id="'+ are.ID +'"></span>'
                                                }
                                                else if (situacionTarea==2 && perfil==1 && rol==2) {
                                                    html += '<span style="color:'+color+'" class="fa fa-search  generar-observacion-tarea" data-tarea-id="'+ are.ID +'"></span>'
                                                }else if (situacionTarea==3 && are.Situacion==3) {
                                                    html +='<span data-toggle="tooltip" title="'+are.Observacion+'"><i class="fa fa-eye"></i></span>';
                                                }
                                                            
                                                html +='</td><td>' + (i_comp + 1) + '.' + (i_act + 1) + '.' + (i_are + 1) + '</td><td style="color:'+alertaRecurso+';">' + are.Descripcion + '</td><td>'+are.UnidadMedida+'</td><td><input type="text" style="color:'+alertaRecurso+';" data-tarea-id="'+are.ID+'" data-meta-fisica="'+are.MetaFisica+'" class="form-control tarea-meta-fisica" readonly value="' + are.MetaFisica+'" >'
                                                       
                                                for (var i_crono_are = 0; i_crono_are < cantmeses; i_crono_are++) {
                                                    
                                                    var cronogramaTarea=are.Cronogramas[i_crono_are];
                                                    html += '<td><input type="text" readonly data-meta-fisica="'+are.MetaFisica+'" data-tarea-id="'+are.ID+'" class="form-control input-number tarea_'+are.ID+' btn-save-cronograma-tarea input-sub2-total"  '
                                                            + 'data-cronograma-tarea-id="' + cronogramaTarea.ID + '" value="'+cronogramaTarea.MetaFisica
                                                            + '" /></td>';
                                                }
                                                html += '</tr>';
                                                
                                            }
                                        }
                                        html += '</tbody>';
                                        html += '</table>';
                                        html += '</div>';
                                        html += '</div>';
                                        html += '</div>';
                                    }
                                    $('#table-container-tareas').html(html);
                                    
                                    $('.input-number').inputmask("decimal", {
                                        radixPoint: ".",
                                        groupSeparator: ",",
                                        groupSize: 3,
                                        digits: 2,
                                        integerDigits: 7,
                                        autoGroup: true,
                                        allowPlus: false,
                                        allowMinus: true,
                                        placeholder: ''
                                    }).click(function () {
                                        $(this).select();
                                    });
                                   
                                }
                            </script>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-seguimiento-tarea">
                        <div id="container-seguimiento-tarea">
                            <style>
                                #container-seguimiento-tarea .input-number {
                                    text-align: right;
                                    min-width: 72px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }

                                #container-seguimiento-tarea .input-number-sm {
                                    text-align: right;
                                    min-width: 52px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }

                                #container-seguimiento-tarea .input-number-md {
                                    text-align: right;
                                    min-width: 82px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }
                                
                                #container-seguimiento-tarea table-act > tbody > tr > td:nth-child(1) {
                                    text-align: center;
                                }

                                #container-seguimiento-tarea .table-act > tbody > tr > td:nth-child(3) {
                                    min-width: 320px !important;
                                }

                                #container-seguimiento-tarea .table-act > tbody > tr > td:nth-child(2) {
                                    min-width: 100px !important;
                                }
                                
                                #container-seguimiento-tarea .table-act > tbody > tr > td:nth-child(4) {
                                    min-width: 100px !important;
                                }
                                #container-seguimiento-tarea .table-act > tbody > tr > td:nth-child(5) {
                                    min-width: 100px !important;
                                }
                                #container-seguimiento-tarea .table-act > tbody > tr > td:nth-child(6) {
                                    min-width: 80px !important;
                                }
                                #container-seguimiento-tarea .table-act > tbody > tr > td:nth-child(7) {
                                    min-width: 100px !important;
                                }
                                
                                #container-seguimiento-tarea .table > tbody > .tr-header > td {
                                   min-width: 50px;
                                    text-align: center;
                                    font-size: 12px;
                                    font-weight: bold;
                                    padding: 1px !important;
                                    vertical-align: middle !important;
                                    border: 1px solid #c0c0c0;
                                    background-color: #e3e3e3;
                                }

                                #container-seguimiento-tarea h4.panel-title {
                                    font-size: 16px;
                                }

                                #container-seguimiento-tarea .tr-comp td, .tr-proy td {
                                    font-weight: bold;
                                }

                                #container-seguimiento-tarea .table-condensed td {
                                    padding: 1px !important;
                                }

                                #container-seguimiento-tarea td, #table-container input {
                                    font-size: 13px !important;
                                }

                                #container-seguimiento-tarea .form-control[readonly] {
                                    background-color: #f2f3f4;
                                }
                                
                                .mes
                                {
                                    min-width: 50px !important;
                                }
                            </style>
                            <br>
                            <div id="table-container-seguimiento-tarea">
                            </div>
                            <br>
                            <div class="text-center">
                                <a class="btn btn-inverse-alt" href="<?= Yii::$app->getUrlManager()->createUrl('recursos-tarea/descargar-tarea?codigo='.$informacion->Codigo.'') ?>">
                                    <i class="fa fa-download"></i>&nbsp;Descargar Cronograma Técnico
                                </a>
                            </div> 
                            <script>
                                var _proyectoSeguimientoTarea = new Object();
                                function getProyectoSeguimientoTarea() {
                                    // var url = "<?= \Yii::$app->request->BaseUrl ?>/../json/recurso.json";
                                    var url = "<?= \Yii::$app->request->BaseUrl ?>/recursos-tarea-ejecutado-uafsi/tareas-json?codigo=<?= $informacion->Codigo ?>";;
                                    return $.getJSON( url );
                                }
                                function _drawTableSeguimientoTarea() {
                                    var situacionTarea=<?= $situacionTarea ?>;
                                    var situacionProyecto=<?= $situacionProyecto ?>;
                                    var html = '';
                                    var cantmeses = _proyectoSeguimientoTarea.Cronogramas.length;
                                  
                                    for (var i_comp = 0; i_comp < _proyectoSeguimientoTarea.Componentes.length; i_comp++) {
                                        var comp = _proyectoSeguimientoTarea.Componentes[i_comp];
                                        html += '<div class="panel panel-gray">';
                                        html += '<div class="panel-heading">'
                                                    + '<h4 class="panel-title">'
                                                        + (comp.Correlativo) + '. Objetivo específico - ' + comp.Nombre
                                                    + '</h4>'
                                        html += '</div>';
                                        html += '<div class="panel-heading">'
                                                    +'<span class="pull-left">'
                                                        +'Peso: '+comp.Peso+' %</span>'
                                                    +'<span class="pull-right">'
                                                +'Avance: <span class="avance_componente_'+comp.ID+'">'+comp.Avance+'%</span></span>';
                                        html += '</div>';
                                        html += '<div class="panel-body">';
                                        html += '<div class="table-responsive">';
                                        html += '<table class="table table-bordered table-condensed table-act">';
                                        html += '<tbody>';
                                        for (var i_act = 0; i_act < comp.Actividades.length; i_act++)
                                        {
                                            var act = comp.Actividades[i_act];
                                            html += '<tr class="tr-header">';
                                                html += '<td rowspan="2">#</td>';
                                                html += '<td rowspan="2">Peso</td>';
                                                html += '<td rowspan="2"></td>';
                                                html += '<td rowspan="2" style="display:none"></td>';
                                                html += '<td rowspan="2">Actividad/Tarea</td>';
                                                html += '<td rowspan="2">Unidad de medida</td>';
                                                html += '<td rowspan="2">Meta física</td>';
                                                html += '<td rowspan="2">Avance %</td>';
                                                html += '<td rowspan="2">Total</td>';
                                            // Cronogramas
                                            
                                            for (var i_crono_act = 0; i_crono_act < cantmeses; i_crono_act++) {
                                                //html += '<td class="mes" colspan="3">Mes ' + act.Cronogramas[i_crono_act].Mes + '</td>';
                                                html += '<td class="mes" colspan="3">Mes ' + act.Cronogramas[i_crono_act].MesDescripcion + '</td>';
                                            }
                                            // Sumatorias
                                            html += '<td rowspan="2">Total</td>';
                                            html +='<tr>';
                                                for (var i_crono_act = 0; i_crono_act < cantmeses; i_crono_act++) {
                                                    html += '<td align="center" class="p" style="vertical-align:middle;min-width:70px !important;background-color: #e3e3e3;border:1px solid #c0c0c0">PRO</td>';
                                                    html += '<td align="center" class="ejecutado" style="vertical-align:middle;min-width:70px !important;border:1px solid #c0c0c0;background-color: #e3e3e3">EJE</td>';
                                                    html += '<td align="center" class="ejecutado" style="vertical-align:middle;min-width:70px !important;border:1px solid #c0c0c0;background-color: #e3e3e3"></td>';
                                                    //html += '<td class="avance" style="vertical-align:middle;min-width:72px !important;border:1px solid #c0c0c0;background-color: #e3e3e3">% Avance</td>';
                                                    
                                                }
                                                html +='</tr>';
                                               
                                                html += '<tr class="tr-act-mf info" data-act-id="' + act.ID + '">';
                                                    html += '<td align="center">' + comp.Correlativo + '.' + act.Correlativo + '</td>'
                                                    html += '<td align="center">'+act.Peso.toFixed(2)+' %</td>';
                                                    
                                                    html += '<td align="center"><span class="total_actividad_'+act.ID+'">'+act.Avance+' %</span></td>';
                                                    //html += '<input class="contador" data-actividad-id="'+act.ID+'" type="hidden" value="'+ctareas+'">';
                                                    html += '<td style="display:none"></td>';
                                                    html += '<td align="center"><b>' + act.Nombre + '</b></td>';
                                                    html += '<td align="center">' + act.UnidadMedida +' </td>';
                                                    html += '<td align="center">'+act.MetaFisica +'</td>';
                                                    html += '<td></td>';
                                                    html += '<td></td>';
                                                for (var i_crono_act = 0; i_crono_act < cantmeses; i_crono_act++) {
                                                        html += '<td>';
                                                        if (act.Cronogramas[i_crono_act].MetaFisica>0) {
                                                        html += '<input disabled  type="text"'
                                                             +   'class="form-control success input-number actividad_'+act.ID+' " value="'
                                                             +   act.Cronogramas[i_crono_act].MetaFisica + '" />';
                                                        }
                                                        html +='</td>';
                                                        
                                                        html +='<td>';
                                                        if (act.Cronogramas[i_crono_act].MetaFisica>0) {
                                                            html +='<input '+deshabilitar+' data-cronograma-mes="'+act.Cronogramas[i_crono_act].Mes+'" data-cronograma-meta-fisica="'+act.Cronogramas[i_crono_act].MetaFisica+'" data-cronograma-actividad-id="'+act.Cronogramas[i_crono_act].ID+'" data-actividad-id="'+act.ID+'" type="text" class="form-control success input-number actividad_ejecutado_'+act.ID+' btn-save-cronograma-actividad-ejecutado" value="'+act.Cronogramas[i_crono_act].MetaFisicaEjecutada+'">';
                                                            
                                                        }
                                                        
                                                        html +='</td>';
                                                        html +='<td>';
                                                        
                                                        if ((act.Cronogramas[i_crono_act].MetaFisicaEjecutada<act.Cronogramas[i_crono_act].MetaFisica) && act.Cronogramas[i_crono_act].MetaFisica!=0 && act.Cronogramas[i_crono_act].MetaFisica!=null && act.Cronogramas[i_crono_act].MesActual==1) {
                                                            html +='<a href="#" data-cronograma-mes="'+act.Cronogramas[i_crono_act].Mes+'" data-cronograma-meta-fisica="'+act.Cronogramas[i_crono_act].MetaFisica+'" data-cronograma-actividad-id="'+act.Cronogramas[i_crono_act].ID+'" data-actividad-id="'+act.ID+'" class="actividad_ejecutado_'+act.ID+'"><span style="color:red" class="fa fa-commenting-o "></span></a>';
                                                            actividad_alertas++;
                                                        }
                                                        
                                                        html +='</td>';
                                                        
                                                    //Max
                                                    //acutotMF += act.Cronogramas[i_crono_act].PoafMetaFisica;
                                                    //End Max
                                                }
                                                html += '<td></td>';
                                                //acutotMF = 0.00;
                                                //
                                                html += '</tr>';
                                            html += '</tr>';
                                          
                                            // Rubros Elegibles
                                            html += '<tr>';
                                                html += '<td colspan="8"> </td>';
                                            html += '</tr>';
                                            var ctareas=1;
                                            for (var i_are = 0; i_are < act.Tareas.length; i_are++) {
                                                
                                                var are = act.Tareas[i_are];
                                                html += '<tr class="tr-sub2-tot success" data-tarea-id="'+i_are+'"  data-meta-fisica="'+are.MetaFisica+'">';
                                                    html += '<td align="center">'+ comp.Correlativo + '.' + act.Correlativo + '.' + are.Correlativo + '</td>';
                                                    html += '<td align="center"></td>';
                                                    
                                                    html += '<td align="center" style="color:#ebf6e1"><span class="ponderacion_tar_'+are.ID+'">'+(are.Peso*are.MetaAvance/100).toFixed(2)+' %</span></td>';
                                                    html += '<input class="contador" data-actividad-id="'+act.ID+'" type="hidden" value="'+(are.Peso*are.MetaAvance/100)+'">';
                                                    html += '<td align="center" style="display:none;min-width:60px !important;color:#ebf6e1"><span class="ponderacion_tareat_'+act.ID+'">'+are.Peso+' %</span></td>'
                                                    html += '<th align="center">'+ are.Descripcion + '</th>';
                                                    html += '<td align="center">'+ are.UnidadMedida+'</td>';
                                                    if (!are.MetaAvance) {
                                                        are.MetaAvance=0.00;
                                                    }
                                                    html += '<td align="center"><input type="text" data-tarea-id="'+are.ID+'" data-meta-fisica="'+are.MetaFisica+'" class="form-control tarea-meta-fisica" readonly value="' + are.MetaFisica+'" ></td>'
                                                    html += '<td align="center"><span class="avance_total_tarea_'+are.ID+'">'+are.MetaAvance+' %</span></td>';
                                                    
                                                    html += '<td></td>'
                                                    for (var i_crono_are = 0; i_crono_are < cantmeses; i_crono_are++) {
                                                        
                                                        var cronogramaTarea=are.Cronogramas[i_crono_are];
                                                        var deshabilitar='disabled';
                                                        if (are.Cronogramas[i_crono_are].SituacionEjecucion==0) {
                                                            deshabilitar='';
                                                        }
                                                        
                                                        html += '<td>';
                                                        if (are.Cronogramas[i_crono_are].MetaFisica>0) {
                                                            html += '<input disabled type="text"'+ 'class="form-control success input-number tarea_'+are.ID+'" value="'+ are.Cronogramas[i_crono_are].MetaFisica + '" />';
                                                        }
                                                        html +='</td>';
                                                        
                                                        html +='<td>';
                                                        if (are.Cronogramas[i_crono_are].MetaFisica>0) {
                                                            html +='<input disabled data-cronograma-mes="'+are.Cronogramas[i_crono_are].Mes+'" data-cronograma-meta-fisica="'+are.Cronogramas[i_crono_are].MetaFisica+'" data-cronograma-tarea-id="'+are.Cronogramas[i_crono_are].ID+'" data-tarea-id="'+are.ID+'" type="text" class="form-control success input-number tarea_ejecutado_'+are.ID+' btn-save-cronograma-tarea-ejecutado" value="'+are.Cronogramas[i_crono_are].MetaFisicaEjecutada+'">';
                                                        }
                                                        html +='</td>';
                                                        html +='<td>';
                                                        
                                                        if (are.Cronogramas[i_crono_are].MetaFisica!=0 && are.Cronogramas[i_crono_are].MetaFisica!=null && are.Cronogramas[i_crono_are].MesActual==1) {
                                                            if (are.Cronogramas[i_crono_are].SituacionEjecucion==3) {
                                                                html +='<a href="#" data-cronograma-mes="'+are.Cronogramas[i_crono_are].Mes+'" data-cronograma-meta-fisica-ejecutada="'+are.Cronogramas[i_crono_are].MetaFisicaEjecutada+'" data-cronograma-meta-fisica="'+are.Cronogramas[i_crono_are].MetaFisica+'" data-cronograma-tarea-id="'+are.Cronogramas[i_crono_are].ID+'" data-tarea-id="'+are.ID+'" class="tarea_ejecutado_'+act.ID+' eliminar-seguimiento-tarea"><span class="fa fa-remove " ></span></a> ';
                                                            }
                                                            else
                                                            {
                                                                html +='<a href="#" data-cronograma-mes="'+are.Cronogramas[i_crono_are].Mes+'" data-cronograma-meta-fisica-ejecutada="'+are.Cronogramas[i_crono_are].MetaFisicaEjecutada+'" data-cronograma-meta-fisica="'+are.Cronogramas[i_crono_are].MetaFisica+'" data-cronograma-tarea-id="'+are.Cronogramas[i_crono_are].ID+'" data-tarea-id="'+are.ID+'" class="tarea_ejecutado_'+act.ID+' justificar-seguimiento-tarea"><span class="fa fa-eye " ></span></a> ';
                                                            html +='<a href="#" data-cronograma-mes="'+are.Cronogramas[i_crono_are].Mes+'" data-cronograma-meta-fisica-ejecutada="'+are.Cronogramas[i_crono_are].MetaFisicaEjecutada+'" data-cronograma-meta-fisica="'+are.Cronogramas[i_crono_are].MetaFisica+'" data-cronograma-tarea-id="'+are.Cronogramas[i_crono_are].ID+'" data-tarea-id="'+are.ID+'" class="tarea_ejecutado_'+act.ID+' observar-seguimiento-tarea"><span class="fa fa-commenting-o"></span></a> ';
                                                            html +='<a href="#" data-cronograma-mes="'+are.Cronogramas[i_crono_are].Mes+'" data-cronograma-meta-fisica-ejecutada="'+are.Cronogramas[i_crono_are].MetaFisicaEjecutada+'" data-cronograma-meta-fisica="'+are.Cronogramas[i_crono_are].MetaFisica+'" data-cronograma-tarea-id="'+are.Cronogramas[i_crono_are].ID+'" data-tarea-id="'+are.ID+'" class="tarea_ejecutado_'+act.ID+' confirmar-seguimiento-tarea"><span class="fa fa-check"></span></a>';
                                                            }
                                                        
                                                        }
                                                        html +='</td>';
                                                    }
                                                    html += '<td></td>';
                                                    
                                                //
                                                html += '</tr>';
                                                
                                                ctareas++;
                                            }
                                            
                                            
                                        }
                                        // Totales por componente
                                        
                                        html += '</tbody>';
                                        html += '</table>';
                                        html += '</div>';
                                        html += '</div>';
                                        html += '</div>';
                                    }
                                    $('#table-container-seguimiento-tarea').html(html);
                                    
                                    $('.input-number').inputmask("decimal", {
                                        radixPoint: ".",
                                        groupSeparator: ",",
                                        groupSize: 3,
                                        digits: 2,
                                        integerDigits: 7,
                                        autoGroup: true,
                                        allowPlus: false,
                                        allowMinus: true,
                                        placeholder: ''
                                    }).click(function () {
                                        $(this).select();
                                    });
                                   
                                }
                            </script>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-pasos-criticos">
                        <div id="container-poaf">
                            <div id="container-pasos-criticos">
                                <div class="table-responsive">
                                    <table id="proyectos" class="display table table-bordered table-striped table-condensed table-hover" cellspacing="0" >
                                        <thead>
                                            <tr>
                                                <!-- <th>Código Proyecto</th> -->
                                                <th>Proyecto</th>
                                                <th>Correlativo</th>
                                                <th>Mes Inicio</th>
                                                <th>Mes Fin</th>
                                                <th>Monto</th>
                                                <th>Situación</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-carga-itf">
                        <div id="container-poaf-itf">
                            
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal-ctrl-recurso" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Detalle de la observación</h4>
                        </div>
                        <div class="modal-body-main">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal-loading" class="modal fade modal-loading in" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-hidden="false" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content-no-shadow modal-content">
            <div class="text-center"><i class="fa fa-spinner fa-pulse fa-fw" style="color:#16a085;font-size: 100px;"></i></div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-ctrl-observacion" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Observación</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-control-presupuesto" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Consolidado por clasificador por monto</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-ctrl-paso-critico" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Paso Crítico</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-control-justificacion" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Justificación</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-control-observar-seguimiento-tarea" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Observar</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>


<script>

    $('body').on('click', '.presupuesto', function (e) {
        e.preventDefault();
        var CodigoProyecto = '<?= $informacion->Codigo ?>';
        $('#modal-control-presupuesto .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/recursos-poa/presupuesto?CodigoProyecto='+CodigoProyecto);
        $('#modal-control-presupuesto').modal('show');
    });

    $('body').on('click', '.btn-observar-poa', function (e) {
        e.preventDefault();
        var ProyectoCodigo="<?= $informacion->Codigo ?>";
        var NivelAprobacionCorrelativo="<?= $NivelAprobacionCorrelativo ?>";
        $('#modal-ctrl-observacion .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/evaluacion-proyecto/observar-proyecto?ProyectoCodigo='+ProyectoCodigo+'&NivelAprobacionCorrelativo='+NivelAprobacionCorrelativo);
        $('#modal-ctrl-observacion').modal('show');
    });
    
    
    $('body').on('click', '.btn-habilitar-recursos', function (e) {
        e.preventDefault();
        
        var CodigoProyecto="<?= $informacion->Codigo ?>";
        
        bootbox.confirm({
            title: '<span style="text-align:center">PNIA - CONFIRMACIÓN</span>',
            message: '¿Está seguro de habilitar la edición de recursos?.',
            buttons: {
                'cancel': {
                    label: 'Cancelar'
                },
                'confirm': {
                    label: 'Aceptar'
                }
            },
            callback: function (confirmed) {
                if (confirmed) {
                    var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                    $.ajax({
                        url: '<?= Yii::$app->getUrlManager()->createUrl('evaluacion-proyecto/habilitar-recursos') ?>',
                        type: 'GET',
                        async: false,
                        data: {CodigoProyecto:CodigoProyecto,_csrf: toke},
                        success: function(data){
                            location.reload();
                        }
                    });
                } 
            }
        });
        
    });
    
    $('body').on('click', '.btn-deshabilitar-recursos', function (e) {
        e.preventDefault();
        
        var CodigoProyecto="<?= $informacion->Codigo ?>";
        
        bootbox.confirm({
            title: '<span style="text-align:center">PNIA - CONFIRMACIÓN</span>',
            message: '¿Está seguro de habilitar la edición de recursos?.',
            buttons: {
                'cancel': {
                    label: 'Cancelar'
                },
                'confirm': {
                    label: 'Aceptar'
                }
            },
            callback: function (confirmed) {
                if (confirmed) {
                    var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                    $.ajax({
                        url: '<?= Yii::$app->getUrlManager()->createUrl('evaluacion-proyecto/deshabilitar-recursos') ?>',
                        type: 'GET',
                        async: false,
                        data: {CodigoProyecto:CodigoProyecto,_csrf: toke},
                        success: function(data){
                            location.reload();
                        }
                    });
                } 
            }
        });
        
    });
    
    $('body').on('click', '#tab-informacion-general', function (e) {
        var investigadorID=<?= $investigadorID ?>;
        $('#container-informacion-general').html('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>');
        $('#container-informacion-general').load('<?= \Yii::$app->request->BaseUrl ?>/informacion-general/index?investigador='+investigadorID);
        $("#container-informacion-general input,#container-informacion-general textarea,#container-informacion-general select").prop("disabled", true);
    });
    
    
    
    $('body').on('click', '.tab-marco-logico', function (e) {
        var investigadorID=<?= $investigadorID ?>;
        $('#container-marco-logico').html('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>');
        $('#container-marco-logico').load('<?= \Yii::$app->request->BaseUrl ?>/marco-logico/index?investigadorID='+investigadorID);
        $("#container-marco-logico input,textarea,select").prop("disabled", true);
        $("#container-marco-logico .btn-edit-ml-finproyecto").prop("disabled", true);
    });
    
    $('body').on('click', '.tab-recursos', function (e) {
        $('#table-container-recursos').html('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>');
        getProyectoRecurso().done(function(json) {
            _proyectoRecurso = json;
            _drawTableRecurso();
        });
    });
    
    $('body').on('click', '.tab-tarea', function (e) {
        $('#table-container-tareas').html('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>');
        getProyectoTarea().done(function(json) {
            _proyectoTarea = json;
            _drawTableTarea();
        });
    });
    
    $('body').on('click', '.tab-seguimiento-tarea', function (e) {
        $('#table-container-seguimiento-tarea').html('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>');
        getProyectoSeguimientoTarea().done(function(json) {
            _proyectoSeguimientoTarea = json;
            _drawTableSeguimientoTarea();
        });
    });
    
    
    $('body').on('click', '.tab-pasos-criticos', function (e) {
        var ProyectoID="<?= $proyecto->ID ?>";
        //$('#container-pasos-criticos').html('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>');
        $.ajax({
            url:'<?= \Yii::$app->request->BaseUrl ?>/paso-critico/lista-paso-critico-uafsi?ProyectoID='+ProyectoID,
            async:false,
            data:{},
            beforeSend:function()
            {
                
            },
            success:function(result)
            {
                $("#container-pasos-criticos #proyectos tbody").html(result);
            },
            error:function(){
                alert('Error al realizar el proceso de busqueda.');
            }
        });
    });
    
    $('body').on('click', '.btn-habilitar-paso-critico', function (e) {
        e.preventDefault();
        var PasoCriticoID = $(this).attr('data-paso-critico-id');
        bootbox.confirm({
            title: '<span style="text-align:center">PNIA - CONFIRMACIÓN</span>',
            message: '¿Está seguro de habilitar el paso crítico?.',
            buttons: {
                'cancel': {
                    label: 'Cancelar'
                },
                'confirm': {
                    label: 'Aceptar'
                }
            },
            callback: function (confirmed) {
                if (confirmed) {
                    var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                    $.ajax({
                        url: '<?= Yii::$app->getUrlManager()->createUrl('paso-critico/habilitar-paso-critico') ?>',
                        type: 'GET',
                        async: false,
                        data: {PasoCriticoID:PasoCriticoID,_csrf: toke},
                        success: function(data){
                            location.reload();
                        }
                    });
                } 
            }
        });
    });
    
    $('body').on('click', '.btn-actualizar-paso-critico', function (e) {
        e.preventDefault();
        var PasoCriticoID = $(this).attr('data-paso-critico-id');
        $('#modal-ctrl-paso-critico .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/paso-critico/actualizar?PasoCriticoID='+PasoCriticoID);
        $('#modal-ctrl-paso-critico').modal('show');
    });
    
    $('body').on('click', '#btn-save-paso-critico', function (e) {
        
        e.preventDefault();
        var $btn = $(this);
        $btn.attr('disabled', 'disabled');
        var isValid = formParsleyDesembolsos.validate();
        if (isValid) {
            sendFormNoMessage($('#frmDesembolsos'), $(this), function (msg) {
                $("#modal-ctrl-paso-critico").modal('hide');
                $( ".tab-pasos-criticos" ).click();
                _pageLoadingEnd();
		
            });
        } else {
        }
    });
    
    
    $('body').on('click', '.btn-habilitar-cronograma-recursos', function (e) {
        e.preventDefault();
        
        var CodigoProyecto="<?= $informacion->Codigo ?>";
        
        bootbox.confirm({
            title: '<span style="text-align:center">PNIA - CONFIRMACIÓN</span>',
            message: '¿Está seguro de habilitar la edición de recursos?.',
            buttons: {
                'cancel': {
                    label: 'Cancelar'
                },
                'confirm': {
                    label: 'Aceptar'
                }
            },
            callback: function (confirmed) {
                if (confirmed) {
                    var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                    $.ajax({
                        url: '<?= Yii::$app->getUrlManager()->createUrl('evaluacion-proyecto/habilitar-cronograma-recursos') ?>',
                        type: 'GET',
                        async: false,
                        data: {CodigoProyecto:CodigoProyecto,_csrf: toke},
                        success: function(data){
                            location.reload();
                        }
                    });
                } 
            }
        });
        
    });
    
    $('body').on('click', '.btn-deshabilitar-cronograma-recursos', function (e) {
        e.preventDefault();
        
        var CodigoProyecto="<?= $informacion->Codigo ?>";
        
        bootbox.confirm({
            title: '<span style="text-align:center">PNIA - CONFIRMACIÓN</span>',
            message: '¿Está seguro de habilitar la edición de recursos?.',
            buttons: {
                'cancel': {
                    label: 'Cancelar'
                },
                'confirm': {
                    label: 'Aceptar'
                }
            },
            callback: function (confirmed) {
                if (confirmed) {
                    var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                    $.ajax({
                        url: '<?= Yii::$app->getUrlManager()->createUrl('evaluacion-proyecto/deshabilitar-cronograma-recursos') ?>',
                        type: 'GET',
                        async: false,
                        data: {CodigoProyecto:CodigoProyecto,_csrf: toke},
                        success: function(data){
                            location.reload();
                        }
                    });
                } 
            }
        });
        
    });
    
    
    $('body').on('click', '.generar-observacion-recurso', function (e) {
        e.preventDefault();
        var recursoID=$(this).attr('data-recurso-id');
        
        var ProyectoCodigo="<?= $informacion->Codigo ?>";;
        $('#modal-ctrl-observacion .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/evaluacion-proyecto/observar-recurso?RecursoID='+recursoID);
        $('#modal-ctrl-observacion').modal('show');
    });
    $('body').on('click', '.generar-observacion-tarea', function (e) {
        e.preventDefault();
        var tareaID=$(this).attr('data-tarea-id');
        
        var ProyectoCodigo="<?= $informacion->Codigo ?>";;
        $('#modal-ctrl-observacion .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/evaluacion-proyecto/observar-tarea?TareaID='+tareaID);
        $('#modal-ctrl-observacion').modal('show');
    });
    
    $('body').on('click', '#btn-observacion-proyecto-p', function () {
        e.preventDefault();
        var $btn = $(this);
        $btn.attr('disabled', 'disabled');
        var isValid = formParsleySeguimiento.validate();
        if (isValid) {
            sendForm($('#frmSeguimientoObservado'), $(this), function (eve) {
                $("#modal-ctrl-observacion").modal('hide');
            });
        }
    });
    
    
    $('body').on('click', '#btn-observacion-recurso-p', function () {
        
        var $btn = $(this);
        $btn.attr('disabled', 'disabled');
        var isValid = formParsleyObservarRecurso.validate();
        if (isValid) {
            sendForm($('#frmObservarRecurso'), $(this), function (eve) {
                $("#modal-ctrl-observacion").modal('hide');
            });
        }
    });

    $('body').on('click', '#btn-observacion-gastos', function () {
        var $btn = $(this);
        $btn.attr('disabled', 'disabled');
        var isValid = formParsleyObservarGasto.validate();
        if (isValid) {
            sendForm($('#frmObservarCargaItf'), $(this), function (eve) {
                $("#modal-ctrl-observacion").modal('hide');
                var CodigoProyecto="<?= $informacion->Codigo ?>";
                $('#container-poaf-itf').html('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>');
                $.ajax({
                    url:'<?= \Yii::$app->request->BaseUrl ?>/gastos-generales/lista-aprobacion?CodigoProyecto='+CodigoProyecto,
                    async:false,
                    data:{},
                    beforeSend:function()
                    {
                        
                    },
                    success:function(result)
                    {
                        $("#container-poaf-itf").html(result);
                    },
                    error:function(){
                        alert('Error al realizar el proceso de busqueda.');
                    }
                });

            });
        }
    });


    $('body').on('click', '#btn-observacion-tarea-p', function () {
        
        var $btn = $(this);
        $btn.attr('disabled', 'disabled');
        var isValid = formParsleyObservarTarea.validate();
        if (isValid) {
            sendForm($('#frmObservarTarea'), $(this), function (eve) {
                
                $("#modal-ctrl-observacion").modal('hide');
               //_pageLoadingEnd();
            });
            
        }
    });
    
    
    $('.btn-habilitar').click(function (e) {
        var $btn = $(this);
        bootbox.confirm({
            title: '<span style="text-align:center">PNIA - CONFIRMACIÓN</span>',
            message: 'Está seguro de habilitar la programación?.',
            buttons: {
                'cancel': {
                    label: 'CANCELAR'
                },
                'confirm': {
                    label: 'CONFIRMAR'
                }
            },
            callback: function (confirmed) {
                if (confirmed) {
                    var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                    $.ajax({
                        url: '<?= Yii::$app->getUrlManager()->createUrl('evaluacion-proyecto/habilitar-proyecto') ?>',
                        type: 'GET',
                        async: false,
                        data: {aprobar:1,codigo:'<?= $informacion->Codigo ?>',_csrf: toke},
                        success: function(data){
                            location.reload();
                        }
                    });
                } 
            }
        });
    });
    
    $('.btn-aprobar-poa').click(function (e) {
        var $btn = $(this);
        bootbox.confirm({
            title: '<span style="text-align:center">PNIA - CONFIRMACIÓN</span>',
            message: 'Está seguro de aprobar el poa del proyecto?.',
            buttons: {
                'cancel': {
                    label: 'CANCELAR'
                },
                'confirm': {
                    label: 'APROBAR POA'
                }
            },
            callback: function (confirmed) {
                if (confirmed) {
                    var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                    $.ajax({
                        url: '<?= Yii::$app->getUrlManager()->createUrl('evaluacion-proyecto/aprobar-proyecto') ?>',
                        type: 'GET',
                        async: false,
                        data: {aprobar:1,codigo:'<?= $informacion->Codigo ?>',_csrf: toke},
                        success: function(data){
                            location.reload();
                        }
                    });
                } 
            }
        });
    });
    $(document).ready(function(){
        $('.fa-search').popover(); 
    });
    $('[data-rel=tooltip]').tooltip();
      



    $('body').on('click', '.tab-carga-itf', function (e) {
        var CodigoProyecto="<?= $informacion->Codigo ?>";
        $('#container-poaf-itf').html('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>');
        $.ajax({
            url:'<?= \Yii::$app->request->BaseUrl ?>/gastos-generales/lista-aprobacion?CodigoProyecto='+CodigoProyecto,
            async:false,
            data:{},
            beforeSend:function()
            {
                
            },
            success:function(result)
            {
                $("#container-poaf-itf").html(result);
            },
            error:function(){
                alert('Error al realizar el proceso de busqueda.');
            }
        });
    });

    $('body').on('click', '.generar-observacion-itf', function (e) {
        e.preventDefault();
        var CodigoProyecto="<?= $informacion->Codigo ?>";
        var GastoID=$(this).attr('data-recurso-id');
        var ProyectoCodigo="<?= $informacion->Codigo ?>";;
        $('#modal-ctrl-observacion .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/evaluacion-proyecto/observar-carga-itf?GastoID=&CodigoProyecto='+CodigoProyecto);
        $('#modal-ctrl-observacion').modal('show');
    });



    $('body').on('click', '.generar-aprobacion-itf', function (e) {
        var $btn = $(this);
        bootbox.confirm({
            title: '<span style="text-align:center">PNIA - CONFIRMACIÓN</span>',
            message: 'Está seguro de aprobar el Gasto?',
            buttons: {
                'cancel': {
                    label: 'CANCELAR'
                },
                'confirm': {
                    label: 'APROBAR'
                }
            },
            callback: function (confirmed) {
                if (confirmed) {
                    var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                    $.ajax({
                        url: '<?= Yii::$app->getUrlManager()->createUrl('evaluacion-proyecto/aprobar-gastos') ?>',
                        type: 'GET',
                        async: false,
                        data: {aprobar:1,codigo:'<?= $informacion->Codigo ?>',_csrf: toke},
                        success: function(data){
                            location.reload();
                        }
                    });
                } 
            }
        });
    });
    
    $('body').on('click', '.justificar-seguimiento-tarea', function (e) {
        e.preventDefault();
        var CronogramaTareaID = $(this).attr('data-cronograma-tarea-id');
        var metafisicaejecutada = $(this).attr('data-cronograma-meta-fisica-ejecutada');
        var metafisica = $(this).attr('data-cronograma-meta-fisica');
        $('#modal-control-justificacion .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/recursos-tarea-ejecutado-uafsi/justificacion-tarea?CronogramaTareaID='+CronogramaTareaID+'');
        $('#modal-control-justificacion').modal('show');
    });
    
    $('body').on('click', '.observar-seguimiento-tarea', function (e) {
        e.preventDefault();
        var InvestigadorID=<?= $investigadorID ?>;
        var CronogramaTareaID = $(this).attr('data-cronograma-tarea-id');
        var CodigoProyecto="<?= $informacion->Codigo ?>";
        var metafisicaejecutada = $(this).attr('data-cronograma-meta-fisica-ejecutada');
        var metafisica = $(this).attr('data-cronograma-meta-fisica');
        $('#modal-control-observar-seguimiento-tarea .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/recursos-tarea-ejecutado-uafsi/observar-tarea?CronogramaTareaID='+CronogramaTareaID+'&CodigoProyecto='+CodigoProyecto+'&InvestigadorID='+InvestigadorID);
        $('#modal-control-observar-seguimiento-tarea').modal('show');
    });
    
    $(document).on('click','.confirmar-seguimiento-tarea',function(e){
        e.preventDefault();
        var CronogramaTareaID=$(this).attr('data-cronograma-tarea-id');
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        bootBoxConfirm('¿Esta seguro de confirmar la tarea?',function(){
            $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/recursos-tarea-ejecutado-uafsi/confirmar-tarea',
                type: 'POST',
                async: false,
                data: {CronogramaTareaID:CronogramaTareaID,_csrf: toke},
                success: function (data) {
                    location.reload();
                },
                error: function () {
                    _pageLoadingEnd();
                }
            });
        });
    });
    
    
    $(document).on('click','.eliminar-seguimiento-tarea',function(e){
        e.preventDefault();
        var CronogramaTareaID=$(this).attr('data-cronograma-tarea-id');
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        bootBoxConfirm('¿Esta seguro de habilitar la edición?',function(){
            $.ajax({
                url: '<?= \Yii::$app->request->BaseUrl ?>/recursos-tarea-ejecutado-uafsi/habilitar-tarea',
                type: 'POST',
                async: false,
                data: {CronogramaTareaID:CronogramaTareaID,_csrf: toke},
                success: function (data) {
                    location.reload();
                },
                error: function () {
                    _pageLoadingEnd();
                }
            });
        });
    });
    
</script>