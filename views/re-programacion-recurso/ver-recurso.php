<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\DetalleReProgramacionRecurso;
?>
<div class="modal-body">
    <div id="wrap">
        <div id="page-heading">
            <h1>Reprogramación de recursos</h1>
        </div>
        <div class="container">
            <style>
                .div-label-left b {
                    margin-bottom: 10px;
                    font-size: 18px;
                }
                .count-message {
                    color: #2460AA;
                    margin-top: 5px;
                }
            </style>
            <style>
                .table > tbody > .tr-header > td {
                    text-align: center;
                    font-size: 16px;
                    font-weight: bold;
                    padding: 1px !important;
                    vertical-align: middle !important;
                    /*border: 1px solid #cfcfd0;
                    background-color: #f0f0f1;*/
                    border: 1px solid #c0c0c0;
                    background-color: #e3e3e3;
                    min-width: 75px !important;
                }
                
            </style>
            <?php // $form = ActiveForm::begin(); ?>
            <?php $form = ActiveForm::begin(
                [
                    'action'            => '',
                    'options'           => [
                        'enctype'       =>'multipart/form-data',
                        'id'            => 'frmReprogramacion'
                    ]
                ]
            ); ?>
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <div id="container-resp"></div>
                        <div class="form-horizontal">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <b>Objetivo: </b>  <?= $Componente->Nombre ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <b>Actividad: </b>  <?= $Actividad->Nombre ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <b>Recurso: </b>  <?= $Recurso->Nombre.'/'.$Recurso->Especifica ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label>Justificación:</label>
                                    <textarea class="form-control" name="ReProgramacionRecurso[Justificacion]" disabled><?= $ReProgramarRecurso->Justificacion ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label>Evidencia:</label>
                                    <div class="col-sm-12">
                                        <a target="_blank" href="<?= \Yii::$app->request->BaseUrl ?>/reprogramaciontareas/<?= $ReProgramarRecurso->Evidencia ?>" class="btn btn-primary" >
                                           <span class="fa fa-download"></span> Descargar 
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="table-responsive col-sm-12">
                                    <table id="repro" class="table table-bordered table-condensed ">
                                        <tbody>
                                            <tr class="tr-header">
                                                <td rowspan="2">Mes</td>
                                                <td colspan="2">Metas - Programación Original</td>
                                                <td colspan="2">Metas - Reprogramación</td>
                                            </tr>
                                            <tr class="tr-header">
                                                <td>Física</td>
                                                <td>Financiera</td>
                                                <td>Física</td>
                                                <td>Financiera</td>
                                            </tr>
                                            <?php foreach($CronogramaRecursos as $CronogramaRecurso){?>
                                            <tr>
                                                <?php $detalle=DetalleReProgramacionRecurso::find()
                                                            ->where('Mes=:Mes and ReProgramacionRecursoID=:ReProgramacionRecursoID',[':Mes'=>$CronogramaRecurso->Mes,':ReProgramacionRecursoID'=>$ReProgramarRecurso->ID])
                                                            ->one()
                                                            ?>
                                                <td><?= Yii::$app->tools->DescripcionMes($CronogramaRecurso->Mes) ?></td>
                                                
                                                <?php if($CronogramaRecurso->MetaFisica!=0){ ?>
                                                <td><?= $CronogramaRecurso->MetaFisica ?></td>
                                                <td><?= $Recurso->CostoUnitario ?></td>
                                                <?php }else{ ?>
                                                <td>0.00</td>
                                                <td>0.00</td>
                                                <?php }?>
                                                
                                                <?php if($detalle && $detalle->Mes==$CronogramaRecurso->Mes){?>
                                                <td><input type="text" value="<?= $detalle->MetaFisica ?>" class="form-control" disabled></td>
                                                <td><input type="text" value="<?= $detalle->CostoUnitario ?>" class="form-control" disabled></td>
                                                <?php }else{ ?>
                                                <td><input type="text" value="0" class="form-control" disabled></td>
                                                <td><input type="text" value="0" class="form-control" disabled></td>
                                                <?php } ?>
                                            </tr>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<script>
    var $form = $('#frmReprogramacion');
    var formReprogramacion = $form.parsley(defaultParsleyForm());
    $(document).on('click','.btn-finalizar',function(){
        var a = $(this).attr('data-id');
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        bootBoxConfirm('Esta seguro que desea finalizar el registro',function(){
            return true;
        });
        return false;
    });
</script>