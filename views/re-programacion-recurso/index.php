<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<div id="page-content" style="min-height: 1342px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Reprogramación de recursos</h1>
        </div>
        <div class="container">
            <style>
                .div-label-left b {
                    margin-bottom: 10px;
                    font-size: 18px;
                }
                .count-message {
                    color: #2460AA;
                    margin-top: 5px;
                }
            </style>
            <?php // $form = ActiveForm::begin(); ?>
            <?php $form = ActiveForm::begin(
                [
                    'action'            => '',
                    'options'           => [
                        'enctype'       =>'multipart/form-data',
                        'id'            => 'frmInformacionGeneral'
                    ]
                ]
            ); ?>
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <div id="container-resp"></div>
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Objetivos</label>
                                <div class="col-sm-6">
                                    <select class="form-control" id="objetivos">
                                        <option value>Seleccionar</option>
                                        <?php foreach($Componentes as $Componente){ ?>
                                        <option value="<?= $Componente->ID ?>"><?= $Componente->Correlativo.' '.$Componente->Nombre ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Actividades</label>
                                <div class="col-sm-6">
                                    <select class="form-control" id="actividades">
                                        <option value>Seleccionar</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div id="recursos" class="col-sm-12">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<style>
    #recursos .table > tbody > .tr-header > td {
        text-align: center;
        font-size: 14px;
        font-weight: bold;
        padding: 1px !important;
        vertical-align: middle !important;
        /*border: 1px solid #cfcfd0;
        background-color: #f0f0f1;*/
        border: 1px solid #c0c0c0;
        background-color: #e3e3e3;
        min-width: 75px !important;
    }
    
</style>
<?php
    $actividades= Yii::$app->getUrlManager()->createUrl('termino-referencia/actividades');
    $recursos=Yii::$app->getUrlManager()->createUrl('termino-referencia/recursos-reprogramacion');
?>
<script>
    $('body').on('change', '#objetivos', function (e) {
        e.preventDefault();
        var ComponenteID=$(this).val();
        var toke = '<?= Yii::$app->request->getCsrfToken() ?>';
        $.ajax({
            url: '<?= $actividades ?>',
            type: 'POST',
            async: false,
            data: {ComponenteID:ComponenteID,_csrf: toke},
            success: function(data){
               $( "#actividades" ).html( data );
            }
        });
    });
    
    var _recursos = new Object();
    
    $('body').on('change', '#actividades', function (e) {
        e.preventDefault();
        var ActividadID=$(this).val();
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $('#recursos').html('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>');
        $.ajax({
            url: '<?= $recursos ?>',
            type: 'POST',
            async: false,
            data: {ActividadID:ActividadID,_csrf: toke},
            success: function(data){
                var html = '';
                var jx = JSON.parse(data);
                //_recursos=JSON.parse(JSON.stringify($.getJSON(data)));
                console.log(JSON.parse(data).length);
                for (var rubro = 0; rubro < jx.length; rubro++) {
                    html += '<div class="panel panel-gray">';
                        html += '<div class="panel-heading">';
                            html += '<h4 class="title-obj panel-title">';
                            html += jx[rubro].Nombre ;
                            html += '</h4>';
                        html += '</div>';
                        
                        html += '<div class="panel-body">';
                            html += '<div class="table-responsive">';
                                html += '<table class="table table-bordered table-condensed table-act">';
                                    html += '<tbody>';
                                            html += '<tr class="tr-header">';
                                                html += '<td>';
                                                    html += 'Codigo Matriz';
                                                html += '</td>';
                                                html += '<td>';
                                                    html += 'Recurso';
                                                html += '</td>';
                                                html += '<td>';
                                                    html += 'Unidad de Medida';
                                                html += '</td>';
                                                html += '<td>';
                                                    html += 'Meta Física';
                                                html += '</td>';
                                                html += '<td>';
                                                    html += 'Costo Unitario';
                                                html += '</td>';
                                                html += '<td>';
                                                    html += '<span class="fa fa-cog"></span>';
                                                html += '</td>';
                                            html += '</tr>';
                                        for (var recur = 0; recur < jx[rubro].Recursos.length; recur++)
                                        {
                                            var recurs=jx[rubro].Recursos[recur];
                                            html += '<tr>';
                                                html += '<td>';
                                                    html += recurs.Codificacion;
                                                html += '</td>';
                                                html += '<td>';
                                                    html += recurs.Recurso;
                                                html += '</td>';
                                                html += '<td>';
                                                    html += recurs.UnidadMedida;
                                                html += '</td>';
                                                html += '<td>';
                                                    html += recurs.MetaFisica;
                                                html += '</td>';
                                                html += '<td>';
                                                    html += recurs.CostoUnitario;
                                                html += '</td>';
                                                html += '<td align="center">';
                                                   // html += '<a href="<?= \Yii::$app->request->BaseUrl ?>/re-programacion-recurso/re-programar?RecursoID='+recurs.ID+'" class="fa fa-calendar"></a>';
                                                    html += '<a href="<?= \Yii::$app->request->BaseUrl ?>/re-programacion-recurso/listado-recursos?RecursoID='+recurs.ID+'" class="fa fa-calendar"></a>';
                                                html += '</td>';
                                            html += '</tr>';
                                        }
                                    html += '</tbody>';
                                html += '</table>';
                            html += '</div>';
                        html += '</div>';
                    html += '</div>';
                }
                $('#recursos').html(html);
                
            }
        });
    });
</script>