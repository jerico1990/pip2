<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div id="page-content" style="min-height: 754px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Enviar aprobación del POA</h1>
        </div>
        <div class="container">
            <?php $form = ActiveForm::begin(['options'=> ['id'=> 'frmSendProject']]); ?>
            <div class="col-sm-9">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <p>Estimado Usuario,</p>
                        <p>Antes de <b>"Enviar"</b> su Proyecto se le recomienda revisar su POA.</p>
                        <!--
                        <p>
                            En el Sistema en Línea de Fondos Concursables del PNIA <b>(SLFC)</b>, Usted podrá encontrar una opción de <b>"Descargar proyecto"</b>,
                            el cual, con un "clic" permitirá grabar en su computadora en formato PDF y proceder a su <b>impresión</b>.
                            Asimismo, al costado de la opción "Descargar proyecto" encontrará una opción de <b>"Descargar adjuntos"</b>,
                            el cual le permitirá descargar en formato <b>Zip</b> todos los documentos adjuntos a su Proyecto para verificar su contenido.
                        </p>
                        <p>Una vez, revisado el documento completo y adjuntos, usted tendrá la oportunidad de realizar los ajustes que requiera su Proyecto, ingresando nuevamente a los formularios del SLFC.</p>
                        <p>Finalmente, cuando se encuentre listo, presionar la opción "Enviar". Una vez realizado esta acción, recibirá una notificación del Sistema en Línea de Fondos Concursables del PNIA en su Cuenta de Usuario, confirmando que su documento ingresó a la Lista de Postulación" y adjuntando los documentos enviados en su versión final (con fecha y hora de envío). Realizada esta acción, en adelante usted no podrá volver acceder a su Proyecto.</p>
                        <br>
                            -->
                        <p><b>Administración del Sistema</b></p>
                        <br>
                        <hr>
                        <div class="">
                            <!--<a class="btn btn-primary" href="<?= \Yii::$app->request->BaseUrl ?>/reporte/marco-logico?investigadorID=<?= $investigador->ID ?>"><i class="fa fa-download"></i>&nbsp;Descargar Marco Lógico</a>-->
                            <!--<a class="btn btn-primary" href="/SLFC/Postulante/DescargarAnexos"><i class="fa fa-paperclip"></i>&nbsp;Descargar adjuntos</a>-->
                            <a class="btn btn-primary" id="btn-send-proyect" href="javascript:void(0);"><i class="fa fa-paper-plane"></i>&nbsp;Enviar Poa</a>
                            <input type="hidden" name="InformacionGeneral[EnviarProyecto]" value="1">
                        </div>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#btn-send-proyect').click(function (e) {
            var $btn = $(this);
            var toke = '<?=Yii::$app->request->getCsrfToken()?>';
            $.ajax({
                url:'<?= Yii::$app->getUrlManager()->createUrl('informacion-general/validar-informacion') ?>',
                async:false,
                method : 'POST',
                data:{CodigoProyecto:"<?= $informacionGeneral->Codigo ?>", _csrf: toke},
                beforeSend:function()
                {
                    
                },
                success:function(data)
                {
                    var jx = JSON.parse(data);
                    if (jx.Success == true) {
                        bootbox.confirm({
                            title: '<span style="text-align:center">PNIA - CONFIRMACIÓN</span>',
                            message: 'Está seguro de enviar su proyecto? Recuerde que usted ya no podrá volver a realizar ninguna modificación.',
                            buttons: {
                                'cancel': {
                                    label: 'NO ENVIAR PROYECTO'
                                },
                                'confirm': {
                                    label: '<i class="fa fa-paper-plane"></i>&nbsp;ENVIAR PROYECTO'
                                }
                            },
                            callback: function (confirmed) {
                                if (confirmed) {
                                    $('#frmSendProject').submit();
                                } 
                            }
                        });
                    }
                    else
                    {
                        bootbox.alert(jx.Error);
                    }
                },
                error:function(){
                    
                }
            });
            
            
        });
    });
    
    
</script>