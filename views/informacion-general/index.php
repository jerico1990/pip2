<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<div id="page-content" style="min-height: 1342px;">
<div id="wrap">
    <div id="page-heading">
        <h1>Información General</h1>
    </div>
    <div class="container">
        <style>
            .div-label-left b {
                margin-bottom: 10px;
                font-size: 18px;
            }
            .count-message {
                color: #2460AA;
                margin-top: 5px;
            }
        </style>
        <?php // $form = ActiveForm::begin(); ?>
        <?php $form = ActiveForm::begin(
            [
                'action'            => '',
                'options'           => [
                    'enctype'       =>'multipart/form-data',
                    'id'            => 'frmInformacionGeneral'
                ]
            ]
        ); ?>
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div id="container-resp"></div>
                    <div class="form-horizontal">
                        <div class="form-group div-label-left row-border">
                            <div class="col-sm-6">
                                <b>Proyecto </b>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Código de proyecto: <span class="f_req">*</span></label>
                            <div class="col-sm-2">
                                <input class="form-control" value="<?= $usuario->Username ?>" disabled>
                            </div>
                            <label class="col-sm-3 control-label">Investigador responsable: <span class="f_req">*</span></label>
                            <div class="col-sm-5">
                                <input class="form-control" value="<?= $datosInvestigador->Nombre." ".$datosInvestigador->ApellidoPaterno." ".$datosInvestigador->ApellidoMaterno ?>" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Título del proyecto: <span class="f_req">*</span></label>
                            <div class="col-sm-10">
                                <textarea class="form-control" cols="20" id="TituloProyecto" maxlength="1500" name="InformacionGeneral[TituloProyecto]" rows="2" required <?= $deshabilitar?> ><?= $model->TituloProyecto?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Resumen del proyecto: <span class="f_req">*</span></label>
                            <div class="col-sm-10">
                                <textarea class="form-control" cols="20" id="Resumen" maxlength="1500000" name="InformacionGeneral[Resumen]" rows="2" required <?= $deshabilitar?>><?= $model->Resumen?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Fin del proyecto: <span class="f_req">*</span></label>
                            <div class="col-sm-10">
                                <textarea class="form-control" cols="20" id="Fin" maxlength="1500" name="InformacionGeneral[Fin]" rows="2" required <?= $deshabilitar?>><?= $model->Fin?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Propósito del proyecto: <span class="f_req">*</span></label>
                            <div class="col-sm-10">
                                <textarea class="form-control" cols="20" id="Proposito" maxlength="1500" name="InformacionGeneral[Proposito]" rows="2" required <?= $deshabilitar?>><?= $model->Proposito?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Antecedente proyecto: <span class="f_req">*</span></label>
                            <div class="col-sm-10">
                                <textarea class="form-control" cols="20" id="Proposito" maxlength="1500" name="InformacionGeneral[Antecedente]" rows="2" required <?= $deshabilitar?>><?= $model->Antecedente?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Tipo de investigación: <span class="f_req">*</span></label>
                            <div class="col-sm-3">
                                <select class="form-control" id="TipoInvestigacionID" name="InformacionGeneral[TipoInvestigacionID]" required <?= $deshabilitar?>>
                                   <option value>Seleccionar</option>
                                   <?php foreach($tiposInvestigaciones as $tipoInvestigacion){?>
                                        <option value="<?= $tipoInvestigacion->ID ?>" <?= ($tipoInvestigacion->ID==$model->TipoInvestigacionID)?'selected':''; ?> <?= $deshabilitar?>><?= $tipoInvestigacion->Nombre ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Fecha de inicio: <span class="f_req">*</span></label>
                            <div class="col-sm-2">
                                <!-- <input class="form-control" data-val="true" data-val-date="The field" data-val-required="El  es obligatorio." id="FechaInicioProyectox" name="FechaInicioProyectox" required="" type="text" value="04/01/2016" data-parsley-required="true"> -->
                                <input type="text" class="form-control fechas" data-val="true" id="FechaInicioProyecto" name="InformacionGeneral[FechaInicioProyecto]" value="<?= Yii::$app->tools->dateFormat($model->FechaInicioProyecto,'d/m/Y') ?>" <?= $deshabilitar?> data-parsley-required="true">

                            </div>
                            <label class="col-sm-2 control-label">Fecha de término: <span class="f_req">*</span></label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control fechas" id="FechaFinProyecto" name="InformacionGeneral[FechaFinProyecto]" value="<?= Yii::$app->tools->dateFormat($model->FechaFinProyecto,'d/m/Y') ?>" data-parsley-required="true" <?= $deshabilitar?>>
                            </div>
                            
                            <label class="col-sm-2 control-label">Duración:</label>
                            <div class="col-sm-2">
                                <input id="Meses" type="text" onKeyPress="return soloNumeros(event)" class="form-control" name="InformacionGeneral[Meses]" value="<?= $model->Meses ?>" required <?= $deshabilitar?>>
                            </div>
                        </div>
                        <div class="form-group div-label-left row-border">
                            <div class="col-sm-6">
                                <b>Dirección y Unidad Ejecutora </b>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Dirección en Linea: <span class="f_req">*</span></label>
                            <div class="col-sm-2">
                                <select class="form-control" id="DireccionLineaID" name="InformacionGeneral[DireccionLineaID]" required <?= $deshabilitar?>>
                                    <option value>Seleccionar</option>
                                    <?php foreach($direccionesLineas as $direccionLinea){?>
                                        <option value="<?= $direccionLinea->ID ?>" <?= ($direccionLinea->ID==$model->DireccionLineaID)?'selected':''; ?>><?= $direccionLinea->Nombre ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            
                            <label class="col-sm-2 control-label">Unidad Ejecutora: <span class="f_req">*</span></label>
                            <div class="col-sm-2">
                                <select class="form-control" id="UnidadEjecutoraID" name="InformacionGeneral[UnidadEjecutoraID]" onchange="unidadOperativa($(this).val())" required <?= $deshabilitar?>>
                                    <option value>Seleccionar</option>
                                    <?php foreach($unidadesEjecutoras as $unidadEjecutora){?>
                                        <option value="<?= $unidadEjecutora->ID ?>" <?= ($unidadEjecutora->ID==$model->UnidadEjecutoraID)?'selected':''; ?>><?= $unidadEjecutora->Nombre ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            
                            <label class="col-sm-2 control-label">Unidad Operativa: <span class="f_req">*</span></label>
                            <div class="col-sm-2">
                                <select class="form-control" id="UnidadOperativaID" name="InformacionGeneral[UnidadOperativaID]" required <?= $deshabilitar?>>
                                    <option value>Seleccionar</option>
                                    <?php foreach($unidadesOperativas as $unidadOperativa){?>
                                        <option value="<?= $unidadOperativa->ID ?>" <?= ($unidadOperativa->ID==$model->UnidadOperativaID)?'selected':''; ?>><?= $unidadOperativa->Nombre ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group div-label-left row-border">
                            <div class="col-sm-6">
                                <b>Programa</b>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Programa: <span class="f_req">*</span></label>
                            <div class="col-sm-2">
                                <select class="form-control" id="ProgramaID" name="InformacionGeneral[ProgramaID]" onchange="cultivoCrianza($(this).val())" required <?= $deshabilitar?>>
                                    <option value>Seleccionar</option>
                                    <?php foreach($programas as $programa){ ?>
                                        <option value="<?= $programa->ID ?>" <?= ($programa->ID==$model->ProgramaID)?'selected':''; ?>><?= $programa->Nombre ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <label class="col-sm-2 control-label">Cultivo y Crianza: <span class="f_req">*</span></label>
                            <div class="col-sm-2">
                                <select class="form-control" id="CultivoCrianzaID" name="InformacionGeneral[CultivoCrianzaID]" onchange="especie($(this).val())" required <?= $deshabilitar?>>
                                    <option value>Seleccionar</option>
                                    <?php foreach($cultivosCrianzas as $cultivoCrianza){ ?>
                                        <option value="<?= $cultivoCrianza->ID ?>" <?= ($cultivoCrianza->ID==$model->CultivoCrianzaID)?'selected':''; ?>><?= $cultivoCrianza->Nombre ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            
                            <label class="col-sm-2 control-label">Especie: <span class="f_req">*</span></label>
                            <div class="col-sm-2">
                                <select class="form-control" id="EspecieID" name="InformacionGeneral[EspecieID]" required <?= $deshabilitar?>>
                                    <option value>Seleccionar</option>
                                    <?php foreach($especies as $especie){ ?>
                                        <option value="<?= $especie->ID ?>" <?= ($especie->ID==$model->EspecieID)?'selected':''; ?>><?= $especie->Nombre ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Programa Transversal: <span class="f_req">*</span></label>
                            <div class="col-sm-6">
                                <select class="form-control" id="ProgramaTransversalID" name="InformacionGeneral[ProgramaTransversalID]" <?= $deshabilitar?>>
                                    <option value>Seleccionar</option>
                                    <?php foreach($programasTransversales as $programaTransversal){ ?>
                                        <option value="<?= $programaTransversal->ID ?>" <?= ($programaTransversal->ID==$model->ProgramaTransversalID)?'selected':''; ?>><?= $programaTransversal->Nombre ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group div-label-left row-border">
                            <div class="col-sm-6">
                                <b>Localización del proyecto </b>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="LocalizacionProyectoIdRegion" class="col-sm-2 control-label">Región: <span class="f_req">*</span></label>
                            <div class="col-sm-2">
                                <select class="form-control" id="DepartamentoID" name="InformacionGeneral[DepartamentoID]" onchange="provincia($(this).val())" <?= $deshabilitar?>>
                                    <option value>Seleccionar</option>
                                    <?php foreach($departamentos as $departamento){ ?>
                                        <option value="<?= $departamento->ID ?>" <?= ($departamento->ID==$model->DepartamentoID)?'selected':''; ?>><?= $departamento->Nombre ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            
                            <label for="LocalizacionProyectoIdProvincia" class="col-sm-2 control-label">Provincia <span class="f_req">*</span></label>
                            <div class="col-sm-2">
                                <select class="form-control" id="ProvinciaID" name="InformacionGeneral[ProvinciaID]" onchange="distrito($(this).val())" <?= $deshabilitar?>>
                                    <option value>Seleccionar</option>
                                    <?php foreach($provincias as $provincia){ ?>
                                        <option value="<?= $provincia->ID ?>" <?= ($provincia->ID==$model->ProvinciaID)?'selected':''; ?>><?= $provincia->Nombre ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            
                            <label for="LocalizacionProyectoIdDistrito" class="col-sm-2 control-label">Distrito: <span class="f_req">*</span></label>
                            <div class="col-sm-2">
                                <select class="form-control" id="DistritoID" name="InformacionGeneral[DistritoID]" <?= $deshabilitar?>>
                                    <option value>Seleccionar</option>
                                    <?php foreach($distritos as $distrito){ ?>
                                        <option value="<?= $distrito->ID ?>" <?= ($distrito->ID==$model->DistritoID)?'selected':''; ?>><?= $distrito->Nombre ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Latitud: <span class="f_req">*</span></label>
                            <div class="col-sm-2">
                                <input class="form-control" id="Latitud" name="InformacionGeneral[Latitud]" value="<?= $model->Latitud ?>" <?= $deshabilitar?>>
                            </div>
                            
                            <label  class="col-sm-2 control-label">Longitud <span class="f_req">*</span></label>
                            <div class="col-sm-2">
                                <input class="form-control" id="Longitud" name="InformacionGeneral[Longitud]" value="<?= $model->Longitud ?>" <?= $deshabilitar?>>
                            </div>
                        </div>
                        <div class="form-group div-label-left row-border">
                            <div class="col-sm-3">
                                <b>Ámbito de intervención </b>
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-sm-2">
                                
                            </div>
                            <div class="col-sm-6">
                                <?php if($deshabilitar==''){ ?>
                                <button type="button" id="btn-add-ambitointervencion" class="btn btn-midnightblue-alt"><i class="fa fa-plus"></i>&nbsp;Agregar</button>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"></label>
                            
                            <div class="col-sm-8">
                                <div id="div-table-ambitointervencion">    
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped table-condensed table-hover tbl-act">
                                            <thead>
                                                <tr class="tr-header">
                                                    <?php if($deshabilitar == ''){ ?>
                                                        <th>Acciones</th>
                                                    <?php } ?>
                                                    <th>Región</th>
                                                    <th>Provincia</th>
                                                    <th>Distrito</th>
                                                    <th>Latitud</th>
                                                    <th>Longitud</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach($ambitosIntervenciones as $ambitoIntervencion){?>
                                                    <tr>
                                                        <?php if($deshabilitar == ''){ ?>
                                                        <td class="td-acciones">
                                                            <a href="#" class="btn-remove-ambitoIntervencion" data-toggle="tooltip" title="" role-id="<?= $ambitoIntervencion->ID ?>" data-original-title="Eliminar"><i class="fa fa-remove fa-lg"></i></a>
                                                        </td>
                                                        <?php } ?>
                                                        <td class="text-center"><?= $ambitoIntervencion->Departamento ?></td>
                                                        <td class="text-center"><?= $ambitoIntervencion->Provincia ?></td>
                                                        <td class="text-center"><?= $ambitoIntervencion->Distrito ?></td>
                                                        <td class="text-center"><?= $ambitoIntervencion->Latitud ?></td>
                                                        <td class="text-center"><?= $ambitoIntervencion->Longitud ?></td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if(!empty($EntidadParticipante)){ ?>
                            <div class="form-group div-label-left row-border">
                                <div class="col-sm-3">
                                    <b>Colaboradores </b>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-8">
                                    <div id="div-table-ambitointervencion">    
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-condensed table-striped table-hover tbl-act" id="colaboradores">
                                                <thead>
                                                    <tr class="tr-header">
                                                        <th>Institución</th>
                                                        <th>Aporte Monetario</th>
                                                        <th>Aporte No Monetario</th>
                                                        <th>Total</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach($EntidadParticipante as $Entidades){?>
                                                        <tr class="contador">
                                                            <td><?= $Entidades->RazonSocial ?></td>
                                                            <td class="amone input-number"><?= number_format($Entidades->AporteMonetario,2) ?></td>
                                                            <td class="anomone input-number"><?= number_format($Entidades->AporteNoMonetario,2) ?></td>
                                                            <td class="totalAp input-number"><?= number_format( ($Entidades->AporteMonetario + $Entidades->AporteNoMonetario),2 ) ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                    <tr>
                                                        <td>TOTAL</td>
                                                        <td id="apMonetario" class="input-number"></td>
                                                        <td id="apNoMonetario" class="input-number"></td>
                                                        <td id="totalApRs" class="input-number"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="form-group div-label-left row-border">
                            <div class="col-sm-3">
                                <b>Documentación </b>
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="col-sm-2">
                                
                            </div>
                            <div class="col-sm-6">
                                <?php if($deshabilitar==''){ ?>
                                <button type="button" id="btn-add-documentos" class="btn btn-midnightblue-alt"><i class="fa fa-plus"></i>&nbsp;Agregar</button>
                                <?php } ?>
                            </div>
                        </div>
                            
                            <div class="form-group">
                                <label class="col-sm-3 control-label"></label>
                                <div class="col-sm-6">
                                    <?php if(!empty($documentosInformacionesGenerales)): ?>
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-condensed table-striped table-hover tbl-act">
                                                <thead>
                                                    <tr class="tr-header">
                                                        <?php if($deshabilitar == ''){ ?>
                                                            <th width="100">Acciones</th>
                                                        <?php } ?>
                                                        <th>Documento</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i=1; foreach($documentosInformacionesGenerales as $documentoInformacionGeneral){?>
                                                        <tr >
                                                            <?php if($deshabilitar == ''){ ?>
                                                                <td class="text-center">
                                                                    <a href="#" class="btn-remove-adjunto" data-toggle="tooltip" title="" role-id="<?= $documentoInformacionGeneral->ID ?>" data-original-title="Eliminar"><i class="fa fa-remove fa-lg"></i></a>
                                                                </td>
                                                            <?php } ?>
                                                            <td class="text-center">
                                                                <a target='_blank' class='btn btn-info' href='../web/informaciongeneral/<?= $documentoInformacionGeneral->Documento ?>'><span class='fa fa-cloud-download'></span> <?= $documentoInformacionGeneral->Documento ?></a>
                                                            </td>
                                                            
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>

                                    <?php else: ?>
                                        <div class="alert alert-info">
                                            <i class="fa fa-info"></i>&nbsp;Subir los sustentos del proyecto
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                </div>
                <?php if($deshabilitar==''){ ?>
                    <div class="panel-footer text-right">
                        <div class="form-group">
                            <!-- <div class="col-sm-3"></div> -->
                            <div class="col-sm-6" style="padding-right: 0px">
                                <button type="submit" id="grabar" class="btn btn-primary">Guardar</button>
                                <a class="btn btn-inverse-alt" href="<?= \Yii::$app->request->BaseUrl ?>/informacion-general/descargar?investigador=<?= $investigador->ID ?>" target="_blank">
                                    <i class="fa fa-download"></i>&nbsp;Descargar
                                </a>
                            </div>
                        </div>
                    </div>
                <?php }else { ?>
                    <div class="panel-footer text-right">
                        <div class="form-group">
                            <div class="col-sm-6" style="padding-right: 0px">
                                <a class="btn btn-inverse-alt" href="<?= \Yii::$app->request->BaseUrl ?>/informacion-general/descargar?investigador=<?= $investigador->ID ?>" target="_blank">
                                    <i class="fa fa-download"></i>&nbsp;Descargar
                                </a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                    
                
                <?php 
                    /*if(\Yii::$app->user->identity->SituacionProyecto==1){ ?>
                    <div class="panel-footer text-right">
                        <div class="form-group">
                            <div class="col-sm-6"></div>
                            <div class="col-sm-6" style="padding-right: 0px"><button class="btn btn-primary btn-reprogramacion-informacion-general"><i class="fa fa-check"></i>&nbsp;Modificar información general</button></div>
                        </div>
                    </div>
                    <?php }*/ 
                ?>

            </div>
        <?php ActiveForm::end(); ?>
        <?php if($deshabilitar=='' && \Yii::$app->user->identity->SituacionProyecto==0){ ?>
            <div class="panel panel-danger">
                <div class="panel-body">
                    <strong>IMPORTANTE: </strong>Una vez completado el registro del POA, enviar su proyecto a control de calidad para su revisión<!--Al modificar la cantidad de meses de duración de su proyecto, se volverá a generar el plan operativo.-->
                </div>
            </div>
        <?php } ?>
        
        
        
        <div id="modal-ambitointervencion" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Ámbito de Intervención</h4>
                    </div>
                    <div class="modal-body">
                    </div>
                </div>
            </div>
        </div>
        
        <div id="modal-documentos" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Adjuntar Documentos</h4>
                    </div>
                    <div class="modal-body">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!--
<div id="modal-loading" class="modal fade modal-loading" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content-no-shadow modal-content">
            <div class="text-center"><i class="fa fa-spinner fa-pulse fa-fw" style="color:#16a085;font-size: 100px;"></i></div>
        </div>
    </div>
</div>
-->
<?php
    $unidadOperativa=Yii::$app->getUrlManager()->createUrl('informacion-general/unidad-operativa');
    $cultivoCrianza=Yii::$app->getUrlManager()->createUrl('informacion-general/cultivo-crianza');
    $especie=Yii::$app->getUrlManager()->createUrl('informacion-general/especie');
    $provincia=Yii::$app->getUrlManager()->createUrl('informacion-general/provincia');
    $distrito=Yii::$app->getUrlManager()->createUrl('informacion-general/distrito');
?>


<script>
    var situacionProyecto=<?= \Yii::$app->user->identity->SituacionProyecto ?>;
    
    if (situacionProyecto!=0) {
        $("input,textarea,select").prop("disabled", true);
        $("#btn-add-ambitointervencion,#w0 a,#btn-add-documentos").hide();
        $("#w0 #grabar").hide();
    }
    
    var total_monetario = 0;
    var total_no_monetario = 0;
    var total_total = 0.00;


    $('#colaboradores tr.contador').each(function(){
        suma = $(this).find('td.amone')[0].textContent;
        total_monetario = parseFloat(getNum(suma)) + parseFloat(total_monetario) ;
        NoSuma = $(this).find('td.anomone')[0].textContent;
        total_no_monetario = parseFloat(getNum(NoSuma)) + parseFloat(total_no_monetario) ;
        TotSuma = $(this).find('td.totalAp')[0].textContent;
        total_total = parseFloat(getNum(TotSuma)) + parseFloat(total_total) ;
    });

    $('#apMonetario').text(number_format(total_monetario,2));
    $('#apNoMonetario').text( number_format(total_no_monetario,2) );
    $('#totalApRs').text( number_format(total_total,2) );

    function getNum(val) {
       if (val == '0.00' || val == '') {
            return 0;
       }
       var val2 = val.replace(',','');
       return val2;
    }

    function number_format(amount, decimals) {
        amount += '';
        amount = parseFloat(amount.replace(/[^0-9\.]/g, ''));
        decimals = decimals || 0;
        if (isNaN(amount) || amount === 0) 
            return parseFloat(0).toFixed(decimals);
        amount = '' + amount.toFixed(decimals);
        var amount_parts = amount.split('.'),
            regexp = /(\d+)(\d{3})/;
        while (regexp.test(amount_parts[0]))
            amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');
        return amount_parts.join('.');
    }

    function unidadOperativa(valor) {
        var unidadEjecutoraID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $unidadOperativa ?>',
            type: 'POST',
            async: false,
            data: {unidadEjecutoraID:unidadEjecutoraID,_csrf: toke},
            success: function(data){
               $( "#UnidadOperativaID" ).html( data );
            }
        });
    }
    
    function cultivoCrianza(valor) {
        var programaID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $cultivoCrianza ?>',
            type: 'POST',
            async: false,
            data: {programaID:programaID,_csrf: toke},
            success: function(data){
               $( "#CultivoCrianzaID" ).html( data );
            }
        });
    }
    function especie(valor) {
        var cultivoCrianzaID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $especie ?>',
            type: 'POST',
            async: false,
            data: {cultivoCrianzaID:cultivoCrianzaID,_csrf: toke},
            success: function(data){
               $( "#EspecieID" ).html( data );
            }
        });
    }
    
    function provincia(valor) {
        var departamentoID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $provincia ?>',
            type: 'POST',
            async: false,
            data: {departamentoID:departamentoID,_csrf: toke},
            success: function(data){
               $( "#ProvinciaID" ).html( data );
            }
        });
    }
    
    function distrito(valor) {
        var provinciaID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $distrito ?>',
            type: 'POST',
            async: false,
            data: {provinciaID:provinciaID,_csrf: toke},
            success: function(data){
               $( "#DistritoID" ).html( data );
            }
        });
    }
    $('#btn-add-ambitointervencion').click(function (e) {
        e.preventDefault();
        $('#modal-ambitointervencion .modal-body').load("<?= Yii::$app->getUrlManager()->createUrl('informacion-general/ambito-intervencion-create?informaciongeneralid='.$model->ID) ?>");
        $('#modal-ambitointervencion').modal('show');
    });
    
    $('#btn-add-documentos').click(function (e) {
        e.preventDefault();
        $('#modal-documentos .modal-body').load("<?= Yii::$app->getUrlManager()->createUrl('informacion-general/adjuntar?informaciongeneralid='.$model->ID) ?>");
        $('#modal-documentos').modal('show');
    });
    
    $(document).on('click','.btn-remove-ambitoIntervencion',function (e) {
        e.preventDefault();
        var $btn = $(this)
        bootboxConfirmEliminar($btn,
            'Está seguro de eliminar el ámbito de intervención?',
            function () {
                var ambitoID = $btn.attr('role-id');
                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                $.post('<?= \Yii::$app->request->BaseUrl ?>/informacion-general/eliminar-ambito/', { ambitoID: ambitoID, _csrf: toke }, function (data) {
                    var jx = JSON.parse(data);
                    if (jx.Success == true) {
                        //cargarColaboradoras();
                        location.reload();
                    }
                });
            });
    });
    
    $(document).on('click','.btn-remove-adjunto',function (e) {
        e.preventDefault();
        var $btn = $(this)
        bootboxConfirmEliminar($btn,
            'Está seguro de eliminar el adjunto?',
            function () {
                var adjuntoID = $btn.attr('role-id');
                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                $.post('<?= \Yii::$app->request->BaseUrl ?>/informacion-general/eliminar-adjunto/', { adjuntoID: adjuntoID, _csrf: toke }, function (data) {
                    var jx = JSON.parse(data);
                    if (jx.Success == true) {
                        location.reload();
                    }
                });
            });
    });


    // $('#btn-save-ambitointervencion').click(function (e) {
    $(document).on('click','#btn-save-ambitointervencion',function (e) {
        e.preventDefault();
        var isValid = formParsleyAmbito.validate();
        if (isValid) {
            sendForm($('#frmAmbito'), $(this), function (eve) {
                console.log(eve);
                $('#modal-ambitointervencion').modal('hide');
                location.reload();
                //$('#btn-add-replegal-colaboradora').removeClass('hidden');
                //$('#btn-add-replegal-colaboradora').attr('data-id',eve.EntidadID);
                // $('#btn-save-colaboradora').attr('disabled','disabled');
                // alert(eve.EntidadID);
                // cargarRepresentantesLegalesColaboradora(eve.EntidadID);
            });
        }
    });


    $(document).ready(function () {
        $('.fechas').datepicker({
            language: 'es',
            format: 'dd/mm/yyyy',
            autoclose: true,
            //startDate: fechainicio_date,
            //endDate: fechafin_date_max
        }).on('changeDate', function (selected) {
            // calcularDuracionProyecto();
        });

        var $form = $('#frmInformacionGeneral');
        var formParsleyInfoGeneral = $form.parsley(defaultParsleyForm());

        $('#grabar').click(function(e){
            e.preventDefault();
            _pageLoadingStart();
            var isValid = formParsleyInfoGeneral.validate();
            if (isValid) {

                sendFormNoMessage($('#frmInformacionGeneral'), $(this), function (dato) {
                    // _pageLoadingEnd();
                    var $cont = $('#container-resp');
                    console.log(dato);
                    var htm =  '<div class="alert alert-success">';
                        htm += '<i class="fa fa-check"></i>&nbsp;La información fue guardada correctamente';
                        htm += '</div>';
                    console.log(htm);
                    $cont.html(htm);
                    $cont.removeClass('hidden');
                    
                    // $('#frmInformacionGeneral').submit();
                });
            }
        });

    });
</script>