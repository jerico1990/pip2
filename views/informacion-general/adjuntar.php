<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<?php $form = ActiveForm::begin(
    [
        'action'            => \Yii::$app->request->BaseUrl.'/informacion-general/adjuntar?informaciongeneralid='.$model->ID.'',
        'options'           => [
            'enctype'       =>'multipart/form-data',
            'id'            => 'frmAdjuntar',
        ]
    ]
); ?>
    <div class="form-horizontal">
        <div class="form-group">
            <label class="col-sm-3 control-label">Documento <span class="f_req">*</span></label>
            <div class="col-sm-9">

                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="input-group">
                        <div class="form-control uneditable-input" data-trigger="fileinput">
                            <i class="fa fa-file fileinput-exists"></i>&nbsp;
                            <span class="fileinput-filename">
                            Seleccione archivo...
                            </span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                            <span class="fileinput-new">Seleccionar</span>
                            <span class="fileinput-exists">Cambiar</span>
                            <input type="file" onchange="return Imagen(this);" id="file-cv-administrativo" name="InformacionGeneral[Archivos]" required>
                            <input class="form-control" type="hidden" name="InformacionGeneral[InvestigadorID]" value="<?= $model->InvestigadorID ?>">
                        </span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Eliminar</a>
                    </div>
                </div>


                
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button id="btn-save-adjuntar" type="submit" class="btn btn-primary">Guardar</button>
        
    </div>
<?php ActiveForm::end(); ?>

<script>
    var formParsleyAdjuntar = $('#frmAdjuntar').parsley(defaultParsleyForm());
    /*$('#btn-save-adjuntar').click(function (e) {
        e.preventDefault();
        var isValid = formParsleyAdjuntar.validate();
        if (isValid) {
            sendForm($('#frmAdjuntar'), $(this), function (eve) {
                console.log(eve);
            });
        }
    });*/
</script>
