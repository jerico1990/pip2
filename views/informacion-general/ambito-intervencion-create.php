<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<?php $form = ActiveForm::begin(['options'=> ['id'=> 'frmAmbito']]); ?>
    <div class="form-horizontal">
        <div class="form-group">
            <label class="col-sm-3 control-label">Región <span class="f_req">*</span></label>
            <div class="col-sm-6">
                <select class="form-control" id="ADepartamentoID" name="AmbitoIntervencion[DepartamentoID]" onchange="provincia($(this).val())" required>
                    <option value>Seleccionar</option>
                    <?php foreach($departamentos as $departamento){ ?>
                        <option value="<?= $departamento->ID ?>" ><?= $departamento->Nombre ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Provincia <span class="f_req">*</span></label>
            <div class="col-sm-6">
                <select class="form-control" id="AProvinciaID" name="AmbitoIntervencion[ProvinciaID]" onchange="distrito($(this).val())" required>
                    <option value>Seleccionar</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="LocalizacionProyectoIdDistrito" class="col-sm-3 control-label">Distrito <span class="f_req">*</span></label>
            <div class="col-sm-6">
                <select class="form-control" id="ADistritoID" name="AmbitoIntervencion[DistritoID]" required>
                    <option value>Seleccionar</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Latitud: <span class="f_req">*</span></label>
            <div class="col-sm-6">
                <input class="form-control" id="Latitud" name="AmbitoIntervencion[Latitud]" >
            </div>
        </div>
        <div class="form-group">
            <label  class="col-sm-3 control-label">Longitud <span class="f_req">*</span></label>
            <div class="col-sm-6">
                <input class="form-control" id="Longitud" name="AmbitoIntervencion[Longitud]" >
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button id="btn-save-ambitointervencion" type="submit" class="btn btn-primary">Guardar</button>
        
    </div>
<?php ActiveForm::end(); ?>

<?php
    $provincia=Yii::$app->getUrlManager()->createUrl('informacion-general/provincia');
    $distrito=Yii::$app->getUrlManager()->createUrl('informacion-general/distrito');
?>
<script>
    function provincia(valor) {
        var departamentoID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $provincia ?>',
            type: 'POST',
            async: false,
            data: {departamentoID:departamentoID,_csrf: toke},
            success: function(data){
               $( "#AProvinciaID" ).html( data );
            }
        });
    };
    
    function distrito(valor) {
        var provinciaID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $distrito ?>',
            type: 'POST',
            async: false,
            data: {provinciaID:provinciaID,_csrf: toke},
            success: function(data){
               $( "#ADistritoID" ).html( data );
            }
        });
    };
    var formParsleyAmbito = $('#frmAmbito').parsley(defaultParsleyForm());
    
</script>
