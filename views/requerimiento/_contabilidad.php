
<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay_progress.min.js"></script>
<script src=" //cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>


<div id="page-content" style="min-height: 1342px;">
    
    <div id="wrap">
        <div id="page-heading">
            <h1>Compromisos Presupuestales</h1>
        </div>
        
           
        <div class="container">
            <!-- <div class="form-group">
                <button class="btn btn-crear-requerimiento" >Generar requerimiento</button>
            </div> -->
            <div class="table-responsive">
                <table id="proyectos" class="display table table-bordered table-striped table-condensed table-hover" cellspacing="0" >
                    <thead>
                        <tr>
                            <th>N°</th>
                            <th>Proyecto</th>
                            <th>Tipo</th>
                            <th>Fecha Registro</th>
                            <th>Situación</th>
                            <?php if(\Yii::$app->user->identity->rol == 2 || \Yii::$app->user->identity->rol == 11){ ?>
                                <th>Gastos</th>
                                <th></th>
                            <?php } ?>


                            <?php if(\Yii::$app->user->identity->rol == 13 || \Yii::$app->user->identity->rol == 14){ ?>
                                <th>Especialista</th>
                                <th></th>
                            <?php } ?>
                            
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<?php 
if(\Yii::$app->user->identity->rol == 2 || \Yii::$app->user->identity->rol == 11){ 
    $requerimiento = Yii::$app->getUrlManager()->createUrl('requerimiento/actualizar-requerimiento');
}

if(\Yii::$app->user->identity->rol == 13 || \Yii::$app->user->identity->rol == 14){ 
    $requerimiento = Yii::$app->getUrlManager()->createUrl('requerimiento/actualizar-requerimiento-contabilidad');
} 
$compro=Yii::$app->getUrlManager()->createUrl('compromiso-presupuestal/generar-compromiso-anual');
?>

<div class="modal fade" id="modal-ctrl-requerimiento" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Requerimiento</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<script>
    function js_buscar()
    {
       
    }
    var tblresultjs;
        var configDTjs={
            "order": [[ 2, "desc" ]],
                "language": {
                    "sProcessing":    "Procesando...",
                    "sLengthMenu":    "Mostrar _MENU_ registros",
                    "sZeroRecords":   "No se encontraron resultados",
                    "sEmptyTable":    "Ningún dato disponible en esta lista",
                    "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":   "",
                    "sSearch":        "Buscar:",
                    "sUrl":           "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":    "Último",
                        "sNext":    "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
        };
    $(document).ready(function() {
        $.ajax({
            url:'listado-contabilidad',
            async:false,
            data:{},
            beforeSend:function()
            {
                $('.panel-default').LoadingOverlay("show");
            },
            success:function(result)
            {
                    //$tblresultjs.destroy();//
                    $("#proyectos tbody").html(result);
                    $('#proyectos').DataTable(configDTjs);
                    //tblresultjs=$('#example').DataTable(configDTjs);
                    $('.panel-default').LoadingOverlay("hide",true);
            },
            error:function(){
                $('.panel-default').LoadingOverlay("hide",true);
                alert('Error al realizar el proceso de busqueda.');
            }
        });
        
    } );
    
    // $('body').on('click', '.btn-crear-requerimiento', function (e) {
    //     e.preventDefault();
    //     var CodigoProyecto = $(this).attr('data-codigo-proyecto');
    //     $('#modal-ctrl-requerimiento .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/requerimiento/crear?CodigoProyecto='+CodigoProyecto);
    //     $('#modal-ctrl-requerimiento').modal('show');
    // });
    
    $('body').on('click', '#btn-guardar-requerimientos', function (e) {
        
        e.preventDefault();
        // alert('s');
        
        var $btn = $(this);
        $btn.attr('disabled', 'disabled');
        var isValid = formParsleyRequerimientos.validate();
        if (isValid) {
            sendFormNoMessage($('#frmRequerimientos'), $(this), function (msg) {
                
                $("#modal-ctrl-requerimiento").modal('hide');
                _pageLoadingEnd();
        
            });
        } else {
        }
    });
    
    $('body').on('click', '.btn-remove', function (e) {
        e.preventDefault();
        var $this = $(this);
        bootboxConfirmEliminar($this,
            'Está seguro de eliminar el requerimiento?',
            function () {
                var id = $this.attr('data-id');
                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                $.get('<?= \Yii::$app->request->BaseUrl ?>/requerimiento/eliminar', { id: id ,_csrf: toke }, function (data) {
                    var jx = JSON.parse(data);
                    if (jx.Success == true) {
                        
                    }
                });
            });
    });
    
    $('body').on('click', '.btn-edit-requerimiento', function (e) {
        e.preventDefault();
        var id=$(this).attr('data-id');
        $('#modal-ctrl-requerimiento .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/requerimiento/actualizar?ID='+id);
        $('#modal-ctrl-requerimiento').modal('show');
    });

   
        $(document).on('click','.verifica',function(){
            var x = $(this).parent().siblings().children('select').val();
            var a = $(this).parent().siblings().children('select').attr('data-id');
            
            var toke = '<?=Yii::$app->request->getCsrfToken()?>';
            if(x == ''){
                bootbox.alert('Seleccione un item.');
            }else{
                bootBoxConfirm('Esta seguro que desea registrar la información',function(){
                    $.ajax({
                        url: '<?= $requerimiento ?>',
                        type: 'POST',
                        async: false,
                        data: {gastos:x,_csrf: toke,ID:a},
                        success: function (data) {
                            location.reload();
                        },
                        error: function () {
                            _pageLoadingEnd();
                        }
                    });
                });
            }
        });
        
        $(document).on('click','.generar-compromiso',function(){
            var ID=$(this).attr('data-id');
            var CodigoProyecto=$(this).attr('data-codigo-proyecto');
            
            var toke = '<?=Yii::$app->request->getCsrfToken()?>';
            bootBoxConfirm('Esta seguro de Generar el Compromiso Anual',function(){
                $.ajax({
                    url: '<?= $compro ?>',
                    type: 'POST',
                    async: false,
                    data: {ID:ID,CodigoProyecto:CodigoProyecto,_csrf: toke},
                    success: function (data) {
                        //location.reload();
                    },
                    error: function () {
                        _pageLoadingEnd();
                    }
                });
            });
            
        });
        



</script>