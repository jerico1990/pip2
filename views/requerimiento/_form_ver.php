<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'requerimiento/actualizar?ID='.$ID,
        'options'           => [
            'enctype'       =>'multipart/form-data',
            'id'            => 'frmRequerimientos',
            
        ]
    ]
); ?>
    <div class="modal-body">
        <div class="form-horizontal">
            <div class="tab-container">
                <ul class="nav nav-tabs">
                    <li id="li-poaf" class="active"><a href="#tab-informacion-general" data-toggle="tab"><i class="fa fa-home"></i>&nbsp;Información General</a></li>
                    <?php if($requerimiento->TipoRequerimiento!=5){?>
                    <li id="li-poaf"><a href="#tab-detalle" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Detalle</a></li>
                    <?php }?>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-informacion-general">
                        <?php if($requerimientoAnt){ ?>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Observación:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <textarea class="form-control" disabled><?= $requerimientoAnt->Observacion ?></textarea>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tipo requerimiento:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <select type="text" class="form-control" name="Requerimiento[TipoRequerimiento]" disabled>
                                    <option value="">Seleccionar</option>
                                    <?php foreach ($tipoGasto as $value): ?>
                                        <option value="<?php echo $value->ID ?>" <?= ($requerimiento->TipoRequerimiento==$value->ID)?'selected':''; ?>><?php echo $value->Nombre ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Referencia:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="Requerimiento[Referencia]" value="<?= $requerimiento->Referencia ?>" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Asunto:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="Requerimiento[Asunto]" value="<?= $requerimiento->Asunto ?>" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Fecha de solicitud:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <input type="date" class="form-control" name="Requerimiento[FechaSolicitud]" value="<?= date('Y-m-d',strtotime($requerimiento->FechaSolicitud)) ?>" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">CUT:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="Requerimiento[CUT]" value="<?= $requerimiento->CUT ?>" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Descripción:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <textarea class="form-control" id="descripcion" name="Requerimiento[Descripcion]"  disabled><?= $requerimiento->Descripcion ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-detalle">
                        <div class="form-group">
                            <div class="col-sm-12" style="max-width: 1200px !important;overflow-x: auto ">
                                <table class="table borderless table-hover" id="detalle">
                                    <thead>
                                        <tr>
                                            <th width="250">Objetivo</th>
                                            <th width="250">Actividad</th>
                                            <th width="250">Recurso</th>
                                            <th width="100">Código matriz</th>
                                            <th width="250">Descripción</th>
                                            <th width="50">Cantidad</th>
                                            <th width="50">P. unitario</th>
                                            <th width="50">Total</th>
                                            <th width="22">&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=1; //print_r($detallesRequerimientos); ?>
                                        <?php foreach($detallesRequerimientos as $detalleRequerimiento){ ?>
                                        <tr>
                                            <td width="250">
                                                <!--
                                                <input class="form-control" type="hidden" value="<?= $detalleRequerimiento->ID ?>" name="Requerimiento[DetallesIDs][]">
                                                <input class="form-control" type="hidden" value="<?= $detalleRequerimiento->Objetivo ?>" name="Requerimiento[DetallesObjetivos][]">
                                                <input class="form-control" type="hidden" value="<?= $detalleRequerimiento->Actividad ?>" name="Requerimiento[DetallesActividades][]">
                                                <input class="form-control" type="hidden" value="<?= $detalleRequerimiento->Recurso ?>" name="Requerimiento[DetallesRecurs][]">
                                                <input class="form-control" type="hidden" value="<?= $detalleRequerimiento->CodigoMatriz ?>" name="Requerimiento[DetallesCodigos][]">
                                                <input class="form-control" type="hidden" value="<?= $detalleRequerimiento->AreSubCategoriaID ?>" name="Requerimiento[DetallesRecursos][]">-->
                                                <input class="form-control" style="width:150px"  type="text" value="<?= $detalleRequerimiento->Objetivo ?>" disabled>
                                            </td>
                                            <td width="250">
                                                <input class="form-control" style="width:150px" type="text" value="<?= $detalleRequerimiento->Actividad ?>" disabled>
                                            </td>
                                            <td>
                                                <input class="form-control" style="width:150px" type="text" value="<?= $detalleRequerimiento->Recurso ?>" disabled>
                                            </td>
                                            <td>
                                                <input class="form-control" style="width:150px" type="text" value="<?= $detalleRequerimiento->CodigoMatriz ?>" disabled>
                                            </td>
                                            <td>
                                                <input class="form-control" style="width:150px" type="text"  value="<?= $detalleRequerimiento->Descripcion ?>" disabled>
                                            </td>
                                            <td>
                                                <input class="form-control" style="width:150px" type="text" value="<?= $detalleRequerimiento->Cantidad ?>" onkeyup="Cantidades($(this).val(),detalle)" id="cantidades_detalle" disabled>
                                            </td>
                                            <td>
                                                <input class="form-control" style="width:150px" type="text" value="<?= $detalleRequerimiento->PrecioUnitario ?>" onkeyup="PreciosUnitarios($(this).val(),detalle)" id="preciosunitarios_detalle"disabled>
                                            </td>
                                            <td>
                                                <input class="form-control" style="width:150px"type="text" id="preciostotales_detalle" value="<?= $detalleRequerimiento->Total ?>" disabled>
                                            </td>
                                            <td>
                                                <span class="eliminar icon-minus-sign" >
                                                </span>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                        <?php } ?>
                                        <input type="hidden" value="<?= $i ?>" id="contador">
                                        <tr id='detalle_1'></tr>
                                    </tbody>
                                </table>
                            </div>
                            
                            <?php if (!empty($resultados) ): ?>
                                <div class="col-sm-12">
                                    <table class="table borderless table-hover" id="detallesx">
                                        <thead>
                                            <tr>
                                                <th >Código</th>
                                                <th >Código matriz</th>
                                                <th >Recurso</th>
                                                <th >Específica</th>
                                                <th width="50">Unidad medida</th>
                                                <th width="50">Costo unitario</th>
                                                <th width="50">Meta fisica</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($resultados as $det) { ?>
                                            <tr>
                                                <td><?php echo $det['CorrelaCompr'].'.'.$det['CorrelaActv'].'.'.$det['CorrelaRecurso']; ?></td>
                                                <td><?php echo $det['RubroElegibleID'].'.'.$det['Codificacion']; ?></td>
                                                <td><?php echo $det['Nombre']; ?></td>
                                                <td><?php echo $det['Especifica']; ?></td>
                                                <td><?php echo $det['UnidadMedida']; ?></td>
                                                <td><?php echo $det['CostoUnitario']; ?></td>
                                                <td><?php echo $det['MetaFisica']; ?></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            <?php endif ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
    </div>
<?php ActiveForm::end(); ?>


