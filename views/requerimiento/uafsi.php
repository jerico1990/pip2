<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay_progress.min.js"></script>
<script src=" //cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>


<div id="page-content" style="min-height: 1342px;">
    
    <div id="wrap">
        <div id="page-heading">
            <h1>Requerimiento</h1>
        </div>
        <div class="container">
            <div class="table-responsive">
                <table id="proyectos" class="display table table-bordered table-striped table-condensed table-hover" cellspacing="0" >
                    <thead>
                        <tr>
                            <th>EE.AA.</th>
                            <th>Código proyecto</th>
                            <th>Tipo requerimiento</th>
                            <th>Requerimiento</th>
                            <th>Referencia</th>
                            <th>Fecha solicitud</th>
                            <th>Situación</th>
                            <th>Ubicación</th>
                            <th>Documento adjunto</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            
        </div>
    </div>
</div>


    <div class="modal fade" id="modal-ctrl-requerimiento" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Requerimiento (<?php echo date('d-m-Y') ?>)</h4>
                </div>
                <div class="modal-body-main">
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="modal fade" id="modal-ctrl-upload" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Adjuntar</h4>
                </div>
                <div class="modal-body-main">
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="modal-ctrl-poa" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">POA</h4>
                </div>
                <div class="modal-body-main">
                </div>
            </div>
        </div>
    </div>
    
<?php 
$requerimiento = Yii::$app->getUrlManager()->createUrl('requerimiento/actualizar-requerimiento-listado');
$eliminarDetalle = Yii::$app->getUrlManager()->createUrl('requerimiento/eliminar-detalle');
?>
    
<script>
    
    function js_buscar()
    {
       
    }
    var tblresultjs;
    var configDTjs={
        "order": [[ 1, "desc" ]],
            "language": {
                "sProcessing":    "Procesando...",
                "sLengthMenu":    "Mostrar _MENU_ registros",
                "sZeroRecords":   "No se encontraron resultados",
                "sEmptyTable":    "Ningún dato disponible en esta lista",
                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":   "",
                "sSearch":        "Buscar:",
                "sUrl":           "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":    "Último",
                    "sNext":    "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
    };
    
    $(document).ready(function() {
        $.ajax({
            url:'<?= \Yii::$app->request->BaseUrl ?>/requerimiento/lista-requerimientos-uafsi',
            async:false,
            data:{},
            beforeSend:function()
            {
                $('.panel-default').LoadingOverlay("show");
            },
            success:function(result)
            {
                    //$tblresultjs.destroy();//
                    $("#proyectos tbody").html(result);
                    $('#proyectos').DataTable(configDTjs);
                    //tblresultjs=$('#example').DataTable(configDTjs);
                    $('.panel-default').LoadingOverlay("hide",true);
            },
            error:function(){
                $('.panel-default').LoadingOverlay("hide",true);
                alert('Error al realizar el proceso de busqueda.');
            }
        });
    } );
    
    $('body').on('click', '.btn-requerimiento-ver', function (e) {
        e.preventDefault();
        var id=$(this).attr('data-id');
        $('#modal-ctrl-requerimiento .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/requerimiento/ver?ID='+id);
        $('#modal-ctrl-requerimiento').modal('show');
    });
    
    $('body').on('click', '.btn-requerimiento-ver-ubicacion', function (e) {
        e.preventDefault();
        var id=$(this).attr('data-id');
        $('#modal-ctrl-requerimiento .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/requerimiento/ver-ubicacion?ID='+id);
        $('#modal-ctrl-requerimiento').modal('show');
    });
    
    
</script>