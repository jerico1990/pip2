<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'requerimiento/actualizar?ID='.$ID,
        'options'           => [
            'enctype'       =>'multipart/form-data',
            'id'            => 'frmRequerimientos',
            
        ]
    ]
); ?>
    <div class="modal-body">
        <div class="form-horizontal">
            <div class="tab-container">
                <ul class="nav nav-tabs">
                    <li id="li-poaf" class="active"><a href="#tab-informacion-general" data-toggle="tab"><i class="fa fa-home"></i>&nbsp;Ubicación - Historial</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-informacion-general">
                        <div class="form-group">
                            <table class="table">
                                <thead>
                                    <tr class="tr-header">
                                        <th width="250">Ubicación</th>
                                        <th width="250">Días</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
    </div>
<?php ActiveForm::end(); ?>


