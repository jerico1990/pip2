<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'requerimiento/actualizar?ID='.$ID,
        'options'           => [
            'enctype'       =>'multipart/form-data',
            'id'            => 'frmRequerimientos',
            
        ]
    ]
); ?>
    <div class="modal-body">
        <div class="form-horizontal">
            <div class="tab-container">
                <ul class="nav nav-tabs">
                    <li id="li-poaf" class="active"><a href="#tab-informacion-general" data-toggle="tab"><i class="fa fa-home"></i>&nbsp;Información General</a></li>
                    <?php if($requerimiento->TipoRequerimiento!=5){?>
                    <li id="li-poaf"><a href="#tab-detalle" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Detalle</a></li>
                    <?php }?>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-informacion-general">
                        <?php if($requerimientoAnt){ ?>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Observación:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <textarea class="form-control" disabled><?= $requerimientoAnt->Observacion ?></textarea>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tipo Requerimiento:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <select type="text" class="form-control" id="tipo-requerimiento" name="Requerimiento[TipoRequerimiento]" required>
                                    <option value="">[SELECCIONE]</option>
                                    <?php foreach ($tipoGasto as $value): ?>
                                        <option value="<?php echo $value->ID ?>" <?= ($requerimiento->TipoRequerimiento==$value->ID)?'selected':''; ?>><?php echo $value->Nombre ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Referencia:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="Requerimiento[Referencia]" value="<?= $requerimiento->Referencia ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Asunto:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="Requerimiento[Asunto]" value="<?= $requerimiento->Asunto ?>" required>
                            </div>
                        </div>
                        <!--
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Fecha de Solicitud:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <input type="date" class="form-control datepicker" name="Requerimiento[FechaSolicitud]" value="<?= date('Y-m-d',strtotime($requerimiento->FechaSolicitud)) ?>" required>
                            </div>
                        </div>
                        -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">CUT:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="Requerimiento[CUT]" value="<?= $requerimiento->CUT ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Descripción:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <textarea class="form-control" id="descripcion" name="Requerimiento[Descripcion]"  required><?= $requerimiento->Descripcion ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Documento:</label>
                            <div class="col-sm-9">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="input-group">
                                        <div class="form-control uneditable-input" data-trigger="fileinput">
                                            <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                            <span class="fileinput-filename">
                                            Seleccione archivo...
                                            </span>
                                        </div>
                                        <span class="input-group-addon btn btn-default btn-file">
                                            <span class="fileinput-new">Seleccionar</span>
                                            <span class="fileinput-exists">Cambiar</span>
                                            <input type="file" onchange="return Imagen(this);" id="file-cv-administrativo" name="Requerimiento[Archivo]">
                                        </span>
                                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Eliminar</a>
                                    </div>
                                </div>
                                <br>
                                <?php if(!empty($requerimiento->Documento)): ?>
                                    <?php if($requerimiento->Documento): ?>
                                        <a href="<?= \Yii::$app->request->BaseUrl ?>/requerimientos/<?php echo $requerimiento->Documento ?>" target="_blank"><i class="fa fa-download"></i>&nbsp;Descargar adjunto</a>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-detalle">
                        <div class="form-group">
                            <div class="col-sm-3">
                                <a class="btn btn-primary" id="agregar-poa">Agregar POA</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12" style="max-width: 1200px !important;overflow-x: auto ">
                                <table class="table borderless table-hover" id="detalle">
                                    <thead>
                                        <tr>
                                            <th width="250">Objetivo</th>
                                            <th width="250">Actividad</th>
                                            <th width="250">Recurso</th>
                                            <th width="100">Código Matriz</th>
                                            <th width="250">Descripción</th>
                                            <th width="50">Cantidad</th>
                                            <th width="50">P. Unitario</th>
                                            <th width="50">Total</th>
                                            <th width="22">&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody id="detalle_poa">
                                        <?php $i=1; ?>
                                        <?php foreach($detallesRequerimientos as $detalleRequerimiento){ ?>
                                        <tr>
                                            <td width="250">
                                                <input class="form-control" style="width:150px"  type="text" value="<?= $detalleRequerimiento->Objetivo ?>" disabled>
                                            </td>
                                            <td width="250">
                                                <input class="form-control" style="width:150px" type="text" value="<?= $detalleRequerimiento->Actividad ?>" disabled>
                                            </td>
                                            <td>
                                                <input class="form-control" style="width:150px" type="text" value="<?= $detalleRequerimiento->Recurso ?>" disabled>
                                            </td>
                                            <td>
                                                <input class="form-control codigo_poa" style="width:150px" type="text" value="<?= $detalleRequerimiento->CodigoMatriz ?>" disabled>
                                            </td>
                                            <td>
                                                <input class="form-control" style="width:150px" type="text"  value="<?= $detalleRequerimiento->Descripcion ?>" disabled>
                                            </td>
                                            <td>
                                                <input class="form-control" style="width:150px" type="text" value="<?= $detalleRequerimiento->Cantidad ?>" onkeyup="Cantidades($(this).val(),detalle)" id="cantidades_detalle" disabled>
                                            </td>
                                            <td>
                                                <input class="form-control" style="width:150px" type="text" value="<?= $detalleRequerimiento->PrecioUnitario ?>" onkeyup="PreciosUnitarios($(this).val(),detalle)" id="preciosunitarios_detalle" disabled>
                                            </td>
                                            <td>
                                                <input class="form-control" style="width:150px" type="text" id="preciostotales_detalle" value="<?= number_format(($detalleRequerimiento->Total), 2, '.', ' ')  ?>" disabled>
                                            </td>
                                            <td>
                                                <a href="javascript:;" class="eliminar"><input type="hidden" value="<?= $detalleRequerimiento->ID ?>"><i class="fa fa-times fa-2x" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                        <?php } ?>
                                        <input type="hidden" value="<?= $i ?>" id="contador">
                                    </tbody>
                                </table>
                            </div>
                            
                            <?php if (!empty($resultados) ): ?>
                                <div class="col-sm-12">
                                    <table class="table borderless" id="detallesx">
                                        <thead>
                                            <tr>
                                                <th >Codigo</th>
                                                <th >Codigo Matriz</th>
                                                <th >Recurso</th>
                                                <th >Especifica</th>
                                                <th width="50">Costo Unitario</th>
                                                <th width="50">Meta Fisica</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($resultados as $det) { ?>
                                            <tr>
                                                <td><?php echo $det['CorrelaCompr'].'.'.$det['CorrelaActv'].'.'.$det['CorrelaRecurso']; ?></td>
                                                <td><?php echo $det['RubroElegibleID'].'.'.$det['Codificacion']; ?></td>
                                                <td><?php echo $det['Nombre']; ?></td>
                                                <td><?php echo $det['Especifica']; ?></td>
                                                <td><?php echo $det['CostoUnitario']; ?></td>
                                                <td><?php echo $det['MetaFisica']; ?></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            <?php endif ?>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary" id="btn-guardar-requerimientos"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>


<script type="text/javascript">
    
    var $form = $('#frmRequerimientos');
    var formParsleyRequerimientos = $form.parsley(defaultParsleyForm());

</script>
