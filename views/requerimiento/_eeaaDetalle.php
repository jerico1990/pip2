<?php
use app\models\Orden;
?>
<div id="page-content" style="min-height: 885px;">
    <div id="wrap">
        <div id="page-heading">
            <h1></h1>
        </div>
        <div class="container">
                    
            <style>
                .panel .panel-body {
                    padding: 10px !important;
                }

                .panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 1px solid #a8a8ab !important;
                }

                .panel-gray .panel-body {
                    border-top: none !important;
                }

                .btn-sm {
                    padding: 5px 10px !important;
                    line-height: 1.5 !important;
                }

                .input-comp-nombre {
                    text-transform: uppercase;
                }

                .tbl-act > tbody > .tr-header > td {
                    text-align: center;
                    font-size: 12px;
                    font-weight: bold;
                    padding: 5px !important;
                    vertical-align: middle !important;
                    /*border: 1px solid #cfcfd0;
                    background-color: #f0f0f1;*/
                    border: 1px solid #c0c0c0;
                    background-color: #e3e3e3;
                }

                /*.tbl-act .tr-item td {
                    padding: 2px !important;
                }*/
                input.form-control, select.form-control {
                    /*height: 28px !important;*/
                }

                .tbl-act .tr-act .input-nombre {
                    min-width: 320px !important;
                }

                .tbl-act .tr-act .input-unidadmedida {
                    min-width: 100px !important;
                }

                .input-number {
                    min-width: 92px !important;
                }

                .btn-add-re {
                    float: right;
                }

                /*.tbl-act > tbody > tr > td:nth-child(2) {
                    min-width: 40px !important;
                    text-align: center !important;
                }*/
                .text-left {
                    text-align: left !important;
                    font-weight: bold;
                }

                .tr-comp input, .tr-proy input {
                    font-weight: bold;
                }

                .th-proy {
                    min-width: 630px !important;
                }
            </style>

            <div class="tab-container">
                <ul class="nav nav-tabs">
                    <li id="li-poaf" class="active">
                        <a href="#tab-informacion-general" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Detalle Requerimiento</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-informacion-general">
                        <div id="container-poaf">
                            <?php 
                            switch ($cabecera->Situacion) {
                                case '1':
                                    $situacion = 'PENDIENTE';
                                break;
                                case '2':
                                    $situacion = 'PENDIENTE';
                                break;
                                case '3':
                                    $situacion = 'ACTIVO';
                                break;
                                case '0':
                                    $situacion = 'PENDIENTE DE APROBAR';
                                break;
                            }
                            ?>
                            <h3>Proyecto <?php echo $cabecera->Codigo ?> / Documento Nº <?php echo $cabecera->Correlativo ?> - <strong><?php echo $situacion ?></strong></h3>


                            <?php if(\Yii::$app->user->identity->rol == 11 || \Yii::$app->user->identity->rol == 2): ?>
                                <?php if($cabecera->GastoElegible != ''): ?>
                                    <h4><strong><?php echo strtoupper($gastos) ?></strong></h4>
                                <?php endif; ?>
                            <?php endif; ?>

                            <?php if(\Yii::$app->user->identity->rol == 15): ?>
                                <?php if($cabecera->Situacion != 3): ?>
                                    <form class="form-horizontal">
                                        <div class="col-md-12">
                                            <p>Aprobar el requerimiento</p>
                                            <a href="javascript:;" class="btn btn-primary verifica-aprobar" data-id="<?php echo $cabecera->ID ?>" id="">APROBAR</a>
                                            <br>
                                        </div>
                                    </form>
                                <?php endif ?>
                                
                            <?php endif; ?>


                            <br>
                            <table class="table table-hover">
                                <thead>
                                    <th>Tipo</th>
                                    <th>Correlativo</th>
                                    <th>Fecha Orden</th>
                                    <th>Tipo de Proceso</th>
                                    <th>Monto Total</th>
                                    <th>Formato</th>
                                </thead>
                                <tbody>
                                    <?php foreach ($resultados as $res): ?>
                                        <tr>
                                            <td><?php echo($res['Nombre']) ?></td>
                                            <td><?php echo($res['Correlativo']) ?></td>
                                            <td><?php echo($res['FechaOrden']) ?></td>
                                            <td><?php echo($res["TipoProceso"] == 1 ? 'Comparacion de Precios' : 'Evaluacion de CVs') ?></td>
                                            <td><?php echo number_format(($res["Monto"]), 2, '.', ' ') ?></td>
                                            <?php if($res["TipoGasto"]==3){ ?>
                                            <td><a href="<?= \Yii::$app->request->BaseUrl ?>/ordenCompra/<?= $res["Documento"] ?>" target="_blank">Descargar</a></td>
                                            <?php } ?>
                                            
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                        <a href="<?= \Yii::$app->request->BaseUrl ?>/requerimiento/uafsi" class="btn btn-info">VOLVER</a>
                    </div>

                </div>
            </div>


        </div>
    </div>
</div>

<div id="modal-loading" class="modal fade modal-loading in" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-hidden="false" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content-no-shadow modal-content">
            <div class="text-center"><i class="fa fa-spinner fa-pulse fa-fw" style="color:#16a085;font-size: 100px;"></i></div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-ctrl-observacion" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Observacion</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<?php 
if(\Yii::$app->user->identity->rol == 2 || \Yii::$app->user->identity->rol == 11){ 
    $requerimiento = Yii::$app->getUrlManager()->createUrl('requerimiento/actualizar-requerimiento');
}

if(\Yii::$app->user->identity->rol == 15){ 
    $requerimiento = Yii::$app->getUrlManager()->createUrl('requerimiento/actualizar-requerimiento-pac');
} 

?>


<script>
    $(document).ready(function(){
       $('.fa-search').popover(); 
    });
    $('[data-rel=tooltip]').tooltip();
      


    $('#verifica').click(function(){
        var x = $('#gastos').val();
        var a = $('#gastos').attr('data-id');
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        if(x == ''){
            bootbox.alert('Seleccione un item.');
        }else{
            bootBoxConfirm('Esta seguro que desea registrar la información',function(){
                $.ajax({
                    url: '<?= $requerimiento ?>',
                    type: 'POST',
                    async: false,
                    data: {gastos:x,_csrf: toke,ID:a},
                    success: function (data) {
                        location.reload();
                    },
                    error: function () {
                        _pageLoadingEnd();
                    }
                });
            });
        }
    });



    $(document).on('click','.verifica-aprobar',function(){
        var ID = $(this).attr('data-id');
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        bootBoxConfirm('Esta seguro que desea aprobar requerimiento',function(){
            $.ajax({
                url: '<?= $requerimiento ?>',
                type: 'POST',
                async: false,
                data: {_csrf: toke,ID:ID},
                success: function (data) {
                    location.reload();
                },
                error: function () {
                    _pageLoadingEnd();
                }
            });
        });
    });
    $('body').on('click', '.observar', function () {
        var ID = $(this).attr('data-id');
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $('#modal-ctrl-observacion .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/requerimiento/eeaa-observacion?ID='+ID);
        $('#modal-ctrl-observacion').modal('show');
    });
</script>