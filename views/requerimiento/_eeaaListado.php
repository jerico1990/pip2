
<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay_progress.min.js"></script>
<script src=" //cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>


<div id="page-content" style="min-height: 1342px;">
    
    <div id="wrap">
        <div id="page-heading">
            <h1>Requerimiento</h1>
        </div>
        
           
        <div class="container">
            <!-- <div class="form-group">
                <button class="btn btn-crear-requerimiento" >Generar requerimiento</button>
            </div> -->
            <div class="table-responsive">
                <table id="proyectos" class="display table table-bordered table-striped table-condensed table-hover" cellspacing="0" >
                    <thead>
                        <tr>
                            <th style="display: none"></th>
                            <th>Código de Proyecto</th>
                            <th>Tipo Requerimiento</th>
                            <th>Requerimiento</th>
                            <th>Referencia</th>
                            <th>Fecha Solicitud</th>
                            <th>Documento adjunto</th>
                            <th>Situación</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<?php 
if(\Yii::$app->user->identity->rol == 5){ 
    $requerimiento = Yii::$app->getUrlManager()->createUrl('requerimiento/actualizar-requerimiento-listado-eeaa');
} 

?>

<div class="modal fade" id="modal-ctrl-observacion" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Requerimiento</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-ctrl-requerimiento" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Requerimiento</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>
<script>
    function js_buscar()
    {
       
    }
    var tblresultjs;
        var configDTjs={
            "order": [[ 1, "desc" ]],
                "language": {
                    "sProcessing":    "Procesando...",
                    "sLengthMenu":    "Mostrar _MENU_ registros",
                    "sZeroRecords":   "No se encontraron resultados",
                    "sEmptyTable":    "Ningún dato disponible en esta lista",
                    "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":   "",
                    "sSearch":        "Buscar:",
                    "sUrl":           "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":    "Último",
                        "sNext":    "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
        };
    $(document).ready(function() {
        $.ajax({
            url:'listado-eeaa',
            async:false,
            data:{},
            beforeSend:function()
            {
                $('.panel-default').LoadingOverlay("show");
            },
            success:function(result)
            {
                    //$tblresultjs.destroy();//
                    $("#proyectos tbody").html(result);
                    $('#proyectos').DataTable(configDTjs);
                    //tblresultjs=$('#example').DataTable(configDTjs);
                    $('.panel-default').LoadingOverlay("hide",true);
            },
            error:function(){
                $('.panel-default').LoadingOverlay("hide",true);
                alert('Error al realizar el proceso de busqueda.');
            }
        });
        
    } );
   

        

    $(document).on('click','.verifica-aprobar',function(){
        var a = $(this).attr('data-id');
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        bootBoxConfirm('¿Esta seguro que desea aprobar requerimiento?',function(){
            $.ajax({
                url: '<?= $requerimiento ?>',
                type: 'POST',
                async: false,
                data: {_csrf: toke,ID:a},
                success: function (data) {
                    location.reload();
                },
                error: function () {
                    _pageLoadingEnd();
                }
            });
        });
    });
    
    $('body').on('click', '.btn-requerimiento-ver', function (e) {
        e.preventDefault();
        var id=$(this).attr('data-id');
        $('#modal-ctrl-requerimiento .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/requerimiento/ver?ID='+id);
        $('#modal-ctrl-requerimiento').modal('show');
        
    });
    
    $('body').on('click', '.observar', function () {
        
        var ID = $(this).attr('data-id');
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $('#modal-ctrl-observacion .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/requerimiento/eeaa-observacion?ID='+ID);
        $('#modal-ctrl-observacion').modal('show');
    });
    
    $('body').on('click', '.btn-remove', function (e) {
        e.preventDefault();
        var $this = $(this);
        bootboxConfirmEliminar($this,
        'Está seguro de eliminar el requerimiento?',
        function () {
            var id = $this.attr('data-id');
            var toke = '<?=Yii::$app->request->getCsrfToken()?>';
            $.get('<?= \Yii::$app->request->BaseUrl ?>/requerimiento/eliminar', { id: id ,_csrf: toke }, function (data) {
                var jx = JSON.parse(data);
                if (jx.Success == true) {
                    location.reload();
                }
            });
        });
    });
</script>