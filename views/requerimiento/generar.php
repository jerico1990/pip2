<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Orden;
use app\models\Viatico;
use app\models\DetalleRequerimiento;
use app\models\CajaChica;
use app\models\Encargo;
use app\models\CompromisoPresupuestal;
use app\models\DetalleCajaChica;

?>
<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay_progress.min.js"></script>
<script src=" //cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>


<link rel="stylesheet" href="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/jquery_ui/jquery-ui.css">

<script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/jquery_ui/jquery-ui.js"></script>

<link href="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/form-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">


<!-- <link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css" /> -->
<link rel="stylesheet" media="all" type="text/css" href="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/timepicker/jquery-ui-timepicker-addon.css" />

<!-- <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.11.0/jquery-ui.min.js"></script> -->
<script type="text/javascript" src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/timepicker/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/timepicker/i18n/jquery-ui-timepicker-es.js"></script>



<div id="page-content" style="min-height: 754px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Generar </h1>
        </div>
        
        <div class="container">
            
            <!-- OBJETIVOS -->
            <style>
                .panel .panel-body {
                    padding: 10px !important;
                }

                .panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 1px solid #a8a8ab !important;
                }

                .panel-gray .panel-body {
                    border-top: none !important;
                }

                .btn-sm {
                    padding: 5px 10px !important;
                    line-height: 1.5 !important;
                }

                .input-comp-nombre {
                    text-transform: uppercase;
                }

                .tbl-act > tbody > .tr-header > td {
                    text-align: center;
                    font-size: 12px;
                    font-weight: bold;
                    padding: 5px !important;
                    vertical-align: middle !important;
                    /*border: 1px solid #cfcfd0;
                    background-color: #f0f0f1;*/
                    border: 1px solid #c0c0c0;
                    background-color: #e3e3e3;
                }

                /*.tbl-act .tr-item td {
                    padding: 2px !important;
                }*/
                input.form-control, select.form-control {
                    /*height: 28px !important;*/
                }

                .tbl-act .tr-act .input-nombre {
                    min-width: 320px !important;
                }

                .tbl-act .tr-act .input-unidadmedida {
                    min-width: 100px !important;
                }

                .input-number {
                    min-width: 92px !important;
                }

                .btn-add-re {
                    float: right;
                }

                .text-left {
                    text-align: left !important;
                    font-weight: bold;
                }

                .tr-comp input, .tr-proy input {
                    font-weight: bold;
                }

                .th-proy {
                    min-width: 630px !important;
                }
            </style>
            
            <div class="panel panel-primary">
                <a href="<?= \Yii::$app->request->BaseUrl ?>/requerimiento" class="btn btn-info">VOLVER</a><br><br>
                <div id="container-resp"> <?= Yii::$app->session->getFlash('error'); ?></div>
                <div class="panel-body">
                    <div class="row">
                        <div class="container">
                            <?php $form = ActiveForm::begin(
                                [
                                    'action'            => 'crear-orden-compra?CodigoProyecto=',
                                    'options'           => [
                                        'enctype'       =>'multipart/form-data',
                                        'id'            => 'DetalleRequerimiento'
                                    ]
                                ]
                            ); ?>

    
                            <?php if( empty(Yii::$app->session->getFlash('Message')) ){ ?>
                                <div class="alert alert-dismissable alert-danger">
                                    <strong>Errores:</strong><ul>
                                    <li><?php echo Yii::$app->session->getFlash('Message'); ?></li>
                                </ul></div>
                            <?php } ?>


                            <div class="form-horizontal">
                                <input type="hidden" name="Orden[RequerimientoID]" value="">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tipo de requerimiento:<span class="f_req">*</span></label>
                                    <div class="col-sm-4">
                                        <select class="form-control" disabled>
                                            <?php foreach ($tipoGasto as $value): ?>
                                                <option value="<?php echo $value->ID ?>" <?= ($requerimiento->TipoRequerimiento==$value->ID)?'selected':''; ?>><?php echo $value->Nombre ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                    <label class="col-sm-2 control-label">Requerimiento:<span class="f_req">*</span></label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" value="Requerimiento N° <?= str_pad($requerimiento->Correlativo, 3, "0", STR_PAD_LEFT)  ."-".$requerimiento->Annio ?>" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Referencia:<span class="f_req">*</span></label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control"  value="<?= $requerimiento->Referencia ?>" disabled>
                                    </div>
                                    <label class="col-sm-2 control-label">Fecha de solicitud:<span class="f_req">*</span></label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control"  value="<?= date('d-m-Y',strtotime($requerimiento->FechaSolicitud)) ?>" disabled>
                                    </div>
                                </div>
                                <hr>
                                <?php $boton=0;$b=0;?>
                                <div class="container">
                                    <?php if($requerimiento->TipoRequerimiento == 1){ ?>
                                        <div class="form-group">
                                            <table class="table tbl-act">
                                                <tbody>
                                                    <tr class="tr-header">
                                                        <td>Código matriz</td>
                                                        <td>Recurso</td>
                                                        <td>Precio unitario (S/.)</td>
                                                        <td>Cantidad</td>
                                                        <td>Total (S/.)</td>
                                                        <td>Acciones</td>
                                                    </tr>
                                                    <?php $TipoDetalleOrden=0 ;?>
                                                    <?php foreach($detallesrequerimientos as $detallerequerimiento){ ?>
                                                        <?php $ordenServicioFactura=Orden::find()->where('ID=:ID and Estado=1',[':ID'=>$detallerequerimiento->TipoDetalleOrdenID])->one();
                                                            if($TipoDetalleOrden!=$detallerequerimiento->TipoDetalleOrdenID){
                                                                $TipoDetalleOrden=$detallerequerimiento->TipoDetalleOrdenID;
                                                                $a=0;
                                                                $b=0;
                                                            }
                                                            $BanderaContador=0;
                                                            if($ordenServicioFactura){ 
                                                                $detalles=DetalleRequerimiento::find()->where('RequerimientoID=:RequerimientoID and TipoDetalleOrdenID=:TipoDetalleOrdenID',[':RequerimientoID'=>$ordenServicioFactura->RequerimientoID,':TipoDetalleOrdenID'=>$ordenServicioFactura->ID])->all();
                                                                $BanderaContador=count($detalles);
                                                            }
                                                        ?>
                                                        <?php if($ordenServicioFactura){ ?>
                                                        <?php $b++; ?>
                                                        <?php } ?>
                                                        
                                                        <?php $comprometido = CompromisoPresupuestal::find()->where('RequerimientoID=:RequerimientoID',[':RequerimientoID'=>$requerimiento->ID])->one(); ?>
                                                        <tr>
                                                            <td><?= substr($detallerequerimiento->CodigoMatriz,0,1).'.'.$detallerequerimiento->Codificacion ?> </td>
                                                            <td><?= $detallerequerimiento->Recurso .' - '.$detallerequerimiento->Situacion?> </td>
                                                            <td class="input-number"><?= number_format($detallerequerimiento->PrecioUnitario, 2, '.', ' ') ?></td>
                                                            <td class="input-number"><?= $detallerequerimiento->Cantidad ?></td>
                                                            <td><?= number_format(($detallerequerimiento->Cantidad*$detallerequerimiento->PrecioUnitario), 2, '.', ' ') ?></td>
                                                            <?php if($detallerequerimiento->Situacion==2){ ?>
                                                                <td class="text-center"><input type="checkbox" class="obtener_check" onclick="AgregarCodigo(this,<?= $detallerequerimiento->ID ?>)" data-detalle-requerimiento-id="<?= $detallerequerimiento->ID ?>"></td>
                                                                <?php $boton++; ?>
                                                                <!--<td><a data-precio-total="<?= ($detallerequerimiento->Cantidad*$detallerequerimiento->PrecioUnitario) ?>" data-codigo-proyecto="<?= $requerimiento->CodigoProyecto ?>" data-codificacion="<?= $detallerequerimiento->Codificacion ?>" data-detalle-requerimiento-id="<?= $detallerequerimiento->ID ?>" data-requerimiento-id="<?= $requerimiento->ID ?>" data-tipo-req="<?= $requerimiento->TipoRequerimiento ?>" href="#" class="btn btn-primary btn-crear">Crear</a></td>-->
                                                            <?php }elseif($detallerequerimiento->Situacion==3 && $b==$BanderaContador){ ?>
                                                                <td><a target="_blank" href="../orden/plantilla?ID=<?= $detallerequerimiento->TipoDetalleOrdenID ?>" class="btn btn-default"><span class="fa fa-cloud-download"></span> Formato</a> 
                                                                <?php if(!$comprometido): ?>
                                                                    <a href='#' class='btn btn-danger btn-remove' data-tipo-req="<?= $requerimiento->TipoRequerimiento ?>" data-id="<?= $detallerequerimiento->TipoDetalleOrdenID ?>"><i class='fa fa-remove fa-lg'></i></a>
                                                                    </td>
                                                                <?php endif ?>
                                                            <?php } ?>
                                                        </tr>
                                                        <?php if($ordenServicioFactura){ ?>
                                                        <?php $a++; ?>
                                                        <tr>
                                                            <?php
                                                            $detalles=DetalleRequerimiento::find()->where('RequerimientoID=:RequerimientoID and TipoDetalleOrdenID=:TipoDetalleOrdenID',[':RequerimientoID'=>$ordenServicioFactura->RequerimientoID,':TipoDetalleOrdenID'=>$ordenServicioFactura->ID])->all();
                                                            $CadenaCodigoPoa="";
                                                            $i=1;
                                                            foreach($detalles as $detalle){
                                                                if($i>1)
                                                                {
                                                                    $CadenaCodigoPoa='O.'.explode('.', $detalle->Objetivo)[0].' A'.explode('.', $detalle->Actividad)[0].','.$CadenaCodigoPoa;    
                                                                }
                                                                else
                                                                {
                                                                    $CadenaCodigoPoa='O.'.explode('.', $detalle->Objetivo)[0].' A'.explode('.', $detalle->Actividad)[0];    
                                                                }
                                                                $i++;
                                                            } ?>
                                                            <?php if($a==$BanderaContador){ ?>
                                                            <td colspan="6">
                                                                <table class="table tbl-act">
                                                                    <tbody>
                                                                        <tr class="tr-header">
                                                                            <td rowspan="2">Detalle Orden</td>
                                                                            <td>N° orden</td>
                                                                            <td>Fecha registro</td>
                                                                            <td>N° certificación</td>
                                                                            <td>Código POA</td>
                                                                            <td>Documento</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><?= str_pad($ordenServicioFactura->Correlativo, 3, "0", STR_PAD_LEFT)."-".$ordenServicioFactura->Annio ?></td>
                                                                            <td><?= date('d-m-Y',strtotime($ordenServicioFactura->FechaRegistro)) ?></td>
                                                                            <td><?= $ordenServicioFactura->Certificacion ?></td>
                                                                            <td><?= $CadenaCodigoPoa ?></td>
                                                                            <?php if(!$ordenServicioFactura->Documento){ ?>
                                                                            <td><a class="btn btn-default btn-adjuntar" data-tipo-req="<?= $requerimiento->TipoRequerimiento ?>" data-id="<?= $ordenServicioFactura->ID ?>" target="_blank" href="#"><span class="fa fa-cloud-upload"></span> Subir documento</a></td>
                                                                            <?php }else{ ?>
                                                                            <td class="text-center">
                                                                            <?php if($requerimiento->TipoRequerimiento==1){ ?>
                                                                            <a target="_blank" href="../ordenServicioFactura/<?= $ordenServicioFactura->Documento ?>" class="btn btn-default "><span class='fa fa-cloud-download'></span> Descargar documento</a> 
                                                                            <?php } ?>
                                                                                <?php if(!$comprometido): ?>
                                                                                    <a href='#' class='btn btn-danger btn-remove-documento' data-tipo-req="<?= $requerimiento->TipoRequerimiento ?>" data-id="<?= $ordenServicioFactura->ID ?>"><i class='fa fa-remove'></i></a>
                                                                                <?php endif ?>
                                                                            </td>
                                                                            <?php } ?>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <?php } ?>
                                                        </tr>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    <?php if($boton!=0){ ?>
                                                    <tr>
                                                        <td colspan="4"></td>
                                                        <td>
                                                            <td class="text-center"><a data-codigo-proyecto="<?= $requerimiento->CodigoProyecto ?>"  data-requerimiento-id="<?= $requerimiento->ID ?>" data-tipo-req="<?= $requerimiento->TipoRequerimiento ?>" href="#" class="btn btn-primary btn-crear">Crear</a></td>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    <?php }elseif($requerimiento->TipoRequerimiento == 2){ ?>
                                        <div class="form-group">
                                            <table class="table tbl-act">
                                                <tbody>
                                                    <tr class="tr-header">
                                                        <td>Código matriz</td>
                                                        <td>Recurso</td>
                                                        <td>Precio unitario (S/.)</td>
                                                        <td>Cantidad</td>
                                                        <td>Total (S/.)</td>
                                                        <td>Acciones</td>
                                                    </tr>
                                                    <?php $TipoDetalleOrden=0 ;?>
                                                    <?php foreach($detallesrequerimientos as $detallerequerimiento){ ?>
                                                        <?php $ordenServicioRh=Orden::find()->where('ID=:ID and Estado=1',[':ID'=>$detallerequerimiento->TipoDetalleOrdenID])->one();
                                                            if($TipoDetalleOrden!=$detallerequerimiento->TipoDetalleOrdenID){
                                                                $TipoDetalleOrden=$detallerequerimiento->TipoDetalleOrdenID;
                                                                $a=0;
                                                                $b=0;
                                                            }
                                                            $BanderaContador=0;
                                                            if($ordenServicioRh){ 
                                                                $detalles=DetalleRequerimiento::find()->where('RequerimientoID=:RequerimientoID and TipoDetalleOrdenID=:TipoDetalleOrdenID',[':RequerimientoID'=>$ordenServicioRh->RequerimientoID,':TipoDetalleOrdenID'=>$ordenServicioRh->ID])->all();
                                                                $BanderaContador=count($detalles);
                                                            }
                                                        ?>
                                                        <?php if($ordenServicioRh){ ?>
                                                        <?php $b++; ?>
                                                        <?php } ?>
                                                        
                                                        <?php $comprometido = CompromisoPresupuestal::find()->where('RequerimientoID=:RequerimientoID',[':RequerimientoID'=>$requerimiento->ID])->one(); ?>
                                                        <tr>
                                                            <td><?= substr($detallerequerimiento->CodigoMatriz,0,1).'.'.$detallerequerimiento->Codificacion ?> </td>
                                                            <td><?= $detallerequerimiento->Recurso .' - '.$detallerequerimiento->Situacion?> </td>
                                                            <td class="input-number"><?= number_format($detallerequerimiento->PrecioUnitario, 2, '.', ' ') ?></td>
                                                            <td class="input-number"><?= $detallerequerimiento->Cantidad ?></td>
                                                            <td><?= number_format(($detallerequerimiento->Cantidad*$detallerequerimiento->PrecioUnitario), 2, '.', ' ') ?></td>
                                                            <?php if($detallerequerimiento->Situacion==2){ ?>
                                                                <td class="text-center"><input type="checkbox" class="obtener_check" onclick="AgregarCodigo(this,<?= $detallerequerimiento->ID ?>)" data-detalle-requerimiento-id="<?= $detallerequerimiento->ID ?>"></td>
                                                                <?php $boton++; ?>
                                                                <!--<td><a data-precio-total="<?= ($detallerequerimiento->Cantidad*$detallerequerimiento->PrecioUnitario) ?>" data-codigo-proyecto="<?= $requerimiento->CodigoProyecto ?>" data-codificacion="<?= $detallerequerimiento->Codificacion ?>" data-detalle-requerimiento-id="<?= $detallerequerimiento->ID ?>" data-requerimiento-id="<?= $requerimiento->ID ?>" data-tipo-req="<?= $requerimiento->TipoRequerimiento ?>" href="#" class="btn btn-primary btn-crear">Crear</a></td>-->
                                                            <?php }elseif($detallerequerimiento->Situacion==3 && $b==$BanderaContador){ ?>
                                                                <td><a target="_blank" href="../orden/plantilla?ID=<?= $detallerequerimiento->TipoDetalleOrdenID ?>" class="btn btn-default"><span class="fa fa-cloud-download"></span> Formato</a> 
                                                                <?php if(!$comprometido): ?>
                                                                    <a href='#' class='btn btn-danger btn-remove' data-tipo-req="<?= $requerimiento->TipoRequerimiento ?>" data-id="<?= $detallerequerimiento->TipoDetalleOrdenID ?>"><i class='fa fa-remove fa-lg'></i></a>
                                                                    </td>
                                                                <?php endif ?>
                                                            <?php } ?>
                                                        </tr>
                                                        <?php if($ordenServicioRh){ ?>
                                                        <?php $a++; ?>
                                                        <tr>
                                                            <?php
                                                            $detalles=DetalleRequerimiento::find()->where('RequerimientoID=:RequerimientoID and TipoDetalleOrdenID=:TipoDetalleOrdenID',[':RequerimientoID'=>$ordenServicioRh->RequerimientoID,':TipoDetalleOrdenID'=>$ordenServicioRh->ID])->all();
                                                            $CadenaCodigoPoa="";
                                                            $i=1;
                                                            // echo '<pre>';
                                                            foreach($detalles as $detalle){
                                                                if($i>1)
                                                                {
                                                                    $CadenaCodigoPoa='O.'.explode('.', $detalle->Objetivo)[0].' A'.explode('.', $detalle->Actividad)[0].','.$CadenaCodigoPoa;    
                                                                }
                                                                else
                                                                {
                                                                    $CadenaCodigoPoa='O.'.explode('.', $detalle->Objetivo)[0].' A'.explode('.', $detalle->Actividad)[0];      
                                                                }
                                                                $i++;
                                                            } ?>
                                                            <?php if($a==$BanderaContador){ ?>
                                                            <td colspan="6">
                                                                <table class="table tbl-act">
                                                                    <tbody>
                                                                        <tr class="tr-header">
                                                                            <td rowspan="2">Detalle Orden</td>
                                                                            <td>N° orden</td>
                                                                            <td>Fecha registro</td>
                                                                            <td>N° certificación</td>
                                                                            <td>Código POA</td>
                                                                            <td>Documento</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><?= str_pad($ordenServicioRh->Correlativo, 3, "0", STR_PAD_LEFT)."-".$ordenServicioRh->Annio ?></td>
                                                                            <td><?= date('d-m-Y',strtotime($ordenServicioRh->FechaRegistro)) ?></td>
                                                                            <td><?= $ordenServicioRh->Certificacion ?></td>
                                                                            <td><?= $CadenaCodigoPoa ?></td>
                                                                            <?php if(!$ordenServicioRh->Documento){ ?>
                                                                            <td><a class="btn btn-default btn-adjuntar" data-tipo-req="<?= $requerimiento->TipoRequerimiento ?>" data-id="<?= $ordenServicioRh->ID ?>" target="_blank" href="#"><span class="fa fa-cloud-upload"></span> Subir documento</a></td>
                                                                            <?php }else{ ?>
                                                                            <td class="text-center">
                                                                            <?php if($requerimiento->TipoRequerimiento==2){ ?>
                                                                            <a target="_blank" href="../ordenServicioRH/<?= $ordenServicioRh->Documento ?>" class="btn btn-default "><span class='fa fa-cloud-download'></span> Descargar documento</a> 
                                                                            <?php } ?>
                                                                                <?php if(!$comprometido): ?>
                                                                                    <a href='#' class='btn btn-danger btn-remove-documento' data-tipo-req="<?= $requerimiento->TipoRequerimiento ?>" data-id="<?= $ordenServicioRh->ID ?>"><i class='fa fa-remove'></i></a>
                                                                                <?php endif ?>
                                                                            </td>
                                                                            <?php } ?>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <?php } ?>
                                                        </tr>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    <?php if($boton!=0){ ?>
                                                    <tr>
                                                        <td colspan="4"></td>
                                                        <td>
                                                            <td class="text-center"><a data-codigo-proyecto="<?= $requerimiento->CodigoProyecto ?>"  data-requerimiento-id="<?= $requerimiento->ID ?>" data-tipo-req="<?= $requerimiento->TipoRequerimiento ?>" href="#" class="btn btn-primary btn-crear">Crear</a></td>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    <?php }elseif($requerimiento->TipoRequerimiento == 3){ ?>
                                        <div class="form-group">
                                            <table class="table tbl-act">
                                                <tbody>
                                                    <tr class="tr-header">
                                                        <td>Código matriz</td>
                                                        <td>Recurso</td>
                                                        <td>Precio unitario (S/.)</td>
                                                        <td>Cantidad</td>
                                                        <td>Total (S/.)</td>
                                                        <td>Acciones</td>
                                                    </tr>
                                                    <?php $TipoDetalleOrden=0 ;?>
                                                    <?php foreach($detallesrequerimientos as $detallerequerimiento){ ?>
                                                        <?php $ordenCompra=Orden::find()->where('ID=:ID and Estado=1',[':ID'=>$detallerequerimiento->TipoDetalleOrdenID])->one();
                                                            if($TipoDetalleOrden!=$detallerequerimiento->TipoDetalleOrdenID){
                                                                $TipoDetalleOrden=$detallerequerimiento->TipoDetalleOrdenID;
                                                                $a=0;
                                                                $b=0;
                                                            }
                                                            $BanderaContador=0;
                                                            if($ordenCompra){ 
                                                                $detalles=DetalleRequerimiento::find()->where('RequerimientoID=:RequerimientoID and TipoDetalleOrdenID=:TipoDetalleOrdenID',[':RequerimientoID'=>$ordenCompra->RequerimientoID,':TipoDetalleOrdenID'=>$ordenCompra->ID])->all();
                                                                $BanderaContador=count($detalles);
                                                            }
                                                        ?>
                                                        <?php if($ordenCompra){ ?>
                                                        <?php $b++; ?>
                                                        <?php } ?>
                                                        
                                                        <?php $comprometido = CompromisoPresupuestal::find()->where('RequerimientoID=:RequerimientoID',[':RequerimientoID'=>$requerimiento->ID])->one(); ?>
                                                        <tr>
                                                            <td><?= substr($detallerequerimiento->CodigoMatriz,0,1).'.'.$detallerequerimiento->Codificacion ?> </td>
                                                            <td><?= $detallerequerimiento->Recurso .' - '.$detallerequerimiento->Situacion?> </td>
                                                            <td class="input-number"><?= number_format($detallerequerimiento->PrecioUnitario, 2, '.', ' ') ?></td>
                                                            <td class="input-number"><?= $detallerequerimiento->Cantidad ?></td>
                                                            <td><?= number_format(($detallerequerimiento->Cantidad*$detallerequerimiento->PrecioUnitario), 2, '.', ' ') ?></td>
                                                            <?php if($detallerequerimiento->Situacion==2){ ?>
                                                                <td class="text-center"><input type="checkbox" class="obtener_check" onclick="AgregarCodigo(this,<?= $detallerequerimiento->ID ?>)" data-detalle-requerimiento-id="<?= $detallerequerimiento->ID ?>"></td>
                                                                <?php $boton++; ?>
                                                                <!--<td><a data-precio-total="<?= ($detallerequerimiento->Cantidad*$detallerequerimiento->PrecioUnitario) ?>" data-codigo-proyecto="<?= $requerimiento->CodigoProyecto ?>" data-codificacion="<?= $detallerequerimiento->Codificacion ?>" data-detalle-requerimiento-id="<?= $detallerequerimiento->ID ?>" data-requerimiento-id="<?= $requerimiento->ID ?>" data-tipo-req="<?= $requerimiento->TipoRequerimiento ?>" href="#" class="btn btn-primary btn-crear">Crear</a></td>-->
                                                            <?php }elseif($detallerequerimiento->Situacion==3 && $b==$BanderaContador){ ?>
                                                                <td><a target="_blank" href="../orden/plantilla?ID=<?= $detallerequerimiento->TipoDetalleOrdenID ?>" class="btn btn-default"><span class="fa fa-cloud-download"></span> Formato</a> 
                                                                <?php if(!$comprometido): ?>
                                                                    <a href='#' class='btn btn-danger btn-remove' data-tipo-req="<?= $requerimiento->TipoRequerimiento ?>" data-id="<?= $detallerequerimiento->TipoDetalleOrdenID ?>"><i class='fa fa-remove fa-lg'></i></a>
                                                                    </td>
                                                                <?php endif ?>
                                                            <?php } ?>
                                                        </tr>
                                                        <?php if($ordenCompra){ ?>
                                                        <?php $a++; ?>
                                                        <tr>
                                                            <?php
                                                            $detalles=DetalleRequerimiento::find()->where('RequerimientoID=:RequerimientoID and TipoDetalleOrdenID=:TipoDetalleOrdenID',[':RequerimientoID'=>$ordenCompra->RequerimientoID,':TipoDetalleOrdenID'=>$ordenCompra->ID])->all();
                                                            $CadenaCodigoPoa="";
                                                            $i=1;
                                                            foreach($detalles as $detalle){
                                                                if($i>1)
                                                                {
                                                                    $CadenaCodigoPoa='O.'.explode('.', $detalle->Objetivo)[0].' A'.explode('.', $detalle->Actividad)[0].','.$CadenaCodigoPoa;    
                                                                }
                                                                else
                                                                {
                                                                    $CadenaCodigoPoa='O.'.explode('.', $detalle->Objetivo)[0].' A'.explode('.', $detalle->Actividad)[0];    
                                                                }
                                                                $i++;
                                                            } ?>
                                                            <?php if($a==$BanderaContador){ ?>
                                                            <td colspan="6">
                                                                <table class="table tbl-act">
                                                                    <tbody>
                                                                        <tr class="tr-header">
                                                                            <td rowspan="2">Detalle Orden</td>
                                                                            <td>N° orden</td>
                                                                            <td>Fecha registro</td>
                                                                            <td>N° certificación</td>
                                                                            <td>Código POA</td>
                                                                            <td>Documento</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><?= str_pad($ordenCompra->Correlativo, 3, "0", STR_PAD_LEFT)."-".$ordenCompra->Annio ?></td>
                                                                            <td><?= date('d-m-Y',strtotime($ordenCompra->FechaRegistro)) ?></td>
                                                                            <td><?= $ordenCompra->Certificacion ?></td>
                                                                            <td><?= $CadenaCodigoPoa ?></td>
                                                                            <?php if(!$ordenCompra->Documento){ ?>
                                                                            <td><a class="btn btn-default btn-adjuntar" data-tipo-req="<?= $requerimiento->TipoRequerimiento ?>" data-id="<?= $ordenCompra->ID ?>" target="_blank" href="#"><span class="fa fa-cloud-upload"></span> Subir documento</a></td>
                                                                            <?php }else{ ?>
                                                                            <td class="text-center">
                                                                            <?php if($requerimiento->TipoRequerimiento==3){ ?>
                                                                            <a target="_blank" href="../ordenCompra/<?= $ordenCompra->Documento ?>" class="btn btn-default "><span class='fa fa-cloud-download'></span> Descargar documento</a> 
                                                                            <?php } ?>
                                                                                <?php if(!$comprometido): ?>
                                                                                    <a href='#' class='btn btn-danger btn-remove-documento' data-tipo-req="<?= $requerimiento->TipoRequerimiento ?>" data-id="<?= $ordenCompra->ID ?>"><i class='fa fa-remove'></i></a>
                                                                                <?php endif ?>
                                                                            </td>
                                                                            <?php } ?>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <?php } ?>
                                                        </tr>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    <?php if($boton!=0){ ?>
                                                    <tr>
                                                        <td colspan="4"></td>
                                                        <td>
                                                            <td class="text-center"><a data-codigo-proyecto="<?= $requerimiento->CodigoProyecto ?>"  data-requerimiento-id="<?= $requerimiento->ID ?>" data-tipo-req="<?= $requerimiento->TipoRequerimiento ?>" href="#" class="btn btn-primary btn-crear">Crear</a></td>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    <?php }elseif($requerimiento->TipoRequerimiento == 4){ ?>
                                        <div class="form-group">
                                            <table class="table tbl-act">
                                                <tbody>
                                                    <tr class="tr-header">
                                                        <td>Código matriz</td>
                                                        <td>Recurso</td>
                                                        <td>Precio unitario</td>
                                                        <td>Cantidad</td>
                                                        <td>Total</td>
                                                        <td>Acciones</td>
                                                    </tr>
                                                    <?php $TipoDetalleOrden=0 ;?>
                                                    <?php foreach($detallesrequerimientos as $detallerequerimiento){ ?>
                                                       
                                                        <?php $viaticos=Viatico::find()->where('ID=:ID and Estado=1',[':ID'=>$detallerequerimiento->TipoDetalleOrdenID])->one();
                                                            if($TipoDetalleOrden!=$detallerequerimiento->TipoDetalleOrdenID){
                                                                $TipoDetalleOrden=$detallerequerimiento->TipoDetalleOrdenID;
                                                                $a=0;
                                                                $b=0;
                                                            }
                                                            
                                                            $BanderaContador=0;
                                                            
                                                            if($viaticos){ 
                                                                $detalles=DetalleRequerimiento::find()->where('RequerimientoID=:RequerimientoID and TipoDetalleOrdenID=:TipoDetalleOrdenID',[':RequerimientoID'=>$viaticos->RequerimientoID,':TipoDetalleOrdenID'=>$viaticos->ID])->all();
                                                                $BanderaContador=count($detalles);
                                                            }
                                                        ?>
                                                        <?php if($viaticos){ ?>
                                                        <?php $b++; ?>
                                                        <?php } ?>
                                                        <?php $comprometido = CompromisoPresupuestal::find()->where('RequerimientoID=:RequerimientoID and Estado=1',[':RequerimientoID'=>$requerimiento->ID])->one(); ?>
                                                        <tr>
                                                            <td class="text-center"><?= substr($detallerequerimiento->CodigoMatriz,0,1).'.'.$detallerequerimiento->Codificacion ?> </td>
                                                            <td><?= $detallerequerimiento->Recurso .' - '.$detallerequerimiento->Situacion ?> </td>
                                                            <td class="input-number"><?= number_format($detallerequerimiento->PrecioUnitario, 2, '.', ' ') ?></td>
                                                            <td class="text-center"><?= $detallerequerimiento->Cantidad ?></td>
                                                            <td class="input-number"><?= number_format(($detallerequerimiento->Cantidad*$detallerequerimiento->PrecioUnitario), 2, '.', ' ') ?></td>
                                                            <?php if($detallerequerimiento->Situacion==2){ ?>
                                                            <td class="text-center"><input type="checkbox" class="obtener_check" onclick="AgregarCodigo(this,<?= $detallerequerimiento->ID ?>)" data-detalle-requerimiento-id="<?= $detallerequerimiento->ID ?>"></td>
                                                            <?php $boton++; ?>
                                                                <!--<td class="text-center"><a data-precio-total="<?= ($detallerequerimiento->Cantidad*$detallerequerimiento->PrecioUnitario) ?>" data-codigo-proyecto="<?= $requerimiento->CodigoProyecto ?>" data-codificacion="<?= $detallerequerimiento->Codificacion ?>" data-detalle-requerimiento-id="<?= $detallerequerimiento->ID ?>" data-requerimiento-id="<?= $requerimiento->ID ?>" data-tipo-req="<?= $requerimiento->TipoRequerimiento ?>" href="#" class="btn btn-primary btn-crear">Crear</a></td>-->
                                                            <?php }elseif($detallerequerimiento->Situacion==3 && $b==$BanderaContador){ ?>
                                                                <td class="text-center"><a target="_blank" href="../viatico/plantilla?ID=<?= $detallerequerimiento->TipoDetalleOrdenID ?>" class="btn btn-default"><span class="fa fa-cloud-download"></span> Formato</a> 
                                                                <?php if(!$comprometido): ?>
                                                                <a href='#' class='btn btn-danger btn-remove btn' data-tipo-req="<?= $requerimiento->TipoRequerimiento ?>" data-id="<?= $detallerequerimiento->TipoDetalleOrdenID ?>"><i class='fa fa-remove fa-lg'></i></a>
                                                                <?php endif ?>
                                                                </td>
                                                            <?php } ?>
                                                        </tr>
                                                        
                                                        <?php if($viaticos){ ?>
                                                        <?php $a++; ?>
                                                        <tr>
                                                            <?php
                                                            $detalles=DetalleRequerimiento::find()->where('RequerimientoID=:RequerimientoID and TipoDetalleOrdenID=:TipoDetalleOrdenID',[':RequerimientoID'=>$viaticos->RequerimientoID,':TipoDetalleOrdenID'=>$viaticos->ID])->all();
                                                            $CadenaCodigoPoa="";
                                                            $i=1;
                                                            foreach($detalles as $detalle){
                                                                if($i>1)
                                                                {
                                                                    $CadenaCodigoPoa='O'.substr($detalle->Objetivo,0,1).'.A'.substr($detalle->Actividad,0,1).'-'.substr($detalle->CodigoMatriz,0,1).'.'.substr($detalle->CodigoMatriz,2,1).','.$CadenaCodigoPoa;    
                                                                }
                                                                else
                                                                {
                                                                    $CadenaCodigoPoa='O'.substr($detalle->Objetivo,0,1).'.A'.substr($detalle->Actividad,0,1).'-'.substr($detalle->CodigoMatriz,0,1).'.'.substr($detalle->CodigoMatriz,2,1);    
                                                                }
                                                                $i++;
                                                            }
                                                            ?>
                                                            <!-- <td>Detalle Viático</td> -->
                                                            <?php if($a==$BanderaContador){ ?>
                                                            <td colspan="6">
                                                                <table class="table tbl-act">
                                                                    <tbody>
                                                                    <tr class="tr-header">
                                                                        <td rowspan="2">Detalle Orden</td>
                                                                        <td>N°</td>
                                                                        <td>Nombres y apellidos</td>
                                                                        <td>DNI</td>
                                                                        <td>Código POA</td>
                                                                        <td>Nro días</td>
                                                                        <td>Documento</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="text-center"><?= str_pad($viaticos->Correlativo, 3, "0", STR_PAD_LEFT)."-".$viaticos->Annio ?></td>
                                                                        <td><?= $viaticos->Apellido.' '.$viaticos->Nombre ?></td>
                                                                        <td class="text-center"><?= $viaticos->Dni ?></td>
                                                                        <td class="text-center"><?= $CadenaCodigoPoa ?></td>
                                                                        <td class="text-center"><?= $viaticos->NroDias ?></td>
                                                                        <?php if(!$viaticos->Documento){ ?>
                                                                        <td class="text-center"><a class="btn btn-default btn-adjuntar btn" data-tipo-req="<?= $requerimiento->TipoRequerimiento ?>" data-id="<?= $viaticos->ID ?>" target="_blank" href="#"><span class="fa fa-cloud-upload"></span> Subir documento</a></td>
                                                                        <?php }else{ ?>
                                                                        <td class="text-center"><a target="_blank" class="btn btn-default" href="../viatico/<?= $viaticos->Documento ?>" ><span class='fa fa-cloud-download'></span> Descargar documento</a> 
                                                                        <?php if(!$comprometido): ?>
                                                                        <a href='#' class='btn btn-danger btn-remove-documento btn' data-tipo-req="<?= $requerimiento->TipoRequerimiento ?>" data-id="<?= $viaticos->ID ?>"><i class='fa fa-remove fa-lg'></i></a>
                                                                        <?php endif ?>
                                                                        </td>
                                                                        <?php } ?>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <?php } ?>
                                                        </tr>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    <?php if($boton!=0){ ?>
                                                    <tr>
                                                        <td colspan="4"></td>
                                                        <td>
                                                            <td class="text-center"><a data-codigo-proyecto="<?= $requerimiento->CodigoProyecto ?>"  data-requerimiento-id="<?= $requerimiento->ID ?>" data-tipo-req="<?= $requerimiento->TipoRequerimiento ?>" href="#" class="btn btn-primary btn-crear">Crear</a></td>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    <?php }elseif($requerimiento->TipoRequerimiento==5){ ?>
                                        <?php $cajaChica=DetalleCajaChica::find()->where('RequerimientoID=:RequerimientoID and Estado=1',[':RequerimientoID'=>$requerimiento->ID])->one(); ?>

                                        <?php $comprometido = CompromisoPresupuestal::find()->where('RequerimientoID=:RequerimientoID',[':RequerimientoID'=>$requerimiento->ID])->one(); ?>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <?php if($cajaChica){ ?>
                                                <table class="table tbl-act">
                                                    <tbody>
                                                        <tr class="tr-header">
                                                            <td>Caja chica</td>
                                                            <td>Tipo</td>
                                                            <td>Bienes</td>
                                                            <td>Servicios</td>
                                                            <td>Resolución directorial</td>
                                                            <td>Plantilla</td>
                                                            <td>Acciones</td>
                                                        </tr>
                                                        <tr>
                                                            <td>N° <?= str_pad($cajaChica->Correlativo, 3, "0", STR_PAD_LEFT)  ."-".$cajaChica->Annio ?></td>
                                                            <td><?= ($cajaChica->Tipo==1)?'Apertura':'Reembolso';?></td>
                                                            <td class="input-number"><?= number_format($cajaChica->Bienes, 2, '.', ' ') ?></td>
                                                            <td class="input-number"><?= number_format($cajaChica->Servicios, 2, '.', ' ') ?></td>
                                                            <td><a target="_blank" href="../cajachica/<?= $cajaChica->ResolucionDirectorial ?>" class="btn btn-default"><span class='fa fa-cloud-download'></span> Descargar</a> </td>
                                                            <?php if(!$cajaChica->Documento && $cajaChica->Tipo==1){ ?>
                                                            <td>
                                                            <a class="btn btn-default" target="_blank" href="../caja-chica/plantilla?ID=<?= $cajaChica->ID ?>"><span class="fa fa-cloud-download"></span> Formato</a> 
                                                            <a class="btn btn-default btn-adjuntar" data-tipo-req="<?= $requerimiento->TipoRequerimiento ?>" data-id="<?= $cajaChica->ID ?>" target="_blank" href="#"><span class="fa fa-cloud-upload"></span> Subir documento</a></td>
                                                            <?php }elseif(!$cajaChica->Documento && $cajaChica->Tipo==2){ ?>
                                                            <td>
                                                            <a target="_blank" href="../caja-chica/plantilla-solicitud?ID=<?= $cajaChica->ID ?>" class="btn btn-default"><span class="fa fa-cloud-download"></span> Formato</a> 
                                                            <a class="btn btn-default btn-adjuntar" data-tipo-req="<?= $requerimiento->TipoRequerimiento ?>" data-id="<?= $cajaChica->ID ?>" target="_blank" href="#"><span class="fa fa-cloud-upload"></span> Subir</a></td>
                                                            <?php }else{?>
                                                            <td> 
                                                            <a target="_blank" href="../cajachica/<?= $cajaChica->Documento ?>" class="btn btn-default"><span class='fa fa-cloud-download'></span> Descargar documento</a> 
                                                            <?php if(!$comprometido): ?>
                                                            <a href='#' class='btn btn-danger btn-remove-documento' data-tipo-req="<?= $requerimiento->TipoRequerimiento ?>" data-id="<?= $cajaChica->ID ?>"><i class='fa fa-remove fa-lg'></i></a>
                                                            <?php endif ?>
                                                            </td>
                                                            <?php } ?>
                                                            <?php if(!$comprometido): ?>
                                                            <td> <a href='#' class='btn btn-danger btn-remove' data-tipo-req="<?= $requerimiento->TipoRequerimiento ?>" data-id="<?= $cajaChica->ID ?>"><i class='fa fa-remove fa-lg'></i></a>
                                                            <?php endif ?>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <?php }else{?>
                                                <a href="#" data-codigo-proyecto="<?= $requerimiento->CodigoProyecto ?>" data-requerimiento-id="<?= $requerimiento->ID ?>" data-tipo-req="<?= $requerimiento->TipoRequerimiento ?>" class="btn btn-primary btn-crear">Crear</a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php }elseif($requerimiento->TipoRequerimiento==6){?>
                                        <div class="form-group">
                                            <table class="table tbl-act">
                                                <tbody>
                                                    <tr class="tr-header">
                                                        <td>Código matriz</td>
                                                        <td>Recurso</td>
                                                        <td>Precio unitario</td>
                                                        <td>Cantidad</td>
                                                        <td>Total</td>
                                                        <td>Acciones</td>
                                                    </tr>
                                                    <?php $TipoDetalleOrden=0; ?>
                                                    <?php foreach($detallesrequerimientos as $detallerequerimiento){ ?>
                                                        <?php $encargo=Encargo::find()->where('ID=:ID and Estado=1',[':ID'=>$detallerequerimiento->TipoDetalleOrdenID])->one();
                                                            if($TipoDetalleOrden!=$detallerequerimiento->TipoDetalleOrdenID){
                                                                $TipoDetalleOrden=$detallerequerimiento->TipoDetalleOrdenID;
                                                                $a=0;
                                                                $b=0;
                                                            }
                                                            
                                                            $BanderaContador=0;
                                                            
                                                            if($encargo){ 
                                                                $detalles=DetalleRequerimiento::find()->where('RequerimientoID=:RequerimientoID and TipoDetalleOrdenID=:TipoDetalleOrdenID',[':RequerimientoID'=>$encargo->RequerimientoID,':TipoDetalleOrdenID'=>$encargo->ID])->all();
                                                                $BanderaContador=count($detalles);
                                                            }
                                                        ?>
                                                        <?php if($encargo){ ?>
                                                        <?php $b++; ?>
                                                        <?php } ?>
                                                        
                                                        <?php $comprometido = CompromisoPresupuestal::find()->where('RequerimientoID=:RequerimientoID and Estado=1',[':RequerimientoID'=>$requerimiento->ID])->one(); ?>
                                                        <tr>
                                                            <td><?= substr($detallerequerimiento->CodigoMatriz,0,1).'.'.$detallerequerimiento->Codificacion ?> </td>
                                                            <td><?= $detallerequerimiento->Recurso .' - '.$detallerequerimiento->Situacion?> </td>
                                                            <td class="input-number"><?= $detallerequerimiento->PrecioUnitario ?></td>
                                                            <td><?= $detallerequerimiento->Cantidad ?></td>
                                                            <td class="input-number"><?= number_format(($detallerequerimiento->Cantidad*$detallerequerimiento->PrecioUnitario), 2, '.', ' ') ?></td>
                                                            <?php if($detallerequerimiento->Situacion==2){ ?>
                                                                <td class="text-center"><input type="checkbox" class="obtener_check" onclick="AgregarCodigo(this,<?= $detallerequerimiento->ID ?>)" data-detalle-requerimiento-id="<?= $detallerequerimiento->ID ?>"></td>
                                                                <?php $boton++; ?>
                                                                
                                                            <?php }elseif($detallerequerimiento->Situacion==3 && $b==$BanderaContador){ ?>
                                                                <td><a target="_blank" href="../encargo/plantilla?ID=<?= $detallerequerimiento->TipoDetalleOrdenID ?>" class="btn btn-default"><span class="fa fa-cloud-download"></span> Formato</a> 
                                                                <?php if(!$comprometido): ?>
                                                                <a href='#' class='btn btn-danger btn-remove' data-tipo-req="<?= $requerimiento->TipoRequerimiento ?>" data-id="<?= $detallerequerimiento->TipoDetalleOrdenID ?>"><i class='fa fa-remove fa-lg'></i></a>
                                                                <?php endif ?>
                                                                </td>
                                                            <?php } ?>
                                                        </tr>
                                                        <?php if($encargo){ ?>
                                                        <?php $a++; ?>
                                                            <tr>
                                                                <?php
                                                                    $detalles=DetalleRequerimiento::find()->where('RequerimientoID=:RequerimientoID and TipoDetalleOrdenID=:TipoDetalleOrdenID',[':RequerimientoID'=>$encargo->RequerimientoID,':TipoDetalleOrdenID'=>$encargo->ID])->all();
                                                                    $CadenaCodigoPoa="";
                                                                    $i=1;
                                                                    // print_r($detalles);echo '<br>------------------<br>';
                                                                    foreach($detalles as $detalle){
                                                                        if($i>1)
                                                                        {
                                                                            $CadenaCodigoPoa='O.'.explode('.', $detalle->Objetivo)[0].' A'.explode('.', $detalle->Actividad)[0].','.$CadenaCodigoPoa;    
                                                                        }
                                                                        else
                                                                        {
                                                                            $CadenaCodigoPoa='O.'.explode('.', $detalle->Objetivo)[0].' A'.explode('.', $detalle->Actividad)[0];    
                                                                        }
                                                                        $i++;
                                                                    }
                                                                ?>
                                                                <?php if($a==$BanderaContador){ ?>
                                                                <td colspan="6">
                                                                    <table class="table tbl-act">
                                                                        <tr class="tr-header">
                                                                            <td rowspan="2">Detalle Orden</td>
                                                                            <td>N° encargo</td>
                                                                            <td>Fecha registro</td>
                                                                            <td>N° certificación</td>
                                                                            <td>Código POA</td>
                                                                            <td>Resolución directorial</td>
                                                                            <td>Documento</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><?= str_pad($encargo->Correlativo, 3, "0", STR_PAD_LEFT)."-".$encargo->Annio ?></td>
                                                                            <td><?= date('d-m-Y',strtotime($encargo->FechaRegistro)) ?></td>
                                                                            <td><?= $encargo->Certificacion ?></td>
                                                                            <td><?= $CadenaCodigoPoa ?></td>
                                                                            <td><a class="btn btn-default" target="_blank" href="../encargos/<?= $encargo->ResolucionDirectorial ?>" ><span class='fa fa-cloud-download'></span> Descargar</a> </td>
                                                                            <?php if(!$encargo->Documento){ ?>
                                                                                <td><a class="btn btn-default btn-adjuntar" data-tipo-req="<?= $requerimiento->TipoRequerimiento ?>" data-id="<?= $encargo->ID ?>" target="_blank" href="#"><span class="fa fa-cloud-upload"></span> Subir documento</a></td>
                                                                            <?php }else{?>
                                                                                <td><a class="btn btn-default" target="_blank" href="../encargos/<?= $encargo->Documento ?>" ><span class='fa fa-cloud-download'></span> Descargar documento</a> 
                                                                                <?php if(!$comprometido): ?>
                                                                                <a href='#' class='btn btn-danger btn-remove-documento' data-tipo-req="<?= $requerimiento->TipoRequerimiento ?>" data-id="<?= $encargo->ID ?>"><i class='fa fa-remove fa-lg'></i></a>
                                                                                <?php endif ?>
                                                                                </td>
                                                                            <?php } ?>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <?php } ?>
                                                            </tr>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    <?php if($boton!=0){ ?>
                                                    <tr>
                                                        <td colspan="4"></td>
                                                        <td>
                                                            <td class="text-center"><a data-codigo-proyecto="<?= $requerimiento->CodigoProyecto ?>"  data-requerimiento-id="<?= $requerimiento->ID ?>" data-tipo-req="<?= $requerimiento->TipoRequerimiento ?>" href="#" class="btn btn-primary btn-crear">Crear</a></td>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-ctrl-orden-compra" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Orden de Compra</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-ctrl-orden-servicio" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Orden de Servicio</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-ctrl-orden-adjuntar" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Adjuntar Documento</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-ctrl-viatico" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Viático</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-ctrl-caja-chica" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Caja Chica</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-ctrl-encargo" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Encargo</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>
<script>
    $( function() {

        // $('#date_begin,#date_end').datetimepicker(); 

        var startDateTextBox = $('#salida_viatico');
        var endDateTextBox = $('#regreso_viatico');

        startDateTextBox.datetimepicker({ 
            language: 'es',
            onClose: function(dateText, inst) {
                if (endDateTextBox.val() != '') {
                    var testStartDate = startDateTextBox.datetimepicker('getDate');
                    var testEndDate = endDateTextBox.datetimepicker('getDate');
                    if (testStartDate > testEndDate)
                        endDateTextBox.datetimepicker('setDate', testStartDate);
                }
                else {
                    endDateTextBox.val(dateText);
                }
            },
            onSelect: function (selectedDateTime){
                endDateTextBox.datetimepicker('option', 'minDate', startDateTextBox.datetimepicker('getDate') );
            }
        });
        endDateTextBox.datetimepicker({ 
            language: 'es',
            onClose: function(dateText, inst) {
                if (startDateTextBox.val() != '') {
                    var testStartDate = startDateTextBox.datetimepicker('getDate');
                    var testEndDate = endDateTextBox.datetimepicker('getDate');
                    if (testStartDate > testEndDate)
                        startDateTextBox.datetimepicker('setDate', testEndDate);
                }
                else {
                    startDateTextBox.val(dateText);
                }
            },
            onSelect: function (selectedDateTime){
                startDateTextBox.datetimepicker('option', 'maxDate', endDateTextBox.datetimepicker('getDate') );
            }
        });
    });
</script>
<script>
    var Codigos=[];
    function AgregarCodigo(elemento,DetalleRequerimientoID) {
        if ($(elemento)[0].checked) {
            Codigos.push(DetalleRequerimientoID);
        }
        else
        {
            Codigos = jQuery.grep(Codigos, function(value) {
                return value != DetalleRequerimientoID;
            });     
        }
        console.log(Codigos);
    }
    
    $('body').on('click', '.btn-crear', function (e) {
        e.preventDefault();
        
        var tiporequerimiento =$(this).attr('data-tipo-req');
        var requerimientoid=$(this).attr('data-requerimiento-id');
        var codigoproyecto=$(this).attr('data-codigo-proyecto');
        if(tiporequerimiento == 1){
            console.log(tiporequerimiento);
            if (Codigos.length==0) {
                bootbox.alert('Debe seleccionar');
                return false;
            }
            var codificacion=$(this).attr('data-codificacion');
            var preciototal=$(this).attr('data-precio-total');
            //var detallerequerimientoid=$(this).attr('data-detalle-requerimiento-id');
            $('#modal-ctrl-orden-servicio .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/orden/crear-orden-servicio-factura?RequerimientoID='+requerimientoid+'&CodigoProyecto='+codigoproyecto+'&DetallesRequerimientosIDs='+Codigos);
            $('#modal-ctrl-orden-servicio').modal('show');
        }else if(tiporequerimiento == 2){
            console.log(tiporequerimiento);
            if (Codigos.length==0) {
                bootbox.alert('Debe seleccionar');
                return false;
            }
            var codificacion=$(this).attr('data-codificacion');
            var preciototal=$(this).attr('data-precio-total');
            //var detallerequerimientoid=$(this).attr('data-detalle-requerimiento-id');
            $('#modal-ctrl-orden-servicio .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/orden/crear-orden-servicio-rh?RequerimientoID='+requerimientoid+'&CodigoProyecto='+codigoproyecto+'&DetallesRequerimientosIDs='+Codigos);
            $('#modal-ctrl-orden-servicio').modal('show');
        }else if(tiporequerimiento == 3){
            if (Codigos.length==0) {
                bootbox.alert('Debe seleccionar');
                return false;
            }
            var codificacion=$(this).attr('data-codificacion');
            var preciototal=$(this).attr('data-precio-total');
            //var detallerequerimientoid=$(this).attr('data-detalle-requerimiento-id');
            $('#modal-ctrl-orden-compra .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/orden/crear-orden-compra?RequerimientoID='+requerimientoid+'&CodigoProyecto='+codigoproyecto+'&DetallesRequerimientosIDs='+Codigos);
            $('#modal-ctrl-orden-compra').modal('show');
        }else if(tiporequerimiento == 4){
            if (Codigos.length==0) {
                bootbox.alert('Debe seleccionar');
                return false;
            }
            var codificacion=$(this).attr('data-codificacion');
            var preciototal=$(this).attr('data-precio-total');
            //var detallerequerimientoid=$(this).attr('data-detalle-requerimiento-id');
            
            
            //$('#modal-ctrl-viatico .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/viatico/crear-planilla-viaticos?RequerimientoID='+requerimientoid+'&Codificacion='+codificacion+'&CodigoProyecto='+codigoproyecto+'&PrecioTotal='+preciototal);
            $('#modal-ctrl-viatico .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/viatico/crear-planilla-viaticos?RequerimientoID='+requerimientoid+'&CodigoProyecto='+codigoproyecto+'&DetallesRequerimientosIDs='+Codigos);
            $('#modal-ctrl-viatico').modal('show');
        }else if(tiporequerimiento == 5){
            $('#modal-ctrl-caja-chica .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/caja-chica/crear?RequerimientoID='+requerimientoid+'&CodigoProyecto='+codigoproyecto);
            $('#modal-ctrl-caja-chica').modal('show');
        }else if(tiporequerimiento == 6){
            if (Codigos.length==0) {
                bootbox.alert('Debe seleccionar');
                return false;
            }
            var codificacion=$(this).attr('data-codificacion');
            var preciototal=$(this).attr('data-precio-total');
            $('#modal-ctrl-encargo .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/encargo/crear?RequerimientoID='+requerimientoid+'&CodigoProyecto='+codigoproyecto+'&DetallesRequerimientosIDs='+Codigos);
            $('#modal-ctrl-encargo').modal('show');
        }
        
    });
    
    $('body').on('click', '.btn-adjuntar', function (e) {
        e.preventDefault();
        var ID=$(this).attr('data-id');
        var TipoRequerimiento=$(this).attr('data-tipo-req');
        if (TipoRequerimiento==1) {
            $('#modal-ctrl-orden-adjuntar .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/orden/adjuntar-documento?ID='+ID);
        }
        else if (TipoRequerimiento==2) {
            $('#modal-ctrl-orden-adjuntar .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/orden/adjuntar-documento?ID='+ID);
        }
        else if (TipoRequerimiento==3) {
            $('#modal-ctrl-orden-adjuntar .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/orden/adjuntar-documento?ID='+ID);
        }
        else if (TipoRequerimiento==4) {
            $('#modal-ctrl-orden-adjuntar .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/viatico/adjuntar-documento?ID='+ID);
        }
        else if (TipoRequerimiento==5) {
            $('#modal-ctrl-orden-adjuntar .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/caja-chica/adjuntar-documento?ID='+ID);
        }
        else if (TipoRequerimiento==6) {
            $('#modal-ctrl-orden-adjuntar .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/encargo/adjuntar-documento?ID='+ID);
        }
        
        $('#modal-ctrl-orden-adjuntar').modal('show');
    });
    
    
    
    $('body').on('click', '.btn-remove-documento', function (e) {
        e.preventDefault();
        var $this = $(this);
        var tiporequerimiento =$(this).attr('data-tipo-req');
        if (tiporequerimiento==1) {
            bootboxConfirmEliminar($this,
            '¿Está seguro de eliminar el documento adjunto?',
            function () {
                var id = $this.attr('data-id');
                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                $.get('<?= \Yii::$app->request->BaseUrl ?>/orden/eliminar-adjunto', { id: id ,_csrf: toke }, function (data) {
                    var jx = JSON.parse(data);
                    if (jx.Success == true) {
                        location.reload();
                    }
                });
            });
        }
        else if (tiporequerimiento==2) {
            bootboxConfirmEliminar($this,
            'Está seguro de eliminar el documento adjunto?',
            function () {
                var id = $this.attr('data-id');
                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                $.get('<?= \Yii::$app->request->BaseUrl ?>/orden/eliminar-adjunto', { id: id ,_csrf: toke }, function (data) {
                    var jx = JSON.parse(data);
                    if (jx.Success == true) {
                        location.reload();
                    }
                });
            });
        }
        else if (tiporequerimiento==3) {
            bootboxConfirmEliminar($this,
            'Está seguro de eliminar el documento adjunto?',
            function () {
                var id = $this.attr('data-id');
                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                $.get('<?= \Yii::$app->request->BaseUrl ?>/orden/eliminar-adjunto', { id: id ,_csrf: toke }, function (data) {
                    var jx = JSON.parse(data);
                    if (jx.Success == true) {
                        location.reload();
                    }
                });
            });
        }
        else if (tiporequerimiento==4) {
            bootboxConfirmEliminar($this,
            'Está seguro de eliminar el documento adjunto?',
            function () {
                var id = $this.attr('data-id');
                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                $.get('<?= \Yii::$app->request->BaseUrl ?>/viatico/eliminar-adjunto', { id: id ,_csrf: toke }, function (data) {
                    var jx = JSON.parse(data);
                    if (jx.Success == true) {
                        location.reload();
                    }
                });
            });
        }
        else if (tiporequerimiento==5) {
            bootboxConfirmEliminar($this,
            'Está seguro de eliminar el documento adjunto?',
            function () {
                var id = $this.attr('data-id');
                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                $.get('<?= \Yii::$app->request->BaseUrl ?>/caja-chica/eliminar-adjunto', { id: id ,_csrf: toke }, function (data) {
                    var jx = JSON.parse(data);
                    if (jx.Success == true) {
                        location.reload();
                    }
                });
            });
        }
        else if (tiporequerimiento==6) {
            bootboxConfirmEliminar($this,
            'Está seguro de eliminar el documento adjunto?',
            function () {
                var id = $this.attr('data-id');
                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                $.get('<?= \Yii::$app->request->BaseUrl ?>/encargo/eliminar-adjunto', { id: id ,_csrf: toke }, function (data) {
                    var jx = JSON.parse(data);
                    if (jx.Success == true) {
                        location.reload();
                    }
                });
            });
        }
        
    });
    
    $('body').on('click', '.btn-remove', function (e) {
        e.preventDefault();
        var $this = $(this);
        var tiporequerimiento =$(this).attr('data-tipo-req');
        if (tiporequerimiento==1) {
            bootboxConfirmEliminar($this,
            'Está seguro de eliminar el registro de la orden?',
            function () {
                var id = $this.attr('data-id');
                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                $.get('<?= \Yii::$app->request->BaseUrl ?>/orden/eliminar', { id: id ,_csrf: toke }, function (data) {
                    var jx = JSON.parse(data);
                    if (jx.Success == true) {
                        location.reload();
                    }
                });
            });
        }
        else if (tiporequerimiento==2) {
            bootboxConfirmEliminar($this,
            'Está seguro de eliminar el registro de la orden?',
            function () {
                var id = $this.attr('data-id');
                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                $.get('<?= \Yii::$app->request->BaseUrl ?>/orden/eliminar', { id: id ,_csrf: toke }, function (data) {
                    var jx = JSON.parse(data);
                    if (jx.Success == true) {
                        location.reload();
                    }
                });
            });
        }
        else if (tiporequerimiento==3) {
            bootboxConfirmEliminar($this,
            'Está seguro de eliminar el registro de la orden?',
            function () {
                var id = $this.attr('data-id');
                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                $.get('<?= \Yii::$app->request->BaseUrl ?>/orden/eliminar', { id: id ,_csrf: toke }, function (data) {
                    var jx = JSON.parse(data);
                    if (jx.Success == true) {
                        location.reload();
                    }
                });
            });
        }
        else if (tiporequerimiento==4) {
            bootboxConfirmEliminar($this,
            'Está seguro de eliminar el registro de viáticos?',
            function () {
                var id = $this.attr('data-id');
                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                $.get('<?= \Yii::$app->request->BaseUrl ?>/viatico/eliminar', { id: id ,_csrf: toke }, function (data) {
                    var jx = JSON.parse(data);
                    if (jx.Success == true) {
                        location.reload();
                    }
                });
            });
        }
        else if (tiporequerimiento==5) {
            bootboxConfirmEliminar($this,
            'Está seguro de eliminar el registro de caja chica?',
            function () {
                var id = $this.attr('data-id');
                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                $.get('<?= \Yii::$app->request->BaseUrl ?>/caja-chica/eliminar', { id: id ,_csrf: toke }, function (data) {
                    var jx = JSON.parse(data);
                    if (jx.Success == true) {
                        location.reload();
                    }
                });
            });
        }
        else if (tiporequerimiento==6) {
            bootboxConfirmEliminar($this,
            'Está seguro de eliminar el registro de encargo?',
            function () {
                var id = $this.attr('data-id');
                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                $.get('<?= \Yii::$app->request->BaseUrl ?>/encargo/eliminar', { id: id ,_csrf: toke }, function (data) {
                    var jx = JSON.parse(data);
                    if (jx.Success == true) {
                        location.reload();
                    }
                });
            });
        }
    });
    
    $('body').on('click', '#btn-guardar-caja', function (e) {
        e.preventDefault();
        var $btn = $(this);
        var total=0;
        total=parseFloat($('#caja-chica-bienes').val())+parseFloat($('#caja-chica-servicios').val());
        
        $btn.attr('disabled', 'disabled');
        var isValid = formParsleyCaja.validate();
       
        if (isValid) {
            
            if (total>2025) {
                bootbox.alert('El monto suma total de Bienes y Servicios no puede pasar de S/. 2025.00 ');
                $btn.removeAttr('disabled');
                return false;
            }
            else
            {   
                $('#frmCaja').submit();
                // sendFormNoMessage($('#frmCaja'), $(this), function (eve) {
                //     location.reload();
                // });
            }
            return true; 
            
        }else{
            return false;
            $btn.removeAttr('disabled');
        }
        return false;
        $btn.removeAttr('disabled');
    });
    
    validarNumeros();
    
</script>


