<?php
use app\models\Orden;
use app\models\Siaft;
?>
<div id="page-content" style="min-height: 885px;">
    <div id="wrap">
        <div id="page-heading">
            <h1></h1>
        </div>
        <div class="container">
                    
            <style>
                .panel .panel-body {
                    padding: 10px !important;
                }

                .panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 1px solid #a8a8ab !important;
                }

                .panel-gray .panel-body {
                    border-top: none !important;
                }

                .btn-sm {
                    padding: 5px 10px !important;
                    line-height: 1.5 !important;
                }

                .input-comp-nombre {
                    text-transform: uppercase;
                }

                .tbl-act > tbody > .tr-header > th {
                    text-align: center;
                    font-size: 12px;
                    font-weight: bold;
                    padding: 5px !important;
                    vertical-align: middle !important;
                    /*border: 1px solid #cfcfd0;
                    background-color: #f0f0f1;*/
                    border: 1px solid #c0c0c0;
                    background-color: #e3e3e3;
                }

                /*.tbl-act .tr-item td {
                    padding: 2px !important;
                }*/
                input.form-control, select.form-control {
                    /*height: 28px !important;*/
                }

                .tbl-act .tr-act .input-nombre {
                    min-width: 320px !important;
                }

                .tbl-act .tr-act .input-unidadmedida {
                    min-width: 100px !important;
                }

                .input-number {
                    min-width: 92px !important;
                }

                .btn-add-re {
                    float: right;
                }

                /*.tbl-act > tbody > tr > td:nth-child(2) {
                    min-width: 40px !important;
                    text-align: center !important;
                }*/
                .text-left {
                    text-align: left !important;
                    font-weight: bold;
                }

                .tr-comp input, .tr-proy input {
                    font-weight: bold;
                }

                .th-proy {
                    min-width: 630px !important;
                }
            </style>

            <div class="tab-container">
                <ul class="nav nav-tabs">
                    <li id="li-poaf" class="active">
                        <a href="#tab-informacion-general" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Detalle Requerimiento</a>
                    </li>
                </ul>
                <div class="tab-content" style="background: #fff">
                    <div class="tab-pane active" id="tab-informacion-general">
                        <div id="container-poaf">
                            <?php 
                            switch ($cabecera->Situacion) {
                                case '1':
                                    $situacion = 'PENDIENTE';
                                break;
                                case '2':
                                    $situacion = 'PENDIENTE';
                                break;
                                case '3':
                                    $situacion = 'ACTIVO';
                                break;
                                case '6':
                                    $situacion = 'ANULADO';
                                break;
                                case '5':
                                    $situacion = '';
                                break;
                                case '0':
                                    $situacion = 'PENDIENTE DE APROBAR';
                                break;
                            }

                            switch ($cabecera->GastoElegible) {
                                case '1':
                                    $gastos = 'Gasto Elegible';
                                break;
                                case '2':
                                    $gastos = 'Gasto No Elegible';
                                break;
                                case '0':
                                    $gastos = '';
                                break;
                            }
                            ?>
                            <h3>Proyecto <?php echo $cabecera->Codigo ?> / Documento Nº <?php echo $cabecera->Correlativo ?> - <strong><?= $gastos ?></strong>-<strong><?php echo $situacion ?></strong></h3>


                            <?php if(\Yii::$app->user->identity->rol == 14 || \Yii::$app->user->identity->rol == 13): ?>
                                <?php if($cabecera->UsuarioAsignado == ''): ?>
                                    <form class="form-horizontal">
                                        <div class="col-md-6">
                                            <label class="form-label">Gasto</label>
                                            <select name="gasto" id="gastos" class="form-control" required="" data-id="<?php echo $cabecera->ID ?>">
                                                <option value="">[SELECCIONE]</option>
                                                <?php foreach ($res as $ue) { ?>
                                                    <option value="<?php echo $ue['ID'] ?>"><?php echo $ue['Nombre'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>

                                        <div class="col-md-12">
                                            <br>
                                            <a href="javascript:;" class="btn btn-primary" id="verifica">ENVIAR</a>
                                        </div>
                                    </form>
                                <?php else: ?>
                                    <h4><strong><?php echo strtoupper($cabecera->Nombre.' '.$cabecera->ApellidoPaterno) ?></strong></h4>
                                <?php endif; ?>
                            <?php endif; ?>


                            <br>
                            <?php if($requerimiento->TipoRequerimiento==1 || $requerimiento->TipoRequerimiento==2 || $requerimiento->TipoRequerimiento==3){?>
                            <table class="table table-hover tbl-act">
                                <tbody>
                                    <tr class="tr-header">
                                        <?php if(\Yii::$app->user->identity->rol == 14): ?>
                                            <!-- <th></th> -->
                                        <?php endif ?>
                                        <th>Tipo</th>
                                        <th>RUC</th>
                                        <th>Razon Social</th>
                                        <!-- <th>Tipo de Proceso</th> -->
                                        <th>Monto Total</th>
                                        <th>Formato</th>
                                        <th>Compromiso Anual</th>
                                        <th>Compromiso Mensual</th>
                                    </tr>
                                <?php // echo '<pre>'; print_r($resultados); //die(); ?>
                                    <?php foreach ($resultados as $res): ?>
                                        <tr>
                                            <?php if(\Yii::$app->user->identity->rol == 14): ?>
                                                <?php if($cabecera->Situacion == 3): ?>
                                                    <?php if($res['SituacionSIAFT'] == 0): ?>
                                                        <!--<td><input type="checkbox" name="" data-id="<?php echo $res['ID'] ?>" class="check"></td>-->
                                                    <?php elseif($res['SituacionSIAFT'] == 1): ?>
                                                        <!--<td><input type="checkbox" name="" data-id="<?php echo $res['ID'] ?>" class="check" checked="checked"></td>-->
                                                    <?php else: ?>
                                                        <!-- <td></td> -->
                                                    <?php endif ?>
                                                <?php endif ?>
                                            <?php endif ?>
                                            <td><?php echo($res['Nombre']) ?></td>
                                            <td><?php echo($res['RUC']) ?></td>
                                            <td><?php echo($res['RazonSocial']) ?></td>
                                            <!-- <td><?php echo($res["TipoProceso"] == 1 ? 'Comparacion de Precios' : 'Evaluacion de CVs') ?></td> -->
                                            <td><?php echo number_format(($res["Monto"]), 2, '.', ' ') ?></td>
                                            <?php if($res["TipoGasto"]==1){ ?>
                                            <td><a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/ordenServicioFactura/<?= $res["Documento"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Descargar</a></td>
                                            <?php }elseif($res["TipoGasto"]==2){ ?>
                                            <td><a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/ordenServicioRH/<?= $res["Documento"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Descargar</a></td>
                                            <?php }elseif($res["TipoGasto"]==3){ ?>
                                            <td><a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/ordenCompra/<?= $res["Documento"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Descargar </a></td>
                                            <?php } ?>

                                            <?php 
                                            $CompAnual = Siaft::find()->where('TipoGastoID=:TipoGastoID AND Orden=:Orden and Fase=9',[':TipoGastoID'=>$requerimiento->TipoRequerimiento,':Orden'=>$res['ID']])->one();
                                            $CompMensual = Siaft::find()->where('TipoGastoID=:TipoGastoID AND Orden=:Orden and Fase=10',[':TipoGastoID'=>$requerimiento->TipoRequerimiento,':Orden'=>$res['ID']])->one();
                                            if($CompAnual){
                                                $canual = $CompAnual->Siaf;
                                            }else{
                                                $canual = "--";
                                            }
                                            
                                            if($CompMensual){
                                                $cmensual = $CompMensual->Siaf;
                                            }else{
                                                $cmensual = "--";
                                            }
                                            ?>
                                            <td><?php echo $canual; ?></td>
                                            <td><?php echo $cmensual; ?></td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                            <?php }elseif($requerimiento->TipoRequerimiento==4){?>
                            <table class="table table-hover tbl-act">
                                <tbody>
                                    <tr class="tr-header">
                                        <th>Tipo</th>
                                        <th>Correlativo</th>
                                        <th>Datos</th>
                                        <th>DNI</th>
                                        <th>Monto Total</th>
                                        <th>Formato</th>
                                        <th>Compromiso Anual</th>
                                        <th>Compromiso Mensual</th>
                                    </tr>
                                    <?php foreach ($resultados as $res): ?>
                                        <tr>
                                            <td>Viático</td>
                                            
                                            
                                            <td><?php echo (str_pad($res['Correlativo'], 3, "0", STR_PAD_LEFT)  ."-".$res["Annio"]) ?></td>
                                            <td><?php echo $res['Apellido'].' '.$res['Nombre'] ?></td>
                                            <td><?php echo $res['Dni'] ?></td>
                                            <td>S/.<?php echo $res['Monto'] ?></td>

                                            <td><a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/viatico/<?= $res["Documento"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Descarga adjunto</a></td>

                                            <?php 
                                            $Or = Siaft::find()->where('TipoGastoID=:TipoGastoID AND Orden=:Orden',[':TipoGastoID'=>$requerimiento->TipoRequerimiento,':Orden'=>$res['ID']])->one();
                                            
                                            if($Or){
                                                $val = $Or->Siaf;
                                            }else{
                                                $val = "--";

                                            }
                                            ?>
                                            <td><?php echo $val; ?></td>
                                            
                                        </tr>
                                        <?php $compromisoID = $res['CompromisoPresupuestalID']; ?>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                            <?php }elseif($requerimiento->TipoRequerimiento==5){?>
                            <table class="table table-hover tbl-act">
                                <tbody>
                                    <tr class="tr-header">
                                        <th>Tipo de Gasto</th>
                                        <th>Tipo</th>
                                        <th>Bienes</th>
                                        <th>Servicios</th>
                                        <th>Monto Total</th>
                                        <th>Formato</th>
                                        <th>Compromiso Anual</th>
                                        <th>Compromiso Mensual</th>
                                    </tr>

                                    <?php foreach ($resultados as $res): ?>
                                    <tr>
                                        <td>Caja Chica</td>
                                        <td><?php echo($res['Tipo']==1)?'Apertura':'Desembolso'; ?></td>
                                        <td><?php echo number_format(($res['Bienes']), 2, '.', ' ') ?></td>
                                        <td><?php echo number_format(($res['Servicios']), 2, '.', ' ') ?></td>
                                        <td><?php echo number_format(($res['Servicios']+$res['Bienes']), 2, '.', ' ') ?></td>
                                        <td><a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/cajachica/<?= $res["ResolucionDirectorial"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Descarga adjunto</a></td>

                                        <?php 
                                            $Or = Siaft::find()->where('TipoGastoID=:TipoGastoID AND Orden=:Orden',[':TipoGastoID'=>$requerimiento->TipoRequerimiento,':Orden'=>$res['ID']])->one();
                                            
                                            if($Or){
                                                $val = $Or->Siaf;
                                            }else{
                                                $val = "--";

                                            }
                                            ?>
                                            <td><?php echo $val; ?></td>
                                    </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                            <?php $compromisoID = $cabecera->ID; ?>
                            <?php }elseif($requerimiento->TipoRequerimiento==6){?>
                            <table class="table table-hover tbl-act">
                                <tbody>
                                    <tr class="tr-header">
                                        <th>Tipo de Gasto</th>
                                        <th>Bienes</th>
                                        <th>Servicios</th>
                                        <th>Monto Total</th>
                                        <th>Formato</th>
                                        <th>Compromiso Anual</th>
                                        <th>Compromiso Mensual</th>
                                    </tr>
                                    <?php foreach ($resultados as $res): ?>
                                    <tr>
                                        <td>Encargos</td>
                                        <td><?php echo number_format(($res['Bienes']), 2, '.', ' ') ?></td>
                                        <td><?php echo number_format(($res['Servicios']), 2, '.', ' ') ?></td>
                                        <td><?php echo number_format(($res['Servicios']+$res['Bienes']), 2, '.', ' ') ?></td>
                                        <td><a class="btn btn-default" href="<?= \Yii::$app->request->BaseUrl ?>/encargos/<?= $res["Documento"] ?>" target="_blank"><span class='fa fa-cloud-download'></span> Descarga adjunto</a></td>
                                        <?php 
                                            $Or = Siaft::find()->where('TipoGastoID=:TipoGastoID AND Orden=:Orden',[':TipoGastoID'=>$requerimiento->TipoRequerimiento,':Orden'=>$res['ID']])->one();
                                            
                                            if($Or){
                                                $val = $Or->Siaf;
                                            }else{
                                                $val = "--";

                                            }
                                            ?>
                                            <td><?php echo $val; ?></td>
                                    </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                            <?php $compromisoID = $cabecera->ID; ?>
                            <?php } ?>
                        </div>
                        <?php if(\Yii::$app->user->identity->rol == 14): ?>
                            <a target="_blank" class="btn btn-primary" href="<?= \Yii::$app->request->BaseUrl ?>/compromisopresupuestal/<?php echo $cabecera->FormatoUafsi ?>">
                                <span class="fa fa-cloud-download"></span> Descargar compromiso presupuestal
                            </a>
                            <?php if($cabecera->Situacion == 3 && ($requerimiento->TipoRequerimiento == 1 ||$requerimiento->TipoRequerimiento == 2 || $requerimiento->TipoRequerimiento == 3) ){ ?>
                                <?php if($cabecera->CompromisoAnual!=1 ){?>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-primary generar-compromiso'>Generar Compromiso Anual</a>
                                <?php }elseif($cabecera->CompromisoAnual==1 && $cabecera->CompromisoAdministrativo!=1){?>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-success generar-compromiso-administrativo'>Generar Compromiso Administrativo</a>
                                <?php }elseif($cabecera->CompromisoAdministrativo==1){?>
                                <!--<a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-primary generar-devengado'>Generar Devengado</a>-->
                                <?php } ?>
                            <?php } ?>
                            <?php if($cabecera->Situacion == 3 && ($requerimiento->TipoRequerimiento == 4) ){ ?>
                                <?php if($cabecera->CompromisoAnual!=1 ){?>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-primary generar-compromiso'>Generar Compromiso Anual</a>
                                <?php }elseif($cabecera->CompromisoAnual==1 && $cabecera->CompromisoAdministrativo!=1){?>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-success generar-compromiso-administrativo'>Generar Compromiso Administrativo</a>
                                <?php }elseif($cabecera->CompromisoAdministrativo==1){?>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-primary generar-devengado'>Generar Devengado</a>
                                <?php } ?>
                            <?php } ?>
                            <?php if($cabecera->Situacion == 3 && ($requerimiento->TipoRequerimiento == 5) ){ ?>
                                <?php if($cabecera->CompromisoAnual!=1 ){?>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-primary generar-compromiso'>Generar Compromiso Anual</a>
                                <?php }elseif($cabecera->CompromisoAnual==1 && $cabecera->CompromisoAdministrativo!=1){?>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-success generar-compromiso-administrativo'>Generar Compromiso Administrativo</a>
                                <?php }elseif($cabecera->CompromisoAdministrativo==1){?>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-primary generar-devengado'>Generar Devengado</a>
                                <?php } ?>
                            <?php } ?>

                            <?php if($cabecera->Situacion == 3 && ($requerimiento->TipoRequerimiento == 6) ){ ?>
                                <?php if($cabecera->CompromisoAnual!=1 ){?>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-primary generar-compromiso'>Generar Compromiso Anual</a>
                                <?php }elseif($cabecera->CompromisoAnual==1 && $cabecera->CompromisoAdministrativo!=1){?>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-success generar-compromiso-administrativo'>Generar Compromiso Administrativo</a>
                                <?php }elseif($cabecera->CompromisoAdministrativo==1){?>
                                <a href='#' data-id='<?php echo $cabecera->ID ?>' data-codigo-proyecto='<?php echo $cabecera->Codigo ?>' class='btn btn-primary generar-devengado'>Generar Devengado</a>
                                <?php } ?>
                            <?php } ?>
                            
                        <?php endif ?>

                        <a href="<?= \Yii::$app->request->BaseUrl ?>/requerimiento/contabilidad" class="btn btn-default">VOLVER</a>
                    </div>

                </div>
            </div>


        </div>
    </div>
</div>

<div id="modal-loading" class="modal fade modal-loading in" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-hidden="false" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content-no-shadow modal-content">
            <div class="text-center"><i class="fa fa-spinner fa-pulse fa-fw" style="color:#16a085;font-size: 100px;"></i></div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-ctrl-observacion" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Recursos</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<?php 

    if(\Yii::$app->user->identity->rol == 13 || \Yii::$app->user->identity->rol == 14){ 
        $requerimiento = Yii::$app->getUrlManager()->createUrl('requerimiento/actualizar-requerimiento-contabilidad');
    } 

    $compro = Yii::$app->getUrlManager()->createUrl('compromiso-presupuestal/generar-compromiso-anual');
    $devengado = Yii::$app->getUrlManager()->createUrl('compromiso-presupuestal/generar-devengado');
    $administrativo = Yii::$app->getUrlManager()->createUrl('compromiso-presupuestal/generar-administrativo');
    $ordenes = Yii::$app->getUrlManager()->createUrl('compromiso-presupuestal/situacion-ordenes');
?>



<script>
    $(document).ready(function(){
       $('.fa-search').popover(); 
    });
    $('[data-rel=tooltip]').tooltip();
      


    $('#verifica').click(function(){
        var x = $('#gastos').val();
        var a = $('#gastos').attr('data-id');
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        if(x == ''){
            bootbox.alert('Seleccione un item.');
        }else{
            bootBoxConfirm('Esta seguro que desea registrar la información',function(){
                $.ajax({
                    url: '<?= $requerimiento ?>',
                    type: 'POST',
                    async: false,
                    data: {gastos:x,_csrf: toke,ID:a},
                    success: function (data) {
                        location.reload();
                    },
                    error: function () {
                        _pageLoadingEnd();
                    }
                });
            });
        }
    });



    $(document).on('click','.generar-compromiso',function(){
        var arrVacio = [];
        var ID=$(this).attr('data-id');
        var CodigoProyecto=$(this).attr('data-codigo-proyecto');
        
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';

        $('.check:checked').each(function () {
            var $this = $(this);
            arrVacio.push({
                vacio: 1
            });
        });

        //if(arrVacio.length == 0){
           // bootbox.alert('Debe de selcconaar un item.');
        //}else{
            bootBoxConfirm('Esta seguro de Generar el Compromiso Anual',function(){
                $.ajax({
                    url: '<?= $compro ?>',
                    type: 'POST',
                    async: false,
                    data: {ID:ID,CodigoProyecto:CodigoProyecto,_csrf: toke},
                    success: function (data) {
                        location.reload();
                    },
                    error: function () {
                        _pageLoadingEnd();
                    }
                });
            });
        //}
    });
    
    $(document).on('click','.generar-devengado',function(){
        var ID=$(this).attr('data-id');
        var CodigoProyecto=$(this).attr('data-codigo-proyecto');
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        bootBoxConfirm('Esta seguro de Generar el Devengado',function(){
            $.ajax({
                url: '<?= $devengado ?>',
                type: 'POST',
                async: false,
                data: {ID:ID,CodigoProyecto:CodigoProyecto,_csrf: toke},
                success: function (data) {
                    location.reload();
                },
                error: function () {
                    _pageLoadingEnd();
                }
            });
        });
        //}
    });
    
    $(document).on('click','.generar-compromiso-administrativo',function(){
        var ID=$(this).attr('data-id');
        var CodigoProyecto=$(this).attr('data-codigo-proyecto');
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        bootBoxConfirm('Esta seguro de Generar el Compromiso Administrativo',function(){
            $.ajax({
                url: '<?= $administrativo ?>',
                type: 'POST',
                async: false,
                data: {ID:ID,CodigoProyecto:CodigoProyecto,_csrf: toke},
                success: function (data) {
                    //location.reload();
                },
                error: function () {
                    _pageLoadingEnd();
                }
            });
        });
        //}
    });
    
    $('body').on('click', '.check', function (e) {
        // e.preventDefault();
        $(this).attr('checked','checked');
        var ID = $(this).attr('data-id');
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $ordenes ?>',
            type: 'POST',
            async: false,
            data: {ID:ID,_csrf: toke},
            success: function (data) {
                //location.reload();
            },
            error: function () {
                _pageLoadingEnd();
            }
        });

    });

    
</script>