<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
?>

    <div class="modal-body">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label">Objetivo:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <select class="form-control" name="Requerimiento[ComponenteID]" id="requerimiento-componenteid-filtro">
                        <option>[SELECCIONE]</option>
                        <?php foreach($componentes as $componente){ ?>
                            <option value="<?= $componente->ID ?>"><?= $componente->Correlativo ?> <?= $componente->Nombre ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Actividad:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <select class="form-control" name="Requerimiento[ActividadID]" id="requerimiento-actividadid-filtro" onchange="Recurso($(this).val())">
                        
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3">
                    <a class="btn btn-primary seleccionar_poa" >Seleccionar </a>
                </div>
            </div>
            
            <div class="table-responsive">
                <table class="poas display table table-bordered table-striped table-condensed table-hover" cellspacing="0" >
                    <thead>
                        <tr>
                            <th>Código POA</th>
                            <th>Código Matriz</th>
                            <th>Recurso</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
<?php
    $actividades= Yii::$app->getUrlManager()->createUrl('termino-referencia/actividades');
    $recursos=Yii::$app->getUrlManager()->createUrl('termino-referencia/recursos');
    $comp=Yii::$app->getUrlManager()->createUrl('requerimiento/recursos-identificador');
?>
<script>
   
    var tblresultjs1={
        "order": [[ 0, "desc" ]],
        "language": {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta lista",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":    "Último",
                "sNext":    "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        //"destroy": true
    };

    $(document).ready(function() {
        var CodigoProyecto="<?= $CodigoProyecto ?>";
        $.ajax({
            url:'<?php echo Url::toRoute('requerimiento/listado-poa?CodigoProyecto='.$CodigoProyecto.'&ComponenteID=&ActividadID=') ?>',
            async:false,
            data:{},
            beforeSend:function()
            {
                //$('.panel-default').LoadingOverlay("show");
            },
            success:function(result)
            {
                    //$("#poas").destroy();//
                    $(".poas tbody").html(result);
                    $('.poas').DataTable(tblresultjs1);
                    //tblresultjs=$('#example').DataTable(configDTjs);
                    //$('.panel-default').LoadingOverlay("hide",true);
            },
            error:function(){
                //$('.panel-default').LoadingOverlay("hide",true);
                alert('Error al realizar el proceso de busqueda.');
            }
        });
    });
    
    $('body').on('click', '.pintar_poa', function (e) {
        e.preventDefault();
        var id=$(this).attr('id');
        $('.fila').each(function(){
            var td=$(this).children();
            td.removeAttr('style');
        });
        $('.pintar_poa').removeClass('fila');
        $('#'+id).addClass('fila');
        $('#'+id+" td").css('background','yellow');
    });

    $('body').on('change', '#requerimiento-componenteid-filtro', function (e) {
        e.preventDefault();
        var ComponenteID=$(this).val();
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $actividades ?>',
            type: 'POST',
            async: false,
            data: {ComponenteID:ComponenteID,_csrf: toke},
            success: function(data){
               $( "#requerimiento-actividadid-filtro" ).html( data );
            }
        });
        var tblresultjs2={
            "order": [[ 0, "desc" ]],
            "language": {
                "sProcessing":    "Procesando...",
                "sLengthMenu":    "Mostrar _MENU_ registros",
                "sZeroRecords":   "No se encontraron resultados",
                "sEmptyTable":    "Ningún dato disponible en esta lista",
                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":   "",
                "sSearch":        "Buscar:",
                "sUrl":           "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":    "Último",
                    "sNext":    "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            "destroy":true
        };
        
        var CodigoProyecto="<?= $CodigoProyecto ?>";
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: location.origin+'/pip2/web/requerimiento/listado-poa?CodigoProyecto='+CodigoProyecto+'&ComponenteID='+$(this).val()+'&ActividadID='+$('#requerimiento-actividadid-filtro').val()+'&_csrf='+toke,
            async:false,
            success:function(result)
            {
                    // $(".poas").destroy();//
                    $('.poas').DataTable(tblresultjs2).destroy();
                    // console.log(result);
                    //$(".poas tbody").html('');
                    $(".poas tbody").html(result);
                    $('.poas').DataTable(tblresultjs2);
                    
                    //tblresultjs=$('#example').DataTable(configDTjs);
                    $('.panel-default').LoadingOverlay("hide",true);
            }
        });
    });

    $('body').on('change', '#requerimiento-actividadid-filtro', function (e) {
        e.preventDefault();
        
        var tblresultjs2={
            "order": [[ 0, "desc" ]],
            "language": {
                "sProcessing":    "Procesando...",
                "sLengthMenu":    "Mostrar _MENU_ registros",
                "sZeroRecords":   "No se encontraron resultados",
                "sEmptyTable":    "Ningún dato disponible en esta lista",
                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":   "",
                "sSearch":        "Buscar:",
                "sUrl":           "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":    "Último",
                    "sNext":    "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            "destroy":true
        };
        
        var CodigoProyecto="<?= $CodigoProyecto ?>";
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: location.origin+'/pip2/web/requerimiento/listado-poa?CodigoProyecto='+CodigoProyecto+'&ComponenteID=&ActividadID='+$(this).val()+'&_csrf='+toke,
            async:false,
            success:function(result)
            {
                    // $(".poas").destroy();//
                    $('.poas').DataTable(tblresultjs2).destroy();
                    // console.log(result);
                    //$(".poas tbody").html('');
                    $(".poas tbody").html(result);
                    $('.poas').DataTable(tblresultjs2);
                    
                    //tblresultjs=$('#example').DataTable(configDTjs);
                    $('.panel-default').LoadingOverlay("hide",true);
            }
        });
    });
    
    
    function Actividad(valor) {
        var ComponenteID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $actividades ?>',
            type: 'POST',
            async: false,
            data: {ComponenteID:ComponenteID,_csrf: toke},
            success: function(data){
               $( "#requerimiento-actividadid-filtro" ).html( data );
            }
        });
        var tblresultjs2={
            "order": [[ 0, "desc" ]],
            "language": {
                "sProcessing":    "Procesando...",
                "sLengthMenu":    "Mostrar _MENU_ registros",
                "sZeroRecords":   "No se encontraron resultados",
                "sEmptyTable":    "Ningún dato disponible en esta lista",
                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":   "",
                "sSearch":        "Buscar:",
                "sUrl":           "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":    "Último",
                    "sNext":    "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            "destroy":true
        };
        

        var CodigoProyecto="<?= $CodigoProyecto ?>";
        $.ajax({
            url:'requerimiento/listado-poa?CodigoProyecto='+CodigoProyecto+'&ComponenteID='+$('#requerimiento-componenteid-filtro').val()+'&ActividadID='+$('#requerimiento-actividadid-filtro').val(),
            async:false,
            success:function(result)
            {
                    //$("#poas").destroy();//
                    //$('.poas').DataTable(tblresultjs1).destroy();
                    console.log(result);
                    
                    $(".poas tbody").html(result);
                    $('.poas').DataTable(tblresultjs1);
                    
                    //tblresultjs=$('#example').DataTable(configDTjs);
                    $('.panel-default').LoadingOverlay("hide",true);
            }
        });
    }
    
    function Recurso(valor) {
        var ActividadID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $recursos ?>',
            type: 'POST',
            async: false,
            data: {ActividadID:ActividadID,_csrf: toke},
            success: function(data){
               $( "#requerimiento-areasubcategoriaid" ).html( data );
            }
        });
    }


</script>