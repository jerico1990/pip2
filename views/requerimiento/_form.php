<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>


<?php $form = ActiveForm::begin(
    [
        'action'            => 'requerimiento/crear?CodigoProyecto='.$CodigoProyecto,
        'options'           => [
            'enctype'       =>'multipart/form-data',
            'id'            =>'frmRequerimientos'
        ]
    ]
); ?>
    <input type="hidden" name="Requerimiento[CodigoProyecto]" value="<?= $CodigoProyecto ?>">
    <div class="modal-body">
        <div class="form-horizontal">
            <div class="tab-container">
                <ul class="nav nav-tabs">
                    <li id="li-poaf" class="active"><a href="#tab-informacion-general" data-toggle="tab"><i class="fa fa-home"></i>&nbsp;Información General</a></li>
                    <li id="li-poaf" class="detallet"><a href="#tab-detalle" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Detalle</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-informacion-general">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tipo Requerimiento:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <select type="text" class="form-control" name="Requerimiento[TipoRequerimiento]" id="tipo-requerimiento" onchange="Tipo(this)" required>
                                    <option value="">[SELECCIONE]</option>
                                    <?php foreach ($tipoGasto as $value): ?>
                                        <option value="<?php echo $value->ID ?>"><?php echo $value->Nombre ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Referencia:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="Requerimiento[Referencia]" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Asunto:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="Requerimiento[Asunto]" required>
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <label class="col-sm-3 control-label">Fecha de Solicitud:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control datepicker" name="Requerimiento[FechaSolicitud]" required>
                            </div>
                        </div> -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label">CUT:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="Requerimiento[CUT]">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Descripción:<span class="f_req">*</span></label>
                            <div class="col-sm-9">
                                <textarea  class="form-control" id="descripcion" name="Requerimiento[Descripcion]" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Documento:</label>
                            <div class="col-sm-9">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="input-group">
                                        <div class="form-control uneditable-input" data-trigger="fileinput">
                                            <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                            <span class="fileinput-filename">
                                            Seleccione archivo...
                                            </span>
                                        </div>
                                        <span class="input-group-addon btn btn-default btn-file">
                                            <span class="fileinput-new">Seleccionar</span>
                                            <span class="fileinput-exists">Cambiar</span>
                                            <input type="file" onchange="return Imagen(this);" id="file-cv-administrativo" name="Requerimiento[Archivo]">
                                        </span>
                                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Eliminar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-detalle">
                        <div class="form-group">
                            <div class="col-sm-3">
                                <a class="btn btn-primary" id="agregar-poa">Agregar POA</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12" style="max-width: 1200px !important;overflow-x: auto ">
                                <table class="table borderless table-hover tbl-act" id="detalle">
                                    <thead>
                                        <tr class="tr-header">
                                            <th width="250">Objetivo</th>
                                            <th width="250">Actividad</th>
                                            <th width="250">Recurso</th>
                                            <th width="100">Código Matriz</th>
                                            <th width="250">Descripción</th>
                                            <th width="50">Cantidad</th>
                                            <th width="50">P. Unitario</th>
                                            <th width="50">Total</th>
                                            <th width="22"><em class="fa fa-cog"></em></th>
                                        </tr>
                                    </thead>
                                    <tbody id="detalle_poa">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary" id="btn-guardar-requerimientos"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>


<style type="text/css">
    .none{
        display: none;
    }
    .block{
        display: block;
    }

    .ui-draggable, .ui-droppable {
      background-position: top;
    }
    .ui-widget-content{
        position: relative;
        z-index: 9999;
        width: 400px !important;
    }

    .tbl-act > thead > .tr-header > th {
        text-align: center;
        font-size: 12px;
        font-weight: bold;
        padding: 5px !important;
        vertical-align: middle !important;
        /*border: 1px solid #cfcfd0;
        background-color: #f0f0f1;*/
        border: 1px solid #c0c0c0;
        background-color: #e3e3e3;
    }

</style>



<script>
    var $form = $('#frmRequerimientos');
    var formParsleyRequerimientos = $form.parsley(defaultParsleyForm());
    $('.datepicker').datepicker(defaultDatePicker());
    function Tipo(elemento) {
        console.log($(elemento).val());
        if ($(elemento).val()=='5') {
            $('.detallet').hide();
        }
        else
        {
            $('.detallet').show();
        }
    }
</script>

