<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay_progress.min.js"></script>
<script src=" //cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<div id="page-content" style="min-height: 1342px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Requerimiento</h1>
        </div>
        <div class="container">
            <div class="form-group">
                <button class="btn btn-primary btn-crear-requerimiento" data-codigo-proyecto="<?= $CodigoProyecto ?>">Generar nuevo requerimiento</button>
                <!--<a class="btn " href="documentos/0_OFICIO_MODELO_REQUERIMIENTO.docx">Descargar plantilla</a>-->
            </div>
            <div class="table-responsive">
                <table id="proyectos" class="display table table-bordered table-striped table-condensed table-hover" cellspacing="0" >
                    <thead>
                        <tr>
                            <th>Tipo Requerimiento</th>
                            <th>Requerimiento</th>
                            <th>Referencia</th>
                            <th>Fecha Solicitud</th>
                            <th>Situación</th>
                            <th>Documento adjunto</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <a class="btn" href="<?= Yii::$app->getUrlManager()->createUrl('requerimiento/excel?CodigoProyecto='.$CodigoProyecto.'') ?>">Descargar Excel</a>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-ctrl-requerimiento" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Requerimiento (<?php echo date('d-m-Y') ?>)</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-ctrl-upload" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Adjuntar</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-ctrl-poa" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">POA</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>
    
<?php 
$requerimiento = Yii::$app->getUrlManager()->createUrl('requerimiento/actualizar-requerimiento-listado');
$eliminarDetalle = Yii::$app->getUrlManager()->createUrl('requerimiento/eliminar-detalle');
?>
    
<script>
    
    function js_buscar()
    {
       
    }
    var tblresultjs;
    var configDTjs={
        "order": [[ 1, "desc" ]],
            "language": {
                "sProcessing":    "Procesando...",
                "sLengthMenu":    "Mostrar _MENU_ registros",
                "sZeroRecords":   "No se encontraron resultados",
                "sEmptyTable":    "Ningún dato disponible en esta lista",
                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":   "",
                "sSearch":        "Buscar:",
                "sUrl":           "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":    "Último",
                    "sNext":    "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
    };
    $(document).ready(function() {
        var CodigoProyecto="<?= $CodigoProyecto ?>";
        $.ajax({
            url:'requerimiento/lista-requerimientos?CodigoProyecto='+CodigoProyecto,
            async:false,
            data:{},
            beforeSend:function()
            {
                $('.panel-default').LoadingOverlay("show");
            },
            success:function(result)
            {
                    //$tblresultjs.destroy();//
                    $("#proyectos tbody").html(result);
                    $('#proyectos').DataTable(configDTjs);
                    //tblresultjs=$('#example').DataTable(configDTjs);
                    $('.panel-default').LoadingOverlay("hide",true);
            },
            error:function(){
                $('.panel-default').LoadingOverlay("hide",true);
                alert('Error al realizar el proceso de busqueda.');
            }
        });
    } );
    

    $(document).on('ready',function(){
        // validarNumeros();
    })
    
    $('body').on('click', '.btn-crear-requerimiento', function (e) {
        e.preventDefault();
        var CodigoProyecto = $(this).attr('data-codigo-proyecto');
        $('#modal-ctrl-requerimiento .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/requerimiento/crear?CodigoProyecto='+CodigoProyecto);
        $('#modal-ctrl-requerimiento').modal('show');
    });
    
    $('body').on('click', '#btn-guardar-requerimientos', function (e) {
        e.preventDefault();
        var $btn = $(this);
        var poas = $('#detalle tbody tr').length;
        var tiporequerimiento=$('#tipo-requerimiento').val();
        if (tiporequerimiento==5) {
            poas=1;
        }
        $btn.attr('disabled', 'disabled');

        // alert(poas)
        // return false;
        var isValid = formParsleyRequerimientos.validate();
        if (isValid) {
            
            if(poas != 0){
                $('#frmRequerimientos').submit();      
            }else{
                bootbox.alert('Debe de ingresar un POA');
            }
        }else{
            $btn.removeAttr('disabled');
        }
        $btn.removeAttr('disabled');
    });
    
    $('body').on('click', '#btn-guardar-orden', function (e) {
        e.preventDefault();
        var $btn = $(this);
        $btn.attr('disabled', 'disabled');
        var isValid = formParsleyOrden.validate();
        if (isValid) {
            
        }else{
            $btn.removeAttr('disabled');
        }
        $btn.removeAttr('disabled');
    });
    
    $('body').on('click', '.btn-retraer', function (e) {
        e.preventDefault();
        var $this = $(this);
        bootboxConfirmEliminar($this,
        'Está seguro de retraer el requerimiento?',
        function () {
            var id = $this.attr('data-id');
            var toke = '<?=Yii::$app->request->getCsrfToken()?>';
            $.get('<?= \Yii::$app->request->BaseUrl ?>/requerimiento/retraer', { id: id ,_csrf: toke }, function (data) {
                var jx = JSON.parse(data);
                if (jx.Success == true) {
                    location.reload();
                }
            });
        });
    });
    $('body').on('click', '.btn-remove', function (e) {
        e.preventDefault();
        var $this = $(this);
        bootboxConfirmEliminar($this,
        'Está seguro de eliminar el requerimiento?',
        function () {
            var id = $this.attr('data-id');
            var toke = '<?=Yii::$app->request->getCsrfToken()?>';
            $.get('<?= \Yii::$app->request->BaseUrl ?>/requerimiento/eliminar', { id: id ,_csrf: toke }, function (data) {
                var jx = JSON.parse(data);
                if (jx.Success == true) {
                    location.reload();
                }
            });
        });
    });

    $('body').on('click', '.btn-upload', function (e) {
        e.preventDefault();
        var ID = $(this).attr('data-id');
        $('#modal-ctrl-upload .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/requerimiento/adjuntar-documento?ID='+ID);
        $('#modal-ctrl-upload').modal('show');
    });
    
    $('body').on('click', '.btn-edit-requerimiento', function (e) {
        e.preventDefault();
        var id=$(this).attr('data-id');
        $('#modal-ctrl-requerimiento .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/requerimiento/actualizar?ID='+id);
        $('#modal-ctrl-requerimiento').modal('show');
    });
    
    $('body').on('click', '#agregar-poa', function (e) {
        e.preventDefault();
        var CodigoProyecto="<?= $CodigoProyecto ?>";
        AjaxSendForm('<?= \Yii::$app->request->BaseUrl ?>/requerimiento/poa', "#modal-ctrl-poa .modal-body-main", {'CodigoProyecto':CodigoProyecto}, false);

        // $('#modal-ctrl-poa .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/requerimiento/poa?CodigoProyecto='+CodigoProyecto);
        
        $('#modal-ctrl-poa').modal('show');
    });
    
    $('body').on('click', '.btn-requerimiento-ver', function (e) {
        e.preventDefault();
        var id=$(this).attr('data-id');
        $('#modal-ctrl-requerimiento .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/requerimiento/ver?ID='+id);
        $('#modal-ctrl-requerimiento').modal('show');
    });
    
    detalle=1;
    $('body').on('click', '.seleccionar_poa', function (e) {
        e.preventDefault();
        //console.log(jx.metafisica);
        $('.fila').each(function(){
            var id=$(this).attr('data-id');
            var componenteid=$(this).attr('data-componente');
            var actividadid=$(this).attr('data-actividad');
            var rubroelegibleid=$(this).attr('data-rubro-elegible');
            
            var toke = '<?=Yii::$app->request->getCsrfToken()?>';
            $.get('<?= \Yii::$app->request->BaseUrl ?>/requerimiento/get-dato-poa', { id: id ,componenteid:componenteid,actividadid:actividadid,rubroelegibleid:rubroelegibleid,_csrf: toke }, function (data) {
                var jx = JSON.parse(data);
                console.log(jx);
                if (jx.Success == true) {
                    if(jx.metafisica == 0){
                        bootbox.alert('La meta fisica programada del recurso, ya esta completa');
                    }else{
                        $('#requerimiento-componenteid').val(jx.componente)
                        $('#requerimiento-actividadid').val(jx.actividad)
                        $('#requerimiento-areasubcategoriaid').val(jx.recurso)
                        $('#requerimiento-codigo').val(jx.codematriz)
                        $('#requerimiento-componenteid-c').val(jx.componenteid)
                        $('#requerimiento-actividadid-c').val(jx.actividadid)
                        $('#requerimiento-areasubcategoriaid-c').val(jx.aresubcategoriaid)
                        $('#requerimiento-codigo-c').val(jx.codematriz)
                        //var validador=true;
                        // var validador=parseFloat(VerificarDuplicado(jx.aresubcategoriaid)).toFixed(2);
                        var validador=parseFloat(VerificarDuplicado(jx.aresubcategoriaid));
                        console.log(validador);
                        var gtml ='<tr>'+
                                '<td width="250">'+
                                    '<input class="form-control" type="hidden" value="'+jx.componente+'" name="Requerimiento[DetallesObjetivos][]">'+
                                    '<input class="form-control" type="hidden" value="'+jx.actividad+'" name="Requerimiento[DetallesActividades][]">'+
                                    '<input class="form-control" type="hidden" value="'+jx.recurso+'" name="Requerimiento[DetallesRecurs][]">'+
                                    '<input class="form-control" type="hidden" value="'+jx.codematriz+'" name="Requerimiento[DetallesCodigos][]">'+
                                    '<input class="form-control codigo_poa" type="hidden" value="'+jx.aresubcategoriaid+'" name="Requerimiento[DetallesRecursos][]">'+
                                    '<input class="form-control" type="hidden" value="'+jx.codificacion+'" name="Requerimiento[DetallesCodificaciones][]">'+
                                    '<input class="form-control" style="width:150px"  type="text" value="'+jx.componente+'" disabled>'+
                                '</td>'+
                                '<td width="250">'+
                                    '<input class="form-control" style="width:150px" type="text" value="'+jx.actividad+'" disabled>'+
                                '</td>'+
                                '<td>'+
                                    '<input class="form-control" style="width:150px" type="text" value="'+jx.recurso+'" disabled>'+
                                '</td>'+
                                '<td>'+
                                    '<input class="form-control " style="width:150px" type="text" value="'+jx.codematriz+'" disabled>'+
                                '</td>'+
                                '<td>'+
                                    '<input class="form-control" style="width:150px" type="text" name="Requerimiento[DetallesDescripciones][]" required>'+
                                '</td>'+
                                '<td class="cant">'+
                                    '<input class="form-control input-number hijo" style="width:150px" type="text" name="Requerimiento[DetallesCantidades][]" onkeyup="Cantidades(this,$(this).val(),'+detalle+','+(jx.metafisica-validador)+')" id="cantidades_'+detalle+'" value="'+(jx.metafisica-validador)+'"  required>'+
                                '</td>'+
                                '<td>'+
                                    '<input class="form-control input-number" style="width:150px" type="text" name="Requerimiento[DetallesPreciosUnitarios][]" onkeyup="PreciosUnitarios(this,$(this).val(),'+detalle+','+jx.preciounitario+')" id="preciosunitarios_'+detalle+'" value="'+jx.preciounitario+'" required>'+
                                '</td>'+
                                '<td>'+
                                    '<input class="form-control input-number" style="width:150px"type="text" id="preciostotales_'+detalle+'" value="'+((jx.metafisica-validador)*jx.preciounitario)+'" disabled>'+
                                '</td>'+
                                '<td>'+
                                    '<a href="javascript:;" class="eliminar"><i class="fa fa-times fa-2x" aria-hidden="true"></i></a>'+
                                '</td>'+
                            '</tr>';
                        
                        //if (validador) {
                            $('#detalle_poa').append(gtml);
                            detalle++;
                        //}

                        $(jx.resultado).each(function (index, value){
                            var ht = '<tr>';
                            ht += '<td>'+value.CorrelaCompr+'.'+value.CorrelaActv+'.'+value.RubroElegibleID+'</td>';
                            ht += '<td>'+value.RubroElegibleID+'.'+value.Codificacion+'</td>';
                            ht += '<td>'+value.Nombre+'</td>';
                            ht += '<td>'+value.Especifica+'</td>';
                            ht += '<td>'+value.CostoUnitario+'</td>';
                            ht += '<td>'+value.MetaFisica+'</td>';
                            ht += '<tr>';
                            $('.conten').append(ht);
                        });

                        validarNumeros();
                    }
                }
            });
            $('#modal-ctrl-poa').modal('hide');
        });
    });
    
    function VerificarDuplicado(codigo) {
        validar=false;
        var total=0;
        console.log(codigo);
        $('.codigo_poa').each(function(){
            if ($(this).val()==codigo) {
                //bootbox.alert('Ya ha seleccionado el recurso');
                validar=true;

                total=total+parseFloat($(this).parent().siblings('.cant').find('.hijo').val());
            }
        });
        return total;
    }
    
    $('body').on('click', '.eliminar', function (e) {
        var r = confirm("Estas seguro de Eliminar?");
        var mensaje = '';
        if (r == true) {
            id=$(this).children().val();
            console.log(id);
            
            if (id) {
                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                $.ajax({
                    url: '<?= $eliminarDetalle ?>',
                    type: 'POST',
                    async: false,
                    data: {id:id,_csrf: toke,},
                    success: function (data) {
                        
                    },
                    error: function () {
                        _pageLoadingEnd();
                    }
                });
                $(this).parent().parent().remove();
            }
            else
            {
                $(this).parent().parent().remove();    
            }   
            mensaje = "Se elimino el Registro Correctamente";
        } 
    });
    
    $(document).on('click','.enviar-requerimiento',function(){
        var a = $(this).attr('data-id');
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        bootBoxConfirm('Esta seguro que desea enviar el requerimiento',function(){
            $.ajax({
                url: '<?= $requerimiento ?>',
                type: 'POST',
                async: false,
                data: {_csrf: toke,ID:a},
                success: function (data) {
                    location.reload();
                },
                error: function () {
                    _pageLoadingEnd();
                }
            });
        });
    });

    function Cantidades(elemento,valor,correlativo,techo) {
        if (techo<valor) {
            alert("El valor ingresado supera, a la meta fisica programada, deberia solicitar reprogramación de POA");
            $(elemento).val('');
        }
        
        var precio=$('#preciosunitarios_'+correlativo).val()
        if (precio=='') {
            precio=0;
        }
        $('#preciostotales_'+correlativo).val(getNum(precio)*valor);
        return true;
    }
    
    function PreciosUnitarios(elemento,valor,correlativo,techo) {
        var max=(techo*20/100)+techo;
        if (max<getNum(valor)) {
            alert("El precio unitario ingresado supera al monto total, deberia solicitar reprogramación de POA");
            $(elemento).val('');
        }
        
        var cantidad=$('#cantidades_'+correlativo).val()
        if (cantidad=='') {
            cantidad=0;
        }
        $('#preciostotales_'+correlativo).val(cantidad*getNum(valor));
        return true;
    }

    function getNum(val) {
       if (val == '0.00' || val == '') {
            return 0;
       }
       var val2 = val.replace(',','');
       return val2;
    }


    var i = 0;
    function AjaxSendForm(url, placeholder,data, append) {
        // var data = $(form).serialize();
        append = (append === undefined ? false : true); 
        $.ajax({
            type: 'GET',
            url: url,
            data: data,
            beforeSend: function() {
                // setting a timeout
                $(placeholder).html("");
                $(placeholder).append('<div class="loading"><i class="fa fa-spinner fa-pulse fa-5x fa-fw"></i></div>');
                i++;
            },
            success: function(data) {
                $(placeholder).html("");
                if (append) {
                    $(placeholder).append(data);
                } else {
                    $(placeholder).html(data);
                }
            },
            error: function(xhr) { // if error occured
                alert("Error occured.please try again");
                $(placeholder).append(xhr.statusText + xhr.responseText);
            },
            complete: function() {
                i--;
                if (i <= 0) {
                    $(".loading").remove();
                }
            },
            dataType: 'html'
        });
    }

</script>

<style type="text/css">
    .none{
        display: none;
    }
    .block{
        display: block;
    }

    .modal-body-main .loading{
        text-align: center;
    }

    .loading{
        margin: 40px;
    }
</style>
