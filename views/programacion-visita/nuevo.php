<?php
use yii\helpers;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<?php if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start(); ?>
<link href="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
<script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/bootstrap-table/bootstrap-table.min.js"></script>
<script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/bootstrap-table/bootstrap-table-es-SP.min.js"></script>
<div id="page-content" style="min-height: 885px;">
    <div id="wrap">
        <div id="page-heading">
            <h1 id="h1Titulo"></h1>
        </div>
        <div class="container">
            <div class="panel panel-primary">
                <div id="Divnotificaciones"></div>
                <div class="panel-body">

                    <?php $form = ActiveForm::begin(
                        [
                            'action'            => '',
                            'options'           => [
                                'enctype'       => 'multipart/form-data',
                                'id'            => 'frmEjecucion',
                                'class'         => 'form-horizontal'
                            ]
                        ]
                    ); ?>    
                        
                        <div class="form-group div-label-left">
                            <div class="col-sm-6">
                                <b>Datos de la Estacion </b> 
                                &nbsp;&nbsp;<a href="#btnEditProgramacionTecnica" id="btnEditProgramacionTecnica" style="display:none;"><span class=" fa fa-pencil fa-lg"></span>&nbsp;Cambiar</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label">Estaci&oacute;n:<span class="f_req">*</span></label>
                            <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
                                <input type="text" class="form-control input-sm" id="programacion-proyecto" readonly="readonly" value=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div id="tblProyectosToolbar">
                                    <p class="help-block">A continuacion indique los proyectos que visitara..</p>
                                </div>
                                <table id="tblProyectos" data-height="250">
                                    <thead>
                                            <tr>
                                                <th data-field="Item" data-width="35" data-align="center">N°</th>
                                                <th data-width="35" data-checkbox="true"></th>
                                                <th data-field="CodigoProyecto" data-width="150" data-align="left">Proyecto</th>
                                                <th data-field="Titulo" data-align="left">Titulo</th>
                                            </tr>
                                        </thead>
                                </table>
                            </div>
                        </div>
                        <div class="form-group div-label-left row-border">
                            <div class="col-sm-6">
                                <b>Datos de la visita</b>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label">Mes / Año:<span class="f_req">*</span></label>
                            <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
                                <select class="form-control input-sm" id="visita-mes" required>
                                    <option value="">[SELECCIONE]</option>
                                    <option value="1">Enero</option>
                                    <option value="2">Febrero</option>
                                    <option value="3">Marzo</option>
                                    <option value="4">Abril</option>
                                    <option value="5">Mayo</option>
                                    <option value="6">Junio</option>
                                    <option value="7">Julio</option>
                                    <option value="8">Agosto</option>
                                    <option value="9">Setiembre</option>
                                    <option value="10">Octurbe</option>
                                    <option value="11">Noviembre</option>
                                    <option value="12">Diciembre</option>
                                </select>
                            </div>
                            <div class="col-xs-6 col-sm-2 col-md-2 col-lg-2">
                                <input type="text" class="form-control input-sm" id="visita-anio" required value=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label">Asunto:<span class="f_req">*</span></label>
                            <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                                <input type="text" class="form-control input-sm" id="visita-asunto" required value=""/>
                            </div>
                        </div>
                        <div class="form-group div-label-left row-border" style="margin-bottom: 0 !important; padding-bottom:  0 !important;">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                                <button id="btnGuardar" class="btn btn-default btn-primary" type="button"><span class=" fa fa-floppy-o"></span>&nbsp;Guardar</button>  
                                &nbsp;
                                &nbsp;
                                <a class="btn btn-default" href="<?= Yii::$app->getUrlManager()->createUrl('programacion-visita'); ?>"><span class=" fa fa-ban"></span>&nbsp;Cancelar</a>
                            </div>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-buscar-programacion" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Seleccionar programaci&oacute;n</h4>
            </div>
            <div class="modal-body">
                <div id="tblProgramados_tool" class="alert alert-info text-left" style="margin-bottom:0px; padding: 0px 9px;">
                    <strong>Doble clic</strong> para seleccionar el registro
                </div>
                <table id="tblProgramados" data-height="350">
                    <thead>
                            <tr>
                                <th data-field="Item" data-width="35" data-align="center">N°</th>
                                <th data-field="Estacion" data-align="left">Estaci&oacute;n</th>
                            </tr>
                        </thead>
                </table>
            </div>
            <div class="modal-footer" style="margin-top: 0px;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var formParsleyEjecucion = $('#frmEjecucion').parsley(defaultParsleyForm2());
    window.tblSelProgramados_events = {
        'click .tblSelProgramados-eliminar': function (e, value, row, index) {
            
        }
    };
    function tblSelProgramados_item(value, row, index) {
        return (index + 1);
    }
    function tblSelProgramados_eliminar(value, row, index) {
        return '<a class="tblSelProgramados-eliminar" href="#"><span class="fa fa-trash"></span></a>';
    }

    $(document).ready(function(){
        $('#tblSelProgramados').bootstrapTable({
            cache: false,
            striped: true,
            pagination: false,
            search: false,
            classes: 'table table-condensed table-hover'
        });
        $('#tblProyectos').bootstrapTable({
            cache: false,
            striped: false,
            pagination: false,
            search: false,
            classes: 'table table-condensed table-hover',
            clickToSelect: true,
            toolbar: '#tblProyectosToolbar'
        });

        <?php if(/*$EjecucionTecnica['IdEjecucionTecnica']*/-1 == -1){ ?>
            $('#h1Titulo').text('Programacion visita (Nuevo)');
            $('#modal-buscar-programacion').modal('show');
            $('#btnEditProgramacionTecnica').show().click(function(){
                $('#modal-buscar-programacion').modal('show');
                cargarEventosNoEjecutados();
            });
            $('#modal-buscar-programacion').modal({ backdrop:'static', keyboard:false, show:false });
            $('#tblProgramados').bootstrapTable({
                cache: false,
                striped: true,
                pagination: false,
                search: true,
                classes: 'table table-condensed table-hover',
                toolbar: '#tblProgramados_tool',
                onDblClickRow: function(row, elementHtml, field){
                    //seleccionaProgramacion(row['IdProgramacionTecnica']);
                    seleccionaProgramacion(row);
                    $('#tblProgramados').bootstrapTable('remove', { field:'IdProgramacionTecnica', values: [ row['IdProgramacionTecnica'] ] });
                    $('#modal-buscar-programacion').modal('hide');
                }
            });
            $('#tblDocumentosSustento tbody').empty();
            cargarEventosNoEjecutados();
        <?php } else { ?>
            $('#h1Titulo').text('Programacion visita (Edición)');
            $('#btnEditProgramacionTecnica, #modal-buscar-programacion').remove();
            /*$.ajax({
                url: '<?= Yii::$app->getUrlManager()->createUrl('/ejecucion-evento-new/lista-sustentos-json'); ?>',
                type: 'POST',
                async: false,
                data: { 
                    'IdEjecucionTecnica': $('#hddIdEjecucion').val(), 
                    _csrf: '<?=Yii::$app->request->getCsrfToken()?>' 
                },
                success: function(result){
                    result = JSON.parse(result);
                    lista_archivos_sustento(result, '#tblDocumentosSustento');
                }
            });*/
        <?php } ?>
/*
        $('#evento-titulo, #evento-descripcion, #evento-lugar, #evento-observacion').filter_input_alfanumericos_validate();
        $('#evento-fechainicio, #evento-fechafin').filter_input_fecha_validate();
*/
        $('#btnGuardar').click(function(){
            if(formParsleyEjecucion.validate()) {    
                $(this).attr('disabled', 'disabled');

                var datos = { 
                    EjecucionTecnica: { 
                        'IdEjecucionTecnica': $('#hddIdEjecucion').val(),
                        'ResponsableTecnico': $('#evento-responsable').val(),
                        'DuracionHoras': $('#evento-duracion').val(),
                        'BeneficiarioHombres': $('#evento-benhombres').val(),
                        'BeneficiarioMujeres': $('#evento-benmujeres').val(),
                        'Material': $('#evento-material').val(),
                        'Metodo': $('#evento-metodo').val(),
                        'Resultado': $('#evento-resultado').val(),
                        'Costo': $('#evento-costos').val(),

                        'IdProgramacionTecnica': $('#hddIdProgramacion').val(),
                        'Tipologia':$('#evento-tipologia').val(),
                        'ActividadID':$('#evento-actividadid').val(),
                        'Objetivo':$('#evento-componenteid').val(),
                        'LugarDesarrollo': $('#evento-lugar').val(),
                        'IntroduccionDescripcion': $('#evento-introduccion').val(),
                        'FechaInicio': $('#evento-fechainicio').datepicker('getFormattedDate','yyyy-mm-dd'),
                        'FechaFin': $('#evento-fechafin').datepicker('getFormattedDate','yyyy-mm-dd')
                    },
                    _csrf: $('input:hidden[name="_csrf"]').val()
                };

                $.ajax({
                    url: '<?= Yii::$app->getUrlManager()->createUrl('ejecucion-evento-new/registrar'); ?>',
                    data: datos,
                    type: 'POST',
                    beforeSend:function(){
                    },
                    success:function(result)
                    {
                        result = JSON.parse(result);
                        //console.log('1: eliminamos los archivos nuevos');
                        if($('#hddDeleted', $('#tblDocumentosSustento')).val() != ''){
                            $.ajax({
                                url: '<?= Yii::$app->getUrlManager()->createUrl('ejecucion-evento-new/eliminar-sustentos-json'); ?>',
                                data: {
                                    _csrf: $('input:hidden[name="_csrf"]').val(),
                                    IdsSustentos: $('#hddDeleted', $('#tblDocumentosSustento')).val() + ','
                                },
                                type: 'post',
                                cache: false,
                                async: false,
                                beforeSend:function(){
                                    
                                },
                                success:function(result){
                                    
                                },
                                error:function(){
                                    alert('Error al realizar el proceso.');
                                },
                                complete: function (){
                                    
                                }
                            });
                        }
                        //console.log('2: guardar los archivos nuevos');
                        $.each($('input:file[name="Sustento"]'), function(i, element){
                            console.dir(element.files);
                            if(element.files.length > 0){
                                datos = new FormData();
                                datos.append('_csrf', $('input:hidden[name="_csrf"]').val());
                                datos.append('IdEjecucionTecnica', $('#hddIdProgramacion').val());
                                datos.append('CodigoProyecto', $('#hddCodigoProyecto').val());
                                datos.append('Sustento', element.files[0]);
                               
                                $.ajax({
                                    url: '<?= Yii::$app->getUrlManager()->createUrl('ejecucion-evento-new/registrar-sustento'); ?>',
                                    data: datos,
                                    type: 'post',
                                    cache: false,
                                    async: false,
                                    contentType: false,
                                    processData: false,
                                    beforeSend:function(){
                                        //console.log('guardando: ', element.files[0].name);
                                    },
                                    success:function(result){
                                        //console.dir(JSON.parse(result));
                                    },
                                    error:function(){
                                        alert('Error al realizar el proceso.');
                                    },
                                    complete: function (){
                                    }
                                });
                            }
                        });
                        
                        $('body').append('<form id="frmBack" method="get" action="<?= Yii::$app->getUrlManager()->createUrl('ejecucion-evento-new'); ?>"></form>');
                        $('#frmBack').submit(); 
                        //console.log('3: fin');
                    },
                    error:function(){
                        alert('Error al realizar el proceso.');
                    },
                    complete: function (){
                        $('#btnGuardar').removeAttr('disabled');
                    }
                });
            }
        });

        $('#btnAddSustento').click(function(){
            $('#tblDocumentosSustento tbody')
                .append(
                    $('<tr>'+'</tr>').append(
                        '<td class="td-img">&nbsp;</td>'
                        ,'<td class="td-nombre">&nbsp;</td>'
                        ,'<td class="tbl-opciones-carga-archivo">'
                            +'<a href="#tblDocumentosSustento" class="btn-seleccionar" onclick="seleccionar(this);"><i class="fa fa-cloud-upload fa-lg"></i> Seleccionar</a>'
                            +'<a href="#tblDocumentosSustento" class="btn-eliminar" onclick="rowRemove(this);"><i class="fa fa-trash-o fa-lg"></i>&nbsp;Eliminar</a>'
                            +'<input name="Sustento" type="file" style="display: none;" onchange="file_selected(this);"/>'
                            +'<input type="hidden" id="hddIdAdjunto" name="hddIdAdjunto" value="-1" />'
                        +'</td>'
                    )
                );            
        });
    });
    

    function cargarEventosNoEjecutados(){
        var data = [
            {"Item":"1",'Estacion': "EEA Donoso", 'Proyectos': [
                {"Item":"1","CodigoProyecto":"088_PI","CodigoPOA":"3.2", "Titulo":"Decodificando el genoma del Maiz Morado Zea Mays L. Etapa I"}
                ,{"Item":"2","CodigoProyecto":"089_PI","CodigoPOA":"3.2", "Titulo":"Decodificando el genoma del Maiz Morado Zea Mays L. Etapa II"}
                ,{"Item":"3","CodigoProyecto":"090_PI","CodigoPOA":"3.2", "Titulo":"Decodificando el genoma del Maiz Morado Zea Mays L. Etapa III"}] 
            },
            {"Item":"1",'Estacion': "EEA Baños del Inca", 'Proyectos': [
                {"Item":"1", "CodigoProyecto":"116_PI","CodigoPOA":"3.2", "Titulo":"Decodificando el genoma del Maiz Morado Zea Mays L. Etapa I"}
                ,{"Item":"2","CodigoProyecto":"117_PI","CodigoPOA":"3.2", "Titulo":"Decodificando el genoma del Maiz Morado Zea Mays L. Etapa II"}
                ,{"Item":"3","CodigoProyecto":"118_PI","CodigoPOA":"3.2", "Titulo":"Decodificando el genoma del Maiz Morado Zea Mays L. Etapa III"}] 
            },
            {"Item":"1",'Estacion': "EEA Pichanaki", 'Proyectos': [
                {"Item":"1","CodigoProyecto":"088_PI","CodigoPOA":"3.2", "Titulo":"Fortalecimiento de capacidades tÃ©cnicas en manejo integrado del cultivo de mango de exportaciÃ³n en la Region Piura. Etapa I"}
                ,{"Item":"2","CodigoProyecto":"089_PI","CodigoPOA":"3.2", "Titulo":"Fortalecimiento de capacidades tÃ©cnicas en manejo integrado del cultivo de mango de exportaciÃ³n en la Region Piura. Etapa II"}
                ,{"Item":"3","CodigoProyecto":"090_PI","CodigoPOA":"3.2", "Titulo":"Fortalecimiento de capacidades tÃ©cnicas en manejo integrado del cultivo de mango de exportaciÃ³n en la Region Piura. Etapa III"}] 
            }
        ];
        //console.dir(data);
        $('#tblProgramados').bootstrapTable('load', data);

        /*$.ajax({
            url:'<?= \Yii::$app->request->BaseUrl ?>/programacion-evento-new/lista-eventos-no-ejecutados-json',
            cache:false,
            data:{},
            contentType: "application/json",
            beforeSend:function()
            {
                $('#tblProgramados').bootstrapTable('showLoading');
            },
            success:function(result)
            {
                result = JSON.parse(result);
                $('#tblProgramados').bootstrapTable('load', result);
            },
            error:function(){   
                alert('Error al realizar el proceso de busqueda.');
            },
            complete: function (){
                $('#tblProgramados').bootstrapTable('hideLoading');
            }
        });*/
    }
    function seleccionaProgramacion(pIdProgramacionTecnica){
        /*var data = $('#tblSelProgramados').bootstrapTable('getData');
        data.push(pIdProgramacionTecnica);
        $('#tblSelProgramados').bootstrapTable('load', data);*/
        $('#programacion-proyecto').val(pIdProgramacionTecnica['Estacion']);
        $('#tblProyectos').bootstrapTable('load', pIdProgramacionTecnica['Proyectos']);
        /*$.ajax({
            url: '<?= Yii::$app->getUrlManager()->createUrl('/programacion-evento-new/consulta-evento-json'); ?>',
            type: 'POST',
            async: false,
            data: { 
                'IdProgramacionTecnica': pIdProgramacionTecnica, 
                _csrf: '<?=Yii::$app->request->getCsrfToken()?>' 
            },
            success: function(result){
                result = JSON.parse(result);
                //console.dir(result);
                $('#hddIdProgramacion').val(result['IdProgramacionTecnica']);
                $('#hddCodigoProyecto').val(result['CodigoProyecto']);
                $('#evento-titulo').val(result['Titulo']);
                $('#evento-componenteid').val(result['ComponenteID']);
                Actividad(result['ComponenteID'], result['ActividadID']);
                $('#evento-tipologia').val(result['Tipologia']);
                $('#evento-lugar').val(result['LugarDesarrollo']);
                $('#evento-fechainicio').datepicker('setDate',result['FechaInicio']);
                $('#evento-fechafin').datepicker('setDate',result['FechaFin']);
                $('#evento-introduccion').val(result['IntroduccionDescripcion']);
            }
        });*/

    }
    function lista_archivos_sustento(archivos, idcontenedor){
        $('tbody', $(idcontenedor)).empty();
        //console.dir(archivos);
        $.each(archivos, function(i, row){
            var tr = $('<tr>'+'</tr>')
                    .append('<td class="td-img">&nbsp;</td>'
                        ,'<td class="td-nombre">&nbsp;</td>'
                        ,'<td class="tbl-opciones-carga-archivo">'
                            +'<a href="#tblDocumentosSustento" class="btn-seleccionar" onclick="seleccionar(this);"><i class="fa fa-cloud-upload fa-lg"></i> Seleccionar</a>'
                            +'<a href="#tblDocumentosSustento" class="btn-eliminar" onclick="rowRemove(this);"><i class="fa fa-trash-o fa-lg"></i>&nbsp;Eliminar</a>'
                            +'<input  name="Sustento" type="file" style="display: none;" onchange="file_selected(this);"/>'
                            +'<input type="hidden" id="hddIdAdjunto" name="hddIdAdjunto" value="'+row['id']+'" />'
                        +'</td>'                    
                    );  
                addImg(tr, row['nombre'].split('.')[1]);
                setNameFile(tr, row['nombre'], row['ruta']);
                delSeleccionar(tr.find('td.tbl-opciones-carga-archivo'));
                if($('a.btn-modificar', tr).length == 0){
                    addModificar(tr.find('td.tbl-opciones-carga-archivo'));    
                }
            $('tbody', $(idcontenedor)).append(tr);
        });  
    }
    //-----------------------------------------------------
    function file_selected(sender){
        var tr = $(sender).closest('tr');
        clearNameFile(tr);
        clearImg(tr);

        if($('input:file', tr)[0].files.length){
            var nombre = $('input:file', tr)[0].files[0].name;
            if($('a.btn-modificar', tr).length == 0){
                addModificar(tr.find('td.tbl-opciones-carga-archivo'));    
            }
            if($('#hddIdAdjunto', tr).val() != '-1'){
                var del = $('#hddDeleted', $(sender).closest('table'));
                del.val(del.val() + ',' + $('#hddIdAdjunto', tr).val());
            }
            setIdFile(tr,'-1');
            addImg(tr, nombre.split('.')[1]);
            setNameFile(tr, nombre, '');
            delSeleccionar(tr.find('td.tbl-opciones-carga-archivo'));
        }
    }
    function seleccionar(sender){
        $('input:file',$(sender).parent()).trigger('click');
    }
    function setIdFile(contenedor, id){
        $('#hddIdAdjunto', contenedor).val(id);
    }
    function setNameFile(contenedor, nombre, ruta){
        if(ruta == ''){
            $('td.td-nombre', contenedor).empty().append(nombre);
        } else {
            $('td.td-nombre', contenedor).empty().append('<a target="blank" href="<?= Yii::$app->getUrlManager()->createUrl('/'); ?>'+ruta+nombre+'">'+nombre+'</a>');
        }
    }
    function clearNameFile(contenedor){
        $('td.td-nombre', contenedor).empty();
    }
    function addImg(contenedor, extension){
        console.log(extension);
        var img = '<i class="fa fa-file-o  fa-lg"></i>';
        if(extension == 'txt'){
            img = '<i class="fa fa-file-text-o  fa-lg"></i>';
        } else if(extension == 'doc' || extension == 'docx'){
            img = '<i class="fa fa-file-word-o  fa-lg"></i>';
        } else if(extension == 'xls' || extension == 'xlsx'){
            img = '<i class="fa fa-file-excel-o  fa-lg"></i>';
        } else if(extension == 'pdf'){
            img = '<i class="fa fa-file-pdf-o  fa-lg"></i>';
        } else if(extension == 'jpg' || extension == 'bmp' || extension == 'tif' || extension == 'png'){
            img = '<i class="fa fa-picture-o  fa-lg"></i>';
        } else if(extension == 'mpg' || extension == 'avi' || extension == 'wma' || extension == '3gp'){
            img = '<i class="fa fa-film  fa-lg"></i>';
        }
        $('td.td-img', contenedor).empty().append(img);
    }
    function clearImg(contenedor){
        $('td.td-img', contenedor).empty();
    }
    function rowRemove(sender){
        var del = $('#hddDeleted', $(sender).closest('table'));
        del.val(del.val() + ',' + $('#hddIdAdjunto', $(sender).closest('tr')).val());
        $(sender).closest('tr').remove();
    }
    function addSeleccionar(contenedor){
        $(contenedor).append('<a href="#tblDocumentosSustento" class="btn-seleccionar" onclick="seleccionar(this);"><i class="fa fa-cloud-upload fa-lg"></i> Seleccionar</a>');
    }
    function addModificar(contenedor){
        $(contenedor).append('<a href="#tblDocumentosSustento" class="btn-modificar" onclick="seleccionar(this);"><i class="fa fa-refresh fa-lg"></i> Cambiar</a>');
    }
    function addEliminar(contenedor){
        $(contenedor).append('<a href="#tblDocumentosSustento" class="btn-eliminar" onclick="rowRemove(this);"><i class="fa fa-trash-o fa-lg"></i>&nbsp;Eliminar</a>');
    }
    function delSeleccionar(contenedor){
        $('a.btn-seleccionar',$(contenedor)).remove();
    }
    function delModificar(contenedor){
        $('a.btn-modificar',$(contenedor)).remove();
    }
    function delEliminar(contenedor){
        $('a.btn-eliminar',$(contenedor)).remove();
    }
</script>



