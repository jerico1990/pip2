<?php if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start(); ?>
<link href="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
<script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/bootstrap-table/bootstrap-table.min.js"></script>
<script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/bootstrap-table/bootstrap-table-es-SP.min.js"></script>
<link href="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/form-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">

<link rel="stylesheet" media="all" type="text/css" href="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/timepicker/jquery-ui-timepicker-addon.css" />

<div id="page-content" style="min-height: 885px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Programaci&oacute;n de visitas</h1>
        </div>
        <div class="container">
            <div class="panel panel-primary">
                <div id="Divnotificaciones"></div>
				<div class="panel-body">
                    <div id="tblListaToolbar">
                        <button id="btnCrearVisita" class="btn btn-primary" type="button"><span class=" fa fa-plus"></span>&nbsp;Generar Visita</button>
					</div>
                    <table id="tblLista" data-height="450">
						<thead>
							<tr>
								<th data-field="Item" data-width="35" data-align="center">N°</th>
								<th data-field="CodigoProyecto" data-width="100" data-align="left">Estaci&oacute;n</th>
								<th data-field="Mesanio" data-width="120" data-align="left">Mes/Año</th>	
								<th data-field="Estado" data-width="90" data-align="center">Estado</th>	
								<th data-field="Asunto" data-align="left" data-halign="center">Asunto</th>	
								<th data-formatter="tblLista_editar" data-events="tblLista_events" data-width="35" data-align="center">&nbsp;</th>	
								<th data-formatter="tblLista_eliminar" data-events="tblLista_events" data-width="35" data-align="center">&nbsp;</th>
							</tr>
						</thead>
					</table>
				</div>
            </div>
        </div>
    </div>
</div>
<script>
	window.tblLista_events = {
        'click .tblLista-editar': function (e, value, row, index) {
            var formulario = $('<form method="post" action="<?= Yii::$app->getUrlManager()->createUrl('/programacion-visita/editar'); ?>"></form>')
            .append('<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>"/>');
            //.append('<input type="hidden" name="CodigoProyecto" value="'+$(this).data('codigo-proyecto')+'"/>');

            $('body').append(formulario);
            formulario.submit();
        },
        'click .tblLista-eliminar': function (e, value, row, index) {
            
        }
    };
	
	function tblLista_editar(value, row, index) {
		return '<a class="tblLista-editar" href="#"><span class="fa fa-pencil"></span></a>';
	}
	function tblLista_eliminar(value, row, index) {
		return '<a class="tblLista-eliminar" href="#"><span class="fa fa-trash"></span></a>';
	}
	
    function cargar_grilla(){
             
        $('#tblLista').bootstrapTable('load', [
            {Item:1, CodigoProyecto:'118_PI', CodigoPOA:'1.8', Mesanio:'Setiembre 2017', Estado:'Ejecutado', Asunto:'Verif. compras de equipos de laboratorio...' },
            {Item:2, CodigoProyecto:'116_PI', CodigoPOA:'1.5', Mesanio:'Octubre 2017', Estado:'Pediente', Asunto:'Verif. compras de insumos quimicos del extranjero...' },
            {Item:3, CodigoProyecto:'055_PI', CodigoPOA:'1.6', Mesanio:'Octubre 2017', Estado:'Pediente', Asunto:'Verif. compras de equipos de laboratorio...' }
        ]);  
    }
	$(document).ready(function(){

        $('#tblLista').bootstrapTable({
            cache: false,
            striped: true,
            pagination: false,
            search: true,
			classes: 'table table-condensed',
            toolbar: '#tblListaToolbar'
		});
		
		cargar_grilla();

        $('#btnCrearVisita').click(function(){
            var formulario = $('<form method="post" action="<?= Yii::$app->getUrlManager()->createUrl('/programacion-visita/nuevo'); ?>"></form>')
            .append('<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>"/>');
            //.append('<input type="hidden" name="CodigoProyecto" value="'+$(this).data('codigo-proyecto')+'"/>');

            $('body').append(formulario);
            formulario.submit();
        });
	});
    
</script>
