<?php if(!empty($FinDetalle)): ?>
    <table id="tbl-ml-finproyecto" class="table table-bordered table-condensed tbl-act">
        <tbody>
            <tr class="tr-header">
                <td rowspan="2"></td>
                <td colspan="2">Indicador</td>
                <td rowspan="2">Medio de verificación</td>
                <td rowspan="2">Supuestos</td>
            </tr>
            <tr class="tr-header">
                <!-- <td>Meta</td> -->
                <td>Nombre Indicador</td>
                <td>Unidad de Medida</td>
            </tr>
            <?php $i=1; foreach ($FinDetalle as $fin): ?>
                <tr>
                    <td class="td-acciones">
                        <?php if(\Yii::$app->user->identity->rol==7){ ?>
                            <a href="#" class="btn-edit-ml-finproyecto" data-toggle="tooltip" title="Editar" role-id="<?php echo $fin->ID ?>"><i class="fa fa-edit fa-lg"></i></a>&nbsp;
                            <a href="#" class="btn-remove-ml-finproyecto" data-toggle="tooltip" title="Eliminar" role-id="<?php echo $fin->ID ?>"><i class="fa fa-remove fa-lg"></i></a>
                        <?php } ?>
                    </td>
                    <!-- <td><?php echo $fin->IndicadorMeta ?></td> -->
                    <td><?php echo $fin->IndicadorDescripcion ?></td>
                    <td><?php echo $fin->IndicadorUnidadMedida ?></td>
                    <td><?php echo $fin->MedioVerificacion ?></td>
                    <td><?php echo $fin->Supuestos ?></td>
                </tr>
            <?php $i++; endforeach; ?>
        </tbody>
    </table>


    <script>
        var deshabilitar=<?= \Yii::$app->user->identity->SituacionProyecto ?>;
        if (deshabilitar!=0) {
            $(".container input,.container textarea,.container select").prop("disabled", true);
            $(".btn-edit-ml-finproyecto,.btn-remove-ml-finproyecto,.container button").hide();
        }
        $('.btn-edit-ml-finproyecto').click(function (e) {
            e.preventDefault();
            var id = $(this).attr('role-id');
            $('#modal-finproyecto .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/marco-logico/finform?id=' + id);
            $('#modal-finproyecto').modal('show');
        });

        $('.btn-remove-ml-finproyecto').click(function (e) {
            e.preventDefault();
            var $this = $(this);
            bootboxConfirmEliminar($this,
                'Está seguro de eliminar el indicador seleccionado?',
                function () {
                    var id = $this.attr('role-id');
                    var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                    $.post('<?= \Yii::$app->request->BaseUrl ?>/marco-logico/findelete', { id: id ,_csrf: toke }, function (data) {
                        var jx = JSON.parse(data);
                        if (jx.Success == true) {
                            cargarFinProyectoTable();
                        }
                    });
                });
        });
    </script>
<?php else: ?>
<div class="alert alert-info"><i class="fa fa-info"></i>&nbsp;No se registraron indicadores para el Fin del Proyecto.</div>
<?php endif ?>