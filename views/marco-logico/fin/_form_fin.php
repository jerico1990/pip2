<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => empty($model->ID) ? 'marco-logico/indicadoresfincrear' : 'marco-logico/indicadoresfinactualizar?id='.$model->ID,
        'options'           => [
            'autocomplete'  => 'off',
            'id'            => 'frmFinProyecto'
        ]
    ]
); ?>

    <input data-val="true" data-val-number="The field ID must be a number." data-val-required="El campo ID es obligatorio." id="ID" name="ID" type="hidden" value="0">
    <input data-val="true" data-val-number="The field ProyectoID must be a number." data-val-required="El campo ProyectoID es obligatorio." id="ProyectoID" name="ProyectoID" type="hidden" value="80">    
    <div class="modal-body">
        <div class="form-horizontal">
            <!--
            <div class="form-group">
                <label class="col-sm-4 control-label">Tipo <span class="f_req">*</span></label>
                <div class="col-sm-8">
                    <textarea class="form-control" cols="20" id="IndicadorTipo" maxlength="100" name="MarcoLogicoFinProyecto[IndicadorTipo]" required="" rows="2"><?php echo $model->IndicadorTipo ?></textarea>
                    <h6 class="pull-left count-message"></h6>
                </div>
            </div>
            -->
            <div class="form-group">
                <label class="col-sm-4 control-label">Nombre Indicador <span class="f_req">*</span></label>
                <div class="col-sm-8">
                    <textarea class="form-control" cols="20" id="IndicadorDescripcion" maxlength="100000" name="MarcoLogicoFinProyecto[IndicadorDescripcion]" required="" rows="2"><?php echo $model->IndicadorDescripcion ?></textarea>
                    <h6 class="pull-left count-message"></h6>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Unidad de Medida <span class="f_req">*</span></label>
                <div class="col-sm-8">
                    <textarea class="form-control" cols="20" id="IndicadorUnidadMedida" maxlength="100000" name="MarcoLogicoFinProyecto[IndicadorUnidadMedida]" required="" rows="2"><?php echo $model->IndicadorUnidadMedida ?></textarea>
                    <h6 class="pull-left count-message"></h6>
                </div>
            </div>
            <!--
            <div class="form-group">
                <label class="col-sm-4 control-label">Inicio <span class="f_req">*</span></label>
                <div class="col-sm-8">
                    <input class="form-control input-xsmall input-number" data-val="true" data-val-number="The field IndicadorInicio must be a number." data-val-required="El campo IndicadorInicio es obligatorio." id="IndicadorInicio" name="MarcoLogicoFinProyecto[IndicadorInicio]" required="" type="text" value="<?php echo $model->IndicadorInicio ?>">
                </div>
            </div>
            -->
            <div class="form-group">
                <label class="col-sm-4 control-label">Meta <span class="f_req">*</span></label>
                <div class="col-sm-8">
                    <input class="form-control input-number" onKeyPress="return soloNumeros(event)" data-val="true" data-val-number="The field IndicadorMeta must be a number." data-val-required="El campo IndicadorMeta es obligatorio." id="IndicadorMeta" name="MarcoLogicoFinProyecto[IndicadorMeta]" required="" type="text" value="<?php echo $model->IndicadorMeta ?>">
                </div>
            </div>
            <hr>
            <div class="form-group">
                <label class="col-sm-4 control-label">Medio Verificación <span class="f_req">*</span></label>
                <div class="col-sm-8">
                    <textarea class="form-control" cols="20" id="MedioVerificacion" maxlength="100000" name="MarcoLogicoFinProyecto[MedioVerificacion]" required="" rows="2"><?php echo $model->MedioVerificacion ?></textarea>
                    <h6 class="pull-left count-message"></h6>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Supuestos <span class="f_req">*</span></label>
                <div class="col-sm-8">
                    <textarea class="form-control" cols="20" id="Supuestos" maxlength="100000" name="MarcoLogicoFinProyecto[Supuestos]" required="" rows="2"><?php echo $model->Supuestos ?></textarea>
                    <h6 class="pull-left count-message"></h6>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button id="btn-save-finproyecto" type="button" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>


<script>
    var situacionProyecto=<?= \Yii::$app->user->identity->SituacionProyecto ?>;
    if (situacionProyecto!=0) {
        $(".container input,.container textarea,.container select").prop("disabled", true);
        $(".btn-edit-ml-finproyecto,.btn-remove-ml-finproyecto,.container button").hide();
    }
    var $form = $('#frmFinProyecto');
    var formParsleyFinProyecto = $form.parsley(defaultParsleyForm());

    $(document).ready(function () {
        $('#frmFinProyecto .input-number').inputmask("numeric").click(function () {
            $(this).select();
        });

        // $('.count-message').each(function () {
        //     var $this = $(this);
        //     var $divformgroup = $this.closest('div.form-group');
        //     var $txt = $divformgroup.find('textarea, input[type="text"]');
        //     var text_max = parseInt($txt.attr('maxlength'));

        //     $txt.keyup(function () {
        //         var text_length = $txt.val().length;
        //         var text_remaining = text_max - text_length;
        //         $this.html(text_remaining + ' caracteres restantes');
        //     });
        // });
    });

    $(document).on('click','#btn-save-finproyecto',function (e) {
        e.preventDefault();
        var $btn = $(this);
        $btn.attr('disabled', 'disabled');
        var isValid = formParsleyFinProyecto.validate();
        if (isValid) {
            sendFormNoMessage($('#frmFinProyecto'), $(this), function () {
                cargarFinProyectoTable();
                _pageLoadingEnd();
            });
        } else {
            $btn.removeAttr('disabled');
        }
    });

</script>