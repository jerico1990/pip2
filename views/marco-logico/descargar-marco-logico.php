<?php
use app\models\Actividad;
use app\models\ActRubroElegible;
use app\models\CronogramaAreSubCategoria;
use app\models\CronogramaProyecto;
use app\models\MarcoLogicoActividad;


/** Error reporting */
error_reporting(E_ALL);

/** Include path **/
ini_set('include_path', ini_get('include_path').';../web/PHPExcel/Classes/');
//require(__DIR__ . '/../../web/PHPExcel/Classes/');
/** PHPExcel */
include 'PHPExcel.php';

/** PHPExcel_Writer_Excel2007 */
include 'PHPExcel/Writer/Excel2007.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
$sheet = $objPHPExcel->getActiveSheet();
$sheet->setTitle('POA');
$styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));  // Bordes

$sheet->getColumnDimension('A')->setWidth(15);
$sheet->getColumnDimension('B')->setWidth(5);
$sheet->getColumnDimension('C')->setWidth(25);
$sheet->getColumnDimension('D')->setWidth(18);
$sheet->getColumnDimension('E')->setWidth(25);
$sheet->getColumnDimension('F')->setWidth(20);
$sheet->getColumnDimension('G')->setWidth(15);
$sheet->getRowDimension('1')->setRowHeight(20);
$sheet->getRowDimension('2')->setRowHeight(30);
$sheet->getRowDimension('3')->setRowHeight(30);
$sheet->getRowDimension('4')->setRowHeight(30);
$sheet->getRowDimension('5')->setRowHeight(35);
$sheet->getStyle('A1')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
$sheet->getStyle('A2')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
$sheet->getStyle('A3')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
$sheet->getStyle('A4')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
$sheet->getStyle('D2')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
$sheet->getStyle('D3')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
$sheet->getStyle('D4')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
$sheet->getStyle('E4')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
$sheet->getStyle('G4')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
$sheet->getStyle('A5')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
$sheet->getStyle('D5')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));

$sheet->getStyle('D6')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
$sheet->getStyle('E6')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
$sheet->getStyle('F5')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
$sheet->getStyle('G5')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
$sheet->getStyle('A')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));


$sheet->getStyle('D2:G2')->getAlignment()->setWrapText(true);
$sheet->getStyle('E4:F4')->getAlignment()->setWrapText(true);
$sheet->getStyle('F5:F6')->getAlignment()->setWrapText(true);
$sheet->getStyle('A2:C2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('F2F2F2');
$sheet->getStyle('A3:C3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('F2F2F2');
$sheet->getStyle('A4:C4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('F2F2F2');
$sheet->getStyle('E4:F4')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('F2F2F2');

$sheet->getStyle('A5')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('C4BD97');
$sheet->getStyle('D5')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('C4BD97');
$sheet->getStyle('D6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('C4BD97');
$sheet->getStyle('E6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('C4BD97');
$sheet->getStyle('F5')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('C4BD97');
$sheet->getStyle('G5')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('C4BD97');


//$sheet->getStyle('A2')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
//$sheet->getStyle('A2')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
//$sheet->getStyle('A2')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));


//$sheet->SetCellValue('A1:G1', 'MARCO LÓGICO DEL PROYECTO');
$contadorFines=0;
foreach($fines as $fin){
    $contadorFines++;
}

$contadorPropositos=0;
foreach($propositos as $proposito){
    $contadorPropositos++;
}

$contadorComponentes=0;
foreach($componentes as $componente){
    $contadorComponentes++;
}


$contadorActividades=0;
foreach($actividades as $actividad){
    $contadorActividades++;
}

$sheet->mergeCells('A1:G1');
$sheet->SetCellValue('A1', 'MARCO LÓGICO DEL PROYECTO');



$sheet->mergeCells('A2:C2');
$sheet->SetCellValue('A2', 'TÍTULO DEL PROYECTO');




$sheet->mergeCells('D2:G2');
$sheet->SetCellValue('D2', $informacion->TituloProyecto);
$sheet->mergeCells('A3:C3');
$sheet->SetCellValue('A3', 'DEPENDENCIA DEL INIA EJECUTORA');
$sheet->mergeCells('D3:G3');
$sheet->SetCellValue('D3', $unidadEjecutora->Nombre);

$sheet->mergeCells('A4:C4');
$sheet->SetCellValue('A4', 'CÓDIGO DEL PROYECTO ');
$sheet->SetCellValue('D4', $informacion->Codigo);
$sheet->mergeCells('E4:F4');
$sheet->SetCellValue('E4', 'MESES');
$sheet->SetCellValue('G4', $informacion->Meses);

$sheet->mergeCells('A5:C6');
$sheet->SetCellValue('A5', 'LÓGICA DE INTERVENCIÓN ');
$sheet->mergeCells('D5:E5');
$sheet->SetCellValue('D5', 'INDICADORES ');
$sheet->SetCellValue('D6', 'UNIDAD MEDIDA ');
$sheet->SetCellValue('E6', 'META FÍSICA ');
$sheet->mergeCells('F5:F6');
$sheet->SetCellValue('F5', 'MEDIOS DE VERIFICACIÓN ');
$sheet->mergeCells('G5:G6');
$sheet->SetCellValue('G5', 'SUPUESTOS ');


//Fin
//$sheet->mergeCells('A7:B'.(7+$contadorFines-1));
$sheet->SetCellValue('A7', 'FIN');
$f=0;
foreach($fines as $fin){
    $sheet->SetCellValue('B'.(7+$f),'');
    $sheet->SetCellValue('C'.(7+$f), $fin->IndicadorDescripcion);
    $sheet->SetCellValue('D'.(7+$f), $fin->IndicadorUnidadMedida);
    $sheet->SetCellValue('E'.(7+$f), $fin->IndicadorMeta);
    $sheet->SetCellValue('F'.(7+$f), $fin->MedioVerificacion);
    $sheet->SetCellValue('G'.(7+$f), $fin->Supuestos);
    $f++;
}
//Proposito
//$sheet->mergeCells('A'.(8+$f-1).':B'.(8+$f+$contadorPropositos-2));
$sheet->SetCellValue('A'.(8+$f-1), 'PROPÓSITO');
$p=0;
foreach($propositos as $proposito){
    $sheet->SetCellValue('B'.(8+$f+$p-1), '');
    $sheet->SetCellValue('C'.(8+$f+$p-1), $proposito->IndicadorDescripcion);
    $sheet->SetCellValue('D'.(8+$f+$p-1), $proposito->IndicadorUnidadMedida);
    $sheet->SetCellValue('E'.(8+$f+$p-1), $proposito->IndicadorMeta);
    $sheet->SetCellValue('F'.(8+$f+$p-1), $proposito->MedioVerificacion);
    $sheet->SetCellValue('G'.(8+$f+$p-1), $proposito->Supuestos);
    $p++;
}

//Objetivos
$sheet->mergeCells('A'.(9+$f+$p-2).':A'.(9+$f+$p+$contadorComponentes-3));
$sheet->SetCellValue('A'.(9+$f+$p-2), 'OBJETIVOS ESPECÍFICOS ');
$c=0;
foreach($componentes as $componente){
    $sheet->SetCellValue('B'.(9+$f+$p+$c-2), $componente->ComponenteCorrelativo);
    $sheet->SetCellValue('C'.(9+$f+$p+$c-2), $componente->ComponenteNombre);
    $sheet->SetCellValue('D'.(9+$f+$p+$c-2), $componente->IndicadorUnidadMedida);
    $sheet->SetCellValue('E'.(9+$f+$p+$c-2), $componente->IndicadorMeta);
    $sheet->SetCellValue('F'.(9+$f+$p+$c-2), $componente->MedioVerificacion);
    $sheet->SetCellValue('G'.(9+$f+$p+$c-2), $componente->Supuestos);
    $c++;
}


//actividades
$sheet->mergeCells('A'.(10+$f+$p+$c-3).':A'.(10+$f+$p+$c+$contadorActividades-4));
$sheet->SetCellValue('A'.(10+$f+$p+$c-3), 'ACTIVIDADES ');
$a=0;
foreach($actividades as $actividad){
    $sheet->SetCellValue('B'.(10+$f+$p+$c+$a-3), $actividad->ComponenteCO.'.'.$actividad->ActividadCorrelativo);
    $sheet->SetCellValue('C'.(10+$f+$p+$c+$a-3), $actividad->ActividadNombre);
    $sheet->SetCellValue('D'.(10+$f+$p+$c+$a-3), $actividad->IndicadorUnidadMedida);
    $sheet->SetCellValue('E'.(10+$f+$p+$c+$a-3), $actividad->IndicadorMetaFisica);
    $sheet->SetCellValue('F'.(10+$f+$p+$c+$a-3), $actividad->IndicadorMedioVerificacion);
    $sheet->SetCellValue('G'.(10+$f+$p+$c+$a-3), $actividad->IndicadorSupuestos);
    $a++;
}

$sheet->getStyle('A1:G'.(6+$f+$p+$c+$a))->applyFromArray($styleArray);
$sheet->getStyle('A7:G'.(6+$f+$p+$c+$a))->getAlignment()->setWrapText(true);
//$sheet->getStyle('A7:G'.(6+$f+$p+$c+$a))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('C4BD97');
$sheet->getStyle('A7:G'.(6+$f+$p+$c+$a))->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));

/*



$sheet->SetCellValue('A3:C3', 'DEPENDENCIA DEL INIA EJECUTORA');
$sheet->SetCellValue('D3:G3', $unidadEjecutora->Nombre);*/
/*
$sheet->SetCellValue('C1', 'Clasificador');
$sheet->SetCellValue('D1', 'Unidad Medida');
$sheet->SetCellValue('E1', 'Cantidad');
$sheet->SetCellValue('F1', 'Costo Unitario');
$sheet->SetCellValue('G1', 'Total Proyecto');

    $sheet->SetCellValue('A'.$i, ''.$componente->Correlativo.'');
    $sheet->SetCellValue('B'.$i, 'O.E. '.$componente->Nombre);
    $sheet->SetCellValue('C'.$i, '');
    $sheet->SetCellValue('D'.$i, '');
    $sheet->SetCellValue('E'.$i, $componente->Cantidad);
    $sheet->SetCellValue('F'.$i, $componente->CostoUnitario);
    $sheet->SetCellValue('G'.$i, $TotalComponente);
   
*/


// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="MarcoLogico-'.$informacion->Codigo.'.xls"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
