<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<style>
    #container-poaf .input-number {
        text-align: right;
        min-width: 72px !important;
        height: 22px !important;
        font-size: 13px !important;
    }

    #container-poaf .input-number-sm {
        text-align: right;
        min-width: 52px !important;
        height: 22px !important;
        font-size: 13px !important;
    }

    #container-poaf .input-number-md {
        text-align: right;
        min-width: 82px !important;
        height: 22px !important;
        font-size: 13px !important;
    }

    #container-poaf table-act > tbody > tr > td:nth-child(1) {
        text-align: center;
    }

    #container-poaf .table-act > tbody > tr > td:nth-child(2) {
        min-width: 320px !important;
    }

    #container-poaf .table-act > tbody > tr > td:nth-child(3) {
        min-width: 100px !important;
    }

    #container-poaf .table > tbody > .tr-header > td {
        text-align: center;
        font-size: 12px;
        font-weight: bold;
        padding: 1px !important;
        vertical-align: middle !important;
        /*border: 1px solid #cfcfd0;
        background-color: #f0f0f1;*/
        border: 1px solid #c0c0c0;
        background-color: #e3e3e3;
        min-width: 75px !important;
    }

    #container-poaf h4.panel-title {
        font-size: 16px;
    }

    #container-poaf .tr-comp td, .tr-proy td {
        font-weight: bold;
    }

    #container-poaf .table-condensed td {
        padding: 1px !important;
    }

    #container-poaf td, #table-container input {
        font-size: 13px !important;
    }

    #container-poaf .form-control[readonly] {
        background-color: #f2f3f4;
    }
</style>

<input data-val="true" data-val-number="The field ID must be a number." data-val-required="El campo ID es obligatorio." id="ID" name="ID" type="hidden" value="0">
    <input data-val="true" data-val-number="The field ProyectoID must be a number." data-val-required="El campo ProyectoID es obligatorio." id="ProyectoID" name="ProyectoID" type="hidden" value="80">    
<div class="modal-body">

    <div class="tab-container">
        <ul class="nav nav-tabs">
            <li id="li-poaf" class="active"><a href="#tab-act" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Actividad</a></li>
            <li id="li-pc"><a href="#indicadores-tab" data-toggle="tab"><i class="fa fa-pencil"></i>&nbsp;Indicadores</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab-act">
                <?php $form = ActiveForm::begin(
                    [
                        'action'            => empty($model->ID) ? 'marco-logico/actividad-crear' : 'marco-logico/actividad-actualizar?id='.$model->ID,
                        'options'           => [
                            'autocomplete'  => 'off',
                            'id'            => 'frmActividadComponente'
                        ]
                    ]
                ); ?>
                    <input type="hidden" name="Actividad[ComponenteID]" value="<?= $model->ComponenteID ?>">
                    <input type="hidden" name="Meses" value="<?= $Meses ?>">
                    
                    <div id="container-resp"></div>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Descripcion <span class="f_req">*</span></label>
                            <div class="col-sm-10">
                                <textarea class="form-control" cols="20" id="Nombre" maxlength="250" name="Actividad[Nombre]" required="" rows="2"><?php echo $model->Nombre ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Peso <span class="f_req">*</span></label>
                            <div class="col-sm-10">
                                <input class="form-control input-number" onKeyPress="return NumCheck(event, this)" type="text" id="Peso" name="Actividad[Peso]" required="" type="text" value="<?php echo round($model->Peso, 2);  ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Experimento <span class="f_req">*</span></label>
                            <div class="col-sm-4">
                                <input class="form-control input-number" type="text" id="Peso" name="Actividad[Experimento]" required="" onKeyPress="return soloNumeros(event)" type="text" value="<?php echo $model->Experimento ?>">
                            </div>
                            <label class="col-sm-2 control-label">Evento <span class="f_req">*</span></label>
                            <div class="col-sm-4">
                                <input class="form-control input-number" type="text" id="Peso" name="Actividad[Evento]" required="" type="text" onKeyPress="return soloNumeros(event)" value="<?php echo $model->Evento ?>">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button id="" type="submit" class="btn btn-primary btn-save-act-proyecto" id="act-btn"><i class="fa fa-check"></i>&nbsp;<?php echo ($model->ID)?'Actualizar Actividad':'Continuar'; ?></button>
                        </div>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>

            <div class="tab-pane" id="indicadores-tab">
                <?php $form = ActiveForm::begin(
                    [
                        'action'            => 'marco-logico/actividad-crear-indicador',
                        'options'           => [
                            'autocomplete'  => 'off',
                            'id'            => 'frmActividadComponenteIndicadores'
                        ]
                    ]
                ); ?>
                    <div class="alert alert-info indica">
                        <i class="fa fa-info"></i>&nbsp;&nbsp;No tiene indicadores registrados.
                    </div>
                    <div class="form-horizontal hidden" id="indicador-form-actv">
                        <p><strong>INDICADORES</strong></p>
                        <div class="form-group">
                            <input type="hidden" name="MarcoLogicoActividad[ActividadID]" value="<?php echo $model->ID ?>" id="actividad-indicador-id">
                            <label class="col-sm-2 control-label">Unidad de Medida <span class="f_req">*</span></label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" cols="20" id="UnidadMedida" maxlength="100" name="MarcoLogicoActividad[IndicadorUnidadMedida]" required="" rows="2" value="">
                            </div>
                            <label class="col-sm-2 control-label">Meta Física<span class="f_req">*</span></label>
                            <div class="col-sm-4">
                                <input class="form-control input-number" data-val="true" data-val-number="The field IndicadorMeta must be a number." onKeyPress="return soloNumeros(event)" data-val-required="El campo IndicadorMeta es obligatorio." id="MetaFisica" name="MarcoLogicoActividad[IndicadorMetaFisica]" required="" type="text" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Nombre Indicador <span class="f_req">*</span></label>
                            <div class="col-sm-10">
                                <textarea class="form-control" cols="20" id="UnidadMedida" maxlength="10000" name="MarcoLogicoActividad[IndicadorDescripcion]" required="" rows="2"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Medio Verificación <span class="f_req">*</span></label>
                            <div class="col-sm-10">
                                <textarea class="form-control" cols="20" id="MedioVerificacion" maxlength="250" name="MarcoLogicoActividad[IndicadorMedioVerificacion]" required="" rows="2"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Supuestos <span class="f_req">*</span></label>
                            <div class="col-sm-10">
                                <textarea class="form-control" cols="20" id="Supuestos" maxlength="250" name="MarcoLogicoActividad[IndicadorSupuestos]" required="" rows="2"></textarea>
                            </div>
                        </div>
                        <button id="btn-save-indicador-act" type="submit" class="btn btn-primary "> Guardar Indicador</button>
                        <hr>
                            
                        <div class="table_indica_actv">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    
                    </div>
                <?php ActiveForm::end(); ?>   
            </div>
        </div>
    </div>

</div>


<script>

    var $form = $('#frmActividadComponente');
    var $formI = $('#frmActividadComponenteIndicadores');
    var formParsleyActividadProyecto = $form.parsley(defaultParsleyForm());
    var formParsleyActividadProyecto1 = $formI.parsley(defaultParsleyForm());


    $(document).ready(function () {
        var como = $("#actividad-indicador-id").val();
        // alert(como);
        if(como != ''){
            $('.indica').css('display','none');
            
            $('#indicador-form-actv').removeClass('hidden');
        }
        cargarIndicadoresActividad(como);
    });

    
    function cargarIndicadoresActividad(x) {
        $('.table_indica_actv').load('<?= \Yii::$app->request->BaseUrl ?>/marco-logico/lista-indicadores-actividad?ActividadID='+x);
    }
</script>