<div class="container-resp"></div>
<?php 
if(!empty($model)): ?>
	<table class="table table-hover tbl-act">
	    <thead >
	    	<tr class="tr-header">
				<th>Descripción Indicador</th>
		        <th>Unidad de medida</th>
		        <th>Meta Fisica</th>
		        <th>Medios de verificacion</th>
		        <th>Supuestos</th>
		        <th>Acciones</th>
	    	</tr>
	    </thead>
	    <tbody id="table_indica_obj">
	        <?php foreach ($model as $value): ?>
	        	<tr>
			    <td><textarea onfocusout="Descripcion($(this).val(),<?= $value->ID ?>)" class="form-control" ><?php echo $value->IndicadorDescripcion ?></textarea></td>
			    <td><input onfocusout="Unidad($(this).val(),<?= $value->ID ?> )" class="form-control" value="<?php echo $value->IndicadorUnidadMedida ?>"></td>
			    <td><input onfocusout="Meta($(this).val(),<?= $value->ID ?> )" class="form-control" value="<?php echo $value->IndicadorMetaFisica ?>"></td>
			    <td><textarea onfocusout="Verificacion($(this).val(),<?= $value->ID ?> )" class="form-control" ><?php echo $value->IndicadorMedioVerificacion ?></textarea></td>
			    <td><textarea onfocusout="Supuestoss($(this).val(),<?= $value->ID ?> )" class="form-control" ><?php echo $value->IndicadorSupuestos ?></textarea></td>
			    <td><a href="#" data-delete-comp-id="<?php echo $value->ID ?>" data-comp-id="<?php echo $value->ActividadID ?>" class="btn-remove-ind-act btn btn-danger"><i class="fa fa-remove fa-lg"></i></a></td>
	        	</tr>
	        <?php endforeach ?>
	    </tbody>
	</table>
<?php else: ?>
	<div class="alert alert-info">
        <i class="fa fa-info"></i>&nbsp;&nbsp;No tiene indicadores registrados.
    </div>
<?php endif; ?>
<?php
    $aprobar=Yii::$app->getUrlManager()->createUrl('plan-operativo/actualizar-marco-actividad');
?>
<script>
    function Descripcion(Valor,ID) {
	
	var toke = '<?=Yii::$app->request->getCsrfToken()?>';
	$.ajax({
	    url: '<?= $aprobar ?>',
	    type: 'GET',
	    async: false,
	    data: {Valor:Valor,ID:ID,Tipo:1,_csrf: toke},
	    success: function(data){
	       var $cont = $('.container-resp');
	        $cont.html('');
            var htm =  '<div class="alert alert-success">';
                htm += '<i class="fa fa-check"></i>&nbsp;La <strong>descripción</strong> fue actualizada correctamente';
                htm += '</div>';
                console.log(htm);
            $cont.html(htm);
            $cont.removeClass('hidden');
	    }
	});
    }
    
    function Unidad(Valor,ID) {
	var toke = '<?=Yii::$app->request->getCsrfToken()?>';
	$.ajax({
	    url: '<?= $aprobar ?>',
	    type: 'GET',
	    async: false,
	    data: {Valor:Valor,ID:ID,Tipo:2,_csrf: toke},
	    success: function(data){
	       var $cont = $('.container-resp');
	        $cont.html('');
            var htm =  '<div class="alert alert-success">';
                htm += '<i class="fa fa-check"></i>&nbsp;La <strong>unidad de medida</strong> fue actualizada correctamente';
                htm += '</div>';
            $cont.html(htm);
            $cont.removeClass('hidden');
	    }
	});
    }
    
    function Meta(Valor,ID) {
	var toke = '<?=Yii::$app->request->getCsrfToken()?>';
	$.ajax({
	    url: '<?= $aprobar ?>',
	    type: 'GET',
	    async: false,
	    data: {Valor:Valor,ID:ID,Tipo:3,_csrf: toke},
	    success: function(data){
	        var $cont = $('.container-resp');
	        $cont.html('');
            var htm =  '<div class="alert alert-success">';
                htm += '<i class="fa fa-check"></i>&nbsp;La <strong>meta física</strong> fue actualizada correctamente';
                htm += '</div>';
            $cont.html(htm);
            $cont.removeClass('hidden');
	    }
	});
    }
    
    function Verificacion(Valor,ID) {
	var toke = '<?=Yii::$app->request->getCsrfToken()?>';
	$.ajax({
	    url: '<?= $aprobar ?>',
	    type: 'GET',
	    async: false,
	    data: {Valor:Valor,ID:ID,Tipo:4,_csrf: toke},
	    success: function(data){
	       	var $cont = $('.container-resp');
	        $cont.html('');
            var htm =  '<div class="alert alert-success">';
                htm += '<i class="fa fa-check"></i>&nbsp;El <strong>medio de verificación</strong> fue actualizada correctamente';
                htm += '</div>';
            $cont.html(htm);
            $cont.removeClass('hidden');
	    }
	});
    }
    
    function Supuestoss(Valor,ID) {
	var toke = '<?=Yii::$app->request->getCsrfToken()?>';
	$.ajax({
	    url: '<?= $aprobar ?>',
	    type: 'GET',
	    async: false,
	    data: {Valor:Valor,ID:ID,Tipo:5,_csrf: toke},
	    success: function(data){
	       	var $cont = $('.container-resp');
	        $cont.html('');
            var htm =  '<div class="alert alert-success">';
                htm += '<i class="fa fa-check"></i>&nbsp;Los <strong>Supuestos</strong> fue actualizada correctamente';
                htm += '</div>';
            $cont.html(htm);
            $cont.removeClass('hidden');
	    }
	});
    }
</script>