<div id="page-content" style="min-height: 754px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Marco lógico</h1>
        </div>
        <div class="container">
            <style>
                .panel .panel-body {
                    padding: 10px !important;
                }

                .panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 1px solid #a8a8ab !important;
                }

                .panel-gray .panel-body {
                    border-top: none !important;
                }

                .btn-sm {
                    padding: 5px 10px !important;
                    line-height: 1.5 !important;
                }

                .input-comp-nombre {
                    text-transform: uppercase;
                }

                .tbl-act > tbody > .tr-header > td {
                    text-align: center;
                    font-size: 12px;
                    font-weight: bold;
                    padding: 5px !important;
                    vertical-align: middle !important;
                    /*border: 1px solid #cfcfd0;
                    background-color: #f0f0f1;*/
                    border: 1px solid #c0c0c0;
                    background-color: #e3e3e3;
                }

                .tbl-act > thead > .tr-header > th {
                    text-align: center;
                    font-size: 12px;
                    font-weight: bold;
                    padding: 5px !important;
                    vertical-align: middle !important;
                    /*border: 1px solid #cfcfd0;
                    background-color: #f0f0f1;*/
                    border: 1px solid #c0c0c0;
                    background-color: #e3e3e3;
                }

                /*.tbl-act .tr-item td {
                    padding: 2px !important;
                }*/

                input.form-control, select.form-control {
                    height: 33px !important;
                }

                .tbl-act .tr-act .input-nombre {
                    min-width: 320px !important;
                }

                .tbl-act .tr-act .input-unidadmedida {
                    min-width: 100px !important;
                }

                .input-number {
                    min-width: 92px !important;
                }

                .btn-add-re {
                    float: right;
                }

                .tbl-act > tbody > tr > td:nth-child(2) {
                    min-width: 40px !important;
                    text-align: center !important;
                }

                .tr-comp input, .tr-proy input {
                    font-weight: bold;
                }

                .th-proy {
                    min-width: 630px !important;
                }
            </style>
            <!-- OBJETIVO Y FIN DEL PROYECTO -->
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4>Fin Y Propósito del proyecto</h4>
                </div>
                <div class="panel-body">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-1 control-label"><b>Fin:</b></label>
                                    <div class="col-sm-11">
                                        <textarea class="form-control" readonly="readonly"><?php echo ($infoGeneral->Fin) ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <button id="btn-add-finproyecto" class="btn btn-midnightblue-alt"><i class="fa fa-plus"></i>&nbsp;Agregar Indicador</button>
                            
                            <br><br>
                            <div id="div-table-finproyecto">
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-1 control-label"><b>Propósito:</b></label>
                                    <div class="col-sm-11">
                                        <textarea class="form-control" readonly="readonly"><?php echo ($infoGeneral->Proposito) ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <button id="btn-add-objetivoproyecto" class="btn btn-midnightblue-alt"><i class="fa fa-plus"></i>&nbsp;Agregar Indicador</button>
                            
                            <br><br>
                            <div id="div-table-objetivoproyecto">
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- FIN DEL PROYECTO -->
            <!-- OBJETIVOS -->
            <style>
                .panel .panel-body {
                    padding: 10px !important;
                }
                .panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 1px solid #a8a8ab !important;
                }
                .panel-gray .panel-body {
                    border-top: none !important;
                }
                .btn-sm {
                    padding: 5px 10px !important;
                    line-height: 1.5 !important;
                }
                .input-comp-nombre {
                    text-transform: uppercase;
                }
                .tbl-act > tbody > .tr-header > td {
                    text-align: center;
                    font-size: 12px;
                    font-weight: bold;
                    padding: 5px !important;
                    vertical-align: middle !important;
                    /*border: 1px solid #cfcfd0;
                    background-color: #f0f0f1;*/
                    border: 1px solid #c0c0c0;
                    background-color: #e3e3e3;
                }
                /*.tbl-act .tr-item td {
                    padding: 2px !important;
                }*/
                input.form-control, select.form-control {
                    height: 33px !important;
                }
                .tbl-act .tr-act .input-nombre {
                    min-width: 320px !important;
                }
                .tbl-act .tr-act .input-unidadmedida {
                    min-width: 100px !important;
                }
                .input-number {
                    min-width: 92px !important;
                }
                .btn-add-re {
                    float: right;
                }
                .tbl-act > tbody > tr > td:nth-child(2) {
                    min-width: 40px !important;
                    text-align: center !important;
                }
                .tr-comp input, .tr-proy input {
                    font-weight: bold;
                }
                .th-proy {
                    min-width: 630px !important;
                }
            </style>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4>OBJETIVO</h4>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <a href="javascript:void(0);" id="btn-add-comp" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>&nbsp;Agregar Objetivo</a>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-12">
                            <div id="div-marco-logico">
                            </div>
                        </div>
                        <div class="text-center">
                            <?= $infoGeneral->Codigo ?>
                            <a target="_blank" class="btn btn-inverse-alt" href="<?= Yii::$app->getUrlManager()->createUrl('marco-logico/descargar-marco-logico?CodigoProyecto='.$infoGeneral->CodigoProyecto.'') ?>">
                                <i class="fa fa-download"></i>&nbsp;Descargar Marco Lógico
                            </a>
                        </div>
                    </div>



                    <div class="modal fade" id="modal-edit-are" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-body-main">
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="modal fade" id="modal-ctrl-objetivo"  data-keyboard="false" data-backdrop="static"  tabindex="-1" role="dialog">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <h4 class="modal-title">OBJETIVO</h4>
                                </div>
                                <div class="modal-body-main">
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php /*if(\Yii::$app->user->identity->SituacionProyecto==1){ ?>
                    <button class="btn btn-primary btn-reprogramacion-marco-logico"><i class="fa fa-check"></i>&nbsp;Modificar marco lógico</button>
                    <?php }*/ ?>
                </div>
                
            </div>
            
            <!-- END OBJETIVOS -->
            <?php /*if(\Yii::$app->user->identity->Rol==3){?>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4>Evaluación UPPS</h4>
                    </div>
                    <div class="panel-body">
                        <div class="panel panel-primary">
                            <div class="panel-body">
                                <div class="form-horizontal"></div>
                                <button class="btn-aceptar btn btn-primary btn-sm">Aceptar</button>
                                <button class="btn-observar btn btn-primary btn-sm">Observar </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php }*/ ?>
        </div>
    </div>
</div>


<!--
<div id="modal-loading" class="modal-content-no-shadow modal fade modal-loading" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content-no-shadow modal-content">
            <div class="text-center"><i class="fa fa-spinner fa-pulse fa-fw" style="color:#16a085;font-size: 100px;"></i></div>
        </div>
    </div>
</div>
-->

<div class="modal fade" id="modal-finproyecto" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Indicador para Fin del Proyecto</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-objetivoproyecto" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Indicador para Propósito del Proyecto</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-actividadcomponente" tabindex="-1" role="dialog"  data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Actividad</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<?php
    $aprobar=Yii::$app->getUrlManager()->createUrl('informacion-general/aprobar-proyecto');
?>
<script>
    
    
    $(document).ready(function () {
        _pageLoadingStart();
        cargarFinProyectoTable();
        cargarObjetivoProyectoTable();
        // cargarComponenteTable();
        // cargarActividades();
        //loadPresupuesto();
        _pageLoadingEnd();
    });

    $(document).on('click','#btn-add-finproyecto',function () {
        $('#modal-finproyecto .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/marco-logico/finform');
        $('#modal-finproyecto').modal('show');
    });

    $(document).on('click','#btn-add-objetivoproyecto',function () {
        $('#modal-objetivoproyecto .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/marco-logico/propositoform');
        $('#modal-objetivoproyecto').modal('show');
    });

    function cargarFinProyectoTable() {
        $('#modal-finproyecto').modal('hide');
        $('#div-table-finproyecto').load('<?= \Yii::$app->request->BaseUrl ?>/marco-logico/finlistado?investigadorID=<?= $infoGeneral->InvestigadorID ?>');
    }

    function cargarObjetivoProyectoTable() {
        $('#modal-objetivoproyecto').modal('hide');
        $('#div-table-objetivoproyecto').load('<?= \Yii::$app->request->BaseUrl ?>/marco-logico/propositolistado?investigadorID=<?= $infoGeneral->InvestigadorID ?>');
        
    }
</script>





<script>
    var _proyecto = new Object();
    var _$rubrosElegibles = $();
    
    //var habilitar=<?= ($infoGeneral->getHabilitar()==1)?1:0; ?>;
    $(document).ready(function () {
        getProyecto().done(function(json) {
            _proyecto = json;
            drawTable();
            var situacionProyecto=<?= \Yii::$app->user->identity->SituacionProyecto ?>;
            if (situacionProyecto!=0) {
                $(".btn-add-actv,.btn-editar-compx,.btn-remove-comp,.btn-edit-activ,.btn-remove-act").hide();
            }
        });
        // buildSelectRubrosElegibles();
        setWhenReady();
    });

    // Botones

    $('#btn-add-comp').click(function (e) {
        e.preventDefault();
        $('#modal-ctrl-objetivo .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/plan-operativo/objetivoform');
        $('#modal-ctrl-objetivo').modal('show');
    });

    $('body').on('click', '.btn-remove-act', function (e) {
        e.preventDefault();
        var $this = $(this)
        //data-actividad_id
        var $tr = $this.closest('tr');
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        bootboxConfirmEliminar($this, 'Está seguro de eliminar la actividad? se eliminarán todos los datos asociados a ella.'
            , function () {
                $.post('marco-logico/eliminar-actividad'
               , {
                   id: $tr.attr('data-act-id'),
                   _csrf: toke
               }
               , function (data) {
                   // _pageLoadingStart();
                   limpiar_marco();
                   
               });
            });
    });

    $('body').on('click', '.btn-remove-comp', function (e) {
        e.preventDefault();
        var $this = $(this);
        var roleID = $this.attr('role-id');
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        bootboxConfirmEliminar($this, 'Está seguro de eliminar el objetivo? Se eliminarán todos los datos asociados a él.'
            , function () {
                $.post('marco-logico/eliminar-componente'
               , {
                   id: roleID,
                   _csrf: toke
               }
               , function (data) {
                    console.log(data);
                    limpiar_marco();
                   if (data.Success) {
                       _pageLoadingStart();
                       // location.reload();
                   }
               });
            });
    });

    $('body').on('click', '.btn-editar-compx', function (e) {
        e.preventDefault();
        var $this = $(this);
        var roleID = $this.attr('role-id');
        // alert(roleID);
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $('#modal-ctrl-objetivo .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/plan-operativo/objetivoform?id='+roleID);
        $('#modal-ctrl-objetivo').modal('show');
    });

    //btn-add-act-pred
    $('body').on('click', '.btn-add-act-pred', function (e) {
        e.preventDefault();
        var cid = $(this).attr('data-comp-id');
        $('#modal-rubros-pred #hdf-cid').val(cid);
        $('#modal-rubros-pred').modal('show');
    });

    $('body').on('click', '.btn-add-actv', function (e) {
        e.preventDefault();
        var cid = $(this).parent().attr('data-comp-id');
        var componente=cid;
        
        var actividad='';
        $('#modal-actividadcomponente .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/marco-logico/actividadform?ComponenteID='+componente+'&ActividadID='+actividad);
        $('#modal-actividadcomponente').modal('show');
    });

    $('body').on('click', '.btn-edit-activ', function (e) {
        e.preventDefault();
        var cid = $(this).parent().parent().parent().attr('data-comp-id');
        var actv = $(this).attr('data-actividad_id');

        var componente=cid;
        var actividad=actv;
        console.log("-"+componente);
        console.log("act:"+actividad);
        $('#modal-actividadcomponente .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/marco-logico/actividadform?ComponenteID='+componente+'&ActividadID='+actividad);

        $('#modal-actividadcomponente').modal('show');
    });
        

    $(document).on('click','.btn-save-objetivoproyecto',function (e) {
        e.preventDefault();
        var $btn = $(this);
        var isValid = formParsleyObjetivoProyecto.validate();
        if (isValid) {
            sendFormNoMessage($('#frmObjetivoProyecto'), $(this), function (dato) {
                $('#componente-indicador-id').val(dato.id);

                if(dato.id != ''){
                    cargarIndicadoresObjetivo(dato.id);
                    $('#frmObjetivoProyecto').attr('action','plan-operativo/actualiza-objetivo?id='+dato.id);
                    $btn.text('Actualizar Objetivo');
                }else{
                    cargarIndicadoresObjetivo(0);
                }

                // $btn.attr('disabled', 'disabled');
                // $('#IndicadorTipo').attr('disabled', 'disabled');
                
                $('#indicador-form').removeClass('hidden');
                $('.indica').addClass('hidden');
                // cargarObjetivoProyectoTable();
                // _pageLoadingEnd();
                // 
                limpiar_marco();
                
            });
        } else {
            $btn.removeAttr('disabled');
        }
    });

    $(document).on('click','.btn-save-act-proyecto',function (e) {
        e.preventDefault();
        var $btn = $(this);
        console.log($btn);
        $('#act-btn').prop('disabled', true);

        var isValid = formParsleyActividadProyecto.validate();
        if (isValid) {
            sendFormNoMessage($('#frmActividadComponente'), $(this), function (data) {
                limpiar_marco();
                $('#actividad-indicador-id').val(data.ID);
                $('#IndicadorTipo').prop('disabled', true);
                $('#indicador-form-actv').removeClass('hidden');
                $('.indica').addClass('hidden');
                if(data.ID != ''){
                    cargarIndicadoresActividad(data.ID);
                }else{
                    cargarIndicadoresActividad(0);
                }
                $('#frmActividadComponente').attr('action','marco-logico/actividad-actualizar?id='+data.ID)
                // $("#modal-actividadcomponente").modal('hide');
                _pageLoadingEnd();
            });
        } else {
            $btn.removeAttr('disabled');
        }
    });


    $(document).on('click','#btn-save-indicador',function (e) {
        e.preventDefault();
        var $btn = $(this);
        $btn.attr('disabled', 'disabled');
        var isValid2 = formParsleyObjetivoProyecto1.validate();
        if (isValid2) {
            sendFormNoMessage($('#frmObjetivoProyectoIndicador'), $(this), function (dato) {
                $('#table_indica_obj').html('');
                // console.log(dato.id);
                var xxx = $('#componente-indicador-id').val();
                cargarIndicadoresObjetivo(xxx)

                $('#frmObjetivoProyectoIndicador')[0].reset();
                limpiar_marco();
                $btn.removeAttr('disabled');
            });
        }else {
            $btn.removeAttr('disabled');
        }
    });


    $(document).on('click','#btn-save-indicador-act',function (e) {
        e.preventDefault();
        var $btn = $(this);
        $btn.attr('disabled', 'disabled');
        
        var isValid2 = formParsleyActividadProyecto1.validate();
        if (isValid2) {
        sendFormNoMessage($('#frmActividadComponenteIndicadores'), $(this), function (dato) {
            $('#table_indica_obj').html('');
            // console.log(dato.id);
            limpiar_marco();
            var xxx = $('#actividad-indicador-id').val();
            $('#frmActividadComponenteIndicadores')[0].reset();
            // alert(xxx);
            cargarIndicadoresActividad(xxx)
            // cargarIndicadoresActividad();
            $btn.removeAttr('disabled');
        });
        }else {
            $btn.removeAttr('disabled');
        }
    });


    $('body').on('click', '.btn-remove-ind', function (e) {
        e.preventDefault();
        var $this = $(this);
        bootboxConfirmEliminar($this,
        'Está seguro de eliminar la indicador?',
        function () {
            var epID = $this.attr('data-delete-comp-id');
            var CompID = $this.attr('data-comp-id');
            var toke = '<?=Yii::$app->request->getCsrfToken()?>';
            $.post('<?= \Yii::$app->request->BaseUrl ?>/plan-operativo/eliminar-indicador-objetivo/', { id: epID, comp:CompID, _csrf: toke }, function (data) {
                var jx = JSON.parse(data);
                if (jx.Success == true) {
                    cargarIndicadoresObjetivo(jx.comp);
                    limpiar_marco();
                }
            });
        });
    });


    $('body').on('click', '.btn-remove-ind-act', function (e) {
        e.preventDefault();
        var $this = $(this);
        bootboxConfirmEliminar($this,
        'Está seguro de eliminar la indicador?',
        function () {
            var epID = $this.attr('data-delete-comp-id');
            var CompID = $this.attr('data-comp-id');
            var toke = '<?=Yii::$app->request->getCsrfToken()?>';
            $.post('<?= \Yii::$app->request->BaseUrl ?>/marco-logico/eliminar-indicador-actividad/', { id: epID, comp:CompID, _csrf: toke }, function (data) {
                var jx = JSON.parse(data);
                if (jx.Success == true) {
                    cargarIndicadoresActividad(jx.comp);
                    limpiar_marco();
                }
            });
        });
    });



    function limpiar_marco(){
        $("#div-marco-logico").html('');
        getProyecto().done(function(json) {
            _proyecto = json;
            console.log(_proyecto);
            drawTable();
        });
    }

    function setWhenReady() {
        $('.input-number').each(function () {
            var valor = parseFloat($(this).val());
            $(this).val(parseFloat(valor.toFixed(2)));
            if (valor < 0) {
                $(this).css('color', 'red');
                $(this).css('border-color', 'red');
            }
        });
        $('.input-number').inputmask("decimal", {
            radixPoint: ".",
            groupSeparator: ",",
            groupSize: 3,
            digits: 2,
            integerDigits: 7,
            autoGroup: true,
            allowPlus: false,
            allowMinus: true,
            placeholder: ''
        }).click(function () {
            $(this).select();
        });

        $('[data-toggle="tooltip]').tooltip();

        //$('.table-responsive').css('max-height', $(window).height() * 0.45);
        $('.table-responsive').css('max-height', $(window).height() * 0.60);

        //
        $('.select-are:not(:last) option[value="9"]').remove();
        $('.select-are:not([disabled]) option[value="1"]:not(:selected)').remove();
        var $areh = $('.select-are option[value="1"]:selected');
        if ($areh.length) {
            $('a.btn-add-act-pred').remove();
            var arehactid = $areh.closest('tr').attr('data-act-id');
            $('tr[data-act-id="' + arehactid + '"] .input-nombre').attr('disabled', 'disabled');
            $('tr[data-act-id="' + arehactid + '"].tr-act .input-metafisica').attr('disabled', 'disabled');
            $('tr[data-act-id="' + arehactid + '"] .select-are').attr('disabled', 'disabled');
            $('tr[data-act-id="' + arehactid + '"] a:not(.btn-remove-act)').remove();
        }

        var $divGP = $('.panel-comp:last');
        $divGP.find('a').remove();
        $divGP.find('select, input').attr('disabled', 'disabled');

        var trproytotalpnia = parseFloat($('.tr-proy .input-totalfinanciamientopnia').first().inputmask('unmaskedvalue'));
        
        if (trproytotalpnia > 196000) {
            $('.tr-proy .input-totalfinanciamientopnia').first().css('color', 'red');
        }
    }

    function drawTable() {
        var _conta_aporte_aplica = 0;
        
        for (var i_comp = 0; i_comp < _proyecto.Componentes.length; i_comp++) {
            var componente = _proyecto.Componentes[i_comp];
            var $panel = $('<div class="panel panel-gray panel-comp"></div>');
            
           // if (habilitar==1) {
                var $panelheading = $('<div class="panel-heading"> <a href="javascript:void(0);" class="btn-editar-compx" role-id="' + componente.ID + '"><i class="fa fa-edit"></i> </a> <a href="javascript:void(0);" class="btn-remove-comp" role-id="' + componente.ID + '"><i class="fa fa-remove"></i> </a> Objetivo ' + componente.Correlativo
                + '<div class="btn-heading">'
                +' <b>Peso:</b> '+(componente.Peso).toFixed(2) + ' '
                + '</div></div>');
           /* }
            else if (habilitar==0)
            {
                var $panelheading = $('<div class="panel-heading">Objetivo ' + (i_comp + 1)
                + '<div class="btn-heading">'
                + '</div></div>');
            }*/
            
            
            var $panelbody = $('<div class="panel-body"></div>');
            var $forminputcomp = $('<div class="form-horizontal">'
            + '<div class="form-group"><label class="col-sm-1 control-label">Objetivo:</label>'
            + '<div class="col-sm-6"><textarea data-comp-id="' + componente.ID + '" '
            + 'class="form-control input-comp-nombre"  required="" placeholder="Ingrese Objetivo...">' + componente.Nombre+'</textarea></div>' 

            + '<div class="col-sm-2"><a href="javascript:void(0);" class="btn btn-sm btn-info btn-add-act"'
            + ' data-comp-id="' + componente.ID + '" style="width: 100%">'
            + '<i class="fa fa-plus"></i>&nbsp;Actividad</a></div></div>');

            var $div_table = $('<div class="table-responsive div-tbl-comp"></div>');
            
            var $table = $('<table class="tbl-act table"><tbody></tbody></table>');

                var $tr_thead_0 = getTR_Header();
                // $tr_thead_0.append(getTD().text(''));
                $tr_thead_0.append(getTD().attr('colspan', '3').text('Objetivo'));
                $tr_thead_0.append(getTD().text('Meta Física'));
                $tr_thead_0.append(getTD().text('Indicador'));
                $tr_thead_0.append(getTD().text('Unidad de medida'));
                $tr_thead_0.append(getTD().text('Medios de verificación'));
                $tr_thead_0.append(getTD().text('Supuestos'));
                $table.append($tr_thead_0);

                var totIndi = componente.Indicadores.length;
                var _x = 0;
               
            if(totIndi == 0){
                var $tr_act00 = getTR_Item().addClass('tr-act').addClass('warning').attr({ 'data-comp-id': componente.ID});
                $tr_act00.append(getTD('style="text-align:left !important"').attr('rowspan', totIndi).attr('colspan', '3').append(componente.Nombre));
                $tr_act00.append(getTD('style="text-align:center !important"').append(''));
                $tr_act00.append(getTD('style="text-align:center !important"').append(''));
                $tr_act00.append(getTD('style="text-align:center !important"').append(''));
                $tr_act00.append(getTD('style="text-align:center !important"').append(''));
                $tr_act00.append(getTD('style="text-align:center !important"').append(''));
                $table.append($tr_act00);
            }else{
                for (var i_act0 = 0; i_act0 < componente.Indicadores.length; i_act0++) {
                    var indicadorx = componente.Indicadores[i_act0];
                    var $tr_act00 = getTR_Item().addClass('tr-act').addClass('warning').attr({ 'data-comp-id': componente.ID,'data-act-id': indicadorx.ID});
                    if(_x == 0){
                        // $tr_act00.append(getTD());
                        $tr_act00.append(getTD('style="text-align:left !important"').attr('rowspan', totIndi).attr('colspan', '3').append(componente.Nombre));
                    }else{
                        //$tr_act00.append(getTD('style="text-align:left !important"').attr('colspan', '2'));
                    }
                    $tr_act00.append(getTD('style="text-align:center !important"').append(indicadorx.IndicadorMeta));
                    $tr_act00.append(getTD('style="text-align:center !important"').append(indicadorx.IndicadorDescripcion));
                    $tr_act00.append(getTD('style="text-align:center !important"').append(indicadorx.IndicadorUnidadMedida));
                    
                    $tr_act00.append(getTD('style="text-align:center !important"').append(indicadorx.MedioVerificacion));
                    $tr_act00.append(getTD('style="text-align:center !important"').append(indicadorx.Supuestos));
                    $table.append($tr_act00);
                    _x++;
                }
            }

            var $tr_thead_t = getTR_Item();
            //if (habilitar==1) {
                $tr_thead_t.append(getTD().attr({ 'data-comp-id': componente.ID, 'colspan':'7' }).html('<a href="javascript:void(0);" class="btn-add-actv btn btn-primary btn-sm"><i class="fa fa-plus"></i>&nbsp;Agregar Actividades</a>'));
            /*}
            else if (habilitar==0) {
                $tr_thead_t.append(getTD().attr({ 'data-comp-id': componente.ID, 'colspan':'7' }).html(''));
            }*/
            $table.append($tr_thead_t);
         
            for (var i_act = 0; i_act < componente.Actividades.length; i_act++) {
                _conta_aporte_aplica = 0;
                var actividad = componente.Actividades[i_act];
                var $tr_thead_1 = getTR_Header().attr({ 'data-comp-id': componente.ID, 'data-act-id': actividad.ID });

                if (i_act==0) {
                    $tr_thead_1.append(getTD().text('#'));
                    $tr_thead_1.append(getTD().attr('colspan', '2').text('Actividad'));
                    $tr_thead_1.append(getTD().text('Meta Física'));
                    $tr_thead_1.append(getTD().text('Indicador'));
                    $tr_thead_1.append(getTD().text('Unidad de medida'));
                    
                    $tr_thead_1.append(getTD().text('Medios de verificación'));
                    $tr_thead_1.append(getTD().text('Supuestos'));
                }
                var $tr_thead_2 = getTR_Header().attr({ 'data-comp-id': componente.ID, 'data-act-id': actividad.ID });
                var $tr_thead_3 = getTR_Header().attr({ 'data-comp-id': componente.ID, 'data-act-id': actividad.ID });
                
                $table.append($tr_thead_1);
                $table.append($tr_thead_2);
                $table.append($tr_thead_3);

                var totA = actividad.Indicadores.length;
                var _y = 0;
                
                if(totA == 0){
                    var $tr_act = getTR_Item().addClass('tr-act').addClass('info').attr({ 'data-comp-id': componente.ID, 'data-act-id': actividad.ID });

                    $tr_act.append(getTD('style="text-align:center;width: 80px;"').html('<a href="javascript:void(0);"><i class="fa fa-edit btn-edit-activ" data-actividad_id='+actividad.ID+' ></i></a><a href="javascript:void(0);" data-toggle="tooltip" title="Eliminar" class="btn-remove-act" data-actividad_id='+actividad.ID+'>'
                        + '<i class="fa fa-remove"></i></a> <b>' + (componente.Correlativo) + '.' + (actividad.Correlativo) + '</b>'));
                    $tr_act.append(getTD('style="text-align:left !important"').attr({ 'rowspan': totA, 'colspan': 2 }).append(actividad.Nombre));
                    $tr_act.append(getTD('style="text-align:center !important"').append(''));
                    $tr_act.append(getTD('style="text-align:center !important"').append(''));
                    $tr_act.append(getTD('style="text-align:center !important"').append(''));
                    $tr_act.append(getTD('style="text-align:center !important"').append(''));
                    $tr_act.append(getTD('style="text-align:center !important"').append(''));
                    $table.append($tr_act);
                }else{
                    for (var i_acta = 0; i_acta < actividad.Indicadores.length; i_acta++) {
                        var $tr_act = getTR_Item().addClass('tr-act').addClass('info').attr({ 'data-comp-id': componente.ID, 'data-act-id': actividad.ID });
                        var indi_act = actividad.Indicadores[i_acta];
                        // console.log(_y);
                        if(_y == 0){
                            $tr_act.append(getTD('style="text-align:center;width: 80px;"').html('<a href="javascript:void(0);"><i class="fa fa-edit btn-edit-activ" data-actividad_id='+actividad.ID+' ></i></a><a href="javascript:void(0);" data-toggle="tooltip" title="Eliminar" class="btn-remove-act" data-actividad_id='+actividad.ID+'>'
                                + '<i class="fa fa-remove"></i></a> <b>' + (componente.Correlativo) + '.' + (actividad.Correlativo) + '</b>'));
                            
                            // $tr_act.append(getTD('style="text-align:left !important"').attr('rowspan', totA).append(actividad.Nombre));
                            $tr_act.append(getTD('style="text-align:left !important"').attr({ 'rowspan': totA, 'colspan': 2 }).append(actividad.Nombre));
                        }else{
                            $tr_act.append(getTD());
                        }
                        _y++;
                        // console.log(indi_act);
                        $tr_act.append(getTD('style="text-align:center !important"').append(indi_act.IndicadorMeta));
                        $tr_act.append(getTD('style="text-align:center !important"').append(indi_act.IndicadorDescripcion));
                        $tr_act.append(getTD('style="text-align:center !important"').append(indi_act.IndicadorUnidadMedida));
                        
                        $tr_act.append(getTD('style="text-align:center !important"').append(indi_act.MedioVerificacion));
                        $tr_act.append(getTD('style="text-align:center !important"').append(indi_act.Supuestos));
                        $table.append($tr_act);
                    }
                }

            }
            
           
            
            $div_table.append($table);
            $panelbody.append($div_table);

            $panel.append($panelheading);
            $panel.append($panelbody);
            
            
    
    
            $('#div-marco-logico').append($panel);
            var desab='<?= $deshabilitar ?>';
            console.log(desab);
            if (desab) {
                console.log(desab);
                $("#container-marco-logico button").hide();
                $(".btn-editar-compx,#btn-add-comp,.btn-remove-comp,.btn-edit-activ,.btn-remove-act,.btn-add-actv").hide();
                
            }
        }
    }

    function getProyecto() {
        var url = "<?= \Yii::$app->request->BaseUrl ?>/plan-operativo/marco?investigadorID=<?= $infoGeneral->InvestigadorID ?>";
        return $.getJSON( url );
    }

    function getRubrosElegibles() {
        
        return jQuery.parseJSON($('#jsonRubrosElegibles').val());
    }

    function buildSelectRubrosElegibles() {
        _$rubrosElegibles = $('<select class="form-control"></select>').css('padding-top', '3px');
        $.each(getRubrosElegibles(), function (key, value) {
            _$rubrosElegibles
                .append($("<option></option>")
                           .attr("value", value.ID)
                           .text(value.Nombre));
        });
    }

    function getTR_Header() {
        return $('<tr class="tr-header"></tr>');
    }
    function getTR_Item() {
        return $('<tr class="tr-item"></tr>');
    }

    function getTH() {
        return $('<th></th>');
    }

    function getTD(s = '') {
        if(s != '' || s != undefined){
            return $('<td '+s+'></td>');
        }else{
            return $('<td></td>');
        }
    }

    function getTDvacios(q) {
        var html = '';
        for (var i = 0; i < q; i++) {
            html += '<td>&nbsp;</td>';
        }
        return html;
    }

    function getInputText() {
        return $('<input type="text" class="form-control" />');
    }

    function getTextArea(){
        return $('<textarea class="form-control"></textarea')
    }

    function getInputTextNumber() {
        return $('<input type="text" class="form-control input-number" />');
    }
    
    var situacionProyecto=<?= \Yii::$app->user->identity->SituacionProyecto ?>;
    if (situacionProyecto!=0) {
        //$(".btn-add-actv,.btn-editar-comp,.btn-remove-comp,.btn-edit-activ,.btn-remove-act").hide();
        $(".container input,.container textarea,.container select").prop("disabled", true);
        $("#btn-add-comp,#btn-add-objetivoproyecto,#btn-add-finproyecto").hide();
    }
    
    $(document).ready(function () {
        
        $('.btn-aceptar').click(function (e) {
            var $btn = $(this);
            bootbox.confirm({
                title: '<span style="text-align:center">PNIA - CONFIRMACIÓN</span>',
                message: 'Está seguro de aprobar el proyecto?.',
                buttons: {
                    'cancel': {
                        label: 'CANCELAR'
                    },
                    'confirm': {
                        label: 'APROBAR PROYECTO'
                    }
                },
                callback: function (confirmed) {
                    if (confirmed) {
                        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                        $.ajax({
                            url: '<?= $aprobar ?>',
                            type: 'GET',
                            async: false,
                            data: {aprobar:1,codigo:'<?= $infoGeneral->CodigoProyecto ?>',_csrf: toke},
                            success: function(data){
                               
                            }
                        });
                    } 
                }
            });
        });
    });


    
    
</script>