<?php if(!empty($PropositoDetalle)): ?>
    <table id="tbl-ml-objetivoproyecto" class="table table-bordered table-condensed tbl-act">
        <tbody>
            <tr class="tr-header">
                <td rowspan="2"></td>
                <td colspan="2">Indicador</td>
                <td rowspan="2">Medio de verificación</td>
                <td rowspan="2">Supuestos</td>
            </tr>
            <tr class="tr-header">
                <!-- <td>Meta</td> -->
                <td>Nombre Indicador</td>
                <td>Unidad de Medida</td>
            </tr>
            <?php $ii=1; foreach ($PropositoDetalle as $proposito): ?>
                <tr>
                    <td class="td-acciones">
                        <?php if(\Yii::$app->user->identity->rol==7){ ?>
                            <a href="#" class="btn-edit-ml-objetivoproyecto" data-toggle="tooltip" title="Editar" role-id="<?php echo $proposito->ID ?>"><i class="fa fa-edit fa-lg"></i></a>&nbsp;
                            <a href="#" class="btn-remove-ml-objetivoproyecto" data-toggle="tooltip" title="Eliminar" role-id="<?php echo $proposito->ID ?>"><i class="fa fa-remove fa-lg"></i></a>
                        <?php } ?>
                    </td>
                    <!-- <td><?php echo $proposito->IndicadorMeta ?></td> -->
                    <td><?php echo $proposito->IndicadorDescripcion ?></td>
                    <td><?php echo $proposito->IndicadorUnidadMedida ?></td>
                    <td><?php echo $proposito->MedioVerificacion ?></td>
                    <td><?php echo $proposito->Supuestos ?></td>
                </tr>
            <?php $ii++;endforeach ?>
        </tbody>
    </table>


    <script>
        var situacionProyecto=<?= \Yii::$app->user->identity->SituacionProyecto ?>;
        if (situacionProyecto!=0) {
            $(".container input,.container textarea,.container select").prop("disabled", true);
            $(".btn-edit-ml-objetivoproyecto,.btn-remove-ml-objetivoproyecto").hide();
        }
        $('.btn-edit-ml-objetivoproyecto').click(function (e) {
            e.preventDefault();
            var id = $(this).attr('role-id');
            $('#modal-objetivoproyecto .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/marco-logico/propositoform?id=' + id);
            $('#modal-objetivoproyecto').modal('show');
        });

        $(document).on('click','.btn-remove-ml-objetivoproyecto',function (e) {
            e.preventDefault();
            var $this = $(this);
            bootboxConfirmEliminar($this,
                'Está seguro de eliminar el indicador seleccionado?',
                function () {
                    var id = $this.attr('role-id');
                    var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                    $.post('<?= \Yii::$app->request->BaseUrl ?>/marco-logico/propositodelete', { id: id,_csrf: toke }, function (data) {
                        var jx = JSON.parse(data);
                        if (jx.Success == true) {
                            cargarObjetivoProyectoTable();
                        }
                    });
                });
        });
    </script>

<?php else: ?>
    <div class="alert alert-info"><i class="fa fa-info"></i>&nbsp;No se registraron indicadores para el Propósito del Proyecto.</div>
<?php endif ?>