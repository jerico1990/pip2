<div id="page-content" style="min-height: 754px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Tarea</h1>
        </div>
        <div class="container">
            
            <!-- OBJETIVOS -->
            <style>
                .panel .panel-body {
                    padding: 10px !important;
                }

                .panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 1px solid #a8a8ab !important;
                }

                .panel-gray .panel-body {
                    border-top: none !important;
                }

                .btn-sm {
                    padding: 5px 10px !important;
                    line-height: 1.5 !important;
                }

                .input-comp-nombre {
                    text-transform: uppercase;
                }

                .tbl-act > tbody > .tr-header > td {
                    text-align: center;
                    font-size: 12px;
                    font-weight: bold;
                    padding: 5px !important;
                    vertical-align: middle !important;
                    /*border: 1px solid #cfcfd0;
                    background-color: #f0f0f1;*/
                    border: 1px solid #c0c0c0;
                    background-color: #e3e3e3;
                }

                /*.tbl-act .tr-item td {
                    padding: 2px !important;
                }*/
                input.form-control, select.form-control {
                    /*height: 28px !important;*/
                }

                .tbl-act .tr-act .input-nombre {
                    min-width: 320px !important;
                }

                .tbl-act .tr-act .input-unidadmedida {
                    min-width: 100px !important;
                }

                .input-number {
                    min-width: 92px !important;
                }

                .btn-add-re {
                    float: right;
                }

                .tbl-act > tbody > tr > td:nth-child(2) {
                    min-width: 40px !important;
                    text-align: center !important;
                }

                .tr-comp input, .tr-proy input {
                    font-weight: bold;
                }

                .th-proy {
                    min-width: 630px !important;
                }
            </style>

             <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4>Recursos del POA</h4>
                </div>
                <div class="panel-body">
                        <div id="select_obj">
                            
                        </div>
                    <div class="row">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-2 control-label text-right">Objetivo:</label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="btn-change-comp" >
                                        <option value="0">[SELECCIONAR]</option>
                                        <?php $i = 1;foreach($componentes as $componente){ ?>
                                        <option value="<?= $componente->ID ?>"><?= $i.".- ".$componente->Nombre ?></option>
                                        <?php $i++;} ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-sm-12">
                            <div id="div-presupuesto">
                                <div class="alert alert-info"><i class="fa fa-info"></i>&nbsp;Seleccione Objetivo Especifico.</div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="modal-ctrl-recurso" tabindex="-1" role="dialog">
                        <div class="modal-dialog " role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <h4 class="modal-title">OBJETIVO</h4>
                                </div>
                                <div class="modal-body">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- END OBJETIVOS -->
        </div>
    </div>
</div>




<script>
    var _proyecto = new Object();
    var _$rubrosElegibles = $();

    $(document).ready(function () {
        // getProyecto().done(function(json) {
            // _proyecto = json;
            // drawTable();
        // });

        setWhenReady();
    });

    $(document).on('change','#btn-change-comp',function(){
        $('#div-presupuesto, #select_obj').html('');
        var s = $(this).val();
        var _x = 0;
        if(s == 0){
            $('#div-presupuesto').html('<div class="alert alert-warning"><i class="fa fa-info"></i>&nbsp;Seleccione un Objetivo.</div>');
        }else{
            getProyecto(s).done(function(json) {
                if(json.Actividades.length == 0){
                    $('#div-presupuesto').html('<div class="alert alert-info"><i class="fa fa-info"></i>&nbsp;No tiene registrado actividades en este Objetivo Especifico.</div>');
                }else{
                    _proyecto = json;
                    drawTable();
                }
            });
        }

    });


    // Botones
        
        $(document).on('click','.btn-add-recurso',function (e) {
            e.preventDefault();
            $('#modal-ctrl-recurso .modal-body').load('<?= \Yii::$app->request->BaseUrl ?>/recursos-poa/recursos-form');
            $('#modal-ctrl-recurso').modal('show');
        });



        $('body').on('click', '.btn-remove-act', function (e) {
            e.preventDefault();
            var $this = $(this)
            //data-actividad_id
            var $tr = $this.closest('tr');
            var toke = '<?=Yii::$app->request->getCsrfToken()?>';
            bootboxConfirmEliminar($this, 'Está seguro de eliminar la actividad? se eliminarán todos los datos asociados a ella.'
                , function () {
                    $.post('marco-logico/eliminar-actividad'
                   , {
                       id: $tr.attr('data-act-id'),
                       _csrf: toke
                   }
                   , function (data) {
                       _pageLoadingStart();
                       location.reload();
                       
                   });
                });
        });


        $('body').on('click', '.btn-remove-comp', function (e) {
            e.preventDefault();
            var $this = $(this);
            var roleID = $this.attr('role-id');
            var toke = '<?=Yii::$app->request->getCsrfToken()?>';
            bootboxConfirmEliminar($this, 'Está seguro de eliminar el objetivo? Se eliminarán todos los datos asociados a él.'
                , function () {
                    $.post('marco-logico/eliminar-componente'
                   , {
                       id: roleID,
                       _csrf: toke
                   }
                   , function (data) {
                       if (data.Success) {
                           _pageLoadingStart();
                           location.reload();
                       }
                   });
                });
        });


        $('body').on('click', '.btn-editar-comp', function (e) {
            e.preventDefault();
            var $this = $(this);
            var roleID = $this.attr('role-id');
            var toke = '<?=Yii::$app->request->getCsrfToken()?>';
            $('#modal-ctrl-objetivo .modal-body').load('<?= \Yii::$app->request->BaseUrl ?>/plan-operativo/objetivoform?id='+roleID);
            $('#modal-ctrl-objetivo').modal('show');
        });


 
        //btn-add-act-pred
        $('body').on('click', '.btn-add-act-pred', function (e) {
            e.preventDefault();
            var cid = $(this).attr('data-comp-id');
            $('#modal-rubros-pred #hdf-cid').val(cid);
            $('#modal-rubros-pred').modal('show');
        });

        
        $('body').on('click', '.btn-add-actv', function (e) {
            e.preventDefault();
            var cid = $(this).parent().attr('data-comp-id');
            var componente=cid;
            var actividad='';
            $('#modal-actividadcomponente .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/marco-logico/actividadform?ComponenteID='+componente+'&ActividadID='+actividad);
            $('#modal-actividadcomponente').modal('show');
        });

        $('body').on('click', '.btn-edit-activ', function (e) {
            e.preventDefault();
            var cid = $(this).parent().attr('data-comp-id');
            var actv = $(this).attr('data-actividad_id');
            
            var componente=cid;
            var actividad=actv;
            $('#modal-actividadcomponente .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/marco-logico/actividadform?ComponenteID='+componente+'&ActividadID='+actividad);
            $('#modal-actividadcomponente').modal('show');
        });
        

    function setWhenReady() {
        $('.input-number').each(function () {
            var valor = parseFloat($(this).val());
            $(this).val(parseFloat(valor.toFixed(2)));
            if (valor < 0) {
                $(this).css('color', 'red');
                $(this).css('border-color', 'red');
            }
        });
        $('.input-number').inputmask("decimal", {
            radixPoint: ".",
            groupSeparator: ",",
            groupSize: 3,
            digits: 2,
            integerDigits: 7,
            autoGroup: true,
            allowPlus: false,
            allowMinus: true,
            placeholder: ''
        }).click(function () {
            $(this).select();
        });

        $('[data-toggle="tooltip]').tooltip();

        //$('.table-responsive').css('max-height', $(window).height() * 0.45);
        $('.table-responsive').css('max-height', $(window).height() * 0.60);

        //
        $('.select-are:not(:last) option[value="9"]').remove();
        $('.select-are:not([disabled]) option[value="1"]:not(:selected)').remove();
        var $areh = $('.select-are option[value="1"]:selected');
        if ($areh.length) {
            $('a.btn-add-act-pred').remove();
            var arehactid = $areh.closest('tr').attr('data-act-id');
            $('tr[data-act-id="' + arehactid + '"] .input-nombre').attr('disabled', 'disabled');
            $('tr[data-act-id="' + arehactid + '"].tr-act .input-metafisica').attr('disabled', 'disabled');
            $('tr[data-act-id="' + arehactid + '"] .select-are').attr('disabled', 'disabled');
            $('tr[data-act-id="' + arehactid + '"] a:not(.btn-remove-act)').remove();
        }

        var $divGP = $('.panel-comp:last');
        $divGP.find('a').remove();
        $divGP.find('select, input').attr('disabled', 'disabled');

        var trproytotalpnia = parseFloat($('.tr-proy .input-totalfinanciamientopnia').first().inputmask('unmaskedvalue'));
        console.log(trproytotalpnia);
        if (trproytotalpnia > 196000) {
            $('.tr-proy .input-totalfinanciamientopnia').first().css('color', 'red');
        }
    }

    function drawTable() {
        var _conta_aporte_aplica = 0;
        for (var i_comp = 0; i_comp < _proyecto.Actividades.length; i_comp++) {
            var actividada = _proyecto.Actividades[i_comp];
                // console.log(actividada);
            var $panel = $('<div class="panel panel-gray panel-comp"></div>');
            var $panelheading = $('<div class="panel-heading">Actividad ' + (i_comp + 1)
                + '</div>');
            var $panelbody = $('<div class="panel-body"></div>');
            var $forminputcomp = $('<div class="form-horizontal">'
            + '<div class="form-group"><label class="col-sm-1 control-label">Objetivo:</label>'
            + '<div class="col-sm-6"><textarea data-comp-id="' + actividada.ID + '" '
            + 'class="form-control input-comp-nombre"  required="" placeholder="Ingrese Objetivo...">' + actividada.Nombre+'</textarea></div>' 

            + '<div class="col-sm-2"><a href="javascript:void(0);" class="btn btn-sm btn-info btn-add-act"'
            + ' data-comp-id="' + actividada.ID + '" style="width: 100%">'
            + '<i class="fa fa-plus"></i>&nbsp;Actividad</a></div></div>');

            var $div_table = $('<div class="table-responsive div-tbl-comp"></div>');
            console.log(actividada);
            var $table = $('<table class="tbl-act table"><tbody></tbody></table>');

                var $tr_thead_0 = getTR_Header();
                $tr_thead_0.append(getTD().attr('colspan', '3').text('Actividad'));
                // $tr_thead_0.append(getTD().text('Descripcion'));
                $tr_thead_0.append(getTD().text('Unidad de medida'));
                $tr_thead_0.append(getTD().text('Meta Física'));
                $tr_thead_0.append(getTD().text('Medios de verificación'));
                $tr_thead_0.append(getTD().text('Supuestos'));
                $table.append($tr_thead_0);

                var totIndi = actividada.Indicadores.length;
                var _x = 0;
               
            for (var i_act0 = 0; i_act0 < actividada.Indicadores.length; i_act0++) {
                var indicadorx = actividada.Indicadores[i_act0];
                var $tr_act00 = getTR_Item().addClass('tr-act').addClass('warning').attr({ 'data-comp-id': actividada.ID,'data-act-id': indicadorx.ID});
                if(_x == 0){
                    $tr_act00.append(getTD());
                    $tr_act00.append(getTD('style="text-align:left !important"').attr('rowspan', totIndi).append(actividada.Nombre));
                }else{
                    $tr_act00.append(getTD('style="text-align:left !important"'));
                }
                $tr_act00.append(getTD('style="text-align:center !important"').append(indicadorx.IndicadorDescripcion));
                $tr_act00.append(getTD('style="text-align:center !important"').append(indicadorx.IndicadorUnidadMedida));
                $tr_act00.append(getTD('style="text-align:center !important"').append(indicadorx.IndicadorMeta));
                $tr_act00.append(getTD('style="text-align:center !important"').append(indicadorx.MedioVerificacion));
                $tr_act00.append(getTD('style="text-align:center !important"').append(indicadorx.Supuestos));
                $table.append($tr_act00);
                _x++;
            }
               

            var $tr_thead_t = getTR_Item();
            $tr_thead_t.append(getTD().attr({ 'data-actv-id': actividada.ID}).html('<a href="javascript:void(0);" class="btn-add-recurso btn btn-primary btn-sm"><i class="fa fa-plus"></i>&nbsp;Agregar Tarea</a>'));
            $table.append($tr_thead_t);
         
            for (var i_act = 0; i_act < actividada.Recursos.length; i_act++) {
                _conta_aporte_aplica = 0;
                var actividad = actividada.Recursos[i_act];
                var $tr_thead_1 = getTR_Header().attr({ 'data-comp-id': actividada.ID, 'data-act-id': actividad.ID });

                if (i_act==0) {
                    $tr_thead_1.append(getTD().text('Clasificador'));
                    $tr_thead_1.append(getTD().text('Descripcion'));
                    $tr_thead_1.append(getTD().text('Fuente'));
                    $tr_thead_1.append(getTD().text('Unidad'));
                    $tr_thead_1.append(getTD().text('Cantidad'));
                }
                var $tr_thead_2 = getTR_Header().attr({ 'data-comp-id': actividada.ID, 'data-act-id': actividad.ID });
                var $tr_thead_3 = getTR_Header().attr({ 'data-comp-id': actividada.ID, 'data-act-id': actividad.ID });
                
                $table.append($tr_thead_1);
                $table.append($tr_thead_2);
                $table.append($tr_thead_3);

                // var totA = actividad.Indicadores.length;
                // var _y = 0;
                
                var $tr_act = getTR_Item().addClass('tr-act').addClass('info').attr({ 'data-comp-id': actividada.ID, 'data-act-id': actividad.ID });
                // for (var i_acta = 0; i_acta < actividad.Indicadores.length; i_acta++) {
                //     var $tr_act = getTR_Item().addClass('tr-act').addClass('info').attr({ 'data-comp-id': actividada.ID, 'data-act-id': actividad.ID });
                //     var indi_act = actividad.Indicadores[i_acta];
                //     if(_y == 0){
                //         $tr_act.append(getTD('style="text-align:center"').html('<a href="javascript:void(0);"><i class="fa fa-edit btn-edit-activ" data-actividad_id='+actividad.ID+' ></i></a><a href="javascript:void(0);" data-toggle="tooltip" title="Eliminar" class="btn-remove-act" data-actividad_id='+actividad.ID+'>'
                //             + '<i class="fa fa-remove"></i></a> <b>' + (i_comp + 1) + '.' + (i_act + 1) + '</b>'));
                        
                //         $tr_act.append(getTD('style="text-align:left !important"').attr('rowspan', totA).append(actividad.Nombre));
                //     }else{
                //         $tr_act.append(getTD('style="text-align:left !important"'));
                //     }
                //     _y++;
                    
                // }
                    $tr_act.append(getTD('style="text-align:center !important"').append(actividad.Clasificador));
                    $tr_act.append(getTD('style="text-align:center !important"').append(actividad.Descripcion));
                    $tr_act.append(getTD('style="text-align:center !important"').append(actividad.Fuente));
                    $tr_act.append(getTD('style="text-align:center !important"').append(actividad.Unidad));
                    $tr_act.append(getTD('style="text-align:center !important"').append(actividad.Cantidad));
                    $table.append($tr_act);
                    

            }
                $div_table.append($table);
                $panelbody.append($div_table);

            

            $panel.append($panelheading);
            $panel.append($panelbody);

            $('#div-presupuesto').append($panel);
        }

        
    }

    function getProyecto(id) {
        var url = "http://localhost/pip2/recurso.json";
        // var url = "<?= \Yii::$app->request->BaseUrl ?>/plan-operativo/actividades-ajax?id="+id;

        return $.getJSON( url );
    }

  


    function getTR_Header() {
        return $('<tr class="tr-header"></tr>');
    }
    function getTR_Item() {
        return $('<tr class="tr-item"></tr>');
    }

    function getTH() {
        return $('<th></th>');
    }

    function getTD(s = '') {
        if(s != '' || s != undefined){
            return $('<td '+s+'></td>');
        }else{
            return $('<td></td>');
        }
    }

    function getTDvacios(q) {
        var html = '';
        for (var i = 0; i < q; i++) {
            html += '<td>&nbsp;</td>';
        }
        return html;
    }

    function getInputText() {
        return $('<input type="text" class="form-control" />');
    }

    function getTextArea(){
        return $('<textarea class="form-control"></textarea')
    }

    function getInputTextNumber() {
        return $('<input type="text" class="form-control input-number" />');
    }
</script>