<?php
use yii\helpers;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<?php if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start(); ?>
<link href="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
<script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/bootstrap-table/bootstrap-table.min.js"></script>
<script src="<?= \Yii::$app->request->BaseUrl ?>/recursos/plugins/bootstrap-table/bootstrap-table-es-SP.min.js"></script>
<div id="page-content" style="min-height: 885px;">
    <div id="wrap">
        <div id="page-heading">
            <h1 id="h1Titulo"></h1>
        </div>
        <div class="container">
            <div class="panel panel-primary">
                <div id="Divnotificaciones"></div>
                <div class="panel-body">

                    <?php $form = ActiveForm::begin(
                        [
                            'action'            => '',
                            'options'           => [
                                'enctype'       => 'multipart/form-data',
                                'id'            => 'frmEjecucion',
                                'class'         => 'form-horizontal'
                            ]
                        ]
                    ); ?>    
                        <input type="hidden" id="hddIdProgramacion" value="<?= $EjecucionTecnica['IdProgramacionTecnica'] ?>">
                        <input type="hidden" id="hddIdEjecucion" value="<?= $EjecucionTecnica['IdEjecucionTecnica'] ?>">
                        <input type="hidden" id="hddCodigoProyecto" value="<?= $EjecucionTecnica['CodigoProyecto'] ?>">
                        <div class="form-group div-label-left">
                            <div class="col-sm-6">
                                <b>Datos del evento (Programado)</b> 
                                &nbsp;&nbsp;<a href="#btnEditProgramacionTecnica" id="btnEditProgramacionTecnica" style="display:none;"><span class=" fa fa-pencil fa-lg"></span>&nbsp;Modificar</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label">Titulo:<span class="f_req">*</span></label>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <input type="text" class="form-control input-sm" id="evento-titulo" required readonly="readonly" value="<?= $EjecucionTecnica['Titulo'] ?>"/>
                            </div>
                            <label class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label">Tipología:<span class="f_req">*</span></label>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <select class="form-control input-sm" id="evento-tipologia" required>
                                    <option value="">[SELECCIONE]</option>
                                    <option value="1">Experimentos de investigación de cultivos priorizado</option>
                                    <option value="2">Experimentos de investigación de crianzas priorizadas</option>
                                    <option value="3">Experimentos biotecnológicos.</option>
                                    <option value="4">Accesiones mantenidas en los Bancos de Germoplasmas (indicar en observaciones la cantidad)</option>
                                    <option value=5>Experimentos de investigación en cambio climático</option>
                                    <option value="6">Análisis socio-económicos, de mercados, TT y sistemas de producción</option>
                                    <option value="7">Experimentos en investigación en post-cosecha</option>
                                    <option value="8">Variedad liberada / lanzamiento de cultivar mejorado</option>
                                    <option value="9">Tecnología liberada / lanzamiento de tecnología productiva</option>
                                    <option value="10">Experimentos incluyan una publicación</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label">Objetivo:<span class="f_req">*</span></label>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <select class="form-control input-sm" name="ProgramacionTecnica[ComponenteID]" id="evento-componenteid" onchange="Actividad(this.value);" required>
                                    <option value="">[SELECCIONE]</option>
                                    <?php foreach($componentes as $componente){ ?>
                                        <option value="<?= $componente->ID ?>"><?= $componente->Correlativo ?>.- <?= $componente->Nombre ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <label class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label">Actividad:<span class="f_req">*</span></label>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <select class="form-control input-sm" name="ProgramacionTecnica[ActividadID]" id="evento-actividadid" required>
                                    <option value="">[SELECCIONE]</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label">Lugar desarrollo:<span class="f_req">*</span></label>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <textarea class="form-control input-sm" id="evento-lugar" required rows="3" cols="1"><?= preg_replace("[\n|\r|\n\r]", '<br />', $EjecucionTecnica['LugarDesarrollo']) ?></textarea>
                            </div>
                            <label class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label">Introduccion:<span class="f_req">*</span></label>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <textarea class="form-control input-sm" id="evento-introduccion" required rows="3" cols="1"><?= preg_replace("[\n|\r|\n\r]", '<br />', $EjecucionTecnica['IntroduccionDescripcion']) ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-md-2 col-lg-2 control-label">Fecha Inicio:<span class="f_req">*</span></label>
                            <div class="col-sm-2 col-md-2 col-lg-2">
                                <div class="input-group input-group-sm">
                                    <input type="text" class="form-control fechas" name="ProgramacionTecnica[FechaInicio]" id="evento-fechainicio" required maxlength="10" value="<?= $EjecucionTecnica['FechaInicio'] ?>"/>
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                </div>
                            </div>
                            <label class="col-sm-1 col-md-1 col-lg-1 control-label">Fin:<span class="f_req">*</span></label>
                            <div class="col-sm-2 col-md-2 col-lg-2">
                                <div class="input-group input-group-sm">
                                    <input type="text" class="form-control fechas" name="ProgramacionTecnica[FechaFin]" id="evento-fechafin" required maxlength="10" value="<?= $EjecucionTecnica['FechaFin'] ?>"/>    
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group div-label-left row-border">
                            <div class="col-sm-6">
                                <b>Datos de la ejecuci&oacute;n</b>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label">Responsable:<span class="f_req">*</span></label>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <input type="text" class="form-control input-sm" id="evento-responsable"  required value="<?= $EjecucionTecnica['ResponsableTecnico'] ?>"/>
                            </div>
                            <label class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label">Duraci&oacute;n (Horas):<span class="f_req">*</span></label>
                            <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">
                                <input type="text" class="form-control input-sm" id="evento-duracion"     required value="<?= $EjecucionTecnica['DuracionHoras'] ?>"/>
                            </div>
                            <label class="col-xs-12 col-sm-1 col-md-1 col-lg-1 control-label">Costos:<span class="f_req">*</span></label>
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <div class="input-group input-group-sm">
                                    <span class="input-group-addon">S/.</span>
                                    <input type="text" class="form-control input-sm text-right" id="evento-costos" required value="<?= $EjecucionTecnica['Costo'] ?>"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label">Beneficiarios:<span class="f_req">*</span></label>
                            <label class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label">Hombres</label>
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <input type="text" class="form-control input-sm text-right" id="evento-benhombres" required value="<?= $EjecucionTecnica['BeneficiarioHombres'] ?>"/>
                            </div>
                            <label class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label">Mujeres</label>
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                <input type="text" class="form-control input-sm text-right" id="evento-benmujeres" required  value="<?= $EjecucionTecnica['BeneficiarioMujeres'] ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label">Material:<span class="f_req">*</span></label>
                            <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                <textarea class="form-control input-sm" id="evento-material" required rows="3" cols="1"><?= preg_replace("[\n|\r|\n\r]", '<br />', $EjecucionTecnica['Material']) ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label">Metodo:<span class="f_req">*</span></label>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <textarea class="form-control input-sm" id="evento-metodo" required rows="3" cols="1"><?= preg_replace("[\n|\r|\n\r]", '<br />', $EjecucionTecnica['Metodo']) ?></textarea>
                            </div>
                            <label class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label">Resultado:<span class="f_req">*</span></label>
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <textarea class="form-control input-sm" id="evento-resultado" required rows="3" cols="1"><?= preg_replace("[\n|\r|\n\r]", '<br />', $EjecucionTecnica['Resultado']) ?></textarea>
                            </div>
                        </div>
                        <style>
                            .tbl-opciones-carga-archivo a,
                            .tbl-opciones-carga-archivo button { margin-left: 15px; }
                        </style>
                        <div class="form-group">
                            <div class="col-sx-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="table-responsive">
                                    <table class="table table-hover" id="tblDocumentosSustento">
                                        <thead>
                                            <th colspan="2">Documentos de sustento
                                                &nbsp;&nbsp;<a id="btnAddSustento" href="#tblDocumentosSustento"><span class=" fa fa-plus  fa-lg"></span>&nbsp;Agregar</a>
                                            </th>
                                            <th>&nbsp;<input type="hidden" id="hddDeleted"/></th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="3" style="text-align: center;">
                                                    Cargando Documentos de sustento ...
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="form-group div-label-left row-border" style="margin-bottom: 0 !important; padding-bottom:  0 !important;">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                                <button id="btnGuardar" class="btn btn-default btn-primary" type="button"><span class=" fa fa-floppy-o"></span>&nbsp;Guardar</button>  
                                &nbsp;
                                &nbsp;
                                <a class="btn btn-default" href="<?= Yii::$app->getUrlManager()->createUrl('ejecucion-evento-new'); ?>"><span class=" fa fa-ban"></span>&nbsp;Cancelar</a>
                            </div>
                        </div>
                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-buscar-programacion" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Seleccionar programaci&oacute;n</h4>
            </div>
            <div class="modal-body">
                <div id="tblProgramados_tool" class="alert alert-info text-left" style="margin-bottom:0px; padding: 0px 9px;">
                    <strong>Doble clic</strong> para seleccionar el registro
                </div>
                <table id="tblProgramados" data-height="350">
                    <thead>
                            <tr>
                                <th data-field="Item" data-width="35" data-align="center">N°</th>
                                <th data-field="Titulo" data-align="left">Titulo</th>
                                <th data-field="CodigoPOA" data-width="80" data-align="center">Objetivo /<br/> Actividad</th>
                                <th data-field="LugarDesarrollo" data-width="150" data-align="left">Lugar<br/>desarrollo</th>   
                                <th data-field="FechaInicio" data-width="90" data-align="center">Fecha<br/>inicio</th>  
                                <th data-field="FechaFin" data-width="90" data-align="center">Fecha<br/>T&eacute;rmino</th> 
                                <th data-field="Probabilidad" data-width="90" data-align="center">Probabilidad<br/>termino plazo</th>
                            </tr>
                        </thead>
                </table>
            </div>
            <div class="modal-footer" style="margin-top: 0px;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var formParsleyEjecucion = $('#frmEjecucion').parsley(defaultParsleyForm2());
    
    $(document).ready(function(){

        <?php if($EjecucionTecnica['IdEjecucionTecnica'] == -1){ ?>
            $('#h1Titulo').text('Ejecución eventos (Nuevo)');
            $('#modal-buscar-programacion').modal('show');
            cargarEventosNoEjecutados();
            $('#btnEditProgramacionTecnica').show().click(function(){
                $('#modal-buscar-programacion').modal('show');
                cargarEventosNoEjecutados();
            });
            $('#modal-buscar-programacion').modal({ backdrop:'static', keyboard:false, show:false });
            $('#tblProgramados').bootstrapTable({
                cache: false,
                striped: true,
                pagination: false,
                search: true,
                classes: 'table table-condensed table-hover',
                toolbar: '#tblProgramados_tool',
                onDblClickRow: function(row, elementHtml, field){
                    seleccionaProgramacion(row['IdProgramacionTecnica']);
                    $('#modal-buscar-programacion').modal('hide');
                }
            });
            $('#tblDocumentosSustento tbody').empty();
        <?php } else { ?>
            $('#h1Titulo').text('Ejecución eventos (Edición)');
            $('#btnEditProgramacionTecnica, #modal-buscar-programacion').remove();
            $('#evento-componenteid').val('<?= $EjecucionTecnica['Objetivo'] ?>');
            Actividad('<?= $EjecucionTecnica['Objetivo'] ?>', '<?= $EjecucionTecnica['ActividadID'] ?>');
            $('#evento-tipologia').val('<?= $EjecucionTecnica['Tipologia'] ?>');
            $.ajax({
                url: '<?= Yii::$app->getUrlManager()->createUrl('/ejecucion-evento-new/lista-sustentos-json'); ?>',
                type: 'POST',
                async: false,
                data: { 
                    'IdEjecucionTecnica': $('#hddIdEjecucion').val(), 
                    _csrf: '<?=Yii::$app->request->getCsrfToken()?>' 
                },
                success: function(result){
                    result = JSON.parse(result);
                    lista_archivos_sustento(result, '#tblDocumentosSustento');
                }
            });
        <?php } ?>

        $('#evento-fechainicio').datepicker({
            language: 'es',
            format: 'dd/mm/yyyy',
            autoclose: true
        }).on('changeDate', function (ev) {
            $('#evento-fechafin').datepicker('setStartDate', ev.date)
        });
        $('#evento-fechafin').datepicker({
            language: 'es',
            format: 'dd/mm/yyyy',
            autoclose: true
        }).on('changeDate', function (ev) {
            $('#evento-fechainicio').datepicker('setEndDate', ev.date);
        });

        $('#evento-titulo, #evento-descripcion, #evento-lugar, #evento-observacion').filter_input_alfanumericos_validate();
        $('#evento-fechainicio, #evento-fechafin').filter_input_fecha_validate();

        $('#btnGuardar').click(function(){
            if(formParsleyEjecucion.validate()) {    
                $(this).attr('disabled', 'disabled');

                var datos = { 
                    EjecucionTecnica: { 
                        'IdEjecucionTecnica': $('#hddIdEjecucion').val(),
                        'ResponsableTecnico': $('#evento-responsable').val(),
                        'DuracionHoras': $('#evento-duracion').val(),
                        'BeneficiarioHombres': $('#evento-benhombres').val(),
                        'BeneficiarioMujeres': $('#evento-benmujeres').val(),
                        'Material': $('#evento-material').val(),
                        'Metodo': $('#evento-metodo').val(),
                        'Resultado': $('#evento-resultado').val(),
                        'Costo': $('#evento-costos').val(),

                        'IdProgramacionTecnica': $('#hddIdProgramacion').val(),
                        'Tipologia':$('#evento-tipologia').val(),
                        'ActividadID':$('#evento-actividadid').val(),
                        'Objetivo':$('#evento-componenteid').val(),
                        'LugarDesarrollo': $('#evento-lugar').val(),
                        'IntroduccionDescripcion': $('#evento-introduccion').val(),
                        'FechaInicio': $('#evento-fechainicio').datepicker('getFormattedDate','yyyy-mm-dd'),
                        'FechaFin': $('#evento-fechafin').datepicker('getFormattedDate','yyyy-mm-dd')
                    },
                    _csrf: $('input:hidden[name="_csrf"]').val()
                };

                $.ajax({
                    url: '<?= Yii::$app->getUrlManager()->createUrl('ejecucion-evento-new/registrar'); ?>',
                    data: datos,
                    type: 'POST',
                    beforeSend:function(){
                    },
                    success:function(result)
                    {
                        result = JSON.parse(result);
                        //console.log('1: eliminamos los archivos nuevos');
                        if($('#hddDeleted', $('#tblDocumentosSustento')).val() != ''){
                            $.ajax({
                                url: '<?= Yii::$app->getUrlManager()->createUrl('ejecucion-evento-new/eliminar-sustentos-json'); ?>',
                                data: {
                                    _csrf: $('input:hidden[name="_csrf"]').val(),
                                    IdsSustentos: $('#hddDeleted', $('#tblDocumentosSustento')).val() + ','
                                },
                                type: 'post',
                                cache: false,
                                async: false,
                                beforeSend:function(){
                                    
                                },
                                success:function(result){
                                    
                                },
                                error:function(){
                                    alert('Error al realizar el proceso.');
                                },
                                complete: function (){
                                    
                                }
                            });
                        }
                        //console.log('2: guardar los archivos nuevos');
                        $.each($('input:file[name="Sustento"]'), function(i, element){
                            console.dir(element.files);
                            if(element.files.length > 0){
                                datos = new FormData();
                                datos.append('_csrf', $('input:hidden[name="_csrf"]').val());
                                datos.append('IdEjecucionTecnica', $('#hddIdProgramacion').val());
                                datos.append('CodigoProyecto', $('#hddCodigoProyecto').val());
                                datos.append('Sustento', element.files[0]);
                               
                                $.ajax({
                                    url: '<?= Yii::$app->getUrlManager()->createUrl('ejecucion-evento-new/registrar-sustento'); ?>',
                                    data: datos,
                                    type: 'post',
                                    cache: false,
                                    async: false,
                                    contentType: false,
                                    processData: false,
                                    beforeSend:function(){
                                        //console.log('guardando: ', element.files[0].name);
                                    },
                                    success:function(result){
                                        //console.dir(JSON.parse(result));
                                    },
                                    error:function(){
                                        alert('Error al realizar el proceso.');
                                    },
                                    complete: function (){
                                    }
                                });
                            }
                        });
                        
                        $('body').append('<form id="frmBack" method="get" action="<?= Yii::$app->getUrlManager()->createUrl('ejecucion-evento-new'); ?>"></form>');
                        $('#frmBack').submit(); 
                        //console.log('3: fin');
                    },
                    error:function(){
                        alert('Error al realizar el proceso.');
                    },
                    complete: function (){
                        $('#btnGuardar').removeAttr('disabled');
                    }
                });
            }
        });

        $('#btnAddSustento').click(function(){
            $('#tblDocumentosSustento tbody')
                .append(
                    $('<tr>'+'</tr>').append(
                        '<td class="td-img">&nbsp;</td>'
                        ,'<td class="td-nombre">&nbsp;</td>'
                        ,'<td class="tbl-opciones-carga-archivo">'
                            +'<a href="#tblDocumentosSustento" class="btn-seleccionar" onclick="seleccionar(this);"><i class="fa fa-cloud-upload fa-lg"></i> Seleccionar</a>'
                            +'<a href="#tblDocumentosSustento" class="btn-eliminar" onclick="rowRemove(this);"><i class="fa fa-trash-o fa-lg"></i>&nbsp;Eliminar</a>'
                            +'<input name="Sustento" type="file" style="display: none;" onchange="file_selected(this);"/>'
                            +'<input type="hidden" id="hddIdAdjunto" name="hddIdAdjunto" value="-1" />'
                        +'</td>'
                    )
                );            
        });
    });
    function Actividad(valor, selectValue = null) {
        var ComponenteID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= Yii::$app->getUrlManager()->createUrl('termino-referencia/actividades'); ?>',
            type: 'POST',
            async: false,
            data: {ComponenteID:ComponenteID,_csrf: toke},
            success: function(data){
                $("#evento-actividadid").html(data);
                if(selectValue != null)
                    $("#evento-actividadid").val(selectValue);
            }
        });
    }
    function cargarEventosNoEjecutados(){
            $.ajax({
                url:'<?= \Yii::$app->request->BaseUrl ?>/programacion-evento-new/lista-eventos-no-ejecutados-json',
                cache:false,
                data:{},
                contentType: "application/json",
                beforeSend:function()
                {
                    $('#tblProgramados').bootstrapTable('showLoading');
                },
                success:function(result)
                {
                    result = JSON.parse(result);
                    $('#tblProgramados').bootstrapTable('load', result);
                },
                error:function(){   
                    alert('Error al realizar el proceso de busqueda.');
                },
                complete: function (){
                    $('#tblProgramados').bootstrapTable('hideLoading');
                }
            });
        }
    function seleccionaProgramacion(pIdProgramacionTecnica){
        $.ajax({
            url: '<?= Yii::$app->getUrlManager()->createUrl('/programacion-evento-new/consulta-evento-json'); ?>',
            type: 'POST',
            async: false,
            data: { 
                'IdProgramacionTecnica': pIdProgramacionTecnica, 
                _csrf: '<?=Yii::$app->request->getCsrfToken()?>' 
            },
            success: function(result){
                result = JSON.parse(result);
                //console.dir(result);
                $('#hddIdProgramacion').val(result['IdProgramacionTecnica']);
                $('#hddCodigoProyecto').val(result['CodigoProyecto']);
                $('#evento-titulo').val(result['Titulo']);
                $('#evento-componenteid').val(result['ComponenteID']);
                Actividad(result['ComponenteID'], result['ActividadID']);
                $('#evento-tipologia').val(result['Tipologia']);
                $('#evento-lugar').val(result['LugarDesarrollo']);
                $('#evento-fechainicio').datepicker('setDate',result['FechaInicio']);
                $('#evento-fechafin').datepicker('setDate',result['FechaFin']);
                $('#evento-introduccion').val(result['IntroduccionDescripcion']);
            }
        });
    }
    function lista_archivos_sustento(archivos, idcontenedor){
        $('tbody', $(idcontenedor)).empty();
        //console.dir(archivos);
        $.each(archivos, function(i, row){
            var tr = $('<tr>'+'</tr>')
                    .append('<td class="td-img">&nbsp;</td>'
                        ,'<td class="td-nombre">&nbsp;</td>'
                        ,'<td class="tbl-opciones-carga-archivo">'
                            +'<a href="#tblDocumentosSustento" class="btn-seleccionar" onclick="seleccionar(this);"><i class="fa fa-cloud-upload fa-lg"></i> Seleccionar</a>'
                            +'<a href="#tblDocumentosSustento" class="btn-eliminar" onclick="rowRemove(this);"><i class="fa fa-trash-o fa-lg"></i>&nbsp;Eliminar</a>'
                            +'<input  name="Sustento" type="file" style="display: none;" onchange="file_selected(this);"/>'
                            +'<input type="hidden" id="hddIdAdjunto" name="hddIdAdjunto" value="'+row['id']+'" />'
                        +'</td>'                    
                    );  
                addImg(tr, row['nombre'].split('.')[1]);
                setNameFile(tr, row['nombre'], row['ruta']);
                delSeleccionar(tr.find('td.tbl-opciones-carga-archivo'));
                if($('a.btn-modificar', tr).length == 0){
                    addModificar(tr.find('td.tbl-opciones-carga-archivo'));    
                }
            $('tbody', $(idcontenedor)).append(tr);
        });  
    }
    //-----------------------------------------------------
    function file_selected(sender){
        var tr = $(sender).closest('tr');
        clearNameFile(tr);
        clearImg(tr);

        if($('input:file', tr)[0].files.length){
            var nombre = $('input:file', tr)[0].files[0].name;
            if($('a.btn-modificar', tr).length == 0){
                addModificar(tr.find('td.tbl-opciones-carga-archivo'));    
            }
            if($('#hddIdAdjunto', tr).val() != '-1'){
                var del = $('#hddDeleted', $(sender).closest('table'));
                del.val(del.val() + ',' + $('#hddIdAdjunto', tr).val());
            }
            setIdFile(tr,'-1');
            addImg(tr, nombre.split('.')[1]);
            setNameFile(tr, nombre, '');
            delSeleccionar(tr.find('td.tbl-opciones-carga-archivo'));
        }
    }
    function seleccionar(sender){
        $('input:file',$(sender).parent()).trigger('click');
    }
    function setIdFile(contenedor, id){
        $('#hddIdAdjunto', contenedor).val(id);
    }
    function setNameFile(contenedor, nombre, ruta){
        if(ruta == ''){
            $('td.td-nombre', contenedor).empty().append(nombre);
        } else {
            $('td.td-nombre', contenedor).empty().append('<a target="blank" href="<?= Yii::$app->getUrlManager()->createUrl('/'); ?>'+ruta+nombre+'">'+nombre+'</a>');
        }
    }
    function clearNameFile(contenedor){
        $('td.td-nombre', contenedor).empty();
    }
    function addImg(contenedor, extension){
        console.log(extension);
        var img = '<i class="fa fa-file-o  fa-lg"></i>';
        if(extension == 'txt'){
            img = '<i class="fa fa-file-text-o  fa-lg"></i>';
        } else if(extension == 'doc' || extension == 'docx'){
            img = '<i class="fa fa-file-word-o  fa-lg"></i>';
        } else if(extension == 'xls' || extension == 'xlsx'){
            img = '<i class="fa fa-file-excel-o  fa-lg"></i>';
        } else if(extension == 'pdf'){
            img = '<i class="fa fa-file-pdf-o  fa-lg"></i>';
        } else if(extension == 'jpg' || extension == 'bmp' || extension == 'tif' || extension == 'png'){
            img = '<i class="fa fa-picture-o  fa-lg"></i>';
        } else if(extension == 'mpg' || extension == 'avi' || extension == 'wma' || extension == '3gp'){
            img = '<i class="fa fa-film  fa-lg"></i>';
        }
        $('td.td-img', contenedor).empty().append(img);
    }
    function clearImg(contenedor){
        $('td.td-img', contenedor).empty();
    }
    function rowRemove(sender){
        var del = $('#hddDeleted', $(sender).closest('table'));
        del.val(del.val() + ',' + $('#hddIdAdjunto', $(sender).closest('tr')).val());
        $(sender).closest('tr').remove();
    }
    function addSeleccionar(contenedor){
        $(contenedor).append('<a href="#tblDocumentosSustento" class="btn-seleccionar" onclick="seleccionar(this);"><i class="fa fa-cloud-upload fa-lg"></i> Seleccionar</a>');
    }
    function addModificar(contenedor){
        $(contenedor).append('<a href="#tblDocumentosSustento" class="btn-modificar" onclick="seleccionar(this);"><i class="fa fa-refresh fa-lg"></i> Cambiar</a>');
    }
    function addEliminar(contenedor){
        $(contenedor).append('<a href="#tblDocumentosSustento" class="btn-eliminar" onclick="rowRemove(this);"><i class="fa fa-trash-o fa-lg"></i>&nbsp;Eliminar</a>');
    }
    function delSeleccionar(contenedor){
        $('a.btn-seleccionar',$(contenedor)).remove();
    }
    function delModificar(contenedor){
        $('a.btn-modificar',$(contenedor)).remove();
    }
    function delEliminar(contenedor){
        $('a.btn-eliminar',$(contenedor)).remove();
    }
</script>



