<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<div id="page-content" style="min-height: 754px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Orden de Servicio</h1>
        </div>
        <div class="container">
            
            <!-- OBJETIVOS -->
            <style>
                .panel .panel-body {
                    padding: 10px !important;
                }

                .panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 1px solid #a8a8ab !important;
                }

                .panel-gray .panel-body {
                    border-top: none !important;
                }

                .btn-sm {
                    padding: 5px 10px !important;
                    line-height: 1.5 !important;
                }

                .input-comp-nombre {
                    text-transform: uppercase;
                }

                .tbl-act > tbody > .tr-header > td {
                    text-align: center;
                    font-size: 12px;
                    font-weight: bold;
                    padding: 5px !important;
                    vertical-align: middle !important;
                    /*border: 1px solid #cfcfd0;
                    background-color: #f0f0f1;*/
                    border: 1px solid #c0c0c0;
                    background-color: #e3e3e3;
                }

                /*.tbl-act .tr-item td {
                    padding: 2px !important;
                }*/
                input.form-control, select.form-control {
                    /*height: 28px !important;*/
                }

                .tbl-act .tr-act .input-nombre {
                    min-width: 320px !important;
                }

                .tbl-act .tr-act .input-unidadmedida {
                    min-width: 100px !important;
                }

                .input-number {
                    min-width: 92px !important;
                }

                .btn-add-re {
                    float: right;
                }

                .text-left {
                    text-align: left !important;
                    font-weight: bold;
                }

                .tr-comp input, .tr-proy input {
                    font-weight: bold;
                }

                .th-proy {
                    min-width: 630px !important;
                }

            </style>

            <div class="panel panel-primary">

                <div class="panel-body">
                    <div class="row">
                        <div class="container">

                            <?php $form = ActiveForm::begin(
                                [
                                    'action'            => 'requerimiento/crear?CodigoProyecto='.$CodigoProyecto,
                                    'options'           => [
                                        'enctype'       =>'multipart/form-data',
                                        'id'            => 'frmRequerimientos',
                                        
                                    ]
                                ]
                            ); ?>

                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">POA:<span class="f_req">*</span></label>
                                    <div class="col-sm-4">
                                        <a class="btn btn-primary" id="agregar-poa">Agregar POA</a>
                                    </div>
                                </div>
                                <input type="hidden" name="Orden[RequerimientoID]" value="<?= $RequerimientoID ?>">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Objetivo:<span class="f_req">*</span></label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="requerimiento-componenteid" value="<?= $requerimiento->Componente ?>" disabled>
                                        <input type="hidden" class="form-control" name="Requerimiento[ComponenteID]" id="requerimiento-componenteid-c" value="<?= $requerimiento->ComponenteID ?>" >
                                    </div>
                                    <label class="col-sm-2 control-label">Actividad:<span class="f_req">*</span></label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" id="requerimiento-actividadid" value="<?= $requerimiento->Actividad ?>" disabled>
                                        <input type="hidden" class="form-control" name="Requerimiento[ActividadID]" id="requerimiento-actividadid-c" value="<?= $requerimiento->ActividadID ?>" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Recurso:<span class="f_req">*</span></label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control"  id="requerimiento-areasubcategoriaid" value="<?= $requerimiento->AreSubCategoria ?>" disabled>
                                        <input type="hidden" class="form-control" name="Orden[AreSubCategoriaID]" id="requerimiento-areasubcategoriaid-c" value="<?= $requerimiento->AreSubCategoriaID ?>">
                                    </div>
                                    <label class="col-sm-2 control-label">Código POA:<span class="f_req">*</span></label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control"  id="requerimiento-codigo" value="<?= $requerimiento->CodigoPoa ?>" disabled>
                                        <input type="hidden" class="form-control" name="Requerimiento[CodigoPoa]" id="requerimiento-codigo-c" value="<?= $requerimiento->CodigoPoa ?>" >
                                    </div>
                                </div>

                                <div id="pa" class="none">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Objetivo:<span class="f_req">*</span></label>
                                        <div class="col-sm-6">
                                            <select class="form-control" name="Requerimiento[ComponenteID]" id="requerimiento-componenteid" onchange="Actividad($(this).val())">
                                                <option>[SELECCIONE]</option>
                                                <?php foreach($componentes as $componente){ ?>
                                                    <option value="<?= $componente->ID ?>"><?= $componente->Nombre ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-sm-2 control-label">N° de Orden:<span class="f_req">*</span></label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="Requerimiento[Correlativo]">
                                    </div>
                                    <label class="col-sm-2 control-label">N° de Siaft: <span class="f_req">*</span></label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="Requerimiento[Siaft]">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Certificación N°:<span class="f_req">*</span></label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="Requerimiento[Siaft]">
                                    </div>
                                    <label class="col-sm-2 control-label">Tipo Recibo: <span class="f_req">*</span></label>
                                    <div class="col-sm-4">
                                        <select class="form-control" name="Requerimiento[TipoServicio]" id="servicios">
                                            <option>[SELECCIONE]</option>
                                            <option value="1">Factura</option>
                                            <option value="2">Recibo por Honorarios</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tipo de Proceso:<span class="f_req">*</span></label>
                                    <div class="col-sm-4">
                                        <select class="form-control" name="Requerimiento[TipoServicio]" id="servicios">
                                            <option>[SELECCIONE]</option>
                                            <option>Comparacion de Precios</option>
                                            <option>Evaluacion de CVs</option>
                                        </select>
                                    </div>
                                    <label class="col-sm-2 control-label">Monto: <span class="f_req">*</span></label>
                                    <div class="col-sm-4">
                                        <input type="number" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Concepto:<span class="f_req">*</span></label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Descripcion:<span class="f_req">*</span></label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control"></textarea>
                                    </div>
                                </div>

                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
                            </div>
                            <?php ActiveForm::end(); ?>
                            <?php
                                $actividades=Yii::$app->getUrlManager()->createUrl('requerimiento/actividades');
                                $recursos=Yii::$app->getUrlManager()->createUrl('requerimiento/recursos');
                            ?>

                            <style type="text/css">
                                .none{
                                    display: none;
                                }
                                .block{
                                    display: block;
                                }
                            </style>

                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
<div class="modal fade" id="modal-ctrl-poa" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-lg" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">POA</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>










<script>
    var $form = $('#frmRequerimientos');
    var formParsleyRequerimientos = $form.parsley(defaultParsleyForm());

    
    function Actividad(valor) {
        var ComponenteID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $actividades ?>',
            type: 'POST',
            async: false,
            data: {ComponenteID:ComponenteID,_csrf: toke},
            success: function(data){
               $( "#requerimiento-actividadid" ).html( data );
            }
        });
    }
    
    function Recurso(valor) {
        var ActividadID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $recursos ?>',
            type: 'POST',
            async: false,
            data: {ActividadID:ActividadID,_csrf: toke},
            success: function(data){
               $( "#requerimiento-areasubcategoriaid" ).html( data );
            }
        });
    }

    $('#orden_tdr').change(function(){
        
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        var cod = $(this).val();
        $.ajax({
            url: 'tipo-referencia-orden-servicio?id='+cod+"&_csrf="+toke,
            type: 'GET',
            dataTye: 'JSON',
            success: function(data){
               console.log(data);
                if(data == 'PA'){
                    // $('#poa').removeClass('none');
                    $('#pa').removeClass('none');
                    $('#pa').addClass('block');

                    $('#poa').removeClass('block');
                    $('#poa').addClass('none');
                }

                if(data == 'POA'){
                    $('#poa').removeClass('none');
                    $('#poa').addClass('block');

                    $('#pa').removeClass('block');
                    $('#pa').addClass('none');
                }

                if(data == ''){
                    $('#poa').removeClass('block');
                    $('#pa').removeClass('block');

                    $('#poa').addClass('none');
                    $('#pa').addClass('none');
                }
            }
        });


        // var va = $(this).val();
        // console.log(va);
        // if(va == 1){
        //     $('#serv').removeClass('none');
        //     $('#serv').addClass('block');
        // }

        // if(va == 2){
        //     $('#serv').removeClass('block');
        //     $('#serv').addClass('none');
        // }

    });
    
    $('body').on('click', '#agregar-poa', function (e) {
        e.preventDefault();
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        var CodigoProyecto="<?= $requerimiento->CodigoProyecto ?>";
        $('#modal-ctrl-poa .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/requerimiento/poa?CodigoProyecto='+CodigoProyecto+'&_csrf='+toke);
        $('#modal-ctrl-poa').modal('show');
    });
    
    
    $('body').on('click', '.seleccionar_poa', function (e) {
        e.preventDefault();
        
        
        $('.fila').each(function(){
            var id=$(this).attr('data-id');
            var componenteid=$(this).attr('data-componente');
            var actividadid=$(this).attr('data-actividad');
            var toke = '<?=Yii::$app->request->getCsrfToken()?>';
            $.get('<?= \Yii::$app->request->BaseUrl ?>/requerimiento/get-dato-poa', { id: id ,componenteid:componenteid,actividadid:actividadid,_csrf: toke }, function (data) {
                var jx = JSON.parse(data);
                if (jx.Success == true) {
                    console.log(jx);
                    $('#requerimiento-componenteid').val(jx.componente)
                    $('#requerimiento-actividadid').val(jx.actividad)
                    $('#requerimiento-areasubcategoriaid').val(jx.recurso)
                    $('#requerimiento-codigo').val(jx.codigo)
                    
                    $('#requerimiento-componenteid-c').val(jx.componenteid)
                    $('#requerimiento-actividadid-c').val(jx.actividadid)
                    $('#requerimiento-areasubcategoriaid-c').val(jx.aresubcategoriaid)
                    $('#requerimiento-codigo-c').val(jx.codigo)
                    
                    //$('#requerimiento-componenteid-c').val(data.componente)
                    
                }
            });
            $('#modal-ctrl-poa').modal('hide');
        });
        
    });
    
    $('body').on('click', '#agregar-poa', function (e) {
        e.preventDefault();
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        var CodigoProyecto="<?= $requerimiento->CodigoProyecto ?>";
        $('#modal-ctrl-poa .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/requerimiento/poa?CodigoProyecto='+CodigoProyecto+'&_csrf='+toke);
        $('#modal-ctrl-poa').modal('show');
    });
    
    
    $('body').on('click', '.seleccionar_poa', function (e) {
        e.preventDefault();
        
        
        $('.fila').each(function(){
            var id=$(this).attr('data-id');
            var componenteid=$(this).attr('data-componente');
            var actividadid=$(this).attr('data-actividad');
            var toke = '<?=Yii::$app->request->getCsrfToken()?>';
            $.get('<?= \Yii::$app->request->BaseUrl ?>/requerimiento/get-dato-poa', { id: id ,componenteid:componenteid,actividadid:actividadid,_csrf: toke }, function (data) {
                var jx = JSON.parse(data);
                if (jx.Success == true) {
                    console.log(jx);
                    $('#requerimiento-componenteid').val(jx.componente)
                    $('#requerimiento-actividadid').val(jx.actividad)
                    $('#requerimiento-areasubcategoriaid').val(jx.recurso)
                    $('#requerimiento-codigo').val(jx.codigo)
                    
                    $('#requerimiento-componenteid-c').val(jx.componenteid)
                    $('#requerimiento-actividadid-c').val(jx.actividadid)
                    $('#requerimiento-areasubcategoriaid-c').val(jx.aresubcategoriaid)
                    $('#requerimiento-codigo-c').val(jx.codigo)
                    
                    //$('#requerimiento-componenteid-c').val(data.componente)
                    
                }
            });
            $('#modal-ctrl-poa').modal('hide');
        });
        
    });
</script>   