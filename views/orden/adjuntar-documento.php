<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => '../orden/adjuntar-documento?ID='.$ID,
        'options'           => [
            'enctype'       =>'multipart/form-data',
            'id'            => 'frmOrden',
            
        ]
    ]
); ?>

    <div class="modal-body">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-2 control-label">Documento:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="input-group">
                            <div class="form-control uneditable-input" data-trigger="fileinput">
                                <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                <span class="fileinput-filename">
                                Seleccione archivo...
                                </span>
                            </div>
                            <span class="input-group-addon btn btn-default btn-file">
                                <span class="fileinput-new">Seleccionar</span>
                                <span class="fileinput-exists">Cambiar</span>
                                <input type="file" onchange="return Imagen(this);" id="file-cv-administrativo" name="Orden[archivo]">
                                <input class="form-control" type="hidden" name="Orden[Correlativo]" value="<?= $orden->Correlativo ?>">
                            </span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Eliminar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>
<script>
    var $form = $('#frmOrden');
    var formParsleyOrden = $form.parsley(defaultParsleyForm());
</script>