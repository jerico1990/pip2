<?php

/** Error reporting */
error_reporting(E_ALL);

/** Include path **/
ini_set('include_path', ini_get('include_path').';../web/PHPExcel/Classes/');
//require(__DIR__ . '/../../web/PHPExcel/Classes/');
/** PHPExcel */
include 'PHPExcel.php';

/** PHPExcel_Writer_Excel2007 */
include 'PHPExcel/Writer/Excel2007.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
$sheet = $objPHPExcel->getActiveSheet();

$sheet->setTitle('Requerimientos');


$objPHPExcel->setActiveSheetIndex(0);



$sheet->SetCellValue('A3', '#');
$sheet->SetCellValue('B3', 'Código Proyecto');
$sheet->SetCellValue('C3', 'Orden Compra/Servicio N°');
$sheet->SetCellValue('D3', 'RUC');
$sheet->SetCellValue('E3', 'Razon Social');
$sheet->SetCellValue('F3', 'Concepto');
$sheet->SetCellValue('G3', 'Cuenta Bancaria');
$sheet->SetCellValue('H3', 'CCI');
$sheet->SetCellValue('I3', 'Objetivo');
$sheet->SetCellValue('J3', 'Actividad');
$sheet->SetCellValue('K3', 'Recurso');
$sheet->SetCellValue('L3', 'Codigo POA');
$sheet->SetCellValue('M3', 'Cantidad');
$sheet->SetCellValue('N3', 'Precio Unitario');

$i=4;
foreach($resultados as $resultado)
{
    $sheet->SetCellValue('A'.$i, ($i-3));
    $sheet->SetCellValue('B'.$i, $resultado["CodigoProyecto"]);
    $sheet->SetCellValue('C'.$i, str_pad($resultado["OCorrelativo"], 3, "0", STR_PAD_LEFT));
    $sheet->SetCellValue('D'.$i, $resultado["RUC"]);
    $sheet->SetCellValue('E'.$i, $resultado["RazonSocial"]);
    $sheet->SetCellValue('F'.$i, $resultado["Concepto"]);
    $sheet->SetCellValue('G'.$i, $resultado["CuentaBancaria"]);
    $sheet->SetCellValue('H'.$i, $resultado["CCI"]);
    $sheet->SetCellValue('I'.$i, $resultado["CComponente"].".".$resultado["NComponente"]);
    $sheet->SetCellValue('J'.$i, $resultado["CActividad"].".".$resultado["NActividad"]);
    $sheet->SetCellValue('K'.$i, $resultado["CAreSubCategoria"].".".$resultado["NAreSubCategoria"]);
    $sheet->SetCellValue('L'.$i, $resultado["CComponente"].".".$resultado["CActividad"].".".$resultado["CAreSubCategoria"]);
    $sheet->SetCellValue('M'.$i, $resultado["CDetalleRequerimiento"]);
    $sheet->SetCellValue('N'.$i, $resultado["PrecioUnitario"]);
    $i++;
}
    




// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Requerimientos.xls"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
