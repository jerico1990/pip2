
<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay_progress.min.js"></script>
<script src=" //cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>


<div id="page-content" style="min-height: 1342px;">
    
    <div id="wrap">
        <div id="page-heading">
            <h1>Ordenes de Compra/Servicio</h1>
        </div>
        
           
        <div class="container">
             <div class="form-group">
              <!--
                <button class="btn btn-crear-requerimiento" data-codigo-proyecto="<?= $CodigoProyecto ?>">Generar orden Compra</button>
                <a href="orden/crear-servicio-factura?CodigoProyecto=<?php echo $CodigoProyecto ?>" class="btn btn-crear-requerimientoz" data-codigo-proyecto="<?= $CodigoProyecto ?>">Generar orden Servicio</a>
                
                <a href="orden/crear-orden-compra?CodigoProyecto=<?= $CodigoProyecto ?>" class="btn" data-codigo-proyecto="<?= $CodigoProyecto ?>">Generar orden de compra</a>
                <a href="orden/crear-orden-servicio-factura?CodigoProyecto=<?= $CodigoProyecto ?>" class="btn" data-codigo-proyecto="<?= $CodigoProyecto ?>">Generar orden de servicio terceros factura</a>
                <a href="orden/crear-orden-servicio-rh?CodigoProyecto=<?= $CodigoProyecto ?>" class="btn" data-codigo-proyecto="<?= $CodigoProyecto ?>">Generar orden de servicio terceros RH</a>
                <br>
                -->
               <!-- <a class="btn " href="documentos/0_OFICIO_MODELO_REQUERIMIENTO.docx">Descargar plantilla</a>-->
                
                
                
            </div>
            <div class="table-responsive">
                <table id="proyectos" class="display table table-bordered table-striped table-condensed table-hover" cellspacing="0" >
                    <thead>
                        <tr>
                            <th>Estado</th>
                            <th>Tipo de Orden</th>
                            <th>Orden</th>
                            <th>Fecha de Orden</th>
                            <th>Tipo de Proceso</th>
                            <th>Monto total</th>
                            <th>Descargar</th>
                            <!--<th>Acciones</th>-->
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <a class="btn" href="<?= Yii::$app->getUrlManager()->createUrl('orden/excel?CodigoProyecto='.$CodigoProyecto.'') ?>">Descargar Excel</a>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-ctrl-orden" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Orden</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<script>
    function js_buscar()
    {
       
    }
    var tblresultjs;
        var configDTjs={
            "order": [[ 2, "desc" ]],
                "language": {
                    "sProcessing":    "Procesando...",
                    "sLengthMenu":    "Mostrar _MENU_ registros",
                    "sZeroRecords":   "No se encontraron resultados",
                    "sEmptyTable":    "Ningún dato disponible en esta lista",
                    "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":   "",
                    "sSearch":        "Buscar:",
                    "sUrl":           "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":    "Último",
                        "sNext":    "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
        };
    $(document).ready(function() {
        var CodigoProyecto="<?= $CodigoProyecto ?>";
        $.ajax({
            url:'orden/lista-ordenes?CodigoProyecto='+CodigoProyecto,
            async:false,
            data:{},
            beforeSend:function()
            {
                $('.panel-default').LoadingOverlay("show");
            },
            success:function(result)
            {
                    //$tblresultjs.destroy();//
                    $("#proyectos tbody").html(result);
                    $('#proyectos').DataTable(configDTjs);
                    //tblresultjs=$('#example').DataTable(configDTjs);
                    $('.panel-default').LoadingOverlay("hide",true);
            },
            error:function(){
                $('.panel-default').LoadingOverlay("hide",true);
                alert('Error al realizar el proceso de busqueda.');
            }
        });
        
    } );
    
    $('body').on('click', '.btn-crear-requerimiento', function (e) {
        e.preventDefault();
        var CodigoProyecto = $(this).attr('data-codigo-proyecto');
        $('#modal-ctrl-requerimiento .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/requerimiento/crear?CodigoProyecto='+CodigoProyecto);
        $('#modal-ctrl-requerimiento').modal('show');
    });


    $('body').on('click', '.btn-crear-requerimiento-servicio', function (e) {
        e.preventDefault();
        var CodigoProyecto = $(this).attr('data-codigo-proyecto');
        $('#modal-ctrl-requerimiento .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/orden/crear-servicio-factura?CodigoProyecto='+CodigoProyecto);
        $('#modal-ctrl-requerimiento').modal('show');
    });

    $('body').on('click', '.btn-upload', function (e) {
        e.preventDefault();
        var ID = $(this).attr('data-id');
        $('#modal-ctrl-orden .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/orden/adjuntar-documento?ID='+ID);
        $('#modal-ctrl-orden').modal('show');
    });
    
    $('body').on('click', '#btn-guardar-requerimientos', function (e) {
        
        e.preventDefault();
        // alert('s');
        
        var $btn = $(this);
        $btn.attr('disabled', 'disabled');
        var isValid = formParsleyRequerimientos.validate();
        if (isValid) {
            sendFormNoMessage($('#frmRequerimientos'), $(this), function (msg) {
                
                $("#modal-ctrl-requerimiento").modal('hide');
                _pageLoadingEnd();
		
            });
        } else {
        }
    });
    
    
    
    
</script>