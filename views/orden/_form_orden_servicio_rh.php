<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<?php $form = ActiveForm::begin(
[
    // 'action'            => '../orden/crear-orden-servicio-rh?DetalleRequerimientoID='.$DetalleRequerimientoID.'&RequerimientoID='.$RequerimientoID.'&Codificacion='.$Codificacion.'&CodigoProyecto='.$CodigoProyecto.'&PrecioTotal='.$PrecioTotal,

    'action'            => '../orden/crear-orden-servicio-rh?RequerimientoID='.$RequerimientoID.'&CodigoProyecto='.$CodigoProyecto.'&DetallesRequerimientosIDs='.$DetallesRequerimientosIDs,
    
    'options'           => [
        'enctype'       =>'multipart/form-data',
    ]
]
); ?>
<div class="modal-body">
<div class="form-horizontal">
    
    <input type="hidden" name="Orden[RequerimientoID]" value="<?= $RequerimientoID ?>">
    
    <div class="form-group">
        <label class="col-sm-2 control-label">RUC:<span class="f_req">*</span></label>
        <div class="col-sm-4">
            <input type="text" class="form-control" onKeyPress="return soloNumeros(event);" maxlength="11"  name="Orden[RUC]" onfocusout="RUC($(this).val())" required>
        </div>
        <label class="col-sm-2 control-label">Razon Social: <span class="f_req">*</span></label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="orden-razonsocial" disabled>
            <input type="hidden" name="Orden[RazonSocial]" id="orden-razonsocial2">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Dirección:<span class="f_req">*</span></label>
        <div class="col-sm-4">
            <input type="text" class="form-control"  id="orden-direccion" disabled>
            <input type="hidden" name="Orden[Direccion]" id="orden-direccion2">
        </div>
        <label class="col-sm-2 control-label">Telefono: <span class="f_req">*</span></label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="orden-telefono" name="Orden[Telefono]" >
            <input type="hidden"   id="orden-telefono2">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Tipo de Proceso:<span class="f_req">*</span></label>
        <div class="col-sm-4">
            <select class="form-control" name="Orden[TipoProceso]">
                <option value>[SELECCIONE]</option>
                <option value=1>Comparacion de Precios</option>
                <option value=2>Evaluacion de CVs</option>
                <option value=3>Contratación directa</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Concepto:<span class="f_req">*</span></label>
        <div class="col-sm-10">
            <textarea class="form-control" name="Orden[Concepto]" required><?= $orden->Concepto ?></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Cantidad:<span class="f_req">*</span></label>
        <div class="col-sm-2">
            <input type="text" class="form-control" id="cantidad" value="<?= $orden->Cantidad ?>" disabled>
        </div>
        <label class="col-sm-2 control-label">Precio Unitario:<span class="f_req">*</span></label>
        <div class="col-sm-2">
            <input type="text" class="form-control punitario" id="precio-unitario" name="Orden[PrecioUnitario]" required>
        </div>
        <label class="col-sm-2 control-label">Monto referencial:<span class="f_req">*</span></label>
        <div class="col-sm-2">
            <input type="text" class="form-control" id="total-servicio" value="<?= $PrecioTotal ?>" disabled>
        </div>
    </div>
    
    
    <div class="form-group">
        <div class="col-sm-12">
        <!--
            <button type="button"  class="agregar btn btn-primary">Agregar</button><br><br>
        -->
            <table class="table borderless table-hover" id="detalle" >
                <thead>
                    <tr>
                        <th class="col-sm-2">Código</th>
                        <th class="col-sm-2">Unidad Medida</th>
                        <th class="col-sm-6">Descripción</th>
                        <th class="col-sm-2">Valor Total</th>
                        <th width="22">&nbsp;</th>
                    </tr>
                </thead>
                <tbody class="sdetalleo_">
                    <tr class="detalleo" id="detalle_2">
                        <td><input class="form-control col-sm-2" type="text" name="Orden[Codigos][]" required=""></td>
                        <td><input class="form-control col-sm-2" type="text" name="Orden[UnidadesMedidas][]" required="" value="<?= $orden->UnidadMedida ?>"></td>
                        <td><input class="form-control col-sm-6" type="text" name="Orden[Descripciones][]" required="" value="<?= $orden->Descripcion ?>"></td>
                        <td>
                            <input class="form-control col-sm-2 input-number contadorx" type="text" id="oTotal" disabled>
                            <input class="form-control col-sm-2 " type="hidden" id="vTotal" name="Orden[Totales][]" required="" style="text-align: right;">
                        </td>
                        <td></td>
                    </tr>
                </tbody>
                
                <thead>
                    <tr id="css" style="display: none;">
                        <th></th>
                        <th></th>
                        <th>Total:</th>
                        <th id="total">S/.0.00</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <!--
        <div class="form-group">
            <label class="col-sm-2 control-label">Actividad a desarrollar:<span class="f_req">*</span></label>
            <div class="col-sm-10">
                <textarea type="text" class="form-control" name="Orden[PlazoEntrega]"></textarea>
            </div>
        </div>
        -->
        <!-- <hr>
        <div class="form-group">
            <label class="col-sm-2 control-label">Entregables:<span class="f_req">*</span></label>
            <div class="col-sm-4">
                <select  class="form-control">
                    <option>NO</option>
                    <option>SI</option>
                </select>
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-sm-12">
                <button type="button"  class="agregar btn btn-primary">Agregar</button><br><br>
                <table class="table borderless table-hover" id="detalle" >
                    <thead>
                        <tr>
                            <th class="col-sm-1">#</th>
                            <th class="col-sm-5">Descripcion</th>
                            <th class="col-sm-2">Mes</th>
                            <th width="10">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody class="detalleo_">
                    </tbody>
                </table>
            </div>
        </div>
    -->


    <div class="form-group">
        <label class="col-sm-2 control-label">Plazo del Servicio:<span class="f_req">*</span></label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="Orden[PlazoServicio]" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Condición de pago:<span class="f_req">*</span></label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="Orden[CondicionPago]" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Cuenta Bancaria:<span class="f_req">*</span></label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="Orden[CuentaBancaria]" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">CCI:<span class="f_req">*</span></label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="Orden[CCI]" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Conformidad de servicio estará a cargo de:<span class="f_req">*</span></label>
        <div class="col-sm-4">
            <input type="text" class="form-control" name="Orden[ConformidadServicio]" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Retención de 4ta:<span class="f_req">*</span></label>
        <div class="col-sm-4">
            <select type="text" class="form-control" name="Orden[Retencion4ta]" required>
                <option value>Seleccionar</option>
                <option value=1>No</option>
                <option value=2>Si</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-12">Constancia de suspensión de retención de Cuarta Categoría si corresponde</div>
        <div class="col-sm-12">Conformidad de servicio estará a cargo del Investigador Responsable del Proyecto</div>
    </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
</div>
<?php ActiveForm::end(); ?>
<?php
    $actividades=Yii::$app->getUrlManager()->createUrl('requerimiento/actividades');
    $recursos=Yii::$app->getUrlManager()->createUrl('requerimiento/recursos');
?>
</div>
<div class="modal fade" id="modal-ctrl-poa" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-lg" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">POA</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>
<script>
    //POA(<?= $RequerimientoID ?>);
    
    $('body').on('click', '.agregar', function (e) {
	var detalleo=$('.detalleo').length+1;
	var error = '';
	if (error != '') {
            return false;
	}
	else
        {
	    var option = null;
            var html='<tr class="detalleo" id="detalle_'+(detalleo+1)+'">'+
                    '<td >'+(detalleo+1)+'</td>'+
                    '<td>'+
                        '<input class="form-control col-sm-2" type="text" name="Orden[UnidadesMedidas][]"  required>'+
                    '</td>'+
                    '<td>'+
                        '<select  class="form-control"><option>Enero</option><option>Febrero</option><option>Marzo</option><option>Abril</option><option>Mayo</option><option>Junio</option><option>Julio</option><option>Agosto</option><option>Septiembre</option><option>Octubre</option><option>Nombiembre</option><option>Diciembre</option> </select>'+
                    '</td>'+
                    '<td>'+
                        '<a href="javascript:;" class="eliminar"><i class="fa fa-times fa-2x" aria-hidden="true"></i></a>'+
                    '</td>'+
                '</tr>';
            $('.detalleo_').append(html);
            $('#css').removeAttr('style');
            detalleo++;
            return true;
        }
    });
    /*
    $('#detalle').on('blur','.contadorx',function(){
        contar();
    });
    */
    $('.punitario').on('blur',function(){
       $('#oTotal').val($(this).val()*$('#cantidad').val());
       $('#vTotal').val($(this).val()*$('#cantidad').val());
       contar();
    });
    
    function contar(){
        var total_total = 0.00;
        $('.contadorx').each(function(x,y){
            TotSuma = $(this).val();
            total_total = parseFloat(getNum(TotSuma)) + parseFloat(total_total) ;
            Suma(total_total,parseFloat($('#total-servicio').val()),this);
        });
        if (total_total>parseFloat($('#total-servicio').val())) {
            return false;
        }
        $('#total').text( "S/."+ number_format(total_total,2) );
    }
    
    function Suma(x,y,elemento) {
        if (elemento && x>y) {
             bootbox.alert('El monto total excede al monto referencial');
            $(elemento).val('0.00');
            $('#precio-unitario').val('0.00');
            $('#vTotal').val('0.00');
            contar();
            return false;
        }
        return true;
    }
    
    function getNum(val) {
       if (val == '0.00' || val == '') {
            return 0;
       }
       var val2 = val.replace(',','');
       return val2;
    }
    
    function number_format(amount, decimals) {
        amount += '';
        amount = parseFloat(amount.replace(/[^0-9\.]/g, ''));
        decimals = decimals || 0;
        if (isNaN(amount) || amount === 0) 
            return parseFloat(0).toFixed(decimals);
        amount = '' + amount.toFixed(decimals);
        var amount_parts = amount.split('.'),
            regexp = /(\d+)(\d{3})/;
        while (regexp.test(amount_parts[0]))
            amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');
        return amount_parts.join('.');
    }
    
    $("#detalle").on('click','.eliminar',function(){
        var r = confirm("Estas seguro de Eliminar?");
        var mensaje = '';
        if (r == true) {
            id=$(this).children().val();
            if (id) {
                
                $(this).parent().parent().remove();
            }
            else
            {
                $(this).parent().parent().remove();    
            }   
            mensaje = "Se elimino el Registro Correctamente";
        } 
    });
    
    function POA(valor) {
        $.get('<?= \Yii::$app->request->BaseUrl ?>/orden/poa/', { valor: valor}, function (data) {
            var jx = JSON.parse(data);
            if (jx.Success == true) {
                $('#objetivo').val(jx.componente);
                $('#actividad').val(jx.actividad);
                $('#recurso').val(jx.recurso);
                $('#codigo-poa').val(jx.codigo);    
            }
            
        });
    }
    
    function RUC(valor) {
        $.get('<?= \Yii::$app->request->BaseUrl ?>/orden/servicio/', { valor: valor}, function (data) {
            var jx = JSON.parse(data);
            console.log(jx);
            var jx = JSON.parse(data);
            if (jx==1) {
                bootbox.alert('RUC no se encuentra');
            }
            else
            {
                $.trim($('#orden-razonsocial').val(jx.razonSocial));
                $.trim($('#orden-direccion').val(jx.domicilioLegal));
                $.trim($('#orden-telefono').val(jx.telefono1));
                
                $.trim($('#orden-razonsocial2').val(jx.razonSocial));
                $.trim($('#orden-direccion2').val(jx.domicilioLegal));
                $.trim($('#orden-telefono2').val(jx.telefono1));
            }
            
        });
    }
    
</script>