<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<?php $form = ActiveForm::begin(
    [
        'action'            => \Yii::$app->request->BaseUrl.'/orden/crear-orden-compra?RequerimientoID='.$RequerimientoID.'&CodigoProyecto='.$CodigoProyecto.'&DetallesRequerimientosIDs='.$DetallesRequerimientosIDs,
        'options'           => [
            'enctype'       =>'multipart/form-data',
            'id'=>'frmOrden'
        ]
    ]
); ?>
<div class="modal-body">
    <div class="form-horizontal">
        <?php //print_r($tdr);die() ?>
        <input type="hidden" name="Orden[RequerimientoID]" value="<?= $RequerimientoID ?>">
        
        <div class="form-group">
            <label class="col-sm-2 control-label">RUC:<span class="f_req">*</span></label>
            <div class="col-sm-4">
                <input type="text" onKeyPress="return soloNumeros(event);" maxlength="11"  class="form-control" name="Orden[RUC]" onfocusout="RUC($(this).val())" required>
            </div>
            <label class="col-sm-2 control-label">Razon Social: <span class="f_req">*</span></label>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="orden-razonsocial" disabled>
                <input type="hidden" name="Orden[RazonSocial]" id="orden-razonsocial2">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Dirección:<span class="f_req">*</span></label>
            <div class="col-sm-4">
                <input type="text" class="form-control"  id="orden-direccion" disabled>
                <input type="hidden" name="Orden[Direccion]" id="orden-direccion2">
            </div>
            <label class="col-sm-2 control-label">Telefono: <span class="f_req">*</span></label>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="orden-telefono" name="Orden[Telefono]" >
                <input type="hidden"  id="orden-telefono2">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Tipo de Proceso:<span class="f_req">*</span></label>
            <div class="col-sm-4">
                <select class="form-control" name="Orden[TipoProceso]" required>
                    <option value>[SELECCIONE]</option>
                    <option value=1>Comparacion de Precios</option>
                    <option value=3>Compra Directa</option>
                </select>
            </div>

            <label class="col-sm-2 control-label">Monto referencial:<span class="f_req">*</span></label>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="total-compra" value="S/. <?= $PrecioTotal ?>" disabled>
            </div>

        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Concepto:<span class="f_req">*</span></label>
            <div class="col-sm-10">
                <textarea class="form-control" name="Orden[Concepto]" required><?= $orden->Concepto ?></textarea>
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-sm-12">
            <button type="button" class="agregar btn btn-primary">Agregar</button><br><br>
            <table class="table borderless table-hover" id="detalle" >
                <thead>
                    <tr>
                        <th class="col-sm-2">Cantidad</th>
                        <th class="col-sm-2">Unidad Medida</th>
                        <th class="col-sm-4">Descripcion</th>
                        <th class="col-sm-2">Precio Unitario</th>
                        <th class="col-sm-2">Total</th>
                        <th width="22">&nbsp;</th>
                    </tr>
                </thead>
                <tbody class="detalleo_">
                    
                </tbody>
                <thead>
                    <tr id="css" style="display: none;">
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>Total:</th>
                        <th id="totala">S/.0.00</th>
                    </tr>
                </thead>
            </table>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Plazo Entrega:<span class="f_req">*</span></label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="Orden[PlazoEntrega]" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Lugar de Entrega:<span class="f_req">*</span></label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="Orden[LugarEntrega]" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Condición de pago:<span class="f_req">*</span></label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="Orden[CondicionPago]" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Penalidad:<span class="f_req">*</span></label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="Orden[Penalidad]" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Cuenta Bancaria:<span class="f_req">*</span></label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="Orden[CuentaBancaria]" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">CCI:<span class="f_req">*</span></label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="Orden[CCI]" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Exoneración:<span class="f_req">*</span></label>
            <div class="col-sm-4">
                <!-- <input type="text" class="form-control" name="Orden[ExoneradoIGV]" required> -->
                <select class="form-control" name="Orden[ExoneradoIGV]" required>
                    <option>[SELECCIONE]</option>
                    <option value="1">SI</option>
                    <option value="2">NO</option>
                </select>
            </div>
        </div>
        <?php 
        $caa = '';
        foreach ($catalogo as $cata) {
            $caa .= '<option value="'.trim($cata["Codigo"]).'">'.trim($cata["Descripcion"]).'</option>';
        } ?>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary" id="guarda-documento"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
    <?php ActiveForm::end(); ?>
    <?php
        $actividades=Yii::$app->getUrlManager()->createUrl('requerimiento/actividades');
        $recursos=Yii::$app->getUrlManager()->createUrl('requerimiento/recursos');
    ?>
</div>
    
<div class="modal fade" id="modal-ctrl-poa" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-lg" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">POA</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<script>

    var $form = $('#frmOrden');
    var formParsleyOrden = $form.parsley(defaultParsleyForm());

    //detalleo=1;
    $('body').on('click', '#guarda-documento', function (e) {
        e.preventDefault();
        var cont = $('.detalleo').length;
        // console.log(cont);
        if(cont==0){
            bootbox.alert('Se debe de agregar un item para la orden');
        }else{
            bootBoxConfirm('Esta seguro que desea guardar la orden?',function(){
                $form.submit();
            });
        }
    });

    $('body').on('click', '.agregar', function (e) {
        var detalleo=$('.detalleo').length+1;
    	var error = '';
    	if (error != '') {
                return false;
    	}
    	else
        {
	    var option = null;
            var html=
                    '<tr class="detalleo" id="detalle_'+(detalleo+1)+'">'+
                        '<td>'+
                            '<input class="form-control col-sm-2 input-number" type="text" name="Orden[Cantidades][]" onkeyup="Cantidades($(this).val(),'+detalleo+')" id="cantidades_'+detalleo+'" required>'+
                        '</td>'+
                        '<td>'+
                            '<select class="form-control col-sm-2" name="Orden[UnidadesMedidas][]" required>'+
                            '<?php echo $caa ?>'+
                            '</select>'+
                        '</td>'+
                        '<td>'+
                            '<input class="form-control col-sm-4" type="text" name="Orden[Descripciones][]" required>'+
                        '</td>'+
                        '<td>'+
                            '<input class="form-control col-sm-2 input-number" type="text" name="Orden[PreciosUnitarios][]" onkeyup="PreciosUnitarios($(this).val(),'+detalleo+')" value="0" id="preciosunitarios_'+detalleo+'" required>'+
                        '</td>'+
                        '<td>'+
                            '<input class="form-control col-sm-2 contadorx totales" type="text" name="Orden[Totales][]" id="preciostotales_'+detalleo+'" disabled>'+
                        '</td>'+
                        '<td>'+
                            '<a href="javascript:;" class="eliminar"><i class="fa fa-times" aria-hidden="true"></i></a>'+
                        '</td>'+
                    '</tr>';
                    
            $('.detalleo_').append(html);
            $('#css').removeAttr('style');
            detalleo++;
            validarNumeros();
            return true;
        }
    });

    $('#detalle').on('blur','.contadorx',function(){
        contar();
    });
    
    $('.numerico').on('change',function(){
        
    });
    
    function contar(){
        var total_total = 0.00;
        $('.contadorx').each(function(x,y){
            TotSuma = $(this).val();
            var xtot = TotSuma.replace(',','');
            total_total = parseFloat(getNum(xtot)) + parseFloat(total_total) ;
            // console.log(number_format(parseFloat(total_total),2));
            var totalCompra = $('#total-compra').val();
            // var xTotalCompra = totalCompra.replace(',','');
            // console.log(parseFloat(xTotalCompra));
            Suma( number_format(parseFloat(total_total),2) , parseFloat(totalCompra) ,this);
        });
        var totalCompra = $('#total-compra').val();
        // var xTotalCompra = totalCompra.replace(',','');
        if (number_format(parseFloat(total_total),2)>parseFloat(totalCompra)) {
            return false;
        }
        $('#totala').text( "S/."+ number_format(total_total,2) );
    }
    
    function Suma(x,y,elemento) {
        if (elemento && x>y) {
            $(elemento).val('0.00');
            contar();
            return false;
        }
        
        
        return true;
    }
    
    function getNum(val) {
       if (val == '0.00' || val == '') {
            return 0;
       }
       var val2 = val.replace(',','');
       return val2;
    }
    
    function number_format(amount, decimals) {
        amount += '';
        amount = parseFloat(amount.replace(/[^0-9\.]/g, ''));
        decimals = decimals || 0;
        if (isNaN(amount) || amount === 0) 
            return parseFloat(0).toFixed(decimals);
        amount = '' + amount.toFixed(decimals);
        var amount_parts = amount.split('.'),
            regexp = /(\d+)(\d{3})/;
        while (regexp.test(amount_parts[0]))
            amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');
        return amount_parts.join('.');
    }
    
    $("#detalle").on('click','.eliminar',function(){
        var r = confirm("Estas seguro de Eliminar?");
        var mensaje = '';
        if (r == true) {
            id=$(this).children().val();
            if (id) {
                
                $(this).parent().parent().remove();
            }
            else
            {
                $(this).parent().parent().remove();    
            }   
            mensaje = "Se elimino el Registro Correctamente";
        } 
    });
    
    function Cantidades(valor,correlativo) {
        var precio=$('#preciosunitarios_'+correlativo).val()
        if (precio=='') {
            precio=0;
        }
        console.log("Cantidad:"+valor);
        var montoValor = valor.replace(',','');
        $('#preciostotales_'+correlativo).val(parseFloat(precio)*parseFloat(montoValor));
        Total('#cantidades_'+correlativo);
        contar();
        return true;
    }
    
    function PreciosUnitarios(valor,correlativo) {
        var cantidad=$('#cantidades_'+correlativo).val()
        if (cantidad=='') {
            cantidad=0;
        }
        console.log("Precio:"+valor);
        var montoValor = valor.replace(',','');
        $('#preciostotales_'+correlativo).val(parseFloat(cantidad)*parseFloat(montoValor));
        Total('#preciosunitarios_'+correlativo);
        contar();
        return true;
    }
    
    function Total(elemento) {
        var total=0;
        var PrecioTotal=parseFloat(<?= $PrecioTotal ?>);
        $('.totales').each(function(e,v){
            total=total+parseFloat($(this).val());
            // console.log('Total:'+$(this).val());
            if (PrecioTotal<total) {
                $(this).val('0.00');
                return false;
            }
        });

        if (PrecioTotal<total) {
            //Total(elemento,elementoTotal);
            bootbox.alert('Ha superado el total.');
            $(elemento).val('0.00');
            //$(elementoTotal).val('0');
            return false;
        }
        $('.total').val(total);
        contar();
        
    }
    
    function RUC(valor) {
        $('#orden-razonsocial').val('');
        $('#orden-direccion').val('');
        $('#orden-telefono').val('');
        
        $('#orden-razonsocial2').val('');
        $('#orden-direccion2').val('');
        $('#orden-telefono2').val(''); 
        $.get('<?= \Yii::$app->request->BaseUrl ?>/orden/servicio/', { valor: valor}, function (data) {
            console.log(data);
            var jx = JSON.parse(data);
            if (jx==1) {
                bootbox.alert('RUC no se encuentra');
            }
            else
            {
                $.trim($('#orden-razonsocial').val(jx.razonSocial));
                $.trim($('#orden-direccion').val(jx.domicilioLegal));
                $.trim($('#orden-telefono').val(jx.telefono1));
                
                $.trim($('#orden-razonsocial2').val(jx.razonSocial));
                $.trim($('#orden-direccion2').val(jx.domicilioLegal));
                $.trim($('#orden-telefono2').val(jx.telefono1));
            }
            
        });
    }
    
    function POA(valor) {
        $.get('<?= \Yii::$app->request->BaseUrl ?>/orden/poa/', { valor: valor}, function (data) {
            var jx = JSON.parse(data);
            if (jx.Success == true) {
                $('#objetivo').val(jx.componente);
                $('#actividad').val(jx.actividad);
                $('#recurso').val(jx.recurso);
                $('#codigo-poa').val(jx.codigo);    
            }
            
        });
    }
    
</script>