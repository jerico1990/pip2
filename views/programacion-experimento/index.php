<?php if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start(); ?>
<div id="page-content" style="min-height: 885px;">
    <div id="wrap">
        <div id="page-heading">
            <h1></h1>
        </div>
        <div class="container">
            <style>
                #page-heading {
                    display: none;
                }
                .tab-content {
                    background-color: #fff;
                }
                .panel-gray .panel-heading, .panel-gray .panel-body {
                    border: 1px solid #a8a8ab !important;
                }
                .panel-gray .panel-body {
                    border-top: none !important;
                }
                .panel-heading {
                    height: auto !important;
                }
            </style>
            <div class="tab-container">
                <ul class="nav nav-tabs">
                    <li id="li-poaf" class="active"><a href="#tab-poaf" data-toggle="tab"><i class="fa fa-calendar"></i>&nbsp;Recursos</a></li>
                    <!-- <li id="li-pc"><a href="#container-pc" data-toggle="tab"><i class="fa fa-pencil"></i>&nbsp;Pasos Críticos</a></li> -->
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-poaf">
                        <div id="container-poaf">
                            <style>
                                #container-poaf .input-number,#container-poaf .success {
                                    text-align: right;
                                    min-width: 72px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }
                                .btn-save-cronograma-recurso{
                                    /*text-align: right;
                                    min-width: 72px !important;
                                    font-size: 13px !important;*/
                                    height: 22px !important;
                                }
                                #container-poaf .input-number-sm {
                                    text-align: right;
                                    min-width: 52px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }
                                #container-poaf .input-number-md {
                                    text-align: right;
                                    min-width: 82px !important;
                                    height: 22px !important;
                                    font-size: 13px !important;
                                }
                                #container-poaf table-act > tbody > tr > td:nth-child(1) {
                                    text-align: center;
                                    min-width: 10px !important;
                                }
                                #container-poaf .table-act > tbody > tr > td:nth-child(n) {
                                    min-width: 100px !important;
                                }
                                #container-poaf .table-act > tbody > tr > td:nth-child(4) {
                                    min-width: 100px !important;
                                }
                                #container-poaf .table > tbody > .tr-header > td {
                                    text-align: center;
                                    font-size: 12px;
                                    font-weight: bold;
                                    padding: 1px !important;
                                    vertical-align: middle !important;
                                    /*border: 1px solid #cfcfd0;
                                    background-color: #f0f0f1;*/
                                    border: 1px solid #c0c0c0;
                                    background-color: #e3e3e3;
                                    min-width: 25px !important;
                                }
                                #container-poaf h4.panel-title {
                                    font-size: 16px;
                                }
                                #container-poaf .tr-comp td, .tr-proy td {
                                    font-weight: bold;
                                }
                                #container-poaf .table-condensed td {
                                    padding: 1px !important;
                                }
                                #container-poaf td, #table-container input {
                                    font-size: 13px !important;
                                }
                                #container-poaf .form-control[readonly] {
                                    background-color: #f2f3f4;
                                }
                            </style>
                            <br>
                            <br>
                            <div id="contenedor-programacion-experimentos"></div>
                            <br>
                            <div class="text-center hide" id="hidens">
                                <button class="btn btn-primary btn-enviar-evaluacion-poa" ><i class="fa fa-check"></i>&nbsp;Validar programación de experimentos</button>
                                <a target="_blank" class="btn btn-inverse-alt" href="<?= Yii::$app->getUrlManager()->createUrl('programacion-experimentos/descargar-programacion-experimentos?codigo=') ?>">
                                    <i class="fa fa-download"></i>&nbsp;Descargar Programación de Experimentos
                                </a> 
                            </div> 
                            <script>
                                var _experimentos = new Object();
                                $(document).ready(function () {
                                    $('#contenedor-programacion-experimentos').html('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>');
                                    getExperimentos().done(function(json) {
                                        _experimentos = JSON.parse(JSON.stringify(json));
                                        _PintarTabla();
                                        $('#hidens').removeClass('hide');
                                        $('#hidens2').removeClass('hide');
                                    })
                                });
                                
                                $('.btn-descargar-poa').click(function (e) {
                                    var $btn = $(this);
                                    var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                                    $.ajax({
                                        url: '<?= Yii::$app->getUrlManager()->createUrl('recursos-poa/descargar-poa') ?>',
                                        type: 'GET',
                                        async: false,
                                        data: {codigo:'<?= $usuario->username ?>',_csrf: toke},
                                        success: function(data){
                                           
                                        }
                                    });
                                });
                                
                                $('.btn-aprobar-poa').click(function (e) {
                                    var $btn = $(this);
                                    bootbox.confirm({
                                        title: '<span style="text-align:center">PNIA - CONFIRMACIÓN</span>',
                                        message: 'Está seguro de aprobar el poa del proyecto?.',
                                        buttons: {
                                            'cancel': {
                                                label: 'CANCELAR'
                                            },
                                            'confirm': {
                                                label: 'APROBAR POA'
                                            }
                                        },
                                        callback: function (confirmed) {
                                            if (confirmed) {
                                                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                                                $.ajax({
                                                    url: '<?= Yii::$app->getUrlManager()->createUrl('recursos-poa/aprobar-poa') ?>',
                                                    type: 'GET',
                                                    async: false,
                                                    data: {aprobar:1,codigo:'<?= $usuario->username ?>',_csrf: toke},
                                                    success: function(data){
                                                       
                                                    }
                                                });
                                            } 
                                        }
                                    });
                                });

                                $('body').on('click', '.btn-aprobar-recursos', function (e) {
                                    var $this = $(this);
                                    var ObservacionID=$this.attr('data-id');
                                    bootbox.confirm({
                                        title: '<span style="text-align:center">PNIA - OBSERVACIÓN</span>',
                                        message: 'Está seguro de aprobar la observación ?.',
                                        buttons: {
                                            'cancel': {
                                                label: 'CANCELAR'
                                            },
                                            'confirm': {
                                                label: 'APROBAR OBSERVACIÓN'
                                            }
                                        },
                                        callback: function (confirmed) {
                                            if (confirmed) {
                                                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                                                $.ajax({
                                                    url: '<?= Yii::$app->getUrlManager()->createUrl('recursos-poa/aprobar-observacion') ?>',
                                                    type: 'GET',
                                                    async: false,
                                                    data: {ObservacionID:ObservacionID,_csrf: toke},
                                                    success: function(data){
                                                       
                                                    }
                                                });
                                            } 
                                        }
                                    });
                                });
                                
                                
                                function getNum(val) {
                                    if (val == '0.00' || val == '') {
                                         return 0;
                                    }
                                    var val2 = val.replace(',','');
                                    return val2;
                                }
                                $('body').on('focus', '.btn-save-cronograma-recurso', function (e) {
                                    var $this = $(this);
                                    $this.css('background', 'white');
                                });
                                
                                $('body').on('change', '.btn-save-cronograma-recurso', function (e) {
                                    var $this = $(this);
                                    var ID=$this.attr('data-cronograma-recurso-id');
                                    var RecursoID=$this.attr('data-recurso-id');
                                    var RecursoMetaFisica=parseFloat($this.attr('data-meta-fisica'));
                                    
                                    suma=0;
                                    $('.recurso_'+RecursoID).each(function(){
                                        suma = suma+parseFloat(getNum($(this).val()));
                                    });
                                    
                                    if (RecursoMetaFisica<suma.toFixed(2)) {
                                        bootbox.alert('La cantidad supera a la meta fisica general.');
                                        $this.val('0.00');
                                        return false;
                                    }
                                    if (parseFloat(RecursoMetaFisica)==suma.toFixed(2))
                                    {
                                        $('.recurso_alerta_'+RecursoID).css("color", "black");
                                    }
                                    else
                                    {
                                        $('.recurso_alerta_'+RecursoID).css("color", "red");
                                    }
                                    
                                    var MetaFisica=$this.val();
                                    $.ajax({
                                        cache: false,
                                        type: 'GET',
                                        url: '<?= \Yii::$app->request->BaseUrl ?>/recursos-poa/grabar-meta-cronograma-recurso',
                                        data: {ID:ID,MetaFisica:MetaFisica},
                                        success: function (data) {
                                            var jx = JSON.parse(data);
                                            if (jx.Success == true) {
                                            //if (data.Success) {
                                                $this.css('background', 'green');
                                                //loadPoaf();
                                                _pageLoadingEnd();
                                            } else {
                                                //toastr.error(data.Error);
                                            }
                                        },
                                        error: function () {
                                            _pageLoadingEnd();
                                        }
                                    });
                                    return true;
                                });

                                
                                $('body').on('click', '.btn-enviar-evaluacion-poa', function (e) {
                                    e.preventDefault();
                                    var $this = $(this);
                                    var codigo="<?= $usuario->username ?>";
                                    var error='';
                                    
                                    $('.recurso-meta-fisica').each(function(){
                                        
                                        var suma=0;
                                        var RecursoID=$(this).attr('data-recurso-id');
                                        var RecursoMetaFisica=parseFloat($(this).attr('data-meta-fisica'));
                                        $('.recurso_'+RecursoID).each(function(){
                                            suma = suma+parseFloat(getNum($(this).val()));
                                            
                                        });
                                        
                                        if (RecursoMetaFisica.toFixed(2)>suma.toFixed(2)) {
                                            
                                            error=error+'a';
                                        }
                                    });
                                    
                                    
                                    if (error!='') {
                                        bootbox.alert('Falta programar algunos recurso');
                                        return false;
                                    }
                                    bootbox.alert('Su programación de recursos, esta correcta');
                                    
                                });

                                
                                function getExperimentos() {
                                    var url = "<?= \Yii::$app->request->BaseUrl ?>/programacion-experimento/experimentos-json?codigo=<?= $informacionGeneral->Codigo ?>";
                                    return $.getJSON( url );
                                }

                                function _PintarTabla() {
                                   console.time('Test performance');
                                    var html = '';
                                    var cantmeses = _experimentos.Cronogramas;
                                    
                                    for (var i_objetivo = 0; i_objetivo < _experimentos.Objetivos.length; i_objetivo++) {
                                        var objetivo = _experimentos.Objetivos[i_objetivo];
                                        
                                        html += '<div class="panel panel-gray">';
                                            html += '<div class="panel-heading">';
                                                html += '<h4 class="title-obj panel-title">';
                                                    html += (i_objetivo + 1) + '. Objetivo específico - ' + objetivo.Nombre ;
                                                html += '</h4>';
                                            html += '</div>';
                                            
                                            html += '<div class="panel-heading">';
                                                html += '<h4 class="panel-title">';
                                                    html += 'Total de Objetivo: <span class="input-number" style="font-size:15px !important">'+objetivo.TotalObjetivo+'</span>' ;
                                                html += '</h4>';
                                            html += '</div>';
                                            
                                            html += '<div class="panel-body">';
                                                html += '<div class="table-responsive">';
                                                    html += '<table class="table table-bordered table-condensed table-act">';
                                                        html += '<tbody>';
                                                        for (var i_actividad = 0; i_actividad < objetivo.Actividades.length; i_actividad++)
                                                        {
                                                            var actividad = objetivo.Actividades[i_actividad];
                                                           
                                                            html += '<tr class="tr-header">';
                                                                html += '<td>#</td>';
                                                                html += '<td>Actividad/Experimentos</td>';
                                                                html += '<td>Meta física</td>';
                                                                var m=1;
                                                                for (var i_cronograma_actividad = 0; i_cronograma_actividad < cantmeses.length; i_cronograma_actividad++) {
                                                                    var Cronograma = _experimentos.Cronogramas[i_cronograma_actividad];
                                                                    html += '<td> Mes ' + Cronograma.MesDescripcion + '</td>';
                                                                    m++;
                                                                }
                                                            html += '</tr>';
                                                            
                                                            
                                                            html += '<tr data-actividad-id="' + actividad.ID + '">';
                                                                html += '<td>' + (i_objetivo + 1) + '.' + (i_actividad + 1) + '</td>';
                                                                html += '<td><b>' + actividad.Nombre + '</b></td>';
                                                                html += '<td align="center">' + actividad.MetaFisica + '</td>';
                                                                html += '<td colspan="'+m+'"></td>';
                                                            html += '</tr>';
                                            
                                                            html += '<tr>';
                                                            if (actividad.ID) {
                                                                html += '<td colspan="4">';
                                                                    html += '<a href="javascript:void(0);" data-actividad-id="'+ actividad.ID +'" style="margin: 5px;font-size:12px" class="btn btn-primary btn-agregar-experimento"><i class="fa fa-plus"></i>&nbsp;Agregar Experimento</a>';
                                                                html += '</td>';
                                                            }
                                                            html += '</tr>';
                                                            for (var i_experimentos = 0; i_experimentos < actividad.Experimentos.length; i_experimentos++) {
                                                                var experimento = actividad.Experimentos[i_experimentos];
                                                                if (experimento) {
                                                                    html += '<tr>';
                                                                        html += '<td align="center"><a href="#" data-id="'+experimento.ID+'" class="btn-editar-experimento" data-toggle="tooltip" title="Editar"><i class="fa fa-edit fa-lg"></i></a> <a href="#" class="btn-eliminar-experimento" data-toggle="tooltip" title="Eliminar" ><i class="fa fa-remove fa-lg"></i></a></td>';
                                                                        html += '<td align="center">'+experimento.Nombre+'</td>';
                                                                        html += '<td align="center">'+experimento.MetaFisica+'</td>';
                                                                        for (var i_cronograma_experimento = 0; i_cronograma_experimento < cantmeses.length; i_cronograma_experimento++) {
                                                                            var CronogramaExperimento = _experimentos.Cronogramas[i_cronograma_experimento];
                                                                            html += '<td><input style="height:22px" type="text" data-experimento-id="'+ experimento.ID +'" data-meta-fisica="'+ experimento.MetaFisica +'"  class="form-control experimento_'+experimento.ID+' btn-save-cronograma-experimento" /></td>';
                                                                        }
                                                                    html += '</tr>';
                                                                }
                                                              
                                                            }
                                                        }
                                                        html += '</tbody>';
                                                    html += '</table>';
                                                html += '</div>';
                                            html += '</div>';
                                        html += '</div>';
                                    }
                                    $('#contenedor-programacion-experimentos').html(html);
                                    $('.table-responsive').css('max-height', $(window).height() * 0.60);
                                    validarNumeros();
                                    console.timeEnd('Test performance');
                                }
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-ctrl-crear-experimento" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Experimentos</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-ctrl-editar-experimento" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Experimentos</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>
<script>
    $('body').on('click', '.btn-agregar-experimento', function (e) {
        e.preventDefault();
        var ActividadID = $(this).attr('data-actividad-id');
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        var CodigoProyecto="<?= $informacionGeneral->Codigo ?>";
        $('#modal-ctrl-crear-experimento .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/programacion-experimento/crear-experimento?CodigoProyecto='+CodigoProyecto+'&ActividadID='+ActividadID+'&_csrf='+toke);
        $('#modal-ctrl-crear-experimento').modal('show');
    });
    
    $('body').on('click', '.btn-editar-experimento', function (e) {
        e.preventDefault();
        var ID=$(this).attr('data-id');
        $('#modal-ctrl-editar-experimento .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/programacion-experimento/editar-experimento?ID='+ID);
        $('#modal-ctrl-editar-experimento').modal('show');
    });
</script>