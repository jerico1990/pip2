<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'programacion-experimento/crear-experimento?CodigoProyecto='.$CodigoProyecto.'&ActividadID='.$ActividadID,
        'options'           => [
            'autocomplete'  => 'off',
            'id'            => 'frmProgramacionExperimento'
        ]
    ]
); ?>
    <input type="hidden" name="ProgramacionExperimento[ActividadID]" value="<?php echo $ActividadID ?>">
    <div class="modal-body">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label">Descripción <span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="ProgramacionExperimento[Nombre]" required>
                </div>
            </div>
            <hr>

            <div class="form-group">
                <label class="col-sm-3 control-label">Meta Fisica <span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="number" class="form-control" name="ProgramacionExperimento[MetaFisica]" required>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button id="btn-guardar-experimento" type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>


<script>
    var $form = $('#frmProgramacionExperimento');
    var formParsleyExperimento = $form.parsley(defaultParsleyForm());

    $(document).ready(function () {
        $('.count-message').each(function () {
            var $this = $(this);
            var $divformgroup = $this.closest('div.form-group');
            var $txt = $divformgroup.find('textarea, input[type="text"]');
            var text_max = parseInt($txt.attr('maxlength'));

            $txt.keyup(function () {
                var text_length = $txt.val().length;
                var text_remaining = text_max - text_length;
                $this.html(text_remaining + ' caracteres restantes');
            });
        });
    });
</script>