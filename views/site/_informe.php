<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<div id="page-content" style="min-height: 1342px;">
<div id="wrap">
    <div id="page-heading">
        <h1>INFORME DE EVALUACIÓN TÉCNICA FINANCIERA</h1>
    </div>
    <div class="container">
        <style>
            .div-label-left b {
                margin-bottom: 10px;
                font-size: 18px;
            }
            .count-message {
                color: #2460AA;
                margin-top: 5px;
            }
        </style>
        <?php $form = ActiveForm::begin(); ?>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4>Datos Generales</h4>
                <div class="options">
                    <a class="panel-collapse" href="#">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Fecha de recepción de IT <span class="f_req">*</span></label>
                    <div class="col-sm-2">
                        <input class="form-control" value="" type="date">
                    </div>
                    <label class="col-sm-3 control-label">N° de contrato <span class="f_req">*</span></label>
                    <div class="col-sm-5">
                        <input class="form-control" value="" type="text">
                    </div><br>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-2 control-label">Responsable de evaluación <span class="f_req">*</span></label>
                    <div class="col-sm-10">
                        <input class="form-control" value="" type="text">
                    </div><br>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-2 control-label">Paso critico a evaluar <span class="f_req">*</span></label>
                    <div class="col-sm-2">
                        <input class="form-control" value="" type="text">
                    </div>
                    <label class="col-sm-2 control-label">Periodo <span class="f_req">*</span></label>
                    <div class="col-sm-2">
                        <input class="form-control" value="" type="text">
                    </div>
                </div>
            </div>
        </div>
        
        <?php ActiveForm::end(); ?>
    </div>
</div>
</div>