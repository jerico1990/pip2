<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'experimento/crear?CodigoProyecto='.$CodigoProyecto,
        'options'           => [
            'enctype'       =>'multipart/form-data',
            
        ]
    ]
); ?>
    <input type="hidden" name="Experimento[CodigoProyecto]" value="<?= $CodigoProyecto ?>">
    <div class="modal-body">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label">Objetivo:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <select class="form-control" name="Experimento[ComponenteID]" id="experimento-componenteid" onchange="Actividad($(this).val())">
                        <option>[SELECCIONE]</option>
                        <?php foreach($componentes as $componente){ ?>
                            <option value="<?= $componente->ID ?>"><?= $componente->Correlativo ?>.- <?= $componente->Nombre ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Actividad:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <select class="form-control" name="Experimento[ActividadID]" id="experimento-actividadid" onchange="Recurso($(this).val())">
                        
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Título experimento:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="Experimento[Titulo]" >
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Responsable del experimento:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="Experimento[ResponsableExperimento]" >
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Tipología:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <select class="form-control" name="Experimento[Tipologia]">
                        <option>[SELECCIONE]</option>
                        <option value=1>Transferencia</option>
                        <option value=2>Capacitación</option>
                        <option value=3>Taller de inicio</option>
                        <option value=4>Taller de cierre</option>
                        <option value=5>Capacitación y experimentos</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Duración:(en horas)<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="Experimento[Duracion]" >
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Beneficiario:(hombres)<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="Experimento[BeneficiarioHombres]" >
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Beneficiario:(mujeres)<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="Experimento[BeneficiarioMujeres]" >
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Introducción:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="Experimento[Introduccion]"></textarea>
                </div>
            </div>
            
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Material:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="Experimento[Material]"></textarea>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Métodos:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="Experimento[Metodo]"></textarea>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Resultados:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="Experimento[Resultado]"></textarea>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Costo:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="Experimento[Costo]" >
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Anexos:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input class="form-control" type="file" name="Experimento[Archivo]">
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>
<?php
    $actividades=Yii::$app->getUrlManager()->createUrl('termino-referencia/actividades');
    $recursos=Yii::$app->getUrlManager()->createUrl('termino-referencia/recursos');
?>
<script>
    function Actividad(valor) {
        var ComponenteID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $actividades ?>',
            type: 'POST',
            async: false,
            data: {ComponenteID:ComponenteID,_csrf: toke},
            success: function(data){
               $( "#experimento-actividadid" ).html( data );
            }
        });
    }
    
</script>