<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'experimento/actualizar?ID='.$experimento->ID,
        'options'           => [
            'enctype'       =>'multipart/form-data',
            
        ]
    ]
); ?>
    <input type="hidden" name="Experimento[CodigoProyecto]" value="<?= $experimento->CodigoProyecto ?>">
    <div class="modal-body">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label">Objetivo:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <select class="form-control" name="Experimento[ComponenteID]" id="evento-componenteid" onchange="Actividad($(this).val())">
                        <option>[SELECCIONE]</option>
                        <?php foreach($componentes as $componente){ ?>
                            <option value="<?= $componente->ID ?>" <?= ($componente->ID==$experimento->ComponenteID)?'selected':'';?>><?= $componente->Correlativo ?>.- <?= $componente->Nombre ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Actividad:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <select class="form-control" name="Experimento[ActividadID]" id="evento-actividadid" onchange="Recurso($(this).val())">
                        <?php foreach($actividades as $actividad){ ?>
                            <option value="<?= $actividad->ID ?>" <?= ($actividad->ID==$experimento->ActividadID)?'selected':'';?>><?= $actividad->Nombre ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Título evento:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="Experimento[Titulo]" value="<?= $experimento->Titulo ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Responsable del evento:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="Experimento[ResponsableExperimento]" value="<?= $experimento->ResponsableExperimento ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Tipología:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <select class="form-control" name="Experimento[Tipologia]">
                        <option>[SELECCIONE]</option>
                        <option value=1  <?= ($experimento->Tipologia==1)?'selected':'';?>>Transferencia</option>
                        <option value=2  <?= ($experimento->Tipologia==2)?'selected':'';?>>Capacitación</option>
                        <option value=3  <?= ($experimento->Tipologia==3)?'selected':'';?>>Taller de inicio</option>
                        <option value=4  <?= ($experimento->Tipologia==4)?'selected':'';?>>Taller de cierre</option>
                        <option value=5  <?= ($experimento->Tipologia==5)?'selected':'';?>>Capacitación y eventos</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Duración:(en horas)<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="Experimento[Duracion]" value="<?= $experimento->Duracion ?>">
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Beneficiario:(hombres)<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="Experimento[BeneficiarioHombres]" value="<?= $experimento->BeneficiarioHombres ?>">
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Beneficiario:(mujeres)<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="Experimento[BeneficiarioMujeres]" value="<?= $experimento->BeneficiarioMujeres ?>">
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Introducción:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="Experimento[Introduccion]"><?= $experimento->Introduccion ?></textarea>
                </div>
            </div>
            
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Material:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="Experimento[Material]"><?= $experimento->Material ?></textarea>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Métodos:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="Experimento[Metodo]"><?= $experimento->Metodo ?></textarea>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Resultados:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="Experimento[Resultado]"><?= $experimento->Resultado ?></textarea>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Costo:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="Experimento[Costo]" value="<?= $experimento->Costo ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Anexos:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input class="form-control" type="file" name="Experimento[Archivo]">
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>
<?php
    $actividades=Yii::$app->getUrlManager()->createUrl('termino-referencia/actividades');
    $recursos=Yii::$app->getUrlManager()->createUrl('termino-referencia/recursos');
?>
<script>
    function Actividad(valor) {
        var ComponenteID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $actividades ?>',
            type: 'POST',
            async: false,
            data: {ComponenteID:ComponenteID,_csrf: toke},
            success: function(data){
               $( "#evento-actividadid" ).html( data );
            }
        });
    }
    
</script>