<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay_progress.min.js"></script>
<script src=" //cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<div id="page-content" style="min-height: 1342px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Permiso</h1>
        </div>
        <div class="container">
            <div class="form-group">
                <button class="btn btn-primary btn-crear-formulario">Generar nuevo rol</button>
            </div>
            <div class="table-responsive">
                <table id="proyectos" class="display table table-bordered table-striped table-condensed table-hover" cellspacing="0" >
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Usuario</th>
                            <th>Nombre</th>
                            <th>Nombre Rol</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-formulario" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Usuario Rol</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

    
<script>
    
    function js_buscar()
    {
       
    }
    var tblresultjs;
    var configDTjs={
        "order": [[ 1, "desc" ]],
            "language": {
                "sProcessing":    "Procesando...",
                "sLengthMenu":    "Mostrar _MENU_ registros",
                "sZeroRecords":   "No se encontraron resultados",
                "sEmptyTable":    "Ningún dato disponible en esta lista",
                "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":   "",
                "sSearch":        "Buscar:",
                "sUrl":           "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":    "Último",
                    "sNext":    "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
    };

    $(document).ready(function() {
        listaTabla();
    });
    
    function listaTabla(){
        $.ajax({
            url:'usuario-rol/lista-usuario',
            async:false,
            data:{},
            beforeSend:function()
            {
                $('.panel-default').LoadingOverlay("show");
            },
            success:function(result)
            {
                    //$tblresultjs.destroy();//
                    $("#proyectos tbody").html(result);
                    $('#proyectos').DataTable(configDTjs);
                    //tblresultjs=$('#example').DataTable(configDTjs);
                    $('.panel-default').LoadingOverlay("hide",true);
            },
            error:function(){
                $('.panel-default').LoadingOverlay("hide",true);
                alert('Error al realizar el proceso de busqueda.');
            }
        });
    }
    
    $('body').on('click', '.btn-crear-formulario', function (e) {
        e.preventDefault();
        AjaxSendForm('<?= \Yii::$app->request->BaseUrl ?>/usuario-rol/crear','#modal-formulario .modal-body-main', {}, false);
        $('#modal-formulario').modal('show');
    });

    $('body').on('click', '.btn-edit', function (e) {
        e.preventDefault();
        var codigo = $(this).attr('data-id');
        AjaxSendForm('<?= \Yii::$app->request->BaseUrl ?>/usuario-rol/crear', "#modal-formulario .modal-body-main", {'id':codigo}, false);
        $('#modal-formulario').modal('show');
    });

    $('body').on('click', '.btn-remove', function (e) {
        e.preventDefault();
        var $this = $(this);
        bootboxConfirmEliminar($this,
            'Está seguro de eliminar el registro?',
            function () {
                var id = $this.attr('data-id');
                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                $.get('<?= \Yii::$app->request->BaseUrl ?>/usuario-rol/eliminar', { id: id ,_csrf: toke }, function (data) {
                    var jx = JSON.parse(data);
                    if (jx.Success == true) {
                        location.reload();
                    }
                });
            });
    });


    var i = 0;
    function AjaxSendForm(url, placeholder,data, append) {
        // var data = $(form).serialize();
        append = (append === undefined ? false : true); 
        $.ajax({
            type: 'GET',
            url: url,
            data: data,
            beforeSend: function() {
                $(placeholder).html("");
                $(placeholder).append('<div class="loading"><i class="fa fa-spinner fa-pulse fa-4x fa-fw"></i></div>');
                i++;
            },
            success: function(data) {
                $(placeholder).html("");
                if (append) {
                    $(placeholder).append(data);
                } else {
                    $(placeholder).html(data);
                }
            },
            error: function(xhr) {
                alert("Error occured.please try again");
                $(placeholder).append(xhr.statusText + xhr.responseText);
            },
            complete: function() {
                i--;
                if (i <= 0) {
                    $(".loading").remove();
                }
            },
            dataType: 'html'
        });
    }

</script>

<style type="text/css">
    .modal-body-main .loading{
        text-align: center;
    }

    .loading{
        margin: 40px;
    }
</style>