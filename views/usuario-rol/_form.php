<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UsuarioRol */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usuario-rol-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="form-group field-usuariorol-usuarioid required">
        <label class="control-label" for="usuariorol-usuarioid">Usuario ID</label>
        <select type="text" id="usuariorol-usuarioid" class="form-control" name="UsuarioRol[UsuarioID]">
            <option value>Seleccionar</option>
            <?php foreach($usuarios as $usuario){ ?>
                <option value="<?= $usuario->ID ?>"><?= $usuario->username ?></option>
            <?php } ?>
        </select>
    </div>
    
    <div class="form-group field-usuariorol-rolid required">
        <label class="control-label" for="usuariorol-rolid">Rol ID</label>
        <select type="text" id="usuariorol-rolid" class="form-control" name="UsuarioRol[RolID]">
            <option value>Seleccionar</option>
            <?php foreach($roles as $rol){ ?>
                <option value="<?= $rol->ID ?>"><?= $rol->Nombre ?></option>
            <?php } ?>
        </select>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
