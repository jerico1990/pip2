<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\UsuarioRol */

?>
<div id="page-content" style="min-height: 754px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Ver Asignación </h1>
        </div>
        <div class="container">
            
            <div class="usuario-rol-view">
            
                <h1><?= Html::encode($this->title) ?></h1>
            
                <p>
                    <?= Html::a('Update', ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->ID], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </p>
            
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'ID',
                        'UsuarioID',
                        'RolID',
                    ],
                ]) ?>
            
            </div>
        </div>
    </div>
</div>