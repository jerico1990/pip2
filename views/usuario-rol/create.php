<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>


<?php $form = ActiveForm::begin(
    [
        'action'            => empty($model) ? 'usuario-rol/create': 'usuario-rol/update?id='.$model->ID,
        'options'           => [
            'enctype'       =>'multipart/form-data',
            'id'            =>'frmCrearRol'
        ]
    ]
); ?>
    <div class="modal-body">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label">Usuario:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                <?php if($model){
                    $usux = $model->UsuarioID;
                    $rolx = $model->RolID;
                }else{
                    $usux = '';
                    $rolx = '';
                }
                ?>
                    <select class="form-control" name="UsuarioRol[UsuarioID]" required>
                        <option value>Seleccione</option>
                        <?php foreach ($usuario as $usu): ?>
                            <option value="<?php echo $usu->ID; ?>" <?php echo $usux==$usu->ID?'selected':'' ?> ><?php echo $usu->username ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Rol:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <select class="form-control" name="UsuarioRol[RolID]" required>
                        <option value>Seleccione</option>
                        <?php foreach ($rol as $ro): ?>
                            <option value="<?php echo $ro->ID; ?>"  <?php echo $rolx==$ro->ID?'selected':'' ?> ><?php echo $ro->Nombre ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>

        </div>
    </div>
    
    
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary" id="btn-guardar-requerimientos"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>


<style type="text/css">
    .none{
        display: none;
    }
    .block{
        display: block;
    }

    .ui-draggable, .ui-droppable {
      background-position: top;
    }
    .ui-widget-content{
        position: relative;
        z-index: 9999;
        width: 400px !important;
    }

    .tbl-act > thead > .tr-header > th {
        text-align: center;
        font-size: 12px;
        font-weight: bold;
        padding: 5px !important;
        vertical-align: middle !important;
        /*border: 1px solid #cfcfd0;
        background-color: #f0f0f1;*/
        border: 1px solid #c0c0c0;
        background-color: #e3e3e3;
    }

</style>



<script>
    var $form = $('#frmCrearRol');
    var frmCrearRol = $form.parsley(defaultParsleyForm());
    $('.datepicker').datepicker(defaultDatePicker());
    function Tipo(elemento) {
        console.log($(elemento).val());
        if ($(elemento).val()=='5') {
            $('.detallet').hide();
        }
        else
        {
            $('.detallet').show();
        }
    }
</script>

