<style>
.panel-primary .panel-heading {
  background-color: #595f69;
}

[class*="panel-"].panel .panel-body {
      background-color: #EAEAEA;
}


.form-group {
      background-color: #fff;
    }


.thumbnail1 {
  display: block;
  padding: 10px;
  margin-bottom: 20px;
  line-height: 1.428571429;
  background-color: #5bcdbb;
  border: 1px solid #e6e7e8;
  border-radius: 1px;
  -webkit-transition: all 0.2s ease-in-out;
  transition: all 0.2s ease-in-out;
}
.thumbnail1 > img,
.thumbnail1 a > img {
  display: block;
  max-width: 100%;
  height: auto;
  margin-left: auto;
  margin-right: auto;
}
a.thumbnail1:hover,
a.thumbnail1:focus,
a.thumbnail1.active {
  border-color: #4f8edc;
}
.thumbnail1 .caption {
  padding: 10px;
  color: #4d4d4d;
}

.thumbnail2 {
  display: block;
  padding: 10px;
  margin-bottom: 20px;
  line-height: 1.428571429;
  background-color: #a5d44e;
  border: 1px solid #e6e7e8;
  border-radius: 1px;
  -webkit-transition: all 0.2s ease-in-out;
  transition: all 0.2s ease-in-out;
}
.thumbnail2 > img,
.thumbnail2 a > img {
  display: block;
  max-width: 100%;
  height: auto;
  margin-left: auto;
  margin-right: auto;
}
a.thumbnail2:hover,
a.thumbnail2:focus,
a.thumbnail2.active {
  border-color: #4f8edc;
}
.thumbnail2 .caption {
  padding: 10px;
  color: #4d4d4d;
}

.thumbnail3 {
  display: block;
  padding: 10px;
  margin-bottom: 20px;
  line-height: 1.428571429;
  background-color: #d69525;
  border: 1px solid #e6e7e8;
  border-radius: 1px;
  -webkit-transition: all 0.2s ease-in-out;
  transition: all 0.2s ease-in-out;
}
.thumbnail3 > img,
.thumbnail3 a > img {
  display: block;
  max-width: 100%;
  height: auto;
  margin-left: auto;
  margin-right: auto;
}
a.thumbnail3:hover,
a.thumbnail3:focus,
a.thumbnail3.active {
  border-color: #4f8edc;
}
.thumbnail3 .caption {
  padding: 10px;
  color: #4d4d4d;
}











  .lista-reportes{
    padding-left: 0px !important;
  }
  ul.lista-reportes > li {
    list-style: none;
  }
  ul.lista-reportes > li > a {
    display: block;
    padding: 3px 5px;
    clear: both;
    font-weight: normal;
    line-height: 1.428571429;
    color: #fff;
    white-space: normal;
  }

  ul.lista-reportes > li > a:hover,
  ul.lista-reportes > li > a:focus {
    text-decoration: none;
    color: #444;
    background-color: #f5f5f5;
  }
</style>

<div id="page-content" style="min-height: 934px;">
    <div id="wrap">
        <div class="container">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4 id="tituloReporte">Reporte BI</h4>
                    <div class="options">
                      <a id="repSiguiente" href="#" style="color: white; display:none;">Siguiente</a>
                      <a id="blk" href="#" style="color: white; display:none;">|</a>
                      <a id="repAnterior" href="#" style="color: white; display:none;">Anterior</a>
                      <a id="blk2" href="#" style="color: white; display:none;">|</a>
                      <a id="repPrincipal" href="#" style="color: white; display:none;">Principal</a>
                    </div>
                </div>


                <div class="panel-body">


                  <div id="repMenu" class="form-horizontal">



                    <div class="embed-responsive embed-responsive-16by9" >
                        <iframe src="https://app.powerbi.com/view?r=eyJrIjoiNTdiOGI0NzgtZTJkNC00MTcwLWE5N2QtNjQwYTEzYzc2NzM4IiwidCI6IjE2N2M0ZGUyLWMzMDItNGU4ZS1hODU0LTEyYjcwZmRhMjJlZSIsImMiOjR9" class="embed-responsive-item" frameborder="0" allowfullscreen="" style="width: 100%; height: 640px;"></iframe>
                    </div>



                    <div class="form-group">

                        <div class="col-sm-4">
                            &nbsp; &nbsp;
                          <h3 class="text-center">Formulaci&oacute;n</h3>
                          <div class="thumbnail1">
                            <img id="img1" src="/pip2/web/recursos/img/Inversion_BI.jpg" alt="..." class="img-rounded">
                            <div class="caption">
                              <ul id="list-Inversion" class="lista-reportes"></ul>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-4">
                            &nbsp; &nbsp;
                          <h3 class="text-center">Programaci&oacute;n</h3>
                          <div class="thumbnail2">
                            <img id="img2" src="/pip2/web/recursos/img/Programacion_BI.jpg" alt="..." class="img-rounded">
                            <div class="caption">
                              <ul id="list-Programacion" class="lista-reportes"></ul>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-4">
                            &nbsp; &nbsp;
                          <h3 class="text-center">Ejecuci&oacute;n</h3>
                          <div class="thumbnail3">
                            <img id="img3" src="/pip2/web/recursos/img/Ejecucion_BI.jpg" alt="..." class="img-rounded">
                            <div class="caption">
                              <ul id="list-Ejecucion" class="lista-reportes"></ul>
                            </div>
                          </div>
                        </div>
                          &nbsp; &nbsp;
                    </div>



                  </div>


                  <div id="frame" class="embed-responsive embed-responsive-16by9" style="display: none;">
                    <iframe id="if_reporte" class="embed-responsive-item" frameborder="0" allowfullscreen="" style="width: 100%; height: 640px;"></iframe>
                  </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var urls = [
  { id: 1, texto: 'Inversión', url: 'https://app.powerbi.com/view?r=eyJrIjoiODYxYTk4NWYtOGFjNi00MzE0LTg2NjktZDI4MmQwODAyNzc0IiwidCI6IjE2N2M0ZGUyLWMzMDItNGU4ZS1hODU0LTEyYjcwZmRhMjJlZSIsImMiOjR9', img: '', tipo: 'Inv' },
  { id: 2, texto: 'Inversión por Región', url: 'https://app.powerbi.com/view?r=eyJrIjoiMzJjNWJkYjEtYmNlNy00NWYwLWE3ZWQtYjMzZmEzMzNlODZkIiwidCI6IjE2N2M0ZGUyLWMzMDItNGU4ZS1hODU0LTEyYjcwZmRhMjJlZSIsImMiOjR9', img: '', tipo: 'Inv' },
  { id: 3, texto: 'Inversión por Estación', url: 'https://app.powerbi.com/view?r=eyJrIjoiMTM0ZmZjYTctMWRlNi00ZWU5LWE0NDYtZWViZGIzMjg3MGU2IiwidCI6IjE2N2M0ZGUyLWMzMDItNGU4ZS1hODU0LTEyYjcwZmRhMjJlZSIsImMiOjR9', img: '', tipo: 'Inv' },
  { id: 4, texto: 'Inversión por Investigación', url: 'https://app.powerbi.com/view?r=eyJrIjoiOTA2YTc4NjQtYWRlNy00NTViLTg5ZTctM2M5ZDNkNjY4ZmI5IiwidCI6IjE2N2M0ZGUyLWMzMDItNGU4ZS1hODU0LTEyYjcwZmRhMjJlZSIsImMiOjR9', img: '', tipo: 'Inv' },
  { id: 5, texto: 'Inversión por Área Temática', url: 'https://app.powerbi.com/view?r=eyJrIjoiNjc4Zjc3NmYtMTNhYi00ZDMyLTlmNzktYzdkNTdiZjM4MjU3IiwidCI6IjE2N2M0ZGUyLWMzMDItNGU4ZS1hODU0LTEyYjcwZmRhMjJlZSIsImMiOjR9', img: '', tipo: 'Inv' },
  { id: 6, texto: 'Inversión por Especie', url: 'https://app.powerbi.com/view?r=eyJrIjoiODJmYWFiZTItYzgxMS00OTFlLWJlNTAtMWZlY2JiYjM3OTY4IiwidCI6IjE2N2M0ZGUyLWMzMDItNGU4ZS1hODU0LTEyYjcwZmRhMjJlZSIsImMiOjR9', img: '', tipo: 'Inv' },
  { id: 7, texto: 'Inversión por Cultivo', url: 'https://app.powerbi.com/view?r=eyJrIjoiZjI2YmIxNjItYWU1MC00NzVlLThmNjAtNWI0Yzk2YWNkMDY4IiwidCI6IjE2N2M0ZGUyLWMzMDItNGU4ZS1hODU0LTEyYjcwZmRhMjJlZSIsImMiOjR9', img: '', tipo: 'Inv' },

  { id: 8, texto: 'Desembolso', url: 'https://app.powerbi.com/view?r=eyJrIjoiM2JiNjBlNGUtZDNiOS00ZWNiLWExNmQtNGIyOWYyNThkNTZlIiwidCI6IjE2N2M0ZGUyLWMzMDItNGU4ZS1hODU0LTEyYjcwZmRhMjJlZSIsImMiOjR9', img: '', tipo: 'Prg' },
  { id: 9, texto: 'Programación por Periodo', url: 'https://app.powerbi.com/view?r=eyJrIjoiOTMzNWI0Y2YtM2FhMy00MTI5LTg4MDUtNmYwMGQwOWU3NTAyIiwidCI6IjE2N2M0ZGUyLWMzMDItNGU4ZS1hODU0LTEyYjcwZmRhMjJlZSIsImMiOjR9', img: '', tipo: 'Prg' },
  { id: 10, texto: 'Programación por Región', url: 'https://app.powerbi.com/view?r=eyJrIjoiOGQxODAyZTEtNTZlNy00YzEwLWFhZWItYjc1OWExOTgyNDBmIiwidCI6IjE2N2M0ZGUyLWMzMDItNGU4ZS1hODU0LTEyYjcwZmRhMjJlZSIsImMiOjR9', img: '', tipo: 'Prg' },
  { id: 11, texto: 'Programación por Ejecutora', url: 'https://app.powerbi.com/view?r=eyJrIjoiMjU1NWIwNWUtMWMxZi00MjVlLThhYWItOTZmN2VkZjUxYzBlIiwidCI6IjE2N2M0ZGUyLWMzMDItNGU4ZS1hODU0LTEyYjcwZmRhMjJlZSIsImMiOjR9', img: '', tipo: 'Prg' },
  { id: 12, texto: 'Programación por Estación', url: 'https://app.powerbi.com/view?r=eyJrIjoiM2Y0ZWI5ZDMtNjNlNS00YjllLWI5MGUtZDc3YmNmYzg0NDFlIiwidCI6IjE2N2M0ZGUyLWMzMDItNGU4ZS1hODU0LTEyYjcwZmRhMjJlZSIsImMiOjR9', img: '', tipo: 'Prg' },
  { id: 13, texto: 'Programación por Proyecto', url: 'https://app.powerbi.com/view?r=eyJrIjoiMmE2ZDJmYWQtNjg1MS00Mjk3LTlmMTMtZDFkN2E5YTcxYjBmIiwidCI6IjE2N2M0ZGUyLWMzMDItNGU4ZS1hODU0LTEyYjcwZmRhMjJlZSIsImMiOjR9', img: '', tipo: 'Prg' },
  { id: 14, texto: 'Programación por Especialista', url: 'https://app.powerbi.com/view?r=eyJrIjoiYzYwODg3ODAtNmFkNS00NzI3LTg5OTctMDJlNTNjMDFkOTk1IiwidCI6IjE2N2M0ZGUyLWMzMDItNGU4ZS1hODU0LTEyYjcwZmRhMjJlZSIsImMiOjR9', img: '', tipo: 'Prg' },

  { id: 15, texto: 'Ejecución Financiera por CPI', url: 'https://app.powerbi.com/view?r=eyJrIjoiMDM1NjYyNzItMmY3NS00NWUzLThjZWYtOTMyZTNmNDI0OGIwIiwidCI6IjE2N2M0ZGUyLWMzMDItNGU4ZS1hODU0LTEyYjcwZmRhMjJlZSIsImMiOjR9', img: '', tipo: 'Eje' },
  { id: 16, texto: 'Ejecución Financiera Total', url: 'https://app.powerbi.com/view?r=eyJrIjoiNjQ3MTkzNjctMTg1MC00YjkxLThlOTQtYmI4NGJjZmM4NjIxIiwidCI6IjE2N2M0ZGUyLWMzMDItNGU4ZS1hODU0LTEyYjcwZmRhMjJlZSIsImMiOjR9', img: '', tipo: 'Eje' },
  { id: 17, texto: 'Ejecución de Experimentos y Eventos', url: 'https://app.powerbi.com/view?r=eyJrIjoiZDNlNDZiZGItMTJiMS00NTA2LThjOWQtNWYwYzQyY2FiYjA4IiwidCI6IjE2N2M0ZGUyLWMzMDItNGU4ZS1hODU0LTEyYjcwZmRhMjJlZSIsImMiOjR9', img: '', tipo: 'Eje' },
  { id: 18, texto: 'Ejecución de Actividades POA', url: 'https://app.powerbi.com/view?r=eyJrIjoiMmNmNzY4YjktYTJmYS00MWQ0LWJmOGQtOTEzYjE3MTZhM2Y0IiwidCI6IjE2N2M0ZGUyLWMzMDItNGU4ZS1hODU0LTEyYjcwZmRhMjJlZSIsImMiOjR9', img: '', tipo: 'Eje' },
  { id: 19, texto: 'Ejecución por Recursos / Solicitudes', url: 'https://app.powerbi.com/view?r=eyJrIjoiNTE1YTYwZmMtNjBmMi00ZDMyLWJmYzUtNTNkM2U1ZDFkNjhlIiwidCI6IjE2N2M0ZGUyLWMzMDItNGU4ZS1hODU0LTEyYjcwZmRhMjJlZSIsImMiOjR9', img: '', tipo: 'Eje' },
  { id: 20, texto: 'Ejecución Financiera SIAF', url: 'https://app.powerbi.com/view?r=eyJrIjoiN2ZlYTJkZDItZTg2Yi00YTBiLWJkMzMtZDhmYzhmYjhkMzU3IiwidCI6IjE2N2M0ZGUyLWMzMDItNGU4ZS1hODU0LTEyYjcwZmRhMjJlZSIsImMiOjR9', img: '', tipo: 'Eje' },
  { id: 21, texto: 'Ejecución por Especialista', url: 'https://app.powerbi.com/view?r=eyJrIjoiYzdiNjI2ZTctODFjYy00Njk3LTkwMGQtMjMzMTE3Y2E0MTA3IiwidCI6IjE2N2M0ZGUyLWMzMDItNGU4ZS1hODU0LTEyYjcwZmRhMjJlZSIsImMiOjR9', img: '', tipo: 'Eje' }

];
  function jsf_reporte(cod){
      $('#repMenu').hide();
      $.each(urls, function (i, e){
        if(e['id'] == cod){
          $('#tituloReporte').html(e['texto']);
          $('#frame, #repPrincipal, #blk2').show();
          $('#if_reporte').attr('src',e['url']);
          $('#repAnterior').off('click').on('click', function (){
              jsf_reporte(cod-1);
          });
          if(i-1 < 0){
            $('#repAnterior, #blk').hide();
          } else if(urls[i-1]['tipo'] != urls[i]['tipo']){
            $('#repAnterior, #blk').hide();
          } else {
            $('#repAnterior').html("< " + urls[i-1]['texto'])
            $('#repAnterior, #blk').show();
          }

          $('#repSiguiente').off('click').on('click', function (){
            jsf_reporte(cod+1);
          });
          if(i+1 >= urls.length){
            $('#repSiguiente').hide();
          } else if(urls[i+1]['tipo'] != urls[i]['tipo']){
            $('#repSiguiente').hide();
          } else {
            $('#repSiguiente').html(urls[i+1]['texto'] + " >")
            $('#repSiguiente').show();
          }
        }
      });

  }
  $(document).ready(function(){

      $.each(urls, function (i, e){
        if(e['tipo'] == 'Inv'){
          $('#img1').off('click').click(function(){
            $('ul.lista-reportes li', $(this).parent()).eq(0).find('a').click();
          });
          $('#list-Inversion').append('<li><a href="#" onclick="jsf_reporte('+e['id']+');">'+e['texto']+'</a></li>');
        }

        if(e['tipo'] == 'Prg'){
          $('#img2').off('click').click(function(){
            $('ul.lista-reportes li', $(this).parent()).eq(0).find('a').click();
          });
          $('#list-Programacion').append('<li><a href="#" onclick="jsf_reporte('+e['id']+');">'+e['texto']+'</a></li>');
        }

        if(e['tipo'] == 'Eje'){
          $('#img3').off('click').click(function(){
            $('ul.lista-reportes li', $(this).parent()).eq(0).find('a').click();
          });
          $('#list-Ejecucion').append('<li><a href="#" onclick="jsf_reporte('+e['id']+');">'+e['texto']+'</a></li>');
        }
      });

      $('#repPrincipal').click(function(){
        $('#frame').hide(); $('#if_reporte').empty(); $('#repMenu').show(); $('#tituloReporte').html('Reportes BI');
        $('#repAnterior, #blk, #repSiguiente, #blk2, #repPrincipal').hide();
      });
      //$('#leftmenu-trigger').click();
  });
</script>
