<div id="page-content" style="min-height: 934px;">
    <div id="wrap">
        <div class="container">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4><?php echo ucwords($roles->Nombre) ?></h4>
                    <div class="options">
                        <a class="panel-collapse" href="#">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-12 control-label"> </label>
                        <div class="col-sm-12">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php 
    $provincia=Yii::$app->getUrlManager()->createUrl('informacion-general/provincia');
    $distrito=Yii::$app->getUrlManager()->createUrl('informacion-general/distrito');
?>



<script type="text/javascript">
    function provincia(valor) {
        var departamentoID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $provincia ?>',
            type: 'POST',
            async: false,
            data: {departamentoID:departamentoID,_csrf: toke},
            success: function(data){
               $( "#ProvinciaID" ).html( data );
            }
        });
    }
    
    function distrito(valor) {
        var provinciaID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $distrito ?>',
            type: 'POST',
            async: false,
            data: {provinciaID:provinciaID,_csrf: toke},
            success: function(data){
               $( "#DistritoID" ).html( data );
            }
        });
    }


    $('body').on('click', '#mapaSearch', function (e) {
        e.preventDefault();
        var xdep = $('#departamento').val();
        var xprov = $('#ProvinciaID').val();
        var xdist = $('#DistritoID').val();
        e.preventDefault();
        AjaxSendForm('<?= \Yii::$app->request->BaseUrl ?>/panel/geo-mapa','.mapaGeo', {dep:xdep,prov:xprov,dist:xdist}, false);
    });


    var i = 0;
    function AjaxSendForm(url, placeholder,data, append) {
        // var data = $(form).serialize();
        append = (append === undefined ? false : true); 
        $.ajax({
            type: 'GET',
            url: url,
            data: data,
            beforeSend: function() {
                $(placeholder).html("");
                $(placeholder).append('<div class="loading"><i class="fa fa-spinner fa-pulse fa-4x fa-fw"></i></div>');
                i++;
            },
            success: function(data) {
                $(placeholder).html("");
                if (append) {
                    $(placeholder).append(data);
                } else {
                    $(placeholder).html(data);
                }
            },
            error: function(xhr) {
                alert("Error occured.please try again");
                $(placeholder).append(xhr.statusText + xhr.responseText);
            },
            complete: function() {
                i--;
                if (i <= 0) {
                    $(".loading").remove();
                }
            },
            dataType: 'html'
        });
    }

</script>