<style type="text/css">
  #map {
        height: 400px;
        width: 100%;
      }
      #legend {
        font-family: Arial, sans-serif;
        background: #fff;
        padding: 10px;
        margin: 10px;
        border: 3px solid #000;
      }
      #legend h3 {
        margin-top: 0;
      }
      #legend img {
        vertical-align: middle;
      }
</style>

<!--  <pre>
    <?php //print_r($informacion); die(); ?>
</pre>   -->

<?php if(empty($informacion)): ?>
    <pre>No existen datos de geolocalización</pre>
<?php else: ?>
<?php endif ?>
    <div class="google-map" id="map"></div>
    

<script type="text/javascript">
    <?php if(!empty($informacion)): ?>
        function initMap() {
            var map = new google.maps.Map(document.getElementById('map'), {
                center: new google.maps.LatLng(<?php echo $informacion[0]->Latitud ?>, <?php echo $informacion[0]->Longitud ?>),
                zoom: 7
            });
            var icons = {
                // 1: {
                //     name: 'Servicios de Extensión',
                //     icon: 'http://200.48.8.77/pip1/public/img/EA.png'
                // },
                2: {
                    name: 'Ubicación Proyectos',
                    icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'
                },
                // 3: {
                //     name: 'Investigación Adaptativa',
                //     icon: '../public/img/IA.png'
                // },
                // 4: {
                //     name: 'Empresas Semilleristas',
                //     icon: '../public/img/ES.png'
                // },
                // 5: {
                //     name: 'Capacitación por competencias',
                //     icon: '../public/img/CC.png'
                // },
            };
            
            <?php foreach ($informacion as $mapa): ?>
                var marker = new google.maps.Marker({
                    position: { lat: <?php echo $mapa->Latitud ?>,lng: <?php echo $mapa->Longitud ?> },
                    map: map,
                    icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                    title: '<?php echo $mapa->Codigo ?>'
                });
            <?php endforeach ?>
            
            
            var legend = document.getElementById('legend');
                for (var key in icons) {
                    var type = icons[key];
                    var name = type.name;
                    var icon = type.icon;
                    var div = document.createElement('div');
                    div.innerHTML = '<img src="' + icon + '" style="width:32px;height32px;"> ' + name;
                    legend.appendChild(div);
            }

            map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(legend);
        }
    <?php else: ?>
        var map;
          function initMap() {
            map = new google.maps.Map(document.getElementById('map'), {
              center: {lat: -13.4318056, lng: -72.06428055555556},
              zoom: 5
            });
          }
    <?php endif ?>
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAprw2oXNo6jgbWAobmZ-OGyK-jP7fTxgk&amp;callback=initMap" async="" defer=""></script>



<?php if(!empty($estacion)): ?>
    <div id="legend"><h3>Leyenda</h3></div>
    <hr>
    <div id="container-pie"></div>
    <script type="text/javascript">

        $(document).ready(function () {

            // Build the chart
            Highcharts.chart('container-pie', {
                chart: {
                    type: 'pie'
                },
                title: {
                    text: 'Consolidado de programa'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y}</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: [
                    <?php foreach ($programa as $prog): ?>
                        {
                            name: "<?php echo $prog['Nombre'].': Total ('.$prog['Total'].')' ?>",
                            y: <?php echo $prog['Total'] ?>
                        }, 
                    <?php endforeach ?>
                    ]
                }]
            });
        });

    </script>
    <!-- <pre>
        <?php //print_r($estacion); die(); ?>
    </pre> -->
    <hr>
    <div class="container">
        <table class="table table-hover tbl-act">
            <thead >
                <tr class="tr-header">
                    <th>Programa</th>
                    <th>Proyecto</th>
                    <th>Cultivo</th>
                    <th>Monto PNIA (S/)</th>
                    <!-- <th>Razon Social</th> -->
                </tr>
            </thead>
            <tbody id="table_indica_obj">
                <?php foreach ($estacion as $estac): ?>
                    <tr>
                        <td><?php echo $estac['Nombre'] ?></td>
                        <td><?php echo $estac['Codigo'] ?></td>
                        <td><?php echo $estac['Cultivo'] ?></td>
                        <td class="input-number"><?php echo $estac['AporteMonetario'] ?></td>
                        <!-- <td><?php echo $estac['RazonSocial'] ?></td> -->
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>                    
    </div>
<?php else: ?>
    <pre>No existen datos</pre>
<?php endif ?>


