<div id="page-content" style="min-height: 934px;">
    <div id="wrap">
        <div class="container">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4>Mapa de Proyectos</h4>
                    <div class="options">
                        <a class="panel-collapse" href="#">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-12 control-label"> </label>
                        <div class="col-sm-12">
                            <script src="https://code.highcharts.com/highcharts.js"></script>
                            <script src="https://code.highcharts.com/modules/exporting.js"></script>

                            <!--  MAPA -->
                            <form name="form1" method="POST" action="">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3"><h5>Departamento</h5></div>
                                        <div class="col-md-3"><h5>Provincia</h5></div>
                                        <div class="col-md-3"><h5>Distrito</h5></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <select name="Departamento" id="departamento" class="form-control" required="" onchange="provincia($(this).val())">
                                                <option value>Seleccionar</option>
                                                <?php foreach($departamentos as $departamento){ ?>
                                                    <option value="<?= trim($departamento->ID) ?>" ><?= $departamento->Nombre ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <select name="Provincia" id="ProvinciaID" class="form-control" onchange="distrito($(this).val())">
                                                <option value>Seleccionar</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <select name="Distrito" id="DistritoID" class="form-control">
                                                <option value>Seleccionar</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <a href="#" id="mapaSearch" class="btn btn-primary"><i class="fa fa-search"></i> Realizar consulta</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="mapaGeo"></div>
                            <!-- END MAPA -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php 
    $provincia=Yii::$app->getUrlManager()->createUrl('informacion-general/provincia');
    $distrito=Yii::$app->getUrlManager()->createUrl('informacion-general/distrito');
?>



<script type="text/javascript">
    function provincia(valor) {
        var departamentoID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $provincia ?>',
            type: 'POST',
            async: false,
            data: {departamentoID:departamentoID,_csrf: toke},
            success: function(data){
               $( "#ProvinciaID" ).html( data );
            }
        });
    }
    
    function distrito(valor) {
        var provinciaID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $distrito ?>',
            type: 'POST',
            async: false,
            data: {provinciaID:provinciaID,_csrf: toke},
            success: function(data){
               $( "#DistritoID" ).html( data );
            }
        });
    }


    $('body').on('click', '#mapaSearch', function (e) {
        e.preventDefault();
        var xdep = $('#departamento').val();
        var xprov = $('#ProvinciaID').val();
        var xdist = $('#DistritoID').val();
        e.preventDefault();
        AjaxSendForm('<?= \Yii::$app->request->BaseUrl ?>/panel/geo-mapa','.mapaGeo', {dep:xdep,prov:xprov,dist:xdist}, false);
    });


    var i = 0;
    function AjaxSendForm(url, placeholder,data, append) {
        // var data = $(form).serialize();
        append = (append === undefined ? false : true); 
        $.ajax({
            type: 'GET',
            url: url,
            data: data,
            beforeSend: function() {
                $(placeholder).html("");
                $(placeholder).append('<div class="loading"><i class="fa fa-spinner fa-pulse fa-4x fa-fw"></i></div>');
                i++;
            },
            success: function(data) {
                $(placeholder).html("");
                if (append) {
                    $(placeholder).append(data);
                } else {
                    $(placeholder).html(data);
                }
            },
            error: function(xhr) {
                alert("Error occured.please try again");
                $(placeholder).append(xhr.statusText + xhr.responseText);
            },
            complete: function() {
                i--;
                if (i <= 0) {
                    $(".loading").remove();
                }
            },
            dataType: 'html'
        });
    }

</script>