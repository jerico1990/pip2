<div id="page-content" style="min-height: 934px;">
    <div id="wrap">
        <div class="container">
            
            <div class="panel panel-primary col-sm-12" style="position: inherit !important">
                <div class="panel-heading">
                    <h4>Nota:</h4>
                    <div class="options">
                        <a class="panel-collapse" href="#">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body" >
                    <?php if($NivelAprobacionCorrelativo==0){?>
                         Pendiente de enviar POA <span class="f_req">*</span>
                    <?php }elseif($NivelAprobacionCorrelativo==1){?>
                        Pendiente de Aprobación de UAFSI <span class="f_req">*</span>
                    <?php }elseif($NivelAprobacionCorrelativo==2){?>
                        POA Aprobado <span class="f_req">*</span>
                    <?php }elseif($NivelAprobacionCorrelativo==3){?>
                        Pendiente de Aprobación de Jefe estación <span class="f_req">*</span>
                    <?php } ?>
                    <br>
                    <?php if(\Yii::$app->user->identity->SituacionProyecto==3){ ?>
                    <?php $observacionOR=0; ?>
                        <?php $observacionAR=0; ?>
                        <?php foreach($ObservacionesRecursos as $ObservacionRecurso){
                                if($ObservacionRecurso->Estado==1)
                                {
                                    $observacionOR++;
                                }
                                elseif($ObservacionRecurso->Estado==3)
                                {
                                    $observacionAR++;
                                }
                        }?>
                        
                        <?php if($observacionOR>0){ ?>
                            <span style="color: red">Existen observaciones pendientes en Recursos, Total: <?= $observacionOR ?> .</span>
                        <?php } ?>
                        <?php if($observacionAR>0){ ?>
                            <span style="color: green">Tiene <?= $observacionAR ?> aprobaciones de recursos cerradas.</span>
                        <?php } ?>
                        <br>
                        <?php $observacionOT=0; ?>
                        <?php $observacionAT=0; ?>
                        <?php foreach($ObservacionesTareas as $ObservacionTarea){
                                if($ObservacionTarea->Estado==1)
                                {
                                    $observacionOT++;
                                }
                                elseif($ObservacionTarea->Estado==3)
                                {
                                    $observacionAT++;
                                }
                        }?>
                        
                        <?php if($observacionOT>0){ ?>
                            <span style="color: red">Existen observaciones pendientes en tareas, Total: <?= $observacionOT ?> .</span>
                        <?php } ?>
                        <?php if($observacionAT>0){ ?>
                            <span style="color: green">Tiene <?= $observacionAT ?> aprobaciones de tareas cerradas.</span>
                        <?php } ?>
                    <?php } ?>
                        
                </div>
            </div>
            
            <div class="panel panel-primary col-sm-12">
                <div class="panel-heading">
                    <h4>Información general:</h4>
                    <div class="options">
                        <a class="panel-collapse" href="#">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="col-sm-8">
                        <label class="col-sm-12 control-label"><b>Titulo del sub proyecto:</b> </label>
                        <div class="col-sm-12">
                           <?= $informacionGeneral->TituloProyecto ?><br><br>
                        </div>
                        <br><br>
                        <label class="col-sm-12 control-label"><b>Entidad ejecutora:</b> </label>
                        <div class="col-sm-12">
                           <?= $unidadEjecutora->Nombre ?><br><br>
                        </div>
                        <br><br>
                        <label class="col-sm-12 control-label"><b>Propósito:</b> </label>
                        <div class="col-sm-12">
                           <?= $proyecto->Proposito ?><br><br>
                        </div>
                        <br><br>
                        <label class="col-sm-12 control-label"><b>Duración del proyecto:</b> </label>
                        <div class="col-sm-12">
                           <?= $informacionGeneral->Meses ?> Meses<br><br>
                        </div>
                        <br><br>
                        <!--
                        <label class="col-sm-12 control-label"><b>Paso critico:</b> </label>
                        <div class="col-sm-12">
                           <?= $pasoCritico->Correlativo ?> 
                        </div>
                        -->
                    </div>
                    <div class="col-sm-4">
                        <?= $this->render('_mapa',['DepartamentoID'=>$informacionGeneral->DepartamentoID]); ?>
                    </div>
                </div>
            </div>
            <div class="panel panel-primary col-sm-2"></div>
            <div class="panel panel-primary col-sm-4">
                <div class="panel-heading">
                    <h4>Financiero PNIA</h4>
                    <div class="options">
                        <a class="panel-collapse" href="#">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="col-sm-12">
                        <label class="col-sm-6 control-label"><b>Programado</b> </label>
                        
                        <label class="col-sm-6 control-label"><b>Ejecutado</b> </label>
                        <div class="col-sm-6">
                            <?php if($MontoTotalPnia['MontoTotalPnia']!=0){?>
                            <?php $porcentage=($MontoTotalPnia['MontoTotalPniaEjecutada']*100)/$MontoTotalPnia['MontoTotalPnia']; ?>
                            <?php }else{$porcentage=0;}?>
                            <?= number_format($MontoTotalPnia['MontoTotalPnia'], 0, '.', ' ') ?>
                        </div>
                        <div class="col-sm-6">
                            <?= number_format($MontoTotalPnia['MontoTotalPniaEjecutada'], 0, '.', ' ') ?>
                        </div><br><br><br>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <div class="progress pos-rel" data-percent="<?= $porcentage ?>%" style="height: 20px;">
                                <div class="progress-bar" style="width:<?= $porcentage ?>%;"><?= $porcentage ?>%</div>
                            </div>
                            <div class="pull-left">Avance de ejecución financiera</div><div class="pull-right"><?= $porcentage ?>%</div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="panel panel-primary col-sm-4">
                <div class="panel-heading">
                    <h4>Técnico PNIA</h4>
                    <div class="options">
                        <a class="panel-collapse" href="#">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="col-sm-12">
                        <label class="col-sm-6 control-label"><b></b> </label>
                        
                        <label class="col-sm-6 control-label"><b></b> </label>
                        <div class="col-sm-6">
                            
                        </div>
                        <div class="col-sm-12">
                            Avance de ejecución de tareas
                        </div><br><br><br>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <div class="progress pos-rel" data-percent="<?= ($proyecto->Avance)?$proyecto->Avance:0; ?>%" style="height: 20px;">
                                <div class="progress-bar" style="width:<?= ($proyecto->Avance)?$proyecto->Avance:0; ?>%;"><?= ($proyecto->Avance)?$proyecto->Avance:0; ?>%</div>
                            </div>
                            <div class="pull-left">Avance de ejecución técnica</div><div class="pull-right"><?= $proyecto->Avance ?>%</div>
                        </div>
                    </div>
                </div>
            </div>
            <!--
            <div class="panel panel-primary col-sm-4">
                <div class="panel-heading">
                    <h4>Alianza Estratégica (S/)</h4>
                    <div class="options">
                        <a class="panel-collapse" href="#">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="col-sm-12">
                        <label class="col-sm-6 control-label"><b>Programado</b> </label>
                        <label class="col-sm-6 control-label"><b>Ejecutado</b> </label>
                        <div class="col-sm-6">
                            <?php $totalColaborador=0;?>
                            <?php foreach($colaboradores as $colaborador) { ?>
                                <?php if($colaborador->TipoInstitucionID!=3){ ?>
                                    <?php $totalColaborador=$totalColaborador+$colaborador->AporteMonetario+$colaborador->AporteNoMonetario ?>
                                <?php }?>
                            <?php } ?>
                            <?= $totalColaborador ?>
                        </div>
                        <div class="col-sm-6"></div><br><br><br>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <div class="progress pos-rel" data-percent="0%" style="height: 20px;">
                                <div class="progress-bar" style="width:0%;">0%</div>
                            </div>
                            <div class="pull-left">Avance de ejecución</div><div class="pull-right">0%</div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="panel panel-primary col-sm-4">
                <div class="panel-heading">
                    <h4>Total Ejecutado (S/)</h4>
                    <div class="options">
                        <a class="panel-collapse" href="#">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="col-sm-12">
                        <label class="col-sm-6 control-label"><b>Programado</b> </label>
                        <label class="col-sm-6 control-label"><b>Ejecutado</b> </label>
                        <div class="col-sm-6">
                            <?php $totalColaborador=0;?>
                            <?php foreach($colaboradores as $colaborador) { ?>
                                <?php $totalColaborador=$totalColaborador+$colaborador->AporteMonetario+$colaborador->AporteNoMonetario ?>
                            <?php } ?>
                            <?= $totalColaborador ?>
                        </div>
                        <div class="col-sm-6"></div><br><br><br>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <div class="progress pos-rel" data-percent="0%" style="height: 20px;">
                                <div class="progress-bar" style="width:0%;">0%</div>
                            </div>
                            <div class="pull-left">Avance de ejecución</div><div class="pull-right">0%</div>
                        </div>
                    </div>
                </div>
            </div>
            -->
            <div class="clearfix"></div>
            <div class="panel panel-primary col-sm-6">
                <div class="panel-heading">
                    <h4>Eventos</h4>
                    <div class="options">
                        <a class="panel-collapse" href="#">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="col-sm-12">
                        <label class="col-sm-6 control-label"><b>Programado</b> </label>
                        <label class="col-sm-6 control-label"><b>Ejecutado</b> </label>
                        <div class="col-sm-6">
                            <?= $even_exper->Evento ?>
                        </div>
                        <div class="col-sm-6"><?= $evento ?></div><br><br><br>
                    </div>
                    <?php $evento_porc=0; ?>
                    <?php if($even_exper->Evento!=0){ ?>
                    <?php $evento_porc=(($evento*100)/$even_exper->Evento) ?>
                    <?php } ?>
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <div class="progress pos-rel" data-percent="<?= $evento_porc ?>%" style="height: 20px;">
                                <div class="progress-bar" style="width:<?= round($evento_porc,0) ?>%;"><?= round($evento_porc,0) ?>%</div>
                            </div>
                            <div class="pull-left">Avance de ejecución</div><div class="pull-right"><?= round($evento_porc,0) ?>%</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-primary col-sm-6">
                <div class="panel-heading">
                    <h4>Experimentos</h4>
                    <div class="options">
                        <a class="panel-collapse" href="#">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="col-sm-12">
                        <label class="col-sm-6 control-label"><b>Programado</b> </label>
                        <label class="col-sm-6 control-label"><b>Ejecutado</b> </label>
                        <div class="col-sm-6">
                            <?= $even_exper->Experimento ?>
                        </div>
                        <div class="col-sm-6"><?= $experimento ?></div><br><br><br>
                    </div>
                    <?php $experimento_porc=0; ?>
                    <?php if($even_exper->Experimento!=0){ ?>
                    <?php $experimento_porc=(($experimento*100)/$even_exper->Experimento) ?>
                    <?php } ?>
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <div class="progress pos-rel" data-percent="<?= $experimento_porc ?>%" style="height: 20px;">
                                <div class="progress-bar" style="width:<?= round($experimento_porc,0) ?>%;"><?= round($experimento_porc,0) ?>%</div>
                            </div>
                            <div class="pull-left">Avance de ejecución</div><div class="pull-right"><?= round($experimento_porc,0) ?>%</div>
                        </div>
                    </div>
                </div>
            </div>
           
            <div class="panel-group " id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-primary col-sm-12">
                    <div class="panel-heading" role="tab" id="headingOne">
                      <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            1.- Avance financiero - Ejecución por Paso Critico
                        </a>
                      </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <div class="col-sm-12" id="table-paso-critico"></div>
                        </div>
                    </div>
                </div>
                <!--
                <div class="panel panel-primary col-sm-12">
                    <div class="panel-heading col-sm-12" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                2.- Avance Físico Financiero según Componente y Actividades
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                            <div class="col-sm-12" id="table-componentes-actividades"></div>
                        </div>
                    </div>
                </div>-->
            </div>
          
        </div>
    </div>
</div>

<script>
    var _proyecto = new Object();
    
    $(document).ready(function () {
        $('#table-paso-critico').html('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>');
        getProyecto().done(function(json) {
            _proyecto = json;
            _drawTable();
        });
    });
    
    function getProyecto() {
        var url = "<?= \Yii::$app->request->BaseUrl ?>/recursos-poa-ejecutado/recursos-finanzas-json?codigo=<?= $informacionGeneral->Codigo ?>";
        return $.getJSON( url );
    }
    
    function getMarcoLogico() {
        var url = "<?= \Yii::$app->request->BaseUrl ?>/marco-logico/objetivos-actividades-json?codigo=<?= $informacionGeneral->Codigo ?>";
        return $.getJSON( url );
    }
    
    function _drawTable() {
        var html = '';
            html += '<div class="table-responsive">';
            html += '<table class="table table-bordered table-condensed table-act">';
                html += '<tbody>';
                    html += '<tr class="tr-header">';
                        html += '<td>';
                            html += 'PC';
                        html += '</td>';
                        html += '<td>';
                            html += 'Inicio';
                        html += '</td>';
                        html += '<td>';
                            html += 'Término';
                        html += '</td>';
                        html += '<td>';
                            html += 'Programado';
                        html += '</td>';
                        html += '<td>';
                            html += 'Programado Actual';
                        html += '</td>';
                        html += '<td>';
                            html += 'Ejecutado';
                        html += '</td>';
                        html += '<td>';
                            html += 'Avance %';
                        html += '</td>';
                    html += '</tr>';
                    var PasoCriticos=_proyecto.PasoCritico;
                    console.log(PasoCriticos);
                    for (var i_paso = 0; i_paso < _proyecto.PasoCritico.length; i_paso++) {
                        
                        if (!PasoCriticos[i_paso].MontoTotalEjecutado) {
                            PasoCriticos[i_paso].MontoTotalEjecutado=0;
                        }
                        if (!PasoCriticos[i_paso].MontoTotal) {
                            PasoCriticos[i_paso].MontoTotal=0;
                        }
                        
                    html += '<tr>';
                        html += '<td>';
                            html += PasoCriticos[i_paso].Correlativo;
                        html += '</td>';
                        html += '<td>';
                            html += PasoCriticos[i_paso].MesInicio;
                        html += '</td>';
                        html += '<td>';
                            html += PasoCriticos[i_paso].MesFin;
                        html += '</td>';
                        html += '<td class="">';
                            html += 'S/ '+formato_numero(PasoCriticos[i_paso].MontoTotal,2);
                        html += '</td>';
                        html += '<td class="">';
                            html += 'S/ '+formato_numero(PasoCriticos[i_paso].MontoTotal2,2);
                        html += '</td>';
                        html += '<td>';
                            html += 'S/ '+formato_numero(PasoCriticos[i_paso].MontoTotalEjecutado,2);
                        html += '</td>';
                        html += '<td align="center">';
                            if (PasoCriticos[i_paso].MontoTotal) {
                                var por=((PasoCriticos[i_paso].MontoTotalEjecutado*100)/PasoCriticos[i_paso].MontoTotal).toFixed(2);
                                html +=por+'%';
                                html +='<div class="progress progress-mini">';
                                    html +='<div class="progress-bar" role="progressbar" aria-valuenow="'+por+'" aria-valuemin="0" aria-valuemax="100" style="width: '+por+'%;">';
                                        html +='<span class="sr-only">'+por+'% Complete</span>';
                                    html +='</div>';
                                html +='</div>';
                                
                            }
                            else{
                                var por=0;
                                html +=por+'%';
                                html +='<div class="progress progress-mini">';
                                    html +='<div class="progress-bar" role="progressbar" aria-valuenow="'+por+'" aria-valuemin="0" aria-valuemax="100" style="width: '+por+'%;">';
                                        html +='<span class="sr-only">'+por+'% Complete</span>';
                                    html +='</div>';
                                html +='</div>';
                                
                            }
                            
                        html += '</td>';
                    html += '</tr>';
                    }
                    
                html += '</tbody>';
            html += '</table>';
            html += '</div>';
        
        $('#table-paso-critico').html(html);
        $('.table-responsive').css('max-height', $(window).height() * 0.60);
        $('.input-number').inputmask("decimal", {
            radixPoint: ".",
            groupSeparator: ",",
            groupSize: 3,
            digits: 2,
            integerDigits: 4,
            autoGroup: true,
            allowPlus: false,
            allowMinus: true,
            placeholder: ''
        }).click(function () {
            $(this).select();
        });
        
        $('.input-number-porcentaje,.input-number-recurso-porcentaje').inputmask("decimal",{integerDigits: 4,digits: 2,});
        
    }
</script>

<div class="modal fade" id="modal-ctrl-contrasena" data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>-->
                <h4 class="modal-title">Cambiar contraseña</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>

<?php if(!$usuario->CodigoVerificacion){?>
<script>
    //var CodigoProyecto = $(this).attr('data-codigo-proyecto');
    //$('#modal-ctrl-contrasena .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/usuario/cambiar-contrasena?ID=<?= $usuario->ID ?>');
   // $('#modal-ctrl-contrasena').modal('show');
</script>
<?php }?>
<?php if (Yii::$app->session->hasFlash('CambioContrasena')): ?>
<script>
    //bootbox.alert('Se ha cambiado la contraseña');
</script>
<?php endif; ?>
<script>
    
    $('body').on('click', '#btn-guardar-contrasena', function (e) {
        e.preventDefault();
        
        if($('#contrasena').val()!=$('#re-contrasena').val())
        {
            bootbox.alert('¡Las contraseñas no son idénticas!');
            return false;
        }
        else
        {
            var $btn = $(this);
            $btn.attr('disabled', 'disabled');
            var isValid = formParsleyUsuario.validate();
            if (isValid) {
                $('#frmContrasena').submit();
            }else{
                $btn.removeAttr('disabled');
            }
            $btn.removeAttr('disabled');
        }
    });
</script>