<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Persona */

?>
<div id="page-content" style="min-height: 754px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Crear Persona</h1>
        </div>
        <div class="container">
            <div class="persona-create">
            
                <h1><?= Html::encode($this->title) ?></h1>
            
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>
            
            </div>
        </div>
    </div>
</div>