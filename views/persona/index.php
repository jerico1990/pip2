<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PersonaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div id="page-content" style="min-height: 754px;">
    <div id="wrap">
        <div id="page-heading">
            <h1>Personas</h1>
        </div>
        <div class="container">
            <div class="persona-index">
            
                <h1><?= Html::encode($this->title) ?></h1>
                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            
                <p>
                    <?= Html::a('Crear Persona', ['create'], ['class' => 'btn btn-success']) ?>
                </p>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        //'ID',
                        'ApellidoPaterno',
                        'ApellidoMaterno',
                        'Nombre',
                        'NroDocumento',
                        // 'Telefono',
                        // 'Email:email',
                        ['class' => 'yii\grid\ActionColumn',
                         'template' => '{view} {update} {delete}',
                         'buttons' => [
                                //view button
                                'view' => function ($url, $model) {
                                    return Html::a('<span class="fa fa-eye"></span>', $url, [
                                                'title' => Yii::t('app', 'Ver'),                                 
                                    ]);
                                },
                                
                                'update' => function ($url, $model) {
                                    return Html::a('<span class="fa fa-pencil"></span>', $url, [
                                                'title' => Yii::t('app', 'Actualizar'),                               
                                    ]);
                                },
                                
                                'delete' => function($url, $model){
                                    return Html::a('<span class="fa fa-trash-o"></span>', ['delete', 'id' => $model->ID], [
                                        'class' => '',
                                        'data' => [
                                            'confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.',
                                            'method' => 'post',
                                        ],
                                    ]);
                                }
                            ],
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>