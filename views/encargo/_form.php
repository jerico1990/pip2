<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => '../encargo/crear?RequerimientoID='.$Requerimiento->ID.'&CodigoProyecto='.$CodigoProyecto.'&DetallesRequerimientosIDs='.$DetallesRequerimientosIDs,
        'options'           => [
            'enctype'       =>'multipart/form-data',
            'id'            => 'frmEncargos',
            
        ]
    ]
); ?>
    <input type="hidden" name="Encargo[CodigoProyecto]" value="<?= $CodigoProyecto ?>">
    <div class="modal-body">
        <div class="form-horizontal">
            <input type="hidden" name="Encargo[RequerimientoID]" value="<?= $Requerimiento->ID ?>">
            
            <div class="form-group">
                <label class="col-sm-2 control-label">Actividad:<span class="f_req">*</span></label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="Encargo[DescripcionActividad]" required></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Fecha inicio:<span class="f_req">*</span></label>
                <div class="col-sm-4">
                    <input type="date" class="form-control" name="Encargo[FechaInicio]" required>
                </div>
                <label class="col-sm-2 control-label">Fecha de término:<span class="f_req">*</span></label>
                <div class="col-sm-4">
                    <input type="date" class="form-control" name="Encargo[FechaFin]" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Monto referencial:<span class="f_req">*</span></label>
                <div class="col-sm-2">
                    <input type="text" class="form-control " id="total-encargo" value="<?= $PrecioTotal ?>" disabled>
                </div>
            </div>
            <div class="form-group " id="detalle">
                <label class="col-sm-2 control-label">Bienes:<span class="f_req">*</span></label>
                <div class="col-sm-4">
                    <input type="text" class="form-control contadorx input-number" name="Encargo[Bienes]" required>
                </div>
                <label class="col-sm-2 control-label">Servicio:<span class="f_req">*</span></label>
                <div class="col-sm-4">
                    <input type="text" class="form-control contadorx input-number" name="Encargo[Servicios]" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Monto Total:<span class="f_req">*</span></label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" id="total"  disabled>
                </div>
            </div>
            <hr>
            <div class="form-group">
                <label class="col-sm-2 control-label">DNI:<span class="f_req">*</span></label>
                <div class="col-sm-4">
                    <select name="Viatico[PersonaID]" class="form-control" onchange="Persona(this)">
                        <option value>Seleccionar</option>
                        <?php foreach($integrantes as $integrante){?>
                        <option value="<?= $integrante->ID ?>"><?= $integrante->NroDocumento.'-'.$integrante->Nombre.' '.$integrante->ApellidoPaterno.' '.$integrante->ApellidoMaterno ?></option>
                        <?php }?>
                    </select>
                    <!--<input type="text" class="form-control" onfocusout="DNI($(this).val());" onKeyPress="return soloNumeros(event);" maxlength="8" id="requerimiento-dni" name="Encargo[DNIEncargo]" required>-->
                    <input type="hidden" class="form-control dni" maxlength="8" id="requerimiento-dni" name="Encargo[DNIEncargo]">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Nombres:<span class="f_req">*</span></label>
                <div class="col-sm-4">
                    <input type="hidden" class="form-control nombre" name="Encargo[NombresEncargo]" id="encargo-nombresx">
                    <input type="text" class="form-control nombre" id="encargo-nombres" disabled>
                </div>
                <label class="col-sm-2 control-label">Apellidos:<span class="f_req">*</span></label>
                <div class="col-sm-4">
                    <input type="hidden" class="form-control apellido" name="Encargo[ApellidosEncargo]" id="encargo-apellidosx">
                    <input type="text" class="form-control apellido" id="encargo-apellidos" disabled>
                </div>
            </div>
            
            
            <div class="form-group">
                <label class="col-sm-2 control-label">Cargo o Función:<span class="f_req">*</span></label>
                <div class="col-sm-4">
                    <input type="text" class="form-control cargo"  disabled>
                    <input type="hidden" class="cargo" id="requerimiento-encargo-cargoencargo" name="Encargo[CargoEncargo]" required>
                </div>
                <label class="col-sm-2 control-label">Contrato:<span class="f_req">*</span></label>
                <div class="col-sm-4">
                    <select class="form-control contrato" name="Encargo[ContratoEncargo]" id="requerimiento-encargo-contratoencargo" disabled>
                        <option value>Seleccionar</option>
                        <?php foreach($tipoContratacion as $contrat){ ?>
                            <option value="<?= $contrat->ID ?>" ><?= $contrat->Nombre ?></option>
                        <?php } ?>
                    </select>
                    <input type="hidden" class="contrato" name="Encargo[ContratoEncargo]">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">N° Resolucion directorial:<span class="f_req">*</span></label>
                <div class="col-sm-4">
                    <input type="text" class="form-control"  id="requerimiento-nro-resolucion" name="Encargo[NResolucionDirectorial]" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Adjuntar resolución directorial:<span class="f_req">*</span></label>
                <div class="col-sm-10">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="input-group">
                            <div class="form-control uneditable-input" data-trigger="fileinput">
                                <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                <span class="fileinput-filename">
                                Seleccione archivo...
                                </span>
                            </div>
                            <span class="input-group-addon btn btn-default btn-file">
                                <span class="fileinput-new">Seleccionar</span>
                                <span class="fileinput-exists">Cambiar</span>
                                <input class="form-control" onchange="return Imagen(this);" type="file" name="Encargo[archivo]" required>
                            </span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Eliminar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>
<?php
    $actividades=Yii::$app->getUrlManager()->createUrl('requerimiento/actividades');
    $recursos=Yii::$app->getUrlManager()->createUrl('requerimiento/recursos');
?>
<div class="modal fade" id="modal-ctrl-poa" tabindex="-1" role="dialog" >
        <div class="modal-dialog modal-lg" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">POA</h4>
                </div>
                <div class="modal-body-main">
                </div>
            </div>
        </div>
    </div>
<style type="text/css">
    .none{
        display: none;
    }
    .block{
        display: block;
    }

    .ui-draggable, .ui-droppable {
      background-position: top;
    }
</style>



<script>
    var $form = $('#frmEncargos');
    var formParsleyEncargosEncargos = $form.parsley(defaultParsleyForm());

    validarNumeros();

    function Actividad(valor) {
        var ComponenteID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $actividades ?>',
            type: 'POST',
            async: false,
            data: {ComponenteID:ComponenteID,_csrf: toke},
            success: function(data){
               $( "#requerimiento-actividadid" ).html( data );
            }
        });
    }
    
    function Recurso(valor) {
        var ActividadID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $recursos ?>',
            type: 'POST',
            async: false,
            data: {ActividadID:ActividadID,_csrf: toke},
            success: function(data){
               $( "#requerimiento-areasubcategoriaid" ).html( data );
            }
        });
    }

    
    function DNI(valor) {
        $('#encargo-nombres').val('');
        $('#encargo-apellidos').val('');

        $.get('<?= \Yii::$app->request->BaseUrl ?>/viatico/servicio-reniec/', { valor: valor}, function (data) {
            var jx = JSON.parse(data);
            // console.log(jx);
            if (jx.message!='Ok') {
                alert("DNI no se encuentra");
                console.log(message);
            }
            else
            {
                
                    var name = jx.data.nombres;
                    var paterno = jx.data.apellidoPaterno;
                    var materno = jx.data.apellidoMaterno;
                    $('#encargo-nombres').val(name.trim());
                    $('#encargo-apellidos').val(paterno.trim()+' '+materno.trim());
                    $('#encargo-nombresx').val(name.trim());
                    $('#encargo-apellidosx').val(paterno.trim()+' '+materno.trim());
            }
            
        });
    }
    
    function Persona(elemento) {
        $('.nombre').val('');
        $('.apellido').val('');
        $('.cargo').val('');
        $('.contrato').val('');
        $('.dni').val('');
        $.get('<?= \Yii::$app->request->BaseUrl ?>/recursos-humanos/integrantes/', { CodigoProyecto: '<?= $CodigoProyecto ?>',Tipo:1,ID:$(elemento).val()}, function (data) {
            var jx = JSON.parse(data);
            // console.log(jx);
            if (jx.Success==true) {
                $('.nombre').val(jx.Nombres);
                $('.apellido').val(jx.Apellidos);
                $('.cargo').val(jx.Cargo);
                $('.contrato').val(jx.TipoContratacion);
                $('.dni').val(jx.NroDocumento);
            }
        });
    }
    
    
    $('#detalle').on('blur','.contadorx',function(){
        
        contar();
    });
    
    
    function contar(){
       
        var total_total = 0.00;
        $('.contadorx').each(function(x,y){
           
            TotSuma = $(this).val();
            total_total = parseFloat(getNum(TotSuma)) + parseFloat(total_total) ;
            //Suma(total_total,parseFloat($('#totales').val()),this);
            Suma(total_total,parseFloat($('#total-encargo').val()),this);
        });
        // console.log($('#totales').val());
        
        if (total_total>parseFloat($('#total-encargo').val())) {
           
            return false;
        }
         console.log(total_total);
        $('#total').val( "S/."+ number_format(total_total,2) );
    }
    
    
    function Suma(x,y,elemento) {
        if (elemento && x>y) {
            $(elemento).val('0.00');
            contar();
            return false;
        }
        
        
        return true;
    }


    function getNum(val) {
       if (val == '0.00' || val == '') {
            return 0;
       }
       var val2 = val.replace(',','');
       return val2;
    }
    
    
    function number_format(amount, decimals) {
        amount += '';
        amount = parseFloat(amount.replace(/[^0-9\.]/g, ''));
        decimals = decimals || 0;
        if (isNaN(amount) || amount === 0) 
            return parseFloat(0).toFixed(decimals);
        amount = '' + amount.toFixed(decimals);
        var amount_parts = amount.split('.'),
            regexp = /(\d+)(\d{3})/;
        while (regexp.test(amount_parts[0]))
            amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');
        return amount_parts.join('.');
    }
    
</script>

