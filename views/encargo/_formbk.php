<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'requerimiento-encargo/crear?CodigoProyecto='.$CodigoProyecto,
        'options'           => [
            'enctype'       =>'multipart/form-data',
            'id'            => 'frmRequerimientoEncargos',
            
        ]
    ]
); ?>
    <input type="hidden" name="RequerimientoEncargo[CodigoProyecto]" value="<?= $CodigoProyecto ?>">
    <div class="modal-body">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label">POA:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <a class="btn btn-primary" id="agregar-poa">Agregar POA</a>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Objetivo:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="requerimiento-componenteid" disabled>
                    <input type="hidden" class="form-control" name="RequerimientoEncargo[ComponenteID]" id="requerimiento-componenteid-c" >
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Actividad:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="requerimiento-actividadid" disabled>
                    <input type="hidden" class="form-control" name="RequerimientoEncargo[ActividadID]" id="requerimiento-actividadid-c" >
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Recurso:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control"  id="requerimiento-areasubcategoriaid" disabled>
                    <input type="hidden" class="form-control" name="RequerimientoEncargo[AreSubCategoriaID]" id="requerimiento-areasubcategoriaid-c" >
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Código POA:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input type="text" class="form-control"  id="requerimiento-codigo" disabled>
                    <input type="hidden" class="form-control" name="RequerimientoEncargo[CodigoPoa]" id="requerimiento-codigo-c" >
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Actividad:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="RequerimientoEncargo[DescripcionActividad]" required></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Fecha Inicio:<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <input type="date" class="form-control" name="RequerimientoEncargo[FechaInicio]" required>
                </div>
                <label class="col-sm-3 control-label">Fecha de Término:<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <input type="date" class="form-control" name="RequerimientoEncargo[FechaFin]" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Bienes:<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="RequerimientoEncargo[Bienes]" required>
                </div>
                <label class="col-sm-3 control-label">Servicio:<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="RequerimientoEncargo[Servicios]" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Nombres:<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="RequerimientoEncargo[NombresEncargo]" required>
                </div>
                <label class="col-sm-3 control-label">Apellidos:<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" name="RequerimientoEncargo[ApellidosEncargo]" required>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">DNI:<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" maxlength="8" id="requerimiento-dni" name="RequerimientoEncargo[DNIEncargo]" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Cargo o Función:<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" id="requerimiento-encargo-cargoencargo" name="RequerimientoEncargo[CargoEncargo]" required>
                </div>
                <label class="col-sm-3 control-label">Contrato:<span class="f_req">*</span></label>
                <div class="col-sm-3">
                    <select class="form-control" name="RequerimientoEncargo[ContratoEncargo]" id="requerimiento-encargo-contratoencargo">
                        <option value></option>
                        <option value=1>Consultor</option>
                        <option value=2>Servicio de Tercero</option>
                        <option value=3>Otros</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Resolución Directorial:<span class="f_req">*</span></label>
                <div class="col-sm-9">
                    <input class="form-control" type="file" name="RequerimientoEncargo[archivo]" id="requerimiento-archivo">
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;Guardar</button>
    </div>
<?php ActiveForm::end(); ?>
<?php
    $actividades=Yii::$app->getUrlManager()->createUrl('requerimiento/actividades');
    $recursos=Yii::$app->getUrlManager()->createUrl('requerimiento/recursos');
?>

<style type="text/css">
    .none{
        display: none;
    }
    .block{
        display: block;
    }

    .ui-draggable, .ui-droppable {
      background-position: top;
    }
</style>



<script>
    //var $form = $('#frmRequerimientoEncargos');
    //var formParsleyRequerimientoEncargosEncargos = $form.parsley(defaultParsleyForm());

    
    function Actividad(valor) {
        var ComponenteID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $actividades ?>',
            type: 'POST',
            async: false,
            data: {ComponenteID:ComponenteID,_csrf: toke},
            success: function(data){
               $( "#requerimiento-actividadid" ).html( data );
            }
        });
    }
    
    function Recurso(valor) {
        var ActividadID=valor;
        var toke = '<?=Yii::$app->request->getCsrfToken()?>';
        $.ajax({
            url: '<?= $recursos ?>',
            type: 'POST',
            async: false,
            data: {ActividadID:ActividadID,_csrf: toke},
            success: function(data){
               $( "#requerimiento-areasubcategoriaid" ).html( data );
            }
        });
    }

    
    

</script>

