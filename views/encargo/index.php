
<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay_progress.min.js"></script>
<script src=" //cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>


<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<div id="page-content" style="min-height: 1342px;">
    
    <div id="wrap">
        <div id="page-heading">
            <h1>Requerimientos de Encargos</h1>
        </div>
        
           
        <div class="container">
            <!--
             <div class="form-group">
                <button class="btn btn-crear-requerimiento-encargo" data-codigo-proyecto="<?= $CodigoProyecto ?>">Generar requerimiento</button>
            </div>
        
            -->
            <div class="table-responsive">
                <table id="encargos" class="display table table-bordered table-striped table-condensed table-hover" cellspacing="0" >
                    <thead>
                        <tr>
                            <th>Encargo</th>
                            <th>Nombres y Apellidos</th>
                            <th>Cargo</th>
                            <th>Bienes</th>
                            <th>Servicios</th>
                            <th>Total</th>
                            <th>Resolución Directorial</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <a class="btn" href="<?= Yii::$app->getUrlManager()->createUrl('requerimiento-encargo/excel?CodigoProyecto='.$CodigoProyecto.'') ?>">Descargar Excel</a>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-ctrl-requerimiento-encargo" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Requerimiento de Encargo</h4>
            </div>
            <div class="modal-body-main">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-ctrl-poa" tabindex="-1" role="dialog" >
        <div class="modal-dialog modal-lg" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">POA</h4>
                </div>
                <div class="modal-body-main">
                </div>
            </div>
        </div>
    </div>
<?php 
$requerimiento = Yii::$app->getUrlManager()->createUrl('requerimiento/actualizar-requerimiento-listado');
?>
    
<script>

    function js_buscar()
    {
       
    }
    var tblresultjs;
        var configDTjs={
            "order": [[ 2, "desc" ]],
                "language": {
                    "sProcessing":    "Procesando...",
                    "sLengthMenu":    "Mostrar _MENU_ registros",
                    "sZeroRecords":   "No se encontraron resultados",
                    "sEmptyTable":    "Ningún dato disponible en esta lista",
                    "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":   "",
                    "sSearch":        "Buscar:",
                    "sUrl":           "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":    "Último",
                        "sNext":    "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
        };
    $(document).ready(function() {
        var CodigoProyecto="<?= $CodigoProyecto ?>";
        $.ajax({
            url:'requerimiento-encargo/lista?CodigoProyecto='+CodigoProyecto,
            async:false,
            data:{},
            beforeSend:function()
            {
                $('.panel-default').LoadingOverlay("show");
            },
            success:function(result)
            {
                    //$tblresultjs.destroy();//
                    $("#encargos tbody").html(result);
                    $('#encargos').DataTable(configDTjs);
                    //tblresultjs=$('#example').DataTable(configDTjs);
                    $('.panel-default').LoadingOverlay("hide",true);
            },
            error:function(){
                $('.panel-default').LoadingOverlay("hide",true);
                alert('Error al realizar el proceso de busqueda.');
            }
        });
    } );
    
    $('body').on('click', '.btn-crear-requerimiento-encargo', function (e) {
        e.preventDefault();
        var CodigoProyecto = $(this).attr('data-codigo-proyecto');
        $('#modal-ctrl-requerimiento-encargo .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/requerimiento-encargo/crear?CodigoProyecto='+CodigoProyecto);
        $('#modal-ctrl-requerimiento-encargo').modal('show');
    });
    $('body').on('click', '.btn-edit-requerimiento-encargo', function (e) {
        e.preventDefault();
        var ID = $(this).attr('data-id');
        $('#modal-ctrl-requerimiento-encargo .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/requerimiento-encargo/actualizar?ID='+ID);
        $('#modal-ctrl-requerimiento-encargo').modal('show');
    });
    $('body').on('click', '#agregar-poa', function (e) {
        e.preventDefault();
        var CodigoProyecto="<?= $CodigoProyecto ?>";
        $('#modal-ctrl-poa .modal-body-main').load('<?= \Yii::$app->request->BaseUrl ?>/requerimiento/poa?CodigoProyecto='+CodigoProyecto);
        $('#modal-ctrl-poa').modal('show');
    });
    
    $('body').on('click', '.seleccionar_poa', function (e) {
        e.preventDefault();
        
        
        $('.fila').each(function(){
            var id=$(this).attr('data-id');
            var componenteid=$(this).attr('data-componente');
            var actividadid=$(this).attr('data-actividad');
            var toke = '<?=Yii::$app->request->getCsrfToken()?>';
            $.get('<?= \Yii::$app->request->BaseUrl ?>/requerimiento/get-dato-poa', { id: id ,componenteid:componenteid,actividadid:actividadid,_csrf: toke }, function (data) {
                var jx = JSON.parse(data);
                if (jx.Success == true) {
                    console.log(jx);
                    $('#requerimiento-componenteid').val(jx.componente)
                    $('#requerimiento-actividadid').val(jx.actividad)
                    $('#requerimiento-areasubcategoriaid').val(jx.recurso)
                    $('#requerimiento-codigo').val(jx.codigo)
                    
                    $('#requerimiento-componenteid-c').val(jx.componenteid)
                    $('#requerimiento-actividadid-c').val(jx.actividadid)
                    $('#requerimiento-areasubcategoriaid-c').val(jx.aresubcategoriaid)
                    $('#requerimiento-codigo-c').val(jx.codigo)
                    
                    //$('#requerimiento-componenteid-c').val(data.componente)
                    
                }
            });
            $('#modal-ctrl-poa').modal('hide');
        });
        
    });

</script>