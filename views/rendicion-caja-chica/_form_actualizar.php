<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(
    [
        'action'            => 'rendicion-caja-chica/actualizar?ID='.$ID,
        'options'           => [
            'enctype'       =>'multipart/form-data',
            'id'            => 'frmRendicionCajaChica',
        ]
    ]
); ?>
    <input type="hidden" name="RendicionCajaChica[CodigoProyecto]" value="<?= $rendicionCaja->CodigoProyecto ?>">
    <div class="modal-body">
        <div class="form-horizontal">
            
          
            <div class="form-group">
                <label class="col-sm-2 control-label">Codigo:<span class="f_req">*</span></label>
                <div class="col-sm-4">
                  <!--  <input type="text" class="form-control" name="CajaChica[Servicios]" required>-->
                    <label class="col-sm-3 control-label"><?= $rendicionCaja->CodigoProyecto ?></label>
                </div>
                <div class="col-sm-2 "></div>
                <div class="col-sm-4 " aling="rigth">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td>Bienes</td>
                                <td>S/. <?= $cajaActual->Bienes ?></td>
                            </tr>
                            <tr>
                                <td>Servicios</td>
                                <td>S/. <?= $cajaActual->Servicios ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Solicitante:<span class="f_req">*</span></label>
                <div class="col-sm-4">
                    <input type="text" class="form-control"  value="<?= $cajaChica->Responsable ?>" disabled>
                    <input type="hidden" class="form-control" name="RendicionCajaChica[SolicitanteEncargo]" value="<?= $cajaChica->Responsable ?>" >
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Saldo bienes:<span class="f_req">*</span></label>
                <div class="col-sm-4">
                    <input class="form-control" id="bienes" value="<?= $cajaActual->SaldoActualBienes ?>" disabled>
                    <input type="hidden" name="RendicionCajaChica[SaldoActualBienes]" id="rendicion-anterior-bienes" value="<?= $cajaChica->SaldoActualBienes ?>">
                </div>
                <label class="col-sm-2 control-label">Saldo servicios:<span class="f_req">*</span></label>
                <div class="col-sm-4">
                    <input class="form-control" id="servicios" value="<?= $cajaActual->SaldoActualServicios ?>" disabled>
                    <input type="hidden" name="RendicionCajaChica[SaldoActualServicios]" id="rendicion-anterior-servicios" value="<?= $cajaChica->SaldoActualServicios ?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3">
                    <a class="btn btn-primary" id="agregar-rendicion">Agregar detalle</a>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12" style="max-width: 1200px !important;overflow-x: auto ">
                    <table class="table borderless table-hover" id="detalle">
                        <thead>
                            <tr>
                                <th width="250">Tipo</th>
                                <th width="250">Fecha</th>
                                <th width="250">Tipo de Documento</th>
                                <th width="250">Clase</th>
                                <th width="250">N° Documento</th>
                                <th width="150">RUC</th>
                                <th width="100">Proveedor</th>
                                <th width="250">Detalle de Gasto</th>
                                <th width="50">Importe</th>
                                <th width="22">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody id="detalle_rendicion">
                            <?php $a=1;?>
                            <?php $total=0; ?>
                            <?php foreach($detalles as $detalle){ ?>
                            <tr>
                                <input type="hidden" name="RendicionCajaChica[IDs][]" value="<?= $detalle->ID ?>">
                                <td>
                                    <select style="width:150px" name="RendicionCajaChica[Tipos][]" class="form-control tipo">
                                        <option value>Seleccionar</option>
                                        <option value="1" <?= ($detalle->Tipo=="1")?'selected':'';?>>Bienes</option>
                                        <option value="2" <?= ($detalle->Tipo=="2")?'selected':'';?>>Servicios</option>
                                    </select>
                                </td>
                                <td ><input style="width:150px" class="form-control" name="RendicionCajaChica[Fechas][]" type="date" value="<?= date('Y-m-d',strtotime($detalle->FechaDocumento)) ?>"></td>
                                <td >
                                    <select style="width:150px" name="RendicionCajaChica[TiposDocumentos][]" class="form-control">
                                        <option value>Seleccionar</option>
                                        <option value="1" <?= ($detalle->TipoDocumento==1)?'selected':'';?>>FACTURA</option>
                                        <option value="2" <?= ($detalle->TipoDocumento==2)?'selected':'';?>>RECIBO POR HONORARIOS</option>
                                        <option value="3" <?= ($detalle->TipoDocumento==3)?'selected':'';?>>BOLETA DE VENTA</option>
                                        <option value="4" <?= ($detalle->TipoDocumento==4)?'selected':'';?>>NOTA DE CREDITO</option>
                                        <option value="5" <?= ($detalle->TipoDocumento==5)?'selected':'';?>>NOTA DE DEBITO</option>
                                        <option value="6" <?= ($detalle->TipoDocumento==6)?'selected':'';?>>BOLETO DE VIAJE INTERPROVINCIAL</option>
                                        <option value="7" <?= ($detalle->TipoDocumento==7)?'selected':'';?>>VOUCHER</option>
                                        <option value="8" <?= ($detalle->TipoDocumento==8)?'selected':'';?>>TICKET</option>
                                        <option value="9" <?= ($detalle->TipoDocumento==9)?'selected':'';?>>COMPROBANTE DE GASTO</option>
                                        <option value="10" <?= ($detalle->TipoDocumento==10)?'selected':'';?>>DECLARACION JURADA</option>
                                    </select>
                                </td>
                                <td ><input style="width:150px" name="RendicionCajaChica[Clases][]" class="form-control" type="text" value="<?= $detalle->ClaseDocumento ?>"></td>
                                <td ><input style="width:150px" name="RendicionCajaChica[NumerosDocumentos][]" class="form-control" type="text" value="<?= $detalle->NumeroDocumento ?>"></td>
                                <td ><input style="width:150px" name="RendicionCajaChica[Rucs][]" class="form-control" type="text" value="<?= $detalle->RUC ?>" onblur="Proveedor(this,<?= $a ?> )"></td>
                                <td ><input  style="width:150px" class="form-control" type="text" value="<?= $detalle->Proveedor ?>" id="proveedor_<?= $a ?>" disabled></td>
                                <input name="RendicionCajaChica[Proveedores][]" class="form-control" type="hidden" id="proveedora_<?= $a ?>" value="<?= $detalle->Proveedor ?>">
                                <td ><input style="width:150px" name="RendicionCajaChica[DetallesGastos][]" class="form-control" type="text" value="<?= $detalle->DetalleGasto ?>"></td>
                                <td ><input style="width:150px" name="RendicionCajaChica[Importes][]" class="form-control contadorx" type="text" value="<?= $detalle->Importe ?>"></td>
                                <td>
                                    <a href="javascript:;" class="eliminar"><input type="hidden" value="<?= $detalle->ID ?>"><i class="fa fa-remove fa-lg" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                            <?php $total=$total+$detalle->Importe; ?>
                            <?php $a++;?>
                            <?php } ?>
                            <!-- <tr id='detalle_1'></tr> -->
                        </tbody>
                        <thead>
                            <tr id="css" >
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>Total:</th>
                                <th id="totala">S/. <?= $total ?></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" name="RendicionCajaChica[Y01]" value="1" class="btn btn-primary" id="btn-guardar-rendicion-caja"><i class="fa fa-check"></i>&nbsp;Guardar</button>
        <button type="submit" name="RendicionCajaChica[Y01]" value="2" class="btn btn-primary" ><i class="fa fa-check"></i>&nbsp;Finalizar</button>
    </div>
<?php ActiveForm::end(); ?>


<style type="text/css">
    .none{
        display: none;
    }
    .block{
        display: block;
    }

    .ui-draggable, .ui-droppable {
      background-position: top;
    }
</style>

<?php
$eliminarDetalle = Yii::$app->getUrlManager()->createUrl('rendicion-caja-chica/eliminar-detalle');
?>

<script>
    var bienes=<?= $cajaChica->Bienes; ?>;
    var servicios=<?= $cajaChica->Servicios; ?>;
    
    var $form = $('#frmRendicionCajaChica');
    var formParsleyfrmRendicionCajaChica = $form.parsley(defaultParsleyForm());
    var a=<?= $a ?>;
    $('#agregar-rendicion').click(function (e) {
        
        e.preventDefault();
        var gtml ='<tr>'+
                '<td>'+
                    '<select style="width:150px" name="RendicionCajaChica[Tipos][]" class="form-control tipo" required>'+
                        '<option value>Seleccionar</option>'+
                        '<option value="1">Bienes</option>'+
                        '<option value="2">Servicios</option>'+
                    '</select>'+
                '</td>'+
                '<td width="250">'+
                    '<input class="form-control" style="width:150px" type="date" name="RendicionCajaChica[Fechas][]" required>'+
                '</td>'+
                '<td>'+
                    '<select style="width:150px" name="RendicionCajaChica[TiposDocumentos][]" class="form-control" required>'+
                        '<option value>Seleccionar</option>'+
                        '<option value="1">FACTURA</option>'+
                        '<option value="2">RECIBO POR HONORARIOS</option>'+
                        '<option value="3">BOLETA DE VENTA</option>'+
                        '<option value="4">NOTA DE CREDITO</option>'+
                        '<option value="5">NOTA DE DEBITO</option>'+
                        '<option value="6">BOLETO DE VIAJE INTERPROVINCIAL</option>'+
                        '<option value="7">VOUCHER</option>'+
                        '<option value="8">TICKET</option>'+
                        '<option value="9">COMPROBANTE DE GASTO</option>'+
                        '<option value="10">DECLARACION JURADA</option>'+
                    '</select>'+
                '</td>'+
                '<td>'+
                    '<input class="form-control" style="width:150px" type="text" name="RendicionCajaChica[Clases][]" required>'+
                '</td>'+
                '<td>'+
                    '<input class="form-control" style="width:150px" type="text" name="RendicionCajaChica[NumerosDocumentos][]" required>'+
                '</td>'+
                '<td>'+
                    '<input class="form-control" style="width:150px" type="text" name="RendicionCajaChica[Rucs][]" maxlength="11" onKeyPress="return soloNumeros(event);"  id="rucs_'+a+'" onblur="Proveedor(this,'+a+')" required>'+
                '</td>'+
                '<td>'+
                    '<input class="form-control" style="width:150px" type="text"  id="proveedor_'+a+'"  disabled>'+
                    '<input class="form-control" style="width:150px" type="hidden" name="RendicionCajaChica[Proveedores][]"  id="proveedora_'+a+'"  >'+
                '</td>'+
                '<td>'+
                    '<input class="form-control" style="width:150px" type="text" name="RendicionCajaChica[DetallesGastos][]" required>'+
                '</td>'+
                '<td>'+
                    '<input class="form-control contadorx" style="width:150px"type="text" onKeyPress="return soloNumeros(event);" name="RendicionCajaChica[Importes][]" required>'+
                '</td>'+
                '<td>'+
                    '<a href="javascript:;" class="eliminar"><i class="fa fa-remove fa-lg" aria-hidden="true"></i></a>'+
                '</td>'+
            '</tr>';
        $('#detalle_rendicion').append(gtml);
        $('#css').removeAttr('style');
        a++;
    });
    
    $('#detalle').on('blur','.contadorx',function(){
        contar();
    });
    
    function contar(){
        var total_total = 0.00;
        var tbienes=0.00;
        var tservicios=0.00;
        
        $('.contadorx').each(function(x,y){
            TotSuma = $(this).val();
            total_total = parseFloat(getNum(TotSuma)) + parseFloat(total_total) ;
            Suma(total_total,parseFloat($('#total-compra').val()),this);
            
            tipo=$(this).parent().siblings('td').find('.tipo').val();
            if (tipo==1) {
                tbienes=tbienes+parseFloat($(this).val());
            }
            else if (tipo==2) {
                tservicios=tservicios+parseFloat($(this).val());
            }
        });
        
        if (tbienes>bienes) {
            bootbox.alert('Ha superado el monto máximo de bienes que ha sido asignado para ese encargo');
            
            return false;
        }
        else if (tservicios>servicios) {
            bootbox.alert('Ha superado el monto máximo de servicios que ha sido asignado para ese encargo');
            
            return false;
        }
        if (total_total>parseFloat($('#total-compra').val())) {
            return false;
        }
        $('#bienes').val((bienes-tbienes));
        $('#servicios').val((servicios-tservicios));
        $('#rendicion-anterior-bienes').val((bienes-tbienes));
        $('#rendicion-anterior-servicios').val((servicios-tservicios));
        
        
        $('#totala').text( "S/."+ number_format(total_total,2) );
    }
    
    function Suma(x,y,elemento) {
        if (elemento && x>y) {
            $(elemento).val('0.00');
            contar();
            return false;
        }
        
        
        return true;
    }
    
    function getNum(val) {
       if (val == '0.00' || val == '') {
            return 0;
       }
       var val2 = val.replace(',','');
       return val2;
    }
    
    function number_format(amount, decimals) {
        amount += '';
        amount = parseFloat(amount.replace(/[^0-9\.]/g, ''));
        decimals = decimals || 0;
        if (isNaN(amount) || amount === 0) 
            return parseFloat(0).toFixed(decimals);
        amount = '' + amount.toFixed(decimals);
        var amount_parts = amount.split('.'),
            regexp = /(\d+)(\d{3})/;
        while (regexp.test(amount_parts[0]))
            amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');
        return amount_parts.join('.');
    }
    
    
    $('body').on('click', '.btn-remove-detalle', function (e) {
        
        var $this = $(this);
        bootboxConfirmEliminar($this,
        'Está seguro de eliminar el registro?',
        function () {
            var id = $this.attr('data-id');
            var toke = '<?=Yii::$app->request->getCsrfToken()?>';
            $.get('<?= \Yii::$app->request->BaseUrl ?>/rendicion-caja-chica/eliminar-detalle', { id: id ,_csrf: toke }, function (data) {
                var objCuerpo=$(this).parents().get(3);
                var objFila=$(this).parents().get(2);
                $(objFila).remove();
            });
        });
    });
    function Proveedor(elemento,correlativo) {
        $.get('<?= \Yii::$app->request->BaseUrl ?>/orden/servicio/', { valor: $(elemento).val()}, function (data) {
            console.log(data);
            var jx = JSON.parse(data);
            if (jx==1) {
                bootbox.alert('RUC no se encuentra');
            }
            else
            {
                $('#proveedor_'+correlativo).val(jx.razonSocial);
                $('#proveedora_'+correlativo).val(jx.razonSocial);
            }
            
        });
    }
    
    $('body').on('click', '.eliminar', function (e) {
        var r = confirm("Estas seguro de Eliminar?");
        var mensaje = '';
        if (r == true) {
            id=$(this).children().val();
            console.log(id);
            
            if (id) {
                var toke = '<?=Yii::$app->request->getCsrfToken()?>';
                $.ajax({
                    url: '<?= $eliminarDetalle ?>',
                    type: 'POST',
                    async: false,
                    data: {id:id,_csrf: toke,},
                    success: function (data) {
                        
                    },
                    error: function () {
                        _pageLoadingEnd();
                    }
                });
                $(this).parent().parent().remove();
            }
            else
            {
                $(this).parent().parent().remove();    
            }   
            mensaje = "Se elimino el Registro Correctamente";
        } 
    });
</script>

