<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'defaultRoute'=>'panel/index',
    //'language' => 'es-ES',
    'components' => [
        
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'dateFormat' => 'd-M-Y',
            'datetimeFormat' => 'd-M-Y H:i:s',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            'currencyCode' => 'PEN',
            //'locale' => 'es-ES', 
        ],
        'tools' => [
            'class' => 'app\components\Tools'
        ],
        'administrativo' => [
            'class' => 'app\components\Administrativo'
        ]/*,
        'bnprogramaciontecnica' => [
            'class' => 'app\components\BNProgramacionTecnica'
        ],
        'daprogramaciontecnica' => [
            'class' => 'app\components\DAProgramacionTecnica'
        ]*/,
        //'timeZone' => 'America/Lima',
        'session' => [
            'name' => 'pip2', // unique for backend
            'savePath' => __DIR__ . '/../runtime', // a temporary folder on backend
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '234234#$%#$%#$%#$534535',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\Usuario',
            'enableAutoLogin' => true,
            'loginUrl'=>['login/index'],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,

            // 'rules' => [
            //     'requerimiento' => 'requerimiento/index'
            // ]
            //'class' => 'yii\rbac\DbManager',
            // ...
        ],
        'authManager'=>[
            'class' => 'yii\rbac\DbManager',
            //'defaultRoles' => ['admin','test'],
            
        ],

        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mail' => [
                'class' => 'yii\swiftmailer\Mailer',
                'useFileTransport' => false,
                'transport' => [
                    'class' => 'Swift_SmtpTransport',
                    'host' => 'smtp.gmail.com',
                    'username' => 'soportesistemapip2@pnia.gob.pe',
                    'password' => 'Pn1a.2016@',
                    'port' => '587',
                    'encryption' => 'tls',
                ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'db1' => require(__DIR__ . '/db1.php'),
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
