USE [master]
GO
/****** Object:  Database [PIP2]    Script Date: 18/04/2017 19:47:57 ******/
CREATE DATABASE [PIP2]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PIP2', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\PIP2.mdf' , SIZE = 129024KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'PIP2_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\PIP2_log.ldf' , SIZE = 1108800KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [PIP2] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PIP2].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PIP2] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PIP2] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PIP2] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PIP2] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PIP2] SET ARITHABORT OFF 
GO
ALTER DATABASE [PIP2] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PIP2] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PIP2] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PIP2] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PIP2] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PIP2] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PIP2] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PIP2] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PIP2] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PIP2] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PIP2] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PIP2] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PIP2] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PIP2] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PIP2] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PIP2] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PIP2] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PIP2] SET RECOVERY FULL 
GO
ALTER DATABASE [PIP2] SET  MULTI_USER 
GO
ALTER DATABASE [PIP2] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PIP2] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PIP2] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PIP2] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'PIP2', N'ON'
GO
USE [PIP2]
GO
/****** Object:  Table [dbo].[Actividad]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Actividad](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ComponenteID] [int] NOT NULL,
	[Nombre] [nvarchar](max) NULL,
	[CostoUnitario] [decimal](14, 2) NULL,
	[Total] [decimal](14, 2) NULL,
	[TotalFinanciamientoPnia] [decimal](14, 2) NULL,
	[TotalFinanciamientoAlianza] [decimal](14, 2) NULL,
	[Cantidad] [real] NULL,
	[Peso] [real] NULL,
	[Estado] [int] NULL,
	[identificador] [int] NULL,
	[Correlativo] [int] NULL,
	[Experimento] [int] NULL,
	[Evento] [int] NULL,
	[Avance] [decimal](10, 2) NULL DEFAULT ((0)),
	[TotalFinanciado] [decimal](10, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ActRubroElegible]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ActRubroElegible](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ActividadID] [int] NOT NULL,
	[RubroElegibleID] [int] NOT NULL,
	[UnidadMedida] [varchar](100) NULL,
	[CostoUnitario] [decimal](14, 5) NULL,
	[MetaFisica] [decimal](14, 5) NULL,
	[Total] [decimal](14, 5) NULL,
	[TotalFinanciamientoPnia] [decimal](14, 5) NULL,
	[TotalFinanciamientoAlianza] [decimal](14, 5) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AmbitoIntervencion]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AmbitoIntervencion](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InformacionGeneralID] [int] NOT NULL,
	[DistritoID] [varchar](6) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AntecedenteEntidad]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AntecedenteEntidad](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EntidadParticipanteID] [int] NOT NULL,
	[Antecedentes] [varchar](3000) NULL,
	[AsociadosHombres] [int] NOT NULL,
	[AsociadosMujeres] [int] NOT NULL,
	[AsociadosAtenderHombres] [int] NOT NULL,
	[AsociadosAtenderMujeres] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AporteActividad]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AporteActividad](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ActividadID] [int] NOT NULL,
	[EntidadParticipanteID] [int] NOT NULL,
	[Monetario] [decimal](14, 5) NOT NULL,
	[NoMonetario] [decimal](14, 5) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AporteActividadPC]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AporteActividadPC](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AporteActividadID] [int] NOT NULL,
	[PasoCriticoID] [int] NOT NULL,
	[Monetario] [decimal](14, 5) NOT NULL,
	[NoMonetario] [decimal](14, 5) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AporteActRubroElegible]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AporteActRubroElegible](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ActRubroElegibleID] [int] NOT NULL,
	[EntidadParticipanteID] [int] NOT NULL,
	[Monetario] [decimal](14, 5) NOT NULL,
	[NoMonetario] [decimal](14, 5) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AporteActRubroElegiblePC]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AporteActRubroElegiblePC](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AporteActRubroElegibleID] [int] NOT NULL,
	[PasoCriticoID] [int] NOT NULL,
	[Monetario] [decimal](14, 5) NOT NULL,
	[NoMonetario] [decimal](14, 5) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AporteAreSubCategoria]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AporteAreSubCategoria](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AreSubCategoriaID] [int] NOT NULL,
	[EntidadParticipanteID] [int] NOT NULL,
	[Monetario] [decimal](14, 5) NOT NULL,
	[NoMonetario] [decimal](14, 5) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AporteAreSubCategoriaPC]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AporteAreSubCategoriaPC](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AporteAreSubCategoriaID] [int] NOT NULL,
	[PasoCriticoID] [int] NOT NULL,
	[Monetario] [decimal](14, 5) NOT NULL,
	[NoMonetario] [decimal](14, 5) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AporteComponente]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AporteComponente](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ComponenteID] [int] NOT NULL,
	[EntidadParticipanteID] [int] NOT NULL,
	[Monetario] [decimal](14, 5) NOT NULL,
	[NoMonetario] [decimal](14, 5) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AporteComponentePC]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AporteComponentePC](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AporteComponenteID] [int] NOT NULL,
	[PasoCriticoID] [int] NOT NULL,
	[Monetario] [decimal](14, 5) NOT NULL,
	[NoMonetario] [decimal](14, 5) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AporteProyecto]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AporteProyecto](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProyectoID] [int] NOT NULL,
	[EntidadParticipanteID] [int] NOT NULL,
	[Monetario] [decimal](14, 5) NOT NULL,
	[NoMonetario] [decimal](14, 5) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AporteProyectoPC]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AporteProyectoPC](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AporteProyectoID] [int] NOT NULL,
	[PasoCriticoID] [int] NOT NULL,
	[Monetario] [decimal](14, 5) NOT NULL,
	[NoMonetario] [decimal](14, 5) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AreSubCategoria]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AreSubCategoria](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ActRubroElegibleID] [int] NOT NULL,
	[Nombre] [nvarchar](4000) NULL,
	[UnidadMedida] [nvarchar](100) NULL,
	[CostoUnitario] [decimal](14, 2) NULL,
	[MetaFisica] [decimal](14, 2) NULL,
	[Total] [decimal](14, 2) NULL,
	[TotalFinanciamientoPnia] [decimal](14, 2) NULL,
	[TotalFinanciamientoAlianza] [decimal](14, 2) NULL,
	[TotalConMetaFisica] [decimal](14, 2) NULL,
	[EntidadParticipanteID] [int] NULL,
	[Correlativo] [int] NULL,
	[Situacion] [int] NULL,
	[MetaFisicaEjecutada] [decimal](10, 2) NULL,
	[MetaAvance] [decimal](10, 2) NULL,
	[FondoEntidadParticipanteID] [int] NULL,
	[Especifica] [nvarchar](4000) NULL,
	[Detalle] [nvarchar](4000) NULL,
	[Asignado] [int] NULL,
	[Codificacion] [int] NULL,
	[CostoUnitarioEjecutada] [decimal](14, 2) NULL,
	[SaldoAnterior] [decimal](14, 2) NULL,
	[SaldoActual] [decimal](14, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AreSubCategoriaObservacion]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AreSubCategoriaObservacion](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Observacion] [nvarchar](max) NULL,
	[AreSubCategoriaID] [int] NULL,
	[Estado] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CajaChica]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CajaChica](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FechaRegistro] [datetime] NULL,
	[CodigoProyecto] [nvarchar](150) NULL,
	[Correlativo] [int] NULL,
	[Descripcion] [nvarchar](250) NULL,
	[Bienes] [real] NULL,
	[Servicios] [real] NULL,
	[ResolucionDirectorial] [nvarchar](150) NULL,
	[Situacion] [int] NULL,
	[Estado] [int] NULL,
	[RequerimientoID] [int] NULL,
	[Documento] [nvarchar](150) NULL,
	[Responsable] [nvarchar](250) NULL,
	[Banco] [nvarchar](250) NULL,
	[CTA] [nvarchar](250) NULL,
	[CCI] [nvarchar](250) NULL,
	[NResolucionDirectorial] [nvarchar](150) NULL,
	[Tipo] [int] NULL,
	[Annio] [int] NULL,
	[SaldoAnteriorBienes] [real] NULL,
	[SaldoAnteriorServicios] [real] NULL,
	[SaldoActualBienes] [real] NULL,
	[SaldoActualServicios] [real] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Categoria]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Categoria](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](100) NULL,
	[Fecha] [datetime] NULL,
	[Estado] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CertificacionPresupuestal]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CertificacionPresupuestal](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Memorando] [nvarchar](150) NULL,
	[JefePnia] [nvarchar](150) NULL,
	[PasoCriticoID] [int] NULL,
	[Asunto] [nvarchar](500) NULL,
	[Referencia] [nvarchar](500) NULL,
	[Descripcion] [nvarchar](4000) NULL,
	[MetaPresupuestal] [nvarchar](150) NULL,
	[Clasificacion] [nvarchar](150) NULL,
	[Annio] [nvarchar](10) NULL,
	[Situacion] [int] NULL,
	[Estado] [int] NULL,
	[FechaRegistro] [datetime] NULL,
	[Documento] [nvarchar](150) NULL,
	[MontoTotal] [real] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Componente]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Componente](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](max) NULL,
	[Total] [decimal](14, 5) NULL,
	[TotalFinanciamientoPnia] [decimal](14, 5) NULL,
	[TotalFinanciamientoAlianza] [decimal](14, 5) NULL,
	[CostoUnitario] [real] NULL,
	[Cantidad] [real] NULL,
	[Peso] [real] NULL,
	[Estado] [int] NULL,
	[PoaID] [int] NULL,
	[Identificador] [int] NULL,
	[Correlativo] [int] NULL,
	[Experimento] [int] NULL DEFAULT ((0)),
	[Evento] [int] NULL DEFAULT ((0)),
	[Avance] [decimal](10, 2) NULL,
	[TotalFinanciado] [decimal](10, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CompromisoPresupuestal]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CompromisoPresupuestal](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Correlativo] [int] NULL,
	[JefeUnidad] [nvarchar](100) NULL,
	[NumeroResolucionJefatural] [nvarchar](100) NULL,
	[FechaSolicitud] [datetime] NULL,
	[Codigo] [nvarchar](150) NULL,
	[MontoTotal] [float] NULL,
	[Clasificacion] [nvarchar](50) NULL,
	[FechaRegistro] [datetime] NULL,
	[Estado] [int] NULL,
	[Memorando] [nvarchar](100) NULL,
	[Documento] [nvarchar](150) NULL,
	[Situacion] [int] NULL,
	[GastoElegible] [int] NULL,
	[UsuarioAsignado] [int] NULL,
	[RequerimientoID] [int] NULL,
	[Observacion] [nvarchar](max) NULL,
	[CompromisoAnual] [int] NULL,
	[CompromisoAdministrativo] [int] NULL,
	[Devengado] [int] NULL,
	[Girado] [int] NULL,
	[Pagado] [int] NULL,
	[Annio] [int] NULL,
	[PasoCriticoID] [int] NULL,
	[Formato] [nvarchar](150) NULL,
	[FormatoUafsi] [nvarchar](150) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ConformidadPago]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ConformidadPago](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[OrdenID] [int] NULL,
	[Contenido] [nvarchar](max) NULL,
	[Puntuacion] [int] NULL,
	[Observacion] [nvarchar](max) NULL,
	[Tipo] [int] NULL,
	[Estado] [int] NULL,
	[FechaRegistro] [datetime] NULL,
	[Memorando] [nvarchar](150) NULL,
	[Correlativo] [nvarchar](150) NULL,
	[CodigoProyecto] [nvarchar](150) NULL,
	[SIAF] [nvarchar](150) NULL,
	[Situacion] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ConsumoAgua]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ConsumoAgua](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Mes] [nvarchar](2) NULL,
	[Cantidad] [real] NULL,
	[Tipo] [int] NULL,
	[FechaRegistro] [datetime] NULL,
	[Anno] [int] NULL,
	[InformeTecnicoFinancieroID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ConsumoCombustible]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ConsumoCombustible](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Mes] [nvarchar](2) NULL,
	[Cantidad] [real] NULL,
	[Tipo] [int] NULL,
	[FechaRegistro] [datetime] NULL,
	[Anno] [int] NULL,
	[InformeTecnicoFinancieroID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CronogramaActividad]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CronogramaActividad](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ActividadID] [int] NOT NULL,
	[Mes] [int] NOT NULL,
	[MetaFisica] [decimal](14, 5) NULL,
	[MetaFinanciera] [decimal](14, 5) NULL,
	[PoafMetaFinanciera] [decimal](14, 5) NULL,
	[PoafMetaFisica] [decimal](14, 5) NULL,
	[MesDescripcion] [nvarchar](150) NULL,
	[MetaFisicaEjecutada] [decimal](10, 2) NULL,
	[MetaAvance] [decimal](10, 2) NULL,
	[SituacionEjecucion] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CronogramaActRubroElegible]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CronogramaActRubroElegible](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ActRubroElegibleID] [int] NOT NULL,
	[Mes] [int] NOT NULL,
	[MetaFinanciera] [decimal](14, 2) NULL,
	[PoafMetaFinanciera] [decimal](14, 2) NULL,
	[MetaFisica] [decimal](10, 2) NULL,
	[Situacion] [int] NULL,
	[Estado] [int] NULL,
	[MetaFisicaEjecutada] [decimal](10, 2) NULL,
	[MetaAvance] [decimal](10, 2) NULL,
	[SituacionEjecucion] [int] NULL,
	[MesDescripcion] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CronogramaAreSubCategoria]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CronogramaAreSubCategoria](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AreSubCategoriaID] [int] NOT NULL,
	[Mes] [int] NOT NULL,
	[MetaFinanciera] [decimal](14, 2) NULL,
	[PoafMetaFinanciera] [decimal](14, 2) NULL,
	[MetaFisica] [decimal](18, 2) NULL,
	[Situacion] [int] NULL,
	[Estado] [int] NULL,
	[MetaFisicaEjecutada] [decimal](10, 2) NULL,
	[MetaAvance] [decimal](10, 2) NULL,
	[SituacionEjecucion] [int] NULL,
	[MesDescripcion] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CronogramaComponente]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CronogramaComponente](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ComponenteID] [int] NOT NULL,
	[Mes] [int] NOT NULL,
	[MetaFinanciera] [decimal](14, 5) NULL,
	[PoafMetaFinanciera] [decimal](14, 5) NULL,
	[MetaFisica] [decimal](14, 5) NULL,
	[MesDescripcion] [nvarchar](150) NULL,
	[MetaFisicaEjecutada] [decimal](10, 2) NULL,
	[MetaAvance] [decimal](10, 2) NULL,
	[SituacionEjecucion] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CronogramaProyecto]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CronogramaProyecto](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProyectoID] [int] NOT NULL,
	[Mes] [int] NOT NULL,
	[MetaFinanciera] [decimal](14, 5) NOT NULL,
	[PoafMetaFinanciera] [decimal](14, 5) NULL,
	[MetaFisica] [decimal](14, 5) NULL,
	[MetaEjecutada] [decimal](14, 5) NULL,
	[Estado] [int] NULL,
	[MesDescripcion] [nvarchar](255) NULL,
	[MetaFisicaEjecutada] [decimal](10, 2) NULL,
	[MetaAvance] [decimal](10, 2) NULL,
	[SituacionEjecucion] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CronogramaTarea]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CronogramaTarea](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TareaID] [int] NULL,
	[Mes] [int] NULL,
	[MetaFisica] [decimal](10, 2) NULL,
	[PoafMetaFinanciera] [decimal](10, 2) NULL,
	[MetaFinanciera] [decimal](10, 2) NULL,
	[Situacion] [int] NULL,
	[Estado] [int] NULL,
	[MesDescripcion] [nvarchar](150) NULL,
	[MetaFisicaEjecutada] [decimal](10, 2) NULL,
	[MetaAvance] [decimal](10, 2) NULL,
	[SituacionEjecucion] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CultivoCrianza]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CultivoCrianza](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](250) NULL,
	[ProgramaID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DetalleCajaChica]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleCajaChica](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FechaRegistro] [datetime] NULL,
	[CodigoProyecto] [nvarchar](150) NULL,
	[Correlativo] [int] NULL,
	[Descripcion] [nvarchar](250) NULL,
	[Bienes] [real] NULL,
	[Servicios] [real] NULL,
	[ResolucionDirectorial] [nvarchar](150) NULL,
	[Situacion] [int] NULL,
	[Estado] [int] NULL,
	[RequerimientoID] [int] NULL,
	[Documento] [nvarchar](150) NULL,
	[Responsable] [nvarchar](250) NULL,
	[Banco] [nvarchar](250) NULL,
	[CTA] [nvarchar](250) NULL,
	[CCI] [nvarchar](250) NULL,
	[NResolucionDirectorial] [nvarchar](150) NULL,
	[Tipo] [int] NULL,
	[Annio] [int] NULL,
	[SaldoAnteriorBienes] [real] NULL,
	[SaldoAnteriorServicios] [real] NULL,
	[SaldoActualBienes] [real] NULL,
	[SaldoActualServicios] [real] NULL,
	[CajaChicaID] [int] NULL,
	[TipoCambio] [real] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DetalleCertificacionPresupuestal]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleCertificacionPresupuestal](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CertificacionPresupuestalID] [int] NULL,
	[Codigo] [nvarchar](150) NULL,
	[Monto] [numeric](10, 2) NULL,
	[CodigoCertificacion] [nvarchar](150) NULL,
	[Bienes] [numeric](10, 2) NULL,
	[Servicio] [numeric](10, 2) NULL,
	[CodigoPoaPNIA] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DetalleCompromisoPresupuestal]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleCompromisoPresupuestal](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CompromisoPresupuestalID] [int] NULL,
	[TipoGasto] [int] NULL,
	[Correlativo] [nvarchar](100) NULL,
	[RUC] [nvarchar](15) NULL,
	[RazonSocial] [nvarchar](250) NULL,
	[MontoTotal] [real] NULL,
	[Bienes] [real] NULL,
	[Compras] [real] NULL,
	[GastoElegible] [int] NULL,
	[Situacion] [int] NULL,
	[Observacion] [nvarchar](max) NULL,
	[CodigoMatriz] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DetalleConformidadPago]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetalleConformidadPago](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ConformidadPagoID] [int] NULL,
	[Estado] [int] NULL,
	[FechaCreacion] [datetime] NULL,
	[TipoGasto] [int] NULL,
	[OrdenServicio] [varchar](255) NULL,
	[ConformidadServicio] [varchar](255) NULL,
	[ReciboHonorario] [varchar](255) NULL,
	[InformeActividades] [varchar](255) NULL,
	[OrdenCompra] [varchar](255) NULL,
	[ConformidadEspecificaciones] [varchar](255) NULL,
	[Factura] [varchar](255) NULL,
	[Situacion] [int] NULL,
	[Observacion] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetalleEjecucion]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleEjecucion](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AreSubCategoriaID] [int] NULL,
	[Cantidad] [int] NULL,
	[PrecioUnitario] [real] NULL,
	[PrecioTotal] [real] NULL,
	[Estado] [int] NULL,
	[Situacion] [int] NULL,
	[FechaRegistro] [datetime] NULL,
	[CodigoMatriz] [nvarchar](25) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DetalleEjecucionFinancieraColaboradora]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleEjecucionFinancieraColaboradora](
	[ID] [int] NOT NULL,
	[ActividadID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DetalleOrden]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleOrden](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[OrdenID] [int] NULL,
	[Cantidad] [int] NULL,
	[UnidadMedida] [nvarchar](150) NULL,
	[Descripcion] [nvarchar](4000) NULL,
	[PrecioUnitario] [real] NULL,
	[Total] [real] NULL,
	[Codigo] [nvarchar](150) NULL,
	[AreSubCategoriaID] [int] NULL,
	[CodigoPoa] [nvarchar](150) NULL,
	[Objetivo] [nvarchar](250) NULL,
	[Actividad] [nvarchar](250) NULL,
	[Recurso] [nvarchar](250) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DetalleRendicionCajaChica]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleRendicionCajaChica](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RendicionCajaChicaID] [int] NULL,
	[FechaDocumento] [datetime] NULL,
	[ClaseDocumento] [nvarchar](150) NULL,
	[NumeroDocumento] [nvarchar](150) NULL,
	[Proveedor] [nvarchar](250) NULL,
	[DetalleGasto] [nvarchar](max) NULL,
	[Importe] [real] NULL,
	[RUC] [nvarchar](11) NULL,
	[TipoDocumento] [int] NULL,
	[Tipo] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DetalleRendicionEncargo]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleRendicionEncargo](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RendicionEncargoID] [int] NULL,
	[FechaDocumento] [datetime] NULL,
	[ClaseDocumento] [nvarchar](150) NULL,
	[NumeroDocumento] [nvarchar](150) NULL,
	[Proveedor] [nvarchar](250) NULL,
	[DetalleGasto] [nvarchar](max) NULL,
	[Importe] [real] NULL,
	[RUC] [nvarchar](11) NULL,
	[TipoDocumento] [int] NULL,
	[Tipo] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DetalleRendicionEncargoDeclaracion]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleRendicionEncargoDeclaracion](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RendicionEncargoID] [int] NULL,
	[Fecha] [datetime] NULL,
	[Detalle] [nvarchar](max) NULL,
	[Importe] [real] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DetalleRendicionViatico]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetalleRendicionViatico](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RendicionID] [int] NULL,
	[Fecha] [datetime] NULL,
	[Clase] [varchar](5000) NULL,
	[Proveedor] [varchar](3000) NULL,
	[Detalle] [nvarchar](max) NULL,
	[Importe] [int] NULL,
	[FechaCreacion] [datetime] NULL,
	[TipoDoc] [int] NULL,
	[Ruc] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DetalleRequerimiento]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleRequerimiento](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RequerimientoID] [int] NULL,
	[AreSubCategoriaID] [int] NULL,
	[CodigoMatriz] [nvarchar](max) NULL,
	[Cantidad] [real] NULL,
	[PrecioUnitario] [real] NULL,
	[Descripcion] [nvarchar](max) NULL,
	[Total] [real] NULL,
	[Objetivo] [nvarchar](max) NULL,
	[Actividad] [nvarchar](max) NULL,
	[Recurso] [nvarchar](max) NULL,
	[Codificacion] [int] NULL,
	[Situacion] [int] NULL,
	[TipoDetalleOrdenID] [int] NULL,
	[Estado] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DetalleSolicitudPago]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleSolicitudPago](
	[ID] [int] NOT NULL,
	[SolicitudPagoID] [int] NULL,
	[FechaRegistro] [datetime] NULL,
	[Proveedor] [nvarchar](150) NULL,
	[RUC] [nvarchar](11) NULL,
	[Monto] [real] NULL,
	[AreSubCategoriaID] [int] NULL,
	[TerminoReferenciaDocumento] [nvarchar](150) NULL,
	[OrdenDocumento] [nvarchar](150) NULL,
	[InformeDocumento] [nvarchar](150) NULL,
	[ReciboDocumento] [nvarchar](150) NULL,
	[Suspension4taDocumento] [nvarchar](150) NULL,
	[ValidezRHDocumento] [nvarchar](150) NULL,
	[ConformidadServicioDocumento] [nvarchar](150) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DetalleViatico]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DetalleViatico](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ViaticoID] [int] NULL,
	[Tipo] [varchar](100) NULL,
	[Importe] [decimal](8, 2) NULL,
	[Estado] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DireccionLinea]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DireccionLinea](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](250) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Documento]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Documento](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](300) NULL,
	[Estado] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DocumentoInformacionGeneral]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DocumentoInformacionGeneral](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InformacionGeneralID] [int] NULL,
	[Documento] [nvarchar](250) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EjecucionFinancieraColaboradora]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EjecucionFinancieraColaboradora](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InformeTecnicoFinancieroID] [int] NULL,
	[CodigoProyecto] [nvarchar](150) NULL,
	[ColaboradoraID] [int] NULL,
	[TipoAporte] [int] NULL,
	[ActividadID] [int] NULL,
	[MetaTotalProgramado] [real] NULL,
	[MetaSemestralEjecutada] [real] NULL,
	[Avance] [real] NULL,
	[DetalleAporte] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EjecucionMetaFisicaFinanciera]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EjecucionMetaFisicaFinanciera](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InformeTecnicoFinancieroID] [int] NULL,
	[ActividadID] [int] NULL,
	[Descripcion] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Encargo]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Encargo](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Correlativo] [int] NULL,
	[CodigoProyecto] [nvarchar](150) NULL,
	[IRP] [nvarchar](150) NULL,
	[Cargo] [nvarchar](150) NULL,
	[DescripcionActividad] [nvarchar](4000) NULL,
	[FechaInicio] [datetime] NULL,
	[FechaFin] [datetime] NULL,
	[Total] [real] NULL,
	[Bienes] [real] NULL,
	[Servicios] [real] NULL,
	[NombresEncargo] [nvarchar](250) NULL,
	[ApellidosEncargo] [nvarchar](250) NULL,
	[DNIEncargo] [nvarchar](8) NULL,
	[CargoEncargo] [nvarchar](250) NULL,
	[AreSubCategoriaID] [int] NULL,
	[Documento] [nvarchar](250) NULL,
	[Situacion] [int] NULL,
	[Estado] [int] NULL,
	[FechaRegistro] [datetime] NULL,
	[Annio] [int] NULL,
	[ContratoEncargo] [int] NULL,
	[CodigoPoa] [nvarchar](150) NULL,
	[RequerimientoID] [int] NULL,
	[PasoCriticoID] [int] NULL,
	[ResolucionDirectorial] [nvarchar](150) NULL,
	[Certificacion] [nvarchar](150) NULL,
	[NResolucionDirectorial] [nvarchar](150) NULL,
	[TipoCambio] [real] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EntidadOferente]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EntidadOferente](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InvestigadorID] [int] NOT NULL,
	[RazonSocial] [varchar](250) NOT NULL,
	[Ruc] [varchar](11) NOT NULL,
	[Telefono] [varchar](25) NOT NULL,
	[Experiencia] [varchar](250) NOT NULL,
	[Tipo] [int] NOT NULL,
	[Extension_CartaPresentacion] [varchar](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EntidadParticipante]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EntidadParticipante](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InvestigadorID] [int] NOT NULL,
	[RazonSocial] [varchar](250) NOT NULL,
	[Siglas] [varchar](20) NOT NULL,
	[Ruc] [varchar](11) NOT NULL,
	[Regimen] [varchar](20) NOT NULL,
	[Procedencia] [varchar](20) NOT NULL,
	[TipoInstitucionID] [int] NOT NULL,
	[FechaCreacion] [datetime] NOT NULL,
	[DomicilioFiscal] [varchar](250) NOT NULL,
	[Telefono] [varchar](20) NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[PaginaWeb] [varchar](100) NULL,
	[TipoInstitucionOtro] [varchar](100) NULL,
	[Tipo] [int] NOT NULL,
	[Extension_PadronProductores] [varchar](7) NULL,
	[Extension_ActaEntidadProponente] [varchar](7) NULL,
	[Extension_CartaCompromisoColaboradora] [varchar](7) NULL,
	[Articulo] [varchar](10) NULL,
	[Estado] [int] NULL,
	[AporteMonetario] [decimal](10, 2) NULL,
	[AporteNoMonetario] [decimal](10, 2) NULL DEFAULT ((0.00)),
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Especie]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Especie](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](250) NULL,
	[CultivoCrianzaID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Etapa]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Etapa](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](255) NULL,
	[Codigo] [nvarchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Evento]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Evento](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ActividadID] [int] NULL,
	[CodigoProyecto] [nvarchar](10) NULL,
	[InvestigadorResponsable] [nvarchar](250) NULL,
	[EEAA] [nvarchar](250) NULL,
	[Titulo] [nvarchar](500) NULL,
	[ResponsableEvento] [nvarchar](250) NULL,
	[Tipologia] [int] NULL,
	[PasoCriticoID] [int] NULL,
	[Duracion] [real] NULL,
	[Latitud] [real] NULL,
	[Longitud] [real] NULL,
	[CentroPobladoID] [int] NULL,
	[BeneficiarioHombres] [int] NULL,
	[BeneficiarioMujeres] [int] NULL,
	[Introduccion] [nvarchar](max) NULL,
	[Material] [nvarchar](max) NULL,
	[Metodo] [nvarchar](max) NULL,
	[Resultado] [nvarchar](max) NULL,
	[Conclusion] [nvarchar](max) NULL,
	[Costo] [real] NULL,
	[Documento] [nvarchar](150) NULL,
	[Nota] [nvarchar](max) NULL,
	[FechaRegistro] [datetime] NULL,
	[Estado] [int] NULL,
	[Situacion] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Experimento]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Experimento](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ActividadID] [int] NULL,
	[CodigoProyecto] [nvarchar](10) NULL,
	[InvestigadorResponsable] [nvarchar](250) NULL,
	[EEAA] [nvarchar](250) NULL,
	[Titulo] [nvarchar](500) NULL,
	[ResponsableExperimento] [nvarchar](250) NULL,
	[Tipologia] [int] NULL,
	[PasoCriticoID] [int] NULL,
	[Duracion] [real] NULL,
	[Latitud] [real] NULL,
	[Longitud] [real] NULL,
	[CentroPobladoID] [int] NULL,
	[BeneficiarioHombres] [int] NULL,
	[BeneficiarioMujeres] [int] NULL,
	[Introduccion] [nvarchar](max) NULL,
	[Material] [nvarchar](max) NULL,
	[Metodo] [nvarchar](max) NULL,
	[Resultado] [nvarchar](max) NULL,
	[ObservacionUafsi] [nvarchar](max) NULL,
	[ObservacionInvestigadorResponsable] [nvarchar](max) NULL,
	[Costo] [real] NULL,
	[Documento] [nvarchar](150) NULL,
	[Nota] [nvarchar](max) NULL,
	[FechaRegistro] [datetime] NULL,
	[Estado] [int] NULL,
	[Situacion] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FlujoCajaProyectado]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FlujoCajaProyectado](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ComponenteID] [int] NOT NULL,
	[Anio] [int] NOT NULL,
	[Valor] [decimal](14, 5) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FondoRecibidoEntidad]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FondoRecibidoEntidad](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EntidadParticipanteID] [int] NOT NULL,
	[InstitucionOtorgante] [varchar](250) NOT NULL,
	[TituloProyecto] [varchar](250) NOT NULL,
	[Monto] [decimal](14, 5) NOT NULL,
	[FechaInicio] [datetime] NOT NULL,
	[FechaCierre] [datetime] NOT NULL,
	[NumeroEntidadesParticipantes] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GastoItf]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GastoItf](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FECH_EMIS_COP] [datetime] NULL,
	[FECH_PAGO_COP] [datetime] NULL,
	[NUME_COMP_COP] [varchar](20) NULL,
	[ANNO_EJEC_EJE] [int] NULL,
	[FECH_DOCU_DOC] [datetime] NULL,
	[ABRE_DOCU_TPD] [char](4) NULL,
	[SERI_DOCU_TPD] [char](4) NULL,
	[NUME_DOCU_DOC] [varchar](15) NULL,
	[NUM_RUC_CAB] [char](11) NULL,
	[DESC_ANEX_ANX] [nvarchar](max) NULL,
	[TEXT_REFE_CAB] [nvarchar](max) NULL,
	[MNTO_NACI_CAB] [real] NULL,
	[ESTADO] [int] NULL,
	[TIPO] [varchar](2) NULL,
	[PasoCriticoID] [int] NULL,
	[CertificadoPresupuestalID] [int] NULL,
	[FechaUnidadEjecutora] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Hito]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hito](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InformeTecnicoFinancieroID] [int] NULL,
	[CodigoProyecto] [nvarchar](150) NULL,
	[Correlativo] [int] NULL,
	[Titulo] [nvarchar](max) NULL,
	[Descripcion] [nvarchar](max) NULL,
	[PeriodoInicio] [datetime] NULL,
	[PeriodoFin] [datetime] NULL,
	[Cumplio] [int] NULL,
	[DescripcionCumplio] [nvarchar](max) NULL,
	[EjecucionFinanciera] [nvarchar](max) NULL,
	[Observacion] [nvarchar](max) NULL,
	[Logros] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[IndicadorPasoCritico]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IndicadorPasoCritico](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PasoCriticoID] [int] NOT NULL,
	[Descripcion] [varchar](500) NOT NULL,
	[UnidadMedida] [varchar](250) NOT NULL,
	[Cantidad] [decimal](14, 5) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InformacionGeneral]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InformacionGeneral](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InvestigadorID] [int] NOT NULL,
	[TituloProyecto] [varchar](1500) NOT NULL,
	[ProgramaTransversalID] [int] NOT NULL,
	[DistritoID] [varchar](6) NOT NULL,
	[FechaInicioProyecto] [datetime] NOT NULL,
	[FechaFinProyecto] [datetime] NOT NULL,
	[Meses] [int] NOT NULL,
	[DireccionLineaID] [int] NOT NULL,
	[TipoInvestigacionID] [int] NOT NULL,
	[UnidadOperativaID] [int] NOT NULL,
	[EspecieID] [int] NOT NULL,
	[Resumen] [nvarchar](max) NULL,
	[Codigo] [nvarchar](150) NULL,
	[TipoImpactoID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InformeTecnicoFinanciero]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InformeTecnicoFinanciero](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PasoCriticoID] [int] NULL,
	[CodigoProyecto] [nvarchar](150) NULL,
	[MontoDesembolso] [real] NULL,
	[SaldoDesembolsoAnterior] [real] NULL,
	[ResumenEjecutivo] [nvarchar](max) NULL,
	[ProgramacionActividad] [nvarchar](max) NULL,
	[EjecucionFinancieraColaboradora] [nvarchar](max) NULL,
	[LeccionAprendida] [nvarchar](max) NULL,
	[Conclusion] [nvarchar](max) NULL,
	[Recomendacion] [nvarchar](max) NULL,
	[Anexo1] [nvarchar](150) NULL,
	[Anexo2] [nvarchar](150) NULL,
	[Anexo3] [nvarchar](150) NULL,
	[Anexo4] [nvarchar](150) NULL,
	[Anexo5] [nvarchar](150) NULL,
	[Anexo6] [nvarchar](150) NULL,
	[Anexo7] [nvarchar](150) NULL,
	[Anexo8] [nvarchar](150) NULL,
	[Anexo9] [nvarchar](150) NULL,
	[Anexo10] [nvarchar](150) NULL,
	[Anexo11] [nvarchar](150) NULL,
	[Anexo12] [nvarchar](150) NULL,
	[Estado] [int] NULL,
	[Situacion] [int] NULL,
	[FechaRegistro] [datetime] NULL,
	[FechaInforme] [datetime] NULL,
	[FechaDesembolsoPNIA] [datetime] NULL,
	[Correlativo] [int] NULL,
	[DescribirResiduosPeligrosos] [nvarchar](max) NULL,
	[SeguridadOcupacional] [nvarchar](max) NULL,
	[ListaInvolucrados] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Investigador]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Investigador](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UsuarioID] [int] NOT NULL,
	[ConvocatoriaID] [int] NOT NULL,
	[EtapaID] [int] NOT NULL,
	[Extension_ListaDeChequeo] [varchar](7) NULL,
	[FechaEnvioProyecto] [datetime] NULL,
	[CodigoEnvioProyecto] [varchar](50) NULL,
	[AVANCE] [decimal](14, 5) NULL,
	[EstadoAprobatorio] [int] NULL,
	[EstadoEvaPET] [int] NULL,
	[FechaEnvioConsEvaPET] [datetime] NULL,
	[FechaRespConsEvaPET] [datetime] NULL,
	[EtapaEvaluacion] [int] NULL,
	[FechaEnvioProyectoPC] [datetime] NULL,
	[NroInfoConformidadUD] [varchar](120) NULL,
	[NroInfoConformidadUPMSI] [varchar](120) NULL,
	[NroInfoConformidadLegal] [varchar](120) NULL,
	[EstadoAprobatorioFinal] [varchar](120) NULL,
	[FechaInfoConformidadUD] [varchar](120) NULL,
	[ObservacionConformidadUD] [varchar](1500) NULL,
	[MontoDesembolso1] [decimal](14, 5) NULL,
	[Departamento] [varchar](500) NULL,
	[Provincia] [varchar](500) NULL,
	[Distrito] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LibroBanco]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LibroBanco](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SECU_COMP_COP] [nvarchar](5) NULL,
	[FECH_EMIS_COP] [datetime] NULL,
	[FECH_PAGO_COP] [datetime] NULL,
	[NUM_CHEQ_CHE] [nvarchar](15) NULL,
	[DESC_ANEX_ANX] [nvarchar](max) NULL,
	[CONC_PAGO_COP] [nvarchar](max) NULL,
	[MONT_COMP_COP] [real] NULL,
	[MONT_SOLE_CHE] [real] NULL,
	[CODI_BANC_BAN] [nvarchar](5) NULL,
	[CODI_CNTA_CTA] [nvarchar](5) NULL,
	[NUME_CORR_CHE] [nvarchar](10) NULL,
	[NUME_SIAF_COP] [nvarchar](10) NULL,
	[ESTADO] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LogCompromisoPresupuestal]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogCompromisoPresupuestal](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TipoGastoID] [int] NULL,
	[DetalleTipoGastoID] [int] NULL,
	[FechaInicio] [datetime] NULL,
	[FechaFin] [datetime] NULL,
	[Estado] [int] NULL,
	[Annio] [int] NULL,
	[PerfilID] [int] NULL,
	[CompromisoPresupuestalID] [int] NULL,
	[CodigoProyectoID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MarcoLogicoActividad]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MarcoLogicoActividad](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ActividadID] [int] NOT NULL,
	[IndicadorMedioVerificacion] [nvarchar](max) NULL,
	[IndicadorSupuestos] [nvarchar](max) NULL,
	[Estado] [int] NULL,
	[IndicadorUnidadMedida] [nvarchar](255) NULL,
	[IndicadorMetaFisica] [nvarchar](255) NULL,
	[DescripcionMetaFisica] [nvarchar](max) NULL,
	[IndicadorPniaID] [int] NULL,
	[IndicadorDescripcion] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MarcoLogicoComponente]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MarcoLogicoComponente](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ComponenteID] [int] NOT NULL,
	[IndicadorTipo] [nvarchar](255) NULL,
	[IndicadorDescripcion] [nvarchar](max) NULL,
	[IndicadorUnidadMedida] [nvarchar](255) NULL,
	[IndicadorInicio] [decimal](14, 5) NULL,
	[IndicadorMeta] [nvarchar](255) NULL,
	[MedioVerificacion] [nvarchar](max) NULL,
	[Supuestos] [nvarchar](max) NULL,
	[Estado] [int] NULL,
	[DescripcionMeta] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MarcoLogicoFinProyecto]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MarcoLogicoFinProyecto](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProyectoID] [int] NOT NULL,
	[IndicadorTipo] [nvarchar](100) NULL,
	[IndicadorDescripcion] [nvarchar](max) NULL,
	[IndicadorUnidadMedida] [nvarchar](255) NULL,
	[IndicadorInicio] [decimal](14, 5) NULL,
	[IndicadorMeta] [nvarchar](255) NULL,
	[MedioVerificacion] [nvarchar](max) NULL,
	[Supuestos] [nvarchar](max) NULL,
	[Estado] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MarcoLogicoPropositoProyecto]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MarcoLogicoPropositoProyecto](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProyectoID] [int] NOT NULL,
	[IndicadorTipo] [nvarchar](255) NULL,
	[IndicadorDescripcion] [nvarchar](max) NULL,
	[IndicadorUnidadMedida] [nvarchar](255) NULL,
	[IndicadorInicio] [decimal](14, 5) NULL,
	[IndicadorMeta] [nvarchar](255) NULL,
	[MedioVerificacion] [nvarchar](max) NULL,
	[Supuestos] [nvarchar](max) NULL,
	[Estado] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MedidaSocioAmbiental]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MedidaSocioAmbiental](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [nvarchar](max) NULL,
	[FechaRegistro] [datetime] NULL,
	[InformeTecnicoFinancieroID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MiembroEquipoGestion]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MiembroEquipoGestion](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InvestigadorID] [int] NOT NULL,
	[PersonaID] [int] NOT NULL,
	[RolEquipoTecnicoID] [int] NOT NULL,
	[EntidadParticipanteID] [int] NOT NULL,
	[TituloObtenido] [varchar](250) NOT NULL,
	[Experiencia] [varchar](250) NOT NULL,
	[Extension_CV] [varchar](7) NULL,
	[Estado] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MiembroEquipoInvestigacion]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MiembroEquipoInvestigacion](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EntidadOferenteID] [int] NOT NULL,
	[PersonaID] [int] NOT NULL,
	[RolEquipoTecnicoID] [int] NOT NULL,
	[Genero] [char](1) NOT NULL,
	[FechaNacimiento] [datetime] NOT NULL,
	[Ruc] [varchar](11) NOT NULL,
	[Domicilio] [varchar](250) NOT NULL,
	[TituloObtenido] [varchar](250) NOT NULL,
	[ProyectosRealizados] [varchar](500) NOT NULL,
	[ActividadesPrincipales] [varchar](500) NOT NULL,
	[DedicacionHoraria] [varchar](25) NOT NULL,
	[Extension_CV] [varchar](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NivelAprobacion]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NivelAprobacion](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EtapaCodigo] [nvarchar](10) NULL,
	[RolID] [int] NULL,
	[Correlativo] [int] NULL,
	[Estado] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Orden]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orden](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Correlativo] [int] NULL,
	[FechaOrden] [datetime] NULL,
	[RequerimientoID] [int] NULL,
	[RazonSocial] [nvarchar](150) NULL,
	[Direccion] [nvarchar](150) NULL,
	[RUC] [nvarchar](20) NULL,
	[Telefono] [nvarchar](20) NULL,
	[Monto] [real] NULL,
	[TipoCambio] [real] NULL,
	[Concepto] [nvarchar](4000) NULL,
	[PlazoEntrega] [nvarchar](500) NULL,
	[LugarEntrega] [nvarchar](500) NULL,
	[ValorVenta] [real] NULL,
	[IGV] [real] NULL,
	[UnidadEjecutoraNombres] [nvarchar](150) NULL,
	[UnidadEjecutoraDireccion] [nvarchar](150) NULL,
	[UnidadEjecutoraRUC] [nvarchar](11) NULL,
	[FechaConformidad] [datetime] NULL,
	[TipoOrden] [int] NULL,
	[Retencion] [real] NULL,
	[Estado] [int] NULL,
	[FechaRegistro] [datetime] NULL,
	[Relacion] [nvarchar](max) NULL,
	[CodigoProyecto] [nvarchar](150) NULL,
	[TipoProceso] [int] NULL,
	[CondicionPago] [nvarchar](400) NULL,
	[Penalidad] [nvarchar](400) NULL,
	[CuentaBancaria] [nvarchar](400) NULL,
	[CCI] [nvarchar](500) NULL,
	[PlazoServicio] [nvarchar](500) NULL,
	[CuentaDetraccion] [nvarchar](250) NULL,
	[ConformidadServicio] [nvarchar](250) NULL,
	[AreSubCategoriaID] [int] NULL,
	[Certificacion] [nvarchar](150) NULL,
	[Annio] [nvarchar](4) NULL,
	[Documento] [nvarchar](150) NULL,
	[SIAF] [nvarchar](50) NULL,
	[SituacionSIAFT] [int] NULL,
	[Situacion] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Pac]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pac](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PoaID] [int] NULL,
	[Estado] [int] NULL,
	[FechaRegistro] [datetime] NULL,
	[Situacion] [int] NULL,
	[Anio] [int] NULL,
	[Observacion] [varchar](500) NULL,
	[ProyectoID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PacDetalle]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PacDetalle](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AreSubCategoriaID] [int] NULL,
	[CategoriaID] [int] NULL,
	[Cantidad] [decimal](18, 0) NULL,
	[CostoEstimado] [decimal](18, 0) NULL,
	[MetodoAdquisicion] [char](10) NULL,
	[FuenteFinanciamientoID] [int] NULL,
	[MontoFuenteFinanciamiento] [decimal](18, 0) NULL,
	[FechaInicioPA] [varchar](200) NULL,
	[FechaFinPA] [varchar](200) NULL,
	[Observaciones] [varchar](500) NULL,
	[NumeracionPAC] [nvarchar](max) NULL,
	[Estado] [int] NULL,
	[FechaRegistro] [date] NULL,
	[PacID] [int] NULL,
	[Identificador] [nvarchar](max) NULL,
	[Codificacion] [nvarchar](max) NULL,
	[Situacion] [int] NULL,
	[TipoOrden] [int] NULL,
	[Correlativo] [varchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PacObservacion]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PacObservacion](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Observacion] [nvarchar](max) NULL,
	[PacID] [int] NULL,
	[Estado] [int] NULL,
	[AreSubCategoriaID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Padron]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Padron](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ClasificadorID] [int] NULL,
	[Nombres] [nvarchar](250) NULL,
	[ApellidoPaterno] [nvarchar](250) NULL,
	[ApellidoMaterno] [nvarchar](250) NULL,
	[DNI] [nvarchar](8) NULL,
	[FechaNacimiento] [datetime] NULL,
	[Sexo] [int] NULL,
	[CentroPoblado] [nvarchar](250) NULL,
	[IdiomaEspanol] [int] NULL,
	[IdiomaQuechua] [int] NULL,
	[IdiomaAymara] [int] NULL,
	[IdiomaOtro] [int] NULL,
	[BeneficiarioDirecto] [nvarchar](10) NULL,
	[NivelEducativo] [nvarchar](100) NULL,
	[ActividadEconomicaAgricultor] [int] NULL,
	[ActividadEconomicaGanadero] [int] NULL,
	[ActividadEconomicaForestal] [int] NULL,
	[ActividadEconomicaAgroIndustria] [int] NULL,
	[ActividadEconomicaOtro] [int] NULL,
	[ComunidadID] [int] NULL,
	[Codigo] [nvarchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PasoCritico]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PasoCritico](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProyectoID] [int] NOT NULL,
	[MesInicio] [int] NULL,
	[MesFin] [int] NULL,
	[Situacion] [int] NULL,
	[Estado] [int] NULL,
	[Correlativo] [int] NULL,
	[PorcentageProgramado] [decimal](10, 2) NULL,
	[MontoTotal] [real] NULL,
	[AvanceFisico] [decimal](10, 2) NULL,
	[AvanceFinanciero] [decimal](10, 2) NULL,
	[MontoTotalEjecutado] [real] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Pat]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pat](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProyectoID] [int] NULL,
	[Situacion] [int] NULL,
	[Estado] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Persona]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Persona](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ApellidoPaterno] [varchar](50) NOT NULL,
	[ApellidoMaterno] [varchar](50) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[NroDocumento] [varchar](20) NULL,
	[Telefono] [varchar](20) NULL,
	[Email] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PlanNegocios]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PlanNegocios](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InvestigadorID] [int] NOT NULL,
	[CaracteristicasProductos] [varchar](2100) NULL,
	[IdeaPlan] [varchar](2100) NULL,
	[CaracteristicasDemanda] [varchar](2100) NULL,
	[CaracteristicasOferta] [varchar](2100) NULL,
	[PrincipalesClientes] [varchar](2100) NULL,
	[ContribucionColaboradoras] [varchar](2100) NULL,
	[ResultadosEvaluacionEconomica] [varchar](2100) NULL,
	[Extension_AnexosEstadisticos] [varchar](7) NULL,
	[Extension_CartaIntencionCompra] [varchar](7) NULL,
	[Extension_ProyeccionVentas] [varchar](7) NULL,
	[Extension_AnalisisCostos] [varchar](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Poa]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Poa](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProyectoID] [int] NULL,
	[Situacion] [int] NULL,
	[Estado] [int] NULL,
	[Anno] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProduccionResiduoSolido]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProduccionResiduoSolido](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Mes] [nvarchar](2) NULL,
	[Cantidad] [real] NULL,
	[Tipo] [int] NULL,
	[FechaRegistro] [datetime] NULL,
	[Anno] [int] NULL,
	[InformeTecnicoFinancieroID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProductoPlanNegocios]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductoPlanNegocios](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PlanNegociosID] [int] NOT NULL,
	[Nombre] [varchar](250) NOT NULL,
	[UnidadMedida] [varchar](250) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Programa]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Programa](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](250) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProgramaTransversal]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProgramaTransversal](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](250) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Proyecto]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Proyecto](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InvestigadorID] [int] NOT NULL,
	[Fin] [varchar](1500) NULL,
	[Proposito] [varchar](1500) NULL,
	[DescripcionComponentesInvestigacion] [varchar](max) NULL,
	[DescripcionComponentesFortalecimiento] [varchar](max) NULL,
	[Total] [decimal](14, 5) NULL,
	[TotalFinanciamientoPnia] [decimal](14, 5) NULL,
	[TotalFinanciamientoAlianza] [decimal](14, 5) NULL,
	[Situacion] [int] NULL,
	[Avance] [decimal](10, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Rendicion]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Rendicion](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Correlativo] [int] NULL,
	[TipoGastoID] [int] NULL,
	[TransaccionID] [int] NULL,
	[FechaRegistro] [datetime] NULL,
	[Situacion] [int] NULL,
	[Estado] [int] NULL,
	[CodigoProyecto] [varchar](100) NULL,
	[Annio] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RendicionCajaChica]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RendicionCajaChica](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CajaChicaID] [int] NULL,
	[CodigoProyecto] [nvarchar](150) NULL,
	[SolicitanteEncargo] [nvarchar](150) NULL,
	[Cargo] [nvarchar](150) NULL,
	[FechaEncargo] [datetime] NULL,
	[SaldoAnterior] [real] NULL,
	[Total] [real] NULL,
	[Situacion] [int] NULL,
	[Estado] [int] NULL,
	[FechaRegistro] [datetime] NULL,
	[Correlativo] [int] NULL,
	[Annio] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RendicionEncargo]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RendicionEncargo](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoProyecto] [nvarchar](150) NULL,
	[EncargoID] [int] NULL,
	[SIAF] [nvarchar](150) NULL,
	[ComprobantePago] [nvarchar](150) NULL,
	[Situacion] [int] NULL,
	[Estado] [int] NULL,
	[FechaRegistro] [datetime] NULL,
	[Annio] [int] NULL,
	[Correlativo] [int] NULL,
	[Total] [real] NULL,
	[InformeMotivoEncargo] [nvarchar](max) NULL,
	[InformeDetallarActividadesEncargo] [nvarchar](max) NULL,
	[InformeDetallarLogrosEncargo] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RendicionViatico]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RendicionViatico](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoProyecto] [varchar](100) NULL,
	[ViaticoID] [int] NULL,
	[AnticipoRecibido] [int] NULL,
	[GastoSustentado] [int] NULL,
	[Reintegrar] [int] NULL,
	[Situacion] [int] NULL,
	[FechaRendicion] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RepresentanteLegal]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RepresentanteLegal](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PersonaID] [int] NOT NULL,
	[EntidadParticipanteID] [int] NOT NULL,
	[Cargo] [varchar](100) NOT NULL,
	[PartidaResolucion] [varchar](500) NULL,
	[RegistroPartidaResolucion] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Requerimiento]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Requerimiento](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoProyecto] [nvarchar](150) NULL,
	[Situacion] [int] NULL,
	[FechaRegistro] [datetime] NULL,
	[Estado] [int] NULL,
	[Referencia] [nvarchar](250) NULL,
	[Asunto] [nvarchar](250) NULL,
	[TipoRequerimiento] [int] NULL,
	[Documento] [nvarchar](150) NULL,
	[FechaSolicitud] [datetime] NULL,
	[Correlativo] [int] NULL,
	[Descripcion] [nvarchar](4000) NULL,
	[Annio] [int] NULL,
	[CUT] [nvarchar](150) NULL,
	[PasoCriticoID] [int] NULL,
	[Observacion] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Rol]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Rol](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RolEquipoTecnico]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RolEquipoTecnico](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](250) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RubroElegible]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RubroElegible](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](250) NOT NULL,
	[TipoRubroElegibleID] [int] NOT NULL,
	[LimiteSegunBase] [int] NULL,
	[LimiteSegunBaseConcurso] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Seguimiento]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Seguimiento](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProyectoCodigo] [nvarchar](100) NULL,
	[EtapaCodigo] [nvarchar](10) NULL,
	[NivelAprobacionCorrelativo] [int] NULL,
	[Situacion] [int] NULL,
	[Estado] [int] NULL,
	[Observacion] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Sheet3]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sheet3](
	[F1] [datetime] NULL,
	[N1] [nvarchar](255) NULL,
	[F2] [datetime] NULL,
	[Clase] [nvarchar](255) NULL,
	[Serie] [nvarchar](255) NULL,
	[N2] [nvarchar](255) NULL,
	[RUC] [nvarchar](255) NULL,
	[Social] [nvarchar](max) NULL,
	[Concepto] [nvarchar](max) NULL,
	[Importe] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SiafSecuencialProyecto]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SiafSecuencialProyecto](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CodigoProyecto] [nvarchar](150) NULL,
	[Secuencial] [int] NULL,
	[SecuencialOrdenServicioFactura] [int] NULL,
	[SecuencialOrdenServicioRH] [int] NULL,
	[SecuencialOrdenCompra] [int] NULL,
	[SecuencialCajaChica] [int] NULL,
	[SecuencialViatico] [int] NULL,
	[SecuencialEncargo] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Siaft]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Siaft](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Fase] [char](3) NULL,
	[Secuencial] [int] NULL,
	[Fecha] [datetime] NULL,
	[Monto] [real] NULL,
	[FechaProceso] [datetime] NULL,
	[Estado] [int] NULL,
	[CodigoProyecto] [nvarchar](50) NULL,
	[Orden] [nvarchar](20) NULL,
	[Moneda] [varchar](10) NULL,
	[TipoGastoID] [int] NULL,
	[Siaf] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Situacion]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Situacion](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [nvarchar](150) NULL,
	[Estado] [int] NULL,
	[Codigo] [int] NULL,
	[Sigla] [char](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SolicitudPago]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SolicitudPago](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Correlativo] [nvarchar](20) NULL,
	[JefeUnidad] [nvarchar](100) NULL,
	[NumeroResolucionJefatural] [nvarchar](100) NULL,
	[FechaSolicitud] [datetime] NULL,
	[ProyectoID] [int] NULL,
	[MontoTotal] [float] NULL,
	[Clasificacion] [nvarchar](50) NULL,
	[FechaRegistro] [datetime] NULL,
	[Estado] [int] NULL,
	[Memorando] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Tarea]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tarea](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ActividadID] [int] NULL,
	[Descripcion] [nvarchar](max) NULL,
	[Peso] [decimal](10, 2) NULL,
	[UnidadMedida] [nvarchar](255) NULL,
	[MetaFisica] [decimal](10, 2) NULL,
	[Correlativo] [int] NULL,
	[Situacion] [int] NULL,
	[MetaAvance] [decimal](10, 2) NULL,
	[MetaFisicaEjecutada] [decimal](10, 2) NULL,
	[ActividadPonderado] [decimal](10, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TareaObservacion]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TareaObservacion](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Observacion] [nvarchar](max) NULL,
	[TareaID] [int] NULL,
	[Estado] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Temporal_Prueba]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Temporal_Prueba](
	[CODIGO] [nvarchar](25) NULL,
	[P1] [real] NULL,
	[P1T] [real] NULL,
	[P2] [real] NULL,
	[P2T] [real] NULL,
	[P3] [real] NULL,
	[P3T] [real] NULL,
	[P4] [real] NULL,
	[P4T] [real] NULL,
	[P5] [real] NULL,
	[P5T] [real] NULL,
	[P6] [real] NULL,
	[P6T] [real] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TipoContratacion]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoContratacion](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](100) NULL,
	[Estado] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TipoDocumento]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoDocumento](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](300) NULL,
	[Estado] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TipoGasto]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoGasto](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](100) NULL,
	[Estado] [int] NULL,
	[Rendicion] [int] NULL,
	[Sigla] [nvarchar](300) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TipoImpacto]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoImpacto](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nomb] [nvarchar](250) NULL,
 CONSTRAINT [PK_TipoImpacto] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TipoInstitucion]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoInstitucion](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](500) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TipoInvestigacion]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoInvestigacion](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](250) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TipoRubroElegible]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoRubroElegible](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ubigeo]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ubigeo](
	[ID] [varchar](6) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UnidadEjecutora]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UnidadEjecutora](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](250) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UnidadOperativa]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UnidadOperativa](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](250) NULL,
	[UnidadEjecutoraID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuario](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](100) NOT NULL,
	[password] [varchar](100) NOT NULL,
	[CodigoVerificacion] [nvarchar](150) NULL,
	[status] [int] NOT NULL,
	[email] [varchar](500) NULL,
	[PersonaID] [int] NULL,
	[Tipo] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UsuarioProyecto]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsuarioProyecto](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UsuarioID] [int] NULL,
	[CodigoProyecto] [nvarchar](150) NULL,
	[Estado] [int] NULL,
	[FechaRegistro] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UsuarioRol]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsuarioRol](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UsuarioID] [int] NOT NULL,
	[RolID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Viatico]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Viatico](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](200) NULL,
	[Apellido] [varchar](200) NULL,
	[Dni] [char](8) NULL,
	[Cargo] [varchar](300) NULL,
	[PlanTrabajo] [nvarchar](max) NULL,
	[Regimen] [char](5) NULL,
	[TipoViaje] [varchar](1) NULL,
	[Destino] [varchar](100) NULL,
	[Salida] [datetime] NULL,
	[Regreso] [datetime] NULL,
	[FteFto] [varchar](200) NULL,
	[CuentaBancaria] [varchar](200) NULL,
	[LugarFecha] [varchar](200) NULL,
	[Correlativo] [int] NULL,
	[Situacion] [int] NULL,
	[Estado] [int] NULL,
	[FechaCreacion] [datetime] NULL,
	[CodigoPoa] [nvarchar](100) NULL,
	[CodigoProyecto] [nvarchar](150) NULL,
	[AreSubCategoriaID] [int] NULL,
	[ActividadID] [int] NULL,
	[ComponenteID] [int] NULL,
	[RequerimientoID] [int] NULL,
	[NroDias] [int] NULL,
	[AsigHoras] [int] NULL,
	[Annio] [int] NULL,
	[Documento] [nvarchar](150) NULL,
	[Monto] [real] NULL,
	[TipoCambio] [real] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [Indice]    Script Date: 18/04/2017 19:47:57 ******/
CREATE NONCLUSTERED INDEX [Indice] ON [dbo].[Actividad]
(
	[ID] ASC,
	[ComponenteID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [Campos]    Script Date: 18/04/2017 19:47:57 ******/
CREATE NONCLUSTERED INDEX [Campos] ON [dbo].[ActRubroElegible]
(
	[ActividadID] ASC,
	[RubroElegibleID] ASC,
	[ID] ASC,
	[UnidadMedida] ASC,
	[CostoUnitario] ASC,
	[MetaFisica] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [ActRubroElegibleID]    Script Date: 18/04/2017 19:47:57 ******/
CREATE NONCLUSTERED INDEX [ActRubroElegibleID] ON [dbo].[AreSubCategoria]
(
	[ActRubroElegibleID] ASC,
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [A]    Script Date: 18/04/2017 19:47:57 ******/
CREATE NONCLUSTERED INDEX [A] ON [dbo].[Componente]
(
	[ID] ASC,
	[PoaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [A]    Script Date: 18/04/2017 19:47:57 ******/
CREATE NONCLUSTERED INDEX [A] ON [dbo].[InformacionGeneral]
(
	[ID] ASC,
	[InvestigadorID] ASC,
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [A]    Script Date: 18/04/2017 19:47:57 ******/
CREATE NONCLUSTERED INDEX [A] ON [dbo].[Poa]
(
	[ID] ASC,
	[ProyectoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [A]    Script Date: 18/04/2017 19:47:57 ******/
CREATE NONCLUSTERED INDEX [A] ON [dbo].[Proyecto]
(
	[ID] ASC,
	[InvestigadorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Actividad]  WITH CHECK ADD FOREIGN KEY([ComponenteID])
REFERENCES [dbo].[Componente] ([ID])
GO
ALTER TABLE [dbo].[ActRubroElegible]  WITH CHECK ADD FOREIGN KEY([ActividadID])
REFERENCES [dbo].[Actividad] ([ID])
GO
ALTER TABLE [dbo].[ActRubroElegible]  WITH CHECK ADD FOREIGN KEY([RubroElegibleID])
REFERENCES [dbo].[RubroElegible] ([ID])
GO
ALTER TABLE [dbo].[AmbitoIntervencion]  WITH CHECK ADD FOREIGN KEY([DistritoID])
REFERENCES [dbo].[Ubigeo] ([ID])
GO
ALTER TABLE [dbo].[AmbitoIntervencion]  WITH CHECK ADD FOREIGN KEY([InformacionGeneralID])
REFERENCES [dbo].[InformacionGeneral] ([ID])
GO
ALTER TABLE [dbo].[AntecedenteEntidad]  WITH CHECK ADD FOREIGN KEY([EntidadParticipanteID])
REFERENCES [dbo].[EntidadParticipante] ([ID])
GO
ALTER TABLE [dbo].[AporteActividad]  WITH CHECK ADD FOREIGN KEY([ActividadID])
REFERENCES [dbo].[Actividad] ([ID])
GO
ALTER TABLE [dbo].[AporteActividadPC]  WITH CHECK ADD FOREIGN KEY([AporteActividadID])
REFERENCES [dbo].[AporteActividad] ([ID])
GO
ALTER TABLE [dbo].[AporteActividadPC]  WITH CHECK ADD FOREIGN KEY([PasoCriticoID])
REFERENCES [dbo].[PasoCritico] ([ID])
GO
ALTER TABLE [dbo].[AporteActRubroElegible]  WITH CHECK ADD FOREIGN KEY([ActRubroElegibleID])
REFERENCES [dbo].[ActRubroElegible] ([ID])
GO
ALTER TABLE [dbo].[AporteActRubroElegiblePC]  WITH CHECK ADD FOREIGN KEY([AporteActRubroElegibleID])
REFERENCES [dbo].[AporteActRubroElegible] ([ID])
GO
ALTER TABLE [dbo].[AporteActRubroElegiblePC]  WITH CHECK ADD FOREIGN KEY([PasoCriticoID])
REFERENCES [dbo].[PasoCritico] ([ID])
GO
ALTER TABLE [dbo].[AporteAreSubCategoria]  WITH CHECK ADD FOREIGN KEY([AreSubCategoriaID])
REFERENCES [dbo].[AreSubCategoria] ([ID])
GO
ALTER TABLE [dbo].[AporteAreSubCategoriaPC]  WITH CHECK ADD FOREIGN KEY([AporteAreSubCategoriaID])
REFERENCES [dbo].[AporteAreSubCategoria] ([ID])
GO
ALTER TABLE [dbo].[AporteAreSubCategoriaPC]  WITH CHECK ADD FOREIGN KEY([PasoCriticoID])
REFERENCES [dbo].[PasoCritico] ([ID])
GO
ALTER TABLE [dbo].[AporteComponente]  WITH CHECK ADD FOREIGN KEY([ComponenteID])
REFERENCES [dbo].[Componente] ([ID])
GO
ALTER TABLE [dbo].[AporteComponentePC]  WITH CHECK ADD FOREIGN KEY([AporteComponenteID])
REFERENCES [dbo].[AporteComponente] ([ID])
GO
ALTER TABLE [dbo].[AporteComponentePC]  WITH CHECK ADD FOREIGN KEY([PasoCriticoID])
REFERENCES [dbo].[PasoCritico] ([ID])
GO
ALTER TABLE [dbo].[AporteProyecto]  WITH CHECK ADD FOREIGN KEY([ProyectoID])
REFERENCES [dbo].[Proyecto] ([ID])
GO
ALTER TABLE [dbo].[AporteProyectoPC]  WITH CHECK ADD FOREIGN KEY([AporteProyectoID])
REFERENCES [dbo].[AporteProyecto] ([ID])
GO
ALTER TABLE [dbo].[AporteProyectoPC]  WITH CHECK ADD FOREIGN KEY([PasoCriticoID])
REFERENCES [dbo].[PasoCritico] ([ID])
GO
ALTER TABLE [dbo].[AreSubCategoria]  WITH CHECK ADD FOREIGN KEY([ActRubroElegibleID])
REFERENCES [dbo].[ActRubroElegible] ([ID])
GO
ALTER TABLE [dbo].[AreSubCategoriaObservacion]  WITH CHECK ADD FOREIGN KEY([AreSubCategoriaID])
REFERENCES [dbo].[AreSubCategoria] ([ID])
GO
ALTER TABLE [dbo].[CajaChica]  WITH CHECK ADD FOREIGN KEY([RequerimientoID])
REFERENCES [dbo].[Requerimiento] ([ID])
GO
ALTER TABLE [dbo].[Componente]  WITH CHECK ADD FOREIGN KEY([PoaID])
REFERENCES [dbo].[Poa] ([ID])
GO
ALTER TABLE [dbo].[CronogramaActividad]  WITH CHECK ADD FOREIGN KEY([ActividadID])
REFERENCES [dbo].[Actividad] ([ID])
GO
ALTER TABLE [dbo].[CronogramaActRubroElegible]  WITH CHECK ADD FOREIGN KEY([ActRubroElegibleID])
REFERENCES [dbo].[ActRubroElegible] ([ID])
GO
ALTER TABLE [dbo].[CronogramaAreSubCategoria]  WITH CHECK ADD FOREIGN KEY([AreSubCategoriaID])
REFERENCES [dbo].[AreSubCategoria] ([ID])
GO
ALTER TABLE [dbo].[CronogramaComponente]  WITH CHECK ADD FOREIGN KEY([ComponenteID])
REFERENCES [dbo].[Componente] ([ID])
GO
ALTER TABLE [dbo].[CronogramaProyecto]  WITH CHECK ADD FOREIGN KEY([ProyectoID])
REFERENCES [dbo].[Proyecto] ([ID])
GO
ALTER TABLE [dbo].[CronogramaTarea]  WITH CHECK ADD FOREIGN KEY([TareaID])
REFERENCES [dbo].[Tarea] ([ID])
GO
ALTER TABLE [dbo].[CultivoCrianza]  WITH CHECK ADD FOREIGN KEY([ProgramaID])
REFERENCES [dbo].[Programa] ([ID])
GO
ALTER TABLE [dbo].[DetalleCertificacionPresupuestal]  WITH CHECK ADD FOREIGN KEY([CertificacionPresupuestalID])
REFERENCES [dbo].[CertificacionPresupuestal] ([ID])
GO
ALTER TABLE [dbo].[DetalleCompromisoPresupuestal]  WITH CHECK ADD FOREIGN KEY([CompromisoPresupuestalID])
REFERENCES [dbo].[CompromisoPresupuestal] ([ID])
GO
ALTER TABLE [dbo].[DetalleOrden]  WITH CHECK ADD FOREIGN KEY([OrdenID])
REFERENCES [dbo].[Orden] ([ID])
GO
ALTER TABLE [dbo].[DetalleSolicitudPago]  WITH CHECK ADD FOREIGN KEY([SolicitudPagoID])
REFERENCES [dbo].[SolicitudPago] ([ID])
GO
ALTER TABLE [dbo].[DetalleViatico]  WITH CHECK ADD FOREIGN KEY([ViaticoID])
REFERENCES [dbo].[Viatico] ([ID])
GO
ALTER TABLE [dbo].[EjecucionFinancieraColaboradora]  WITH CHECK ADD FOREIGN KEY([InformeTecnicoFinancieroID])
REFERENCES [dbo].[InformeTecnicoFinanciero] ([ID])
GO
ALTER TABLE [dbo].[EjecucionMetaFisicaFinanciera]  WITH CHECK ADD FOREIGN KEY([InformeTecnicoFinancieroID])
REFERENCES [dbo].[InformeTecnicoFinanciero] ([ID])
GO
ALTER TABLE [dbo].[Encargo]  WITH CHECK ADD FOREIGN KEY([RequerimientoID])
REFERENCES [dbo].[Requerimiento] ([ID])
GO
ALTER TABLE [dbo].[EntidadOferente]  WITH CHECK ADD FOREIGN KEY([InvestigadorID])
REFERENCES [dbo].[Investigador] ([ID])
GO
ALTER TABLE [dbo].[EntidadParticipante]  WITH CHECK ADD FOREIGN KEY([InvestigadorID])
REFERENCES [dbo].[Investigador] ([ID])
GO
ALTER TABLE [dbo].[EntidadParticipante]  WITH CHECK ADD FOREIGN KEY([TipoInstitucionID])
REFERENCES [dbo].[TipoInstitucion] ([ID])
GO
ALTER TABLE [dbo].[Especie]  WITH CHECK ADD FOREIGN KEY([CultivoCrianzaID])
REFERENCES [dbo].[CultivoCrianza] ([ID])
GO
ALTER TABLE [dbo].[FlujoCajaProyectado]  WITH CHECK ADD FOREIGN KEY([ComponenteID])
REFERENCES [dbo].[Componente] ([ID])
GO
ALTER TABLE [dbo].[FondoRecibidoEntidad]  WITH CHECK ADD FOREIGN KEY([EntidadParticipanteID])
REFERENCES [dbo].[EntidadParticipante] ([ID])
GO
ALTER TABLE [dbo].[Hito]  WITH CHECK ADD FOREIGN KEY([InformeTecnicoFinancieroID])
REFERENCES [dbo].[InformeTecnicoFinanciero] ([ID])
GO
ALTER TABLE [dbo].[IndicadorPasoCritico]  WITH CHECK ADD FOREIGN KEY([PasoCriticoID])
REFERENCES [dbo].[PasoCritico] ([ID])
GO
ALTER TABLE [dbo].[InformacionGeneral]  WITH CHECK ADD FOREIGN KEY([DireccionLineaID])
REFERENCES [dbo].[DireccionLinea] ([ID])
GO
ALTER TABLE [dbo].[InformacionGeneral]  WITH CHECK ADD FOREIGN KEY([DistritoID])
REFERENCES [dbo].[Ubigeo] ([ID])
GO
ALTER TABLE [dbo].[InformacionGeneral]  WITH CHECK ADD FOREIGN KEY([EspecieID])
REFERENCES [dbo].[Especie] ([ID])
GO
ALTER TABLE [dbo].[InformacionGeneral]  WITH CHECK ADD FOREIGN KEY([InvestigadorID])
REFERENCES [dbo].[Investigador] ([ID])
GO
ALTER TABLE [dbo].[InformacionGeneral]  WITH CHECK ADD FOREIGN KEY([ProgramaTransversalID])
REFERENCES [dbo].[ProgramaTransversal] ([ID])
GO
ALTER TABLE [dbo].[InformacionGeneral]  WITH CHECK ADD FOREIGN KEY([TipoInvestigacionID])
REFERENCES [dbo].[TipoInvestigacion] ([ID])
GO
ALTER TABLE [dbo].[InformacionGeneral]  WITH CHECK ADD FOREIGN KEY([UnidadOperativaID])
REFERENCES [dbo].[UnidadOperativa] ([ID])
GO
ALTER TABLE [dbo].[InformeTecnicoFinanciero]  WITH CHECK ADD FOREIGN KEY([PasoCriticoID])
REFERENCES [dbo].[PasoCritico] ([ID])
GO
ALTER TABLE [dbo].[Investigador]  WITH CHECK ADD FOREIGN KEY([UsuarioID])
REFERENCES [dbo].[Usuario] ([ID])
GO
ALTER TABLE [dbo].[MarcoLogicoActividad]  WITH CHECK ADD FOREIGN KEY([ActividadID])
REFERENCES [dbo].[Actividad] ([ID])
GO
ALTER TABLE [dbo].[MarcoLogicoComponente]  WITH CHECK ADD FOREIGN KEY([ComponenteID])
REFERENCES [dbo].[Componente] ([ID])
GO
ALTER TABLE [dbo].[MarcoLogicoFinProyecto]  WITH CHECK ADD FOREIGN KEY([ProyectoID])
REFERENCES [dbo].[Proyecto] ([ID])
GO
ALTER TABLE [dbo].[MarcoLogicoPropositoProyecto]  WITH CHECK ADD FOREIGN KEY([ProyectoID])
REFERENCES [dbo].[Proyecto] ([ID])
GO
ALTER TABLE [dbo].[MiembroEquipoGestion]  WITH CHECK ADD FOREIGN KEY([EntidadParticipanteID])
REFERENCES [dbo].[EntidadParticipante] ([ID])
GO
ALTER TABLE [dbo].[MiembroEquipoGestion]  WITH CHECK ADD FOREIGN KEY([InvestigadorID])
REFERENCES [dbo].[Investigador] ([ID])
GO
ALTER TABLE [dbo].[MiembroEquipoGestion]  WITH CHECK ADD FOREIGN KEY([PersonaID])
REFERENCES [dbo].[Persona] ([ID])
GO
ALTER TABLE [dbo].[MiembroEquipoGestion]  WITH CHECK ADD FOREIGN KEY([RolEquipoTecnicoID])
REFERENCES [dbo].[RolEquipoTecnico] ([ID])
GO
ALTER TABLE [dbo].[MiembroEquipoInvestigacion]  WITH CHECK ADD FOREIGN KEY([EntidadOferenteID])
REFERENCES [dbo].[EntidadOferente] ([ID])
GO
ALTER TABLE [dbo].[MiembroEquipoInvestigacion]  WITH CHECK ADD FOREIGN KEY([PersonaID])
REFERENCES [dbo].[Persona] ([ID])
GO
ALTER TABLE [dbo].[MiembroEquipoInvestigacion]  WITH CHECK ADD FOREIGN KEY([RolEquipoTecnicoID])
REFERENCES [dbo].[RolEquipoTecnico] ([ID])
GO
ALTER TABLE [dbo].[Orden]  WITH CHECK ADD FOREIGN KEY([RequerimientoID])
REFERENCES [dbo].[Requerimiento] ([ID])
GO
ALTER TABLE [dbo].[Pac]  WITH CHECK ADD FOREIGN KEY([PoaID])
REFERENCES [dbo].[Poa] ([ID])
GO
ALTER TABLE [dbo].[PacDetalle]  WITH CHECK ADD FOREIGN KEY([CategoriaID])
REFERENCES [dbo].[Categoria] ([ID])
GO
ALTER TABLE [dbo].[PacDetalle]  WITH CHECK ADD FOREIGN KEY([PacID])
REFERENCES [dbo].[Pac] ([ID])
GO
ALTER TABLE [dbo].[PacObservacion]  WITH CHECK ADD FOREIGN KEY([PacID])
REFERENCES [dbo].[Pac] ([ID])
GO
ALTER TABLE [dbo].[PasoCritico]  WITH CHECK ADD FOREIGN KEY([ProyectoID])
REFERENCES [dbo].[Proyecto] ([ID])
GO
ALTER TABLE [dbo].[Pat]  WITH CHECK ADD FOREIGN KEY([ProyectoID])
REFERENCES [dbo].[Proyecto] ([ID])
GO
ALTER TABLE [dbo].[PlanNegocios]  WITH CHECK ADD FOREIGN KEY([InvestigadorID])
REFERENCES [dbo].[Investigador] ([ID])
GO
ALTER TABLE [dbo].[Poa]  WITH CHECK ADD FOREIGN KEY([ProyectoID])
REFERENCES [dbo].[Proyecto] ([ID])
GO
ALTER TABLE [dbo].[ProductoPlanNegocios]  WITH CHECK ADD FOREIGN KEY([PlanNegociosID])
REFERENCES [dbo].[PlanNegocios] ([ID])
GO
ALTER TABLE [dbo].[Proyecto]  WITH CHECK ADD FOREIGN KEY([InvestigadorID])
REFERENCES [dbo].[Investigador] ([ID])
GO
ALTER TABLE [dbo].[RepresentanteLegal]  WITH CHECK ADD FOREIGN KEY([EntidadParticipanteID])
REFERENCES [dbo].[EntidadParticipante] ([ID])
GO
ALTER TABLE [dbo].[RepresentanteLegal]  WITH CHECK ADD FOREIGN KEY([PersonaID])
REFERENCES [dbo].[Persona] ([ID])
GO
ALTER TABLE [dbo].[RubroElegible]  WITH CHECK ADD FOREIGN KEY([TipoRubroElegibleID])
REFERENCES [dbo].[TipoRubroElegible] ([ID])
GO
ALTER TABLE [dbo].[Tarea]  WITH CHECK ADD FOREIGN KEY([ActividadID])
REFERENCES [dbo].[Actividad] ([ID])
GO
ALTER TABLE [dbo].[TareaObservacion]  WITH CHECK ADD FOREIGN KEY([TareaID])
REFERENCES [dbo].[Tarea] ([ID])
GO
ALTER TABLE [dbo].[UnidadOperativa]  WITH CHECK ADD FOREIGN KEY([UnidadEjecutoraID])
REFERENCES [dbo].[UnidadEjecutora] ([ID])
GO
ALTER TABLE [dbo].[UsuarioProyecto]  WITH CHECK ADD FOREIGN KEY([UsuarioID])
REFERENCES [dbo].[Usuario] ([ID])
GO
ALTER TABLE [dbo].[UsuarioRol]  WITH CHECK ADD FOREIGN KEY([RolID])
REFERENCES [dbo].[Rol] ([ID])
GO
ALTER TABLE [dbo].[UsuarioRol]  WITH CHECK ADD FOREIGN KEY([UsuarioID])
REFERENCES [dbo].[Usuario] ([ID])
GO
ALTER TABLE [dbo].[Viatico]  WITH CHECK ADD FOREIGN KEY([RequerimientoID])
REFERENCES [dbo].[Requerimiento] ([ID])
GO
/****** Object:  StoredProcedure [dbo].[SP_COMPROMISO_ADMINISTRATIVO]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_COMPROMISO_ADMINISTRATIVO] 
	-- Add the parameters for the stored procedure here
	@Anio varchar(255),
	@Mes varchar(255),
	@Ruc varchar(255),
	@Secuencia varchar(255),
	@TipoDoc varchar(255),
	@NumDoc varchar(255),
	@FechDoc varchar(255),
	@Motivo varchar(255),
	@MontoTotal decimal(10,5),
	@MontoIgv decimal(10,5),
	@Clasificador1 varchar(255),
	@Monto1 decimal(10,5),
	@Clasificador2 varchar(255),
	@Monto2 decimal(10,5),
	@Poa decimal(10,5),
	@a varchar(255) OUTPUT
AS
BEGIN 
	SET NOCOUNT ON;
	DECLARE @r varchar(255);EXECUTE('begin SIAF_INTERFASE.transf_datos_comp_pip2 (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); end;',@Anio, @Mes, @Ruc, @Secuencia,@TipoDoc,@NumDoc,@FechDoc,@Motivo,@MontoTotal,@MontoIgv,@Clasificador1,@Monto1,@Clasificador2,@Monto2,@Poa,@r output) AT ORACLE;
	set @a=@r;
	return @a;
end
GO
/****** Object:  StoredProcedure [dbo].[SP_COMPROMISO_ANUAL]    Script Date: 18/04/2017 19:47:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_COMPROMISO_ANUAL] 
	-- Add the parameters for the stored procedure here
	@CodigoDocumento varchar(255),
	@NumeroDocumento varchar(255),
	@FechaDocumento varchar(255),
	@AnioDocumento varchar(255),
	@ConceptoDocumento varchar(255),
	@TipoID varchar(255),
	@RUCDocumento varchar(255),
	@FuenteFinanciamiento varchar(255),
	@TipoCambioDocumento varchar(255),
	@SiafCertificado varchar(255),
	@SiafSecuencial varchar(255),
	@SiafCorrelativo varchar(255),
	@SiafCertificadof varchar(255),
	@InterfaceSecuencial varchar(255),
	@var varchar(255),
	@TipoFinanciamiento varchar(255),
	@Correlativo varchar(255),
	@MetaFinanciera char(4),
	@MontoTotal decimal(10,5),
	@Clasificador varchar(255),
	@MontoTotal1 decimal(10,5),
	@Clasificador1 varchar(255),
	@a varchar(255) OUTPUT
AS
BEGIN 
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @r varchar(255);EXECUTE('begin SIAF_INTERFASE.TRANSFERIR_DATOS_CERT_PIP2 (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); end;',@CodigoDocumento, @NumeroDocumento, @FechaDocumento, @AnioDocumento,@ConceptoDocumento,@TipoID,@RUCDocumento,@FuenteFinanciamiento,@TipoCambioDocumento,@SiafCertificado,@SiafSecuencial,@SiafCorrelativo,@SiafCertificadof,@InterfaceSecuencial,@var,@TipoFinanciamiento,@Correlativo,@MetaFinanciera,@MontoTotal,@Clasificador,@MontoTotal1,@Clasificador1, @r output) AT ORACLE;set @a=@r;return @a;
end
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'GastoElegible  -> 2 NO ELEGIBLE   1 ELEGIBLE' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CompromisoPresupuestal'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'indicadorPniaID     1 -> Evento   2 -> Experimento' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MarcoLogicoActividad'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Situacion para todas las tablas
0 -> Iniciado
1 -> Aprobado
2 -> Pendiente
3 -> Observado
4 -> Negado
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Proyecto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 Iniciado
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Seguimiento'
GO
USE [master]
GO
ALTER DATABASE [PIP2] SET  READ_WRITE 
GO
