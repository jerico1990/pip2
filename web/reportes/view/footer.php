<!-- Mainly scripts -->
<script src="js/jquery-2.1.1.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="js/inspinia.js"></script>
<script src="js/plugins/pace/pace.min.js"></script>

<script src="js/operaciones-varias.js"></script>

<!-- Graficos -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<!--<script src="js/data-grafico.js"></script>-->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<!-- Aca coloco los graficos -->
<script type="text/javascript">
    $(function () {
    // Create the chart
    Highcharts.chart('container', {
        chart: {
            type: 'pie'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: 'Fuente: Información del PIP2'
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '{point.name}: {point.y}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y} Subproyectos</b><br/>'
        },
        series: [{
            name: 'Sub Proyectos',
            colorByPoint: true,
            data: [{
                name: '<?php echo $f01[0]->Estacion;?>',
                y: <?php echo $f01[0]->Proyecto;?>,
                drilldown: 'Estacion01'
            }, {
                name: '<?php echo $f01[1]->Estacion;?>',
                y: <?php echo $f01[1]->Proyecto;?>,
                drilldown: 'Estacion02'
            }, {
                name: '<?php echo $f01[2]->Estacion;?>',
                y: <?php echo $f01[2]->Proyecto;?>,
                drilldown: 'Estacion03'
            }, {
                name: '<?php echo $f01[3]->Estacion;?>',
                y: <?php echo $f01[3]->Proyecto;?>,
                drilldown: 'Estacion04'
            }, {
                name: '<?php echo $f01[4]->Estacion;?>',
                y: <?php echo $f01[4]->Proyecto;?>,
                drilldown: 'Estacion05'
            }, {
                name: '<?php echo $f01[5]->Estacion;?>',
                y: <?php echo $f01[5]->Proyecto;?>,
                drilldown: 'Estacion06'
            },{
                name: '<?php echo $f01[6]->Estacion;?>',
                y: <?php echo $f01[6]->Proyecto;?>,
                drilldown: 'Estacion07'
            }]
        }],
        drilldown: {
            series: [{
                name: '<?php echo $f01[0]->Estacion;?>',
                id: 'Estacion01',
                data: [
                    ['Proyectos - Biotecnología y Recursos Genéticos', <?php echo $a01[0]->Proyectos; ?>],
                    ['Proyectos - Cambio Climático y Sostenibilidad', <?php echo $a02[0]->Proyectos; ?>],
                    ['Proyectos - Transferencia Tecnológica', <?php echo $a03[0]->Proyectos; ?>],
                    ['Proyectos - Manejo Post Cosecha', <?php echo $a04[0]->Proyectos; ?>]
                ]
            }, {
                name: '<?php echo $f01[1]->Estacion;?>',
                id: 'Estacion02',
                data: [
                    ['Proyectos - Biotecnología y Recursos Genéticos', <?php echo $b01[0]->Proyectos; ?>],
                    ['Proyectos - Cambio Climático y Sostenibilidad', <?php echo $b02[0]->Proyectos; ?>],
                    ['Proyectos - Transferencia Tecnológica', <?php echo $b03[0]->Proyectos; ?>],
                    ['Proyectos - Manejo Post Cosecha', <?php echo $b04[0]->Proyectos; ?>]
                ]
            }, {
                name: '<?php echo $f01[2]->Estacion;?>',
                id: 'Estacion03',
                data: [
                    ['Proyectos - Biotecnología y Recursos Genéticos', <?php echo $c01[0]->Proyectos; ?>],
                    ['Proyectos - Cambio Climático y Sostenibilidad', <?php echo $c02[0]->Proyectos; ?>],
                    ['Proyectos - Transferencia Tecnológica', <?php echo $c03[0]->Proyectos; ?>],
                    ['Proyectos - Manejo Post Cosecha', <?php echo $c04[0]->Proyectos; ?>]
                ]
            }, {
                name: '<?php echo $f01[3]->Estacion;?>',
                id: 'Estacion04',
                data: [
                    ['Proyectos - Biotecnología y Recursos Genéticos', <?php echo $d01[0]->Proyectos; ?>],
                    ['Proyectos - Cambio Climático y Sostenibilidad', <?php echo $d02[0]->Proyectos; ?>],
                    ['Proyectos - Transferencia Tecnológica', <?php echo $d03[0]->Proyectos; ?>],
                    ['Proyectos - Manejo Post Cosecha', <?php echo $d04[0]->Proyectos; ?>]
                ]
            },{
                name: '<?php echo $f01[4]->Estacion;?>',
                id: 'Estacion05',
                data: [
                    ['Proyectos - Biotecnología y Recursos Genéticos', <?php echo $e01[0]->Proyectos; ?>],
                    ['Proyectos - Cambio Climático y Sostenibilidad', <?php echo $e02[0]->Proyectos; ?>],
                    ['Proyectos - Transferencia Tecnológica', <?php echo $e03[0]->Proyectos; ?>],
                    ['Proyectos - Manejo Post Cosecha', <?php echo $e04[0]->Proyectos; ?>]
                ]
            },{
                name: '<?php echo $f01[5]->Estacion;?>',
                id: 'Estacion06',
                data: [
                    ['Proyectos - Biotecnología y Recursos Genéticos', <?php echo $ff01[0]->Proyectos; ?>],
                    ['Proyectos - Cambio Climático y Sostenibilidad', <?php echo $ff02[0]->Proyectos; ?>],
                    ['Proyectos - Transferencia Tecnológica', <?php echo $ff03[0]->Proyectos; ?>],
                    ['Proyectos - Manejo Post Cosecha', <?php echo $ff04[0]->Proyectos; ?>]
                ]
            },{
                name: '<?php echo $f01[6]->Estacion;?>',
                id: 'Estacion07',
                data: [
                    ['Proyectos - Biotecnología y Recursos Genéticos', <?php echo $g01[0]->Proyectos; ?>],
                    ['Proyectos - Cambio Climático y Sostenibilidad', <?php echo $g02[0]->Proyectos; ?>],
                    ['Proyectos - Transferencia Tecnológica', <?php echo $g03[0]->Proyectos; ?>],
                    ['Proyectos - Manejo Post Cosecha', <?php echo $g04[0]->Proyectos; ?>]
                ]
            }]
        }
    });
});
		</script>