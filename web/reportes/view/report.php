<?php 
	switch ($view) 
	{
		case $var0001:
			/**
			 * Colocamos varios reportes de índole general
			*/
			$consulta="SELECT dbo.UnidadEjecutora.ID,dbo.UnidadEjecutora.Nombre AS Estacion,Count(dbo.Investigador.ID) AS Proyecto
			FROM dbo.Investigador
			INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID
			GROUP BY dbo.UnidadEjecutora.ID,dbo.UnidadEjecutora.Nombre";
			$f01=$db->dbSelect($consulta);

			/**
			 * Monto total cofinanciado
			 */
			$consulta="SELECT Sum(dbo.AreSubCategoria.CostoUnitario*dbo.AreSubCategoria.MetaFisica) AS TotalCofinanciado
			FROM dbo.AreSubCategoria
			INNER JOIN dbo.ActRubroElegible ON dbo.AreSubCategoria.ActRubroElegibleID = dbo.ActRubroElegible.ID INNER JOIN dbo.Actividad ON dbo.ActRubroElegible.ActividadID = dbo.Actividad.ID INNER JOIN dbo.Componente ON dbo.Actividad.ComponenteID = dbo.Componente.ID INNER JOIN dbo.Poa ON dbo.Componente.PoaID = dbo.Poa.ID INNER JOIN dbo.Proyecto ON dbo.Poa.ProyectoID = dbo.Proyecto.ID INNER JOIN dbo.Investigador ON dbo.Proyecto.InvestigadorID = dbo.Investigador.ID";
			$f03=$db->dbSelect($consulta);
			/**
			 * Total Subproyectos
			 */
			$consulta="SELECT COUNT(dbo.Investigador.ID) AS TotalSubproyecto FROM dbo.Investigador";
			$f04=$db->dbSelect($consulta);

			//Estacion experimental andenes
			$consulta="SELECT Count(dbo.Investigador.ID) AS Proyectos FROM dbo.Investigador INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID
			WHERE dbo.UnidadEjecutora.ID = 1 AND dbo.InformacionGeneral.ProgramaTransversalID = 1";
			$a01=$db->dbSelect($consulta);
			
			$consulta="SELECT Count(dbo.Investigador.ID) AS Proyectos FROM dbo.Investigador INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID
			WHERE dbo.UnidadEjecutora.ID = 1 AND dbo.InformacionGeneral.ProgramaTransversalID = 2";
			$a02=$db->dbSelect($consulta);
			$consulta="SELECT Count(dbo.Investigador.ID) AS Proyectos FROM dbo.Investigador INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID
			WHERE dbo.UnidadEjecutora.ID = 1 AND dbo.InformacionGeneral.ProgramaTransversalID = 3";
			$a03=$db->dbSelect($consulta);
			$consulta="SELECT Count(dbo.Investigador.ID) AS Proyectos FROM dbo.Investigador INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID
			WHERE dbo.UnidadEjecutora.ID = 1 AND dbo.InformacionGeneral.ProgramaTransversalID = 4";
			$a04=$db->dbSelect($consulta);
			//Estacion experimental Porvenir
			$consulta="SELECT Count(dbo.Investigador.ID) AS Proyectos FROM dbo.Investigador INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID
			WHERE dbo.UnidadEjecutora.ID = 2 AND dbo.InformacionGeneral.ProgramaTransversalID = 1";
			$b01=$db->dbSelect($consulta);
			$consulta="SELECT Count(dbo.Investigador.ID) AS Proyectos FROM dbo.Investigador INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID
			WHERE dbo.UnidadEjecutora.ID = 2 AND dbo.InformacionGeneral.ProgramaTransversalID = 2";
			$b02=$db->dbSelect($consulta);
			$consulta="SELECT Count(dbo.Investigador.ID) AS Proyectos FROM dbo.Investigador INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID
			WHERE dbo.UnidadEjecutora.ID = 2 AND dbo.InformacionGeneral.ProgramaTransversalID = 3";
			$b03=$db->dbSelect($consulta);
			$consulta="SELECT Count(dbo.Investigador.ID) AS Proyectos FROM dbo.Investigador INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID
			WHERE dbo.UnidadEjecutora.ID = 2 AND dbo.InformacionGeneral.ProgramaTransversalID = 4";
			$b04=$db->dbSelect($consulta);
			//Estacion experimental Illpa
			$consulta="SELECT Count(dbo.Investigador.ID) AS Proyectos FROM dbo.Investigador INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID
			WHERE dbo.UnidadEjecutora.ID = 3 AND dbo.InformacionGeneral.ProgramaTransversalID = 1";
			$c01=$db->dbSelect($consulta);
			$consulta="SELECT Count(dbo.Investigador.ID) AS Proyectos FROM dbo.Investigador INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID
			WHERE dbo.UnidadEjecutora.ID = 3 AND dbo.InformacionGeneral.ProgramaTransversalID = 2";
			$c02=$db->dbSelect($consulta);
			$consulta="SELECT Count(dbo.Investigador.ID) AS Proyectos FROM dbo.Investigador INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID
			WHERE dbo.UnidadEjecutora.ID = 3 AND dbo.InformacionGeneral.ProgramaTransversalID = 3";
			$c03=$db->dbSelect($consulta);
			$consulta="SELECT Count(dbo.Investigador.ID) AS Proyectos FROM dbo.Investigador INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID
			WHERE dbo.UnidadEjecutora.ID = 3 AND dbo.InformacionGeneral.ProgramaTransversalID = 4";
			$c04=$db->dbSelect($consulta);
			//Estacion experimental Pucallpa
			$consulta="SELECT Count(dbo.Investigador.ID) AS Proyectos FROM dbo.Investigador INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID
			WHERE dbo.UnidadEjecutora.ID = 4 AND dbo.InformacionGeneral.ProgramaTransversalID = 1";
			$d01=$db->dbSelect($consulta);
			$consulta="SELECT Count(dbo.Investigador.ID) AS Proyectos FROM dbo.Investigador INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID
			WHERE dbo.UnidadEjecutora.ID = 4 AND dbo.InformacionGeneral.ProgramaTransversalID = 2";
			$d02=$db->dbSelect($consulta);
			$consulta="SELECT Count(dbo.Investigador.ID) AS Proyectos FROM dbo.Investigador INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID
			WHERE dbo.UnidadEjecutora.ID = 4 AND dbo.InformacionGeneral.ProgramaTransversalID = 3";
			$d03=$db->dbSelect($consulta);
			$consulta="SELECT Count(dbo.Investigador.ID) AS Proyectos FROM dbo.Investigador INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID
			WHERE dbo.UnidadEjecutora.ID = 4 AND dbo.InformacionGeneral.ProgramaTransversalID = 4";
			$d04=$db->dbSelect($consulta);
			//Estacion experimental Santa Ana
			$consulta="SELECT Count(dbo.Investigador.ID) AS Proyectos FROM dbo.Investigador INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID
			WHERE dbo.UnidadEjecutora.ID = 5 AND dbo.InformacionGeneral.ProgramaTransversalID = 1";
			$e01=$db->dbSelect($consulta);
			$consulta="SELECT Count(dbo.Investigador.ID) AS Proyectos FROM dbo.Investigador INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID
			WHERE dbo.UnidadEjecutora.ID = 5 AND dbo.InformacionGeneral.ProgramaTransversalID = 2";
			$e02=$db->dbSelect($consulta);
			$consulta="SELECT Count(dbo.Investigador.ID) AS Proyectos FROM dbo.Investigador INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID
			WHERE dbo.UnidadEjecutora.ID = 5 AND dbo.InformacionGeneral.ProgramaTransversalID = 3";
			$e03=$db->dbSelect($consulta);
			$consulta="SELECT Count(dbo.Investigador.ID) AS Proyectos FROM dbo.Investigador INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID
			WHERE dbo.UnidadEjecutora.ID = 5 AND dbo.InformacionGeneral.ProgramaTransversalID = 4";
			$e04=$db->dbSelect($consulta);
			//Estacion experimental Vista Florida
			$consulta="SELECT Count(dbo.Investigador.ID) AS Proyectos FROM dbo.Investigador INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID
			WHERE dbo.UnidadEjecutora.ID = 6 AND dbo.InformacionGeneral.ProgramaTransversalID = 1";
			$ff01=$db->dbSelect($consulta);
			$consulta="SELECT Count(dbo.Investigador.ID) AS Proyectos FROM dbo.Investigador INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID
			WHERE dbo.UnidadEjecutora.ID = 6 AND dbo.InformacionGeneral.ProgramaTransversalID = 2";
			$ff02=$db->dbSelect($consulta);
			$consulta="SELECT Count(dbo.Investigador.ID) AS Proyectos FROM dbo.Investigador INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID
			WHERE dbo.UnidadEjecutora.ID = 6 AND dbo.InformacionGeneral.ProgramaTransversalID = 3";
			$ff03=$db->dbSelect($consulta);
			$consulta="SELECT Count(dbo.Investigador.ID) AS Proyectos FROM dbo.Investigador INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID
			WHERE dbo.UnidadEjecutora.ID = 6 AND dbo.InformacionGeneral.ProgramaTransversalID = 4";
			$ff04=$db->dbSelect($consulta);
			//Estacion experimental Sede central
			$consulta="SELECT Count(dbo.Investigador.ID) AS Proyectos FROM dbo.Investigador INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID
			WHERE dbo.UnidadEjecutora.ID = 7 AND dbo.InformacionGeneral.ProgramaTransversalID = 1";
			$g01=$db->dbSelect($consulta);
			$consulta="SELECT Count(dbo.Investigador.ID) AS Proyectos FROM dbo.Investigador INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID
			WHERE dbo.UnidadEjecutora.ID = 7 AND dbo.InformacionGeneral.ProgramaTransversalID = 2";
			$g02=$db->dbSelect($consulta);
			$consulta="SELECT Count(dbo.Investigador.ID) AS Proyectos FROM dbo.Investigador INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID
			WHERE dbo.UnidadEjecutora.ID = 7 AND dbo.InformacionGeneral.ProgramaTransversalID = 3";
			$g03=$db->dbSelect($consulta);
			$consulta="SELECT Count(dbo.Investigador.ID) AS Proyectos FROM dbo.Investigador INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID
			WHERE dbo.UnidadEjecutora.ID = 7 AND dbo.InformacionGeneral.ProgramaTransversalID = 4";
			$g04=$db->dbSelect($consulta);
			

			/**
			 * a.- Reporte por Estación Experimental
			 */
			echo "<div class='row'>";
				echo "<div class='col-md-12'>";
					echo "<h4>Número de Sub Proyectos por Unidad Ejecutora</h4>";
					echo "<div id='container' style='min-width: 300px; height: 350px; margin: 0 auto'></div>";
					echo "<div class='table-responsive'>";
						echo "<table class='table table-bordered'>";
							echo "<thead>";
								echo "<tr>";
									echo "<th class='text-center'><small>#</small></th>";
									echo "<th><small>Estación Experimental</small></th>";
									echo "<th width='15%' class='text-center'><small>N° de Sub Proyectos</small></th>";
									echo "<th width='15%' class='text-center'><small>Monto PNIA (S/)</small></th>";
									echo "<th width='5%'>#</th>";
								echo "</tr>";
							echo "</thead>";
							echo "<tbody>";
								for($i=0;$i<count($f01);$i++)
								{
									$consulta="SELECT Sum(dbo.AreSubCategoria.CostoUnitario*dbo.AreSubCategoria.MetaFisica) AS Total
									FROM dbo.Investigador
									INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID INNER JOIN dbo.Proyecto ON dbo.Proyecto.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.Poa ON dbo.Poa.ProyectoID = dbo.Proyecto.ID INNER JOIN dbo.Componente ON dbo.Componente.PoaID = dbo.Poa.ID INNER JOIN dbo.Actividad ON dbo.Actividad.ComponenteID = dbo.Componente.ID INNER JOIN dbo.ActRubroElegible ON dbo.ActRubroElegible.ActividadID = dbo.Actividad.ID INNER JOIN dbo.AreSubCategoria ON dbo.AreSubCategoria.ActRubroElegibleID = dbo.ActRubroElegible.ID
									WHERE dbo.UnidadEjecutora.ID = '".$f01[$i]->ID."'
									GROUP BY dbo.UnidadEjecutora.Nombre";
									$f02=$db->dbSelect($consulta);
									echo "<tr>";
										echo "<td class='text-center'><small>".($i+1)."</small></td>";
										echo "<td>".$f01[$i]->Estacion."</td>";
										echo "<td class='text-right'>".number_format($f01[$i]->Proyecto)."</td>";
										echo "<td class='text-right'>"; 
											if(isset($f02[0]->Total))
											{
												echo number_format($f02[0]->Total);
											} 
										echo "</td>";
										echo "<td class='text-center'><a href='#' class='open-Modal btn btn-default btn-sm' data-toggle='modal' data-target='#myModal1' data-id='".$f01[$i]->ID."'><i class='fa fa-search-plus'></i></a></td>";
									echo "</tr>";
								}
							echo "</tbody>";
							echo "<tfoot>";
								echo "<tr>";
									echo "<th colspan='2'>TOTALES</th>";
									echo "<th class='text-right'>".number_format($f04[0]->TotalSubproyecto)."</th>";
									echo "<th class='text-right'>".number_format($f03[0]->TotalCofinanciado)."</th>";
									echo "<th class='text-center'>-</th>";
								echo "</tr>";
							echo "</tfoot>";
						echo "</table>";
					echo "</div>";
					
				echo "</div>";
			echo "</div>";
			echo "<div class='modal inmodal fade' id='myModal1' tabindex='-1' role='dialog'  aria-hidden='true'>";
				echo "<div class='modal-dialog modal-lg'>";
					echo "<div class='modal-content'>";
						echo "<div class='modal-header'>"; 
							echo "<button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Cerrar</span></button>";
							echo "<h4>Proyectos atendidos por Tipo de Cultivo/Crianza</h4>";
						echo "</div>";
						echo "<div class='modal-body'>";
							/*$consulta="SELECT dbo.CultivoCrianza.Nombre AS Categoria,dbo.Especie.Nombre AS Producto,Count(dbo.Investigador.ID) AS Proyectos
							FROM dbo.Investigador
							INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.Especie ON dbo.InformacionGeneral.EspecieID = dbo.Especie.ID INNER JOIN dbo.CultivoCrianza ON dbo.CultivoCrianza.ID = dbo.Especie.CultivoCrianzaID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID
							WHERE dbo.UnidadEjecutora.ID = 1
							GROUP BY dbo.CultivoCrianza.Nombre, dbo.Especie.Nombre
							ORDER BY Categoria ASC, Producto ASC";
							$f05=$db->dbSelect($consulta);
							echo "<input type='text' name='DNI' id='id-estacion' value=''/>";
							echo "<div class='table-responsive'>";
								echo "<table class='table table-bordered'>";
									echo "<thead>";
										echo "<tr>";
											echo "<th width='5%' class='text-center'><small>#</small></th>";
											echo "<th class='text-center'><small>Crianza o Cultivo</small></th>";
											echo "<th class='text-center'><small>Producto</small></th>";
											echo "<th width='15%' class='text-center'><small>N° de SubProyectos</small></th>";
										echo "</tr>";
									echo "</thead>";
									echo "<tbody>";
										for($i=0;$i<count($f05);$i++)
										{
											echo "<tr>";
												echo "<td class='text-center'>".($i+1)."</td>";
												echo "<td class='text-center'>".$f05[$i]->Categoria."</td>";
												echo "<td class='text-center'>".$f05[$i]->Producto."</td>";
												echo "<td class='text-right'>".number_format($f05[$i]->Proyectos)."</td>";
											echo "</tr>";
										}
									echo "</tbody>";
								echo "</table>";
							echo "</div>";*/
							echo "<div id='resultado'></div>";

						echo "</div>";
						echo "<div class='modal-footer'>";
							echo "<button type='button' class='btn btn-white' data-dismiss='modal'>Cerrar</button>";
						echo "</div>";
					echo "</div>";
				echo "</div>";
			echo "</div>";
		break;
		
		default:
			echo "Nada que mostrar";
		break;
	}
?>