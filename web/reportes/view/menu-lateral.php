<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <?php include("user-data.php"); ?>
                <div class="logo-element">SysPIP2</div>
            </li>
            <li class="active"><a href="index.php"><i class="fa fa-database"></i> <span class="nav-label">Reportes</span></a></li>
        </ul>

    </div>
</nav>