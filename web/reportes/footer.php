<div class="footer">
    <div class="pull-right">Correo electrónico: info@pnia.gob.pe</div>
    <div><strong>Copyright</strong> Programa Nacional de Innovación Agraria - PNIA &copy; <?php echo date('Y'); ?></div>
</div>