<?php 
require_once('include/config.php');
if($db->dbConnect())
{
?>
<!DOCTYPE html>
<html>
<head>
    <?php include("view/header.php"); ?>
</head>
<body>
<!-- Fin de llamada -->
<div id="wrapper">
    <?php include("view/menu-lateral.php"); ?>
    <div id="page-wrapper" class="gray-bg">
        <?php include("view/menu-superior.php"); ?>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Inicio del contenido -->
                    <div class="ibox">
                        <div class="ibox-content">
                            <?php include("view/report.php"); ?>
                        </div>
                    </div>
                    <!-- Fin del contenido -->
                </div>
            </div>
        </div>
    </div>
<?php include("view/footer.php"); ?>
</body>
</html>
<?php 
} 
?>
