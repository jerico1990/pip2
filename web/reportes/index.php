<?php require_once('include/variables.php'); ?>
<!DOCTYPE html>
<html>
<head>
    <?php include("view/header.php"); ?>
</head>
<body>
<div id="wrapper">
    <?php include("view/menu-lateral.php"); ?>
    <div id="page-wrapper" class="gray-bg">
        <?php include("view/menu-superior.php"); ?>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Inicio del contenido -->
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h4>Módulo de reportes del sistema PIP2</h4>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th width="5%" class="text-center">#</th>
                                            <th class="text-center">Nombre del reporte</th>
                                            <th width="5%" class="text-center">#</th>
                                            <th width="5%" class="text-center">#</th>
                                            <th width="5%" class="text-center">#</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center">1</td>
                                            <td>Número de Sub Proyectos atendidos por Unidad ejecutora</td>
                                            <td class="text-center"><a href="reporte.php?view=<?php echo $var0001;?>" class="btn btn-default btn-sm"><i class="fa fa-search"></i></a></td>
                                            <td class="text-center"><a href="" class="btn btn-default btn-sm"><i class="fa fa-file-pdf-o"></i></a></td>
                                            <td class="text-center"><a href="" class="btn btn-default btn-sm"><i class="fa fa-print"></i></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    <!-- Fin del contenido -->
                </div>
            </div>
        </div>
        <?php include("footer.php"); ?>
    </div>
</div>
<?php include("view/footer.php"); ?>
</body>
</html>
