<?php 
/**
* Aca generamos una clase para realizar consultas a la base de datos del Pnia
*/
class PatronSingleton
{
	/**
	  *		Contenedor de la instancia del singleton
	  */ 
    private static $instancia;
	private $usuarios = array();
	private $dbh;
	
	private function __construct()
	{
		//$this->dbh = new PDO("mysql:host=server;dbname=database", "user", "password");
		$this->dbh = new PDO("sqlsrv:Server=NL1PNIA070\SQLEXPRESS;Database=QAWebSLFC", "sa", "Oscarjavier21");
	}

	/**
	 *		Método Singleton
	 */
    public static function singleton()
    {
        if (!isset(self::$instancia)) {
            $miclase = __CLASS__;
            self::$instancia = new $miclase;
        } 
        return self::$instancia;
    }
    /**
     * 		Generamos una funcion por cada tabla de nuestra base de datos
     */
    public function Usuario()
	{
		$consulta = $this->dbh->prepare("SELECT dbo.Usuario.ID,dbo.Usuario.Email,dbo.Usuario.Clave,dbo.Usuario.CodigoVerificacion,dbo.Usuario.Estado FROM dbo.Usuario");
		$consulta->execute();
		if ($consulta->rowCount()>0) 
		{
            while($reg = $consulta->fetch())
            {
               $this->Usuario[]=$reg;
        	}
            return $this->Usuario;     
        }
	}
	/**
	 * 		Evitamos que nuestro objeto se pueda clonar
	 */
    public function __clone()
    {
        trigger_error('La clonación de este objeto no está permitida', E_USER_ERROR);
    }
}

?>