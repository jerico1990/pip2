<?php
    /**
     * Creador: Oscar Javier Pazos Mogollón
     * Correo: opazos@icloud.com
     * Version:1.1
    **/

    include("config.php"); //Incluyo los parametros con los que trabajare
    class DBALL{
        public static $host;
        public static $usr;
        public static $pass;
        public static $dbname;
        public static $motor;
        private $conex;
        private $result;
        public static $showErrors;

        /**
        * I.- Creo un método que conecta a una base de datos dependiendo del parameto $motor
        * @return boolean
        */
        public function dbConnect()
        {
            self::$motor=strtoupper(self::$motor);
            $v=false;
            if(self::$motor=="MYSQL")
            {
                $this->conex=mysql_connect(self::$host,self::$usr,self::$pass);
                $var=mysql_select_db(self::$dbname,$this->conex);
                if($var)
                {
                    $v=true;
                }
            }
            elseif(self::$motor=="MSSQL")
            {
                $connectionInfo = array( "Database"=>self::$dbname, "UID"=>self::$usr, "PWD"=>self::$pass,"CharacterSet" => "UTF-8");
                $this->conex=sqlsrv_connect(self::$host, $connectionInfo);
                if($this->conex)
                {
                    $v=true;
                }

            }
            elseif(self::$motor=="ORACLE")
            {
                
            }
            return $v;
        } 
        /**
            * II.- Obtengo los registros de una consulta
            *
            * @param string $consulta
            * @return array
        */
        public function dbSelect($consulta)
        {
            $res=array();
            if(self::$motor=="MYSQL")
            {
                $this->result = mysql_query($consulta);
                if(self::$showErrors)
                {
                    if(mysql_errno($this->conex)!=0)
                    echo "Error ".mysql_errno($this->conex)." : ".mysql_error($this->conex);
                }
                $fila= mysql_num_rows($this->result);
                $i=0;
                while ($row = mysql_fetch_object($this->result))
                {
                    $res[$i]=$row;
                    $i++;
                }
                mysql_free_result($this->result);
            }
            elseif (self::$motor=="MSSQL")
            {
              $this->result = sqlsrv_query($this->conex,$consulta);
              if(self::$showErrors)
              {
                    if( $this->result === false ) 
                    {
                        die(print_r( sqlsrv_errors(), true));
                    }
              }
              $fila= sqlsrv_num_rows($this->result);
              $i=0;
              while ($row = sqlsrv_fetch_object($this->result)) 
              {
                $res[$i]=$row;
                $i++;
              }

            }
            return $res;
        }
        /**;
            * III.- Realiza consultas como:UPDATE,INSERT AND DELETE
            * retorna true si la consulta se ejecuto correctamente caso contrario false
            *
            * @param string $consulta
            * @return boolean
        */
        public function dbABM($consulta)
        {
            $r=false;
            if(self::$motor=="MYSQL")
            {
                $this->result = mysql_query($consulta);
                if(self::$showErrors)
                {
                    if(mysql_errno($this->conex)!=0)
                    echo "Error ".mysql_errno($this->conex)." : ".mysql_error($this->conex);
                }
            }
            elseif (self::$motor=="MSSQL")
            {
                $this->result = sqlsrv_query($this->conex,$consulta);
                if ($this->result) 
                {
                    $r=true;
                }
                else
                {
                    die(print_r(sqlsrv_errors(), true));
                }
                /*if(self::$showErrors)
                {
                    if($this->result === false) 
                    {
                        die(print_r(sqlsrv_errors(), true));
                    }
                }*/
            }
            return $r;
        }
        /**
            * IV.- Inicia la transaccion, retorna true si la transaccion se Inicia correctamente caso contrario false
            * @return Boolean
        */
        public function dbBeginTransaction()
        {
            if(self::$motor=="MYSQL")
            {
                $this->result = mysql_query("BEGIN");
            }
            elseif (self::$motor=="MSSQL")
            {
                 $this->result = sqlsrv_query($this->conex,"BEGIN");
            }
            if ($this->result) 
            {
                return true;
                echo "Transacción Iniciada";
            }
            else return false;
        }
        /**
            * V.- Cancela la transaccion, retorna true si la transaccion es cancelada correctamente caso contrario false
            * @return Boolean
        */
        public function dbCancelTransaction()
        {
            if(self::$motor=="MYSQL")
            {
                $this->result = mysql_query("ROLLBACK");
            }
            elseif (self::$motor=="MSSQL")
            {
                 $this->result = sqlsrv_query($this->conex,"ROLLBACK");
            }
            if ($this->result) 
            {
                return true;
                echo "Transacción Cancelada";
            }
            else return false;
        }
        /**
            * VI.- Termina la transaccion, retorna true si la transaccion termina correctamente caso contrario false
            * @return Boolean
        */
        public function dbEndTransaction()
        {
            if(self::$motor=="MYSQL")
            {
                $this->result = mysql_query("COMMIT");
            }
            elseif (self::$motor=="MSSQL")
            {
                 $this->result = sqlsrv_query($this->conex,"COMMIT");
            }
            if ($this->result) 
            {
                return true;
                echo "Transacción Terminada";
            }
            else return false;
        }
         /**
            * VII.- Cierro la conexión a la base de datos
        */
         public function dbClose()
         {
            if(self::$motor=="MYSQL")
            {
                mysql_close();
            }
            elseif (self::$motor=="MSSQL")
            {
                sqlsrv_close($this->conex);
            }
         }
    }
?>