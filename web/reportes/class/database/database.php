<?php 
/**
* Creamos una clase Singleton
*/
class PatronSingleton
{
	/**
	 * Contenedor de la instancia del singleton
	 */
    private static $instancia;
	private $usuarios = array();
	private $dbh;

	/**
	 * Un constructor privado evita la creación de un nuevo objeto
	 */
	function __construct()
	{
		//$this->dbh = new PDO("mysql:host=localhost;dbname=alia2", "root", "root");
		$this->dbh = new PDO ("mssql:host=172.168.3.19;dbname=PIP2","sa","Pni.2015");
	}

	/**
	 * Método singleton
	 */
	public static function singleton()
    {
        if (!isset(self::$instancia)) {
            $miclase = __CLASS__;
            self::$instancia = new $miclase;
        } 
        return self::$instancia;
    }
    /*public function departamentos()
    {
    	$consulta = $this->dbh->prepare("SELECT ts_departamento.id,ts_departamento.descripcion FROM ts_departamento");
		$consulta->execute();
		if ($consulta->rowCount()>0) 
		{
            while($reg = $consulta->fetch())
            {
               $this->departamentos[]=$reg;
        	}
            return $this->departamentos;     
        }
    }
    */
    public function laboratorios()
    {
    	$consulta=$this->dbh->prepare("SELECT dbo.UnidadEjecutora.ID,dbo.UnidadEjecutora.Nombre FROM dbo.UnidadEjecutora");
    	$consulta->execute();
    	if ($consulta->rowCount()>0)
    	{
    		while($reg = $consulta->fetch())
    		{
    			$this->laboratorios[]=$reg;
    		}
    		return $this->laboratorios;
    	}
    }


    /**
     * Evita que el objeto se pueda clonar
     */
    public function __clone()
    {
        trigger_error('La clonación de este objeto no está permitida', E_USER_ERROR);
    }
}

?>