<?php
require_once('../class/database/dataclass.php');
$db=new DBALL();
if($db->dbConnect())
{
	if (isset($_POST['id-data']))
	{
		$tbody=null;
		$consulta="SELECT dbo.Especie.ID,dbo.CultivoCrianza.Nombre AS Categoria,dbo.Especie.Nombre AS Producto,Count(dbo.Investigador.ID) AS Proyectos,InformacionGeneral.Codigo
		FROM dbo.Investigador
		INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.Especie ON dbo.InformacionGeneral.EspecieID = dbo.Especie.ID INNER JOIN dbo.CultivoCrianza ON dbo.CultivoCrianza.ID = dbo.Especie.CultivoCrianzaID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID
		WHERE dbo.UnidadEjecutora.ID = '".$_POST['id-data']."'
		GROUP BY dbo.CultivoCrianza.Nombre, dbo.Especie.Nombre,dbo.Especie.ID,InformacionGeneral.Codigo
		ORDER BY Categoria ASC, Producto ASC";
		$row01=$db->dbSelect($consulta);
		for($i=0;$i<count($row01);$i++)
		{
			$consulta="SELECT Sum(dbo.AreSubCategoria.CostoUnitario* dbo.AreSubCategoria.MetaFisica) AS Total
			FROM dbo.Investigador
			INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID INNER JOIN dbo.Proyecto ON dbo.Proyecto.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.Poa ON dbo.Poa.ProyectoID = dbo.Proyecto.ID INNER JOIN dbo.Componente ON dbo.Componente.PoaID = dbo.Poa.ID INNER JOIN dbo.Actividad ON dbo.Actividad.ComponenteID = dbo.Componente.ID INNER JOIN dbo.ActRubroElegible ON dbo.ActRubroElegible.ActividadID = dbo.Actividad.ID INNER JOIN dbo.AreSubCategoria ON dbo.AreSubCategoria.ActRubroElegibleID = dbo.ActRubroElegible.ID INNER JOIN dbo.Especie ON dbo.InformacionGeneral.EspecieID = dbo.Especie.ID INNER JOIN dbo.CultivoCrianza ON dbo.CultivoCrianza.ID = dbo.Especie.CultivoCrianzaID
			WHERE dbo.UnidadEjecutora.ID = '".$_POST['id-data']."' AND dbo.Especie.ID = '".$row01[$i]->ID."'";
			$row02=$db->dbSelect($consulta);

			$consulta="SELECT count(DISTINCT dbo.Ubigeo.ID) AS Distrito
			FROM dbo.Investigador
			INNER JOIN dbo.InformacionGeneral ON dbo.InformacionGeneral.InvestigadorID = dbo.Investigador.ID INNER JOIN dbo.Ubigeo ON dbo.InformacionGeneral.DistritoID = dbo.Ubigeo.ID INNER JOIN dbo.UnidadOperativa ON dbo.InformacionGeneral.UnidadOperativaID = dbo.UnidadOperativa.ID INNER JOIN dbo.UnidadEjecutora ON dbo.UnidadOperativa.UnidadEjecutoraID = dbo.UnidadEjecutora.ID INNER JOIN dbo.Especie ON dbo.InformacionGeneral.EspecieID = dbo.Especie.ID INNER JOIN dbo.CultivoCrianza ON dbo.CultivoCrianza.ID = dbo.Especie.CultivoCrianzaID
			WHERE dbo.UnidadEjecutora.ID = '".$_POST['id-data']."' AND dbo.Especie.ID = '".$row01[$i]->ID."'";
			$row03=$db->dbSelect($consulta);

			$tbody.='
				<tr>
					<td class="text-center"><small>'.($row01[$i]->Codigo).'</small></td>
					<td class="text-center"><small>'.$row01[$i]->Categoria.'</small></td>
					<td class="text-center"><small>'.$row01[$i]->Producto.'</small></td>
					<td class="text-right"><small>'.number_format($row03[0]->Distrito).'</small></td>
					<td class="text-right"><small>'.number_format($row01[$i]->Proyectos).'</small></td>
					<td class="text-right"><small>'.number_format($row02[0]->Total).'</small></td>
				</tr>
			';
		}


		$thead="<thead>
					<tr>
						<th width='5%' class='text-center'><small>#</small></th>
						<th width='25%' class='text-center'><small>Crianza o Cultivo</small></th>
						<th width='25%' class='text-center'><small>Producto</small></th>
						<th width='15%' class='text-center'><small>N° de Distritos atendidos</small></th>
						<th width='15%' class='text-center'><small>N° de SubProyectos</small></th>
						<th width='15%' class='text-center'><small>Monto Total Cofinanciado (S/)</small></th>
					</tr>
				</thead>";



		$resultado='
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-bordered">
							'.$thead.'
							<tbody>
								'.$tbody.'
							</tbody>
						</table>
					</div>
				</div>
			</div>
		';

		//$resultado=$_POST['id-data'];
		echo $resultado;
	}
}


		


?>