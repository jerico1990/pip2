﻿$('textarea.autosize').autosize({ append: "\n" });
$('textarea.autosize').css({ 'resize': 'none' });
$('[data-numeric=""]').inputmask("numeric");

function defaultParsley() {
    return {
        successClass: 'has-success',
        errorClass: 'has-error',
        classHandler: function (ps) {
            var $el = $(ps.$element);
            return $el.closest('.input-group');
        },
        errorsWrapper: '<ul class="help-block list-unstyled" style="padding-left:3px;"></ul>'
    };
}

function defaultParsleyForm() {
    return {
        successClass: 'has-success',
        errorClass: 'has-error',
        classHandler: function (ps) {            
            var $el = $(ps.$element);
            if (typeof $el.attr('readonly') == 'undefined') {
                return $el.closest('.form-group, td');
            } else {
                return '';
            }            
        },
        errorsContainer: function errorsContainer(ps) {
            var $el = $(ps.$element);
            var type = $el.attr('type');
            if (typeof type != 'undefined') {
                type = type.toLowerCase();
                if (type == 'checkbox' || type == 'radio') {
                    return $el.closest('[data-content-group]');
                }
                if (type == 'file') {
                    return $el.closest('[data-content-file]');
                }
            }
        },
        errorsWrapper: '<ul class="help-block list-unstyled" style="padding-left:3px;"></ul>'
    };
}

function defaultParsleyForm2() {
    return {
        successClass: 'has-success',
        errorClass: 'has-error',
        classHandler: function (ps) {            
            var $el = $(ps.$element);
            if (typeof $el.attr('readonly') == 'undefined') {
                return $el.closest('.form-group');
            } else {
                return '';
            }            
        },
        errorsContainer: function errorsContainer(ps) {
            var $el = $(ps.$element);
            var type = $el.attr('type');
            if (typeof type != 'undefined') {
                type = type.toLowerCase();
                if (type == 'checkbox' || type == 'radio') {
                    return $el.closest('[data-content-group]');
                }
                if (type == 'file') {
                    return $el.closest('[data-content-file]');
                }
                if (type == 'text' && $el.parent().hasClass('input-group')) {
                    return $el.closest('.input-group').parent();
                }
            }
        },
        errorsWrapper: '<ul class="help-block list-unstyled" style="padding-left:3px;"></ul>'
    };
}

function defaultParsleyFormInline() {
    return {
        successClass: 'has-success',
        errorClass: 'has-error',
        classHandler: function (ps) {            
            var $el = $(ps.$element);
            if (typeof $el.attr('readonly') == 'undefined') {
                return $el.closest('div');
            } else {
                return '';
            }            
        },
        errorsContainer: function errorsContainer(ps) {
            var $el = $(ps.$element);
            var type = $el.attr('type');
            if (typeof type != 'undefined') {
                type = type.toLowerCase();
                if (type == 'checkbox' || type == 'radio') {
                    return $el.closest('[data-content-group]');
                }
                if (type == 'file') {
                    return $el.closest('[data-content-file]');
                }
            }           
        },
        errorsWrapper: '<ul class="help-block list-unstyled" style="padding-left:3px;"></ul>'
    };
}

window.Parsley.addValidator('date', {
    validateString: function (value) {
        var date = Date.parseExact(value, 'dd/MM/yyyy');
        return date != null;
    },
    messages: {
        es: 'Este campo debe ser fecha válida.',
    }
});

window.Parsley.addValidator('select', {
    validateString: function (value) {        
        return value != '-1';
    },
    messages: {
        es: 'Debe seleccionar este campo.',
    }
});

function defaultDatePicker() {    
    return {
        language: 'es',
        format: 'dd/mm/yyyy',
        autoclose: true
    };
}

function formSend($form, $btn, mensajeOK, func) {
    var $respuesta = $('#container-resp');
    $respuesta.addClass('hidden');
    $btn.button('loading');

    var disabled = $form.find(':input:disabled').removeAttr('disabled');   
    var serialized = $form.serialize();
    disabled.attr('disabled','disabled');

    $.ajax({
        cache: false,
        type: 'POST',
        url: $form.attr('action'),
        data: serialized,
        success: function (data) {
           
            showMessageResponse(data, $respuesta, mensajeOK, func);

            $btn.button('reset');            
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $btn.button('reset');
            $respuesta.html('<div class="alert alert-dismissable alert-danger">Error interno del servidor, por favor inténtelo en unos minutos.</div>');
            $respuesta.removeClass('hidden');
        }
    });
}

function showMessageResponse(data, $respuesta, mensajeOK, func) {
    if (data.Success) {
        var correcto = '<div class="alert alert-dismissable alert-success">' +
                        mensajeOK + '</div>';
        $respuesta.html(correcto);

        if (func != null) {
            func(data);
        }
    } else {
        var errores = '<div class="alert alert-dismissable alert-danger">' +
                      '<strong>Errores:</strong><ul>';
        errores += '<li>' + data.Error + '.</li>';
        errores += '</ul></div>';

        $respuesta.html(errores);
    }

    $respuesta.removeClass('hidden');
}

// Analisis Economico
function formSendB($form, $btn, mensajeOK, func, $respuesta) {
    if ($respuesta == null) {
        $respuesta = $('#container-resp');
        $respuesta.addClass('hidden');
    }

    $btn.button('loading');

    var disabled = $form.find(':input:disabled').removeAttr('disabled');
    var serialized = $form.serialize();
    disabled.attr('disabled', 'disabled');

    $.ajax({
        cache: false,
        type: 'POST',
        url: $form.attr('action'),
        data: serialized,
        success: function (data) {

            showMessageResponseB(data, $respuesta, mensajeOK, func);

            $btn.button('reset');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $btn.button('reset');
            $respuesta.html('<div class="alert alert-dismissable alert-danger">Error interno del servidor, por favor inténtelo en unos minutos.</div>');
            $respuesta.removeClass('hidden');
        }
    });
}

jQuery.fn.insertAt = function (index, element) {
    var lastIndex = this.children().size()
    if (index < 0) {
        index = Math.max(0, lastIndex + 1 + index)
    }
    this.append(element)
    if (index < lastIndex) {
        this.children().eq(index).before(this.children().last())
    }
    return this;
}
//

function showMessageResponseB(data, $respuesta, mensajeOK, func) {
    if (data.Estado) {
        var correcto = '<div class="alert alert-dismissable alert-success">' +
                        mensajeOK + '</div>';
        $respuesta.html(correcto);

        if (func != null) {
            func(data);
        }
    } else {
        var errores = '<div class="alert alert-dismissable alert-danger">' +
                      '<strong>Errores:</strong><ul>';
        for (var i = 0; i < data.Errores.length; i++) {
            errores += '<li>' + data.Errores[i] + '.</li>';
        }
        errores += '</ul></div>';

        $respuesta.html(errores);
    }

    $respuesta.removeClass('hidden');
}

function showMessageErrorResponse(Message,$respuesta){
    var errores = '<div class="alert alert-dismissable alert-danger">' +
                  '<strong>Errores:</strong><ul>';
    errores += '<li>' + Message + '.</li>';
    errores += '</ul></div>';
    $respuesta.html(errores);
    $respuesta.removeClass('hidden');
}

function btnLoading(selector) {
    $(selector).attr('data-loading-text', 'Procesando...');    
}

function formEnter(selectorFrm, selectorClick) {
    $(selectorFrm).keypress(function (e) {
        if (e.which === 13) {
            e.preventDefault();
            $(selectorClick).click();
        }
    });
}

function bootBoxConfirm(message, func, btnOkText) {
    if (btnOkText == null) {
        btnOkText = 'Aceptar';
    }
    bootbox.dialog({
        message: message,
        buttons: {            
            cancel: {
                label: "Cancelar",
                className: "btn-default",
            },
            ok: {
                label: btnOkText,
                className: "btn-primary",
                callback: function () {
                    func();
                }
            }
        }
    });
}

$.extend(true, $.fn.dataTable.defaults, {
    "sPaginationType": "bootstrap",
    "oLanguage": {
        "sLengthMenu": "_MENU_ registros",
        "sSearch": "_INPUT_",
        "sInfo": "Mostrando _START_ - _END_ de _TOTAL_ registros",
        "sInfoFiltered": "(filtrado de _MAX_ registros)",
        "sInfoEmpty": "No hay registros",
        "sProcessing": "Procesando...",
        "oPaginate": {
            "sNext": "Siguiente",
            "sPrevious": "Anterior"
        },
        "sEmptyTable": "No hay registros",
        "sZeroRecords": "No hay registros"
    }
});

function cleanForm(selector, notSelector) {
    
    $(selector).find('input[type="text"],input[type="password"],textarea,input[type="hidden"]').not(notSelector).val('');
    $(selector).find('input[type="checkbox"]').prop('checked', false);
    $(selector).find('input[type="file"]').each(function (index) {
        $($(this).parent().parent()).find('[data-dismiss="fileinput"]').click();
    });
    
    $(selector).find('select').each(function (index) {
        var $this = $(this);
        $this.val($this.find("option:first").val());
    });
}

function controlsDatatables(){
    $('.dataTables_filter input').addClass('form-control').attr('placeholder', 'Buscar...');
    $('.dataTables_length select').addClass('form-control');
}

//

function sendForm($form, $btn, func) {
    //var $bootdialog = bootbox.dialog({
    //    message: '<div class="text-center"><h3>PROCESANDO...</h3><i class="fa fa-spinner fa-pulse fa-5x fa-fw" style="color:#16a085"></i></div>'
    //});
    //$bootdialog.attr({
    //    'data-backdrop': 'static',
    //    'data-keyboard': 'false'
    //});
    //$bootdialog.find('.bootbox-close-button').remove();

    _pageLoadingStart();
    var $respuesta = $('#container-resp');
    $btn.attr('disabled', 'disabled');

    var disabled = $form.find(':input:disabled').removeAttr('disabled');
    var serialized = $form.serialize();
    disabled.attr('disabled', 'disabled');

    $.ajax({
        cache: false,
        type: 'POST',
        url: $form.attr('action'),
        data: serialized,
        dataType: "json",
        success: function (data) {
            // console.log(data);
            $respuesta.addClass('hidden');
            if (data.Success) {
                bootbox.alert('Se registró correctamente.');
                $btn.removeAttr('disabled');
                if (func != null) {
                    func(data);
                }
                _pageLoadingEnd();
            } else {
                console.error(data.Error);
                if(data.Message){
                    showMessageErrorResponse(data.Message,$respuesta);
                    $btn.removeAttr('disabled');
                    _pageLoadingEnd();
                }
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            _pageLoadingEnd();
            $btn.removeAttr('disabled');
        }
    });
}

function sendFormNoMessage($form, $btn, func) {
    _pageLoadingStart();
    var $respuesta = $('#container-resp');
    $btn.attr('disabled', 'disabled');

    var disabled = $form.find(':input:disabled').removeAttr('disabled');
    var serialized = $form.serialize();
    disabled.attr('disabled', 'disabled');

    $.ajax({
        cache: false,
        type: 'POST',
        url: $form.attr('action'),
        data: serialized,
        dataType:'json',
        success: function (data) {
            $respuesta.addClass('hidden');
            if (data.Success) {
                $btn.removeAttr('disabled');
                if (func != null) {
                    func(data);
                }
                _pageLoadingEnd();
            } else {
                console.error(data.Error);
                // console.log(data.Message);
                if(data.Message){
                    showMessageErrorResponse(data.Message,$respuesta);
                    $btn.removeAttr('disabled');
                    _pageLoadingEnd();
                }
                
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            _pageLoadingEnd();
            $btn.removeAttr('disabled');
        }
    });
}

function sendAction(action, $btn, func) {
    _pageLoadingStart();
    $btn.attr('disabled', 'disabled');

    $.ajax({
        cache: false,
        type: 'POST',
        url: action,
        success: function (data) {
            if (data.Success) {
                $btn.removeAttr('disabled');
                if (func != null) {
                    func(data);
                }
                _pageLoadingEnd();
            } else {
                console.error(data.Error);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            _pageLoadingEnd();
            $btn.removeAttr('disabled');
        }
    });
}

//
function getDateFromStringddMMyyyy(strDate) {
    var dateParts = strDate.split("/");
    var date = new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);
    return date;
}

var datesTool = {
    convert: function (d) {
        // Converts the date in d to a date-object. The input can be:
        //   a date object: returned without modification
        //  an array      : Interpreted as [year,month,day]. NOTE: month is 0-11.
        //   a number     : Interpreted as number of milliseconds
        //                  since 1 Jan 1970 (a timestamp) 
        //   a string     : Any format supported by the javascript engine, like
        //                  "YYYY/MM/DD", "MM/DD/YYYY", "Jan 31 2009" etc.
        //  an object     : Interpreted as an object with year, month and date
        //                  attributes.  **NOTE** month is 0-11.
        return (
            d.constructor === Date ? d :
            d.constructor === Array ? new Date(d[0], d[1], d[2]) :
            d.constructor === Number ? new Date(d) :
            d.constructor === String ? new Date(d) :
            typeof d === "object" ? new Date(d.year, d.month, d.date) :
            NaN
        );
    },
    compare: function (a, b) {
        // Compare two dates (could be of any type supported by the convert
        // function above) and returns:
        //  -1 : if a < b
        //   0 : if a = b
        //   1 : if a > b
        // NaN : if a or b is an illegal date
        // NOTE: The code inside isFinite does an assignment (=).
        return (
            isFinite(a = this.convert(a).valueOf()) &&
            isFinite(b = this.convert(b).valueOf()) ?
            (a > b) - (a < b) :
            NaN
        );
    },
    inRange: function (d, start, end) {
        // Checks if date in d is between dates in start and end.
        // Returns a boolean or NaN:
        //    true  : if d is between start and end (inclusive)
        //    false : if d is before start or after end
        //    NaN   : if one or more of the dates is illegal.
        // NOTE: The code inside isFinite does an assignment (=).
        return (
             isFinite(d = this.convert(d).valueOf()) &&
             isFinite(start = this.convert(start).valueOf()) &&
             isFinite(end = this.convert(end).valueOf()) ?
             start <= d && d <= end :
             NaN
         );
    }
}

function monthDiff(d1, d2) {
    var months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth() + 1;
    months += d2.getMonth();
    return months <= 0 ? 0 : months;
}

function bootboxConfirmEliminar($btn, msg, fun) {
    $btn.attr('disabled');
    bootbox.confirm({
        title: '<span style="text-align:center">PNIA - CONFIRMACIÓN</span>',
        message: msg,
        buttons: {
            'cancel': {
                label: '<i class="fa fa-times"></i>&nbsp;Cancelar'
            },
            'confirm': {
                label: '<i class="fa fa-trash"></i>&nbsp;Eliminar'
            }
        },
        callback: function (confirmed) {
            if (confirmed) {
                fun();
                $btn.removeAttr('disabled');
            } else {
                $btn.removeAttr('disabled');
            }
        }
    });
}

function bootboxConfirm($btn, msg, btnText, fun) {
    $btn.attr('disabled');
    bootbox.confirm({
        title: '<span style="text-align:center">PNIA - CONFIRMACIÓN</span>',
        message: msg,
        buttons: {
            'cancel': {
                label: '<i class="fa fa-times"></i>&nbsp;Cancelar'
            },
            'confirm': {
                label: btnText
            }
        },
        callback: function (confirmed) {
            if (confirmed) {
                fun();
                $btn.removeAttr('disabled');
            } else {
                $btn.removeAttr('disabled');
            }
        }
    });
}

//function pageLoadingStart() {
    //    var $bootdialog = bootbox.dialog({
    //        message: '<div class="text-center"><i class="fa fa-spinner fa-pulse fa-5x fa-fw" style="color:#16a085"></i></div>'
    //    });
    //    $bootdialog.addClass('modal-loading');
    //    $bootdialog.attr({
    //        'data-backdrop': 'static',
    //        'data-keyboard': 'false'
    //    }).find('.bootbox-close-button').remove();
    //    $bootdialog.find('.modal-content').css('background', 'transparent');
    //    setTimeout(function () { bootbox.hideAll(); }, 1500);
    //}

    //function pageLoadingEnd() {
    //    bootbox.hideAll();
//}

function _pageLoadingStart() {
    $('#modal-loading').modal('show');
}

function _pageLoadingEnd() {
    $('#modal-loading').modal('hide');
}

function _updloadFileAjax(btnUploadFileID, inputFileID, url) {
    document.getElementById(btnUploadFileID).onclick = function () {
        var formdata = new FormData();
        var fileInput = document.getElementById(inputFileID);
        if (fileInput.files.length) {
            for (i = 0; i < fileInput.files.length; i++) {
                formdata.append(fileInput.files[i].name, fileInput.files[i]);
            }
            var xhr = new XMLHttpRequest();
            xhr.open('POST', url);
            xhr.send(formdata);
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    console.log(xhr);
                    var op = JSON.parse(xhr.responseText);
                    if (op.Success) {
                        bootbox.alert('El archivo se cargó correctamente');
                    } else {
                        console.error(op.Error);
                    }
                }
            }
        } else {
            bootbox.alert('Debe seleccionar un archivo.');
        }
        return false;
    }
}

(function ($) {
    $.fn._toNumeric = function () {
        this.keydown(function (e) {
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A
                 (e.keyCode == 65 && e.ctrlKey === true) ||
                        // Allow: Ctrl+C
                 (e.keyCode == 67 && e.ctrlKey === true) ||
                        // Allow: Ctrl+X
                 (e.keyCode == 88 && e.ctrlKey === true) ||
                        // Allow: home, end, left, right
                 (e.keyCode >= 35 && e.keyCode <= 39)) {
                        // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });        
    };
}(jQuery));


function NumCheck(e, field) {
  key = e.keyCode ? e.keyCode : e.which
  // backspace
  if (key == 8) return true
  // 0-9
  if (key > 47 && key < 58) {
    if (field.value == "") return true
    regexp = /.[0-9]{3}$/
    return !(regexp.test(field.value))
  }
  // .
  if (key == 46) {
    if (field.value == "") return false
    regexp = /^[0-9]+$/
    return regexp.test(field.value)
  }
  // other key
  return false
 
}

function soloNumeros(e){
    var key = window.Event ? e.which : e.keyCode
    return (key >= 48 && key <= 57)
}

function Imagen(elemento) {
    var ext = $(elemento).val().split('.').pop().toLowerCase();
    var error='';
    
    if($.inArray(ext, ['pdf','jpg','png']) == -1) {
        error=error+'La imagen seleccionada debe estar en los formatos .PDF, .JPG o .PNG';
    }
    if (error=='' && elemento.files[0].size/1024/1024>=5) {
        error=error+'La imagen seleccionada debe ser menor a 5MB';
    }
    
    if (error!='') {
        bootbox.alert(error);
        $(elemento).val('');
        return false;
    }
    return true;
}


function validarNumeros(){
    
    
    
    $('.input-number').inputmask("decimal", {
        radixPoint: ".",
        groupSeparator: ",",
        groupSize: 3,
        digits: 7,
        integerDigits: 7,
        autoGroup: true,
        allowPlus: false,
        allowMinus: true,
        placeholder: ''
    }).click(function () {
        $(this).select();
    });
}


function formato_numero(amount, decimals) {

    amount += ''; // por si pasan un numero en vez de un string
    amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

    decimals = decimals || 0; // por si la variable no fue fue pasada

    // si no es un numero o es igual a cero retorno el mismo cero
    if (isNaN(amount) || amount === 0) 
        return parseFloat(0).toFixed(decimals);

    // si es mayor o menor que cero retorno el valor formateado como numero
    amount = '' + amount.toFixed(decimals);

    var amount_parts = amount.split('.'),
        regexp = /(\d+)(\d{3})/;

    while (regexp.test(amount_parts[0]))
        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

    return amount_parts.join('.');
}



function NotificacionEscritorio(Tipo,Mensaje) {
    if (Notification) {
        if (Notification.permission !== "granted") {
            Notification.requestPermission()
        }
        var title = "Mensaje PIP2"
        var extra = {
            icon: "http://xitrus.es/imgs/logo_claro.png",
            body: Mensaje
        }
        var noti = new Notification( title, extra)
        noti.onclick = {
        // Al hacer click
        }
        noti.onclose = {
        // Al cerrar
        }
        setTimeout( function() { noti.close() }, 10000)
    }
}
//---
// muestra alertas al usuario con un determinado formato
//--- 
function alerta_sistema(content,classType, mensaje){
    var v_alerta = $('<div class="alert alert-'+classType+' alert-dismissible" role="alert">'
        + '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
        + '    <span aria-hidden="true">&times;</span>'
        + '</button>'
        + mensaje
        + '</div>'
    );
    $(content).append(v_alerta);
    v_alerta.alert();
    setTimeout(function() {
        v_alerta.fadeOut(1500, function() {
            v_alerta.fadeIn(1500, function() {
                v_alerta.fadeOut(1500, function(){ v_alerta.remove(); });
            });
        });
    },3000);
}