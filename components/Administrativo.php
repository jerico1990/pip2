<?php 
namespace app\components;

use Yii;
use yii\helpers\Html;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LogRegistro;
use app\models\Usuario;
use app\models\DetalleCertificacionPresupuestal;

/**
* public function
*/
//$result = Yii::$app->tools->Prueba();
class Administrativo extends \yii\base\Object
{



	public function obtenerSiaf($numRegCab,$annio){
		$GetExpedienteSiaf=\Yii::$app->db->createCommand('BEGIN DECLARE @a VARCHAR(255); EXEC SP_OBTENER_EXPEDIENTE_SIAF :NUM_REGI_CAB, :ANNO_EJEC_EJE select @a as NUME_SIAF_CAB end;')
		->bindValue(':NUM_REGI_CAB',$numRegCab)
		->bindValue(':ANNO_EJEC_EJE',$annio)
		->queryOne();

		return $GetExpedienteSiaf['NUME_SIAF_CAB'];
	}


	public function obtenerEstadoDevengado($numRegCab,$annio){
		$mensaje = "";
		$estadoDevengado=\Yii::$app->db->createCommand('BEGIN DECLARE @a VARCHAR(255); EXEC SP_OBTENER_ESTADO_DEVENGADO :NUM_REGI_CAB, :ANNO_EJEC_EJE select @a as NUME_SIAF_CAB end;')
		->bindValue(':NUM_REGI_CAB',$numRegCab)
		->bindValue(':ANNO_EJEC_EJE',$annio)
		->queryOne();

		switch (trim($estadoDevengado['ESTA_REGI_CAB'])) {
            case '1':
                $mensaje = '<strong style="color:red">NO TIENE DEVENGADO AUTORIZADO</strong>';
                break;
            case '2':
                $mensaje = '<strong style="color:green">DEVENGADO TOTAL</strong>';
                break;
            case '3':
                $mensaje = '<strong style="color:silver">GIRADO</strong>';
                break;
            case '4':
                $mensaje = '<strong style="color:green">PAGADO</strong>';
                break;
            case '5':
                $mensaje = '<strong style="color:red">NO SE HA DEVENGADO</strong>';
                break;
            case '*':
                $mensaje = '<strong>DEVENGADO RECHAZADO</strong>';
                break;
        }

        return $mensaje;
	}


    public function obtenerFuente($certificado,$annio,$fuente = ''){
        $Fuente=\Yii::$app->db->createCommand('BEGIN DECLARE @a VARCHAR(255); EXEC SP_FUENTE_FINANCIAMIENTO :ANO_EJE, :CERTIFICADO, :FUENTE  select @a as NUME_SIAF_CAB  END;')
        ->bindValue(':ANO_EJE',$annio)
        ->bindValue(':CERTIFICADO',$certificado)
        ->bindValue(':FUENTE',$fuente)
        ->queryAll();
        return $Fuente;
    }

    public function obtenerTipoCambio($fecha){
        $formatFecha = Yii::$app->tools->dateFormat($fecha,'Y-m-d');
        $Fuente=\Yii::$app->db->createCommand('EXEC SP_TIPO_CAMBIO_SIMEG :FECH_CAMB_TIP ;')
        ->bindValue(':FECH_CAMB_TIP',$formatFecha)
        ->queryOne();
        return $Fuente;
    }

}