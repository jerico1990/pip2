<?php

namespace app\components;
use Yii;
use app\models\ProgramacionTecnica;

//======================================================================
// Descripcion	: Contiene los metodos de acceso a la base de datos 
//				  referentes a la tabla Indicadores.
// Creado 		: fcastro - 29/09/2017
//======================================================================
class DAIndicadores extends \yii\base\Object
{
	//-----------------------------------------------------
	// Descripcion	: Filtra la informacion por el campo ProgramacionTipo 
	// Creado 		: fcastro - 06/09/2017
	//-----------------------------------------------------
	public function Consultar($ProyectoId, $CodigoProyecto){
		$_Programacion=\Yii::$app->db->createCommand('BEGIN EXEC pEjecucionIndicador_Sel :piProyectoId, :pvCodigoProyecto END;')
			->bindValue(':piProyectoId',$ProyectoId)
			->bindValue(':pvCodigoProyecto',$CodigoProyecto)
			->queryAll();
		return $_Programacion;
	}
	//-----------------------------------------------------
	// Descripcion	: Filtra la informacion por el campo ProgramacionTipo 
	// de los experimentos que no estan ejecutados 
	// Creado 		: fcastro - 29/09/2017
	//-----------------------------------------------------
	public function ListarIndicadoresActivos($CodigoProyecto){
		$_Programacion=\Yii::$app->db->createCommand('BEGIN EXEC pIndicador_Sel_CodigoProyecto :pvCodigoProyecto END;')
			->bindValue(':pvCodigoProyecto',$CodigoProyecto)
			->queryAll();
		return $_Programacion;
	}
	//-----------------------------------------------------
	// Descripcion	: Inserta un registro 
	// Creado 		: fcastro - 06/09/2017
	//-----------------------------------------------------
	public function Insertar($ProgramacionTecnicaModel){
		$_Programacion=\Yii::$app->db->createCommand('BEGIN EXEC pEjecucionIndicador_Ins :pvCodigoProyecto, :piIndicadorId, :piProgramado, :piEjecutado, :pvDescripcion, :piProbabilidad, :pvObservacion, :piEstado END;')
			->bindValue(':pvCodigoProyecto', $ProgramacionTecnicaModel->CodigoProyecto)
			->bindValue(':piIndicadorId', $ProgramacionTecnicaModel->IndicadorId)
			->bindValue(':piProgramado', $ProgramacionTecnicaModel->Programado)
			->bindValue(':piEjecutado', $ProgramacionTecnicaModel->Ejecutado)
			->bindValue(':pvDescripcion', $ProgramacionTecnicaModel->Descripcion)
			->bindValue(':piProbabilidad',$ProgramacionTecnicaModel->Probabilidad)
			->bindValue(':pvObservacion', $ProgramacionTecnicaModel->Observacion)
			->bindValue(':piEstado', $ProgramacionTecnicaModel->Estado)
			->queryOne();

		return $_Programacion['FilasAfectadas'];
	}
	//-----------------------------------------------------
	// Descripcion	: Elimina un registro 
	// Creado 		: fcastro - 06/09/2017
	//-----------------------------------------------------
	public function Eliminar($ProgramacionTecnicaModel){
		$_Programacion=\Yii::$app->db->createCommand('BEGIN EXEC pEjecucionIndicador_Del :piProyectoID, :piIndicadorId END;')
			->bindValue(':piProyectoID', $ProgramacionTecnicaModel->ProyectoId)
			->bindValue(':piIndicadorId', $ProgramacionTecnicaModel->IndicadorId)
			->queryOne();

		return $_Programacion['FilasAfectadas'];
	}
}
