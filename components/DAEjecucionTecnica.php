<?php
namespace app\components;
use Yii;
use app\models\EjecucionTecnica;

//======================================================================
// Descripcion	: Contiene los metodos de acceso a la base de datos 
//				  referentes a la tabla ProgramacionTecnica.
// Creado 		: fcastro - 06/09/2017
//======================================================================
class DAEjecucionTecnica extends \yii\base\Object
{
	//-----------------------------------------------------
	// Descripcion	: Filtra la informacion por el campo ProgramacionTipo 
	// Creado 		: fcastro - 06/09/2017
	//-----------------------------------------------------
	public function ConsultarXtipo($ProgramacionTipo){
		$_Programacion=\Yii::$app->db->createCommand('BEGIN EXEC pEjecucionTecnica_Sel_Tipo :pvProgramacionTipo END;')
			->bindValue(':pvProgramacionTipo',$ProgramacionTipo)
			->queryAll();
		return $_Programacion;
	}
	//-----------------------------------------------------
	// Descripcion	: Devuleve un registro de ProgramacionTecnica por el primary key
	// Creado 		: fcastro - 08/09/2017
	//-----------------------------------------------------
	public function Selecciona($IdEjecucionTecnica){
		$_Programacion=\Yii::$app->db->createCommand('BEGIN EXEC pEjecucionTecnica_Sel :piIdEjecucionTecnica END;')
			->bindValue(':piIdEjecucionTecnica',$IdEjecucionTecnica)
			->queryOne();
		return $_Programacion;
	}
	//-----------------------------------------------------
	// Descripcion	: Inserta un registro 
	// Creado 		: fcastro - 06/09/2017
	//-----------------------------------------------------
	public function Insertar($EjecucionTecnicaModel){
		$_Programacion=\Yii::$app->db->createCommand('BEGIN EXEC pEjecucionTecnica_Ins :piIdEjecucionTecnica, :piCorrelativo, :pvResponsableTecnico, :pvDuracionHoras, :pvLatitud, :pvLongitud,:piBeneficiarioHombres, :piBeneficiarioMujeres, :pvMaterial, :pvMetodo, :pvResultado, :pvConclusion, :pvObservacionUafsi, :pvObservacionInvestigador, :pvCosto, :piEstado END;')
		
			->bindValue(':piIdEjecucionTecnica', $EjecucionTecnicaModel->IdEjecucionTecnica)
			->bindValue(':piCorrelativo', $EjecucionTecnicaModel->Correlativo)
			->bindValue(':pvResponsableTecnico', $EjecucionTecnicaModel->ResponsableTecnico)
			->bindValue(':pvDuracionHoras',$EjecucionTecnicaModel->DuracionHoras)
			->bindValue(':pvLatitud',$EjecucionTecnicaModel->Latitud)
			->bindValue(':pvLongitud',$EjecucionTecnicaModel->Longitud)
			->bindValue(':piBeneficiarioHombres',$EjecucionTecnicaModel->BeneficiarioHombres)
			->bindValue(':piBeneficiarioMujeres',$EjecucionTecnicaModel->BeneficiarioMujeres)
			->bindValue(':pvMaterial',$EjecucionTecnicaModel->Material)
			->bindValue(':pvMetodo',$EjecucionTecnicaModel->Metodo)
			->bindValue(':pvResultado',$EjecucionTecnicaModel->Resultado)
			->bindValue(':pvConclusion',$EjecucionTecnicaModel->Conclusion)
			->bindValue(':pvObservacionUafsi',$EjecucionTecnicaModel->ObservacionUafsi)
			->bindValue(':pvObservacionInvestigador',$EjecucionTecnicaModel->ObservacionInvestigador)
			->bindValue(':pvCosto',$EjecucionTecnicaModel->Costo)
			->bindValue(':piEstado',$EjecucionTecnicaModel->Estado)
			->queryOne();

		return true;
	}
	//-----------------------------------------------------
	// Descripcion	: Actualiza un registro 
	// Creado 		: fcastro - 11/09/2017
	//-----------------------------------------------------
	public function Actualizar($EjecucionTecnicaModel){
		$_Programacion=\Yii::$app->db->createCommand('BEGIN EXEC pEjecucionTecnica_Upd :piIdEjecucionTecnica, :pvResponsableTecnico, :piDuracionHoras, :piBeneficiarioHombres, :piBeneficiarioMujeres, :pvMaterial, :pvMetodo, :pvResultado, :pvConclusion, :pvObservacionUafsi, :pvObservacionInvestigador, :pdCosto END;')
			->bindValue(':piIdEjecucionTecnica', $EjecucionTecnicaModel->IdEjecucionTecnica)
			->bindValue(':pvResponsableTecnico', $EjecucionTecnicaModel->ResponsableTecnico)
			->bindValue(':piDuracionHoras',$EjecucionTecnicaModel->DuracionHoras)
			->bindValue(':piBeneficiarioHombres', $EjecucionTecnicaModel->BeneficiarioHombres)
			->bindValue(':piBeneficiarioMujeres', $EjecucionTecnicaModel->BeneficiarioMujeres)
			->bindValue(':pvMaterial', $EjecucionTecnicaModel->Material)
			->bindValue(':pvMetodo', $EjecucionTecnicaModel->Metodo)
			->bindValue(':pvResultado', $EjecucionTecnicaModel->Resultado)
			->bindValue(':pvConclusion', $EjecucionTecnicaModel->Conclusion)
			->bindValue(':pvObservacionUafsi', $EjecucionTecnicaModel->ObservacionUafsi)
			->bindValue(':pvObservacionInvestigador', $EjecucionTecnicaModel->ObservacionInvestigador)
			->bindValue(':pdCosto', $EjecucionTecnicaModel->Costo)
			->queryOne();

		return $_Programacion['FilasAfectadas'];
	}
	//-----------------------------------------------------
	// Descripcion	: Elimina un registro 
	// Creado 		: fcastro - 06/09/2017
	//-----------------------------------------------------
	public function Eliminar($IdProgramacionTecnica){
		/*$_Programacion=\Yii::$app->db->createCommand('BEGIN EXEC pProgramacionTecnica_Del :piIdProgramacionTecnica END;')
			->bindValue(':piIdProgramacionTecnica', $IdProgramacionTecnica)
			->queryOne();

		return $_Programacion['FilasAfectadas'];*/
	}


	//-----------------------------------------------------
	// Descripcion	: Inserta un registro en la tabla  EjecucionTecnicaAdjuntoz
	// Creado 		: fcastro - 06/09/2017
	//-----------------------------------------------------
	public function InsertarSustento($IdEjecucionTecnica, $Ruta, $Nombre){
		$_Programacion=\Yii::$app->db->createCommand('BEGIN EXEC pEjecucionTecnicaAdjunto_Ins :piIdEjecucionTecnica, :pvRutaArchivo, :pvNombreArchivo END;')
		
			->bindValue(':piIdEjecucionTecnica', $IdEjecucionTecnica)
			->bindValue(':pvRutaArchivo', $Ruta)
			->bindValue(':pvNombreArchivo', $Nombre)
			->queryOne();

		return $_Programacion['IdAdjunto'];
	}
	//-----------------------------------------------------
	// Descripcion	: Devuleve un registro de ProgramacionTecnica por el primary key
	// Creado 		: fcastro - 08/09/2017
	//-----------------------------------------------------
	public function SustentoPorEjecucion($IdEjecucionTecnica){
		$_Programacion=\Yii::$app->db->createCommand('BEGIN EXEC pEjecucionTecnicaAdjunto_Sel_IdEje :piIdEjecucionTecnica END;')
			->bindValue(':piIdEjecucionTecnica',$IdEjecucionTecnica)
			->queryAll();
		return $_Programacion;
	}
	//-----------------------------------------------------
	// Descripcion	: Elimia un o varios registro de EjecucionTecnicaAdjuntos por el primary key
	// Creado 		: fcastro - 25/09/2017
	//-----------------------------------------------------
	public function EliminaSustentos($IdAdjuntos){
		$_Programacion=\Yii::$app->db->createCommand('BEGIN EXEC pEjecucionTecnicaAdjunto_Del :pvIdAdjuntos END;')
			->bindValue(':pvIdAdjuntos',$IdAdjuntos)
			->queryAll();
		return $_Programacion;
	}
}