<?php

namespace app\components;
use Yii;
use app\models\ProgramacionTecnica;


/*********************************************************************
* Descripcion	: Contiene la logica del negocio a la que esta 
*				  sugeta la entidad ProgramacionTecnica.
* Creado 		: 06/09/2017
*********************************************************************/
class BNProgramacionTecnica extends \yii\base\Object
{
	private $_DAProgramacionTecnica;

	public function __construct(){
		$this->_DAProgramacionTecnica = new DAProgramacionTecnica();
	}

	//-----------------------------------------------------
	// Descripcion	: Inserta un registro 
	// Creado 		: fcastro - 06/09/2017
	//-----------------------------------------------------
	public function Registrar($ProgramacionTecnica){
		
		if($ProgramacionTecnica->IdProgramacionTecnica == -1){
			$s = $this->_DAProgramacionTecnica->Insertar($ProgramacionTecnica); 
			$ProgramacionTecnica->IdProgramacionTecnica = $s;
		} else {
			$s = $this->_DAProgramacionTecnica->Actualizar($ProgramacionTecnica); 	
			/*if($s > 0){

			}*/	
		}		
		return $ProgramacionTecnica;
	}
	//-----------------------------------------------------
	// Descripcion	: Elimina un registro 
	// Creado 		: fcastro - 06/09/2017
	//-----------------------------------------------------
	public function Eliminar($IdProgramacionTecnica){
		$s = $this->_DAProgramacionTecnica->Eliminar($IdProgramacionTecnica);
		return $s;
	}

	//-----------------------------------------------------
	// Descripcion	: Actualiza algunos campos de la programacion
	// este metodo es ejecutado desde la ejecucion, ya que ciertos
	// campos se puede actualizar desde la etapa de ejecucion. 
	// Creado 		: fcastro - 22/09/2017
	//-----------------------------------------------------
	public function ActualizarDesdeEjecucion($ProgramacionTecnica){
		
		$this->_DAProgramacionTecnica->ActualizarDesdeEjecucion($ProgramacionTecnica); 
		return $ProgramacionTecnica;
	}
}
