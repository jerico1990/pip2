<?php 
namespace app\components;

use Yii;
use yii\helpers\Html;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LogRegistro;
use app\models\Usuario;
use app\models\DetalleCertificacionPresupuestal;

/**
* public function
*/
//$result = Yii::$app->tools->Prueba();
class Tools extends \yii\base\Object
{

    public function dateFormat( $date , $formtato = 'd-m-Y'){
		return date($formtato, strtotime($date));
    }


    public function dias_transcurridos( $dateInicio , $dateFin ){
        $dias	= (strtotime($dateInicio) - strtotime($dateFin))/86400;
        $dias 	= abs($dias); $dias = floor($dias);		
        return $dias;
    }


    public function fines_semana( $fecha_inicio, $fecha_fin){
        setlocale(LC_TIME,"es_CL"); 
        $sunday_val      = 6; 
        $saturday_val    = 7; 
        $seconds_per_day = 86400; // Segudos que tiene un dia de 24 horas
        $d_weekend_days  = 0; 

        $start_date      = strtotime($fecha_inicio); 
        $end_date        = strtotime($fecha_fin); 

        for($day_val = $start_date; $day_val <= $end_date; $day_val += $seconds_per_day){ 
            $pointer_day = date("N", $day_val); 

            if(($pointer_day == $sunday_val) || ($pointer_day == $saturday_val)){ 
                // echo date("d/m/Y",$day_val)."<br>"; 
                $d_weekend_days++; 
            } 
        } 
        return $d_weekend_days;
    }
    
    public function RestarFechas($FechaInicio,$FechaFin,$ID =null)
    {

        $dias = Yii::$app->db->createCommand("SELECT DATEDIFF(day, '$FechaInicio','$FechaFin' ) Dias ")
                    ->queryOne();
        $days = $dias['Dias'];
		// $datetime1= strtotime(date('Y-m-d h:m:s',strtotime($FechaInicio)));
		// $datetime2= strtotime($FechaFin);
		// $diff = abs($datetime1-$datetime2);
		// $years = floor($diff / (365*60*60*24));
		// $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
		// $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

		$color="green";
		$dia="día";
		if($days<1)
		{
		    $color="green";
		    $dia="día";
		}
		elseif($days>1 && $days<=2)
		{
		    $color="orange";
		    $dia="días";
		}
		elseif($days>2)
		{
		    $color="red";
		    $dia="días";
		}
		
		
		return "<span style='color:$color'><b>".$days." $dia</b></span>";
    }
    
    
    public function FormatoDemora($days)
    {
		$color="green";
		$dia="día";
		if($days<1)
		{
		    $color="green";
		    $dia="día";
		}
		elseif($days>1 && $days<=2)
		{
		    $color="orange";
		    $dia="días";
		}
		elseif($days>2)
		{
		    $color="red";
		    $dia="días";
		}
		
		
		return "<span style='color:$color'><b>".$days." $dia</b></span>";
    }
    
    public function DescripcionMes($mes=null)
    {
        setlocale (LC_TIME,"spanish");
        //$mes = 01;
        $mes=$mes-1;
        $fechaInicio = '2016-05-01';
        $nuevafecha = strtotime ( '+'.$mes.' month' , strtotime ( $fechaInicio ) ) ;
        $nuevafecha = strtoupper(strftime ( '%b %Y' , $nuevafecha ) ) ;
        return $nuevafecha;
    }
    


    /**
     * Funcion que valida el formato via expresiones regulares
     *
     * Ej. 1992-01-01 
     * 
     * @param  string  $date Fecha
     * @return boolean     
     */
    public function isDefaultDate($date, $separator = '-')
    {   
        // Separator
        $separator = preg_match('/\-/', $separator) ? '-' : (preg_match('/\//', $separator) ? '\/': '-');
        // Validate
        return (preg_match('/^((19|20)[0-9]{2})'.$separator.'(0[1-9]|1[012])'.$separator.'(0[1-9]|[12][0-9]|3[01])$/', $date) ? true: false);
    }

    public function isTime($time)
    {
        return (preg_match('/^(([1-9]|0[1-9]|1[0-9]|2[1-4]):([0-5][0-9]):([0-5][0-9]))$/', $time) ? $time: null);
    }


    public function pre($arr,$exit = null) 
    {
        echo '<pre>';	
        print_r($arr);
        echo '</pre>';
        ($exit == 1) ? exit() : '';
    }

    public function isValidArray(&$array)
    { 
        return ((is_array($array) && $array && count($array)) ? true: false);
    }


    public function isRuc($ruc_number,$tipo = null)
    {
        $status = false;
        if(is_null($tipo)){
                $special = '/^(10|20|15|17)[0-9]{9}$/';
        }else{
                $special = '/^(20|17)[0-9]{9}$/';
        }
    
        if (preg_match($special, $ruc_number)) 
        {
            $suma = 0;
            $x = 6;
            
            for ($i = 0; $i < 10; $i++) 
            {
                if ( $i == 4 ) $x = 8;
                $digito = substr($ruc_number, $i, 1);
                $x--;
                $suma+= ($digito * $x);
            }
            
            $resto = $suma % 11;
            $resto = 11 - $resto;
            
            if ( $resto >= 10) 
            {
                $resto = $resto - 10;
            }
    
            if ( $resto == substr($ruc_number, 10, 1))
            {
                $status = true;
            }
        }
    
        return $status;	
    }


    public function ServicioReniec($valor=null)
    {
        if(!empty($valor)){
            if(strlen($valor) == 8){
                $respuesta = file_get_contents("http://172.168.3.18/apipide/api/reniec/dni/" . $valor);
                // $respuesta = file_get_contents("http://200.48.8.77/pip2/web/servicio/sunat?valor=" . $valor);
                echo $respuesta;
            }else{
                $arr = array(
                    'error'=>1
                );
                echo json_encode($arr);
            }
        }else{
            $arr = array(
                'error'=>0
                );
            return json_encode($arr);
        }
    }

    public function ServicioSunat($valor=null)
    {
        $respuesta=1;
        if(!empty($valor)){
            if(strlen($valor)==11){
                 $respuesta = file_get_contents("http://172.168.3.18/apisunat/api/consultarruc/" . $valor);
               // $respuesta = file_get_contents("http://200.48.8.77/pip2/web/servicio/sunat?valor=" . $valor);
            }
            return $respuesta;
        }
    }
    
    
    
    public function numtoletras($xcifra)
    {
		$xarray = [0 => "Cero",
		    1 => "UN", "DOS", "TRES", "CUATRO", "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE",
		    "DIEZ", "ONCE", "DOCE", "TRECE", "CATORCE", "QUINCE", "DIECISEIS", "DIECISIETE", "DIECIOCHO", "DIECINUEVE",
		    "VEINTI", 30 => "TREINTA", 40 => "CUARENTA", 50 => "CINCUENTA", 60 => "SESENTA", 70 => "SETENTA", 80 => "OCHENTA", 90 => "NOVENTA",
		    100 => "CIENTO", 200 => "DOSCIENTOS", 300 => "TRESCIENTOS", 400 => "CUATROCIENTOS", 500 => "QUINIENTOS", 600 => "SEISCIENTOS", 700 => "SETECIENTOS", 800 => "OCHOCIENTOS", 900 => "NOVECIENTOS"
		];
		$xcifra = trim($xcifra);
		$xlength = strlen($xcifra);
		$xpos_punto = strpos($xcifra, ".");
		$xaux_int = $xcifra;
		$xdecimales = "00";
		if (!($xpos_punto === false)) {
		    if ($xpos_punto == 0) {
			$xcifra = "0" . $xcifra;
			$xpos_punto = strpos($xcifra, ".");
		    }
		    $xaux_int = substr($xcifra, 0, $xpos_punto); // obtengo el entero de la cifra a covertir
		    $xdecimales = substr($xcifra . "00", $xpos_punto + 1, 2); // obtengo los valores decimales
		}
	     
		$XAUX = str_pad($xaux_int, 18, " ", STR_PAD_LEFT); // ajusto la longitud de la cifra, para que sea divisible por centenas de miles (grupos de 6)
		$xcadena = "";
		for ($xz = 0; $xz < 3; $xz++) {
		    $xaux = substr($XAUX, $xz * 6, 6);
		    $xi = 0;
		    $xlimite = 6; // inicializo el contador de centenas xi y establezco el límite a 6 dígitos en la parte entera
		    $xexit = true; // bandera para controlar el ciclo del While
		    while ($xexit) {
			if ($xi == $xlimite) { // si ya llegó al límite máximo de enteros
			    break; // termina el ciclo
			}
	     
			$x3digitos = ($xlimite - $xi) * -1; // comienzo con los tres primeros digitos de la cifra, comenzando por la izquierda
			$xaux = substr($xaux, $x3digitos, abs($x3digitos)); // obtengo la centena (los tres dígitos)
			for ($xy = 1; $xy < 4; $xy++) { // ciclo para revisar centenas, decenas y unidades, en ese orden
			    switch ($xy) {
				case 1: // checa las centenas
				    if (substr($xaux, 0, 3) < 100) { // si el grupo de tres dígitos es menor a una centena ( < 99) no hace nada y pasa a revisar las decenas
					 
				    } else {
					$key = (int) substr($xaux, 0, 3);
					if (TRUE === array_key_exists($key, $xarray)){  // busco si la centena es número redondo (100, 200, 300, 400, etc..)
					    $xseek = $xarray[$key];
					    $xsub = $this->subfijo($xaux); // devuelve el subfijo correspondiente (Millón, Millones, Mil o nada)
					    if (substr($xaux, 0, 3) == 100)
						$xcadena = " " . $xcadena . " CIEN " . $xsub;
					    else
						$xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
					    $xy = 3; // la centena fue redonda, entonces termino el ciclo del for y ya no reviso decenas ni unidades
					}
					else { // entra aquí si la centena no fue numero redondo (101, 253, 120, 980, etc.)
					    $key = (int) substr($xaux, 0, 1) * 100;
					    $xseek = $xarray[$key]; // toma el primer caracter de la centena y lo multiplica por cien y lo busca en el arreglo (para que busque 100,200,300, etc)
					    $xcadena = " " . $xcadena . " " . $xseek;
					} // ENDIF ($xseek)
				    } // ENDIF (substr($xaux, 0, 3) < 100)
				    break;
				case 2: // checa las decenas (con la misma lógica que las centenas)
				    if (substr($xaux, 1, 2) < 10) {
					 
				    } else {
					$key = (int) substr($xaux, 1, 2);
					if (TRUE === array_key_exists($key, $xarray)) {
					    $xseek = $xarray[$key];
					    $xsub = $this->subfijo($xaux);
					    if (substr($xaux, 1, 2) == 20)
						$xcadena = " " . $xcadena . " VEINTE " . $xsub;
					    else
						$xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
					    $xy = 3;
					}
					else {
					    $key = (int) substr($xaux, 1, 1) * 10;
					    $xseek = $xarray[$key];
					    if (20 == substr($xaux, 1, 1) * 10)
						$xcadena = " " . $xcadena . " " . $xseek;
					    else
						$xcadena = " " . $xcadena . " " . $xseek . " Y ";
					} // ENDIF ($xseek)
				    } // ENDIF (substr($xaux, 1, 2) < 10)
				    break;
				case 3: // checa las unidades
				    if (substr($xaux, 2, 1) < 1) { // si la unidad es cero, ya no hace nada
					 
				    } else {
					$key = (int) substr($xaux, 2, 1);
					$xseek = $xarray[$key]; // obtengo directamente el valor de la unidad (del uno al nueve)
					$xsub = $this->subfijo($xaux);
					$xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
				    } // ENDIF (substr($xaux, 2, 1) < 1)
				    break;
			    } // END SWITCH
			} // END FOR
			$xi = $xi + 3;
		    } // ENDDO
	     
		    if (substr(trim($xcadena), -5, 5) == "ILLON") // si la cadena obtenida termina en MILLON o BILLON, entonces le agrega al final la conjuncion DE
			$xcadena.= " DE";
	     
		    if (substr(trim($xcadena), -7, 7) == "ILLONES") // si la cadena obtenida en MILLONES o BILLONES, entoncea le agrega al final la conjuncion DE
			$xcadena.= " DE";
	     
		    // ----------- esta línea la puedes cambiar de acuerdo a tus necesidades o a tu país -------
		    if (trim($xaux) != "") {
			switch ($xz) {
			    case 0:
				if (trim(substr($XAUX, $xz * 6, 6)) == "1")
				    $xcadena.= "UN BILLON ";
				else
				    $xcadena.= " BILLONES ";
				break;
			    case 1:
				if (trim(substr($XAUX, $xz * 6, 6)) == "1")
				    $xcadena.= "UN MILLON ";
				else
				    $xcadena.= " MILLONES ";
				break;
			    case 2:
				if ($xcifra < 1) {
				    $xcadena = "CERO NUEVOS SOLES $xdecimales/100";
				}
				if ($xcifra >= 1 && $xcifra < 2) {
				    $xcadena = "UN NUEVOS SOLES $xdecimales/100";
				}
				if ($xcifra >= 2) {
				    $xcadena.= " NUEVOS SOLES $xdecimales/100"; //
				}
				break;
			} // endswitch ($xz)
		    } // ENDIF (trim($xaux) != "")
		    // ------------------      en este caso, para México se usa esta leyenda     ----------------
		    $xcadena = str_replace("VEINTI ", "VEINTI", $xcadena); // quito el espacio para el VEINTI, para que quede: VEINTICUATRO, VEINTIUN, VEINTIDOS, etc
		    $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
		    $xcadena = str_replace("UN UN", "UN", $xcadena); // quito la duplicidad
		    $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
		    $xcadena = str_replace("BILLON DE MILLONES", "BILLON DE", $xcadena); // corrigo la leyenda
		    $xcadena = str_replace("BILLONES DE MILLONES", "BILLONES DE", $xcadena); // corrigo la leyenda
		    $xcadena = str_replace("DE UN", "UN", $xcadena); // corrigo la leyenda
		} // ENDFOR ($xz)
		return trim($xcadena);
    }
     
    // END FUNCTION
     
    public function subfijo($xx)
    { // esta función regresa un subfijo para la cifra
		$xx = trim($xx);
		$xstrlen = strlen($xx);
		if ($xstrlen == 1 || $xstrlen == 2 || $xstrlen == 3)
		    $xsub = "";
		//
		if ($xstrlen == 4 || $xstrlen == 5 || $xstrlen == 6)
		    $xsub = "MIL";
		//
		return $xsub;
    }




    public function logData($rowInsert, $action, $table, $comment){
    	$logg = new LogRegistro;
    	$usuario=Usuario::findOne(\Yii::$app->user->id);
		$logg->UserID = $usuario->username;
		$logg->RowAffectedID = $rowInsert;
		$logg->Action = $action;
		$logg->TableAffected = $table;
		$logg->RegistryDate = date('Ymd H:m:s');
		$logg->Comment = $comment;
		$logg->save();
    }

    public function ObtenerCertificacion($codigoProyecto){
        // $logg = new LogRegistro;
        // $certificacin = DetalleCertificacionPresupuestal::find()->where("Codigo ='".$codigoProyecto."'");
        $certificacin=DetalleCertificacionPresupuestal::find()
                    ->where('Codigo=:CodigoProyecto',[':CodigoProyecto'=>$codigoProyecto])->one();
        return $certificacin;
    }


    public function validaAnual($anualCode){
    	$mensaje = "";
    	$GetAnualIntegra=\Yii::$app->db->createCommand('BEGIN DECLARE @a VARCHAR(255); EXEC SP_OBTENER_ESTADO_ANUAL :SECUENCIAL select @a as MEF_ESTADO end;')
                ->bindValue(':SECUENCIAL',$anualCode)
                ->queryOne();
        switch (trim($GetAnualIntegra['MEF_ESTADO'])) {
            case 'X':
                $mensaje = '<strong style="color:red">CP ANULADO</strong>';
                break;
            case '':
                $mensaje = '<strong style="color:blue">CP ENVIADO</strong>';
                break;
            case 'R':
                $mensaje = '<strong style="color:red">CP RECHAZADO</strong>';
                break;
            case 'A':
                $mensaje = '<strong style="color:green">CP APROBADO</strong>';
                break;
            case 'T':
                $mensaje = '<strong style="color:blue">CP Transmisión</strong>';
                break;
            case 'D':
                $mensaje = '<strong>D Integra</strong>';
                break;
        }

        return $mensaje;
    }

    public function validaAnualEstado($anualCode){
    	$GetAnualIntegra=\Yii::$app->db->createCommand('BEGIN DECLARE @a VARCHAR(255); EXEC SP_OBTENER_ESTADO_ANUAL :SECUENCIAL select @a as MEF_ESTADO end;')
                ->bindValue(':SECUENCIAL',$anualCode)
                ->queryOne();
        return $GetAnualIntegra['MEF_ESTADO'];
    }

    public function validaMensualizado($MensualCode){
    	$mensaje = "";
    	// SI existe el mensualizado no debe de enviar 
    	$GetMensualIntegra=\Yii::$app->db->createCommand('BEGIN DECLARE @a VARCHAR(255); EXEC SP_OBTENER_ESTADO_MENSUALIZADO :CODIGO_SIGA select @a as MEF_ESTADO end;')
                ->bindValue(':CODIGO_SIGA',$MensualCode)
                ->queryOne();
        switch (trim($GetMensualIntegra['MEF_ESTADO'])) {
            case 'X':
                $mensaje = '<strong style="color:red">ANULADO</strong>';
                break;
            case '':
                $mensaje = '<strong style="color:blue">ENVIADO</strong>';
                break;
            case 'N':
                $mensaje = '<strong style="color:blue">ENVIADO</strong>';
                break;
            case 'R':
                $mensaje = '<strong style="color:red">RECHAZADO</strong>';
                break;
            case 'A':
                $mensaje = '<strong style="color:green">APROBADO</strong>';
                break;
            case 'T':
                $mensaje = '<strong style="color:blue">EN TRANSMISIÓN</strong>';
                break;
            case 'D':
                $mensaje = '<strong>D Integra</strong>';
                break;
            case 'P':
                $mensaje = '<strong>PENDIENTE</strong>';
                break;
        }

        return $mensaje;
    }

    public function seguimiento($codigoProyecto = null, $ID =null, $rol = null){
        $db = Yii::$app->db;
        $detSeguimiento = $db->createCommand("SELECT SeguimientoCompromisoPresupuestal.*,Usuario.username,UsuarioRol.RolID,
            DATEDIFF(day, SeguimientoCompromisoPresupuestal.FechaInicio,SeguimientoCompromisoPresupuestal.FechaFin ) Dias
            FROM SeguimientoCompromisoPresupuestal 
            INNER JOIN Usuario ON Usuario.ID=SeguimientoCompromisoPresupuestal.UsuarioID
            INNER JOIN UsuarioRol ON UsuarioRol.UsuarioID=Usuario.ID
            WHERE SeguimientoCompromisoPresupuestal.CompromisoPresupuestalID 
            IN (SELECT ID FROM CompromisoPresupuestal WHERE Codigo = '".$codigoProyecto."'
            AND Situacion NOT IN (6))
            AND UsuarioRol.RolID = ".$rol."
            AND SeguimientoCompromisoPresupuestal.CompromisoPresupuestalID ='".$ID."'")->queryOne();

        return $detSeguimiento;
    }



    public function SeguimientoCompromiso($id){
        $respuesta =\Yii::$app->db->createCommand('BEGIN EXEC SEGUIMIENTO_COMPROMISO_PRESUPUESTAL :CODIGO END;')
                ->bindValue(':CODIGO',$id)
                ->queryOne();

        return $respuesta;
    }

    public function colorSeg($id){
        $color = '';
        switch ($id) {
            case '0':
                $color = 'silver';
                break;
            case '':
                $color = 'silver';
                break;
            case '1':
                $color = 'green';
                break;
            case '2':
                $color = 'yellow';
                break;
            case '3':
                $color = 'red';
                break;
        }
        return $color;
    }

    public function valoreVacion($valor){
        $valo = '';
        if(!empty($valor) || !isset($valor) ):
            $valo = $valor;
        endif;
        return $valo;
    }


    public function vacioDias($valor){
        $valo = '0';
        if(!empty($valor) ):
            $valo = $valor;
        endif;
        return $valo;
    }

}
