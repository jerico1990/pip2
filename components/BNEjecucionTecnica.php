<?php

namespace app\components;
use Yii;
use app\models\EjecucionTecnica;


/*********************************************************************
* Descripcion	: Contiene la logica del negocio a la que esta 
*				  sugeta la entidad EjecucionTecnica.
* Creado 		: 06/09/2017
*********************************************************************/
class BNEjecucionTecnica extends \yii\base\Object
{
	private $_DAEjecucionTecnica;

	public function __construct(){
		$this->_DAEjecucionTecnica = new DAEjecucionTecnica();
	}

	//-----------------------------------------------------
	// Descripcion	: Inserta un registro 
	// Creado 		: fcastro - 06/09/2017
	//-----------------------------------------------------
	public function Registrar($EjecucionTecnica){
		
		if($EjecucionTecnica->IdEjecucionTecnica == -1){
			$EjecucionTecnica->IdEjecucionTecnica = $EjecucionTecnica->IdProgramacionTecnica;
			$s = $this->_DAEjecucionTecnica->Insertar($EjecucionTecnica); 
			/*if($s > 0){

			}*/	
		} else {
			$s = $this->_DAEjecucionTecnica->Actualizar($EjecucionTecnica); 	
			/*if($s > 0){

			}*/	
		}		
		return $EjecucionTecnica;
	}
	//-----------------------------------------------------
	// Descripcion	: Elimina un registro 
	// Creado 		: fcastro - 06/09/2017
	//-----------------------------------------------------
	public function Eliminar($IdEjecucionTecnica){
		$s = $this->_DAEjecucionTecnica->Eliminar($IdEjecucionTecnica);
		return $s;
	}

	//-----------------------------------------------------
	// Descripcion	: Inserta un documento de sustento de la ejecucion
	// Creado 		: fcastro - 06/09/2017
	//-----------------------------------------------------
	public function RegistrarSustento($IdSustento,$IdEjecucionTecnica, $Ruta, $Nombre){
		
		if($IdSustento == -1){
			$IdSustento = $this->_DAEjecucionTecnica->InsertarSustento($IdEjecucionTecnica, $Ruta, $Nombre); 
			/*if($s > 0){

			}*/	
		} else {
			//$this->_DAEjecucionTecnica->Actualizar($EjecucionTecnica); 	
			/*if($s > 0){

			}*/	
		}		
		return $IdSustento;
	}
	//-----------------------------------------------------
	// Descripcion	: Elimina uno o varios registros
	// Creado 		: fcastro - 25/09/2017
	//-----------------------------------------------------
	public function EliminaSustentos($IdAdjuntos){
		$s = $this->_DAEjecucionTecnica->EliminaSustentos($IdAdjuntos);
		return $s;
	}
}
