<?php

namespace app\components;
use Yii;
use app\models\ProgramacionTecnica;

//======================================================================
// Descripcion	: Contiene los metodos de acceso a la base de datos 
//				  referentes a la tabla ProgramacionTecnica.
// Creado 		: fcastro - 06/09/2017
//======================================================================
class DAProgramacionTecnica extends \yii\base\Object
{
	//-----------------------------------------------------
	// Descripcion	: Filtra la informacion por el campo ProgramacionTipo 
	// Creado 		: fcastro - 06/09/2017
	//-----------------------------------------------------
	public function ConsultarXtipo($ProgramacionTipo){
		$_Programacion=\Yii::$app->db->createCommand('BEGIN EXEC pProgramacionTecnica_Sel_Tipo :pvProgramacionTipo END;')
			->bindValue(':pvProgramacionTipo',$ProgramacionTipo)
			->queryAll();
		return $_Programacion;
	}
	//-----------------------------------------------------
	// Descripcion	: Filtra la informacion por el campo ProgramacionTipo 
	// de los experimentos que no estan ejecutados 
	// Creado 		: fcastro - 06/09/2017
	//-----------------------------------------------------
	public function ConsultarXtipo_NoEjecutado($ProgramacionTipo){
		$_Programacion=\Yii::$app->db->createCommand('BEGIN EXEC pProgramacionTecnica_Sel_Tipo_NoEjecutado :pvProgramacionTipo END;')
			->bindValue(':pvProgramacionTipo',$ProgramacionTipo)
			->queryAll();
		return $_Programacion;
	}
	//-----------------------------------------------------
	// Descripcion	: Devuleve un registro de ProgramacionTecnica por el primary key
	// Creado 		: fcastro - 08/09/2017
	//-----------------------------------------------------
	public function Selecciona($IdProgramacionTecnica){
		$_Programacion=\Yii::$app->db->createCommand('BEGIN EXEC pProgramacionTecnica_Sel :piIdProgramacionTecnica END;')
			->bindValue(':piIdProgramacionTecnica',$IdProgramacionTecnica)
			->queryOne();
		return $_Programacion;
	}
	//-----------------------------------------------------
	// Descripcion	: Inserta un registro 
	// Creado 		: fcastro - 06/09/2017
	//-----------------------------------------------------
	public function Insertar($ProgramacionTecnicaModel){
		$_Programacion=\Yii::$app->db->createCommand('BEGIN EXEC pProgramacionTecnica_Ins :piProyectoId, :piActividadID, :piCorrelativo, :pvTitulo, :pvIntroduccionDescripcion, :piTipologia, 
			:pvLugarDesarrollo, :pdtFechaInicio, :pdtFechaFin, :piProbabilidad, :pvUniversidadNombre, :pvRevistaPublicacion, 
			:pvRequiereAsesoramiento, :piEsProgramado, :piEstado, :pdtFechaRegistro, :pvProgramacionTipo, :pvObservacionInvestigador, :pvBeneficiario END;')
			->bindValue(':piProyectoId', $ProgramacionTecnicaModel->ProyectoId)
			->bindValue(':piActividadID', $ProgramacionTecnicaModel->ActividadID)
			->bindValue(':piCorrelativo', $ProgramacionTecnicaModel->Correlativo)
			->bindValue(':pvTitulo',$ProgramacionTecnicaModel->Titulo)
			->bindValue(':pvIntroduccionDescripcion', $ProgramacionTecnicaModel->IntroduccionDescripcion)
			->bindValue(':piTipologia', $ProgramacionTecnicaModel->Tipologia)
			->bindValue(':pvLugarDesarrollo', $ProgramacionTecnicaModel->LugarDesarrollo)
			->bindValue(':pdtFechaInicio', \Yii::$app->tools->dateFormat($ProgramacionTecnicaModel->FechaInicio,'Ymd'))
			->bindValue(':pdtFechaFin', \Yii::$app->tools->dateFormat($ProgramacionTecnicaModel->FechaFin,'Ymd'))
			->bindValue(':piProbabilidad', $ProgramacionTecnicaModel->Probabilidad)
			->bindValue(':pvUniversidadNombre',$ProgramacionTecnicaModel->UniversidadNombre)
			->bindValue(':pvRevistaPublicacion',$ProgramacionTecnicaModel->RevistaPublicacion)
			->bindValue(':pvRequiereAsesoramiento',$ProgramacionTecnicaModel->RequiereAsesoramiento)
			->bindValue(':piEsProgramado',$ProgramacionTecnicaModel->EsProgramado)
			->bindValue(':piEstado',$ProgramacionTecnicaModel->Estado)
			->bindValue(':pdtFechaRegistro', $ProgramacionTecnicaModel->FechaRegistro)
			->bindValue(':pvProgramacionTipo', $ProgramacionTecnicaModel->ProgramacionTipo)
			->bindValue(':pvObservacionInvestigador', $ProgramacionTecnicaModel->ObservacionInvestigador)
			->bindValue(':pvBeneficiario', $ProgramacionTecnicaModel->Beneficiario)
			->queryOne();

		return $_Programacion['IdProgramacionTecnica'];
	}
	//-----------------------------------------------------
	// Descripcion	: Actualiza un registro 
	// Creado 		: fcastro - 11/09/2017
	//-----------------------------------------------------
	public function Actualizar($ProgramacionTecnicaModel){
		$_Programacion=\Yii::$app->db->createCommand('BEGIN EXEC pProgramacionTecnica_Upd :piIdProgramacionTecnica, :piActividadID, :pvTitulo, :pvIntroduccionDescripcion, :piTipologia, 
			:pvLugarDesarrollo, :pdtFechaInicio, :pdtFechaFin, :piProbabilidad, :pvObservacionInvestigador, :pvBeneficiario END;')
			->bindValue(':piIdProgramacionTecnica', $ProgramacionTecnicaModel->IdProgramacionTecnica)
			->bindValue(':piActividadID', $ProgramacionTecnicaModel->ActividadID)
			->bindValue(':pvTitulo',$ProgramacionTecnicaModel->Titulo)
			->bindValue(':pvIntroduccionDescripcion', $ProgramacionTecnicaModel->IntroduccionDescripcion)
			->bindValue(':piTipologia', $ProgramacionTecnicaModel->Tipologia)
			->bindValue(':pvLugarDesarrollo', $ProgramacionTecnicaModel->LugarDesarrollo)
			->bindValue(':pdtFechaInicio', \Yii::$app->tools->dateFormat($ProgramacionTecnicaModel->FechaInicio,'Ymd'))
			->bindValue(':pdtFechaFin', \Yii::$app->tools->dateFormat($ProgramacionTecnicaModel->FechaFin,'Ymd'))
			->bindValue(':piProbabilidad', $ProgramacionTecnicaModel->Probabilidad)
			->bindValue(':pvObservacionInvestigador', $ProgramacionTecnicaModel->ObservacionInvestigador)
			->bindValue(':pvBeneficiario', $ProgramacionTecnicaModel->Beneficiario)
			->queryOne();

		return $_Programacion['FilasAfectadas'];
	}
	//-----------------------------------------------------
	// Descripcion	: Elimina un registro 
	// Creado 		: fcastro - 06/09/2017
	//-----------------------------------------------------
	public function Eliminar($IdProgramacionTecnica){
		$_Programacion=\Yii::$app->db->createCommand('BEGIN EXEC pProgramacionTecnica_Del :piIdProgramacionTecnica END;')
			->bindValue(':piIdProgramacionTecnica', $IdProgramacionTecnica)
			->queryOne();

		return $_Programacion['FilasAfectadas'];
	}
	//-----------------------------------------------------
	// Descripcion	: Actualiza algunos campos de un registro 
	// Creado 		: fcastro - 22/09/2017
	//-----------------------------------------------------
	public function ActualizarDesdeEjecucion($ProgramacionTecnicaModel){
		$_Programacion=\Yii::$app->db->createCommand('BEGIN EXEC pProgramacionTecnica_Upd_Ejecucion :piIdProgramacionTecnica, :pvIntroduccionDescripcion, :pvLugarDesarrollo, :pdtFechaInicio, :pdtFechaFin, :piActividadID, :piTipologia, :pvRevistaPublicacion, :pvUniversidadNombre, :pvRequiereAsesoramiento END;')
			->bindValue(':piIdProgramacionTecnica', $ProgramacionTecnicaModel->IdProgramacionTecnica)
			->bindValue(':pvIntroduccionDescripcion', $ProgramacionTecnicaModel->IntroduccionDescripcion)
			->bindValue(':pvLugarDesarrollo', $ProgramacionTecnicaModel->LugarDesarrollo)
			->bindValue(':pdtFechaInicio', \Yii::$app->tools->dateFormat($ProgramacionTecnicaModel->FechaInicio,'Ymd'))
			->bindValue(':pdtFechaFin', \Yii::$app->tools->dateFormat($ProgramacionTecnicaModel->FechaFin,'Ymd'))
			->bindValue(':piActividadID', $ProgramacionTecnicaModel->ActividadID)
			->bindValue(':piTipologia', $ProgramacionTecnicaModel->Tipologia)
			->bindValue(':pvRevistaPublicacion', $ProgramacionTecnicaModel->RevistaPublicacion)
			->bindValue(':pvUniversidadNombre', $ProgramacionTecnicaModel->UniversidadNombre)
			->bindValue(':pvRequiereAsesoramiento', $ProgramacionTecnicaModel->RequiereAsesoramiento)
			->queryOne();

		return $_Programacion['FilasAfectadas'];
	}
}
