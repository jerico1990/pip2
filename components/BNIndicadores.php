<?php

namespace app\components;
use Yii;
use app\models\EjecucionTecnica;


/*********************************************************************
* Descripcion	: Contiene la logica del negocio a la que esta 
*				  sugeta la entidad EjecucionTecnica.
* Creado 		: 06/09/2017
*********************************************************************/
class BNIndicadores extends \yii\base\Object
{
	private $_DAIndicadores;

	public function __construct(){
		$this->_DAIndicadores = new DAIndicadores();
	}

	//-----------------------------------------------------
	// Descripcion	: Inserta un registro 
	// Creado 		: fcastro - 06/09/2017
	//-----------------------------------------------------
	public function Registrar($EjecucionTecnica){
		
		//if($EjecucionTecnica->IdEjecucionTecnica == -1){
			$s = $this->_DAIndicadores->Insertar($EjecucionTecnica); 
			/*if($s > 0){

			}*/	
		/*} else {
			$s = $this->_DAIndicadores->Actualizar($EjecucionTecnica); 	
			//if($s > 0){

			//}	
		}*/		
		return $EjecucionTecnica;
	}
	//-----------------------------------------------------
	// Descripcion	: Elimina un registro 
	// Creado 		: fcastro - 06/09/2017
	//-----------------------------------------------------
	public function Eliminar($EjecucionTecnica){
		$s = $this->_DAIndicadores->Eliminar($EjecucionTecnica);
		return $s;
	}
}
