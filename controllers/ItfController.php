<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\PasoCritico;
use app\models\Proyecto;

use app\models\Siaft;
use app\models\Viatico;
use app\models\Orden;
use app\models\Usuario;
use app\models\Persona;
use app\models\Investigador;
use app\models\InformacionGeneral;
use app\models\GastoItf;
use app\models\LibroBanco;

class ItfController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout='vacio';
        
        return $this->render('index');
    }
    
    public function actionInformeTecnico($PasoCriticoID=null)
    {
        $this->layout='estandar';
        $pasoCritico=PasoCritico::findOne($PasoCriticoID);
        $proyecto=Proyecto::findOne($pasoCritico->ProyectoID);
        $informacion=InformacionGeneralController::InformacionGeneral($proyecto->InvestigadorID);
        
        return $this->render('informe-tecnico',['informacion'=>$informacion]);
    }
    
    public function actionGastosDefinitivos()
    {
        $this->layout='estandar';

        // $usuario=Usuario::findOne(\Yii::$app->user->id);
        //     $datosInvestigador=Persona::findOne($usuario->PersonaID);
        //     $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        //     $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        //     $CodigoProyecto=$informacionGeneral->Codigo;


        // $definitivoViatico = Viatico::find()
        //     ->select('Siaft.*,Viatico.Dni,DetalleRequerimiento.CodigoMatriz')
        //     ->innerJoin('Siaft','Siaft.Orden = Viatico.ID')
        //     ->innerJoin('Requerimiento','Requerimiento.TipoRequerimiento = 4')
        //     ->innerJoin('DetalleRequerimiento','Requerimiento.ID = DetalleRequerimiento.RequerimientoID')
        //     ->where('Siaft.Fase=:fase AND Requerimiento.CodigoProyecto=:CodigoProyecto',[':fase'=>'G',':CodigoProyecto'=>$CodigoProyecto])
        //     ->all();

        // $definitivoOrden = Orden::find()
        //     ->select('Siaft.*,Orden.Ruc,Orden.TipoOrden,DetalleRequerimiento.CodigoMatriz')
        //     ->innerJoin('Siaft','Siaft.Orden = Orden.ID')
        //     ->innerJoin('Requerimiento','Requerimiento.TipoRequerimiento = Orden.TipoOrden')
        //     ->innerJoin('DetalleRequerimiento','Requerimiento.ID = DetalleRequerimiento.RequerimientoID')
        //     ->where('Siaft.Fase=:fase AND Requerimiento.CodigoProyecto=:CodigoProyecto',[':fase'=>'G',':CodigoProyecto'=>$CodigoProyecto])
        //     ->all();


        // $dato = $this->conexion('GASTOS_PIP2');
        // while($row = oci_fetch_array($dato, OCI_ASSOC)) {
        //     $lib = LibroBanco::find()->where(['SECU_COMP_COP'=>$row['SECU_COMP_COP'] ])->one();
        //     if(empty($lib)){
        //         $gastos = new GastoItf;
        //         $gastos->FECH_EMIS_COP = isset($row['FECH_EMIS_COP']) ? Yii::$app->tools->dateFormat($row['FECH_EMIS_COP'],'Y-m-d'):NULL;
        //         $gastos->FECH_PAGO_COP = isset($row['FECH_PAGO_COP']) ? Yii::$app->tools->dateFormat($row['FECH_PAGO_COP'],'Y-m-d'): NULL;
        //         $gastos->NUME_COMP_COP = isset($row['NUME_COMP_COP']) ? $row['NUME_COMP_COP']:'';
        //         $gastos->ANNO_EJEC_EJE = isset($row['ANNO_EJEC_EJE']) ? $row['ANNO_EJEC_EJE']:NULL;
        //         $gastos->FECH_DOCU_DOC = isset($row['FECH_DOCU_DOC']) ? Yii::$app->tools->dateFormat($row['FECH_DOCU_DOC']),'Y-m-d'):NULL;
        //         $gastos->ABRE_DOCU_TPD = isset($row['ABRE_DOCU_TPD']) ? htmlentities($row['ABRE_DOCU_TPD'],ENT_QUOTES):'';
        //         $gastos->SERI_DOCU_DOC = isset($row['SERI_DOCU_DOC']) ? htmlentities($row['SERI_DOCU_DOC'],ENT_QUOTES):'';
        //         $gastos->NUME_DOCU_DOC = isset($row['NUME_DOCU_DOC']) ? htmlentities($row['NUME_DOCU_DOC'],ENT_QUOTES):'';
        //         $gastos->NUM_RUC_CAB   = isset($row['NUM_RUC_CAB'])   ? htmlentities($row['NUM_RUC_CAB'],ENT_QUOTES):'';
        //         $gastos->DESC_ANEX_ANX = isset($row['DESC_ANEX_ANX']) ? htmlentities($row['DESC_ANEX_ANX'],ENT_QUOTES):'';
        //         $gastos->TEXT_REFE_CAB = isset($row['TEXT_REFE_CAB']) ? htmlentities($row['TEXT_REFE_CAB'],ENT_QUOTES):'';
        //         $gastos->MNTO_NACI_CAB = isset($row['MNTO_NACI_CAB']) ? htmlentities($row['MNTO_NACI_CAB'],ENT_QUOTES):'';
        //         $gastos->ESTADO = 1;
        //         $gastos->save();
        //     }
        //     $i++;
        // }


        $definitivos = GastoItf::find()->where("FECH_EMIS_COP IS NULL ")->all();

        return $this->render('gastos-definitivos',['definitivos'=> $definitivos]);
    }

    public function actionNuevoGastoDefinitivo()
    {
        $this->layout='estandar';
        return $this->render('nuevo-gasto-definitivo');
    }


    
    public function actionGastosPendientes()
    {
        $this->layout='estandar';
        $pendientes = GastoItf::find()->where("FECH_EMIS_COP IS NOT NULL ")->all();
        return $this->render('gastos-pendientes',['pendientes'=> $pendientes]);
    }

    public function actionNuevoGastoNoMonetarios()
    {
        $this->layout='estandar';
        return $this->render('nuevo-gasto-no-monetarios');
    }


    
    public function actionInformeFinanciero($PasoCriticoID)
    {
        $this->layout='estandar';
        $pasoCritico=PasoCritico::findOne($PasoCriticoID);
        $proyecto=Proyecto::findOne($pasoCritico->ProyectoID);
        $informacion=InformacionGeneralController::InformacionGeneral($proyecto->InvestigadorID);
        var_dump($informacion);die;
        return $this->render('informe-financiero',['informacion'=>$informacion]);
    }

    public function actionLibroBanco(){
        $this->layout='estandar';
        // $dato = $this->conexion('LIBROBANCO_PIP2');
        // $i=0;
        // while($row = oci_fetch_array($dato, OCI_ASSOC)) {
        //     $lib = LibroBanco::find()->where(['SECU_COMP_COP'=>$row['SECU_COMP_COP'] ])->one();
        //     if(empty($lib)){
        //         $libros =new LibroBanco;
        //         $libros->SECU_COMP_COP = isset($row['SECU_COMP_COP']) ? htmlentities($row['SECU_COMP_COP'],ENT_QUOTES):'';
        //         $libros->FECH_EMIS_COP = isset($row['FECH_EMIS_COP']) ? Yii::$app->tools->dateFormat($row['FECH_EMIS_COP'],'Y-m-d'): date('Y-m-d');
        //         $libros->FECH_PAGO_COP = isset($row['FECH_PAGO_COP']) ? Yii::$app->tools->dateFormat($row['FECH_PAGO_COP'],'Y-m-d'):date('Y-m-d');
        //         $libros->NUM_CHEQ_CHE  = isset($row['NUME_CHEQ_CHE']) ? htmlentities($row['NUME_CHEQ_CHE'],ENT_QUOTES):NULL;
        //         $libros->DESC_ANEX_ANX = isset($row['DESC_ANEX_ANX']) ? htmlentities($row['DESC_ANEX_ANX'],ENT_QUOTES):'';
        //         $libros->CONC_PAGO_COP = isset($row['CONC_PAGO_COP']) ? htmlentities($row['CONC_PAGO_COP'],ENT_QUOTES):'';
        //         $libros->MONT_COMP_COP = isset($row['MONT_COMP_COP']) ? htmlentities($row['MONT_COMP_COP'],ENT_QUOTES):'0';
        //         $libros->MONT_SOLE_CHE = isset($row['MONT_SOLE_CHE']) ? htmlentities($row['MONT_SOLE_CHE'],ENT_QUOTES):'0';
        //         $libros->CODI_BANC_BAN = isset($row['CODI_BANC_BAN']) ? htmlentities($row['CODI_BANC_BAN'],ENT_QUOTES):'';
        //         $libros->CODI_CNTA_CTA = isset($row['CODI_CNTA_CTA']) ? htmlentities($row['CODI_CNTA_CTA'],ENT_QUOTES):'';
        //         $libros->NUME_CORR_CHE = isset($row['NUME_CORR_CHE']) ? htmlentities($row['NUME_CORR_CHE'],ENT_QUOTES):'';
        //         $libros->NUME_SIAF_COP = isset($row['NUME_SIAF_COP']) ? htmlentities($row['NUME_SIAF_COP'],ENT_QUOTES):'';
        //         $libros->ESTADO = 1;
        //         $libros->save();
        //     }
        //     $i++;
        // }

        // echo $i;
        $libro = LibroBanco::find()
        // ->where("NUM_CHEQ_CHE !='' ")
        // ->where("NUM_CHEQ_CHE IS NOT NULL ")
        ->all();
        // print_r($dd);
        // die();

        return $this->render('libro-banco',['libro'=>$libro]);
    }


    private function conexion($nombrestore,$arraydatos=null)
    {
        $usuario = "SIGA01";
        $clave = "SIGA01";
        $bd = '(DESCRIPTION=( ADDRESS_LIST= (ADDRESS= (PROTOCOL=TCP) (HOST=172.168.3.43) (PORT=1521)))( CONNECT_DATA=(SID=XE) ))';
       
        if (function_exists('oci_connect')) {
            $conec =  oci_connect($usuario, $clave, $bd);
        } else {
            echo "Las funciones de oci_connect no están disponibles.<br />\n";
        }
        
        if (!$conec) {
            echo 'Error en conecci&oacute;n a la BASE DE DATOS oracle...<br>';
        }else{
            $stid = oci_parse ($conec, "alter session set nls_date_format = 'dd/mm/yyyy'");
            oci_execute ($stid);
        }

        $stid = oci_parse($conec, 'SELECT * FROM '.$nombrestore);
        oci_execute($stid);
        return  $stid;
    }


    public function actionComida(){

        $arr = array(
            'CHIFA',
            'POLLO',
            'BEMBOS'
            );

        var_dump( array_rand($arr,1) );

        die();
    }
    
}
