<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Usuario;
use app\models\Persona;
use app\models\Investigador;
use app\models\Proyecto;
use app\models\Componente;
use app\models\Actividad;
use app\models\Poa;
use app\models\InformacionGeneral;
use app\models\CronogramaProyecto;
use app\models\ActRubroElegible;
use app\models\CronogramaComponente;
use app\models\CronogramaActividad;
use app\models\CronogramaActRubroElegible;
use app\models\RubroElegible;
use app\models\AreSubCategoria;
use app\models\CronogramaAreSubCategoria;
use app\models\MarcoLogicoActividad;
use app\models\Pat;
use app\models\AreSubCategoriaObservacion;
use app\models\EntidadParticipante;
use yii\web\UploadedFile;
use app\models\PasoCritico;
use app\models\Tarea;


class RecursosPoaController extends Controller
{
    /**
     * @inheritdoc
     */
    
    /*public $investigador = NULL;
    public function init(){
        // parent::__construct();
        $usuario            = Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador  = Persona::findOne($usuario->PersonaID);
        $investigador       = Investigador::find()->where(['UsuarioID'=>$usuario->id])->one();
        $this->investigador = $investigador->ID;
    }*/

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex($investigadorID=null)
    {
        $this->layout='estandar';
        if($investigadorID==NULL)
        {
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        }
        else
        {
            $investigador=Investigador::findOne($investigadorID);
            $usuario=Usuario::findOne($investigador->UsuarioID);
        }
        
        if(!$investigadorID && \Yii::$app->user->identity->rol!=7)
        {
            return $this->redirect(['panel/index']) ;
        }
        
        $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        //$usuario=Usuario::findOne(\Yii::$app->user->id);
        //$datosInvestigador=Persona::findOne($usuario->PersonaID);
        //$investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID and Estado=1',[':ProyectoID'=>$proyecto->ID])->one();
        $componentes=Componente::find()->where('PoaID=:PoaID',[':PoaID'=>$poa->ID])->all();
        return $this->render('index',['componentes'=>$componentes,'usuario'=>$usuario,'poa'=>$poa,'investigadorID'=>$investigador->ID,'informacionGeneral'=>$informacionGeneral]);
    }


    public function actionRecursosForm(){
        $this->layout='vacio';
        
        return $this->render('recursos/_recursos_form');
    }

    function microtime_float() {
        list($useg, $seg) = explode(" ", microtime());
        return ((float)$useg + (float)$seg);
    }

    /**
     * Rescursos JSON 
     * @return [type] [description]
     */
    public function actionRecursosJson($codigo=null){
        $this->layout='vacio';
        // Proyectos
        $usuario=Usuario::find()->where('username=:username',[':username'=>$codigo])->one();
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        
        
 
        $tiempo_inicio = $this->microtime_float();
        $proyecto = Poa::find()
            ->select('Poa.ID, Poa.ProyectoID')
            ->innerJoin('Proyecto','Poa.ProyectoID = Proyecto.ID')
            ->where(['Proyecto.InvestigadorID'=> $investigador->ID,'Poa.Estado' => 1])
            ->one();


        $proyectox = array(
            'ID'             => $proyecto->ProyectoID, 
            'Componentes'    => $this->get_objetivo($proyecto->ID),
            'Cronogramas'    => $this->getCronogramaProyecto($proyecto->ProyectoID,'proyecto')
        );

        $tiempo_fin = $this->microtime_float();
        $tiempo = bcsub($tiempo_fin, $tiempo_inicio, 4);

        if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start();
        array_push($proyectox, $tiempo);
        echo json_encode($proyectox);
    }


    public function actionRecursosJsonPa(){
        $this->layout='vacio';
        // Proyectos
        $usuario            = Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador  = Persona::findOne($usuario->PersonaID);
        $investigador       = Investigador::find()->where(['UsuarioID'=>$usuario->id])->one();
        
        $proyecto = Poa::find()
        ->select('Poa.ID, Poa.ProyectoID')
        ->innerJoin('Proyecto','Poa.ProyectoID = Proyecto.ID')
        ->where(['Proyecto.InvestigadorID'=> $investigador->ID,'Poa.Estado' => 1])
        ->one();

        $proyectox = array(
            'ID'             => $proyecto->ProyectoID, 
            'Componentes'    => $this->get_objetivo($proyecto->ID),
            // 'Cronogramas'    => $this->getCronogramaProyecto($proyecto->ProyectoID,'proyecto')
        );
         echo json_encode($proyectox); die();
    }


    private function get_objetivo($idPoa){
        $jsonComponente = array();
        $component = Componente::find()
                //->select('Componente.ID,Componente.Nombre,Componente.Correlativo,sum(AreSubCategoria.MetaFisica*AreSubCategoria.CostoUnitario) TotalObjetivo')
                ->select('Componente.ID,Componente.Nombre,Componente.Correlativo')
                //->innerJoin('Actividad','Actividad.ComponenteID=Componente.ID')
                //->innerJoin('ActRubroElegible','ActRubroElegible.ActividadID=Actividad.ID')
                //->innerJoin('AreSubCategoria','AreSubCategoria.ActRubroElegibleID=ActRubroElegible.ID')
                ->where(['Componente.Estado' => 1, 'Componente.PoaID' => $idPoa])
                ->groupBy('Componente.ID,Componente.Nombre,Componente.Correlativo')
                ->orderBy('Componente.Correlativo asc')
                ->all();
        if(!empty($component) ){
            foreach ($component as $comp) {
                
                $componente = Componente::find()
                ->select('sum(AreSubCategoria.MetaFisica*AreSubCategoria.CostoUnitario) TotalObjetivo')
                ->innerJoin('Actividad','Actividad.ComponenteID=Componente.ID')
                ->innerJoin('ActRubroElegible','ActRubroElegible.ActividadID=Actividad.ID')
                ->innerJoin('AreSubCategoria','AreSubCategoria.ActRubroElegibleID=ActRubroElegible.ID')
                ->where(['Componente.ID' => $comp->ID,'Actividad.Estado'=>1,'Componente.Estado'=>1])
                ->one();
                
                $jsonComponente[] = array(
                    'ID'            => $comp->ID, 
                    'Nombre'        => $comp->Nombre,
                    'TotalObjetivo' => $componente->TotalObjetivo,
                    'Correlativo'   => $comp->Correlativo,
                    'Actividades'   => $this->get_actividades($comp->ID), 
                    //'Cronogramas'   => $this->getCronogramaProyecto($comp->ID,'componente')
                );

            }
        }
        return $jsonComponente;
    }

    private function get_actividades($idComponente){
        $jsonActividades = array();
        $actv = Actividad::find()
                ->where(['Estado' => 1, 'ComponenteID' => $idComponente])
                ->orderBy('Correlativo asc')
                ->all();
        if(!empty($actv) && $actv){
            foreach ($actv as $actvd) {
                $jsonActividades[] = array(
                    'ID'            => $actvd->ID, 
                    'ComponenteID'  => $actvd->ComponenteID, 
                    'Nombre'        => $actvd->Nombre, 
                    'UnidadMedida'  => $this->get_actividadesmarco($actvd->ID)->IndicadorUnidadMedida, 
                    'CostoUnitario' => $actvd->CostoUnitario, 
                    'MetaFisica'    => $this->get_actividadesmarco($actvd->ID)->IndicadorMetaFisica,
                    'ActRubroElegibles' => $this->get_actRubroElegible($actvd->ID),
                    //'Cronogramas'       => $this->getCronogramaProyecto($actvd->ID,'actividad')
                );
            }
        }
        else
        {
            $jsonActividades[] = array(
                    'ID'            => '', 
                    'ComponenteID'  => '', 
                    'Nombre'        => '', 
                    'UnidadMedida'  => '', 
                    'CostoUnitario' => '', 
                    'MetaFisica'    => '',
                    'ActRubroElegibles' => '',
                    //'Cronogramas'       => $this->getCronogramaProyecto($actvd->ID,'actividad')
                );
        }
        return $jsonActividades;
    }

    private function get_actRubroElegible($idActividad){
        $jsonActividades = array();
        $rubro = ActRubroElegible::find()
                ->where(['ActividadID' => $idActividad])
                ->all();
        if(!empty($rubro) ){
            foreach ($rubro as $rub) {
                $jsonActividades[] = array(
                    'ID'                => $rub->ID, 
                    'ActividadID'       => $rub->ActividadID, 
                    'RubroElegibleID'   => $rub->RubroElegibleID, 
                    'UnidadMedida'      => $rub->UnidadMedida, 
                    'CostoUnitario'     => $rub->CostoUnitario, 
                    'MetaFisica'        => $rub->MetaFisica, 
                    'AreSubCategorias'  => $this->get_areSubCategoria($rub->ID,$rub->RubroElegibleID),
                    'RubroElegible'     => $this->get_rubroElegible($rub->RubroElegibleID),
                    //'Cronogramas'       => $this->getCronogramaProyecto($actvd->ID,'rubroElegible')
                );
            }
        }
        return $jsonActividades;
    }

    private function get_rubroElegible($idRubro){
        $jsonActividades = array();
            $actv = RubroElegible::find()
                    ->where(['ID' => $idRubro])
                    ->all();

        if(!empty($actv) ){
            foreach ($actv as $actvd) {
                $jsonActividades = array(
                    'TipoRubroElegibleID'=> $actvd->TipoRubroElegibleID, 
                    'ID'                 => $actvd->ID, 
                    'Nombre'             => $actvd->Nombre
                );
            }
        }
        return $jsonActividades;
    }

    private function get_areSubCategoria($idActRubroElegible,$RubroElegibleID){
        $jsonActividades = array();
        $actv = AreSubCategoria::find()
                ->where(['ActRubroElegibleID' => $idActRubroElegible])
                ->all();
        if(!empty($actv) ){
            foreach ($actv as $actvd) {
                $jsonActividades[] = array(
                    'ID'                    => $actvd->ID, 
                    'ActRubroElegibleID'    => $actvd->ActRubroElegibleID, 
                    'Nombre'                => $actvd->Nombre, 
                    'Matriz'                => $RubroElegibleID.'.'.$actvd->Codificacion, 
                    'UnidadMedida'          => $actvd->UnidadMedida, 
                    'CostoUnitario'         => $actvd->CostoUnitario, 
                    'MetaFisica'            => $actvd->MetaFisica, 
                    'Total'                 => $actvd->Total, 
                    'TotalConMetaFisica'    => $actvd->TotalConMetaFisica,
                    'ObservacionID'         => $this->getObservacionRecursoID($actvd->ID),
                    'Observacion'           => $this->getObservacionRecurso($actvd->ID),
                    'ObservacionSituacionID'=> $this->getObservacionSituacionID($actvd->ID),
                    'Situacion'             => $actvd->Situacion,
                    'Especifica'             => $actvd->Especifica,
                    
                    'Cronogramas'           => $this->getCronogramaProyecto($actvd->ID,'areSubCategoria')
                );
            }
        }
        return $jsonActividades;
    }

    private function get_actividadesmarco($idActividad){
        $jsonActividadesMarco = array();
        $actvmarco = MarcoLogicoActividad::find()
                ->Select('IndicadorUnidadMedida,sum(CAST(IndicadorMetaFisica AS FLOAT)) IndicadorMetaFisica')
                ->where(['Estado' => 1, 'ActividadID' => $idActividad])
                ->groupBy('IndicadorUnidadMedida')
                ->one();
        if(!$actvmarco)
        {
            $actvmarco=new MarcoLogicoActividad;
        }
        return $actvmarco;
    }

    private function getCronogramaProyecto($id,$tipo = ''){
        $this->layout='vacio';
        $meses = array();
        $cronograma = array();
        $time_start = microtime(true);
        switch ($tipo) {
            case 'proyecto':
                
                $indicador = 'ProyectoID';
                /*$meses = CronogramaProyecto::find()
                        ->where(['ProyectoID'=> $id])
                        ->all();*/
                $meses=Yii::$app->db->createCommand('
                        SELECT ID,'.$indicador.',Mes,MetaFisica,MesDescripcion,Situacion
                        FROM CronogramaProyecto
                        WHERE ProyectoID='.$id.'
                        Order by Mes asc
                        ')
                        ->queryAll();
            break;
            case 'componente':
                $meses = CronogramaComponente::find()
                        ->where(['ComponenteID'=> $id])
                        ->Orderby('Mes asc')
                        ->all();
                $indicador = 'ComponenteID';
            break;

            case 'actividad':
                $meses = CronogramaActividad::find()
                        ->where(['ActividadID'=> $id])
                        ->Orderby('Mes asc')
                        ->all();
                $indicador = 'ActividadID';
            break;

            case 'rubroElegible':
                $meses = CronogramaActRubroElegible::find()
                        ->where(['ActRubroElegibleID'=> $id])
                        ->Orderby('Mes asc')
                        ->all();
                $indicador = 'ActRubroElegibleID';
            break;

            case 'areSubCategoria':
                $indicador = 'AreSubCategoriaID';
                $meses=Yii::$app->db->createCommand('
                        SELECT ID,'.$indicador.',Mes,MetaFisica,MesDescripcion,Situacion
                        FROM CronogramaAreSubCategoria
                        WHERE AreSubCategoriaID='.$id.'
                        Order by Mes asc
                        ')
                        ->queryAll();
                /*
                $meses = CronogramaAreSubCategoria::find()
                        ->where(['AreSubCategoriaID'=> $id])
                        ->all();*/
                
            break;
        }
        $time_end = microtime(true);
        $foreach_time = $time_end - $time_start;
         
        if(!empty($meses)){
            foreach ($meses as $mes) {
                $cronograma[] = array(
                    'ID'                    => $mes['ID'],
                    $indicador              => $mes[''.$indicador.''],
                    'Mes'                   => $mes['Mes'],
                    'MetaFisica'            => $mes['MetaFisica'],
                    'Situacion'             => $mes['Situacion'],
                    'MesDescripcion'        => $this->DescripcionMes($mes['Mes']),
                    'Tiempo'                => number_format($foreach_time * 1000, 3) . "ms\n"
                    );
            }
        }else{
            $cronograma = array(
                'ID'                    => '',
                $indicador              => '',
                'Mes'                   => '',
                'Situacion'             => '',
                'MetaFisica'            => '',
                'MesDescripcion'        => '',
                'Tiempo'                => '',
            );
        }

        return $cronograma;
    }

    public function actionRecursos($id=null,$investigador=null,$valor=null){
        $this->layout='vacio';
        $actv = RubroElegible::find()
                ->all();
        $entidadesParticipantes=EntidadParticipante::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador])->all();
        // $rubroelegibleid=substr($valor,0,1);
        if(!is_null($valor)){
            $rubroelegibleid= explode('.',$valor)[0];
            $correlativoid= explode('.',$valor)[1];
        }else{
            $rubroelegibleid="";
            $correlativoid="";
        }
        
        $resultado = (new \yii\db\Query())
            ->select('RubroElegible.ID,AreSubCategoria.Nombre,AreSubCategoria.Especifica,AreSubCategoria.CostoUnitario,AreSubCategoria.UnidadMedida,AreSubCategoria.EntidadParticipanteID,AreSubCategoria.FondoEntidadParticipanteID,AreSubCategoria.Codificacion')
            ->from('AreSubCategoria')
            ->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')
            ->innerJoin('RubroElegible','RubroElegible.ID=ActRubroElegible.RubroElegibleID')
            ->innerJoin('Actividad','Actividad.ID=ActRubroElegible.ActividadID')
            ->innerJoin('Componente','Componente.ID=Actividad.ComponenteID')
            ->innerJoin('Poa','Poa.ID=Componente.PoaID')
            ->innerJoin('Proyecto','Proyecto.ID=Poa.ProyectoID')
            ->where(['Proyecto.InvestigadorID'=>$investigador])
            ->andwhere(['RubroElegible.ID'=>$rubroelegibleid])
            ->andwhere(['AreSubCategoria.Codificacion'=>$correlativoid])
            ->distinct()
            ->groupBy('RubroElegible.ID,AreSubCategoria.Nombre,AreSubCategoria.Especifica,AreSubCategoria.CostoUnitario,AreSubCategoria.UnidadMedida,AreSubCategoria.EntidadParticipanteID,AreSubCategoria.FondoEntidadParticipanteID,AreSubCategoria.Codificacion')
            ->one();
            
        $tareas=Tarea::find()->where('ActividadID=:ActividadID',[':ActividadID'=>$id])->all();
        return $this->render('recursos/_recursos_form',['idactividad'=>$id,'rubro' => $actv,'investigador'=>$investigador,'entidadesParticipantes'=>$entidadesParticipantes,'resultado'=>$resultado,'tareas'=>$tareas]);
    }
    
    public function actionRecursosActualizar($id=null,$actv=null,$investigador=null){
        $this->layout='vacio';


        $rubrosElegibles=RubroElegible::find()
                ->all();
        $entidadesParticipantes=EntidadParticipante::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador])->all();
        $idActividad = Yii::$app->request->post('ActRubroElegible');
        $act = ActRubroElegible::find()
            ->where(['ActividadID'=> $actv,'RubroElegibleID' => $idActividad['RubroElegibleID']])
            ->one();
        $model = new ActRubroElegible();

        $subC=AreSubCategoria::find()
                    ->select('AreSubCategoria.*,ActRubroElegible.RubroElegibleID')
                    ->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')
                    ->where('AreSubCategoria.ID=:ID',[':ID'=>$id])
                    ->one();
        $MetaFisicaActual=$subC->MetaFisica;
        $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador])->one();

        // echo '<pre>';
        // print_r(Yii::$app->request->post());
        // die();

        // $RubroElegible = Yii::$app->request->post('RubroElegibleID');
        // $Codificacion = Yii::$app->request->post('Codificacion');
        // 
        
        $tareas=Tarea::find()->where('ActividadID=:ActividadID',[':ActividadID'=>$actv])->all();

        if($model->load(Yii::$app->request->post()))
        {
            $subC->load(Yii::$app->request->post());

            $SubTotalPniaMonetario=0;
            $SubTotalPniaNoMonetario=0;
            $RubroElegible=RubroElegible::findOne($model->RubroElegibleID);
            $entidadPnia=EntidadParticipante::find()->where('ID=:ID',[':ID'=>$subC->EntidadParticipanteID])->one();
            $SubTotalesPnia=AreSubCategoria::find()
                    ->select('AreSubCategoria.*')
                    ->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')
                    ->where('AreSubCategoria.ID not in (:AreSubCategoriaID) and ActRubroElegible.ID=:ID and AreSubCategoria.EntidadParticipanteID=:EntidadParticipanteID',
                            [':AreSubCategoriaID'=>$id,':ID'=>$subC->ActRubroElegibleID,':EntidadParticipanteID'=>$subC->EntidadParticipanteID])
                    ->all();
            foreach($SubTotalesPnia as $sub)
            {
                if($subC->FondoEntidadParticipanteID==1)
                {
                    $SubTotalPniaMonetario=$SubTotalPniaMonetario+($sub->CostoUnitario*$sub->MetaFisica);
                }
                elseif($subC->FondoEntidadParticipanteID==2)
                {
                    $SubTotalPniaNoMonetario=$SubTotalPniaNoMonetario+($sub->CostoUnitario*$sub->MetaFisica);
                }                
            }
            $SubTotalPniaMonetario=$SubTotalPniaMonetario+($subC->CostoUnitario*$subC->MetaFisica);
            $SubTotalPniaNoMonetario=$SubTotalPniaNoMonetario+($subC->CostoUnitario*$subC->MetaFisica);
            
            if($subC->FondoEntidadParticipanteID==1 && $SubTotalPniaMonetario>$entidadPnia->AporteMonetario)
            {
                $arr = array(
                    'Success' => true,
                    'incorrecto'=>'Ha superado el aporte monetario total, de la Entidad Colaboradora'
                );
                echo json_encode($arr);
                die;
            }
            elseif($subC->FondoEntidadParticipanteID==2 && $SubTotalPniaMonetario>$entidadPnia->AporteNoMonetario)
            {
                $arr = array(
                    'Success' => true,
                    'incorrecto'=>'Ha superado el aporte no monetario,de la Entidad Colaboradora'
                );
                echo json_encode($arr);
                die;
            }
            
            if($entidadPnia->TipoInstitucionID==3)
            {
                $TotalPorcentajePnia=$this->PorcentajeRubroPnia($entidadPnia->AporteMonetario,$RubroElegible->ID,$RubroElegible->LimiteSegunBaseConcurso);
                if($SubTotalPniaMonetario>$TotalPorcentajePnia)
                {
                    $arr = array(
                        'Success' => true,
                        'incorrecto'=>'El monto máximo del Rubro Elegible '.$RubroElegible->Nombre.' es: '.$TotalPorcentajePnia
                    );
                    echo json_encode($arr);
                    die;
                }
            }
            
            if(!empty($act)){
                $id = $act->ID;
            }else{
                $model->save();
                $id = $model->ID;
            }

            // echo '<pre>';
            // print_r($subC);
            // die();
            
            

            CronogramaActRubroElegible::updateAll(['MetaFisica' => 0], 'ActRubroElegibleID='.$subC->ID);
            
            if ( $subC->load(Yii::$app->request->post()) ) {

                $Nombre = Yii::$app->request->post('Nombre');
                $Especifica = Yii::$app->request->post('Especifica');
                $CostoUnitario = Yii::$app->request->post('CostoUnitario');
                $UnidadMedida = Yii::$app->request->post('UnidadMedida');
                $EntidadParticipanteID = Yii::$app->request->post('EntidadParticipanteID');
                $FondoEntidadParticipanteID = Yii::$app->request->post('FondoEntidadParticipanteID');

                $queryy = Yii::$app->db->createCommand("UPDATE AreSubCategoria
                    SET AreSubCategoria.Nombre = '".$Nombre."', 
                    AreSubCategoria.Especifica = '".$Especifica."',
                    AreSubCategoria.CostoUnitario = '".$CostoUnitario."',
                    AreSubCategoria.UnidadMedida = '".$UnidadMedida."',
                    AreSubCategoria.EntidadParticipanteID = '".$EntidadParticipanteID."',
                    AreSubCategoria.FondoEntidadParticipanteID = '".$FondoEntidadParticipanteID."',
                    AreSubCategoria.Total = '".($CostoUnitario*$subC->MetaFisica)."'
                    FROM AreSubCategoria
                    INNER JOIN [ActRubroElegible] ON ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID 
                    INNER JOIN [RubroElegible] ON RubroElegible.ID = ActRubroElegible.RubroElegibleID 
                    INNER JOIN [Actividad] ON ActRubroElegible.ActividadID = Actividad.ID 
                    INNER JOIN [Componente] ON Actividad.ComponenteID = Componente.ID 
                    INNER JOIN [Poa] ON Poa.ID = Componente.PoaID 
                    INNER JOIN [Proyecto] ON Proyecto.ID = Poa.ProyectoID 
                    INNER JOIN [Investigador] ON Investigador.ID = Proyecto.InvestigadorID 
                    INNER JOIN [InformacionGeneral] ON Investigador.ID = InformacionGeneral.InvestigadorID 
                    WHERE RubroElegible.ID ='".$subC->RubroElegibleID."' AND  Actividad.Estado = 1 
                    AND Componente.Estado = 1 AND
                    InformacionGeneral.Codigo ='".$informacionGeneral->Codigo."' 
                    AND AreSubCategoria.Codificacion =".$subC->Codificacion);
                $queryy->execute();
                $actividad=Actividad::findOne($idActividad['ActividadID']);
                $actividad->TotalFinanciado=$actividad->TotalFinanciado-$subC->Total;
                $actividad->update();

                //Log
                Yii::$app->tools->logData($actividad->ID,'update','Actividad','');

                $componente=Componente::findOne($actividad->ComponenteID);
                $componente->TotalFinanciado=$componente->TotalFinanciado+$actividad->TotalFinanciado;
                $componente->update();

                //Log
                Yii::$app->tools->logData($componente->ID,'update','Componente','');

                //$subC->Total = ($subC->CostoUnitario * $subC->MetaFisica);
                $subC->ActRubroElegibleID = $id;
                $subC->update();
                
                $actividad->TotalFinanciado=$actividad->TotalFinanciado+$subC->Total;
                $actividad->update();
                $componente->TotalFinanciado=$componente->TotalFinanciado+$actividad->TotalFinanciado;
                $componente->update();
                $idz = $subC->ID;
                if($subC->MetaFisica<>$MetaFisicaActual)
                {
                    CronogramaAreSubCategoria::updateAll(['CostoUnitario' => $CostoUnitario,'MetaFisica'=>0], 'AreSubCategoriaID='.$idz);    
                }
                
                
                $arr = array('Success' => true);
                echo json_encode($arr);
                die;
            }

        }
        return $this->render('recursos/actualizar_recursos_form',['id'=>$id,'actv'=>$actv,'investigador'=>$investigador,'recurso'=>$subC,'rubrosElegibles'=>$rubrosElegibles,'entidadesParticipantes'=>$entidadesParticipantes,'tareas'=>$tareas]);
    }

    public function actionRecursosCrear($investigadorID=null){
        $this->layout='vacio';
        
        $mes = InformacionGeneral::find()
            ->select('Meses,Codigo')
            ->where(['InvestigadorID' => $investigadorID])
            ->one();
        $idActividad = Yii::$app->request->post('ActRubroElegible');
        $act = ActRubroElegible::find()
            ->where(['ActividadID'=> $idActividad['ActividadID'],'RubroElegibleID' => $idActividad['RubroElegibleID']])
            ->one();
        $model = new ActRubroElegible();
        $subC = new AreSubCategoria();
        if ( $model->load(Yii::$app->request->post()) ) {
            // echo '<pre>';
            
            $subC->load(Yii::$app->request->post());
            $CodificacionExiste=$this->CodificacionRecursoExiste($idActividad['ActividadID'],$mes->Codigo,$model->RubroElegibleID,$subC->Codificacion);
            
            if($CodificacionExiste)
            {
                $arr = array(
                    'Success' => true,
                    'incorrecto'=>'Ya existe el recurso seleccionado en esa actividad.'
                );
                echo json_encode($arr);
                die;
            }
            
            $SubTotalPniaMonetario=0;
            $SubTotalPniaNoMonetario=0;
            $RubroElegible=RubroElegible::findOne($model->RubroElegibleID);
            $entidadPnia=EntidadParticipante::find()->where('ID=:ID',[':ID'=>$subC->EntidadParticipanteID])->one();
            $SubTotalesPnia=AreSubCategoria::find()
                    ->select('AreSubCategoria.*')
                    ->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')
                    ->where('ActRubroElegible.ID=:ID and AreSubCategoria.EntidadParticipanteID=:EntidadParticipanteID',
                            [':ID'=>$subC->ActRubroElegibleID,':EntidadParticipanteID'=>$subC->EntidadParticipanteID])
                    ->all();
            foreach($SubTotalesPnia as $sub)
            {
                if($subC->FondoEntidadParticipanteID==1)
                {
                    $SubTotalPniaMonetario=$SubTotalPniaMonetario+($sub->CostoUnitario*$sub->MetaFisica);
                }
                elseif($subC->FondoEntidadParticipanteID==2)
                {
                    $SubTotalPniaNoMonetario=$SubTotalPniaNoMonetario+($sub->CostoUnitario*$sub->MetaFisica);
                }                
            }
            $SubTotalPniaMonetario   = $SubTotalPniaMonetario + ($subC->CostoUnitario*$subC->MetaFisica);
            $SubTotalPniaNoMonetario = $SubTotalPniaNoMonetario + ($subC->CostoUnitario*$subC->MetaFisica);
            
            
            if($subC->FondoEntidadParticipanteID==1 && $SubTotalPniaMonetario>$entidadPnia->AporteMonetario)
            {
                $arr = array(
                    'Success' => true,
                    'incorrecto'=>'Ha superado el aporte monetario total, de la Entidad Colaboradora'
                );
                echo json_encode($arr);
                die;
            }
            elseif($subC->FondoEntidadParticipanteID==2 && $SubTotalPniaNoMonetario>$entidadPnia->AporteNoMonetario)
            {
                $arr = array(
                    'Success' => true,
                    'incorrecto'=>'Ha superado el aporte no monetario,de la Entidad Colaboradora'
                );
                echo json_encode($arr);
                die;
            }
            
            if($entidadPnia->TipoInstitucionID==3)
            {
                $TotalPorcentajePnia=$this->PorcentajeRubroPnia($entidadPnia->AporteMonetario,$RubroElegible->ID,$RubroElegible->LimiteSegunBaseConcurso);
                if($SubTotalPniaMonetario>$TotalPorcentajePnia)
                {
                    $arr = array(
                        'Success' => true,
                        'incorrecto'=>'El monto máximo del Rubro Elegible '.$RubroElegible->Nombre.' es: '.$TotalPorcentajePnia
                    );
                    echo json_encode($arr);
                    die;
                }
            }
            
            
            
            // Preguntar antes si el rubro elegible existe para esa actividad 
            if(!empty($act)){
                $id = $act->ID;
            }else{
                
                $model->save();

                //Log
                Yii::$app->tools->logData($model->ID,'insert','ActRubroElegible','');

                // print_r($model);
                // die();
                $id = $model->ID;
                for ($i=1; $i <= $mes->Meses; $i++) { 
                    $cronogramaAct = new CronogramaActRubroElegible();
                    $cronogramaAct->ActRubroElegibleID = $id;
                    $cronogramaAct->Mes = $i;
                    $cronogramaAct->MetaFinanciera = 0;
                    $cronogramaAct->PoafMetaFinanciera = 0;
                    $cronogramaAct->save();
                }
            }
            
            
            
            
            if ( $subC->load(Yii::$app->request->post()) ) {
                
                $CostoUnitario = $_POST['AreSubCategoria']['CostoUnitario'];
                $MetaFisica = Yii::$app->request->post('MetaFisica');
                
                
                //var_dump($CostoUnitario.' '.$MetaFisica);
                $subC->Total = ($subC->CostoUnitario * $subC->MetaFisica);
                $subC->ActRubroElegibleID = $id;
                $subC->Correlativo=$this->CorrelativoRecurso($id);
                if(!$subC->Codificacion)
                {
                    $subC->Codificacion=$this->CodificacionRecurso($mes->Codigo,$model->RubroElegibleID);
                }
                $subC->Situacion=0;
                $subC->MetaFisicaEjecutada=0;
                $subC->MetaAvance=0;
                // echo '<pre>';print_r($subC);die;
                $subC->save();
                
                //Log
                Yii::$app->tools->logData($subC->ID,'insert','AreSubCategoria','');


                $actividad=Actividad::findOne($idActividad['ActividadID']);
                $actividad->TotalFinanciado=$actividad->TotalFinanciado+$subC->Total;
                $actividad->update();

                //Log
                Yii::$app->tools->logData($actividad->ID,'insert','Actividad','');

                $componente=Componente::findOne($actividad->ComponenteID);
                $componente->TotalFinanciado=$componente->TotalFinanciado+$actividad->TotalFinanciado;
                $componente->update();
                
                //Log
                Yii::$app->tools->logData($componente->ID,'update','Componente','');

                $idz = $subC->ID;

                for ($i=1; $i <= $mes->Meses; $i++) {
                    $cronoAreSubCat = new CronogramaAreSubCategoria();
                    $cronoAreSubCat->AreSubCategoriaID = $idz;
                    $cronoAreSubCat->Mes = $i;
                    $cronoAreSubCat->Situacion=0;
                    $cronoAreSubCat->MetaFinanciera = 0;
                    $cronoAreSubCat->PoafMetaFinanciera = 0;
                    $cronoAreSubCat->MetaFisicaEjecutada = 0;
                    $cronoAreSubCat->MetaAvance = 0;
                    $cronoAreSubCat->MetaFisica = 0;
                    $cronoAreSubCat->SituacionEjecucion=0;
                    $cronoAreSubCat->CostoUnitario=$subC->CostoUnitario;
                    $cronoAreSubCat->save();
                }
                
                $arr = array(
                    'Success' => true,
                    'id'     => $idz
                );
                echo json_encode($arr);
            }
        }
    }

    public function actionGrabarMetaCronogramaRecurso($ID=null,$MetaFisica=null)
    {
        $cronogramaTarea=CronogramaAreSubCategoria::findOne($ID);
        $cronogramaTarea->MetaFisica=$MetaFisica;
        $cronogramaTarea->save();
        Yii::$app->tools->logData($cronogramaTarea->ID,'insert','CronogramaAreSubCategoria','Actualizando Cronograma meta:'.$MetaFisica);
        $arr = array('Success' => true,);
        echo json_encode($arr);
    }
    
    public function actionEnviarEvaluacionPoa($codigo)
    {
        $usuario=Usuario::find()->where('username=:username',[':username'=>$codigo])->one();
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID and Estado=1',[':ProyectoID'=>$proyecto->ID])->one();
        $poa->Situacion=2;
        $poa->update();
        Yii::$app->tools->logData($poa->ID,'update','Poa','');
    }
    
    public function actionDeleteRecurso($id=null)
    {
        if($id)
        {
            $subC=AreSubCategoria::find()
                        ->where('AreSubCategoria.ID=:ID',[':ID'=>$id])
                        ->one();
            if (!empty($subC)) {
                // print_r($subC->ActRubroElegibleID); die;
                $actRubroElegible=ActRubroElegible::findOne($subC->ActRubroElegibleID);
                $countRubrosElegibles=ActRubroElegible::find()
                        ->where('ActividadID=:ActividadID and RubroElegibleID=:RubroElegibleID',[':ActividadID'=>$actRubroElegible->ActividadID,':RubroElegibleID'=>$actRubroElegible->RubroElegibleID])->count();
                // echo $subC->ActividadID; die;
                CronogramaAreSubCategoria::deleteAll(['AreSubCategoriaID' => $id]);
                AreSubCategoria::deleteAll(['ID' => $id]);
                Yii::$app->tools->logData($id,'delete','AreSubCategoria','');
                echo $subC->ActRubroElegibleID;
                if($countRubrosElegibles==1)
                {
                    CronogramaActRubroElegible::deleteAll(['ActRubroElegibleID' => $actRubroElegible->ActRubroElegibleID]);
                    ActRubroElegible::deleteAll(['ActividadID' => $actRubroElegible->ActividadID,'RubroElegibleID'=>$actRubroElegible->RubroElegibleID]);
                }
                $arr = array(
                    'Success' => true,
                    
                );
                echo json_encode($arr);
            }
        }
    }
    public function actionDescargarPoa($codigo=null)
    {
        $this->layout='estandar';
        $usuario=Usuario::find()->where('username=:username',[':username'=>$codigo])->one();
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
        $componentes=Componente::find()->where('PoaID=:PoaID and Estado=:Estado',[':PoaID'=>$poa->ID,':Estado'=>1])->orderBy('Correlativo asc')->all();
        
        return $this->render('descargar-poa',['componentes'=>$componentes,'proyecto'=>$proyecto]);
    }
    
    public function actionDescargarPoaFinanciero($codigo=null)
    {
        $this->layout='estandar';
        $usuario=Usuario::find()->where('username=:username',[':username'=>$codigo])->one();
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
        $componentes=Componente::find()->where('PoaID=:PoaID and Estado=1',[':PoaID'=>$poa->ID])->orderBy('Correlativo asc')->all();
        
        return $this->render('descargar-poa-financiero',['componentes'=>$componentes,'proyecto'=>$proyecto,'poa'=>$poa]);
    }
    
    public function CorrelativoRecurso($actRubroElegibleID)
    {
        $recurso=AreSubCategoria::find()->where('ActRubroElegibleID=:ActRubroElegibleID',[':ActRubroElegibleID'=>$actRubroElegibleID])->orderBy('Correlativo desc')->one();
        if($recurso)
        {
            return $recurso->Correlativo+1;
        }
        else
        {
            return 1;
        }
    }
    
    public function CodificacionRecurso($CodigoProyecto,$RubroElegibleID)
    {
        
        $recurso=AreSubCategoria::find()
                ->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')
                ->innerJoin('RubroElegible','RubroElegible.ID=ActRubroElegible.RubroElegibleID')
                ->innerJoin('Actividad','Actividad.ID=ActRubroElegible.ActividadID')
                ->innerJoin('Componente','Componente.ID=Actividad.ComponenteID')
                ->innerJoin('Poa','Poa.ID=Componente.PoaID')
                ->innerJoin('Proyecto','Proyecto.ID=Poa.ProyectoID')
                ->innerJoin('InformacionGeneral','InformacionGeneral.InvestigadorID=Proyecto.InvestigadorID')
                ->where('RubroElegibleID=:RubroElegibleID',[':RubroElegibleID'=>$RubroElegibleID])
                ->andwhere('InformacionGeneral.Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])
                ->orderBy('Codificacion desc')
                ->one();
                
        if($recurso)
        {
            return $recurso->Codificacion+1;
        }
        else
        {
            return 1;
        }
    }
    
    public function CodificacionRecursoExiste($ActividadID,$CodigoProyecto,$RubroElegibleID,$Codificacion)
    {
        
        $recurso=AreSubCategoria::find()
                ->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')
                ->innerJoin('RubroElegible','RubroElegible.ID=ActRubroElegible.RubroElegibleID')
                ->innerJoin('Actividad','Actividad.ID=ActRubroElegible.ActividadID')
                ->innerJoin('Componente','Componente.ID=Actividad.ComponenteID')
                ->innerJoin('Poa','Poa.ID=Componente.PoaID')
                ->innerJoin('Proyecto','Proyecto.ID=Poa.ProyectoID')
                ->innerJoin('InformacionGeneral','InformacionGeneral.InvestigadorID=Proyecto.InvestigadorID')
                ->where('RubroElegibleID=:RubroElegibleID',[':RubroElegibleID'=>$RubroElegibleID])
                ->andwhere('InformacionGeneral.Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])
                ->andwhere('Actividad.ID=:ID',[':ID'=>$ActividadID])
                ->andwhere('AreSubCategoria.Codificacion=:Codificacion',[':Codificacion'=>$Codificacion])
                ->orderBy('Codificacion desc')
                ->one();
        if($recurso)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public function getObservacionRecurso($id)
    {
        $observacion=AreSubCategoriaObservacion::find()->where('AreSubCategoriaID=:AreSubCategoriaID and Estado=1',[':AreSubCategoriaID'=>$id])->one();
        if($observacion)
        {
            return $observacion->Observacion;
        }
        
        return "";
    }
    
    public function getObservacionSituacionID($id)
    {
        $observacion=AreSubCategoriaObservacion::find()->where('AreSubCategoriaID=:AreSubCategoriaID and Estado=3',[':AreSubCategoriaID'=>$id])->one();
        if($observacion)
        {
            return $observacion->Estado;
        }
        
        return "";
    }
    
    public function getObservacionRecursoID($id)
    {
        $observacion=AreSubCategoriaObservacion::find()->where('AreSubCategoriaID=:AreSubCategoriaID and Estado=1',[':AreSubCategoriaID'=>$id])->one();
        if($observacion)
        {
            return $observacion->ID;
        }
        
        return "";
    }
    
    public function DescripcionMes($mes=null)
    {
        setlocale (LC_TIME,"spanish");
        //$mes = 01;
        $mes=$mes-1;
        $fechaInicio = '2016-05-01';
        $nuevafecha = strtotime ( '+'.$mes.' month' , strtotime ( $fechaInicio ) ) ;
        $nuevafecha = strtoupper(strftime ( '%b %Y' , $nuevafecha ) ) ;
        return $nuevafecha;
    }


    public function PorcentajeRubroPnia($MontoMonetario,$RubroElegibleID,$Porcentaje)
    {
        $TotalPorcetaje=0;
        $TotalPorcetaje=($MontoMonetario*$Porcentaje)/100;
       return $TotalPorcetaje;
    }
    
    public function actionAprobarObservacion($ObservacionID)
    {
        $observacion=AreSubCategoriaObservacion::findOne($ObservacionID);
        $observacion->Estado=3;
        $observacion->update();
        $recurso=AreSubCategoria::find()->where('ID=:ID',[':ID'=>$observacion->AreSubCategoriaID])->one();
        $recurso->Situacion=2;
        $recurso->update();
        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
    public function actionRapido(){
        $this->layout='estandar';
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $CodigoProyecto=$informacionGeneral->Codigo;
        $entidadParticipante=EntidadParticipante::find()->where('InvestigadorID=:InvestigadorID and TipoInstitucionID=3',[':InvestigadorID'=>$investigador->ID])->one();
        
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacionGeneral->InvestigadorID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
        $componentes=Componente::find()->where('PoaID=:PoaID',[':PoaID'=>$poa->ID])->orderBy('Correlativo asc')->all();
        
        $idActividad = Yii::$app->request->post('ActRubroElegible');
        $act = ActRubroElegible::find()
            ->where(['ActividadID'=> $idActividad['ActividadID'],'RubroElegibleID' => $idActividad['RubroElegibleID']])
            ->one();
        $mes = InformacionGeneral::find()
            ->select('Meses')
            ->where(['InvestigadorID' => $investigador->ID])
            ->one();
            
        $model = new ActRubroElegible();
        $subC = new AreSubCategoria();
        if ( $model->load(Yii::$app->request->post()) ) {
            //$subC->load(Yii::$app->request->post());
            
            $RubroElegible=RubroElegible::findOne($model->RubroElegibleID);
            //$entidadPnia=EntidadParticipante::find()->where('ID=:ID',[':ID'=>$subC->EntidadParticipanteID])->one();
         
            // Preguntar antes si el rubro elegible existe para esa actividad 
            if(!empty($act)){
                $id = $act->ID;
            }else{
                
                $model->save();
                $id = $model->ID;
                
                for ($i=1; $i <= $mes->Meses; $i++) { 
                    $cronogramaAct = new CronogramaActRubroElegible();
                    $cronogramaAct->ActRubroElegibleID = $id;
                    $cronogramaAct->Mes = $i;
                    $cronogramaAct->MetaFinanciera = 0;
                    $cronogramaAct->PoafMetaFinanciera = 0;
                    $cronogramaAct->save();
                }
            }
            
            if ( $subC->load(Yii::$app->request->post()) ) {
                
                
                
                if(!$subC->Recursos)
                {
                    $countRecursos=0;
                }
                else
                {
                    $countRecursos=count(array_filter($subC->Recursos));
                }
                
                if($countRecursos>0){
                    //var_dump($subC->Recursos);die;
                    for($i=0;$i<$countRecursos;$i++)
                    {
                        $detalle=new AreSubCategoria;
                        $detalle->Nombre=$subC->Recursos[$i];
                        $detalle->Especifica=$subC->Especificas[$i];
                        $detalle->CostoUnitario=$subC->CostosUnitarios[$i];
                        $detalle->UnidadMedida=$subC->UnidadesMedidas[$i];
                        $detalle->MetaFisica=$subC->MetasFisicas[$i];
                        $detalle->Total = ($subC->CostosUnitarios[$i] * $subC->MetasFisicas[$i]);
                        $detalle->ActRubroElegibleID = $id;
                        $detalle->Correlativo=$this->CorrelativoRecurso($id);
                        $detalle->Situacion=0;
                        $detalle->MetaFisicaEjecutada=0;
                        $detalle->MetaAvance=0;
                        $detalle->EntidadParticipanteID=$entidadParticipante->ID;
                        $detalle->FondoEntidadParticipanteID=1;
                        $detalle->save();
                        $idz = $detalle->ID;
        
                        for ($a=1; $a <= $mes->Meses; $a++) {
                            $cronoAreSubCat = new CronogramaAreSubCategoria();
                            $cronoAreSubCat->AreSubCategoriaID = $idz;
                            $cronoAreSubCat->Mes = $a;
                            $cronoAreSubCat->MetaFinanciera = 0;
                            $cronoAreSubCat->PoafMetaFinanciera = 0;
                            $cronoAreSubCat->MetaFisicaEjecutada = 0;
                            $cronoAreSubCat->MetaAvance = 0;
                            $cronoAreSubCat->MetaFisica = 0;
                            $cronoAreSubCat->SituacionEjecucion=0;
                            $cronoAreSubCat->save();
                        }
                    }
                }
                //$CostoUnitario = Yii::$app->request->post('CostoUnitario');
                //$MetaFisica = Yii::$app->request->post('MetaFisica');
                return $this->refresh();
            }
            else
            {
                //var_dump($_FILES);die;
                    //validate whether uploaded file is a csv file
                    $csvMimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
                    if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
                        //var_dump($_FILES['ActRubroElegible']);die;
                        if(is_uploaded_file($_FILES['file']['tmp_name'])){
                            
                            //open uploaded csv file with read only mode
                            $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
                            
                            //skip first line
                            fgetcsv($csvFile);
                            
                            //parse data from csv file line by line
                            while(($line = fgetcsv($csvFile)) !== FALSE){
                                //check whether member already exists in database with same email
                                //var_dump(utf8_encode($line[0]));die;
                                $detalle=new AreSubCategoria;
                                $detalle->Nombre=utf8_encode($line[0]);
                                $detalle->Especifica=utf8_encode($line[1]);
                                $detalle->CostoUnitario=utf8_encode($line[2]);
                                $detalle->UnidadMedida=utf8_encode($line[3]);
                                $detalle->MetaFisica=utf8_encode($line[4]);
                                $detalle->Total = ($detalle->CostoUnitario * $detalle->MetaFisica);
                                $detalle->ActRubroElegibleID = $id;
                                $detalle->Correlativo=$this->CorrelativoRecurso($id);
                                $detalle->Situacion=0;
                                $detalle->MetaFisicaEjecutada=0;
                                $detalle->MetaAvance=0;
                                $detalle->EntidadParticipanteID=$entidadParticipante->ID;
                                $detalle->FondoEntidadParticipanteID=1;
                                $detalle->save();
                                $idz = $detalle->ID;
                
                                for ($a=1; $a <= $mes->Meses; $a++) {
                                    $cronoAreSubCat = new CronogramaAreSubCategoria();
                                    $cronoAreSubCat->AreSubCategoriaID = $idz;
                                    $cronoAreSubCat->Mes = $a;
                                    $cronoAreSubCat->MetaFinanciera = 0;
                                    $cronoAreSubCat->PoafMetaFinanciera = 0;
                                    $cronoAreSubCat->MetaFisicaEjecutada = 0;
                                    $cronoAreSubCat->MetaAvance = 0;
                                    $cronoAreSubCat->MetaFisica = 0;
                                    $cronoAreSubCat->SituacionEjecucion=0;
                                    $cronoAreSubCat->save();
                                }
                                
                            }
                            
                            //close opened csv file
                            fclose($csvFile);
                           
                        }
                    }
            }
        }
        return $this->render('rapido',['componentes'=>$componentes]);
    }
    
    public function actionRapido2(){
        $this->layout='estandar';
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $CodigoProyecto=$informacionGeneral->Codigo;
        $entidadParticipante=EntidadParticipante::find()->where('InvestigadorID=:InvestigadorID and TipoInstitucionID=3',[':InvestigadorID'=>$investigador->ID])->one();
        
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacionGeneral->InvestigadorID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
        $componentes=Componente::find()->where('PoaID=:PoaID',[':PoaID'=>$poa->ID])->orderBy('Correlativo asc')->all();
        
        
        $mes = InformacionGeneral::find()
            ->select('Meses')
            ->where(['InvestigadorID' => $investigador->ID])
            ->one();
            
        $model = new ActRubroElegible();
        //$subC = new AreSubCategoria();
        if ( $model->load(Yii::$app->request->post()) ) {
            //$subC->load(Yii::$app->request->post());
            
            //$RubroElegible=RubroElegible::findOne($model->RubroElegibleID);
            //$entidadPnia=EntidadParticipante::find()->where('ID=:ID',[':ID'=>$subC->EntidadParticipanteID])->one();
         
            // Preguntar antes si el rubro elegible existe para esa actividad 
            
            
                //var_dump($_FILES);die;
                    //validate whether uploaded file is a csv file
                    $csvMimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
                    if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
                        //var_dump($_FILES['ActRubroElegible']);die;
                        if(is_uploaded_file($_FILES['file']['tmp_name'])){
                            
                            //open uploaded csv file with read only mode
                            $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
                            
                            //skip first line
                            fgetcsv($csvFile);
                            
                            //parse data from csv file line by line
                            while(($line = fgetcsv($csvFile)) !== FALSE){
                                //check whether member already exists in database with same email
                                //var_dump(utf8_encode($line[0]));die;
                                $act = ActRubroElegible::find()
                                ->where(['ActividadID'=> $model->ActividadID,'RubroElegibleID' => utf8_encode($line[0])])
                                ->one();
                                if(!empty($act)){
                                    $id = $act->ID;
                                }else{
                                    $ActRubroElegible=new ActRubroElegible;
                                    $ActRubroElegible->ActividadID=$model->ActividadID;
                                    $ActRubroElegible->RubroElegibleID=utf8_encode($line[0]);
                                    $ActRubroElegible->save();
                                    $id = $ActRubroElegible->ID;
                                    
                                    for ($i=1; $i <= $mes->Meses; $i++) { 
                                        $cronogramaAct = new CronogramaActRubroElegible();
                                        $cronogramaAct->ActRubroElegibleID = $id;
                                        $cronogramaAct->Mes = $i;
                                        $cronogramaAct->MetaFinanciera = 0;
                                        $cronogramaAct->PoafMetaFinanciera = 0;
                                        $cronogramaAct->save();
                                    }
                                }
                                $detalle=new AreSubCategoria;
                                $detalle->Nombre=utf8_encode($line[1]);
                                $detalle->Especifica=utf8_encode($line[2]);
                                $detalle->CostoUnitario=utf8_encode($line[3]);
                                $detalle->UnidadMedida=utf8_encode($line[4]);
                                $detalle->MetaFisica=utf8_encode($line[5]);
                                $detalle->Total = ($detalle->CostoUnitario * $detalle->MetaFisica);
                                $detalle->ActRubroElegibleID = $id;
                                $detalle->Correlativo=$this->CorrelativoRecurso($id);
                                $detalle->Situacion=0;
                                $detalle->MetaFisicaEjecutada=0;
                                $detalle->MetaAvance=0;
                                $detalle->EntidadParticipanteID=$entidadParticipante->ID;
                                $detalle->FondoEntidadParticipanteID=1;
                                $detalle->save();
                                $idz = $detalle->ID;
                
                                for ($a=1; $a <= $mes->Meses; $a++) {
                                    $cronoAreSubCat = new CronogramaAreSubCategoria();
                                    $cronoAreSubCat->AreSubCategoriaID = $idz;
                                    $cronoAreSubCat->Mes = $a;
                                    $cronoAreSubCat->MetaFinanciera = 0;
                                    $cronoAreSubCat->PoafMetaFinanciera = 0;
                                    $cronoAreSubCat->MetaFisicaEjecutada = 0;
                                    $cronoAreSubCat->MetaAvance = 0;
                                    $cronoAreSubCat->MetaFisica = 0;
                                    $cronoAreSubCat->SituacionEjecucion=0;
                                    $cronoAreSubCat->save();
                                }
                                
                            }
                            
                            //close opened csv file
                            fclose($csvFile);
                           
                        }
                    }
            
        }
        return $this->render('rapido2',['componentes'=>$componentes]);
    }
    
    public function actionRapido3(){
        $this->layout='estandar';
        ini_set('memory_limit', '-1');
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $CodigoProyecto=$informacionGeneral->Codigo;
        $entidadParticipante=EntidadParticipante::find()->where('InvestigadorID=:InvestigadorID and TipoInstitucionID=3',[':InvestigadorID'=>$investigador->ID])->one();
        
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacionGeneral->InvestigadorID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
        $componentes=Componente::find()->where('PoaID=:PoaID',[':PoaID'=>$poa->ID])->orderBy('Correlativo asc')->all();
        
        
        $mes = InformacionGeneral::find()
            ->select('Meses')
            ->where(['InvestigadorID' => $investigador->ID])
            ->one();
            
        $model = new ActRubroElegible();
        //$subC = new AreSubCategoria();
        if ( $model->load(Yii::$app->request->post()) ) {
            //$subC->load(Yii::$app->request->post());
            
            //$RubroElegible=RubroElegible::findOne($model->RubroElegibleID);
            //$entidadPnia=EntidadParticipante::find()->where('ID=:ID',[':ID'=>$subC->EntidadParticipanteID])->one();
         
            // Preguntar antes si el rubro elegible existe para esa actividad 
            
            
                //var_dump($_FILES);die;
                    //validate whether uploaded file is a csv file
                    $csvMimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
                    if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
                        //var_dump($_FILES['ActRubroElegible']);die;
                        if(is_uploaded_file($_FILES['file']['tmp_name'])){
                            
                            //open uploaded csv file with read only mode
                            $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
                            
                            //skip first line
                            fgetcsv($csvFile);
                            
                            //parse data from csv file line by line
                            while(($line = fgetcsv($csvFile)) !== FALSE){

                                if($line[0] == ''){
                                    echo 'Componente Vacio'; die();
                                }elseif($line[1]== ''){
                                    echo 'Actividad Vacio'; die();
                                }elseif($line[2]== ''){
                                    echo 'Rubro Vacio'; die();
                                }
                                //check whether member already exists in database with same email
                                //var_dump(utf8_encode($line[0]));die;
                                $componente=Componente::find()
                                                            ->where('PoaID=:PoaID and Correlativo=:Correlativo',
                                                            [':PoaID'=>$poa->ID,':Correlativo'=>utf8_encode($line[0])])
                                                            ->one();
                                $actividad=Actividad::find()
                                                            ->where('ComponenteID=:ComponenteID and Correlativo=:Correlativo',[':ComponenteID'=>$componente->ID,':Correlativo'=>utf8_encode($line[1])])
                                                            ->one();
                                
                                $act = ActRubroElegible::find()
                                ->where(['ActividadID'=> $actividad->ID,'RubroElegibleID' => utf8_encode($line[2])])
                                ->one();
                                if(!empty($act)){
                                    $id = $act->ID;
                                }else{
                                    $ActRubroElegible=new ActRubroElegible;
                                    $ActRubroElegible->ActividadID=$actividad->ID;
                                    $ActRubroElegible->RubroElegibleID=utf8_encode($line[2]);
                                    $ActRubroElegible->save();
                                    $id = $ActRubroElegible->ID;
                                    
                                    for ($i=1; $i <= $mes->Meses; $i++) { 
                                        $cronogramaAct = new CronogramaActRubroElegible();
                                        $cronogramaAct->ActRubroElegibleID = $id;
                                        $cronogramaAct->Mes = $i;
                                        $cronogramaAct->MetaFinanciera = 0;
                                        $cronogramaAct->PoafMetaFinanciera = 0;
                                        $cronogramaAct->save();
                                    }
                                }
                                $detalle=new AreSubCategoria;
                                $detalle->Nombre=utf8_encode($line[3]);
                                $detalle->Especifica=utf8_encode($line[4]);
                                $detalle->CostoUnitario=utf8_encode($line[5]);
                                $detalle->UnidadMedida=utf8_encode($line[6]);
                                $detalle->MetaFisica=utf8_encode($line[7]);
                                $detalle->Total = ($detalle->CostoUnitario * $detalle->MetaFisica);
                                $detalle->ActRubroElegibleID = $id;
                                $detalle->Correlativo=$this->CorrelativoRecurso($id);
                                $detalle->Situacion=0;
                                $detalle->MetaFisicaEjecutada=0;
                                $detalle->MetaAvance=0;
                                $detalle->EntidadParticipanteID=$entidadParticipante->ID;
                                $detalle->FondoEntidadParticipanteID=1;
                                $detalle->save();
                                $idz = $detalle->ID;
                
                                for ($a=1; $a <= $mes->Meses; $a++) {
                                    $cronoAreSubCat = new CronogramaAreSubCategoria();
                                    $cronoAreSubCat->AreSubCategoriaID = $idz;
                                    $cronoAreSubCat->Mes = $a;
                                    $cronoAreSubCat->MetaFinanciera = 0;
                                    $cronoAreSubCat->PoafMetaFinanciera = 0;
                                    $cronoAreSubCat->MetaFisicaEjecutada = 0;
                                    $cronoAreSubCat->MetaAvance = 0;
                                    $cronoAreSubCat->MetaFisica = 0;
                                    $cronoAreSubCat->SituacionEjecucion=0;
                                    $cronoAreSubCat->save();
                                }
                                
                            }
                            
                            //close opened csv file
                            fclose($csvFile);
                           
                        }
                    }
            
        }
        return $this->render('rapido3',['componentes'=>$componentes]);
    }
    
    public function actionActividades()
    {
        if(isset($_POST['ComponenteID']) && trim($_POST['ComponenteID'])!='')
        {
            $ComponenteID=$_POST['ComponenteID'];
            $countActividades=Actividad::find()->select('ID,Nombre')->where('ComponenteID=:ComponenteID',[':ComponenteID'=>$ComponenteID])->groupBy('ID,Nombre')->count();
            $actividades=Actividad::find()->select('ID,Nombre,Correlativo')->where('ComponenteID=:ComponenteID',[':ComponenteID'=>$ComponenteID])->groupBy('ID,Nombre,Correlativo')->orderBy('Correlativo asc')->all();
            if($countActividades>0){
                echo "<option value>Seleccionar</option>";
                foreach($actividades as $actividad){
                    echo "<option value='".$actividad->ID."'>".$actividad->Correlativo."-".$actividad->Nombre."</option>";
                }
            }
            else{
                echo "<option value>Seleccionar</option>";
            }
        }
    }
    
    public function actionGetRecurso($valor=null,$investigadorid=null)
    {
        $resultados = (new \yii\db\Query())
            ->select('RubroElegible.ID,AreSubCategoria.Codificacion,AreSubCategoria.Nombre,AreSubCategoria.Especifica')
            ->from('AreSubCategoria')
            ->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')
            ->innerJoin('RubroElegible','RubroElegible.ID=ActRubroElegible.RubroElegibleID')
            ->innerJoin('Actividad','Actividad.ID=ActRubroElegible.ActividadID')
            ->innerJoin('Componente','Componente.ID=Actividad.ComponenteID')
            ->innerJoin('Poa','Poa.ID=Componente.PoaID')
            ->innerJoin('Proyecto','Proyecto.ID=Poa.ProyectoID')
            ->where(['Proyecto.InvestigadorID'=>$investigadorid])
            ->andwhere(['RubroElegible.ID'=>$valor])
            ->distinct()
            ->groupBy('RubroElegible.ID,AreSubCategoria.Codificacion,AreSubCategoria.Nombre,AreSubCategoria.Especifica')
            ->all();
        echo "<option value>[SELECCIONE]</option>";
        foreach($resultados as $resultado)
        {
            echo "<option value='".$resultado['ID'].".".$resultado['Codificacion']."'>".$resultado['Nombre']."/".$resultado['Especifica']."</option>";
        }
    }
    
    
    public function actionGetDatosRecurso($valor=null,$investigadorid=null)
    {
        
        $rubroelegibleid=substr($valor,0,1);
        $correlativoid=substr($valor,2,1);
        
        
        $resultado = (new \yii\db\Query())
            ->select('RubroElegible.ID,AreSubCategoria.Nombre,AreSubCategoria.Especifica,AreSubCategoria.CostoUnitario,AreSubCategoria.UnidadMedida,AreSubCategoria.EntidadParticipanteID,AreSubCategoria.FondoEntidadParticipanteID,AreSubCategoria.Codificacion')
            ->from('AreSubCategoria')
            ->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')
            ->innerJoin('RubroElegible','RubroElegible.ID=ActRubroElegible.RubroElegibleID')
            ->innerJoin('Actividad','Actividad.ID=ActRubroElegible.ActividadID')
            ->innerJoin('Componente','Componente.ID=Actividad.ComponenteID')
            ->innerJoin('Poa','Poa.ID=Componente.PoaID')
            ->innerJoin('Proyecto','Proyecto.ID=Poa.ProyectoID')
            ->where(['Proyecto.InvestigadorID'=>$investigadorid])
            ->andwhere(['RubroElegible.ID'=>$rubroelegibleid])
            ->andwhere(['AreSubCategoria.Codificacion'=>$correlativoid])
            ->distinct()
            ->groupBy('RubroElegible.ID,AreSubCategoria.Nombre,AreSubCategoria.Especifica,AreSubCategoria.CostoUnitario,AreSubCategoria.UnidadMedida,AreSubCategoria.EntidadParticipanteID,AreSubCategoria.FondoEntidadParticipanteID,AreSubCategoria.Codificacion')
            ->one();
            
        $arrayJson=['Success'=>true,'RubroElegible'=>$resultado['ID'],'Nombre'=>$resultado['Nombre'],'Especifica'=>$resultado['Especifica'],'CostoUnitario'=>$resultado['CostoUnitario'],'UnidadMedida'=>$resultado['UnidadMedida'],'EntidadParticipanteID'=>$resultado['EntidadParticipanteID'],'FondoEntidadParticipanteID'=>$resultado['FondoEntidadParticipanteID'],'Codificacion'=>$resultado['Codificacion']];
        return json_encode($arrayJson);
    }
    
    public function actionCodificacion($CodigoProyecto=null,$ActividadID=null)
    {
        $this->layout='vacio';
        $informacionGeneral=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacionGeneral->InvestigadorID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID and Estado=1',[':ProyectoID'=>$proyecto->ID])->one();
        //$componentes=Componente::find()->where('PoaID=:PoaID',[':PoaID'=>$poa->ID])->OrderBy('Correlativo asc')->all();
        $rubrosElegibles=RubroElegible::find()->all();
        return $this->render('recursos/codificacion',['CodigoProyecto'=>$CodigoProyecto,'rubrosElegibles'=>$rubrosElegibles,'ActividadID'=>$ActividadID]);
    }
    
    public function actionListadoCodificacion($CodigoProyecto=null,$RubroElegibleID=null){
        $this->layout='vacio';    
        if($CodigoProyecto)
        {
            $informacionGeneral=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])->one();
            $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacionGeneral->InvestigadorID])->one();
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
            $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacionGeneral->InvestigadorID])->one();
        }
        $PasoCritico=PasoCritico::find()->where('ProyectoID=:ProyectoID and Estado=1 and Situacion=2',[':ProyectoID'=>$proyecto->ID])->one();
        if($RubroElegibleID)
        {
            $resultados = (new \yii\db\Query())
            ->select('RubroElegible.ID,AreSubCategoria.Codificacion,AreSubCategoria.Especifica,AreSubCategoria.Nombre')
            ->from('AreSubCategoria')
            ->innerJoin('CronogramaAreSubCategoria','CronogramaAreSubCategoria.AreSubCategoriaID=AreSubCategoria.ID')
            ->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')
            ->innerJoin('RubroElegible','RubroElegible.ID=ActRubroElegible.RubroElegibleID')
            ->innerJoin('Actividad','Actividad.ID=ActRubroElegible.ActividadID')
            ->innerJoin('Componente','Componente.ID=Actividad.ComponenteID')
            ->innerJoin('Poa','Poa.ID=Componente.PoaID')
            ->innerJoin('Proyecto','Proyecto.ID=Poa.ProyectoID')
            ->innerJoin('Investigador','Investigador.ID=Proyecto.InvestigadorID')
            ->innerJoin('InformacionGeneral','InformacionGeneral.InvestigadorID=Investigador.ID')
            ->where(['InformacionGeneral.Codigo'=>$CodigoProyecto])
            ->andWhere(['RubroElegible.ID'=>$RubroElegibleID])
            ->andWhere(['between', 'CronogramaAreSubCategoria.Mes', $PasoCritico->MesInicio, $PasoCritico->MesFin])
            ->orderBy('RubroElegible.ID desc,AreSubCategoria.Codificacion desc')
            ->groupBy('RubroElegible.ID,AreSubCategoria.Codificacion,AreSubCategoria.Especifica,AreSubCategoria.Nombre')
            ->distinct()
            ->all();
        }
        else{
            $resultados = (new \yii\db\Query())
            ->select('RubroElegible.ID,AreSubCategoria.Codificacion,AreSubCategoria.Especifica,AreSubCategoria.Nombre')
            ->from('AreSubCategoria')
            ->innerJoin('CronogramaAreSubCategoria','CronogramaAreSubCategoria.AreSubCategoriaID=AreSubCategoria.ID')
            ->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')
            ->innerJoin('RubroElegible','RubroElegible.ID=ActRubroElegible.RubroElegibleID')
            ->innerJoin('Actividad','Actividad.ID=ActRubroElegible.ActividadID')
            ->innerJoin('Componente','Componente.ID=Actividad.ComponenteID')
            ->innerJoin('Poa','Poa.ID=Componente.PoaID')
            ->innerJoin('Proyecto','Proyecto.ID=Poa.ProyectoID')
            ->innerJoin('Investigador','Investigador.ID=Proyecto.InvestigadorID')
            ->innerJoin('InformacionGeneral','InformacionGeneral.InvestigadorID=Investigador.ID')
            ->where(['InformacionGeneral.Codigo'=>$CodigoProyecto])
            ->andWhere(['between', 'CronogramaAreSubCategoria.Mes', $PasoCritico->MesInicio, $PasoCritico->MesFin])
            ->orderBy('RubroElegible.ID desc,AreSubCategoria.Codificacion desc')
            ->groupBy('RubroElegible.ID,AreSubCategoria.Codificacion,AreSubCategoria.Especifica,AreSubCategoria.Nombre')
            ->distinct()
            ->all();
        }
        $nro=0;

        // print_r($resultados);//die();
        foreach($resultados as $result)
        {
            echo "<tr style='cursor:pointer' class='pintar_poa' id='".$nro."' data-rubro-elegible='".$result["ID"]."' data-codificacion='".$result["Codificacion"]."'>";
            echo "<td> " . $result["ID"] . "." . $result["Codificacion"] . " </td>";
            echo "<td> " . $result["Nombre"] ."/".$result["Especifica"]. "</td>";
            echo "</tr>";
            $nro++;
        }
        
    }
    
    public function actionRapido4(){
        $this->layout='estandar';
        ini_set('memory_limit', '-1');
        $model = new ActRubroElegible();
        if ( $model->load(Yii::$app->request->post()) ) {
            
            $csvMimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
            if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
                //var_dump($_FILES['ActRubroElegible']);die;
                if(is_uploaded_file($_FILES['file']['tmp_name'])){
                    
                    //open uploaded csv file with read only mode
                    $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
                    
                    //skip first line
                    //fgetcsv($csvFile);
                    
                    //parse data from csv file line by line
                    $Codigo=[];
                    $i=0;
                    while(($line = fgetcsv($csvFile)) !== FALSE){
                        if($i==0)
                        {
                            $Codigo=$line;
                            //var_dump($Codigo);die;
                           //$i++;
                        }
                        else
                        {
                            //echo '<pre>';print_r($Codigo);
                            //echo '<pre>';print_r($line); 
                            for($a=8;$a<count($line);$a=$a+2)
                            {
                                if(!empty($line[$a]) && $line[$a]!=0)
                                {
                                    $informacion=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>utf8_encode($line[0])])->one();
                                    if(!$informacion)
                                    {
                                        echo '----';var_dump(utf8_encode($line[0]));die;
                                    }
                                    else
                                    {
                                        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacion->InvestigadorID])->one();    
                                    }
                                    $poa=Poa::find()->where('ProyectoID=:ProyectoID and Estado=1',[':ProyectoID'=>$proyecto->ID])->one();
                                    $entidadParticipante=EntidadParticipante::find()->where('InvestigadorID=:InvestigadorID and TipoInstitucionID=3',[':InvestigadorID'=>$informacion->InvestigadorID])->one();
                                    $mes = InformacionGeneral::find()
                                        ->select('Meses')
                                        ->where(['InvestigadorID' => $informacion->InvestigadorID])
                                        ->one();
                                    $componenteID=substr(utf8_encode($Codigo[$a]),0,1);
                                    $actividadID=substr(utf8_encode($Codigo[$a]),2,1);
                                    
                                    $componente=Componente::find()
                                                            ->where('PoaID=:PoaID and Correlativo=:Correlativo',
                                                            [':PoaID'=>$poa->ID,':Correlativo'=>utf8_encode($componenteID)])
                                                            ->one();
                                    if(!$componente)
                                    {
                                        echo '---';var_dump($componenteID);die;
                                    }
                                    else
                                    {
                                        
                                    }
                                    $actividad=Actividad::find()
                                                            ->where('ComponenteID=:ComponenteID and Correlativo=:Correlativo',[':ComponenteID'=>$componente->ID,':Correlativo'=>utf8_encode($actividadID)])
                                                            ->one();
                                                            
                                    if(!$actividad)
                                    {
                                        echo $componenteID;
                                        echo '--'.$actividadID;

                                        echo '--';var_dump($componente->ID);die;
                                    }
                                    else
                                    {
                                        $act = ActRubroElegible::find()
                                        ->where(['ActividadID'=> $actividad->ID,'RubroElegibleID' => utf8_encode($line[1])])
                                        ->one();
                                    }
                                    
                                    if(!empty($act)){
                                        $id = $act->ID;
                                    }else{
                                        $ActRubroElegible=new ActRubroElegible;
                                        $ActRubroElegible->ActividadID=$actividad->ID;
                                        $ActRubroElegible->RubroElegibleID=utf8_encode($line[1]);
                                        $ActRubroElegible->save();
                                        $id = $ActRubroElegible->ID;
                                        
                                        for ($m=1; $m <= $mes->Meses; $m++) { 
                                            $cronogramaAct = new CronogramaActRubroElegible();
                                            $cronogramaAct->ActRubroElegibleID = $id;
                                            $cronogramaAct->Mes = $m;
                                            $cronogramaAct->MetaFinanciera = 0;
                                            $cronogramaAct->PoafMetaFinanciera = 0;
                                            $cronogramaAct->save();
                                        }
                                    }
                                    $detalle=new AreSubCategoria;
                                    $detalle->Nombre=utf8_encode($line[2]);
                                    $detalle->Especifica=utf8_encode($line[3]);
                                    $detalle->Detalle=utf8_encode($line[4]);
                                    $detalle->Codificacion=utf8_encode((int)substr($line[5],2,4));
                                    $detalle->UnidadMedida=utf8_encode($line[6]);
                                    $detalle->CostoUnitario=utf8_encode($line[7]);
                                    $detalle->MetaFisica=utf8_encode($line[$a]);
                                    $detalle->Total = ($detalle->CostoUnitario * $detalle->MetaFisica);
                                    $detalle->ActRubroElegibleID = $id;
                                    $detalle->Correlativo=$this->CorrelativoRecurso($id);
                                    $detalle->Situacion=0;
                                    $detalle->MetaFisicaEjecutada=0;
                                    $detalle->MetaAvance=0;
                                    $detalle->EntidadParticipanteID=$entidadParticipante->ID;
                                    $detalle->FondoEntidadParticipanteID=1;
                                    $detalle->save();
                                    $idz = $detalle->ID;
                                    if(!$idz)
                                    {
                                        // echo '<pre>'; print_r($detalle); die();
                                        echo '-';var_dump($i);die;
                                    }
                                    for ($b=1; $b <= $mes->Meses; $b++) {
                                        $cronoAreSubCat = new CronogramaAreSubCategoria();
                                        $cronoAreSubCat->AreSubCategoriaID = $idz;
                                        $cronoAreSubCat->Mes = $b;
                                        $cronoAreSubCat->MetaFinanciera = 0;
                                        $cronoAreSubCat->PoafMetaFinanciera = 0;
                                        $cronoAreSubCat->MetaFisicaEjecutada = 0;
                                        $cronoAreSubCat->MetaAvance = 0;
                                        $cronoAreSubCat->MetaFisica = 0;
                                        $cronoAreSubCat->SituacionEjecucion=0;
                                        $cronoAreSubCat->Situacion=0;
                                        $cronoAreSubCat->save();
                                    }
                                }
                                
                            }
                            
                        }
                        $i++;
                        
                    }
                    
                    //close opened csv file
                    fclose($csvFile);
                   
                }
            }
            
        }
        return $this->render('rapido4');
    }
    
    public function actionDbf()
    {
        $this->layout='vacio';
        $conex        = dbase_open('D:\SIAF_VFP_GN\data\expediente.dbf', 1);
        if($conex){
            $arrData = array();
            $total_registros = dbase_numrecords($conex);
            for ($i = 1; $i <= $total_registros; $i++){
                $arrData[] = dbase_get_record($conex,$i);
            }
            echo '<pre>',print_r($arrData),'</pre>';
        }else{
            echo 'No se pudo acceder al fichero dbf';
        }
        die;
    }


    public function actionPresupuesto($CodigoProyecto=null)
    {
        $this->layout='vacio';
        /*
        $usuario            = Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador  = Persona::findOne($usuario->PersonaID);
        $investigador       = Investigador::find()->where(['UsuarioID'=>$usuario->id])->one();
        */
        $informacion=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])->one();
        
        $proyecto = Poa::find()
        ->select('Poa.ID, Poa.ProyectoID')
        ->innerJoin('Proyecto','Poa.ProyectoID = Proyecto.ID')
        ->where(['Proyecto.InvestigadorID'=> $informacion->InvestigadorID,'Poa.Estado' => 1])
        ->one();


        $db = Yii::$app->db;
        $total=$db->createCommand("
            SELECT 
                    InformacionGeneral.Codigo,
                    sum(AreSubCategoria.CostoUnitario*AreSubCategoria.MetaFisica) Total
            FROM RubroElegible
            INNER JOIN ActRubroElegible ON ActRubroElegible.RubroElegibleID=RubroElegible.ID
            INNER JOIN AreSubCategoria ON AreSubCategoria.ActRubroElegibleID=ActRubroElegible.ID
            INNER JOIN Actividad ON Actividad.ID=ActRubroElegible.ActividadID
            INNER JOIN Componente ON Componente.ID=Actividad.ComponenteID
            INNER JOIN Poa ON Poa.ID=Componente.PoaID
            INNER JOIN Proyecto ON Proyecto.ID=Poa.ProyectoID
            INNER JOIN InformacionGeneral ON InformacionGeneral.InvestigadorID=Proyecto.InvestigadorID
            WHERE InformacionGeneral.Codigo='".$CodigoProyecto."' and Componente.Estado=1 and Actividad.Estado=1
            GROUP BY InformacionGeneral.Codigo")->queryOne();
        
        $resultados=$db->createCommand("
            SELECT 
                    RubroElegible.Nombre,
                    RubroElegible.LimiteSegunBaseConcurso,
                    (RubroElegible.LimiteSegunBaseConcurso*".$total["Total"].")/100 TotalSegunBase,
                    (sum(AreSubCategoria.CostoUnitario*AreSubCategoria.MetaFisica)*100/".$total["Total"].") LimiteActual,
                    sum(AreSubCategoria.CostoUnitario*AreSubCategoria.MetaFisica) TotalRubro
            FROM RubroElegible
            INNER JOIN ActRubroElegible ON ActRubroElegible.RubroElegibleID=RubroElegible.ID
            INNER JOIN AreSubCategoria ON AreSubCategoria.ActRubroElegibleID=ActRubroElegible.ID
            INNER JOIN Actividad ON Actividad.ID=ActRubroElegible.ActividadID
            INNER JOIN Componente ON Componente.ID=Actividad.ComponenteID
            INNER JOIN Poa ON Poa.ID=Componente.PoaID
            INNER JOIN Proyecto ON Proyecto.ID=Poa.ProyectoID
            INNER JOIN InformacionGeneral ON InformacionGeneral.InvestigadorID=Proyecto.InvestigadorID
            WHERE InformacionGeneral.Codigo='".$CodigoProyecto."' and Componente.Estado=1 and Actividad.Estado=1
            GROUP BY RubroElegible.Nombre,RubroElegible.LimiteSegunBaseConcurso")->queryAll();


        $totalObjetivo = $db->createCommand("
            SELECT sum(AreSubCategoria.CostoUnitario*AreSubCategoria.MetaFisica) Total,Componente.Correlativo , Componente.Nombre FROM Componente
            INNER JOIN Actividad ON Componente.ID = Actividad.ComponenteID
            INNER JOIN ActRubroElegible ON Actividad.ID = ActRubroElegible.ActividadID
            INNER JOIN AreSubCategoria ON ActRubroElegible.ID = AreSubCategoria.ActRubroElegibleID
            WHERE Componente.PoaID = ".$proyecto->ID." and Componente.Estado=1 and Actividad.Estado=1
            GROUP BY Componente.Correlativo,Componente.Nombre
            ORDER BY 2")->queryAll();




        return $this->render('presupuesto',['resultados'=>$resultados,'totalObjetivo'=>$totalObjetivo]);
    }
    
    public function actionRapido5()
    {
        
        $this->layout='estandar';
        ini_set('memory_limit', '-1');
        $model = new ActRubroElegible();
        if ( $model->load(Yii::$app->request->post()) ) {
            
            $csvMimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
            if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
                //var_dump($_FILES['ActRubroElegible']);die;
                if(is_uploaded_file($_FILES['file']['tmp_name'])){
                    
                    //open uploaded csv file with read only mode
                    $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
                    
                    //skip first line
                    //fgetcsv($csvFile);
                    
                    //parse data from csv file line by line
                    $Codigo=[];
                    $i=0;
                    while(($line = fgetcsv($csvFile)) !== FALSE){
                        if($i==0)
                        {
                            $Codigo=$line;
                            //var_dump($Codigo);die;
                           //$i++;
                        }
                        else
                        {
                            //echo '<pre>';print_r($Codigo);
                            //echo '<pre>';print_r($line); 
                            for($a=8;$a<count($line);$a=$a+2)
                            {
                                if(!empty($line[$a]) && $line[$a]!=0)
                                {
                                    $informacion=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>utf8_encode($line[0])])->one();
                                    if(!$informacion)
                                    {
                                        echo '----';var_dump(utf8_encode($line[0]));die;
                                    }
                                    else
                                    {
                                        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacion->InvestigadorID])->one();    
                                    }
                                    $poa=Poa::find()->where('ProyectoID=:ProyectoID and Estado=1',[':ProyectoID'=>$proyecto->ID])->one();
                                    $entidadParticipante=EntidadParticipante::find()->where('InvestigadorID=:InvestigadorID and TipoInstitucionID=3',[':InvestigadorID'=>$informacion->InvestigadorID])->one();
                                    $mes = InformacionGeneral::find()
                                        ->select('Meses')
                                        ->where(['InvestigadorID' => $informacion->InvestigadorID])
                                        ->one();
                                        
                                    $componenteID=substr(utf8_encode($line[9]),0,1);
                                    $actividadID=substr(utf8_encode($line[9]),2,1);
                                    
                                    
                                    $RubroElegibleID=explode('.',$line[5])[0];
                                    $Codificacion=explode('.',$line[5])[1];
                                    echo $RubroElegibleID.".".$Codificacion;
                                    $componente=Componente::find()
                                                            ->where('PoaID=:PoaID and Correlativo=:Correlativo',
                                                            [':PoaID'=>$poa->ID,':Correlativo'=>utf8_encode($componenteID)])
                                                            ->one();
                                    if(!$componente)
                                    {
                                        echo '---';var_dump($componenteID);die;
                                    }
                                    else
                                    {
                                        
                                    }
                                    $actividad=Actividad::find()
                                                            ->where('ComponenteID=:ComponenteID and Correlativo=:Correlativo',[':ComponenteID'=>$componente->ID,':Correlativo'=>utf8_encode($actividadID)])
                                                            ->one();
                                                            
                                    if(!$actividad)
                                    {
                                        echo $componenteID;
                                        echo '--'.$actividadID;

                                        echo '--';var_dump($componente->ID);die;
                                    }
                                    else
                                    {
                                        $act = ActRubroElegible::find()
                                        ->where(['ActividadID'=> $actividad->ID,'RubroElegibleID' => $RubroElegibleID])
                                        ->one();
                                    }
                                    
                                    if(!empty($act)){
                                        $id = $act->ID;
                                        $recur=AreSubCategoria::find()->where('ActRubroElegibleID=:ActRubroElegibleID and Codificacion=:Codificacion',
                                                                              [':ActRubroElegibleID'=>$id,':Codificacion'=>$Codificacion])->one();
                                        if(!$recur)
                                        {
                                            var_dump($act);die;
                                        }
                                        //$cronog=CronogramaAreSubCategoria::find()->where('AreSubCategoriaID=:AreSubCategoriaID',[':AreSubCategoriaID'=>$recur->ID])->all();
                                        
                                        
                                        $a=10;
                                        for ($i=1; $i <= $informacion->Meses; $i++) {
                                            
                                            $cronog=CronogramaAreSubCategoria::find()
                                                    ->where('AreSubCategoriaID=:AreSubCategoriaID and Mes=:Mes',[':AreSubCategoriaID'=>$recur->ID,':Mes'=>$i])
                                                    ->one();
                                            $cronog->MetaFisica=(float)(isset($line[$a]))?utf8_encode($line[$a]):0;
                                            $cronog->update();
                                            $a=$a+2;
                                        } 
                                    }
                                    
                                    
                                    /*
                                    $detalle=new AreSubCategoria;
                                    $detalle->Nombre=utf8_encode($line[2]);
                                    $detalle->Especifica=utf8_encode($line[3]);
                                    $detalle->Detalle=utf8_encode($line[4]);
                                    $detalle->Codificacion=utf8_encode((int)substr($line[5],2,4));
                                    $detalle->UnidadMedida=utf8_encode($line[6]);
                                    $detalle->CostoUnitario=utf8_encode($line[7]);
                                    $detalle->MetaFisica=utf8_encode($line[$a]);
                                    $detalle->Total = ($detalle->CostoUnitario * $detalle->MetaFisica);
                                    $detalle->ActRubroElegibleID = $id;
                                    $detalle->Correlativo=$this->CorrelativoRecurso($id);
                                    $detalle->Situacion=0;
                                    $detalle->MetaFisicaEjecutada=0;
                                    $detalle->MetaAvance=0;
                                    $detalle->EntidadParticipanteID=$entidadParticipante->ID;
                                    $detalle->FondoEntidadParticipanteID=1;
                                    $detalle->save();
                                    $idz = $detalle->ID;
                                    if(!$idz)
                                    {
                                        // echo '<pre>'; print_r($detalle); die();
                                        echo '-';var_dump($i);die;
                                    }
                                    for ($b=1; $b <= $mes->Meses; $b++) {
                                        $cronoAreSubCat = new CronogramaAreSubCategoria();
                                        $cronoAreSubCat->AreSubCategoriaID = $idz;
                                        $cronoAreSubCat->Mes = $b;
                                        $cronoAreSubCat->MetaFinanciera = 0;
                                        $cronoAreSubCat->PoafMetaFinanciera = 0;
                                        $cronoAreSubCat->MetaFisicaEjecutada = 0;
                                        $cronoAreSubCat->MetaAvance = 0;
                                        $cronoAreSubCat->MetaFisica = 0;
                                        $cronoAreSubCat->SituacionEjecucion=0;
                                        $cronoAreSubCat->save();
                                    }*/
                                }
                                
                            }
                            
                        }
                        $i++;
                        
                    }
                    
                    //close opened csv file
                    fclose($csvFile);
                   
                }
            }
            
        }
        return $this->render('rapido5');
    }
    
    
}
