<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\PasoCritico;
use app\models\Usuario;
use app\models\Persona;
use app\models\Investigador;
use app\models\Proyecto;
use app\models\InformacionGeneral;
use app\models\Poa;
use app\models\Componente;
use app\models\Actividad;
use app\models\CronogramaProyecto;
use app\models\ActRubroElegible;
use app\models\CronogramaComponente;
use app\models\CronogramaActividad;
use app\models\CronogramaActRubroElegible;
use app\models\RubroElegible;
use app\models\AreSubCategoria;
use app\models\CronogramaAreSubCategoria;
use app\models\MarcoLogicoActividad;
use app\models\AreSubCategoriaObservacion;
use app\models\CertificacionPresupuestal;
use app\models\DetalleCertificacionPresupuestal;
use app\models\UnidadOperativa;
use app\models\SiafSecuencialProyecto;
use app\models\CajaChica;
use app\models\Orden;
use app\models\Siaf;
use app\models\Requerimiento;
use app\models\UnidadEjecutora;
use app\models\ReProgramacionRecurso;
use app\models\DetalleReProgramacionRecurso;
use app\models\ReProgramacionTarea;
use app\models\DetalleReProgramacionTarea;
use app\models\Tarea;
use app\models\CronogramaTarea;
use PhpOffice\PhpWord\PhpWord;
use yii\web\UploadedFile;


class ReProgramacionTareaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout='estandar';
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $Investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $InformacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$Investigador->ID])->one();
        $Proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$Investigador->ID])->one();
        $Poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$Proyecto->ID])->one();
        $Componentes=Componente::find()->where('PoaID=:PoaID and Estado=1',[':PoaID'=>$Poa->ID])->orderBy('Correlativo asc')->all();
        
        
        return $this->render('index',['Componentes'=>$Componentes,'CodigoProyecto'=>$InformacionGeneral->Codigo]);
    }
    
    public function actionReProgramar($TareaID=null)
    {
        $this->layout='estandar';
        $Tarea=Tarea::find()->where('ID=:ID',[':ID'=>$TareaID])->one();
        $Actividad=Actividad::findOne($Tarea->ActividadID);
        $Componente=Componente::findOne($Actividad->ComponenteID);
        $Poa=Poa::findOne($Componente->PoaID);
        $Proyecto=Proyecto::findOne($Poa->ProyectoID);
        $InformacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$Proyecto->InvestigadorID])->one();
        $CronogramaTareas=CronogramaTarea::find()->where('TareaID=:TareaID',[':TareaID'=>$Tarea->ID])->all();
        $ReProgramacionTarea=new ReProgramacionTarea;
        if($ReProgramacionTarea->load(Yii::$app->request->post())){
            $ReProgramacionTarea->TareaID=$Tarea->ID;
            $ReProgramacionTarea->Situacion=1;
            $ReProgramacionTarea->Estado=1;
            $ReProgramacionTarea->FechaRegistro=date('Ymd');
            $ReProgramacionTarea->CodigoProyecto=$InformacionGeneral->Codigo;
            
            $ReProgramacionTarea->save();
            if(!$ReProgramacionTarea->MetasFisicas)
            {
                $countDetalles=0;
            }
            else
            {
                $countDetalles=count(array_filter($ReProgramacionTarea->MetasFisicas));
            }
            for($i=0;$i<$countDetalles;$i++)
            {
                if(isset($ReProgramacionTarea->MetasFisicas[$i]))
                {
                    $detalle=new DetalleReProgramacionTarea;
                    $detalle->ReProgramacionTareaID=$ReProgramacionTarea->ID;
                    $detalle->MetaFisica= $ReProgramacionTarea->MetasFisicas[$i];
                    $detalle->Mes = $ReProgramacionTarea->Meses[$i];;
                    $detalle->save();
                }
            }
            
            $ReProgramacionTarea->Archivo = UploadedFile::getInstance($ReProgramacionTarea, 'Archivo');
            if($ReProgramacionTarea->Archivo)
            {
                $ReProgramacionTarea->Archivo->saveAs('reprogramaciontareas/' . $ReProgramacionTarea->ID . '.' . $ReProgramacionTarea->Archivo->extension);
                $ReProgramacionTarea->Evidencia=$ReProgramacionTarea->ID . '.' . $ReProgramacionTarea->Archivo->extension;
            }
            $ReProgramacionTarea->update();
            
            return $this->redirect(['listado-tareas','TareaID'=>$TareaID]);
        }
        return $this->render('re-programar',['Tarea'=>$Tarea,'Componente'=>$Componente,'Actividad'=>$Actividad,'CronogramaTareas'=>$CronogramaTareas]);
    }
    
    public function actionVerTarea($ID=null)
    {
        $this->layout='vacio';
        $ReProgramarTarea=ReProgramacionTarea::find()->where('Situacion in (1,3) and ID=:ID',[':ID'=>$ID])->one();
        $Tarea=Tarea::find()->where('ID=:ID',[':ID'=>$ReProgramarTarea->TareaID])->one();
        
        $Actividad=Actividad::findOne($Tarea->ActividadID);
        $Componente=Componente::findOne($Actividad->ComponenteID);
        $Poa=Poa::findOne($Componente->PoaID);
        $Proyecto=Proyecto::findOne($Poa->ProyectoID);
        $InformacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$Proyecto->InvestigadorID])->one();
        $CronogramaTareas=CronogramaTarea::find()->where('TareaID=:TareaID',[':TareaID'=>$Tarea->ID])->all();
       
        
        return $this->render('ver-tarea',['Tarea'=>$Tarea,'Componente'=>$Componente,'Actividad'=>$Actividad,'CronogramaTareas'=>$CronogramaTareas,'ReProgramarTarea'=>$ReProgramarTarea]);
    }
    
    public function actionUafsi()
    {
        $this->layout='estandar';
        return $this->render('uafsi');
    }
    
    public function actionListadoTareas($TareaID=null)
    {
        $this->layout='estandar';
        return $this->render('listado-tareas',['TareaID'=>$TareaID]);
    }
    
    public function actionListadoReProTareas($TareaID=null)
    {
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        
        $resultados = (new \yii\db\Query())
                        ->select('Tarea.Descripcion TareaNombre,Tarea.*,ReProgramacionTarea.ID ReProgramacionTareaID,ReProgramacionTarea.*,Situacion.ID SituacionID,Situacion.Descripcion SituacionDescripcion')
                        ->from('ReProgramacionTarea')
                        ->innerJoin('Tarea','Tarea.ID=ReProgramacionTarea.TareaID')
                        ->innerJoin('Situacion','Situacion.ID=ReProgramacionTarea.Situacion')
                        ->where('ReProgramacionTarea.TareaID=:TareaID and ReProgramacionTarea.Situacion in (1,3,6) AND ReProgramacionTarea.Estado=1 AND ReProgramacionTarea.CodigoProyecto=:CodigoProyecto',[':CodigoProyecto'=>$informacionGeneral->Codigo,':TareaID'=>$TareaID])
                        ->all();
        foreach($resultados as $resultado)
        {
            echo '<tr>';
                echo '<td>';
                    echo $resultado['TareaNombre'];
                echo '</td>';
                echo '<td>';
                    echo $resultado['UnidadMedida'];
                echo '</td>';
                echo '<td>';
                    echo $resultado['Justificacion'];
                echo '</td>';
                echo '<td>';
                    echo $resultado['SituacionDescripcion'];
                echo '</td>';
                echo '<td>';
                    if($resultado['SituacionID']==1)
                    {
                        echo '<a href="#" id='.$resultado['ReProgramacionTareaID'].' class="btn btn-primary btn-ver"><span class="fa fa-eye"></span></a> ';
                        echo ' <a href="#" id='.$resultado['ReProgramacionTareaID'].' class="btn btn-danger btn-eliminar"> <span class="fa fa-remove"></span></a>';
                    }
                    elseif($resultado['SituacionID']==3)
                    {
                        echo ' <a href="#" id='.$resultado['ReProgramacionTareaID'].' class="btn btn-primary btn-ver"><span class="fa fa-eye"></span></a>';
                    }
                    elseif($resultado['SituacionID']==6)
                    {
                        echo ' <a href="#" id='.$resultado['ReProgramacionTareaID'].' class="btn btn-primary btn-ver"><span class="fa fa-eye"></span></a>';
                    }
                echo '</td>';
            echo '</tr>';
        }
    }
    
    
    public function actionListadoReProgramacionTareas()
    {
        $resultados = (new \yii\db\Query())
                        ->select('Tarea.Descripcion TareaNombre,Tarea.*,ReProgramacionTarea.ID ReProgramacionTareaID,ReProgramacionTarea.*,Situacion.ID SituacionID,Situacion.Descripcion SituacionDescripcion')
                        ->from('ReProgramacionTarea')
                        ->innerJoin('Tarea','Tarea.ID=ReProgramacionTarea.TareaID')
                        ->innerJoin('UsuarioProyecto','UsuarioProyecto.CodigoProyecto=ReProgramacionTarea.CodigoProyecto')
                        ->innerJoin('Usuario','Usuario.ID=UsuarioProyecto.UsuarioID')
                        ->innerJoin('Situacion','Situacion.ID=ReProgramacionTarea.Situacion')
                        ->where('ReProgramacionTarea.Situacion in (1,3) AND ReProgramacionTarea.Estado=1 AND Usuario.ID=:ID',[':ID'=>\Yii::$app->user->id])
                        ->all();
        foreach($resultados as $resultado)
        {
            echo '<tr>';
                echo '<td>';
                    echo $resultado['TareaNombre'];
                echo '</td>';
                echo '<td>';
                    echo $resultado['UnidadMedida'];
                echo '</td>';
                echo '<td>';
                    echo $resultado['Justificacion'];
                echo '</td>';
                echo '<td>';
                    echo $resultado['SituacionDescripcion'];
                echo '</td>';
                echo '<td>';
                    if($resultado['SituacionID']==1)
                    {
                        echo '<a href="#" id='.$resultado['ReProgramacionTareaID'].' class="btn btn-primary btn-confirmar"><span class="fa fa-check"></span></a>';
                        echo ' <a href="#" id='.$resultado['ReProgramacionTareaID'].' class="btn btn-primary btn-ver"><span class="fa fa-eye"</a>';
                    }
                    elseif($resultado['SituacionID']==3)
                    {
                        
                        echo ' <a href="#" id='.$resultado['ReProgramacionTareaID'].' class="btn btn-primary btn-ver"><span class="fa fa-eye"</a>';
                    }
                echo '</td>';
            echo '</tr>';
        }
    }
    public function actionEliminarTarea($ID=null)
    {
        $ReProgramacionTarea=ReProgramacionTarea::find()->where('ID=:ID and Estado=1',[':ID'=>$ID])->one();
        $ReProgramacionTarea->Situacion=6;
        $ReProgramacionTarea->update();
        $arr = ['Success' => true];
        echo json_encode($arr);
    }
    
    public function actionAprobarTarea($ID=null)
    {
        $ReProgramacionTarea=ReProgramacionTarea::find()->where('ID=:ID and Estado=1',[':ID'=>$ID])->one();
        $CountReProgramacionTarea=ReProgramacionTarea::find()->where('ID=:ID and Estado=1',[':ID'=>$ID])->count();
        $ReProgramacionTarea->Situacion=3;
        $ReProgramacionTarea->update();
        $CronogramaTareas=CronogramaTarea::find()->where('TareaID=:TareaID',[':TareaID'=>$ReProgramacionTarea->TareaID])->all();
        $Tarea=Tarea::findOne($ReProgramacionTarea->TareaID);
        if($CountReProgramacionTarea==1)
        {
            
            $repro=new ReProgramacionTarea;
            $repro->TareaID=$ReProgramacionTarea->TareaID;
            $repro->Situacion=8;
            $repro->Estado=1;
            $repro->FechaRegistro=date('Ymd');
            $repro->CodigoProyecto=$ReProgramacionTarea->CodigoProyecto;
            $repro->save();
            //$CronogramaRecursos=CronogramaAreSubCategoria::find()->where('AreSubCategoriaID=:AreSubCategoriaID',[':AreSubCategoriaID'=>$Recurso->ID])->all();
            Yii::$app->db->createCommand("  insert into DetalleReProgramacionTarea (ReProgramacionTareaID,MetaFisica,Mes)
                                            select ".$repro->ID.",MetaFisica,Mes from CronogramaTarea where TareaID=".$Tarea->ID."")
                                            ->execute();
        }
        $cont=0;
        foreach($CronogramaTareas as $CronogramaTarea)
        {
            $detalle=DetalleReProgramacionTarea::find()
                        ->where('ReProgramacionTareaID=:ReProgramacionTareaID and Mes=:Mes',[':ReProgramacionTareaID'=>$ReProgramacionTarea->ID,':Mes'=>$CronogramaTarea->Mes])->one();
            if($CronogramaTarea->Mes==$detalle->Mes)
            {
                $CronogramaTarea->MetaFisica=$detalle->MetaFisica;
                $CronogramaTarea->update();
                $cont=$cont+$CronogramaTarea->MetaFisica;
            }
        }
        $Tarea->MetaFisica=$cont;
        $Tarea->update();
        $arr = ['Success' => true];
        echo json_encode($arr);
        
    }
}
