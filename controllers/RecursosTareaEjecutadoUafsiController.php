<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Usuario;
use app\models\Persona;
use app\models\Investigador;
use app\models\Proyecto;
use app\models\Componente;
use app\models\Actividad;
use app\models\Poa;
use app\models\InformacionGeneral;
use app\models\CronogramaProyecto;
use app\models\ActRubroElegible;
use app\models\CronogramaComponente;
use app\models\CronogramaActividad;
use app\models\CronogramaActRubroElegible;
use app\models\RubroElegible;
use app\models\AreSubCategoria;
use app\models\CronogramaAreSubCategoria;
use app\models\MarcoLogicoActividad;
use app\models\Tarea;
use app\models\CronogramaTarea;
use app\models\TareaObservacion;
use app\models\Pat;
use app\models\JustificacionTarea;
use app\models\ObservacionGeneral;

use yii\web\UploadedFile;
class RecursosTareaEjecutadoUafsiController extends Controller
{
    /**
     * @inheritdoc
     */
    
    public $investigador = NULL;
    public function init(){
        if(!Yii::$app->user->isGuest){
            $usuario            = Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador  = Persona::findOne($usuario->PersonaID);
            $investigador       = Investigador::find()->where(['UsuarioID'=>$usuario->id])->one();
            if($investigador)
            {
                $this->investigador = $investigador->ID;
            }
        }
        else
        {
            $this->redirect('/login');
            return false; //not run the action
        }
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $this->layout='estandar';
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID and Estado=1',[':ProyectoID'=>$proyecto->ID])->one();
        $componentes=Componente::find()->where('PoaID=:PoaID and Estado=1',[':PoaID'=>$poa->ID])->all();
        $avanceComponente=0;
        return $this->render('index',['componentes'=>$componentes,'proyecto'=>$proyecto,'usuario'=>$usuario,'avanceComponente'=>$avanceComponente]);
    }

    public function actionRecursosForm(){
        $this->layout='vacio';
        return $this->render('recursos/_recursos_form');
    }

    /**
     * Rescursos JSON 
     * @return [type] [description]
     */
    public function actionTareasJson($codigo=null){
        $this->layout='vacio';
        // Proyectos
        $usuario=Usuario::find()->where('username=:username',[':username'=>$codigo])->one();
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $proyecto = Poa::find()
            ->select('Poa.ID, Poa.ProyectoID')
            ->innerJoin('Proyecto','Poa.ProyectoID = Proyecto.ID')
            ->where(['Proyecto.InvestigadorID'=> $investigador->ID,'Poa.Estado' => 1])
            ->one();
        
        $proyectox = array(
            'ID'             => $proyecto->ProyectoID, 
            'Componentes'    => $this->get_objetivo($proyecto->ID),
            'Cronogramas'    => $this->getCronogramaProyecto($proyecto->ProyectoID,'proyecto')
        );

        echo json_encode($proyectox); 
    }

    private function get_objetivo($idPoa){
        $jsonComponente = array();
        $component = Componente::find()
                ->where(['Estado' => 1, 'PoaID' => $idPoa])
                ->orderBy('Correlativo asc')
                ->all();
        if(!empty($component) ){
            foreach ($component as $comp) {
                if(!$comp->Avance)
                {
                    $comp->Avance=0;
                }
                $jsonComponente[] = array(
                    'ID'            => $comp->ID, 
                    'Nombre'        => $comp->Nombre,
                    'Peso'          => $comp->Peso,
                    'Avance'        => $comp->Avance,
                    'Correlativo'   => $comp->Correlativo,
                    'Actividades'   => $this->get_actividades($comp->ID), 
                    'Cronogramas'   => $this->getCronogramaProyecto($comp->ID,'componente')
                );
            }
        }
        return $jsonComponente;
    }

    private function get_actividades($idComponente){
        $jsonActividades = array();
        $actv = Actividad::find()
                ->where(['Estado' => 1, 'ComponenteID' => $idComponente])
                ->orderBy('Correlativo asc')
                ->all();
        if(!empty($actv) ){
            foreach ($actv as $actvd) {
                if(!$actvd->Avance){
                    $actvd->Avance=0;
                }
                
                $jsonActividades[] = array(
                    'ID'            => $actvd->ID, 
                    'ComponenteID'  => $actvd->ComponenteID, 
                    'Nombre'        => $actvd->Nombre,
                    'Avance'        => $actvd->Avance,
                    'Correlativo'   => $actvd->Correlativo, 
                    'Peso'          => $actvd->Peso, 
                    'UnidadMedida'  => $this->get_actividadesmarco($actvd->ID)->IndicadorUnidadMedida, 
                    'CostoUnitario' => $actvd->CostoUnitario, 
                    'MetaFisica'    => $this->get_actividadesmarco($actvd->ID)->IndicadorMetaFisica,
                    'Tareas'        => $this->get_actTareas($actvd->ID),
                    'Cronogramas'   => $this->getCronogramaProyecto($actvd->ID,'actividad')
                );
            }
        }
        return $jsonActividades;
    }

    private function get_actTareas($idActividad){
        $jsonActividades = array();
        $actv = Tarea::find()
                ->where(['ActividadID' => $idActividad])
                ->all();
        if(!empty($actv) ){
            foreach ($actv as $actvd) {
                $jsonActividades[] = array(
                    'ID'                => $actvd->ID, 
                    'ActividadID'       => $actvd->ActividadID, 
                    'Descripcion'       => $actvd->Descripcion, 
                    'UnidadMedida'      => $actvd->UnidadMedida,
                    'Correlativo'       => $actvd->Correlativo, 
                    'Peso'              => $actvd->Peso,
                    'MetaFisica'        => $actvd->MetaFisica,
                    'Situacion'         => $actvd->Situacion,
                    'Observacion'       => $this->getObservacionTarea($actvd->ID),
                    'Situacion'             => $actvd->Situacion,
                    'MetaFisicaEjecutada'   => $actvd->MetaFisicaEjecutada,
                    'MetaAvance'             => $actvd->MetaAvance,
                    'Cronogramas'       => $this->getCronogramaProyecto($actvd->ID,'Tareas')
                );
            }
        }
        return $jsonActividades;
    }

    private function get_rubroElegible($idRubro){
        $jsonActividades = array();
            $actv = RubroElegible::find()
                    ->where(['ID' => $idRubro])
                    ->all();

        if(!empty($actv) ){
            foreach ($actv as $actvd) {
                $jsonActividades = array(
                    'TipoRubroElegibleID'=> $actvd->TipoRubroElegibleID, 
                    'ID'                 => $actvd->ID, 
                    'Nombre'             => $actvd->Nombre
                );
            }
        }
        return $jsonActividades;
    }

    private function get_areSubCategoria($idActRubroElegible){
        $jsonActividades = array();
        $actv = AreSubCategoria::find()
                ->where(['ActRubroElegibleID' => $idActRubroElegible])
                ->all();
        if(!empty($actv) ){
            foreach ($actv as $actvd) {
                $jsonActividades[] = array(
                    'ID'                    => $actvd->ID, 
                    'ActRubroElegibleID'    => $actvd->ActRubroElegibleID,
                    'Nombre'                => $actvd->Nombre, 
                    'UnidadMedida'          => $actvd->UnidadMedida, 
                    'CostoUnitario'         => $actvd->CostoUnitario, 
                    'MetaFisica'            => $actvd->MetaFisica, 
                    'Total'                 => $actvd->Total, 
                    'TotalConMetaFisica'    => $actvd->TotalConMetaFisica,
                    'Cronogramas'           => $this->getCronogramaProyecto($actvd->ID,'areSubCategoria')
                );
            }
        }
        return $jsonActividades;
    }

    private function get_actividadesmarco($idActividad){
        $jsonActividadesMarco = array();
        $actvmarco = MarcoLogicoActividad::find()
                ->where(['Estado' => 1, 'ActividadID' => $idActividad])
                ->one();
        if(!$actvmarco)
        {
            $actvmarco=new MarcoLogicoActividad;
        }
        return $actvmarco;
    }

    private function getCronogramaProyecto($id,$tipo = ''){
        $this->layout='vacio';
        $meses = array();
        $cronograma = array();
        switch ($tipo) {
            case 'proyecto':
                $meses = CronogramaProyecto::find()
                        ->where(['ProyectoID'=> $id])
                        ->all();
                $indicador = 'ProyectoID';
            break;
            case 'componente':
                $meses = CronogramaComponente::find()
                        ->where(['ComponenteID'=> $id])
                        ->all();
                $indicador = 'ComponenteID';
            break;

            case 'actividad':
                $meses = CronogramaActividad::find()
                        ->where(['ActividadID'=> $id])
                        ->all();
                $indicador = 'ActividadID';
            break;

            case 'Tareas':
                
                $meses = CronogramaTarea::find()
                        ->where(['TareaID'=> $id])
                        ->all();
                $indicador = 'TareaID';
            break;

            case 'areSubCategoria':
                $meses = CronogramaAreSubCategoria::find()
                        ->where(['AreSubCategoriaID'=> $id])
                        ->all();
                $indicador = 'AreSubCategoriaID';
            break;
        }

        if(!empty($meses)){
            foreach ($meses as $mes) {
                $cronograma[] = array(
                    'ID'                    => $mes->ID,
                    $indicador              => $mes->{$indicador},
                    'Mes'                   => $mes->Mes,
                    'MetaFinanciera'        => $mes->MetaFinanciera,
                    'PoafMetaFinanciera'    => $mes->PoafMetaFinanciera,
                    'MetaFisica'            => $mes->MetaFisica,
                    'MesDescripcion'        => $this->DescripcionMes($mes->Mes),
                    'MesActual'             => $this->MesActual($mes->Mes),
                    'MetaFisicaEjecutada'   => $mes->MetaFisicaEjecutada,
                    'MetaAvance'            => $mes->MetaAvance,
                    'SituacionEjecucion'    => $mes->SituacionEjecucion,
                    );
            }
        }else{
            $cronograma = array(
                'ID'                    => '',
                $indicador              => '',
                'Mes'                   => '',
                'MetaFinanciera'        => '',
                'PoafMetaFinanciera'    => '',
                'MetaFisica'            => '',
                'MesDescripcion'        => '',
                'MetaFisicaEjecutada'   => '',
                'MetaAvance'            => '',
                'SituacionEjecucion'    => ''    
            );
        }

        return $cronograma;
    }
    
    public function MesActual($mes=null)
    {
        setlocale (LC_TIME,"spanish");
        //$mes = 01;
        $mes=$mes-1;
        $fechaInicio = '2016-05-01';
        $nuevafecha = strtotime ( '+'.$mes.' month' , strtotime ( $fechaInicio ) ) ;
        $nuevafecha = date ( 'Ym' , $nuevafecha ) ;
        //var_dump($nuevafecha);
        if($nuevafecha<=date('Ym'))
        {
            return 1;
        }
        return 0;
    }
    public function DescripcionMes($mes=null)
    {
        setlocale (LC_TIME,"spanish");
        //$mes = 01;
        $mes=$mes-1;
        $fechaInicio = '2016-05-01';
        $nuevafecha = strtotime ( '+'.$mes.' month' , strtotime ( $fechaInicio ) ) ;
        $nuevafecha = strtoupper(strftime ( '%b %Y' , $nuevafecha ) ) ;
        return $nuevafecha;
    }


    public function actionTareas($id=null){
        $this->layout='vacio';
        $tareas = Tarea::find()
                ->all();
        return $this->render('tareas/_tareas_form',['idactividad'=>$id,'tareas' => $tareas]);

    }

    public function actionTareasCrear(){
        $this->layout='vacio';
        
        $mes = InformacionGeneral::find()
            ->select('Meses')
            ->where(['InvestigadorID' => $this->investigador])
            ->one();
        $model = new Tarea();
        if ( $model->load(Yii::$app->request->post()) ) {
            $model->Correlativo=$this->CorrelativoTarea($model->ActividadID);
            $model->save();
            $porcetanje=0;
            $CountTareas=Tarea::find()->where('ActividadID=:ActividadID',[':ActividadID'=>$model->ActividadID])->count();
            $porcetanje=100/$CountTareas;
            \Yii::$app->db->createCommand("UPDATE Tarea SET Peso=:Peso WHERE ActividadID=:ActividadID")
            ->bindValue(':ActividadID', $model->ActividadID)
            ->bindValue(':Peso', $porcetanje)
            ->execute();
            
            for ($i=1; $i <= $mes->Meses; $i++) { 
                $cronogramaTarea = new CronogramaTarea();
                $cronogramaTarea->TareaID = $model->ID;
                $cronogramaTarea->Mes = $i;
                $cronogramaTarea->MetaFisica=0;
                $cronogramaTarea->save();
            }
            $arr = array(
                    'Success' => true,
                );
            echo json_encode($arr);
        }
    }
    
    public function actionGrabarMetaCronogramaTarea($ID=null,$MetaFisica=null)
    {
        $cronogramaTarea=CronogramaTarea::findOne($ID);
        $cronogramaTarea->MetaFisica=$MetaFisica;
        $cronogramaTarea->save();
        $arr = array('Success' => true,);
        echo json_encode($arr);
    }
    
    
    
    public function actionDescargarTarea($codigo=null)
    {
        $this->layout='estandar';
        $usuario=Usuario::find()->where('username=:username',[':username'=>$codigo])->one();
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
        $componentes=Componente::find()->where('PoaID=:PoaID',[':PoaID'=>$poa->ID])->orderBy('Correlativo asc')->all();
        
        return $this->render('descargar-tarea',['componentes'=>$componentes,'proyecto'=>$proyecto]);
    }
    
    public function actionActualizarTarea($id=null)
    {
        $this->layout='vacio';
        $tarea=Tarea::findOne($id);
        if ($tarea->load(Yii::$app->request->post()) ) {
            $tarea->save();
            CronogramaTarea::updateAll(['MetaFisica' => 0], 'TareaID='.$tarea->ID);
        }
        return $this->render('tareas/actualizar_tareas_form',['tarea' => $tarea]);
    }
    
    public function actionDeleteTarea($id=null)
    {
        $tarea=Tarea::findOne($id);
        CronogramaTarea::deleteAll(['TareaID' =>$tarea->ID]);
        Tarea::deleteAll(['ID' =>$id]);
        
    }
    
    public function CorrelativoTarea($actividadID=null)
    {
        $tarea=Tarea::find()->where('ActividadID=:ActividadID',[':ActividadID'=>$actividadID])->orderBy('Correlativo desc')->one();
        if($tarea)
        {
            return $tarea->Correlativo+1;
        }
        else
        {
            return 1;
        }
    }
    
    public function getObservacionTarea($id=null)
    {
        $observacion=TareaObservacion::find()->where('TareaID=:TareaID and Estado=1',[':TareaID'=>$id])->one();
        if($observacion)
        {
            return $observacion->Observacion;
        }
        
        return "";
    }
    
    
    public function actionGrabarMetaEjecutadaCronogramaTarea($ID=null,$MetaFisicaEjecutada=null)
    {
        if($ID)
        {
            $cronogramaTarea=CronogramaTarea::findOne($ID);
            $cronogramaTarea->MetaFisicaEjecutada=$MetaFisicaEjecutada;
            if($cronogramaTarea->MetaFisica<$MetaFisicaEjecutada)
            {
                $MetaFisicaEjecutada=$cronogramaTarea->MetaFisica;
            }
            $porcentaje_cronograma=($MetaFisicaEjecutada*100)/$cronogramaTarea->MetaFisica;
            $cronogramaTarea->MetaAvance=$porcentaje_cronograma;
            $cronogramaTarea->update();
            
            $cronogramaRecursosAvances=CronogramaTarea::find()->where('MetaFisicaEjecutada!=0 and TareaID=:TareaID',[':TareaID'=>$cronogramaTarea->TareaID])->all();
            $MetaFisicaEjecutadaRecurso=0;
            foreach($cronogramaRecursosAvances as $cronogramaRecursoAvance)
            {
                if($cronogramaRecursoAvance->MetaAvance==100)
                {
                    $MetaFisicaEjecutadaRecurso=$MetaFisicaEjecutadaRecurso+$cronogramaRecursoAvance->MetaFisica;
                }
                else
                {
                    $MetaFisicaEjecutadaRecurso=$MetaFisicaEjecutadaRecurso+$cronogramaRecursoAvance->MetaFisicaEjecutada;
                }
            }
            
            $tarea=Tarea::findOne($cronogramaTarea->TareaID);
            $tarea->MetaFisicaEjecutada=$MetaFisicaEjecutadaRecurso;
            $tarea->MetaAvance=($MetaFisicaEjecutadaRecurso*100)/$tarea->MetaFisica;
            if($tarea->MetaAvance!=0)
            {
                $tarea->ActividadPonderado=($tarea->Peso*$tarea->MetaAvance)/100;
            }
            else
            {
                $tarea->ActividadPonderado=0;
            }
            $tarea->update();
            $actividad=Actividad::findOne($tarea->ActividadID);
            $tareas=Tarea::find()->where('ActividadID=:ActividadID',[':ActividadID'=>$actividad->ID])->all();
            $Avance=0;
            foreach($tareas as $t)
            {
                $Avance=$Avance+$t->ActividadPonderado;
            }
            $actividad->Avance=round($Avance,1);
            $actividad->update();
            
            $componente=Componente::findOne($actividad->ComponenteID);
            $actividades=Actividad::find()->where('ComponenteID=:ComponenteID',[':ComponenteID'=>$componente->ID])->all();
            $AvanceComponente=0;
            foreach($actividades as $activ)
            {
                if($activ->Avance)
                {
                    $AvanceComponente=$AvanceComponente+($activ->Peso*$activ->Avance/100);
                }
            }
            $componente->Avance=$AvanceComponente;
            $componente->update();
            $poa=Poa::findOne($componente->PoaID);
            $proyecto=Proyecto::findOne($poa->ProyectoID);
            $componentes=Componente::find()->where('PoaID=:PoaID',[':PoaID'=>$poa->ID])->all();
            $AvanceProyecto=0;
            foreach($componentes as $comp)
            {
                if($comp->Avance)
                {
                    $AvanceProyecto=$AvanceProyecto+($comp->Peso*$comp->Avance/100);    
                }
            }
            $proyecto->Avance=$AvanceProyecto;
            $proyecto->update();
            
            $arr = array('Success' => true,'TareaMetaAvance'=>$tarea->MetaAvance,'AvanceTareaActividad'=>$tarea->ActividadPonderado,'ActividadAvance'=>$actividad->Avance,'ActividadID'=>$actividad->ID,'ComponenteAvance'=>$componente->Avance,'ComponenteID'=>$componente->ID,'ProyectoAvance'=>$proyecto->Avance);
            echo json_encode($arr);
        }
    }
    
    public function actionGrabarMetaEjecutadaCronogramaActividad($ID=null,$MetaFisicaEjecutada=null)
    {
        if($ID)
        {
            $cronogramaActividad=CronogramaActividad::findOne($ID);
            $cronogramaActividad->MetaFisicaEjecutada=$MetaFisicaEjecutada;
            if($cronogramaActividad->MetaFisica<$MetaFisicaEjecutada)
            {
                $MetaFisicaEjecutada=$cronogramaActividad->MetaFisica;
            }
            $porcentaje_cronograma=($MetaFisicaEjecutada*100)/$cronogramaActividad->MetaFisica;
            $cronogramaActividad->MetaAvanceActividad=$porcentaje_cronograma;
            $cronogramaActividad->update();
            /*
            $cronogramaRecursosAvances=CronogramaTarea::find()->where('MetaFisicaEjecutada!=0 and TareaID=:TareaID',[':TareaID'=>$cronogramaTarea->TareaID])->all();
            $MetaFisicaEjecutadaRecurso=0;
            foreach($cronogramaRecursosAvances as $cronogramaRecursoAvance)
            {
                if($cronogramaRecursoAvance->MetaAvance==100)
                {
                    $MetaFisicaEjecutadaRecurso=$MetaFisicaEjecutadaRecurso+$cronogramaRecursoAvance->MetaFisica;
                }
                else
                {
                    $MetaFisicaEjecutadaRecurso=$MetaFisicaEjecutadaRecurso+$cronogramaRecursoAvance->MetaFisicaEjecutada;
                }
            }
            
            $tarea=Tarea::findOne($cronogramaTarea->TareaID);
            $tarea->MetaFisicaEjecutada=$MetaFisicaEjecutadaRecurso;
            $tarea->MetaAvance=($MetaFisicaEjecutadaRecurso*100)/$tarea->MetaFisica;
            if($tarea->MetaAvance!=0)
            {
                $tarea->ActividadPonderado=($tarea->Peso*$tarea->MetaAvance)/100;
            }
            else
            {
                $tarea->ActividadPonderado=0;
            }
            $tarea->update();
            $actividad=Actividad::findOne($tarea->ActividadID);
            $tareas=Tarea::find()->where('ActividadID=:ActividadID',[':ActividadID'=>$actividad->ID])->all();
            $Avance=0;
            foreach($tareas as $t)
            {
                $Avance=$Avance+$t->ActividadPonderado;
            }
            $actividad->Avance=round($Avance,1);
            $actividad->update();
            
            $componente=Componente::findOne($actividad->ComponenteID);
            $actividades=Actividad::find()->where('ComponenteID=:ComponenteID',[':ComponenteID'=>$componente->ID])->all();
            $AvanceComponente=0;
            foreach($actividades as $activ)
            {
                if($activ->Avance)
                {
                    $AvanceComponente=$AvanceComponente+($activ->Peso*$activ->Avance/100);
                }
            }
            $componente->Avance=$AvanceComponente;
            $componente->update();
            $poa=Poa::findOne($componente->PoaID);
            $proyecto=Proyecto::findOne($poa->ProyectoID);
            $componentes=Componente::find()->where('PoaID=:PoaID',[':PoaID'=>$poa->ID])->all();
            $AvanceProyecto=0;
            foreach($componentes as $comp)
            {
                if($comp->Avance)
                {
                    $AvanceProyecto=$AvanceProyecto+($comp->Peso*$comp->Avance/100);    
                }
            }
            $proyecto->Avance=$AvanceProyecto;
            $proyecto->update();
            
            $arr = array('Success' => true,'TareaMetaAvance'=>$tarea->MetaAvance,'AvanceTareaActividad'=>$tarea->ActividadPonderado,'ActividadAvance'=>$actividad->Avance,'ActividadID'=>$actividad->ID,'ComponenteAvance'=>$componente->Avance,'ComponenteID'=>$componente->ID,'ProyectoAvance'=>$proyecto->Avance);
            echo json_encode($arr);
            */
            $arr = array('Success' => true,);
            echo json_encode($arr);
        }
    }
    
    public function actionProcesarEjecucionTareas($codigo=null)
    {
        if($codigo)
        {
            $usuario=Usuario::find()->where('username=:username',[':username'=>$codigo])->one();
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $pat=Pat::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
            $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
            $updateTarea = Yii::$app->db->createCommand(' UPDATE CronogramaTarea SET SituacionEjecucion=1
                                                        FROM CronogramaTarea
                                                        inner join Tarea on Tarea.ID=CronogramaTarea.TareaID
                                                        inner join Actividad on Actividad.ID=Tarea.ActividadID
                                                        inner join Componente on Componente.ID=Actividad.ComponenteID
                                                        inner join Poa on Poa.ID=Componente.PoaID
                                                        where CronogramaTarea.MetaFisicaEjecutada>0 and Poa.ID='.$poa->ID);
            $updateTarea->execute();
            $arr = array('Success' => true,);
            echo json_encode($arr);
        }
    }
    
    public function actionJustificacionTarea($CronogramaTareaID=null)
    {
        $this->layout='vacio';
        $model=JustificacionTarea::find()->where('CronogramaTareaID=:CronogramaTareaID',[':CronogramaTareaID'=>$CronogramaTareaID])->one();
        if(!$model)
        {
            $model=new JustificacionTarea;    
        }
        
        if ($model->load(Yii::$app->request->post()) ) {
            $model->CronogramaTareaID=$CronogramaTareaID;
            $model->FechaRegistro = date('Ymd h:m:s');
            $model->Estado=1;
            $model->Situacion=2;
            $model->save();
            
            $model->Archivo = UploadedFile::getInstance($model, 'Archivo');
            if($model->Archivo)
            {
                $model->Archivo->saveAs('justificaciontarea/' . $model->ID . '.' . $model->Archivo->extension);
                $model->NombreArchivo=$model->Archivo->name;
                $model->Evidencia=$model->ID . '.' . $model->Archivo->extension;
            }
            $model->update();
            return $this->redirect(['/recursos-tarea-ejecutado']);
        }
        
        
        return $this->render('_form_justificacion',['model'=>$model,'CronogramaTareaID'=>$CronogramaTareaID]);
    }
    
    
    public function actionObservarTarea($CronogramaTareaID=null,$CodigoProyecto=null,$InvestigadorID=null)
    {
        
        $this->layout='vacio';
        $model=ObservacionGeneral::find()->where('TipoObservacion=1 and CodigoID=:CodigoID and CodigoProyecto=:CodigoProyecto',[':CodigoID'=>$CronogramaTareaID,':CodigoProyecto'=>$CodigoProyecto])->one();
        $CronogramaTarea=CronogramaTarea::findOne($CronogramaTareaID);
        if(!$model)
        {
            $model=new ObservacionGeneral;    
        }
        
        if ($model->load(Yii::$app->request->post()) ) {
            $model->CodigoProyecto=$CodigoProyecto;
            $model->TipoObservacion=1;
            $model->CodigoID=$CronogramaTareaID;
            //$model->FechaRegistro = date('Ymd h:m:s');
            $model->Estado=1;
            $model->save();
            
            $CronogramaTarea->SituacionEjecucion=4;
            $CronogramaTarea->update();
            
            return $this->redirect(['evaluacion-proyecto/evaluacion-proyecto?investigadorID='.$InvestigadorID.'']);
        }
        
        
        return $this->render('_form_observacion',['model'=>$model,'CronogramaTareaID'=>$CronogramaTareaID,'CodigoProyecto'=>$CodigoProyecto,'InvestigadorID'=>$InvestigadorID]);
    }
    
    
    public function actionJustificacionReprogramacionTarea($CronogramaTareaID=null)
    {
        $this->layout='vacio';
        $model=JustificacionTarea::find()->where('CronogramaTareaID=:CronogramaTareaID',[':CronogramaTareaID'=>$CronogramaTareaID])->one();
        if(!$model)
        {
            $model=new JustificacionTarea;    
        }
        
        if ($model->load(Yii::$app->request->post()) ) {
            $model->CronogramaTareaID=$CronogramaTareaID;
            $model->FechaRegistro = date('Ymd h:m:s');
            $model->Estado=1;
            $model->Situacion=2;
            $model->save();
            
            $model->Archivo = UploadedFile::getInstance($model, 'Archivo');
            if($model->Archivo)
            {
                $model->Archivo->saveAs('justificaciontarea/' . $model->ID . '.' . $model->Archivo->extension);
                $model->NombreArchivo=$model->Archivo->name;
                $model->Evidencia=$model->ID . '.' . $model->Archivo->extension;
            }
            $model->update();
            return $this->redirect(['/recursos-tarea-ejecutado']);
        }
        
        
        return $this->render('_form_reprogramacion_justificacion',['model'=>$model,'CronogramaTareaID'=>$CronogramaTareaID]);
    }
    
    public function actionConfirmarTarea()
    {
        if(isset($_POST['CronogramaTareaID']) && $_POST['CronogramaTareaID']!='')
        {
            $CronogramaTareaID=$_POST['CronogramaTareaID'];
            $CronogramaTarea=CronogramaTarea::findOne($CronogramaTareaID);
            $CronogramaTarea->SituacionEjecucion=3;
            $CronogramaTarea->update();
            $arr = array('Success' => true,);
            echo json_encode($arr);
        }
    }
    
    public function actionHabilitarTarea()
    {
        if(isset($_POST['CronogramaTareaID']) && $_POST['CronogramaTareaID']!='')
        {
            $CronogramaTareaID=$_POST['CronogramaTareaID'];
            $CronogramaTarea=CronogramaTarea::findOne($CronogramaTareaID);
            $CronogramaTarea->SituacionEjecucion=0;
            $CronogramaTarea->update();
            $arr = array('Success' => true,);
            echo json_encode($arr);
        }
    }
    
}
