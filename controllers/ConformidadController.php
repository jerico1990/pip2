<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\PasoCritico;
use app\models\Usuario;
use app\models\Persona;
use app\models\Investigador;
use app\models\Proyecto;
use app\models\InformacionGeneral;
use app\models\Poa;
use app\models\Componente;
use app\models\Actividad;
use app\models\CronogramaProyecto;
use app\models\ActRubroElegible;
use app\models\CronogramaComponente;
use app\models\CronogramaActividad;
use app\models\CronogramaActRubroElegible;
use app\models\RubroElegible;
use app\models\AreSubCategoria;
use app\models\CronogramaAreSubCategoria;
use app\models\MarcoLogicoActividad;
use app\models\AreSubCategoriaObservacion;
use app\models\SolicitudCompromisoPresupuestal;
use app\models\DetalleSolicitudCompromisoPresupuestal;
use app\models\Orden;
use app\models\Conformidad;

use yii\web\UploadedFile;


class ConformidadController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($CodigoProyecto=NULL)
    {
        $this->layout='estandar';
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        return $this->render('index',['CodigoProyecto'=>$CodigoProyecto]);
    }
    
    public function actionListaConformidadesServicios($CodigoProyecto=null)
    {
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        
        
        $resultados = (new \yii\db\Query())
            ->select('Conformidad.*,Orden.Correlativo')
            ->from('InformacionGeneral')
            ->innerJoin('Investigador','InformacionGeneral.InvestigadorID=Investigador.ID')
            ->innerJoin('Usuario','Usuario.ID=Investigador.UsuarioID')
            ->innerJoin('Requerimiento','Requerimiento.Codigo=InformacionGeneral.Codigo')
            ->innerJoin('TerminoReferencia','TerminoReferencia.RequerimientoID=Requerimiento.ID')
            ->innerJoin('Orden','Orden.TerminoReferenciaID=TerminoReferencia.ID')
            ->innerJoin('Conformidad','Conformidad.OrdenID=Orden.ID')
            ->where(['InformacionGeneral.Codigo'=>$CodigoProyecto])
            ->distinct()
            ->all();

        $nro=0;
        foreach($resultados as $result)
        {
            $nro++;
            echo "<tr>";
            echo "<td>" . $result["Correlativo"] . "</td>";
            echo "<td>" . mb_substr ($result["FechaRegistro"],0,100) . "</td>";
            echo "<td>" . $result["MontoTotal"] . "</td>";
            echo "<td><a target='_blank' href='compromisopresupuestal/" . $result["Documento"] . "'><span>descargar</span></a></td>";
            echo "<td></td>";
            echo "</tr>";
        }
    }
    
    public function actionCrear($CodigoProyecto)
    {
        $this->layout='vacio';
        $informacionGeneral=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacionGeneral->InvestigadorID])->one();
        $ordenes=Orden::find()
                ->innerJoin('TerminoReferencia','TerminoReferencia.ID=Orden.TerminoReferenciaID')
                ->innerJoin('Requerimiento','Requerimiento.ID=TerminoReferencia.RequerimientoID')
                ->where(['Requerimiento.Codigo'=>$CodigoProyecto])
                ->all();
        $conformidad=new Conformidad;
        if($conformidad->load(Yii::$app->request->post())){
            $conformidad->FechaRegistro=date('Ymd');
            $conformidad->Estado=1;
            $conformidad->save();
            
            $conformidad->archivo = UploadedFile::getInstance($conformidad, 'archivo');
            
            if($conformidad->archivo)
            {
                $conformidad->archivo->saveAs('conformidad/' . $conformidad->ID . '.' . $conformidad->archivo->extension);
                $conformidad->Documento=$conformidad->ID . '.' . $conformidad->archivo->extension;
            }
            $conformidad->update();
            return $this->redirect(['/compromiso-presupuestal']);
            
        }
        return $this->render('_form',['CodigoProyecto'=>$CodigoProyecto,'ordenes'=>$ordenes]);
    }
    
}
