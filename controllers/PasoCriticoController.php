<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\PasoCritico;
use app\models\Usuario;
use app\models\Persona;
use app\models\Investigador;
use app\models\Proyecto;
use app\models\InformacionGeneral;
use app\models\Poa;
use app\models\Componente;
use app\models\Actividad;
use app\models\CronogramaProyecto;
use app\models\ActRubroElegible;
use app\models\CronogramaComponente;
use app\models\CronogramaActividad;
use app\models\CronogramaActRubroElegible;
use app\models\RubroElegible;
use app\models\AreSubCategoria;
use app\models\CronogramaAreSubCategoria;
use app\models\MarcoLogicoActividad;
use app\models\AreSubCategoriaObservacion;


class PasoCriticoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($investigadorID=NULL)
    {
        $this->layout='estandar';
        if($investigadorID==NULL)
        {
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        }
        else
        {
            $investigador=Investigador::findOne($investigadorID);
        }
        
        if(!$investigadorID && \Yii::$app->user->identity->rol!=7)
        {
            return $this->redirect(['panel/index']) ;
        }
            
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        
        return $this->render('index',['proyecto'=>$proyecto]);
    }
    public function actionListado($CodigoProyecto=null)
    {
        $this->layout='vacio';
        $informacion=InformacionGeneral::find()->where('Codigo=:CodigoProyecto',[':CodigoProyecto'=>$CodigoProyecto])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacion->InvestigadorID])->one();
        return $this->render('listado',['proyecto'=>$proyecto]);
    }
    
    public function actionListaPasoCritico($ProyectoID=null)
    {
        $resultados = (new \yii\db\Query())
            ->select(['Investigador.ID','InformacionGeneral.Codigo','Usuario.username as username','InformacionGeneral.TituloProyecto','InformacionGeneral.Meses','PasoCritico.Estado','PasoCritico.Situacion','PasoCritico.MesInicio','PasoCritico.MesFin','PasoCritico.Correlativo','PasoCritico.ID PasoCriticoID'])
            ->from('InformacionGeneral')
            ->innerJoin('Investigador','InformacionGeneral.InvestigadorID=Investigador.ID')
            ->innerJoin('Usuario','Usuario.ID=Investigador.UsuarioID')
            ->innerJoin('Proyecto','Proyecto.InvestigadorID=Investigador.ID')
            ->innerJoin('PasoCritico','PasoCritico.ProyectoID=Proyecto.ID')
            ->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$ProyectoID])
            ->orderBy('PasoCritico.Correlativo asc') 
            ->distinct()
            ->all();
        $nro=0;
        foreach($resultados as $result)
        {
            $nro++;
            echo "<tr>";
            // echo "<td>" . $result["username"] . "</td>";
	    echo "<td>" . mb_substr ($result["TituloProyecto"],0,100) . "</td>";
            echo "<td> Paso Crítico N° " . $result["Correlativo"] . "</td>";
            echo "<td>" . $this->DescripcionMes($result["MesInicio"]) . "</td>";
            echo "<td>" . $this->DescripcionMes($result["MesFin"]) . "</td>";
            echo "<td>" . $this->MontoPasoCritico($result['Codigo'],$result["MesInicio"],$result["MesFin"]) . "</td>";
            //echo "<td>" . $result["Total"] . "</td>";
            echo "<td><a class='btn btn-primary' href='paso-critico/ver?ID=".$result["PasoCriticoID"]."'>revisar</a></td>";
            echo "</tr>";
        }
    }
    
    public function actionListaPasoCriticoUafsi($ProyectoID=null)
    {
        $resultados = (new \yii\db\Query())
            ->select(['Investigador.ID','InformacionGeneral.Codigo','Usuario.username as username','InformacionGeneral.TituloProyecto','InformacionGeneral.Meses','PasoCritico.Estado','PasoCritico.Situacion','PasoCritico.MesInicio','PasoCritico.MesFin','PasoCritico.Correlativo','PasoCritico.ID PasoCriticoID'])
            ->from('InformacionGeneral')
            ->innerJoin('Investigador','InformacionGeneral.InvestigadorID=Investigador.ID')
            ->innerJoin('Usuario','Usuario.ID=Investigador.UsuarioID')
            ->innerJoin('Proyecto','Proyecto.InvestigadorID=Investigador.ID')
            ->innerJoin('PasoCritico','PasoCritico.ProyectoID=Proyecto.ID')
            ->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$ProyectoID])
            ->orderBy('PasoCritico.Correlativo asc') 
            ->distinct()
            ->all();
        $nro=0;
        foreach($resultados as $result)
        {
            $nro++;
            echo "<tr>";
            // echo "<td>" . $result["username"] . "</td>";
	    echo "<td>" . mb_substr ($result["TituloProyecto"],0,60) . "</td>";
            echo "<td> Paso Crítico N° " . $result["Correlativo"] . "</td>";
            echo "<td>" . $this->DescripcionMes($result["MesInicio"]) . "</td>";
            echo "<td>" . $this->DescripcionMes($result["MesFin"]) . "</td>";
            echo "<td>" . $this->MontoPasoCritico($result['Codigo'],$result["MesInicio"],$result["MesFin"]) . "</td>";
            echo "<td>" . $this->Situacion($result["Situacion"]) . "</td>";
            echo "<td><a data-paso-critico-id='".$result["PasoCriticoID"]."' class='btn btn-primary btn-actualizar-paso-critico' href='#'>Actualizar</a></td>";
            echo "<td><a data-paso-critico-id='".$result["PasoCriticoID"]."' class='btn btn-primary btn-habilitar-paso-critico' href='#'>Habilitar</a></td>";
            echo "</tr>";
        }
    }
    
    public function Situacion($Situacion=null)
    {
        if($Situacion==0)
        {
            return "";
        }
        elseif($Situacion==2){
            return "Habilitado";
        }
    }
    
    public function MontoPasoCritico($CodigoProyecto,$MesInicio,$MesFin)
    {
        $resul = (new \yii\db\Query())
            ->select('SUM(CronogramaAreSubCategoria.MetaFisica*CronogramaAreSubCategoria.CostoUnitario) Total')
            ->from('InformacionGeneral')
            ->innerJoin('Proyecto','Proyecto.InvestigadorID=InformacionGeneral.InvestigadorID')
            ->innerJoin('Poa','Poa.ProyectoID=Proyecto.ID')
            ->innerJoin('Componente','Componente.PoaID=Poa.ID')
            ->innerJoin('Actividad','Actividad.ComponenteID=Componente.ID')
            ->innerJoin('ActRubroElegible','ActRubroElegible.ActividadID=Actividad.ID')
            ->innerJoin('AreSubCategoria','AreSubCategoria.ActRubroElegibleID=ActRubroElegible.ID')
            ->innerJoin('CronogramaAreSubCategoria','CronogramaAreSubCategoria.AreSubCategoriaID=AreSubCategoria.ID')
            ->where(['InformacionGeneral.Codigo'=>$CodigoProyecto,'Componente.Estado'=>1,'Actividad.Estado'=>1])
            ->andWhere(['between', 'CronogramaAreSubCategoria.Mes', $MesInicio, $MesFin])
            ->one();
        
        return number_format($resul['Total'], 2, '.', ' ');
    }
    
    public function actionHabilitarPasoCritico($PasoCriticoID)
    {
        $this->layout='vacio';
        $model=PasoCritico::findOne($PasoCriticoID);
        $PasosCriticos=PasoCritico::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$model->ProyectoID])->all();
        foreach($PasosCriticos as $PasoCritico)
        {
            $PasoCritico->Situacion=0;
            $PasoCritico->update();
        }
        $model->Situacion=2;
        $model->update();
        
        $arr = array(
            'Success' => true,
        );
        echo json_encode($arr);
        
    }
    
    public function actionCrear($ProyectoID)
    {
        $this->layout='vacio';
        $model=new PasoCritico;
        if ($model->load(Yii::$app->request->post())) {
            $model->Situacion=2;
            $model->Estado=1;
            $model->Correlativo=$this->CorrelativoDesembolso($model->ProyectoID);
            $model->save();
            $arr = array(
                'Success' => true,
            );
            echo json_encode($arr);
        }
        return $this->render('_desembolsos_form',['ProyectoID'=>$ProyectoID]);
    }
    
    public function actionActualizar($PasoCriticoID=null)
    {
        $this->layout='vacio';
        $model=PasoCritico::findOne($PasoCriticoID);
        if ($model->load(Yii::$app->request->post())) {
            Yii::$app->tools->logData($model->ID,'update','PasoCritico','');
            $model->save();
            $arr = array(
                'Success' => true,
            );
            echo json_encode($arr);
            die;
        }
        return $this->render('_form_actualizar',['model'=>$model]);
    }
    
    public function CorrelativoDesembolso($ProyectoID)
    {
        $desembolso=PasoCritico::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$ProyectoID])->orderBy('Correlativo desc')->one();
        if($desembolso)
        {
            return $desembolso->Correlativo+1;
        }
        else
        {
            return 1;
        }
    }
    
    public function actionVer($ID=null)
    {
        $this->layout='estandar';
        $pasocritico=PasoCritico::findOne($ID);
        $proyecto=Proyecto::findOne($pasocritico->ProyectoID);
        $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$proyecto->InvestigadorID])->one();
        return $this->render('ver',['informacionGeneral'=>$informacionGeneral,'pasocritico'=>$pasocritico]);
    }
    
    
    public function actionRecursosJson($codigo=null,$mesInicio=null,$mesFin=null){
        $this->layout='vacio';
        // Proyectos
        $usuario=Usuario::find()->where('username=:username',[':username'=>$codigo])->one();
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        
        $proyecto = Poa::find()
            ->select('Poa.ID, Poa.ProyectoID')
            ->innerJoin('Proyecto','Poa.ProyectoID = Proyecto.ID')
            ->where('Proyecto.InvestigadorID=:InvestigadorID and Poa.Estado=:Estado',
                    [':InvestigadorID'=> $investigador->ID,':Estado' => 1])
            ->one();
        
        $proyectox = array(
            'ID'             => $proyecto->ProyectoID, 
            'Componentes'    => $this->get_objetivo($proyecto->ID,$mesInicio,$mesFin),
            'Cronogramas'    => $this->getCronogramaProyecto($proyecto->ProyectoID,'proyecto',$mesInicio,$mesFin)
        );

        echo json_encode($proyectox); 
    }
    
    private function getCronogramaProyecto($id,$tipo = '',$mesInicio,$mesFin){
        $this->layout='vacio';
        $meses = array();
        $cronograma = array();
        switch ($tipo) {
            case 'proyecto':
                $meses = CronogramaProyecto::find()
                        ->where(['ProyectoID'=> $id])
                        ->andWhere(['between', 'Mes', $mesInicio, $mesFin])
                        ->all();
                $indicador = 'ProyectoID';
            break;
            case 'componente':
                $meses = CronogramaComponente::find()
                        ->where(['ComponenteID'=> $id])
                        ->andWhere(['between', 'Mes', $mesInicio, $mesFin])
                        ->all();
                $indicador = 'ComponenteID';
            break;

            case 'actividad':
                $meses = CronogramaActividad::find()
                        ->where(['ActividadID'=> $id])
                        ->andWhere(['between', 'Mes', $mesInicio, $mesFin])
                        ->all();
                $indicador = 'ActividadID';
            break;

            case 'rubroElegible':
                $meses = CronogramaActRubroElegible::find()
                        ->where(['ActRubroElegibleID'=> $id])
                        ->andWhere(['between', 'Mes', $mesInicio, $mesFin])
                        ->all();
                $indicador = 'ActRubroElegibleID';
            break;

            case 'areSubCategoria':
                $meses = CronogramaAreSubCategoria::find()
                        ->where('AreSubCategoriaID=:AreSubCategoriaID',[':AreSubCategoriaID'=> $id])
                        ->andWhere(['between', 'Mes', $mesInicio, $mesFin])
                        ->all();
                $indicador = 'AreSubCategoriaID';
            break;
        }

        if(!empty($meses)){
            foreach ($meses as $mes) {
                $cronograma[] = array(
                    'ID'                    => $mes->ID,
                    $indicador              => $mes->{$indicador},
                    'Mes'                   => $mes->Mes,
                    'MetaFisica'            => $mes->MetaFisica,
                    'MetaFinanciera'        => $mes->MetaFinanciera,
                    'PoafMetaFinanciera'    => $mes->PoafMetaFinanciera,
                    'MesDescripcion'        => $this->DescripcionMes($mes->Mes),
                    );
            }
        }else{
            $cronograma = array(
                'ID'                    => '',
                $indicador              => '',
                'Mes'                   => '',
                'MetaFisica'            => '',
                'MetaFinanciera'        => '',
                'PoafMetaFinanciera'    => '',
                'MesDescripcion'        =>  '',
            );
        }

        return $cronograma;
    }
    
    
    private function get_objetivo($idPoa,$mesInicio,$mesFin){
        $jsonComponente = array();
        $component = Componente::find()
                ->select('Componente.ID,Componente.Nombre,Componente.Correlativo,sum(CronogramaAreSubCategoria.MetaFisica*CronogramaAreSubCategoria.CostoUnitario) TotalObjetivo')
                ->innerJoin('Actividad','Actividad.ComponenteID=Componente.ID')
                ->innerJoin('ActRubroElegible','ActRubroElegible.ActividadID=Actividad.ID')
                ->innerJoin('AreSubCategoria','AreSubCategoria.ActRubroElegibleID=ActRubroElegible.ID')
                ->innerJoin('CronogramaAreSubCategoria','CronogramaAreSubCategoria.AreSubCategoriaID=AreSubCategoria.ID')
                ->where('Componente.Estado=:Estado and Actividad.Estado=:AEstado and Componente.PoaID=:PoaID and CronogramaAreSubCategoria.MetaFisica!=0',[':Estado' => 1,':AEstado'=>1, ':PoaID' => $idPoa])
                ->andWhere(['between', 'CronogramaAreSubCategoria.Mes', $mesInicio, $mesFin])
                ->groupBy('Componente.ID,Componente.Nombre,Componente.Correlativo,ActRubroElegible.RubroElegibleID')
                ->orderBy('Componente.Correlativo asc')
                ->all();
        if(!empty($component) ){
            foreach ($component as $comp) {
                $jsonComponente[] = array(
                    'ID'            => $comp->ID, 
                    'Nombre'        => $comp->Nombre,
                    'Correlativo'   => $comp->Correlativo, 
                    'Actividades'   => $this->get_actividades($comp->ID,$mesInicio,$mesFin),
                    'TotalObjetivo' => $comp->TotalObjetivo,
                    'Cronogramas'   => $this->getCronogramaProyecto($comp->ID,'componente',$mesInicio,$mesFin)
                );

            }
        }
        return $jsonComponente;
    }

    private function get_actividades($idComponente,$mesInicio,$mesFin){
        $jsonActividades = array();
        $actv = Actividad::find()
                ->select('Actividad.ID,Actividad.ComponenteID,Actividad.Nombre,Actividad.CostoUnitario,Actividad.Correlativo')
                ->innerJoin('ActRubroElegible','ActRubroElegible.ActividadID=Actividad.ID')
                ->innerJoin('AreSubCategoria','AreSubCategoria.ActRubroElegibleID=ActRubroElegible.ID')
                ->innerJoin('CronogramaAreSubCategoria','CronogramaAreSubCategoria.AreSubCategoriaID=AreSubCategoria.ID')
                ->where('Actividad.Estado=:Estado and Actividad.ComponenteID=:ComponenteID and CronogramaAreSubCategoria.MetaFisica!=0',[':Estado' => 1, ':ComponenteID' => $idComponente])
                ->andWhere(['between', 'CronogramaAreSubCategoria.Mes', $mesInicio, $mesFin])
                ->groupBy('Actividad.ID,Actividad.Correlativo,Actividad.ComponenteID,Actividad.Nombre,Actividad.CostoUnitario')
                ->orderBy('Actividad.Correlativo asc')
                
                ->all();
        if(!empty($actv) ){
            foreach ($actv as $actvd) {
                $jsonActividades[] = array(
                    'ID'            => $actvd->ID, 
                    'ComponenteID'  => $actvd->ComponenteID,
                    'Correlativo'   => $actvd->Correlativo, 
                    'Nombre'        => $actvd->Nombre, 
                    'UnidadMedida'  => $this->get_actividadesmarco($actvd->ID)->IndicadorUnidadMedida, 
                    'CostoUnitario' => $actvd->CostoUnitario, 
                    'MetaFisica'    => $this->get_actividadesmarco($actvd->ID)->IndicadorMetaFisica,
                    'ActRubroElegibles' => $this->get_actRubroElegible($actvd->ID,$mesInicio,$mesFin),
                    'Cronogramas'       => $this->getCronogramaProyecto($actvd->ID,'actividad',$mesInicio,$mesFin)
                );
            }
        }
        return $jsonActividades;
    }

    private function get_actRubroElegible($idActividad,$mesInicio,$mesFin){
        $jsonActividades = array();
        $actv = ActRubroElegible::find()
                ->select('ActRubroElegible.ID,ActRubroElegible.ActividadID,ActRubroElegible.RubroElegibleID,ActRubroElegible.UnidadMedida,ActRubroElegible.CostoUnitario,ActRubroElegible.MetaFisica')
                ->innerJoin('AreSubCategoria','AreSubCategoria.ActRubroElegibleID=ActRubroElegible.ID')
                ->innerJoin('CronogramaAreSubCategoria','CronogramaAreSubCategoria.AreSubCategoriaID=AreSubCategoria.ID')
                ->where('ActividadID=:ActividadID and CronogramaAreSubCategoria.MetaFisica!=0',[':ActividadID' => $idActividad])
                ->andWhere(['between', 'CronogramaAreSubCategoria.Mes', $mesInicio, $mesFin])
                ->groupBy('ActRubroElegible.ID,ActRubroElegible.ActividadID,ActRubroElegible.RubroElegibleID,ActRubroElegible.UnidadMedida,ActRubroElegible.CostoUnitario,ActRubroElegible.MetaFisica')
                ->all();
        if(!empty($actv) ){
            foreach ($actv as $actvd) {
                $jsonActividades[] = array(
                    'ID'                => $actvd->ID, 
                    'ActividadID'       => $actvd->ActividadID, 
                    'RubroElegibleID'   => $actvd->RubroElegibleID, 
                    'UnidadMedida'      => $actvd->UnidadMedida, 
                    'CostoUnitario'     => $actvd->CostoUnitario, 
                    'MetaFisica'        => $actvd->MetaFisica, 
                    'AreSubCategorias'  => $this->get_areSubCategoria($actvd->ID,$mesInicio,$mesFin,$actvd->RubroElegibleID),
                    'RubroElegible'     => $this->get_rubroElegible($actvd->RubroElegibleID),
                    'Cronogramas'       => $this->getCronogramaProyecto($actvd->ID,'rubroElegible',$mesInicio,$mesFin)
                );
            }
        }
        return $jsonActividades;
    }

    private function get_rubroElegible($idRubro){
        $jsonActividades = array();
            $actv = RubroElegible::find()
                    ->where(['ID' => $idRubro])
                    ->all();

        if(!empty($actv) ){
            foreach ($actv as $actvd) {
                $jsonActividades = array(
                    'TipoRubroElegibleID'=> $actvd->TipoRubroElegibleID, 
                    'ID'                 => $actvd->ID, 
                    'Nombre'             => $actvd->Nombre
                );
            }
        }
        return $jsonActividades;
    }

    private function get_areSubCategoria($idActRubroElegible,$mesInicio,$mesFin,$RubroElegibleID){
        $jsonActividades = array();
        $actv = AreSubCategoria::find()
                ->select('AreSubCategoria.ID,ActRubroElegibleID,Nombre,UnidadMedida,AreSubCategoria.CostoUnitario,AreSubCategoria.MetaFisica,Total,TotalConMetaFisica,AreSubCategoria.Situacion,sum(CronogramaAreSubCategoria.MetaFisica) CronogramaAreSubCategoriaMetaFisica,AreSubCategoria.Codificacion')
                ->innerJoin('CronogramaAreSubCategoria','CronogramaAreSubCategoria.AreSubCategoriaID=AreSubCategoria.ID')
                ->where('ActRubroElegibleID=:ActRubroElegibleID and CronogramaAreSubCategoria.MetaFisica!=0',[':ActRubroElegibleID' => $idActRubroElegible])
                ->andWhere(['between', 'CronogramaAreSubCategoria.Mes', $mesInicio, $mesFin])
                ->groupBy('AreSubCategoria.ID,ActRubroElegibleID,Nombre,UnidadMedida,AreSubCategoria.CostoUnitario,AreSubCategoria.MetaFisica,Total,TotalConMetaFisica,AreSubCategoria.Situacion,AreSubCategoria.Codificacion')
                ->all();
        if(!empty($actv) ){
            foreach ($actv as $actvd) {
                $jsonActividades[] = array(
                    'ID'                    => $actvd->ID, 
                    'ActRubroElegibleID'    => $actvd->ActRubroElegibleID, 
                    'Nombre'                => $actvd->Nombre, 
                    'UnidadMedida'          => $actvd->UnidadMedida, 
                    'CostoUnitario'         => $actvd->CostoUnitario, 
                    'Matriz'                => $RubroElegibleID.'.'.$actvd->Codificacion, 
                    'MetaFisica'            => $actvd->CronogramaAreSubCategoriaMetaFisica, 
                    'Total'                 => $actvd->Total, 
                    'TotalConMetaFisica'    => $actvd->TotalConMetaFisica,
                    'ObservacionID'         => $this->getObservacionRecursoID($actvd->ID),
                    'Observacion'           => $this->getObservacionRecurso($actvd->ID),
                    'ObservacionSituacionID'=> $this->getObservacionSituacionID($actvd->ID),
                    'Situacion'             => $actvd->Situacion,
                    'Cronogramas'           => $this->getCronogramaProyecto($actvd->ID,'areSubCategoria',$mesInicio,$mesFin)
                );
            }
        }
        return $jsonActividades;
    }
    
    private function get_actividadesmarco($idActividad){
        $jsonActividadesMarco = array();
        $actvmarco = MarcoLogicoActividad::find()
                ->where(['Estado' => 1, 'ActividadID' => $idActividad])
                ->one();
        if(!$actvmarco)
        {
            $actvmarco=new MarcoLogicoActividad;
        }
        return $actvmarco;
    }
    
    public function getObservacionSituacionID($id)
    {
        $observacion=AreSubCategoriaObservacion::find()->where('AreSubCategoriaID=:AreSubCategoriaID and Estado=3',[':AreSubCategoriaID'=>$id])->one();
        if($observacion)
        {
            return $observacion->Estado;
        }
        
        return "";
    }
    
    public function getObservacionRecursoID($id)
    {
        $observacion=AreSubCategoriaObservacion::find()->where('AreSubCategoriaID=:AreSubCategoriaID and Estado=1',[':AreSubCategoriaID'=>$id])->one();
        if($observacion)
        {
            return $observacion->ID;
        }
        
        return "";
    }
    
    public function getObservacionRecurso($id)
    {
        $observacion=AreSubCategoriaObservacion::find()->where('AreSubCategoriaID=:AreSubCategoriaID and Estado=1',[':AreSubCategoriaID'=>$id])->one();
        if($observacion)
        {
            return $observacion->Observacion;
        }
        
        return "";
    }
    
    public function DescripcionMes($mes=null)
    {
        setlocale (LC_TIME,"spanish");
        //$mes = 01;
        $mes=$mes-1;
        $fechaInicio = '2016-05-01';
        $nuevafecha = strtotime ( '+'.$mes.' month' , strtotime ( $fechaInicio ) ) ;
        $nuevafecha = strtoupper(strftime ( '%b %Y' , $nuevafecha ) ) ;
        return $nuevafecha;
    }
    
    
    public function actionGenerarDesembolso()
    {
        $proyectos=Proyecto::find()->all();
        foreach($proyectos as $proyecto)
        {
            $desembolso=new PasoCritico;
            $desembolso->ProyectoID=$proyecto->ID;
            $desembolso->MesInicio=28;
            $desembolso->MesFin=32;
            $desembolso->Correlativo=6;
            $desembolso->Situacion=1;
            $desembolso->Estado=1;
            $desembolso->save();
        }
    }
    
    
    public function actionDescargarPoa($PasoCriticoID=null)
    {
        $this->layout='estandar';
        $PasoCritico=PasoCritico::findOne($PasoCriticoID);
        $proyecto=Proyecto::find()->where('ID=:ID',[':ID'=>$PasoCritico->ProyectoID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
        //$componentes=Componente::find()->where('PoaID=:PoaID',[':PoaID'=>$poa->ID])->orderBy('Correlativo asc')->all();
        
        $componentes = Componente::find()
                ->select('Componente.ID,Componente.Nombre,Componente.Correlativo,sum(CronogramaAreSubCategoria.MetaFisica*AreSubCategoria.CostoUnitario) TotalObjetivo')
                ->innerJoin('Actividad','Actividad.ComponenteID=Componente.ID')
                ->innerJoin('ActRubroElegible','ActRubroElegible.ActividadID=Actividad.ID')
                ->innerJoin('AreSubCategoria','AreSubCategoria.ActRubroElegibleID=ActRubroElegible.ID')
                ->innerJoin('CronogramaAreSubCategoria','CronogramaAreSubCategoria.AreSubCategoriaID=AreSubCategoria.ID')
                ->where('Actividad.Estado=:EstadoA and Componente.Estado=:Estado and Componente.PoaID=:PoaID and CronogramaAreSubCategoria.MetaFisica!=0',[':EstadoA'=>1,':Estado' => 1, ':PoaID' => $poa->ID])
                ->andWhere(['between', 'CronogramaAreSubCategoria.Mes', $PasoCritico->MesInicio, $PasoCritico->MesFin])
                ->groupBy('Componente.ID,Componente.Nombre,Componente.Correlativo')
                ->orderBy('Componente.Correlativo asc')
                ->all();
                
        return $this->render('descargar-poa',['componentes'=>$componentes,'proyecto'=>$proyecto,'PasoCritico'=>$PasoCritico,'mesInicio'=>$PasoCritico->MesInicio,'mesFin'=>$PasoCritico->MesFin]);
    }
    
    public function actionDescargarPoaFinanciero($PasoCriticoID=null)
    {
        $this->layout='estandar';
        $PasoCritico=PasoCritico::findOne($PasoCriticoID);
        $proyecto=Proyecto::find()->where('ID=:ID',[':ID'=>$PasoCritico->ProyectoID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
        //$componentes=Componente::find()->where('PoaID=:PoaID',[':PoaID'=>$poa->ID])->orderBy('Correlativo asc')->all();
        $componentes = Componente::find()
                ->select('Componente.ID,Componente.Nombre,Componente.Correlativo,sum(CronogramaAreSubCategoria.MetaFisica*AreSubCategoria.CostoUnitario) TotalObjetivo')
                ->innerJoin('Actividad','Actividad.ComponenteID=Componente.ID')
                ->innerJoin('ActRubroElegible','ActRubroElegible.ActividadID=Actividad.ID')
                ->innerJoin('AreSubCategoria','AreSubCategoria.ActRubroElegibleID=ActRubroElegible.ID')
                ->innerJoin('CronogramaAreSubCategoria','CronogramaAreSubCategoria.AreSubCategoriaID=AreSubCategoria.ID')
                ->where('Componente.Estado=:Estado and Componente.PoaID=:PoaID and CronogramaAreSubCategoria.MetaFisica!=0',[':Estado' => 1, ':PoaID' => $poa->ID])
                ->andWhere(['between', 'CronogramaAreSubCategoria.Mes', $PasoCritico->MesInicio, $PasoCritico->MesFin])
                ->groupBy('Componente.ID,Componente.Nombre,Componente.Correlativo')
                ->orderBy('Componente.Correlativo asc')
                ->all();
        return $this->render('descargar-poa-financiero',['componentes'=>$componentes,'proyecto'=>$proyecto,'PasoCritico'=>$PasoCritico,'mesInicio'=>$PasoCritico->MesInicio,'mesFin'=>$PasoCritico->MesFin]);
    }
}
