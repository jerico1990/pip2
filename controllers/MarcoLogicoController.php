<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\InformacionGeneral;
use app\models\ProgramaTransversal;
use app\models\DireccionLinea;
use app\models\TipoInvestigacion;
use app\models\UnidadEjecutora;
use app\models\UnidadOperativa;
use app\models\Persona;
use app\models\Programa;
use app\models\CultivoCrianza;
use app\models\Especie;
use app\models\Ubigeo;
use app\models\Usuario;
use app\models\Investigador;
use app\models\EntidadParticipante;
use app\models\TipoInstitucion;
use app\models\AmbitoIntervencion;
use app\models\MiembroEquipoGestion;
use app\models\MarcoLogicoFinProyecto;
use app\models\MarcoLogicoPropositoProyecto;
use app\models\Proyecto;
use app\models\Poa;
use app\models\MarcoLogicoActividad;
use app\models\Actividad;
use app\models\Componente;
use app\models\MarcoLogicoComponente;
use app\models\CronogramaComponente;
use app\models\CronogramaActividad;
use app\models\CronogramaProyecto;
use app\models\ActRubroElegible;

class MarcoLogicoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($investigadorID=null)
    {
        
        $deshabilitar='disabled';
        if($investigadorID==null)
        {
            $this->layout='estandar';
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $deshabilitar='';
        }
        else
        {
            $this->layout='vacio';
            $investigador=Investigador::find()->where('ID=:ID',[':ID'=>$investigadorID])->one();//($investigadorID);
        }
        if(!$investigadorID && \Yii::$app->user->identity->rol!=7)
        {
            return $this->redirect(['panel/index']) ;
        }
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
        $model = $this->InformacionGeneral($investigador->ID);
        
        return $this->render('index',['infoGeneral' => $model,'poa'=>$poa,'deshabilitar'=>$deshabilitar]);
    }


    public function InformacionGeneral($investigador=null)
    {
        $model=InformacionGeneral::find()
                ->select('Proyecto.Fin,Proyecto.Proposito,Proyecto.ID,InformacionGeneral.TituloProyecto,InformacionGeneral.InvestigadorID,Usuario.username CodigoProyecto')
                ->innerJoin('Investigador','Investigador.ID=InformacionGeneral.InvestigadorID')
                ->innerJoin('Proyecto','Proyecto.InvestigadorID=Investigador.ID')
                ->innerJoin('Usuario','Usuario.ID=Investigador.UsuarioID')
                ->where('InformacionGeneral.InvestigadorID=:InvestigadorID',['InvestigadorID'=>$investigador])
                ->one();
                
        if(!$model)
        {
            $model=new InformacionGeneral;
        }
        return $model;
    }


    ///////////////////
    // FIN DEL PROYECTO
    ///////////////////

    ## Listado de Fin del proyecto
        public function actionFinlistado($investigadorID=null){
            $this->layout='vacio';
            
            $investigador=Investigador::findOne($investigadorID);
            $model = $this->InformacionGeneral($investigador->ID);
            $finProyectoDetalle = MarcoLogicoFinProyecto::find()->where(['Estado'=>1,'ProyectoID'=>$model->ID])->all();
            return $this->render('fin/_listado_fin',['FinDetalle' => $finProyectoDetalle]);
        }

    ## Form Crear de Fin del proyecto
        public function actionFinform($id = null){
            $this->layout='vacio';
            
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $investigador=$investigador->ID;

            if(!is_null($id)){
                $model = MarcoLogicoFinProyecto::findOne($id);
            }else{
                $model = new MarcoLogicoFinProyecto();
            }

            return $this->render('fin/_form_fin',['model' => $model ]);
        }

    ## Crear Fin del Proyecto (indicadores)
        public function actionIndicadoresfincrear(){
            $this->layout='vacio';

            $model = new MarcoLogicoFinProyecto();
            if ( $model->load(Yii::$app->request->post()) ) {
                
                $usuario=Usuario::findOne(\Yii::$app->user->id);
                $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
                $investigador=$investigador->ID;
                $proy = $this->InformacionGeneral($investigador);

                $model->ProyectoID  = $proy->ID;
                $model->Estado  = 1;

                $model->insert();
                
                $arr = array(
                    'Success' => true
                    );
                echo json_encode($arr);
            }
        }

    ## Actualizar Indicadores Fin del proyecto
        public function actionIndicadoresfinactualizar($id = null){
            $this->layout='vacio';
            $model = MarcoLogicoFinProyecto::findOne($id);
            if ( $model->load(Yii::$app->request->post()) ) {
                $model->update();
                $arr = array(
                    'Success' => true
                    );
                echo json_encode($arr);
            }
        }

    ## Eliminar Indicadores Fin del proyecto
        public function actionFindelete(){
            $this->layout='vacio';
            $ds = Yii::$app->request->post('id');

            $marco = MarcoLogicoFinProyecto::findOne($ds);
            
            $marco->Estado = 0;
            $isx = ($marco->update());

            // print_r($isx);
            if($isx == 1){
                $arr = array(
                    'Success' => true
                );
            }else{
                $arr = array(
                    'Error' => true
                );
            }
            echo json_encode($arr);

        }



    //////////////////////////
    // PROPOSITO DEL PROYECTO
    //////////////////////////

    ## Listado de Proposito del proyecto
        public function actionPropositolistado($investigadorID=null){
            $this->layout='vacio';
            $investigador=Investigador::findOne($investigadorID);
            $model = $this->InformacionGeneral($investigador->ID);
            $PropositoProyectoDetalle = MarcoLogicoPropositoProyecto::find()->where(['Estado'=>1,'ProyectoID'=>$model->ID])->all();
            return $this->render('proposito/_listado_proposito',['PropositoDetalle' => $PropositoProyectoDetalle]);
        }


    ## Form Crear de Fin del proyecto
        public function actionPropositoform($id = null){
            $this->layout='vacio';
            
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $investigador=$investigador->ID;

            if(!is_null($id)){
                $model = MarcoLogicoPropositoProyecto::findOne($id);
            }else{
                $model = new MarcoLogicoPropositoProyecto();
            }
            return $this->render('proposito/_form_proposito',['model' => $model ]);
        }

    ## Crear Proposito del Proyecto (indicadores)
        public function actionIndicadorespropositocrear(){
            $this->layout='vacio';

            $model = new MarcoLogicoPropositoProyecto();
            if ( $model->load(Yii::$app->request->post()) ) {
                
                $usuario=Usuario::findOne(\Yii::$app->user->id);
                $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
                $investigador=$investigador->ID;
                $proy = $this->InformacionGeneral($investigador);

                $model->ProyectoID  = $proy->ID;
                $model->Estado  = 1;

                $model->insert();
                
                $arr = array(
                    'Success' => true
                    );
                echo json_encode($arr);
            }
        }
    
    ## Actualizar Indicadores Proposito del proyecto
        public function actionIndicadorespropositoactualizar($id = null){
            $this->layout='vacio';
            $model = MarcoLogicoPropositoProyecto::findOne($id);
            // print_r(Yii::$app->request->post());
            if ( $model->load(Yii::$app->request->post()) ) {
                $mo =  $model->update();
                if($mo == 1){
                    $arr = array(
                        'Success' => true
                    );    
                }else{
                    $arr = array(
                        'Error' => 'No se modifico'
                    );
                    
                }
                echo json_encode($arr);
            }
        }

    ## Eliminar Indicadores Proposito del proyecto
        public function actionPropositodelete(){
            $this->layout='vacio';
            $ds = Yii::$app->request->post('id');

            $marco = MarcoLogicoPropositoProyecto::findOne($ds);
            $marco->Estado = 0;
            $marco->update();
            $arr = array(
                'Success' => true
                );
            echo json_encode($arr);

        }


    //////////////
    // ACTIVIDADES
    //////////////

    ## Form de actividad
        public function actionActividadform($ComponenteID = null,$ActividadID = null){
            $this->layout='vacio';
            
            // echo $ComponenteID;
            // echo '--------';
            // echo $ActividadID;

            if($ActividadID!=''){
                
                // $actividad=Actividad::findOne($ActividadID);
                // $model = MarcoLogicoActividad::find()->where('ActividadID=:ActividadID and Estado=1',[':ActividadID'=>$actividad->ID])->one();
                // $model->Nombre=$actividad->Nombre;
                // $model->Peso=$actividad->Peso;
                $model = Actividad::find()->where('ID=:ID and Estado=1',[':ID'=>$ActividadID])->one();
                
            }else{
                $model = new Actividad();
            }
            $model->ComponenteID = $ComponenteID;
            $componente=Componente::findOne($ComponenteID);
            $poa=Poa::find()->where('ID=:ID',[':ID'=>$componente->PoaID])->one();
            
            $proyecto=Proyecto::findOne($poa->ProyectoID);
            $investigador=Investigador::findOne($proyecto->InvestigadorID);
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            // print_r($model);
            // die();
            return $this->render('actividad/_form_actividad',['model' => $model,'Meses'=>$informacionGeneral->Meses ]);
        }
    
    ## Crear de actividad
        public function actionActividadCrear(){
            $this->layout='vacio';
            $model = new Actividad();

            if ( $model->load(Yii::$app->request->post()) ) {

                $meses = $_POST['Meses'];
                $model->Estado=1;
                $model->Correlativo=$this->CorrelativoActividad($model->ComponenteID);
                
                $query = (new \yii\db\Query())->from('Actividad')->where('ComponenteID=:ComponenteID AND Estado = 1',[':ComponenteID'=>$model->ComponenteID]);
                $sum = (int) $query->sum('Peso');
                $totalSuma = $sum + $model->Peso;
                if($totalSuma > 100 ){
                    $arr = array(
                        'Error' => true,
                        'Total'=> $totalSuma,
                        'Message' => 'EL PESO INGRESADO AH SUPERADO EL TOTAL DE LOS ACTIVIDADES'
                    );
                }else{
                    $ok = $model->save();
                    
                    for($i=1;$i<=$meses;$i++)
                    {
                        $new= new CronogramaActividad;
                        $new->ActividadID=$model->ID;
                        $new->Mes=$i;
                        $new->insert();
                    }
                    
                    if($ok == 1){
                        $arr = array(
                            'Success' => true,
                            'ID'    => $model->ID
                        );
                    }else{
                        $arr = array(
                            'Error' => true,
                            'Message' => 'Ocurrio un error al crear la actividad intentelo nuevamente'
                        );
                    }
                    
                }

                echo json_encode($arr);
            }
        }
    
    ## Actualizar de actividad
        public function actionActividadActualizar($id = null){
            $this->layout='vacio';
            if(!is_null($id)){
                $modelnue=Actividad::findOne($id);
                $ant = $modelnue->Peso;
                $modelnue->load(Yii::$app->request->post());

                $query = (new \yii\db\Query())->from('Actividad')->where('ComponenteID=:ComponenteID AND Estado = 1',[':ComponenteID'=>$modelnue->ComponenteID]);
                $sum = (int) $query->sum('Peso');

                $sumaAnt = $sum - $ant;
                $totalSuma = ($sumaAnt + (int)$modelnue->Peso);
                
                if($totalSuma > 100 ){
                    $arr = array(
                        'Error' => true,
                        'Total'=> $totalSuma,
                        'Message' => 'EL PESO INGRESADO AH SUPERADO EL TOTAL DE LOS ACTIVIDADES'
                    );
                }else{
                    $s = $modelnue->update();
                    if($s==1){
                        $arr = array(
                            'Success' => true,
                            'ID'      => $id
                        );
                    }else{
                        $arr = array(
                            'Error' => true,
                            'Message' => "Ocurrio un error al actualizar la actividad intentelo nuevamente"
                        );
                    }
                }

                echo json_encode($arr);
            }else{
                $modelant = new Actividad();
            }
            //return $this->render('actividad/_form_actividad',['model' => $model ]);
        }
        
    ## Eliminar Atividad
        public function actionEliminarActividad(){
            $this->layout='vacio';
            $id = Yii::$app->request->post('id');
            $actividad=Actividad::findOne($id);
            $marco = MarcoLogicoActividad::find()->where('ActividadID=:ActividadID and Estado=1',[':ActividadID'=>$actividad->ID])->one();
            if(!empty($marco)){
                $marco->Estado = 0;
                $marco->update();
            }
            $actividad->Estado=0;
            $actividad->update();
            $arr = array(
                'Success' => true
                );
            echo json_encode($arr);
        }

    ## Lista Indicadores Actividades
        public function actionListaIndicadoresActividad($ActividadID = null){
            $this->layout = 'vacio';
            if(!is_null($ActividadID)){
                $model = MarcoLogicoActividad::find()
                ->where(['ActividadID'=> $ActividadID,'Estado' => 1])
                ->all();
            }else{
                $model = new Actividad();
            }
            return $this->render('actividad/_lista_indicadores_actividad',[ 'model' => $model ]);
        }

    ## Indicador
        public function actionActividadCrearIndicador(){
            $this->layout='vacio';
            $model = new MarcoLogicoActividad();
            $xx = array();
            // print_r(Yii::$app->request->post());die();

            if ( $model->load(Yii::$app->request->post()) ) {
                $model->Estado = 1;
                $model->save();
                $id = $model->ActividadID;
                // $id = 1;

                $marcox = MarcoLogicoActividad::find()
                    ->where(['ActividadID'=> $model->ActividadID,'Estado' => 1])
                    ->all();

                foreach ($marcox as $value) {
                    $xx[] = array(
                        'ID'                    => $value->ID , 
                        'ActividadID'           => $value->ActividadID , 
                        'IndicadorDescripcion'  => $value->IndicadorDescripcion, 
                        'IndicadorUnidadMedida' => $value->IndicadorUnidadMedida, 
                        'IndicadorMeta'         => $value->IndicadorMetaFisica, 
                        'MedioVerificacion'     => $value->IndicadorMedioVerificacion, 
                        'Supuestos'             => $value->IndicadorSupuestos
                    );
                }
                // die();
                $arr = array(
                    'Success' => true,
                    'id'     => $id,
                    'marco' => $xx
                );
                echo json_encode($arr);
            }
        }


        public function actionEliminarIndicadorActividad(){
            $this->layout='vacio';
            $ds = Yii::$app->request->post('id');
            $comp = Yii::$app->request->post('comp');

            $customer = MarcoLogicoActividad::findOne($ds);
            $customer->Estado = 0;
            $customer->update();
            $arr = array(
                'Success' => true,
                'comp'    => $comp
                );
            echo json_encode($arr);
        }



    ## Eliminar Objetivo
        public function actionEliminarComponente(){
            $this->layout='vacio';
            $id = Yii::$app->request->post('id');
            $componente=Componente::findOne($id);
            $componente->Estado=0;
            $componente->update();
            MarcoLogicoComponente::updateAll(['Estado' => 0], 'ComponenteID=:ComponenteID and Estado=:Estado',[':ComponenteID'=>$componente->ID,':Estado'=>1]);
            
            Yii::$app->db->createCommand('update Actividad set Estado=0 where ComponenteID='.$componente->ID.' and Estado=1')->execute();
            Yii::$app->db->createCommand('update MarcoLogicoActividad set Estado=0 from MarcoLogicoActividad inner join Actividad on Actividad.ID=MarcoLogicoActividad.ActividadID where Actividad.ComponenteID='.$componente->ID)->execute();
            
            $arr = array(
                'Success' => true
                );
            echo json_encode($arr);
        }
        
        public function actionGenerarCronogramaComponente()
        {
            $componentes=InformacionGeneral::find()
                            ->select('InformacionGeneral.Meses,Componente.ID ComponenteID')
                            ->innerJoin('Investigador','Investigador.ID=InformacionGeneral.InvestigadorID')
                            ->innerJoin('Proyecto','Proyecto.InvestigadorID=Investigador.ID')
                            ->innerJoin('Poa','Poa.ProyectoID=Proyecto.ID')
                            ->innerJoin('Componente','Componente.PoaID=Poa.ID')
                            ->all();
            $a=1;
            foreach($componentes as $componente)
            {  print($componente->Meses);
                for($i=1;$i<=$componente->Meses;$i++)
                {
                    
                    $new= new CronogramaComponente;
                    $new->ComponenteID=$componente->ComponenteID;
                    $new->Mes=$i;
                    $new->MetaFinanciera=0;
                    $new->PoafMetaFinanciera=0;
                    $new->insert();
                }
            }
        }
        
        public function actionGenerarCronogramaActividad()
        {
            $actividades=InformacionGeneral::find()
                            ->select('InformacionGeneral.Meses,Actividad.ID ActividadID')
                            ->innerJoin('Investigador','Investigador.ID=InformacionGeneral.InvestigadorID')
                            ->innerJoin('Proyecto','Proyecto.InvestigadorID=Investigador.ID')
                            ->innerJoin('Poa','Poa.ProyectoID=Proyecto.ID')
                            ->innerJoin('Componente','Componente.PoaID=Poa.ID')
                            ->innerJoin('Actividad','Actividad.ComponenteID=Componente.ID')
                            ->all();
            $a=1;
            foreach($actividades as $actividad)
            {  print($actividad->Meses);
                for($i=1;$i<=$actividad->Meses;$i++)
                {
                    $new= new CronogramaActividad;
                    $new->ActividadID=$actividad->ActividadID;
                    $new->Mes=$i;
                    $new->MetaFinanciera=0;
                    $new->MetaFisica=0;
                    $new->PoafMetaFinanciera=0;
                    $new->PoafMetaFisica=0;
                    $new->insert();
                }
            }
        }
        
        public function actionGenerarCronogramaProyecto()
        {
            $proyectos=InformacionGeneral::find()
                            ->select('InformacionGeneral.Meses,Proyecto.ID ProyectoID')
                            ->innerJoin('Investigador','Investigador.ID=InformacionGeneral.InvestigadorID')
                            ->innerJoin('Proyecto','Proyecto.InvestigadorID=Investigador.ID')
                            ->all();
            $a=1;
            foreach($proyectos as $proyecto)
            {  print($proyecto->Meses);
                for($i=1;$i<=$proyecto->Meses;$i++)
                {
                    $new= new CronogramaProyecto;
                    $new->ProyectoID=$proyecto->ProyectoID;
                    $new->Mes=$i;
                    $new->MetaFinanciera=0;
                    $new->PoafMetaFinanciera=0;
                    $new->MesDescripcion=$this->DescripcionMes($new->Mes,$proyecto->FechaInicioProyecto);
                    $new->insert();
                }
            }
        }
        
        public function actionActualizarCronogramaProyecto()
        {
            $proyectos=InformacionGeneral::find()
                            ->select('InformacionGeneral.Meses,Proyecto.ID ProyectoID,InformacionGeneral.FechaInicioProyecto')
                            ->innerJoin('Investigador','Investigador.ID=InformacionGeneral.InvestigadorID')
                            ->innerJoin('Proyecto','Proyecto.InvestigadorID=Investigador.ID')
                            ->all();
            $a=1;
            foreach($proyectos as $proyecto)
            {  //print($proyecto->Meses);
                for($i=1;$i<=$proyecto->Meses;$i++)
                {
                    $new= new CronogramaProyecto;
                    $new->ProyectoID=$proyecto->ProyectoID;
                    $new->Mes=$i;
                    $new->MetaFinanciera=0;
                    $new->MesDescripcion=$this->DescripcionMes($new->Mes,$proyecto->FechaInicioProyecto);
                    $new->PoafMetaFinanciera=0;
                    $new->insert();
                }
            }
        }
        
        public function actionDescripcionMes($mes=null,$fechaInicio=null)
        {
            setlocale (LC_TIME,"spanish");
            //$mes = 01;
            //$fechaInicio = '2016-10-21';
            $nuevafecha = strtotime ( '+'.$mes.' month' , strtotime ( $fechaInicio ) ) ;
            $nuevafecha = strtoupper(strftime ( '%b %Y' , $nuevafecha ) ) ;
            echo $nuevafecha;
        }
        
        public function CorrelativoActividad($componenteID)
        {
            $actividad=Actividad::find()->where('ComponenteID=:ComponenteID',[':ComponenteID'=>$componenteID])->orderBy('Correlativo desc')->one();
            if($actividad)
            {
                return $actividad->Correlativo+1;
            }
            else
            {
                return 1;
            }
        }
        
        public function actionDescargarMarcoLogico($CodigoProyecto=null)
        {
            if($CodigoProyecto)
            {
                $informacion=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])->one();
                $unidadOperativa=UnidadOperativa::find()->where('ID=:ID',[':ID'=>$informacion->UnidadOperativaID])->one();
                $unidadEjecutora=UnidadEjecutora::find()->where('ID=:ID',[':ID'=>$unidadOperativa->UnidadEjecutoraID])->one();
                
                $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacion->InvestigadorID])->one();
                $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
                $fines=MarcoLogicoFinProyecto::find()->where('ProyectoID=:ProyectoID AND Estado = 1',[':ProyectoID'=>$proyecto->ID])->all();
                $propositos=MarcoLogicoPropositoProyecto::find()->where('ProyectoID=:ProyectoID AND Estado = 1',[':ProyectoID'=>$proyecto->ID])->all();
                $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
                $componentes=MarcoLogicoComponente::find()
                            ->select('MarcoLogicoComponente.*,Componente.Nombre ComponenteNombre,Componente.Correlativo ComponenteCorrelativo')
                            ->innerJoin('Componente','MarcoLogicoComponente.ComponenteID=Componente.ID')
                            ->where('Componente.PoaID=:PoaID and Componente.Estado=1',[':PoaID'=>$poa->ID])
                            ->all();
                
                $actividades=MarcoLogicoActividad::find()
                            ->select('MarcoLogicoActividad.*,Actividad.Nombre ActividadNombre,Actividad.Correlativo ActividadCorrelativo,Componente.Correlativo ComponenteCO')
                            ->innerJoin('Actividad','MarcoLogicoActividad.ActividadID=Actividad.ID')
                            ->innerJoin('Componente','Actividad.ComponenteID=Componente.ID')
                            ->where('Componente.PoaID=:PoaID and Actividad.Estado=1',[':PoaID'=>$poa->ID])
                            ->all();
                            
                
                return $this->render('descargar-marco-logico',['unidadEjecutora'=>$unidadEjecutora,'informacion'=>$informacion,'proyecto'=>$proyecto,'fines'=>$fines,'propositos'=>$propositos,'componentes'=>$componentes,'actividades'=>$actividades]);
            }
        }
        
        public function actionCarga()
        {
            ini_set('memory_limit', '-1');
            $this->layout='estandar';
            $model = new ActRubroElegible();
            if ( $model->load(Yii::$app->request->post()) ) {
                
                $csvMimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
                if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
                    //var_dump($_FILES['ActRubroElegible']);die;
                    if(is_uploaded_file($_FILES['file']['tmp_name'])){
                        
                        //open uploaded csv file with read only mode
                        $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
                        
                        //skip first line
                        fgetcsv($csvFile);
                        
                        //parse data from csv file line by line
                        while(($line = fgetcsv($csvFile)) !== FALSE){
                            //
                            $informacion=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>utf8_encode($line[0])])->one();
                            if(!$informacion)
                            {
                                var_dump(utf8_encode($line[0]));die;
                            }
                            else
                            {
                                $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacion->InvestigadorID])->one();    
                            }
                            
                            $poa=Poa::find()->where('ProyectoID=:ProyectoID and Estado=1',[':ProyectoID'=>$proyecto->ID])->one();
                            $mes = InformacionGeneral::find()
                                ->select('Meses')
                                ->where(['InvestigadorID' => $informacion->InvestigadorID])
                                ->one();
                            if(utf8_encode($line[11])=="1"){
                                $componente=Componente::find()->where('Correlativo=:Correlativo and PoaID=:PoaID',[':Correlativo'=>utf8_encode($line[1]),':PoaID'=>$poa->ID])->one();
                                if(!$componente)
                                {
                                    $componente=new Componente;
                                    $componente->PoaID=$poa->ID;
                                    $componente->Correlativo=utf8_encode($line[1]);
                                    $componente->Nombre=utf8_encode($line[2]);
                                    $componente->Peso=utf8_encode($line[3]);
                                    $componente->Experimento=utf8_encode($line[7]);
                                    $componente->Evento=utf8_encode($line[8]);
                                    $componente->Estado=1;
                                    $componente->save();
                                    
                                    for ($a=1; $a <= $mes->Meses; $a++) {
                                        $cronoComponente = new CronogramaComponente();
                                        $cronoComponente->ComponenteID = $componente->ID;
                                        $cronoComponente->Mes = $a;
                                        $cronoComponente->save();
                                    }
                                }
                                
                                $marcoLogicoComponente=new MarcoLogicoComponente;
                                $marcoLogicoComponente->ComponenteID=$componente->ID;
                                $marcoLogicoComponente->IndicadorMeta=utf8_encode($line[4]);
                                $marcoLogicoComponente->IndicadorUnidadMedida=utf8_encode($line[5]);
                                $marcoLogicoComponente->IndicadorDescripcion=utf8_encode($line[6]);
                                $marcoLogicoComponente->MedioVerificacion=utf8_encode($line[9]);
                                $marcoLogicoComponente->Supuestos=utf8_encode($line[10]);
                                $marcoLogicoComponente->Estado=1;
                                $marcoLogicoComponente->save();
                            }
                            elseif(utf8_encode($line[11])=="2"){
                                $componenteID=substr(utf8_encode($line[1]),0,1);
                                $CorrelativoID=substr(utf8_encode($line[1]),2,1);
                                $componente=Componente::find()->where('Correlativo=:Correlativo and PoaID=:PoaID',[':Correlativo'=>$componenteID,':PoaID'=>$poa->ID])->one();//One($componenteID);
                                if(!$componente)
                                {
                                    var_dump(utf8_encode($line[0]));die;
                                }
                                
                                $actividad=Actividad::find()->where('Correlativo=:Correlativo and ComponenteID=:ComponenteID',[':Correlativo'=>$CorrelativoID,':ComponenteID'=>$componente->ID])->one();
                                if(!$actividad)
                                {
                                    $actividad=new Actividad;
                                    $actividad->ComponenteID=$componente->ID;
                                    $actividad->Correlativo=$CorrelativoID;
                                    $actividad->Nombre=utf8_encode($line[2]);
                                    $actividad->Peso=utf8_encode($line[3]);
                                    $actividad->Experimento=utf8_encode($line[7]);
                                    $actividad->Evento=utf8_encode($line[8]);
                                    $actividad->Estado=1;
                                    $actividad->save();
                                    
                                    for ($a=1; $a <= $mes->Meses; $a++) {
                                        $cronoActividad = new CronogramaActividad();
                                        $cronoActividad->ActividadID = $actividad->ID;
                                        $cronoActividad->Mes = $a;
                                        $cronoActividad->save();
                                    }
                                }
                                
                                $marcoLogicoActividad=new MarcoLogicoActividad;
                                $marcoLogicoActividad->ActividadID=$actividad->ID;
                                $marcoLogicoActividad->IndicadorMetaFisica=utf8_encode($line[4]);
                                $marcoLogicoActividad->IndicadorUnidadMedida=utf8_encode($line[5]);
                                $marcoLogicoActividad->IndicadorDescripcion=utf8_encode($line[6]);
                                $marcoLogicoActividad->IndicadorMedioVerificacion=utf8_encode($line[9]);
                                $marcoLogicoActividad->IndicadorSupuestos=utf8_encode($line[10]);
                                $marcoLogicoActividad->Estado=1;
                                $marcoLogicoActividad->save();
                            }
                        }
                        
                        //close opened csv file
                        fclose($csvFile);
                       
                    }
                }
                
            }
            
            return $this->render('carga');
        }
        
    public function actionObjetivosActividadesJson($codigo=null)
    {
        $totalObjetivo = Yii::$app->db->createCommand("
            SELECT sum(AreSubCategoria.CostoUnitario*AreSubCategoria.MetaFisica) Total,Componente.Correlativo , Componente.Nombre FROM Componente
            INNER JOIN Actividad ON Componente.ID = Actividad.ComponenteID
            INNER JOIN ActRubroElegible ON Actividad.ID = ActRubroElegible.ActividadID
            INNER JOIN AreSubCategoria ON ActRubroElegible.ID = AreSubCategoria.ActRubroElegibleID
            WHERE Componente.PoaID = ".$proyecto->ID."
            GROUP BY Componente.Correlativo,Componente.Nombre
            ORDER BY 2")->queryAll();
    }
    
    public function actionGenerarPasosCriticos()
    {
        $meses=InformacionGeneral::find()->all();
        foreach($meses as $mes){
            for($i=1;$i<$mes->Meses;$i++)
            {
                if($i<=6)
                {
                    if($i==$mes->Meses)
                    {
                        
                    }
                    else
                    {
                        
                    }
                }
                elseif($i>6 && $i<=12)
                {
                    if($i==$mes->Meses)
                    {
                        
                    }
                }
                elseif($i>12 && $i<=17)
                {
                    if($i==$mes->Meses)
                    {
                        
                    }
                }
                elseif($i>17 && $i<=22)
                {
                    if($i==$mes->Meses)
                    {
                        
                    }
                }
                elseif($i>22 && $i<=27)
                {
                    if($i==$mes->Meses)
                    {
                        
                    }
                }
                elseif($i>27 && $i<=36)
                {
                    if($i==$mes->Meses)
                    {
                        
                    }
                }
            }    
        }
        
    }
}