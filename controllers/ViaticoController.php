<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\InformacionGeneral;
use app\models\ProgramaTransversal;
use app\models\DireccionLinea;
use app\models\TipoInvestigacion;
use app\models\UnidadEjecutora;
use app\models\UnidadOperativa;
use app\models\Investigador;
use app\models\EntidadParticipante;
use app\models\TipoInstitucion;
use app\models\RepresentanteLegal;
use app\models\Persona;
use app\models\Usuario;
use app\models\Requerimiento;
use app\models\Padron;

use app\models\Situacion;
use app\models\Rendicion;
use app\models\Viatico;
use app\models\RendicionViatico;
use app\models\DetalleViatico;
use app\models\DetalleRequerimiento;
use app\models\DetalleRendicionViatico;
use app\models\TipoDocumento;

use PhpOffice\PhpWord\PhpWord;
use yii\web\UploadedFile;

class ViaticoController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $this->layout='estandar';
        return $this->render('index');
    }


    public function actionListaViaticos($CodigoProyecto=null)
    {
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        

        $resultados = (new \yii\db\Query())
            ->select('Viaticos.*')
            ->from('Viaticos')
            ->where(['Viaticos.CodigoProyecto'=>$CodigoProyecto])
            ->distinct()
            ->all();




        $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();


        $nro=0;
        foreach($resultados as $result)
        {
            $nro++;
            echo "<tr>";
                echo "<td> Viático N° " . $result["Correlativo"] . "</td>";
                echo "<td>" . $result["Apellido"] . ' ' . $result["Nombre"] . "</td>";
                echo "<td>" . $result["Destino"] . "</td>";
                echo "<td>" . $this->getTipoViaje($result["TipoViaje"]) . "</td>";
                echo "<td></td>";
            echo "</tr>";
        }
    }


    // public function actionCrearRequerimiento($CodigoProyecto = null,$RequerimientoID = null){
        //     $this->layout='vacio';
        //     if(!$CodigoProyecto)
        //     {
        //         $usuario=Usuario::findOne(\Yii::$app->user->id);
        //         $datosInvestigador=Persona::findOne($usuario->PersonaID);
        //         $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        //         $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        //         $CodigoProyecto=$informacionGeneral->Codigo;
        //     }

        //     $req=Requerimiento::find()->where('ID=:ID',[':ID'=>$RequerimientoID])->one();

        //     return $this->render('_requerimiento',['CodigoProyecto'=>$CodigoProyecto,'Requerimiento'=>$req]);
        // }


    public function actionPlanillaViaticos(){
        $this->layout='estandar';

        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $CodigoProyecto=$informacionGeneral->Codigo;
        return $this->render('_planilla_viaticos_listado',['CodigoProyecto'=>$CodigoProyecto]);
    }


    public function actionCrearPlanillaViaticos($RequerimientoID = null,$CodigoProyecto=null,$DetallesRequerimientosIDs=[]){
        $this->layout='vacio';
        $da = Viatico::find()->where(['RequerimientoID'=>$RequerimientoID])->one();
        $tot = DetalleRequerimiento::find()->select('sum(Total) Total')->where('ID in ('.$DetallesRequerimientosIDs.')')->one();
        
        $integrantes=RecursosHumanosController::actionIntegrantes($CodigoProyecto,2);

        return $this->render('_planilla_viaticos',['CodigoProyecto'=>$CodigoProyecto,'RequerimientoID'=>$RequerimientoID,'total'=>$tot,'integrantes'=>$integrantes,'DetallesRequerimientosIDs'=>$DetallesRequerimientosIDs]);

    }

    public function actionCrearPlanilla($CodigoProyecto=null){
        $this->layout='vacio';
        $viaticos=new Viatico;
        if( $viaticos->load( Yii::$app->request->post() ) ){
            $viaticos->Situacion=1;
            $viaticos->Estado=1;
            $viaticos->Monto= str_replace(',','', $viaticos->Monto);
            $viaticos->FechaCreacion=date('Ymd h:m:s');
            $viaticos->Salida=date('Ymd h:m:s',strtotime($viaticos->Salida));
            $viaticos->Regreso=date('Ymd h:m:s',strtotime($viaticos->Regreso));
            $viaticos->Annio = date('Y');
            $viaticos->TipoCambio = 1;
            $viaticos->Correlativo=$this->CorrelativoViaticos($CodigoProyecto);
            $viaticos->NroDocumento=str_replace('_','','PV'.$CodigoProyecto.str_pad($viaticos->Correlativo, 3, "0", STR_PAD_LEFT).$viaticos->Annio);
            $viaticoAdd = $viaticos->save();
            
            if($viaticoAdd == 1){
                $detallesRequerimientos=DetalleRequerimiento::find()->where('ID in ('.$viaticos->DetallesRequerimientosIDs.')')->all();

                foreach($detallesRequerimientos as $detalleRequerimiento){
                    $detalle=DetalleRequerimiento::findOne($detalleRequerimiento->ID);
                    $detalle->TipoDetalleOrdenID=$viaticos->ID;
                    $detalle->Situacion=3;
                    $detalle->update();
                }

                if(!$viaticos->Codigos)
                {
                    $countCodigos=0;
                }
                else
                {
                    $countCodigos=count(($viaticos->Codigos));
                }
                
                for($i=0;$i<$countCodigos;$i++)
                {
                    if(isset($viaticos->Codigos[$i]))
                    {
                        $detallex=new DetalleViatico;
                        $detallex->ViaticoID = $viaticos->ID;
                        $detallex->Tipo = $viaticos->Tipo[$i];
                        $detallex->Estado  = 1;  
                        $detallex->Importe = str_replace(',','', $viaticos->Importe[$i]);
                        $detallex->save();
                    }
                }
            }
            
            return $this->redirect(['requerimiento/generar','ID'=>$viaticos->RequerimientoID]);
        }
        
    }

    public function CorrelativoViaticos($CodigoProyecto)
    {
        $orden=Viatico::find()->where('CodigoProyecto=:CodigoProyecto',[':CodigoProyecto'=>$CodigoProyecto])->orderBy('Correlativo desc')->one();
        if($orden)
        {
            return $orden->Correlativo+1;
        }
        else
        {
            return 1;
        }
    }

    public function getTipoViaje($tipo=null)
    {
        if($tipo==1){
            return "Nacional";
        }
        elseif($tipo==2)
        {
            return "Local o Regional";
        }
        
        
    }

    public function actionCargaPlanilla($CodigoProyecto = null){
        if(!is_null($CodigoProyecto)){
            // $resultados = (new \yii\db\Query())
            // ->select('Viatico.*,Requerimiento.Correlativo CorrelativoA')
            // ->from('RequerimientoViatico')
            // ->innerJoin('Viatico','RequerimientoViatico.ViaticoID != Viatico.ID')
            // ->innerJoin('Requerimiento','Requerimiento.ID = RequerimientoViatico.RequerimientoID')
            // ->where(['Viatico.CodigoProyecto'=>$CodigoProyecto])
            // ->distinct()
            // ->all();
        }else{

        }



        foreach ($resultados as $res) {
            echo '<option>'.$res['Apellido'].' '.$res['Nombre'].' - Req: '.$res['CorrelativoA'].'</option>';
        }
    }


    public function actionPlantilla($ID=null)
    {
        $viatico=Viatico::findOne($ID);
        $detalles=DetalleViatico::find()
                    ->Select('DetalleViatico.*')
                    ->where('ViaticoID=:ViaticoID',[':ViaticoID'=>$ID])
                    ->all();

        $informacion=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$viatico->CodigoProyecto])->one();
        $eea=UnidadOperativa::find()->where('ID=:ID',[':ID'=>$informacion->UnidadOperativaID])->one();

        $req=Requerimiento::findOne($viatico->RequerimientoID);

        $detallesRequerimientos=DetalleRequerimiento::find()->where('RequerimientoID=:RequerimientoID and TipoDetalleOrdenID=:TipoDetalleOrdenID',[':RequerimientoID'=>$viatico->RequerimientoID,':TipoDetalleOrdenID'=>$viatico->ID])->all();
        //$term=TerminoReferencia::findOne($req->TerminoReferenciaID);
        $CadenaCodigoPoa="";
        $i=1;
        foreach($detallesRequerimientos as $detalleRequerimiento){
            if($i>2)
            {
                $CadenaCodigoPoa='O.'.substr($detalleRequerimiento->CodigoMatriz,0,1).' A'.substr($detalleRequerimiento->CodigoMatriz,2,1);    
            }
            else
            {
                $CadenaCodigoPoa='O.'.substr($detalleRequerimiento->CodigoMatriz,0,1).' A'.substr($detalleRequerimiento->CodigoMatriz,2,1).','.$CadenaCodigoPoa;    
            }
            $i++;
        }
        
        $countDetalles=0;
        foreach($detalles as $detalle)
        {         
           $countDetalles++;
        }
      
        require_once(Yii::$app->basePath . '/web/PHPWord/src/PhpWord/Autoloader.php');     
        \PhpOffice\PhpWord\Autoloader::register();
        $phpWord = new PhpWord();
        $codeProyecto = explode('_',$req->CodigoProyecto)[0];
        $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/documentos/PLANTILLA_PLANILLA_VIATICOS.docx');
        // $template->setValue('ID', trim("PV".date('Y').$codeProyecto.str_pad($viatico->Correlativo, 3, "0", STR_PAD_LEFT) ) );

        $template->setValue('ID', $viatico->Annio.'.'. Yii::$app->tools->dateFormat($viatico->FechaCreacion,'m') .'.'.str_pad($viatico->Correlativo, 3, "0", STR_PAD_LEFT).'.'.$viatico->ID);
        $template->setValue('CODIGOPROYECTO', $viatico->CodigoProyecto);
        $template->setValue('DATOSPERSONALES', $viatico->Apellido.' '.$viatico->Nombre);
        $template->setValue('CARGO',$viatico->Cargo);
        $template->setValue('EEAA',$eea->Nombre);
        $template->setValue('PLANDETRABAJO', $viatico->PlanTrabajo);
        $template->setValue('TIPOVIAJE', $this->getTipoViaje($viatico->TipoViaje));
        $template->setValue('DESTINO', $viatico->Destino);
        $template->setValue('SALIDA', date('d/m/Y H:m:s',strtotime($viatico->Salida)) );
        $template->setValue('REGRESO', date('d/m/Y H:m:s',strtotime($viatico->Regreso)) );
        $template->setValue('DIAS', $viatico->NroDias);
        $template->setValue('HORAS', $viatico->AsigHoras);

        $template->setValue('FTEFTO', 'PROYECTO PNIA Nº'.$viatico->CodigoProyecto);
        $template->setValue('BANCO', $viatico->CuentaBancaria);
        $template->setValue('LUGARFECHA', $viatico->LugarFecha);


        // $template->setValue('CODIGOPOA', $CadenaCodigoPoa);
        $template->cloneRow('TIPO', $countDetalles);
        
        $countDetalles=1;
        $total = 0;
        foreach($detalles as $detalle)
        {
            
            $template->setValue('TIPO#'.$countDetalles, "".$detalle->Tipo."");
            $template->setValue('IMPORTE#'.$countDetalles, ($detalle->Importe != '.00')? $detalle->Importe : '0.00' );
            $total = $detalle->Importe + $total;
            //$template->setValue('DIRECTIVONOMBRE#'.$countdirectivos.'', "".ucwords(strtolower($registroparametros->getNombresWord($directivo->persona_id)))."");
            $countDetalles++;
        }

        $template->setValue('TOTAL', "S/. ".$total);
      
        $temp_file = tempnam(sys_get_temp_dir(), 'PHPWord');
        $template->saveAs($temp_file);
        $contentType = 'Content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document;';
        header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
        header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
        header ( "Cache-Control: no-cache, must-revalidate" );
        header ( "Pragma: no-cache" );
        header ( $contentType );
        header ( "Content-Disposition: attachment; filename='".trim("PV-".date('Y').$codeProyecto.str_pad($viatico->Correlativo, 3, "0", STR_PAD_LEFT)).".docx'");
        readfile($temp_file); // or echo file_get_contents($temp_file);
        unlink($temp_file);  // remove temp file
        return true;    
    }


    public function actionPlantillaRendicion($ID=null,$CodigoProyecto=null)
    {
        $viatico=Viatico::findOne($ID);
        $detalles=DetalleViatico::find()
                    ->Select('DetalleViatico.*')
                    ->where('ViaticoID=:ViaticoID',[':ViaticoID'=>$ID])
                    ->all();

        $rendicion = Rendicion::find()->where(['TransaccionID'=>$ID,'CodigoProyecto'=>$CodigoProyecto])->all();
        $rendicionViatico = RendicionViatico::find()->where(['ViaticoID'=>$ID,'CodigoProyecto'=>$CodigoProyecto])->one();
        // $detRendi = DetalleRendicionViatico::find()->where(['RendicionID'=>$rendicionViatico->ID])->all();
        // print_r($rendicionViatico);
        // die();

        $detRendi=DetalleRendicionViatico::find()
                    ->Select('DetalleRendicionViatico.*')
                    ->where('RendicionID=:RendicionID',[':RendicionID'=>$rendicionViatico->ID])
                    ->all();


        $informacion=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$viatico->CodigoProyecto])->one();
        $eea=UnidadOperativa::find()->where('ID=:ID',[':ID'=>$informacion->UnidadOperativaID])->one();

        $req=Requerimiento::findOne($viatico->RequerimientoID);

        $detallesRequerimientos=DetalleRequerimiento::find()->where('RequerimientoID=:RequerimientoID and TipoDetalleOrdenID=:TipoDetalleOrdenID',[':RequerimientoID'=>$viatico->RequerimientoID,':TipoDetalleOrdenID'=>$viatico->ID])->all();
        //$term=TerminoReferencia::findOne($req->TerminoReferenciaID);
        $CadenaCodigoPoa="";
        $i=1;
        foreach($detallesRequerimientos as $detalleRequerimiento){
            if($i>2)
            {
                $CadenaCodigoPoa='O.'.substr($detalleRequerimiento->CodigoMatriz,0,1).' A'.substr($detalleRequerimiento->CodigoMatriz,2,1);    
            }
            else
            {
                $CadenaCodigoPoa='O.'.substr($detalleRequerimiento->CodigoMatriz,0,1).' A'.substr($detalleRequerimiento->CodigoMatriz,2,1).','.$CadenaCodigoPoa;    
            }
            $i++;
        }
        
        $countDetalles=0;
        foreach($detRendi as $detalle)
        {         
           $countDetalles++;
        }
      
        require_once(Yii::$app->basePath . '/web/PHPWord/src/PhpWord/Autoloader.php');     
        \PhpOffice\PhpWord\Autoloader::register();
        $phpWord = new PhpWord();

        $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/documentos/PLANTILLA_RENDICION_VIATICO.docx');
        $template->setValue('ID', $viatico->Annio.'.'. Yii::$app->tools->dateFormat($viatico->FechaCreacion,'m') .'.'.str_pad($viatico->Correlativo, 3, "0", STR_PAD_LEFT).'.'.$viatico->ID);
        $template->setValue('CODIGOPROYECTO', $viatico->CodigoProyecto);
        $template->setValue('DATOSPERSONALES', $viatico->Apellido.' '.$viatico->Nombre);
        $template->setValue('CARGO',$viatico->Cargo);
        $template->setValue('EEAA',$eea->Nombre);
        $template->setValue('PLANDETRABAJO', $viatico->PlanTrabajo);
        $template->setValue('TIPOVIAJE', $this->getTipoViaje($viatico->TipoViaje));
        $template->setValue('DESTINO', $viatico->Destino);
        $template->setValue('SALIDA', $viatico->Salida );
        $template->setValue('REGRESO', $viatico->Regreso );
        $template->setValue('DIAS', $viatico->NroDias.'/'.$viatico->AsigHoras);
        $template->setValue('FTEFTO', 'PROYECTO PNIA Nº'.$viatico->CodigoProyecto);
        $template->setValue('BANCO', $viatico->CuentaBancaria);
        $template->setValue('LUGARFECHA', $viatico->LugarFecha);
        $template->setValue('ANTICIPO', $rendicionViatico->AnticipoRecibido);
        $template->setValue('GASTO', $rendicionViatico->GastoSustentado);
        $template->setValue('REINTEGRAR', $rendicionViatico->Reintegrar);
        $template->setValue('SIAF', '');

        // echo '<pre>';
        // print_r($countDetalles); 
        // die();
        // $template->setValue('CODIGOPOA', $CadenaCodigoPoa);
        // print_r($template); die();
        $template->cloneRow('IMPORTE', $countDetalles);
        $countDetalles=1;
        $total = 0;
        foreach($detRendi as $detalle)
        {
            
            $template->setValue('ID#'.$countDetalles, $detalle->ID);
            $template->setValue('FECHA#'.$countDetalles, Yii::$app->tools->dateFormat($detalle->Fecha,'d/m/Y') );
            $template->setValue('CLASE#'.$countDetalles, $detalle->Clase);
            $template->setValue('PROVEEDOR#'.$countDetalles, $detalle->Proveedor);
            $template->setValue('DETALLE#'.$countDetalles, $detalle->Detalle);
            $template->setValue('IMPORTE#'.$countDetalles, $detalle->Importe);
            $total = $detalle->Importe + $total;
            //$template->setValue('DIRECTIVONOMBRE#'.$countdirectivos.'', "".ucwords(strtolower($registroparametros->getNombresWord($directivo->persona_id)))."");
            $countDetalles++;
        }

        $template->setValue('TOTAL', "S/. ".$total);
      
        $temp_file = tempnam(sys_get_temp_dir(), 'PHPWord');
        $template->saveAs($temp_file);
        header("Content-Disposition: attachment; filename='rendicion-viatico-".$viatico->Correlativo.'-'.$rendicionViatico->ID.".docx'");
        readfile($temp_file); // or echo file_get_contents($temp_file);
        unlink($temp_file);  // remove temp file
        return true;    
    }

    public function actionEliminarAdjunto($id=null)
    {
        $viatico=Viatico::findOne($id);
        $viatico->Documento='';
        $viatico->Situacion=1;
        $viatico->update();
       
        $arr = array('Success' => true);
        echo json_encode($arr);
    }

    public function actionAdjuntarDocumento($ID=null){
        $this->layout='vacio';
        $viatico=Viatico::findOne($ID);
        $Requerimiento=Requerimiento::findOne($viatico->RequerimientoID);
        if($viatico->load(Yii::$app->request->post())){
            $viatico->Situacion=3;
            $viatico->archivo = UploadedFile::getInstance($viatico, 'archivo');
            if($viatico->archivo)
            {
                $viatico->archivo->saveAs('viatico/' . $viatico->ID . '.' . $viatico->archivo->extension);
                $viatico->Documento=$viatico->ID . '.' . $viatico->archivo->extension;
            }
            $viatico->update();
            return $this->redirect(['requerimiento/generar','ID'=>$Requerimiento->ID]);
        }
        return $this->render('adjuntar-documento',['ID'=>$ID,'Viatico'=>$viatico]);
    }



    public function actionEliminar($id=null)
    {
        $viatico=Viatico::findOne($id);
        $viatico->Estado=0;
        $viatico->Situacion=6;
        $viatico->update();

        // echo $viatico->RequerimientoID;die();
        $reqs = DetalleRequerimiento::find()->where(['RequerimientoID'=>$viatico->RequerimientoID,'TipoDetalleOrdenID'=>$id])->all();
        foreach($reqs as $req)
        {
            $req->Situacion=2;
            $req->update();
        }
        

        $arr = array('Success' => true);
        echo json_encode($arr);
    }


    public function actionSumaRendicionPlanilla($id = null){
        if(!empty($id)){
            $detalleTotal = (new \yii\db\Query())
                ->select('sum(Importe) total')
                ->where(['ViaticoID'=>$id])
                ->from('DetalleViatico')
                ->one();
            $resu = $detalleTotal['total'];
        }else{
            $resu = '00.00';
        }

        return $resu;
    }

    public function actionRendicion($CodigoProyecto = null,$codigo = null){
        $this->layout='vacio';


        $viatico=Viatico::find()
        ->select('Viatico.ID,Viatico.Apellido,Viatico.Nombre,Viatico.Dni,Requerimiento.Correlativo,Requerimiento.Annio,Siaft.Siaf,Viatico.Annio,CompromisoPresupuestal.Memorando')
        ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.RequerimientoID = Viatico.RequerimientoID')
        ->innerJoin('Requerimiento','Requerimiento.ID = Viatico.RequerimientoID')
        ->innerJoin('Siaft','Siaft.Orden = Viatico.ID')
        ->where("Siaft.TipoGastoID = 4 AND Siaft.Fase = 10 AND CompromisoPresupuestal.Situacion = 21 AND Viatico.CodigoProyecto ='".$CodigoProyecto."' AND Viatico.ID NOT IN (SELECT ViaticoID FROM RendicionViatico WHERE ViaticoID = Viatico.ID)")
        ->all();

        // $listaRendicion = (new \yii\db\Query())
        //             ->select('Viatico.ID,Viatico.Apellido,Viatico.Nombre,Viatico.Dni,Siaft.Siaf')
        //             ->from('Viatico')
        //             ->innerJoin('Siaft','Siaft.Orden = Viatico.ID')
        //             ->where(['Siaft.Fase'=>10,'Siaft.TipoGastoID'=>4,'Siaft.Estado'=>1,'Viatico.CodigoProyecto'=>$CodigoProyecto,'Siaft.CodigoProyecto'=>$CodigoProyecto])
        //             ->distinct()
        //             ->all();

        $documento = (new \yii\db\Query())
            ->select('TipoDocumento.*')
            ->from('TipoDocumento')
            ->all();

        $usuario=Usuario::findOne(\Yii::$app->user->id);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;

        return $this->render('_rendicion',['CodigoProyecto'=>$CodigoProyecto,'viatico'=>$viatico,'documento'=>json_encode($documento)]);
    }


    // Obtener el total del detalle de viaticos

    public function actionCrearRendicion($CodigoProyecto=null){
        $this->layout='vacio';
        $viaticos=new RendicionViatico;
        
        // echo '<pre>';
        // print_r(Yii::$app->request->post());die();

        if( $viaticos->load( Yii::$app->request->post() ) ){
            $viaticos->Situacion=2;
            $viaticos->FechaRendicion=date('Y-m-d');
            
            // print_r($viaticos);
            // die();
            $viaticoAdd = $viaticos->save();
            
            // echo '<pre>';
            if($viaticoAdd == 1){
                
                // $viat=Rendicion::find()->where(['TransaccionID'=>$viaticos->ViaticoID])->one();
                // $viat->Situacion = 3;
                // $viat->update();

                // print_r($viaticos);die();

                if(!$viaticos->Codigos)
                {
                    $countCodigos=0;
                }
                else
                {
                    $countCodigos=count(($viaticos->Codigos));
                }
                
                for($i=0;$i<$countCodigos;$i++)
                {
                    if(isset($viaticos->Codigos[$i]))
                    {
                        $detallex=new DetalleRendicionViatico;
                        $detallex->RendicionID = $viaticos->ID;
                        $detallex->Fecha = Yii::$app->tools->dateFormat($viaticos->Fecha[$i],'Ymd');
                        $detallex->Clase = $viaticos->Clase[$i];
                        $detallex->Proveedor = $viaticos->Proveedor[$i];
                        $detallex->Detalle = $viaticos->Detalle[$i];
                        $detallex->Importe = str_replace(',','', $viaticos->Importe[$i]);
                        $detallex->FechaCreacion = date('Ymd');
                        $detallex->save();
                    }
                }
            }
            
            // return $this->redirect(['rendicion']);
            $arr = array('Success' => true);
            echo json_encode($arr);

        }
        die();
        
    }


    public function actionServicioReniec($valor=null)
    {
        return Yii::$app->tools->ServicioReniec($valor);
    }
}

