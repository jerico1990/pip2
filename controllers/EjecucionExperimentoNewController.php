<?php

namespace app\controllers;

use Yii;
use yii\web\UploadedFile;
use yii\web\Controller;

use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\LoginForm;

use app\models\Usuario;
use app\models\Persona;
use app\models\Investigador;
use app\models\Proyecto;
use app\models\InformacionGeneral;
use app\models\Poa;
use app\models\Componente;
use app\models\Actividad;
use app\models\EjecucionTecnica;
use app\models\ProgramacionTecnica;
use app\components\BNEjecucionTecnica;
use app\components\BNProgramacionTecnica;
use app\components\DAEjecucionTecnica;

class EjecucionExperimentoNewController extends Controller
{
    private $_BNProgTecnica;
    private $_DAProgTecnica;
	/**
     * @inheritdoc
     */
    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions(){
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout='estandar';
        
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $CodigoProyecto=$informacionGeneral->Codigo;

		return $this->render('index', ['CodigoProyecto'=>$CodigoProyecto]);
    }
	
	/**
     * Rescursos JSON 
     * @return [type] [description]
     */
    public function actionListaExperimentosJson(){
        $this->layout='vacio';
        
        $_DAEjecTecnica = new DAEjecucionTecnica();
		$experimentos = $_DAEjecTecnica->ConsultarXtipo('X');

		header('Content-Type: application/json');		
        echo json_encode($experimentos);
    }


    /**
     * Rescursos JSON 
     * @return [type] [description]
     */
    public function actionListaSustentosJson(){
        $this->layout='vacio';
        $IdEjecucioncionTecnica = Yii::$app->request->post('IdEjecucionTecnica');
        $_DAEjecTecnica = new DAEjecucionTecnica();
        $sustentos = $_DAEjecTecnica->SustentoPorEjecucion($IdEjecucioncionTecnica);
        $data = [];
        foreach ($sustentos as $key => $value) {
            $data[$key] = [
                'id'=>$value['IdAdjunto'], 
                'nombre'=>$value['IdEjecucionTecnica'].'-'.$value['IdAdjunto'].'-'.$value['NombreArchivo'],
                'ruta'=>$value['RutaArchivo'] 
            ];
        }
        header('Content-Type: application/json');       
        echo json_encode($data);
    }

    /**
     * Rescursos JSON 
     * @return [type] [description]
     */
    public function actionEliminarSustentosJson(){
        $this->layout='vacio';
        $IdsSustentos = Yii::$app->request->post('IdsSustentos');
        $_BNEjecTecnica = new BNEjecucionTecnica();
        $_Archivo = $_BNEjecTecnica->EliminaSustentos($IdsSustentos);
        foreach ($_Archivo as $key => $value) {
            if(rename(Yii::$app->basePath.$value['RutaArchivo'].$value['NombreArchivo'], Yii::$app->basePath.'/web/ejecuciontecnicasustento/borrar/'.$value['NombreArchivo']))
            {
                $experimentos[$key] = [
                    //'origen'=>Yii::$app->basePath.$value['RutaArchivo'].$value['NombreArchivo'],
                    //'destino'=>Yii::$app->basePath.'/web/ejecuciontecnicasustento/borrar/'.$value['NombreArchivo'],
                    'mensaje'=>"El fichero se movio a la papelera con éxito."
                ];
            } else {
                $experimentos[$key] = [
                    //'origen'=>Yii::$app->basePath.$value['RutaArchivo'].$value['NombreArchivo'],
                    //'destino'=>Yii::$app->basePath.'/web/ejecuciontecnicasustento/borrar/'.$value['NombreArchivo'],
                    'mensaje'=>'Hubo un error al mover el fichero a la papelera'
                ];
            }
        }
        header('Content-Type: application/json');       
        echo json_encode($experimentos);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionNuevo()
    {
        $this->layout='estandar';
        $CodigoProyecto = Yii::$app->request->post('CodigoProyecto');
        $informacionGeneral=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacionGeneral->InvestigadorID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
        $componentes=Componente::find()->where('PoaID=:PoaID',[':PoaID'=>$poa->ID])->all();

        $_ejecucion=new EjecucionTecnica();
        $_ejecucion->IdEjecucionTecnica = -1;
        $_ejecucion->IdProgramacionTecnica = -1;
        $_ejecucion->CodigoProyecto = '';
        return $this->render('editar', ['CodigoProyecto'=>$CodigoProyecto, 'componentes'=>$componentes, 'EjecucionTecnica'=>$_ejecucion]);
    }

    public function actionRegistrar()
    {
        $this->layout='vacio';
        $retVal = ['Estado'=>1, 'Data'=>[], 'Msj'=>''];
        $_ejecucion = new EjecucionTecnica();
        if($_ejecucion->load(Yii::$app->request->post())) {

            $_programacion = new ProgramacionTecnica();
            //$_programacion->load(Yii::$app->request->post());
            $_programacion->IdProgramacionTecnica = $_ejecucion->IdProgramacionTecnica;
            //$_programacion->IdProgramacionTecnica = $_ejecucion->Objetivo;
            $_programacion->ActividadID = $_ejecucion->ActividadID;
            $_programacion->Tipologia = $_ejecucion->Tipologia;
            $_programacion->IntroduccionDescripcion = $_ejecucion->IntroduccionDescripcion;
            $_programacion->LugarDesarrollo = $_ejecucion->LugarDesarrollo;
            $_programacion->FechaInicio = $_ejecucion->FechaInicio;
            $_programacion->FechaFin = $_ejecucion->FechaFin;
            $_BNProgTecnica = new BNProgramacionTecnica();
            $_BNProgTecnica->ActualizarDesdeEjecucion($_programacion);

            $_BNEjecTecnica = new BNEjecucionTecnica();

            if($_ejecucion->IdEjecucionTecnica == -1){
                $_ejecucion->Estado=1;
                $_ejecucion->Correlativo = 0;
            }           
                
            $_ejecucion = $_BNEjecTecnica->Registrar($_ejecucion);
            
            $retVal['Data'] = []; //['ejecucion'=>$_ejecucion, 'programacion'=>$_programacion];
            $retVal['Msj'] = 'La ejecución del experimento se guardó correctamente.';
        }
        header('Content-Type: application/json');    
        return json_encode($retVal);   
    }
    /**
     * Rescursos JSON 
     * @return [type] [description]
     */
    public function actionRegistrarSustento(){
        $this->layout='vacio';
        if($_FILES['Sustento']['name'])
        {
            $archivo = $_FILES['Sustento'];
            //$archivo = UploadedFile::getInstance($_FILES['Sustento']);
            $Codigo = $_POST['CodigoProyecto'];
            $IdEjecucionTecnica = $_POST['IdEjecucionTecnica'];
            $nombreArchivo = $Codigo.'-'.$archivo['name'];
            
            $_BNEjecTecnica = new BNEjecucionTecnica();
            $_idAdjunto = $_BNEjecTecnica->RegistrarSustento(-1, $IdEjecucionTecnica, 'ejecuciontecnicasustento/experimentos/',  $nombreArchivo);

            $nombreArchivo = $IdEjecucionTecnica.'-'.$_idAdjunto.'-'.str_replace(" ",'',$nombreArchivo);

            if(move_uploaded_file($archivo['tmp_name'], Yii::$app->basePath.'/web/ejecuciontecnicasustento/experimentos/'.$nombreArchivo)){

                $experimentos = [
                    'IdEjecucionTecnica' => $IdEjecucionTecnica,
                    'nombre' => $archivo['name'],
                    'tamaño' => $archivo['size'],
                    'tipo' => $archivo['type'],
                    'nombre_temporal' => $archivo['tmp_name'],
                    'nombreArchivo' => $nombreArchivo,
                    'mensaje'=>"El fichero es válido y se subió con éxito."
                ];
            } else {
                $experimentos = [
                    'error' => '¡Posible ataque de subida de ficheros!'
                ];
            }

        } else {
            $experimentos = [
                'error' => 'No se recuperaron las variables del POST'
            ];
        }
        header('Content-Type: application/json');       
        echo json_encode($experimentos);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionEditar()
    {
        $this->layout='estandar';
        $IdEjecucioncionTecnica = Yii::$app->request->post('IdEjecucionTecnica');
        $_ejecucion=new EjecucionTecnica();
        $DA_Ejecucion = new DAEjecucionTecnica();
        $_ejecucion = $DA_Ejecucion->Selecciona($IdEjecucioncionTecnica);

        $informacionGeneral=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$_ejecucion['CodigoProyecto']])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacionGeneral->InvestigadorID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
        $componentes=Componente::find()->where('PoaID=:PoaID',[':PoaID'=>$poa->ID])->all();

        return $this->render('editar', ['componentes'=>$componentes, 'EjecucionTecnica'=>$_ejecucion]);
    }

    public function actionAnular()
    {
        $this->layout='vacio';
        $retVal = ['Estado'=>1, 'Data'=>[], 'Msj'=>''];
        $_ejecucion=new ProgramacionTecnica();
        if($_ejecucion->load(Yii::$app->request->post())) {

            $usuario=Usuario::findOne(\Yii::$app->user->id);
            
            if($_ejecucion->IdProgramacionTecnica != -1){
                $_BNProgTecnica = new BNProgramacionTecnica();
                $_BNProgTecnica->Eliminar($_ejecucion->IdProgramacionTecnica);
            } 
            $retVal['Data'] = $_ejecucion;
            $retVal['Msj'] = 'El Experimento se eliminó correctamente.';
        }
        header('Content-Type: application/json');    
        return json_encode($retVal);   
    }

}