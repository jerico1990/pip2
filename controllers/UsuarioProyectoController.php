<?php

namespace app\controllers;

use Yii;
use app\models\UsuarioProyecto;
use app\models\UsuarioProyectoSearch;
use app\models\Usuario;
use app\models\InformacionGeneral;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UsuarioProyectoController implements the CRUD actions for UsuarioProyecto model.
 */
class UsuarioProyectoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UsuarioProyecto models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout='estandar';
        $searchModel = new UsuarioProyectoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UsuarioProyecto model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UsuarioProyecto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout='estandar';
        $model = new UsuarioProyecto();
        $usuarios=Usuario::find()
                ->select('Usuario.ID,Usuario.username')
                ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                ->where('RolID in (2,10)')
                ->all();
        $codigos=InformacionGeneral::find()->all();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'usuarios'=> $usuarios,
                'codigos'=> $codigos
            ]);
        }
    }
    
    public function actionCrear()
    {
        $this->layout='vacio';
        $model = new UsuarioProyecto();
        $usuarios=Usuario::find()
                ->select('Usuario.ID,Usuario.username')
                ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                ->all();
        $codigos=InformacionGeneral::find()->all();
        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            return $this->redirect(['/usuario-proyecto']);
        }
        
        return $this->render('crear', [
            'model' => $model,
            'usuarios'=> $usuarios,
            'codigos'=> $codigos
        ]);
    }

    /**
     * Updates an existing UsuarioProyecto model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->layout='estandar';
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UsuarioProyecto model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UsuarioProyecto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UsuarioProyecto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UsuarioProyecto::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionLista()
    {
        
        $asignados = (new \yii\db\Query())
            ->select('Persona.ApellidoPaterno,Persona.ApellidoMaterno,Persona.Nombre,InformacionGeneral.Codigo')
            ->from('UsuarioProyecto')
            ->innerJoin('Usuario','Usuario.ID=UsuarioProyecto.UsuarioID')
            ->innerJoin('Persona','Usuario.PersonaID=Persona.ID')
            ->innerJoin('InformacionGeneral','InformacionGeneral.Codigo=UsuarioProyecto.CodigoProyecto')
            //->innerJoin('Proyecto','Proyecto.ID = UsuarioProyecto.ProyectoID')
            //->where(['UsuarioRol.RolID'=>2])
            ->distinct()
            ->all();
        $nro=0;
        foreach($asignados as $asignado)
        {
            $nro++;

            echo "<tr>";
                echo "<td>" . $asignado["Nombre"] . "". $asignado["ApellidoPaterno"] ."". $asignado["ApellidoMaterno"] ."</td>";
                echo "<td> Proyecto " . $asignado["Codigo"] . "</td>";
                echo "<td></td>";
            echo "</tr>";
        }
    }
}
