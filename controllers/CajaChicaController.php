<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\PasoCritico;
use app\models\Usuario;
use app\models\Persona;
use app\models\Investigador;
use app\models\Proyecto;
use app\models\InformacionGeneral;
use app\models\Poa;
use app\models\Componente;
use app\models\Actividad;
use app\models\CronogramaProyecto;
use app\models\ActRubroElegible;
use app\models\CronogramaComponente;
use app\models\CronogramaActividad;
use app\models\CronogramaActRubroElegible;
use app\models\RubroElegible;
use app\models\AreSubCategoria;
use app\models\CronogramaAreSubCategoria;
use app\models\MarcoLogicoActividad;
use app\models\AreSubCategoriaObservacion;
use app\models\CertificacionPresupuestal;
use app\models\DetalleCertificacionPresupuestal;
use app\models\UnidadOperativa;
use app\models\SiafSecuencialProyecto;
use app\models\CajaChica;
use app\models\Orden;
use app\models\Siaf;
use app\models\Requerimiento;
use app\models\UnidadEjecutora;
use app\models\RendicionCajaChica;
use app\models\DetalleRendicionCajaChica;
use app\models\DetalleRequerimiento;
use app\models\DetalleCajaChica;


use PhpOffice\PhpWord\PhpWord;
use yii\web\UploadedFile;


class CajaChicaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($CodigoProyecto=null)
    {
        $this->layout='estandar';
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        return $this->render('index',['CodigoProyecto'=>$CodigoProyecto]);
    }
    
    
    public function actionLista($CodigoProyecto=null){
        
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        
        $resultados = (new \yii\db\Query())
            ->select('CajaChica.*')
            ->from('CajaChica')
            ->where(['CajaChica.CodigoProyecto'=>$CodigoProyecto])
            ->distinct()
            ->all();

        $nro=0;
        foreach($resultados as $result)
        {
            $nro++;
            if($result["Estado"]==1 )
            {
            echo "<tr>";
            echo "<td style='display:none'>" . $result["ID"] . "</td>";
            echo "<td>" . $result["Correlativo"] . "</td>";
            echo "<td>" . date('d/m/Y',strtotime($result["FechaRegistro"]))  . "</td>";
            echo "<td>" . $result["Descripcion"] . "</td>";
            echo "<td>" . $result["GastoCompraBienes"] . "</td>";
            echo "<td>" . $result["GastoCompraServicios"] . "</td>";
            echo "<td>" .( $result["GastoCompraBienes"] + $result["GastoCompraServicios"]). "</td>";
            echo "<td>" . $this->getSituacion($result["Situacion"]) . "</td>";
            echo "<td><a target='_blank' href='requerimientosCajaChica/" . $result["Documento"] . "'><span class='fa fa-cloud-download'></span></a></td>";
          /*
                echo "<td>";

                   if($result["Situacion"]==1){
                        echo "<a href='#' class='btn btn-info verifica-aprobar' data-id='".$result["ID"]."'>ENVIAR</a>";
                    };
               
                echo "<a href='#' class='btn-edit-requerimiento-caja-chica' data-id='".$result["ID"]."'><i class='fa fa-edit fa-lg'></i></a>
                      <a href='#' class='btn-remove' data-id='".$result["ID"]."'><i class='fa fa-remove fa-lg'></i></a> </td>";
               */
               echo "</tr>";
            }
           
            
           
        }
        
    }
    
   /* public function actionCrear($CodigoProyecto=null)
    {
        $this->layout='vacio';
        return $this->render('_form',['CodigoProyecto'=>$CodigoProyecto]);
    }*/

    public function actionCrear($CodigoProyecto=null,$RequerimientoID=null)
    {
        $this->layout='vacio';
        //$terminosReferencias=TerminoReferencia::find()->where('CodigoProyecto=:CodigoProyecto and Situacion=0',[':CodigoProyecto'=>$CodigoProyecto])->all();
        $CajaChica=new CajaChica;
        $requerimiento=Requerimiento::findOne($RequerimientoID);
        $CajaChicaActual=CajaChica::find()->where('CodigoProyecto=:CodigoProyecto and Estado=1 and Situacion=3',[':CodigoProyecto'=>$CodigoProyecto])->one();
        if($CajaChicaActual)
        {
            $detalleCajaChica=DetalleCajaChica::find()->where('Estado=1 and Situacion in (1,3) and CajaChicaID=:CajaChicaID',[':CajaChicaID'=>$CajaChicaActual->ID])->one();
            $rendiciones=RendicionCajaChica::find()->where('Situacion=3 and Estado=1 and CajaChicaID=:CajaChicaID',[':CajaChicaID'=>$detalleCajaChica->ID])->all();
            $tbienes=0;
            $tservicios=0;
            foreach($rendiciones as $rendicion)
            {
                $Bienes=DetalleRendicionCajaChica::find()->select('sum(Importe) Importe')->where('Tipo=1 and RendicionCajaChicaID=:RendicionCajaChicaID',[':RendicionCajaChicaID'=>$rendicion->ID])->one();
                $Servicios=DetalleRendicionCajaChica::find()->select('sum(Importe) Importe')->where('Tipo=2 and RendicionCajaChicaID=:RendicionCajaChicaID',[':RendicionCajaChicaID'=>$rendicion->ID])->one();
                $tbienes=$tbienes+$Bienes->Importe;
                $tservicios=$tservicios+$Servicios->Importe;
            }
            
            $CajaChica->Bienes=$tbienes;
            $CajaChica->Servicios=$tservicios;
            /*
            $CajaChica->Bienes=$detalleCajaChica->Bienes-$detalleCajaChica->SaldoActualBienes;
            $CajaChica->Servicios=$detalleCajaChica->Servicios-$detalleCajaChica->SaldoActualServicios;*/
        }
        else
        {
            $CajaChicaActual=new CajaChica;
        }
        
        if($CajaChica->load(Yii::$app->request->post())){
            // $GetOracleTipoCambios = \Yii::$app->db->createCommand("Select * from openquery(ORACLE,'SELECT FECH_CAMB_TIP,VALO_SOLE_TIP FROM TIPO_CAMBIO WHERE TO_CHAR(FECH_CAMB_TIP,''DD-MM-YYYY'')=TO_CHAR(SYSDATE,''DD-MM-YYYY'')')")->queryOne();
            
            if($CajaChica->Tipo==1){
                $aperturaCajaChicaPendiente=CajaChica::find()->where('CodigoProyecto=:CodigoProyecto and Annio=:Annio and Tipo=1 and Situacion=1 and Estado=1',[':CodigoProyecto'=>$CajaChica->CodigoProyecto,':Annio'=>date('Y')])->one();
                if($aperturaCajaChicaPendiente)
                {
                    \Yii::$app->getSession()->setFlash('error', '<div class="alert alert-dismissable alert-danger"><strong>Errores:</strong><ul> <li>Ya existe un requerimiento de Apertura de Caja Chica en pendiente.</li> </ul></div>');
                    return $this->redirect(['requerimiento/generar','ID'=>$RequerimientoID]);
                }
                $aperturaCajaChicaActivo=CajaChica::find()->where('CodigoProyecto=:CodigoProyecto and Annio=:Annio and Tipo=1 and Situacion in (2,5) and Estado=1',[':CodigoProyecto'=>$CajaChica->CodigoProyecto,':Annio'=>date('Y')])->one();
                if($aperturaCajaChicaActivo)
                {
                    \Yii::$app->getSession()->setFlash('error', '<div class="alert alert-dismissable alert-danger"><strong>Errores:</strong><ul> <li>Ya existe Apertura de Caja Chica del presente año</li> </ul></div>');
                    return $this->redirect(['requerimiento/generar','ID'=>$RequerimientoID]);
                }
                $CajaChica->Situacion=1;
                $CajaChica->Estado=1;
                $CajaChica->FechaRegistro=date('Ymd');
                $CajaChica->Correlativo=$this->CorrelativoCajaChica($CodigoProyecto);
                $CajaChica->Annio=date('Y');
                $CajaChica->save();
                
                $DetalleCajaChica=new DetalleCajaChica;
                $DetalleCajaChica->TipoCambio=1;
                // $DetalleCajaChica->TipoCambio=$GetOracleTipoCambios['VALO_SOLE_TIP'];
                $DetalleCajaChica->CajaChicaID=$CajaChica->ID;
                $DetalleCajaChica->Dni=$CajaChica->Dni;
                $DetalleCajaChica->Responsable=$CajaChica->Responsable;
                $DetalleCajaChica->Banco=$CajaChica->Banco;
                $DetalleCajaChica->Tipo=1;
                $DetalleCajaChica->CodigoProyecto=$CajaChica->CodigoProyecto;
                $DetalleCajaChica->CTA=$CajaChica->CTA;
                $DetalleCajaChica->CCI=$CajaChica->CCI;
                $DetalleCajaChica->Descripcion=$CajaChica->Descripcion;
                $DetalleCajaChica->Bienes=str_replace(',','', $CajaChica->Bienes);
                $DetalleCajaChica->Servicios=str_replace(',','', $CajaChica->Servicios);
                $DetalleCajaChica->SaldoActualBienes=str_replace(',','', $CajaChica->Bienes);
                $DetalleCajaChica->SaldoActualServicios=str_replace(',','', $CajaChica->Servicios);
                
                $CajaChica->SaldoActualBienes=$CajaChica->Bienes;
                $CajaChica->SaldoActualServicios=$CajaChica->Servicios;
                
                $DetalleCajaChica->RequerimientoID=$CajaChica->RequerimientoID;
                $DetalleCajaChica->NResolucionDirectorial=$CajaChica->NResolucionDirectorial;
                $DetalleCajaChica->Situacion=1;
                $DetalleCajaChica->Estado=1;
                $DetalleCajaChica->FechaRegistro=date('Ymd');
                $DetalleCajaChica->Correlativo=$this->CorrelativoDetalleCajaChica($CajaChica->ID);
                $DetalleCajaChica->Annio=date('Y');
                $DetalleCajaChica->save();
                
                $detalleRequerimiento=DetalleRequerimiento::find()->where('RequerimientoID=:RequerimientoID',[':RequerimientoID'=>$DetalleCajaChica->RequerimientoID])->one();
                $detalleRequerimiento->TipoDetalleOrdenID=$DetalleCajaChica->ID;
                $detalleRequerimiento->Situacion=3;
                $detalleRequerimiento->update();
                
                $CajaChica->archivo = UploadedFile::getInstance($CajaChica, 'archivo');
                
                if($CajaChica->archivo)
                {
                    $CajaChica->archivo->saveAs('cajachica/R' . $detalleRequerimiento->ID . '.' . $CajaChica->archivo->extension);
                    $CajaChica->ResolucionDirectorial='R'.$detalleRequerimiento->ID . '.' . $CajaChica->archivo->extension;
                    $DetalleCajaChica->ResolucionDirectorial='R'.$detalleRequerimiento->ID . '.' . $CajaChica->archivo->extension;
                }
                $CajaChica->update();
                $DetalleCajaChica->update();
            }
            elseif($CajaChica->Tipo==2)
            {
                $aperturaCajaChicaNoActivo=CajaChica::find()->where('CodigoProyecto=:CodigoProyecto and Annio=:Annio and Tipo=1 and Estado not in (0)',[':CodigoProyecto'=>$CajaChica->CodigoProyecto,':Annio'=>date('Y')])->one();
                if(!$aperturaCajaChicaNoActivo)
                {
                    \Yii::$app->getSession()->setFlash('error', '<div class="alert alert-dismissable alert-danger"><strong>Errores:</strong><ul> <li>No puede realizar reembolso, si no tiene apertura de Caja Chica</li> </ul></div>');
                    return $this->redirect(['requerimiento/generar','ID'=>$RequerimientoID]);
                }
                $DetalleCajaChica=new DetalleCajaChica;
                $DetalleCajaChica->CajaChicaID=$CajaChicaActual->ID;
                $DetalleCajaChica->Responsable=$CajaChica->Responsable;
                $DetalleCajaChica->Banco=$CajaChica->Banco;
                $DetalleCajaChica->Tipo=2;
                $DetalleCajaChica->CodigoProyecto=$CajaChica->CodigoProyecto;
                $DetalleCajaChica->CTA=$CajaChica->CTA;
                $DetalleCajaChica->CCI=$CajaChica->CCI;
                $DetalleCajaChica->Descripcion=$CajaChica->Descripcion;
                $DetalleCajaChica->Bienes=$CajaChica->Bienes;
                $DetalleCajaChica->Servicios=$CajaChica->Servicios;
                $DetalleCajaChica->SaldoActualBienes=$CajaChica->Bienes+$detalleCajaChica->SaldoActualBienes;
                $DetalleCajaChica->SaldoActualServicios=$CajaChica->Servicios+$detalleCajaChica->SaldoActualServicios;
                $DetalleCajaChica->SaldoAnteriorBienes=$detalleCajaChica->SaldoActualBienes;
                $DetalleCajaChica->SaldoAnteriorServicios=$detalleCajaChica->SaldoActualServicios;
                $DetalleCajaChica->RequerimientoID=$CajaChica->RequerimientoID;
                $DetalleCajaChica->NResolucionDirectorial=$CajaChica->NResolucionDirectorial;
                $DetalleCajaChica->Situacion=1;
                $DetalleCajaChica->Estado=1;
                $DetalleCajaChica->FechaRegistro=date('Ymd');
                $DetalleCajaChica->Correlativo=$this->CorrelativoDetalleCajaChica($CajaChicaActual->ID);
                $DetalleCajaChica->Annio=date('Y');
                $DetalleCajaChica->save();
                
                $detalleRequerimiento=DetalleRequerimiento::find()->where('RequerimientoID=:RequerimientoID',[':RequerimientoID'=>$DetalleCajaChica->RequerimientoID])->one();
                $detalleRequerimiento->TipoDetalleOrdenID=$DetalleCajaChica->ID;
                $detalleRequerimiento->Situacion=3;
                $detalleRequerimiento->update();
                
                $CajaChica->archivo = UploadedFile::getInstance($CajaChica, 'archivo');
                
                if($CajaChica->archivo)
                {
                    $CajaChica->archivo->saveAs('cajachica/R' . $detalleRequerimiento->ID . '.' . $CajaChica->archivo->extension);
                    $CajaChica->ResolucionDirectorial='R'.$detalleRequerimiento->ID . '.' . $CajaChica->archivo->extension;
                    $DetalleCajaChica->ResolucionDirectorial='R'.$detalleRequerimiento->ID . '.' . $CajaChica->archivo->extension;
                }
                $CajaChica->update();
                $DetalleCajaChica->update();
                
            }
            
            return $this->redirect(['requerimiento/generar','ID'=>$RequerimientoID]);
        }

        return $this->render('_form',['CodigoProyecto'=>$CodigoProyecto,'requerimiento'=>$requerimiento,'RequerimientoID'=>$RequerimientoID,'CajaChicaActual'=>$CajaChicaActual,'CajaChica'=>$CajaChica]);
    }

    public function actionActualizar($ID=null)
    {
        $this->layout='vacio';
        $CajaChica=CajaChica::findOne($ID);

        if($CajaChica->load(Yii::$app->request->post())){

            $CajaChica->save();
            $CajaChica->archivo = UploadedFile::getInstance($CajaChica, 'archivo');

            if($CajaChica->archivo)
            {
                $CajaChica->archivo->saveAs('cajachica/R' . $CajaChica->ID . '.' . $CajaChica->archivo->extension);
                $CajaChica->ResolucionDirectorial='R'.$CajaChica->ID . '.' . $CajaChica->archivo->extension;
            }
            $CajaChica->update();
            return $this->redirect(['requerimiento/generar','ID'=>$RequerimientoID]);
        }
        return $this->render('_form_actualizar',['CajaChica'=>$CajaChica,'ID'=>$ID]);
    }

    public function actionEliminar($id=null)
    {
        $detallecajaChica=DetalleCajaChica::findOne($id);
        if($detallecajaChica->Tipo==1)
        {
            $detallecajaChica->Estado=0;
            $detallecajaChica->Situacion=6;
            $detallecajaChica->update();
            
            $cajaChica=CajaChica::findOne($detallecajaChica->CajaChicaID);
            $cajaChica->Estado=0;
            $cajaChica->Situacion=6;
            $cajaChica->update();
        }
        elseif($detallecajaChica->Tipo==2)
        {
            $detallecajaChica->Estado=0;
            $detallecajaChica->Situacion=6;
            $detallecajaChica->update();
        }
        
        $arr = array('Success' => true);
        echo json_encode($arr);
    }

    public function actionEnviar($ID=null)
    {
        $requerimiento=CajaChica::findOne($_POST["ID"]);
        
        $requerimiento->Situacion=2;
        $requerimiento->update();

        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
     public function actionActualizarRequerimientoListado()
    {
        $this->layout='vacio';
        $requerimiento=CajaChica::findOne($_POST['ID']);
        $requerimiento->Situacion =6;
        $requerimiento->update();
        $arr = array('Success' => true);
        echo json_encode($arr);
    }

    public function CorrelativoCajaChica($CodigoProyecto)
    {
        $cajachica=CajaChica::find()->where('CodigoProyecto=:CodigoProyecto',[':CodigoProyecto'=>$CodigoProyecto])->orderBy('Correlativo desc')->one();
        if($cajachica)
        {
            return $cajachica->Correlativo+1;
        }
        else
        {
            return 1;
        }
    }
    public function CorrelativoDetalleCajaChica($ID)
    {
        $detallecajachica=DetalleCajaChica::find()->where('CajaChicaID=:CajaChicaID',[':CajaChicaID'=>$ID])->orderBy('Correlativo desc')->one();
        if($detallecajachica)
        {
            return $detallecajachica->Correlativo+1;
        }
        else
        {
            return 1;
        }
    }

    public function actionExcel($CodigoProyecto=null)
    {
        $resultados = (new \yii\db\Query())
            ->select('CajaChica.FechaRegistro')
            ->from('CajaChica')
            ->where(['CajaChica.CodigoProyecto'=>$CodigoProyecto])
            ->distinct()
            ->all();
        $requerimiento=new CajaChica;
        return $this->render('excel',['resultados'=>$resultados,'requerimiento'=>$requerimiento]);
    }




     public function getSituacion($situacion=null)
    {
        if($situacion==1){
            return "Pendiente";
        }
        elseif($situacion==2)
        {
            return "En Proceso";
        }
         elseif($situacion==3)
        {
            return "Aprobado";
        }
        else if($situacion==4)
        {
            return "Observado";
        }
        else if($situacion==5)
        {
            return "Cierre";
        }
        else if($situacion==6)
        {
            return "Anulado";
        }
    }

    public function actionPlantilla($ID=null)
    {
        $requerimiento=DetalleCajaChica::findOne($ID);
        $informacion=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$requerimiento->CodigoProyecto])->one();
        $eea=UnidadOperativa::find()->where('ID=:ID',[':ID'=>$informacion->UnidadOperativaID])->one();
        $ejecutora=UnidadEjecutora::find()->where('ID=:ID',[':ID'=>$eea->UnidadEjecutoraID])->one();
        $usuario=Usuario::find()->where('username=:username',[':username'=>$requerimiento->CodigoProyecto])->one();
        $persona=Persona::find()->where('ID=:ID',[':ID'=>$usuario->PersonaID])->one();
       
        
        require_once(Yii::$app->basePath . '/web/PHPWord/src/PhpWord/Autoloader.php');     
        \PhpOffice\PhpWord\Autoloader::register();
        $phpWord = new PhpWord();
       
        $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/documentos/PLANTILLA_CAJA_CHICA.docx');
        //$template->setValue('ID', str_pad($orden->Correlativo, 3, "0", STR_PAD_LEFT)."-".$orden->Annio);
        $template->setValue('EEA',$eea->Nombre);
        //$template->setValue('FECHAORDEN', date('d/m/Y',strtotime($orden->FechaOrden)));
        $template->setValue('CODIGOPROYECTO', $requerimiento->CodigoProyecto);
        $template->setValue('UNIDADEJECUTORA', $ejecutora->Nombre);
        
        $template->setValue('BIENES', number_format($requerimiento->Bienes, 2, '.', ' '));
        $template->setValue('SERVICIOS', number_format($requerimiento->Servicios, 2, '.', ' '));
        $template->setValue('TBIENES', number_format($requerimiento->Bienes, 2, '.', ' '));
        $template->setValue('TSERVICIOS', number_format($requerimiento->Servicios, 2, '.', ' '));
        $template->setValue('TTBISER', number_format(($requerimiento->Servicios+$requerimiento->Bienes), 2, '.', ' '));
        $temp_file = tempnam(sys_get_temp_dir(), 'PHPWord');
        $template->saveAs($temp_file);
        header("Content-Disposition: attachment; filename='CajaChica.docx'");
        readfile($temp_file); // or echo file_get_contents($temp_file);
        unlink($temp_file);  // remove temp file
        return true;    
    }
    
    
    public function actionPlantillaSolicitud($ID=null)
    {
        $rendicionCaja=DetalleCajaChica::findOne($ID);
        $informacion=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$rendicionCaja->CodigoProyecto])->one();
        $eea=UnidadOperativa::find()->where('ID=:ID',[':ID'=>$informacion->UnidadOperativaID])->one();
        $ejecutora=UnidadEjecutora::find()->where('ID=:ID',[':ID'=>$eea->UnidadEjecutoraID])->one();
        $usuario=Usuario::find()->where('username=:username',[':username'=>$rendicionCaja->CodigoProyecto])->one();
        $persona=Persona::find()->where('ID=:ID',[':ID'=>$usuario->PersonaID])->one();
        $rendicionCaja->Correlativo=str_pad($rendicionCaja->Correlativo, 3, "0", STR_PAD_LEFT) ."-".$rendicionCaja->Annio;
        
        require_once(Yii::$app->basePath . '/web/PHPWord/src/PhpWord/Autoloader.php');     
        \PhpOffice\PhpWord\Autoloader::register();
        $phpWord = new PhpWord();
       
        $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/documentos/PLANTILLA_SOLICITUD_COMPROMISO_PRESUPUESTAL_REEMBOLSO_CAJA.docx');
        //$template->setValue('ID', str_pad($orden->Correlativo, 3, "0", STR_PAD_LEFT)."-".$orden->Annio);
        $template->setValue('EEA',$eea->Nombre);
        //$template->setValue('FECHAORDEN', date('d/m/Y',strtotime($orden->FechaOrden)));
        $template->setValue('CODIGOPROYECTO', $rendicionCaja->CodigoProyecto);
        $template->setValue('UNIDADEJECUTORA', $ejecutora->Nombre);
        $template->setValue('IRP', $persona->Nombre.' '.$persona->ApellidoPaterno.' '.$persona->ApellidoMaterno);
        $template->setValue('CORRELATIVO', $rendicionCaja->Correlativo);
        $template->setValue('FECHAREGISTRO', date('d-m-Y',strtotime($rendicionCaja->FechaRegistro)));
        $template->setValue('BIENES', $rendicionCaja->Bienes);
        $template->setValue('SERVICIOS', $rendicionCaja->Servicios);
        $template->setValue('TOTAL', ($rendicionCaja->Servicios+$rendicionCaja->Bienes));
        $template->setValue('RESPONSABLE', $rendicionCaja->Responsable);
        $template->setValue('RESOLUCIONDIRECTORIAL', $rendicionCaja->NResolucionDirectorial);
        
        $tot=$rendicionCaja->Servicios+$rendicionCaja->Bienes;
        $tot = Yii::$app->tools->numtoletras($tot);
        $template->setValue('MONTOLETRAS', $tot);
        
        $temp_file = tempnam(sys_get_temp_dir(), 'PHPWord');
        $template->saveAs($temp_file);
        header("Content-Disposition: attachment; filename='CajaChica.docx'");
        readfile($temp_file); // or echo file_get_contents($temp_file);
        unlink($temp_file);  // remove temp file
        return true;    
    }
    
    public function actionAdjuntarDocumento($ID=null){
        $this->layout='vacio';
        $cajaChica=DetalleCajaChica::findOne($ID);
        $Requerimiento=Requerimiento::findOne($cajaChica->RequerimientoID);
        if($cajaChica->load(Yii::$app->request->post())){
            //$cajaChica->Situacion=3;
            $cajaChica->archivo = UploadedFile::getInstance($cajaChica, 'archivo');
            if($cajaChica->archivo)
            {
                $cajaChica->archivo->saveAs('cajachica/' . $cajaChica->ID . '.' . $cajaChica->archivo->extension);
                $cajaChica->Documento=$cajaChica->ID . '.' . $cajaChica->archivo->extension;
            }
            $cajaChica->update();
            return $this->redirect(['requerimiento/generar','ID'=>$Requerimiento->ID]);
        }
        return $this->render('adjuntar-documento',['ID'=>$ID,'cajaChica'=>$cajaChica]);
    }
    public function actionEliminarAdjunto($id=null)
    {
        $cajaChica=DetalleCajaChica::findOne($id);
        $cajaChica->Documento='';
        $cajaChica->Situacion=1;
        $cajaChica->update();
       
        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
    public function actionApertura($CodigoProyecto=null)
    {
        $this->layout='estandar';
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        return $this->render('apertura',['CodigoProyecto'=>$CodigoProyecto]);
    }
    
    
    public function actionListaApertura($CodigoProyecto=null){
        
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        
        $resultados = (new \yii\db\Query())
            ->select('AperturaCajaChica.*')
            ->from('AperturaCajaChica')
            ->where(['AperturaCajaChica.CodigoProyecto'=>$CodigoProyecto])
            ->distinct()
            ->all();

        $nro=0;
        foreach($resultados as $result)
        {
            $nro++;
            if($result["Estado"]==1 )
            {
            echo "<tr>";
            echo "<td style='display:none'>" . $result["ID"] . "</td>";
            echo "<td>" . $result["Correlativo"] . "</td>";
            echo "<td>" . date('d/m/Y',strtotime($result["FechaRegistro"]))  . "</td>";
            echo "<td>" . $result["Descripcion"] . "</td>";
            echo "<td>" . $result["Bienes"] . "</td>";
            echo "<td>" . $result["Servicios"] . "</td>";
            echo "<td>" .( $result["Bienes"] + $result["Servicios"]). "</td>";
            echo "<td>" . $this->getSituacion($result["Situacion"]) . "</td>";
            echo "<td><a target='_blank' href='cajachica/R" . $result["ResolucionDirectorial"] . "'><span class='fa fa-cloud-download'></span></a></td>";
            echo "</tr>";
            }
        }
    }
    
    
    public function actionCrearApertura($CodigoProyecto=null,$RequerimientoID=null)
    {
        $this->layout='vacio';
        //$terminosReferencias=TerminoReferencia::find()->where('CodigoProyecto=:CodigoProyecto and Situacion=0',[':CodigoProyecto'=>$CodigoProyecto])->all();
        $aperturaCaja=new AperturaCajaChica;
        $requerimiento=Requerimiento::findOne($RequerimientoID);
        if($aperturaCaja->load(Yii::$app->request->post())){
            
            $requerimientoCaja->Situacion=1;
            $requerimientoCaja->Estado=1;
            $requerimientoCaja->FechaRegistro=date('Ymd');
            $requerimientoCaja->Correlativo=$this->CorrelativoRequerimiento($CodigoProyecto);
          


            $requerimientoCaja->save();
            $requerimientoCaja->archivo = UploadedFile::getInstance($requerimientoCaja, 'archivo');
            
            if($requerimientoCaja->archivo)
            {
                $requerimientoCaja->archivo->saveAs('cajachica/R' . $requerimientoCaja->ID . '.' . $requerimientoCaja->archivo->extension);
                $requerimientoCaja->ResolucionDirectorial='R'.$requerimientoCaja->ID . '.' . $requerimientoCaja->archivo->extension;
            }
            $requerimientoCaja->update();
            
           
            
            return $this->redirect(['requerimiento/generar','ID'=>$RequerimientoID]);
        }

        return $this->render('_form_apertura',['CodigoProyecto'=>$CodigoProyecto,'requerimiento'=>$requerimiento,'RequerimientoID'=>$RequerimientoID]);
    }

    public function actionActualizarApertura($ID=null)
    {
        $this->layout='vacio';
        $requerimiento=CajaChica::findOne($ID);

        if($requerimiento->load(Yii::$app->request->post())){

            $requerimiento->save();
            $requerimiento->archivo = UploadedFile::getInstance($requerimiento, 'archivo');

            if($requerimiento->archivo)
            {
                $requerimiento->archivo->saveAs('cajachica/R' . $requerimiento->ID . '.' . $requerimiento->archivo->extension);
                $requerimiento->ResolucionDirectorial='R'.$requerimiento->ID . '.' . $requerimiento->archivo->extension;
            }
            $requerimiento->update();
            return $this->redirect(['requerimiento/generar','ID'=>$RequerimientoID]);
        }
        return $this->render('_form_actualizar_apertura',['requerimiento'=>$requerimiento,'ID'=>$ID]);
    }

    public function actionEliminarApertura($id=null)
    {
        $cajaChica=AperturaCajaChica::findOne($id);
        $cajaChica->Estado=0; 
        $cajaChica->update();

        $arr = array('Success' => true);
        echo json_encode($arr);
    }
}
