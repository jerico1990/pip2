<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\PasoCritico;
use app\models\Usuario;
use app\models\Persona;
use app\models\Investigador;
use app\models\Proyecto;
use app\models\InformacionGeneral;
use app\models\Poa;
use app\models\Componente;
use app\models\Actividad;
use app\models\CronogramaProyecto;
use app\models\ActRubroElegible;
use app\models\CronogramaComponente;
use app\models\CronogramaActividad;
use app\models\CronogramaActRubroElegible;
use app\models\RubroElegible;
use app\models\AreSubCategoria;
use app\models\CronogramaAreSubCategoria;
use app\models\MarcoLogicoActividad;
use app\models\AreSubCategoriaObservacion;
use app\models\InformeTecnicoFinanciero;
use app\models\Hito;
use app\models\EjecucionMetaFisicaFinanciera;
use app\models\EntidadParticipante;
use app\models\EjecucionFinancieraColaboradora;
use app\models\MedidaSocioAmbiental;
use app\models\MarcoLogicoComponente;
use app\models\CronogramaTarea;
use app\models\MarcoLogicoFinProyecto;
use app\models\MarcoLogicoPropositoProyecto;
use app\models\ProduccionResiduoSolido;
use app\models\ConsumoAgua;
use app\models\ConsumoCombustible;

use app\models\EquidadInclusion;
use app\models\ListaIncidencia;
use app\models\ListaParticipativa;

use PhpOffice\PhpWord\Writer\PDF;
use yii\web\UploadedFile;

class InformeTecnicoFinancieroController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($PasoCriticoID=NULL)
    {
        $this->layout='estandar';
        $model=InformeTecnicoFinanciero::find()->where('PasoCriticoID=:PasoCriticoID',[':PasoCriticoID'=>$PasoCriticoID])->one();
        
        if(!$model)
        {
            $model=new InformeTecnicoFinanciero;
            
        }
        else
        {
            $hito=Hito::find()->where('InformeTecnicoFinancieroID=:InformeTecnicoFinancieroID',[':InformeTecnicoFinancieroID'=>$model->ID])->one();
            if($hito)
            {
                $model->TituloHito=$hito->Titulo;
                $model->DescripcionHito=$hito->Descripcion;
                $model->PeriodoInicioHito=date('Y-m-d',strtotime($hito->PeriodoInicio));
                $model->PeriodoFinHito=date('Y-m-d',strtotime($hito->PeriodoFin));
                $model->DescripcionCumplio=$hito->DescripcionCumplio;
                $model->EjecucionFinanciera=$hito->EjecucionFinanciera;
                $model->Observacion=$hito->Observacion;
                $model->Logros=$hito->Logros;
            }
        }
        $pasoCritico=PasoCritico::findOne($PasoCriticoID);
       
        $proyecto=Proyecto::findOne($pasoCritico->ProyectoID);
        $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$proyecto->InvestigadorID])->one();
        /*$componentes=Componente::find()
                            ->select('Componente.*')
                            ->innerJoin('Poa','Poa.ID=Componente.PoaID')
                            ->where('Poa.ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])
                            ->all();*/
        $componentes = Componente::find()
                ->select('Componente.ID,Componente.Nombre,Componente.Correlativo')
                ->innerJoin('Poa','Poa.ID=Componente.PoaID')
                ->innerJoin('Actividad','Actividad.ComponenteID=Componente.ID')
                ->innerJoin('ActRubroElegible','ActRubroElegible.ActividadID=Actividad.ID')
                ->innerJoin('AreSubCategoria','AreSubCategoria.ActRubroElegibleID=ActRubroElegible.ID')
                ->innerJoin('CronogramaAreSubCategoria','CronogramaAreSubCategoria.AreSubCategoriaID=AreSubCategoria.ID')
                ->where('Componente.Estado=:Estado and Poa.ProyectoID=:ProyectoID and CronogramaAreSubCategoria.MetaFisica!=0',[':Estado' => 1, ':ProyectoID' => $proyecto->ID])
                ->andWhere(['between', 'CronogramaAreSubCategoria.Mes', $pasoCritico->MesInicio, $pasoCritico->MesFin])
                ->groupBy('Componente.ID,Componente.Nombre,Componente.Correlativo')
                ->orderBy('Componente.Correlativo asc')
                ->all();
                
        $colaboradoras=EntidadParticipante::find()->where('InvestigadorID=:InvestigadorID and TipoInstitucionID not in (3)',[':InvestigadorID'=>$proyecto->InvestigadorID])->all();
        $medidas=MedidaSocioAmbiental::find()->where('InformeTecnicoFinancieroID=:InformeTecnicoFinancieroID',[':InformeTecnicoFinancieroID'=>$model->ID])->all();
        
        $equidades=EquidadInclusion::find()->where('InformeTecnicoFinancieroID=:InformeTecnicoFinancieroID and Tipo=1',[':InformeTecnicoFinancieroID'=>$model->ID])->all();
        $inclusiones=EquidadInclusion::find()->where('InformeTecnicoFinancieroID=:InformeTecnicoFinancieroID and Tipo=2',[':InformeTecnicoFinancieroID'=>$model->ID])->all();
        $incidencias=ListaIncidencia::find()->where('InformeTecnicoFinancieroID=:InformeTecnicoFinancieroID',[':InformeTecnicoFinancieroID'=>$model->ID])->all();
        $participativas=ListaParticipativa::find()->where('InformeTecnicoFinancieroID=:InformeTecnicoFinancieroID',[':InformeTecnicoFinancieroID'=>$model->ID])->all();
        $equidadesinclusiones=EquidadInclusion::find()->where('InformeTecnicoFinancieroID=:InformeTecnicoFinancieroID',[':InformeTecnicoFinancieroID'=>$model->ID])->all();
        $bandera=$pasoCritico->MesInicio;
        for($i=1;$i<=($pasoCritico->MesFin-$pasoCritico->MesInicio+1);$i++){
            
            $npMes=ProduccionResiduoSolido::find()
                    ->where('InformeTecnicoFinancieroID=:InformeTecnicoFinancieroID and Tipo=1 and Mes=:Mes',
                            [':InformeTecnicoFinancieroID'=>$model->ID,':Mes'=>$bandera])->one();
            
            if($npMes){
                $model->{'NPMes'.$i}=$npMes->Cantidad;
            }
            
            $pMes=ProduccionResiduoSolido::find()
                        ->where('InformeTecnicoFinancieroID=:InformeTecnicoFinancieroID and Tipo=2 and Mes=:Mes',
                                [':InformeTecnicoFinancieroID'=>$model->ID,':Mes'=>$bandera])->one();
            if($pMes){
                $model->{'PMes'.$i}=$pMes->Cantidad;
            }
            
            $ccMes=ConsumoCombustible::find()
                        ->where('InformeTecnicoFinancieroID=:InformeTecnicoFinancieroID and Tipo=1 and Mes=:Mes',
                                [':InformeTecnicoFinancieroID'=>$model->ID,':Mes'=>$bandera])->one();
            if($ccMes){
                $model->{'CCMes'.$i}=$ccMes->Cantidad;
            }
            
            $CAMes=ConsumoAgua::find()
                        ->where('InformeTecnicoFinancieroID=:InformeTecnicoFinancieroID and Tipo=1 and Mes=:Mes',
                                [':InformeTecnicoFinancieroID'=>$model->ID,':Mes'=>$bandera])->one();
            if($CAMes){
                $model->{'CAMes'.$i}=$CAMes->Cantidad;
            }
                
            $bandera++;
        }
        
        if($model->load(Yii::$app->request->post()))
        {
            $model->FechaDesembolsoPNIA=date('Ymd h:m:s',strtotime($model->FechaDesembolsoPNIA));
            $model->FechaInforme=date('Ymd h:m:s',strtotime($model->FechaInforme));
            $model->PasoCriticoID=$PasoCriticoID;
            $model->CodigoProyecto=$informacionGeneral->Codigo;
            $model->save();
            $hito=Hito::find()->where('InformeTecnicoFinancieroID=:InformeTecnicoFinancieroID',[':InformeTecnicoFinancieroID'=>$model->ID])->one();
            
            if(!$hito)
            {
                $hito=new Hito;
                $hito->InformeTecnicoFinancieroID=$model->ID;
                $hito->Titulo=$model->TituloHito;
                $hito->Descripcion=$model->DescripcionHito;
                $hito->PeriodoInicio=date('Ymd h:m:s',strtotime($model->PeriodoInicioHito));
                $hito->PeriodoFin=date('Ymd h:m:s',strtotime($model->PeriodoFinHito));
                $hito->DescripcionCumplio=$model->DescripcionCumplio;
                $hito->EjecucionFinanciera=$model->EjecucionFinanciera;
                $hito->Observacion=$model->Observacion;
                $hito->Logros=$model->Logros;
                $hito->save();
            }
            else
            {
                $hito->Titulo=$model->TituloHito;
                $hito->Descripcion=$model->DescripcionHito;
                $hito->PeriodoInicio=date('Ymd h:m:s',strtotime($model->PeriodoInicioHito));
                $hito->PeriodoFin=date('Ymd h:m:s',strtotime($model->PeriodoFinHito));
                $hito->DescripcionCumplio=$model->DescripcionCumplio;
                $hito->EjecucionFinanciera=$model->EjecucionFinanciera;
                $hito->Observacion=$model->Observacion;
                $hito->Logros=$model->Logros;
                $hito->update();
            }
            
            if(!$model->EjecucionMetaFisicaFinancieraDescripciones)
            {
                $countDetallesDescripciones=0;
            }
            else
            {
                $countDetallesDescripciones=count(array_filter($model->EjecucionMetaFisicaFinancieraDescripciones));
            }
            
            for($i=0;$i<$countDetallesDescripciones;$i++)
            {
                if(isset($model->EjecucionMetaFisicaFinancieraIDs[$i]) && $model->EjecucionMetaFisicaFinancieraIDs[$i]!='')
                {
                    $detalle=EjecucionMetaFisicaFinanciera::findOne($model->EjecucionMetaFisicaFinancieraIDs[$i]);
                    $detalle->Descripcion=$model->EjecucionMetaFisicaFinancieraDescripciones[$i];
                    $detalle->update();
                }
                else
                {
                    $detalle=new EjecucionMetaFisicaFinanciera;
                    $detalle->InformeTecnicoFinancieroID=$model->ID;
                    $detalle->ActividadID=$model->EjecucionMetaFisicaFinancieraActividadesIDs[$i];
                    $detalle->Descripcion=$model->EjecucionMetaFisicaFinancieraDescripciones[$i];
                    $detalle->save();
                }
            }
            
            if(!$model->ColaboradoraMetasProgramadas)
            {
                $countDetallesColaboradoras=0;
            }
            else
            {
                $countDetallesColaboradoras=count(array_filter($model->ColaboradoraMetasProgramadas));
            }
            
            for($i=0;$i<$countDetallesColaboradoras;$i++)
            {
                if(isset($model->EjecucionColaboradoraIDs[$i]) && $model->EjecucionColaboradoraIDs[$i]!='')
                {
                    $detalle=EjecucionFinancieraColaboradora::find()->where('ColaboradoraID=:ColaboradoraID and InformeTecnicoFinancieroID=:InformeTecnicoFinancieroID',[':ColaboradoraID'=>$model->ColaboradoraIDs[$i],':InformeTecnicoFinancieroID'=>$model->ID])->one(); 
                    $detalle->CodigoProyecto=$informacionGeneral->Codigo;
                    $detalle->ColaboradoraID=$model->ColaboradoraIDs[$i];
                    $detalle->ActividadDetalle=$model->ColaboradoraActividadesDetalles[$i];
                    $detalle->TipoAporte=$model->ColaboradoraTipos[$i];
                    $detalle->MetaTotalProgramado=$model->ColaboradoraMetasProgramadas[$i];
                    $detalle->MetaSemestralEjecutada=$model->ColaboradoraMetasEjecutadas[$i];
                    $detalle->Avance=$model->ColaboradoraAvances[$i];
                    $detalle->DetalleAporte=$model->ColaboradoraDetalles[$i];
                    $detalle->update();
                }
                else
                {
                    $detalle=new EjecucionFinancieraColaboradora;
                    $detalle->InformeTecnicoFinancieroID=$model->ID;
                    $detalle->CodigoProyecto=$informacionGeneral->Codigo;
                    $detalle->ColaboradoraID=$model->ColaboradoraIDs[$i];
                    $detalle->TipoAporte=$model->ColaboradoraTipos[$i];
                    $detalle->MetaTotalProgramado=$model->ColaboradoraMetasProgramadas[$i];
                    $detalle->MetaSemestralEjecutada=$model->ColaboradoraMetasEjecutadas[$i];
                    $detalle->Avance=$model->ColaboradoraAvances[$i];
                    $detalle->DetalleAporte=$model->ColaboradoraDetalles[$i];
                    $detalle->save();
                }
            }
            
            
            if(!$model->MedidasSocioAmbientales)
            {
                $countDetallesSalvaguardas=0;
            }
            else
            {
                $countDetallesSalvaguardas=count(array_filter($model->MedidasSocioAmbientales));
            }
            
            for($i=0;$i<$countDetallesSalvaguardas;$i++)
            {
                if(isset($model->MedidasSocioAmbientalesIDs[$i]) && $model->MedidasSocioAmbientalesIDs[$i]!='')
                {
                    $detalle=MedidaSocioAmbiental::findOne($model->MedidasSocioAmbientalesIDs[$i]);
                    $detalle->Descripcion=$model->MedidasSocioAmbientales[$i];
                    $detalle->update();
                }
                else
                {
                    $detalle=new MedidaSocioAmbiental;
                    $detalle->InformeTecnicoFinancieroID=$model->ID;
                    $detalle->Descripcion=$model->MedidasSocioAmbientales[$i];
                    $detalle->save();
                }
            }
            
            if(!$model->ParticipativaTemas)
            {
                $countParticipativas=0;
            }
            else
            {
                $countParticipativas=count(array_filter($model->ParticipativaTemas));
            }
            
            for($i=0;$i<$countParticipativas;$i++)
            {
                if(isset($model->ParticipativaIDs[$i]) && $model->ParticipativaIDs[$i]!='')
                {
                    $detalle=ListaParticipativa::findOne($model->ParticipativaIDs[$i]);
                    $detalle->Fecha=date('Ymd h:m:s',strtotime($model->ParticipativaFechas[$i]));
                    $detalle->Tema=$model->ParticipativaTemas[$i];
                    $detalle->NumeroPersonas=$model->ParticipativaNumerosPersonas[$i];
                    $detalle->Comunidad=$model->ParticipativaComunidades[$i];
                    $detalle->Lugar=$model->ParticipativaLugares[$i];
                    $detalle->Tiempo=$model->ParticipativaTiempos[$i];
                    $detalle->HorasHombre=$model->ParticipativaHorasHombres[$i];
                    $detalle->update();
                }
                else
                {
                    $detalle=new ListaParticipativa;
                    $detalle->InformeTecnicoFinancieroID=$model->ID;
                    $detalle->Fecha=date('Ymd h:m:s',strtotime($model->ParticipativaFechas[$i]));
                    $detalle->Tema=$model->ParticipativaTemas[$i];
                    $detalle->NumeroPersonas=$model->ParticipativaNumerosPersonas[$i];
                    $detalle->Comunidad=$model->ParticipativaComunidades[$i];
                    $detalle->Lugar=$model->ParticipativaLugares[$i];
                    $detalle->Tiempo=$model->ParticipativaTiempos[$i];
                    $detalle->HorasHombre=$model->ParticipativaHorasHombres[$i];
                    $detalle->save();
                }
            }
            
            if(!$model->IncidenciaMedidasCorrectivas)
            {
                $countIncidencias=0;
            }
            else
            {
                $countIncidencias=count(array_filter($model->IncidenciaMedidasCorrectivas));
            }
            
            for($i=0;$i<$countIncidencias;$i++)
            {
                if(isset($model->IncidenciaIDs[$i]) && $model->IncidenciaIDs[$i]!='')
                {
                    $detalle=ListaIncidencia::findOne($model->IncidenciaIDs[$i]);
                    $detalle->Tipo=$model->IncidenciaTipos[$i];
                    $detalle->Fecha=date('Ymd h:m:s',strtotime($model->IncidenciaFechas[$i]));
                    $detalle->MedidaCorrectiva=$model->IncidenciaMedidasCorrectivas[$i];
                    $detalle->MedidaPreventiva=$model->IncidenciaMedidasPreventivas[$i];
                    $detalle->NumeroInvolucrados=$model->IncidenciaNumerosInvolucrados[$i];
                    $detalle->DanoPersona=$model->IncidenciaDanosPersonas[$i];
                    $detalle->Costo=$model->IncidenciaCostos[$i];
                    $detalle->update();
                }
                else
                {
                    $detalle=new ListaIncidencia;
                    $detalle->InformeTecnicoFinancieroID=$model->ID;
                    $detalle->Tipo=$model->IncidenciaTipos[$i];
                    $detalle->Fecha=$model->IncidenciaFechas[$i];
                    $detalle->MedidaCorrectiva=$model->IncidenciaMedidasCorrectivas[$i];
                    $detalle->MedidaPreventiva=$model->IncidenciaMedidasPreventivas[$i];
                    $detalle->NumeroInvolucrados=$model->IncidenciaNumerosInvolucrados[$i];
                    $detalle->DanoPersona=$model->IncidenciaDanosPersonas[$i];
                    $detalle->Costo=$model->IncidenciaCostos[$i];
                    $detalle->save();
                }
            }
            
            
            if(!$model->EquidadInclusionDescripciones)
            {
                $countEquidadInclusion=0;
            }
            else
            {
                $countEquidadInclusion=count(array_filter($model->EquidadInclusionDescripciones));
            }
            
            for($i=0;$i<$countEquidadInclusion;$i++)
            {
                if(isset($model->EquidadInclusionIDs[$i]) && $model->EquidadInclusionIDs[$i]!='')
                {
                    $detalle=EquidadInclusion::findOne($model->EquidadInclusionIDs[$i]);
                    $detalle->Tipo=$model->EquidadInclusionTipos[$i];
                    $detalle->Descripcion=$model->EquidadInclusionDescripciones[$i];
                    $detalle->Hombres=$model->EquidadInclusionHombres[$i];
                    $detalle->Mujeres=$model->EquidadInclusionMujeres[$i];
                    $detalle->update();
                }
                else
                {
                    $detalle=new EquidadInclusion;
                    $detalle->InformeTecnicoFinancieroID=$model->ID;
                    $detalle->Tipo=$model->EquidadInclusionTipos[$i];
                    $detalle->Descripcion=$model->EquidadInclusionDescripciones[$i];
                    $detalle->Hombres=$model->EquidadInclusionHombres[$i];
                    $detalle->Mujeres=$model->EquidadInclusionMujeres[$i];
                    $detalle->save();
                }
            }
            
            
            $bandera=$pasoCritico->MesInicio;
            for($i=1;$i<=($pasoCritico->MesFin-$pasoCritico->MesInicio+1);$i++){
                $npMes=ProduccionResiduoSolido::find()
                        ->where('InformeTecnicoFinancieroID=:InformeTecnicoFinancieroID and Tipo=1 and Mes=:Mes',
                                [':InformeTecnicoFinancieroID'=>$model->ID,':Mes'=>$model->{'NPMesID'.$i}])->one();
                if($npMes){
                    $npMes->Cantidad=$model->{'NPMes'.$i};
                    $npMes->update();
                }
                else
                {
                    $npMes=new ProduccionResiduoSolido;
                    $npMes->Mes=$model->{'NPMesID'.$i};
                    $npMes->Cantidad=$model->{'NPMes'.$i};
                    $npMes->Tipo=1;
                    $npMes->FechaRegistro=date('Ymd');
                    $npMes->Anno=date('Y');
                    $npMes->InformeTecnicoFinancieroID=$model->ID;
                    $npMes->save();
                }
                
                
                $pMes=ProduccionResiduoSolido::find()
                        ->where('InformeTecnicoFinancieroID=:InformeTecnicoFinancieroID and Tipo=2 and Mes=:Mes',
                                [':InformeTecnicoFinancieroID'=>$model->ID,':Mes'=>$model->{'PMesID'.$i}])->one();
                if($pMes){
                    //$pMes->Mes=$model->{'PMesID'.$i};
                    $pMes->Cantidad=$model->{'PMes'.$i};
                    $pMes->update();
                }
                else
                {
                    $pMes=new ProduccionResiduoSolido;
                    $pMes->Mes=$model->{'PMesID'.$i};
                    $pMes->Cantidad=$model->{'PMes'.$i};
                    $pMes->Tipo=2;
                    $pMes->FechaRegistro=date('Ymd');
                    $pMes->Anno=date('Y');
                    $pMes->InformeTecnicoFinancieroID=$model->ID;
                    $pMes->save();
                }
                
                
                
                $ccMes=ConsumoCombustible::find()
                        ->where('InformeTecnicoFinancieroID=:InformeTecnicoFinancieroID and Tipo=1 and Mes=:Mes',
                                [':InformeTecnicoFinancieroID'=>$model->ID,':Mes'=>$model->{'CCMesID'.$i}])->one();
                if($ccMes){
                    //$ccMes->Mes=$model->{'CCMesID'.$i};
                    $ccMes->Cantidad=$model->{'CCMes'.$i};
                    $ccMes->update();
                }
                else
                {
                    $ccMes=new ConsumoCombustible;
                    $ccMes->Mes=$model->{'CCMesID'.$i};
                    $ccMes->Cantidad=$model->{'CCMes'.$i};
                    $ccMes->Tipo=1;
                    $ccMes->FechaRegistro=date('Ymd');
                    $ccMes->Anno=date('Y');
                    $ccMes->InformeTecnicoFinancieroID=$model->ID;
                    $ccMes->save();
                }
                
                
                
                $CAMes=ConsumoAgua::find()
                        ->where('InformeTecnicoFinancieroID=:InformeTecnicoFinancieroID and Tipo=1 and Mes=:Mes',
                                [':InformeTecnicoFinancieroID'=>$model->ID,':Mes'=>$model->{'CAMesID'.$i}])->one();
                if($CAMes){
                    //$CAMes->Mes=$model->{'CAMesID'.$i};
                    $CAMes->Cantidad=$model->{'CAMes'.$i};
                    $CAMes->update();
                }
                else
                {
                    $CAMes=new ConsumoAgua;
                    $CAMes->Mes=$model->{'CAMesID'.$i};
                    $CAMes->Cantidad=$model->{'CAMes'.$i};
                    $CAMes->Tipo=1;
                    $CAMes->FechaRegistro=date('Ymd');
                    $CAMes->Anno=date('Y');
                    $CAMes->InformeTecnicoFinancieroID=$model->ID;
                    $CAMes->save();
                }
                $bandera++; 
            }
            
            return $this->refresh();
        }
        return $this->render('index',['PasoCriticoID'=>$PasoCriticoID,'informacionGeneral'=>$informacionGeneral,'model'=>$model,'componentes'=>$componentes,'colaboradoras'=>$colaboradoras,'medidas'=>$medidas,'pasoCritico'=>$pasoCritico,
                                      'equidades'=>$equidades,'inclusiones'=>$inclusiones,'incidencias'=>$incidencias,'participativas'=>$participativas,'equidadesinclusiones'=>$equidadesinclusiones]);
    }
    
    
    public function actionAdjuntarDocumento($PasoCriticoID=null,$Tipo=null)
    {
        $this->layout='vacio';
        $adjuntar=InformeTecnicoFinanciero::find()->where('PasoCriticoID=:PasoCriticoID',[':PasoCriticoID'=>$PasoCriticoID])->one();
        if(!$adjuntar)
        {
            $adjuntar=new InformeTecnicoFinanciero;
        }
        if($adjuntar->load(Yii::$app->request->post())){
            
            $adjuntar->archivo = UploadedFile::getInstance($adjuntar, 'archivo');
            if($adjuntar->archivo)
            {
                if($Tipo==1)
                {
                    $adjuntar->archivo->saveAs('informetecnicofinanciero/anexo1_' . $adjuntar->ID . '.' . $adjuntar->archivo->extension);
                    $adjuntar->Anexo1='anexo1_'.$adjuntar->ID . '.' . $adjuntar->archivo->extension;
                }
                elseif($Tipo==2)
                {
                    $adjuntar->archivo->saveAs('informetecnicofinanciero/anexo2_' . $adjuntar->ID . '.' . $adjuntar->archivo->extension);
                    $adjuntar->Anexo2='anexo2_'.$adjuntar->ID . '.' . $adjuntar->archivo->extension;
                }
                elseif($Tipo==3)
                {
                    $adjuntar->archivo->saveAs('informetecnicofinanciero/anexo3_' . $adjuntar->ID . '.' . $adjuntar->archivo->extension);
                    $adjuntar->Anexo3='anexo3_'.$adjuntar->ID . '.' . $adjuntar->archivo->extension;
                }
                elseif($Tipo==4)
                {
                    $adjuntar->archivo->saveAs('informetecnicofinanciero/anexo4_' . $adjuntar->ID . '.' . $adjuntar->archivo->extension);
                    $adjuntar->Anexo4='anexo4_'.$adjuntar->ID . '.' . $adjuntar->archivo->extension;
                }
                elseif($Tipo==5)
                {
                    $adjuntar->archivo->saveAs('informetecnicofinanciero/anexo5_' . $adjuntar->ID . '.' . $adjuntar->archivo->extension);
                    $adjuntar->Anexo5='anexo5_'.$adjuntar->ID . '.' . $adjuntar->archivo->extension;
                }
                elseif($Tipo==6)
                {
                    $adjuntar->archivo->saveAs('informetecnicofinanciero/anexo6_' . $adjuntar->ID . '.' . $adjuntar->archivo->extension);
                    $adjuntar->Anexo6='anexo6_'.$adjuntar->ID . '.' . $adjuntar->archivo->extension;
                }
                elseif($Tipo==7)
                {
                    $adjuntar->archivo->saveAs('informetecnicofinanciero/anexo7_' . $adjuntar->ID . '.' . $adjuntar->archivo->extension);
                    $adjuntar->Anexo7='anexo7_'.$adjuntar->ID . '.' . $adjuntar->archivo->extension;
                }
                elseif($Tipo==8)
                {
                    $adjuntar->archivo->saveAs('informetecnicofinanciero/anexo8_' . $adjuntar->ID . '.' . $adjuntar->archivo->extension);
                    $adjuntar->Anexo8='anexo8_'.$adjuntar->ID . '.' . $adjuntar->archivo->extension;
                }
                elseif($Tipo==9)
                {
                    $adjuntar->archivo->saveAs('informetecnicofinanciero/anexo9_' . $adjuntar->ID . '.' . $adjuntar->archivo->extension);
                    $adjuntar->Anexo9='anexo9_'.$adjuntar->ID . '.' . $adjuntar->archivo->extension;
                }
                elseif($Tipo==10)
                {
                    $adjuntar->archivo->saveAs('informetecnicofinanciero/anexo10_' . $adjuntar->ID . '.' . $adjuntar->archivo->extension);
                    $adjuntar->Anexo10='anexo10_'.$adjuntar->ID . '.' . $adjuntar->archivo->extension;
                }
                elseif($Tipo==11)
                {
                    $adjuntar->archivo->saveAs('informetecnicofinanciero/anexo11_' . $adjuntar->ID . '.' . $adjuntar->archivo->extension);
                    $adjuntar->Anexo11='anexo11_'.$adjuntar->ID . '.' . $adjuntar->archivo->extension;
                }
                
            }
            $adjuntar->update();
            return $this->redirect(['/informe-tecnico-financiero/index','PasoCriticoID'=>$PasoCriticoID]);

        }
        return $this->render('adjuntar-documento',['adjuntar'=>$adjuntar,'PasoCriticoID'=>$PasoCriticoID,'Tipo'=>$Tipo]);
    }
    
    public function actionEliminarAdjunto($id=null,$Tipo=null,$PasoCriticoID=null)
    {
        $informe=InformeTecnicoFinanciero::findOne($id);
        if($Tipo==1)
        {
            $informe->Anexo1='';
        }
        elseif($Tipo==2)
        {
            $informe->Anexo2='';
        }
        elseif($Tipo==3)
        {
            $informe->Anexo3='';
        }
        elseif($Tipo==4)
        {
            $informe->Anexo4='';
        }
        elseif($Tipo==5)
        {
            $informe->Anexo5='';
        }
        elseif($Tipo==6)
        {
            $informe->Anexo6='';
        }
        elseif($Tipo==7)
        {
            $informe->Anexo7='';
        }
        elseif($Tipo==8)
        {
            $informe->Anexo8='';
        }
        elseif($Tipo==9)
        {
            $informe->Anexo9='';
        }
        elseif($Tipo==10)
        {
            $informe->Anexo10='';
        }
        elseif($Tipo==11)
        {
            $informe->Anexo11='';
        }
        $informe->update();
       
        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
    public function actionAnexo1()
    {
        require_once(Yii::$app->basePath . '/web/PHPWord/src/PhpWord/Autoloader.php');
        \PhpOffice\PhpWord\Autoloader::register();
        //var_dump(Yii::$app->basePath .'/web/TCPDF/tcpdf');die;
        
        \PhpOffice\PhpWord\Settings::setPdfRendererPath(Yii::$app->basePath .'/web/dompdf');
        \PhpOffice\PhpWord\Settings::setPdfRendererName('Dompdf');
        
        $phpWord = new \PhpOffice\PhpWord\PhpWord();

        //Open template and save it as docx
        $document = $phpWord->loadTemplate(Yii::$app->basePath . '/web/documentos/PLANTILLA_CAJA_CHICA.docx');
        $document->saveAs('temp.docx');
        $phpWord = \PhpOffice\PhpWord\IOFactory::load('temp.docx'); 
        //Save it
        //var_dump(\PhpOffice\PhpWord\Settings::setPdfRendererPath(Yii::$app->basePath .'/web/TCPDF/tcpdf.php'));die;
        $xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord , 'PDF');
        
        $xmlWriter->save('result.pdf');
        die;
    }
    
    public function actionAnexo2($PasoCriticoID=2)
    {
        require_once(Yii::$app->basePath . '/vendor/autoload.php');
        require_once(Yii::$app->basePath . '/web/mpdf/src/Mpdf.php');
        $pasoCritico=PasoCritico::findOne($PasoCriticoID);
        $proyecto=Proyecto::findOne($pasoCritico->ProyectoID);
        $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
        $componentes=MarcoLogicoComponente::find()
                    ->select('MarcoLogicoComponente.*,Componente.Peso ComponentePeso,Componente.Nombre ComponenteNombre')
                    ->innerJoin('Componente','MarcoLogicoComponente.ComponenteID=Componente.ID')
                    ->where('Componente.PoaID=:PoaID',[':PoaID'=>$poa->ID])
                    ->all();
                    
                    
        $mpdf=new \mPDF('c', 'A4-L');
        $html='<table class="table borderless table-hover" border=1 style="border:solid 1px black;font-size:11px">';
            $html.='<thead>';
                $html.='<tr>';
                    $html.='<td rowspan="2">PESO</td>';
                    $html.='<td rowspan="2">OBJETIVO / ACTIVIDAD</td>';
                    $html.='<td colspan="2">INDICADOR</td>';
                    $html.='<td colspan="3">Meta '.$pasoCritico->Correlativo.' ITF</td>';
                $html.='</tr>';
                $html.='<tr>';
                    $html.='<td>UNIDAD DE MEDIDA</td>';
                    $html.='<td>META</td>';
                    
                    $html.='<td>Programado</td>';
                    $html.='<td>Ejecutado</td>';
                    $html.='<td>% Ejecutado</td>';
                $html.='</tr>';
            $html.='</thead>';
            $html.='<tbody>';
            foreach($componentes as $componente){
                $actividades=MarcoLogicoActividad::find()
                        ->select('MarcoLogicoActividad.*,Actividad.Nombre ActividadNombre,Actividad.Peso ActividadPeso')
                        ->innerJoin('Actividad','MarcoLogicoActividad.ActividadID=Actividad.ID')
                        ->innerJoin('Componente','Actividad.ComponenteID=Componente.ID')
                        ->where('Componente.ID=:ID',[':ID'=>$componente->ComponenteID])
                        ->all();
                $html.='<tr>';
                    $html.='<td><b>'.$componente->ComponentePeso.'</b></td>';
                    $html.='<td><b>'.$componente->ComponenteNombre.'</b></td>';
                    $html.='<td><b>'.$componente->IndicadorDescripcion.'</b></td>';
                    $html.='<td><b>'.$componente->IndicadorMeta.'</b></td>';
                $html.='</tr>';
                foreach($actividades as $actividad){
                
                    $html.='<tr>';
                        $html.='<td>'.$actividad->ActividadPeso.'</td>';
                        $html.='<td>'.$actividad->ActividadNombre.'</td>';
                        $html.='<td>'.$actividad->IndicadorDescripcion.'</td>';
                        $html.='<td>'.$actividad->IndicadorMetaFisica.'</td>';
                    $html.='</tr>';
                }
            }    
            $html.='</tbody>';
        $html.='</table>';
        
        $mpdf->WriteHTML($html);
        $mpdf->Output('Marco Logico.pdf','D');
        die;
    }
    
    
    public function actionImprimirItf($PasoCriticoID=null)
    {
        require_once(Yii::$app->basePath . '/vendor/autoload.php');
        require_once(Yii::$app->basePath . '/web/mpdf/src/Mpdf.php');
        $pasoCritico=PasoCritico::findOne($PasoCriticoID);
        $proyecto=Proyecto::findOne($pasoCritico->ProyectoID);
        $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
        $informe=InformeTecnicoFinanciero::find()->where('PasoCriticoID=:PasoCriticoID',[':PasoCriticoID'=>$PasoCriticoID])->one();
        $componentes=MarcoLogicoComponente::find()
                    ->select('MarcoLogicoComponente.*,Componente.Peso ComponentePeso,Componente.Nombre ComponenteNombre')
                    ->innerJoin('Componente','MarcoLogicoComponente.ComponenteID=Componente.ID')
                    ->where('Componente.PoaID=:PoaID',[':PoaID'=>$poa->ID])
                    ->all();
        $componts = Componente::find()
                ->select('Componente.ID,Componente.Nombre,Componente.Correlativo')
                ->innerJoin('Poa','Poa.ID=Componente.PoaID')
                ->innerJoin('Actividad','Actividad.ComponenteID=Componente.ID')
                ->innerJoin('ActRubroElegible','ActRubroElegible.ActividadID=Actividad.ID')
                ->innerJoin('AreSubCategoria','AreSubCategoria.ActRubroElegibleID=ActRubroElegible.ID')
                ->innerJoin('CronogramaAreSubCategoria','CronogramaAreSubCategoria.AreSubCategoriaID=AreSubCategoria.ID')
                ->where('Componente.Estado=:Estado and Poa.ProyectoID=:ProyectoID and CronogramaAreSubCategoria.MetaFisica!=0',[':Estado' => 1, ':ProyectoID' => $proyecto->ID])
                ->andWhere(['between', 'CronogramaAreSubCategoria.Mes', $pasoCritico->MesInicio, $pasoCritico->MesFin])
                ->groupBy('Componente.ID,Componente.Nombre,Componente.Correlativo')
                ->orderBy('Componente.Correlativo asc')
                ->all();
                
        $resultado = (new \yii\db\Query())
                ->select('InformacionGeneral.Codigo,
                         InformacionGeneral.TituloProyecto,
                         InformacionGeneral.InvestigadorID,
                    ProgramaTransversal.Nombre ProgramaTransversalNombre,
                    d.Nombre dNombre,
                    p.Nombre pNombre,
                    r.Nombre rNombre,
                    DireccionLinea.Nombre DireccionLineaNombre,
                    TipoInvestigacion.Nombre TipoInvestigacionNombre, 
                    UnidadOperativa.Nombre UnidadOperativaNombre,
                    UnidadEjecutora.Nombre UnidadEjecutoraNombre,
                    Persona.Nombre IRPNombre,
                    Persona.ApellidoPaterno IRPApellidoPaterno,
                    Persona.ApellidoMaterno IRPApellidoMaterno,
                    Especie.Nombre EspecieNombre,
                    CultivoCrianza.Nombre CultivoCrianzaNombre,
                    Programa.Nombre ProgramaNombre,
                    InformacionGeneral.Meses,
                    EntidadParticipante.RazonSocial EntidadParticipanteRazonSocial,
                    EntidadParticipante.AporteMonetario EntidadParticipanteAporteMonetario,
                    EntidadParticipante.AporteNoMonetario EntidadParticipanteAporteNoMonetario,
                    TipoInstitucion.Nombre TipoColaboradora,
                    InformacionGeneral.Antecedente')
                ->from('InformacionGeneral')
                ->innerJoin('Investigador','Investigador.ID=InformacionGeneral.InvestigadorID')
                ->innerJoin('Usuario','Usuario.ID=Investigador.UsuarioID')
                ->innerJoin('Persona','Persona.ID=Usuario.PersonaID')
                ->innerJoin('ProgramaTransversal','ProgramaTransversal.ID=InformacionGeneral.ProgramaTransversalID')
                ->innerJoin('Ubigeo d','d.ID=InformacionGeneral.DistritoID')
                ->innerJoin('Ubigeo p','p.ID=substring(InformacionGeneral.DistritoID,1,4)')
                ->innerJoin('Ubigeo r','r.ID=substring(InformacionGeneral.DistritoID,1,2)')
                ->innerJoin('DireccionLinea','DireccionLinea.ID=InformacionGeneral.DireccionLineaID')
                ->innerJoin('TipoInvestigacion','TipoInvestigacion.ID=InformacionGeneral.TipoInvestigacionID')
                ->innerJoin('UnidadOperativa','UnidadOperativa.ID=InformacionGeneral.UnidadOperativaID')
                ->innerJoin('UnidadEjecutora','UnidadEjecutora.ID=UnidadOperativa.UnidadEjecutoraID')
                ->innerJoin('Especie','Especie.ID=InformacionGeneral.EspecieID')
                ->innerJoin('CultivoCrianza','CultivoCrianza.ID=Especie.CultivoCrianzaID')
                ->innerJoin('Programa','Programa.ID=CultivoCrianza.ProgramaID')
                ->innerJoin('EntidadParticipante','EntidadParticipante.InvestigadorID=InformacionGeneral.InvestigadorID')
                ->innerJoin('TipoInstitucion','TipoInstitucion.ID=EntidadParticipante.TipoInstitucionID and TipoInstitucion.ID=3')
                ->where('InformacionGeneral.InvestigadorID=:InvestigadorID',['InvestigadorID'=>$proyecto->InvestigadorID])
                ->one();
        
        
        $colaboradoras=EntidadParticipante::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$resultado['InvestigadorID']])->all();
        //$mpdf=new \mPDF('c', 'A4-L');
        $mpdf=new \mPDF;
        /*
        $html='<table class="col-md-12 table borderless table-hover" border=1 style="width=100%;border:1px solid black">';
            $html.='<thead>';
                $html.='<tr>';
                    $html.='<td colspan="4">INFORME TÉCNICO FINANCIERO N° '.$pasoCritico->Correlativo.'</td>';
                $html.='</tr>';
                $html.='<tr>';
                    $html.='<td colspan="3" align="rigth">Fecha:</td>';
                    $html.='<td>'.date('d-m-Y',strtotime($informe->FechaInforme)).'</td>';
                $html.='</tr>';
                $html.='<tr>';
                    $html.='<td><b>I</b></td>';
                    $html.='<td colspan="3"><b>Información general del proyecto</b></td>';
                $html.='</tr>';
            $html.='</thead>';
            $html.='<tbody>';
            $html.='</tbody>';
        $html.='</table>';
        */
        
        $html.='<style>#col table { border-collapse: collapse;} #col td, th { border: 2px solid black;margin:0px;padding: 10px;text-align: left;}</style>';
        
        $html.='<div class="col-md-12" align="center">INFORME TÉCNICO FINANCIERO N° '.$pasoCritico->Correlativo.'</div>';
        $html.='<div class="clearfix"></div>';
        $html.='<table width="100%"><tr><td width="90%" align="right">Fecha:</td><td width="10%">'.date('d-m-Y',strtotime($informe->FechaInforme)).'</td></tr></table>';
        
        $html.='<table width="100%"><tr><td width="5%" align="left">I.</td><td width="95%"> Información general del proyecto</td></tr></table>';
        
        $html.='<table width="100%">';
            $html.='<tr>';
                $html.='<td width="5%" align="left">1.-</td>';
                $html.='<td width="30%"> N° de Proyecto</td>';
                $html.='<td width="5%" align="left">:</td>';
                $html.='<td width="60%"> '.$resultado['Codigo'].'</td>';
            $html.='</tr>';
        $html.='</table>';
        
        $html.='<table width="100%">';
            $html.='<tr>';
                $html.='<td width="5%" align="left">2.-</td>';
                $html.='<td width="30%"> Dirección de Línea</td>';
                $html.='<td width="5%" align="left">:</td>';
                $html.='<td width="60%"> '.$resultado['DireccionLineaNombre'].'</td>';
            $html.='</tr>';
        $html.='</table>';
        
        $html.='<table width="100%">';
            $html.='<tr>';
                $html.='<td width="5%" align="left">3.-</td>';
                $html.='<td width="30%"> Unidad Ejecutora</td>';
                $html.='<td width="5%" align="left">:</td>';
                $html.='<td width="60%"> '.$resultado['UnidadEjecutoraNombre'].'</td>';
            $html.='</tr>';
        $html.='</table>';
        
        $html.='<table width="100%">';
            $html.='<tr>';
                $html.='<td width="5%" align="left">4.-</td>';
                $html.='<td width="30%"> Unidad Operativa</td>';
                $html.='<td width="5%" align="left">:</td>';
                $html.='<td width="60%"> '.$resultado['UnidadOperativaNombre'].'</td>';
            $html.='</tr>';
        $html.='</table>';
        
        $html.='<table width="100%">';
            $html.='<tr>';
                $html.='<td width="5%" align="left">5.-</td>';
                $html.='<td width="30%"> Nombre del Proyecto</td>';
                $html.='<td width="5%" align="left">:</td>';
                $html.='<td width="60%"> '.$resultado['TituloProyecto'].'</td>';
            $html.='</tr>';
        $html.='</table>';
        
        $html.='<table width="100%">';
            $html.='<tr>';
                $html.='<td width="5%" align="left">6.-</td>';
                $html.='<td width="30%"> Investigador Responsable</td>';
                $html.='<td width="5%" align="left">:</td>';
                $html.='<td width="60%"> '.$resultado['IRPNombre'].' '.$resultado['IRPApellidoPaterno'].' '.$resultado['IRPApellidoMaterno'].'</td>';
            $html.='</tr>';
        $html.='</table>';
        
        $html.='<table width="100%">';
            $html.='<tr>';
                $html.='<td width="5%" align="left">7.-</td>';
                $html.='<td width="30%"> Departamento</td>';
                $html.='<td width="5%" align="left">:</td>';
                $html.='<td width="60%"> '.$resultado['rNombre'].'</td>';
            $html.='</tr>';
        $html.='</table>';
        
        $html.='<table width="100%">';
            $html.='<tr>';
                $html.='<td width="5%" align="left">8.-</td>';
                $html.='<td width="30%"> Provincia</td>';
                $html.='<td width="5%" align="left">:</td>';
                $html.='<td width="60%"> '.$resultado['pNombre'].'</td>';
            $html.='</tr>';
        $html.='</table>';
        
        $html.='<table width="100%">';
            $html.='<tr>';
                $html.='<td width="5%" align="left">9.-</td>';
                $html.='<td width="30%"> Distrito</td>';
                $html.='<td width="5%" align="left">:</td>';
                $html.='<td width="60%"> '.$resultado['dNombre'].'</td>';
            $html.='</tr>';
        $html.='</table>';
        
        $html.='<table width="100%">';
            $html.='<tr>';
                $html.='<td width="5%" align="left">10.-</td>';
                $html.='<td width="30%"> Duración del Proyecto (meses)</td>';
                $html.='<td width="5%" align="left">:</td>';
                $html.='<td width="60%"> '.$resultado['Meses'].' meses</td>';
            $html.='</tr>';
        $html.='</table>';
        
        $html.='<table width="100%">';
            $html.='<tr>';
                $html.='<td width="5%" align="left">11.-</td>';
                $html.='<td width="30%"> Monto total del Proyecto – PNIA</td>';
                $html.='<td width="5%" align="left">:</td>';
                $html.='<td width="60%"> S/ '.number_format($resultado['EntidadParticipanteAporteMonetario'],2,'.',' ').'</td>';
            $html.='</tr>';
        $html.='</table>';
        
        $html.='<table width="100%">';
            $html.='<tr>';
                $html.='<td width="5%" align="left">12.-</td>';
                $html.='<td width="30%"> Monto otorgado en el '.$pasoCritico->Correlativo.' Tramo</td>';
                $html.='<td width="5%" align="left">:</td>';
                $html.='<td width="60%"> </td>';
            $html.='</tr>';
        $html.='</table>';
        
        $html.='<table width="100%">';
            $html.='<tr>';
                $html.='<td width="5%" align="left">13.-</td>';
                $html.='<td width="30%"> Periodo del Informe</td>';
                $html.='<td width="5%" align="left">:</td>';
                $html.='<td width="60%"></td>';
            $html.='</tr>';
        $html.='</table>';
        $monetario=0.00;
        $nomonetario=0.00;
        foreach($colaboradoras as $colaboradora)
        {
            $monetario=$monetario+$colaboradora->AporteMonetario;
            $nomonetario=$nomonetario+$colaboradora->AporteNoMonetario;
        }
        
        $html.='<table width="100%">';
            $html.='<tr>';
                $html.='<td width="5%" align="left">14.-</td>';
                $html.='<td width="30%"> Presupuesto del Proyecto</td>';
                $html.='<td width="5%" align="left">:</td>';
                $html.='<td width="60%"> S/ '.number_format(($monetario+$nomonetario),2,'.',' ').'</td>';
            $html.='</tr>';
        $html.='</table>';
       
        $html.='<br><br>';
        
        $html.='<table width="100%" style="font-size:12px;" cellspacing=0 cellspading=0>';
            $html.='<tr>';
                $html.='<td width="20%" align="left" style="border:1px solid black;background:#a8a8ab"></td>';
                $html.='<td width="15%" style="border:1px solid black;background:#a8a8ab"> Monetario</td>';
                $html.='<td width="15%" style="border:1px solid black;background:#a8a8ab"> No Monetario</td>';
                $html.='<td width="15%" style="border:1px solid black;background:#a8a8ab"> Total</td>';
            $html.='</tr>';
            $monetario=0;
            $nomonetario=0;
            foreach($colaboradoras as $colaboradora)
            {
                $html.='<tr>';
                    $html.='<td width="20%" align="left" style="border:1px solid black;">'.$colaboradora->RazonSocial.'</td>';
                    $html.='<td width="15%" style="border:1px solid black;"> '.number_format($colaboradora->AporteMonetario,2,'.',' ').'</td>';
                    $html.='<td width="15%" style="border:1px solid black;"> '.number_format($colaboradora->AporteNoMonetario,2,'.',' ').'</td>';
                    $html.='<td width="15%" style="border:1px solid black;"> '.number_format(($colaboradora->AporteMonetario+$colaboradora->AporteNoMonetario),2,'.',' ').'</td>';
                $html.='</tr>';
                $monetario=$monetario+$colaboradora->AporteMonetario;
                $nomonetario=$nomonetario+$colaboradora->AporteNoMonetario;
            }
            $html.='<tr>';
                $html.='<td width="20%" align="left" style="border:1px solid black;">Total S/</td>';
                $html.='<td width="15%" style="border:1px solid black;"> '.number_format($monetario,2,'.',' ').'</td>';
                $html.='<td width="15%" style="border:1px solid black;"> '.$nomonetario.'</td>';
                $html.='<td width="15%" style="border:1px solid black;"> '.number_format(($monetario+$nomonetario),2,'.',' ').'</td>';
            $html.='</tr>';
        $html.='</table>';
        $html.='<br><br>';
        $html.='<table width="100%"><tr><td width="5%" align="left">II.</td><td width="95%"> ANTECEDENTES</td></tr></table>';
        $html.='<table width="100%">';
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                $html.='<td width="95%"> '.$resultado['Antecedente'].'</td>';
            $html.='</tr>';
        $html.='</table>';
        $html.='<br><br>';
        /*
        $html.='<table width="100%"><tr><td width="5%" align="left">III.</td><td width="95%"> MARCO LÓGICO</td></tr></table>';
        
        $html.='<table width="100%" cellspacing=0 cellspading=0>';
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                $html.='<td width="95%">Fin del proyecto:</td>';
            $html.='</tr>';
            $html.='<tr>';
                $html.='<td width="5%"  align="left"></td>';
                $html.='<td width="95%" style="border:1px solid black;font-size:12px">'.$proyecto->Fin.'</td>';
            $html.='</tr>';
        $html.='</table>';
        
        $marcosFines=MarcoLogicoFinProyecto::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->all();
        $html.='<table width="100%" cellspacing=0 cellspading=0>';
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                $html.='<td width="25%" style="border:1px solid black;background:#a8a8ab">Indicador</td>';
                $html.='<td width="20%" style="border:1px solid black;background:#a8a8ab">Unidad de Medida</td>';
                $html.='<td width="25%" style="border:1px solid black;background:#a8a8ab">Medio de verificación</td>';
                $html.='<td width="25%" style="border:1px solid black;background:#a8a8ab">Supuestos</td>';
            $html.='</tr>';
            foreach($marcosFines as $marcoFine){
                $html.='<tr>';
                    $html.='<td width="5%" align="left"></td>';
                    $html.='<td width="25%" style="border:1px solid black;font-size:12px">'.$marcoFine->IndicadorDescripcion.'</td>';
                    $html.='<td width="20%" style="border:1px solid black;font-size:12px">'.$marcoFine->IndicadorUnidadMedida.'</td>';
                    $html.='<td width="25%" style="border:1px solid black;font-size:12px">'.$marcoFine->MedioVerificacion.'</td>';
                    $html.='<td width="25%" style="border:1px solid black;font-size:12px">'.$marcoFine->Supuestos.'</td>';
                $html.='</tr>';
            }
        $html.='</table>';
        
        $html.='<br>';
        $html.='<table width="100%" cellspacing=0 cellspading=0>';
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                $html.='<td width="95%">Propósito del proyecto:</td>';
            $html.='</tr>';
            $html.='<tr>';
                $html.='<td width="5%"  align="left"></td>';
                $html.='<td width="95%" style="border:1px solid black">'.$proyecto->Proposito.'</td>';
            $html.='</tr>';
        $html.='</table>';
        
        $marcosPropositos=MarcoLogicoPropositoProyecto::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->all();
        $html.='<table width="100%" cellspacing=0 cellspading=0>';
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                $html.='<td width="25%" style="border:1px solid black;background:#a8a8ab">Indicador</td>';
                $html.='<td width="20%" style="border:1px solid black;background:#a8a8ab">Unidad de Medida</td>';
                $html.='<td width="25%" style="border:1px solid black;background:#a8a8ab">Medio de verificación</td>';
                $html.='<td width="25%" style="border:1px solid black;background:#a8a8ab">Supuestos</td>';
            $html.='</tr>';
            foreach($marcosPropositos as $marcoProposito){
                $html.='<tr>';
                    $html.='<td width="5%" align="left"></td>';
                    $html.='<td width="25%" style="border:1px solid black;font-size:12px">'.$marcoProposito->IndicadorDescripcion.'</td>';
                    $html.='<td width="20%" style="border:1px solid black;font-size:12px">'.$marcoProposito->IndicadorUnidadMedida.'</td>';
                    $html.='<td width="25%" style="border:1px solid black;font-size:12px">'.$marcoProposito->MedioVerificacion.'</td>';
                    $html.='<td width="25%" style="border:1px solid black;font-size:12px">'.$marcoProposito->Supuestos.'</td>';
                $html.='</tr>';
            }
        $html.='</table>';
        
        $html.='<br><br>';
        $html.='<table width="100%" cellspacing=0 cellspading=0>';
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                
                $html.='<td width="25%" style="border:1px solid black;background:#a8a8ab">Indicador</td>';
                $html.='<td width="20%" style="border:1px solid black;background:#a8a8ab">Unidad de Medida</td>';
                $html.='<td width="25%" style="border:1px solid black;background:#a8a8ab">Medio de verificación</td>';
                $html.='<td width="25%" style="border:1px solid black;background:#a8a8ab">Supuestos</td>';
            $html.='</tr>';
            foreach($componentes as $componente){
                $actividades=MarcoLogicoActividad::find()
                        ->select('MarcoLogicoActividad.*,Actividad.Nombre ActividadNombre,Actividad.Peso ActividadPeso')
                        ->innerJoin('Actividad','MarcoLogicoActividad.ActividadID=Actividad.ID')
                        ->innerJoin('Componente','Actividad.ComponenteID=Componente.ID')
                        ->where('Componente.ID=:ID',[':ID'=>$componente->ComponenteID])
                        ->all();
                $html.='<tr>';
                    $html.='<td width="5%" align="left"></td>';
                    
                    $html.='<td width="25%" style="border:1px solid black;font-size:12px"><b>'.$componente->ComponentePeso.'</b></td>';
                    $html.='<td width="20%" style="border:1px solid black;font-size:12px"><b>'.$componente->ComponenteNombre.'</b></td>';
                    $html.='<td width="25%" style="border:1px solid black;font-size:12px"><b>'.$componente->IndicadorDescripcion.'</b></td>';
                    $html.='<td width="25%" style="border:1px solid black;font-size:12px"><b>'.$componente->IndicadorMeta.'</b></td>';
                $html.='</tr>';
                foreach($actividades as $actividad){
                
                    $html.='<tr>';
                        $html.='<td width="5%" align="left"></td>';
                        $html.='<td width="25%" style="border:1px solid black;font-size:12px">'.$actividad->ActividadPeso.'</td>';
                        $html.='<td width="20%" style="border:1px solid black;font-size:12px">'.$actividad->ActividadNombre.'</td>';
                        $html.='<td width="25%" style="border:1px solid black;font-size:12px">'.$actividad->IndicadorDescripcion.'</td>';
                        $html.='<td width="25%" style="border:1px solid black;font-size:12px">'.$actividad->IndicadorMetaFisica.'</td>';
                    $html.='</tr>';
                }
            }
        $html.='</table>';
        */
        $html.='<br><br>';
        $html.='<table width="100%"><tr><td width="5%" align="left">IV.</td><td width="95%"> PROGRAMACIÓN DE ACTIVIDADES</td></tr></table>';
        
        $html.='<br><br>';
        $html.='<table width="100%"><tr><td width="5%" align="left">V.</td><td width="95%"> RESUMEN EJECUTIVO DEL PROYECTO</td></tr></table>';
        $html.='<table width="100%">';
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                $html.='<td width="95%">'.$informe->ResumenEjecutivo.'</td>';
            $html.='</tr>';
        $html.='</table>';
        $html.='<br><br>';
        /*
        $actividades = Actividad::find()
                        ->select('Actividad.ID,Actividad.ComponenteID,Actividad.Nombre,Actividad.CostoUnitario,Actividad.Correlativo')
                        ->innerJoin('ActRubroElegible','ActRubroElegible.ActividadID=Actividad.ID')
                        ->innerJoin('AreSubCategoria','AreSubCategoria.ActRubroElegibleID=ActRubroElegible.ID')
                        ->innerJoin('CronogramaAreSubCategoria','CronogramaAreSubCategoria.AreSubCategoriaID=AreSubCategoria.ID')
                        ->where('Actividad.Estado=:Estado and Actividad.ComponenteID=:ComponenteID and CronogramaAreSubCategoria.MetaFisica!=0',[':Estado' => 1, ':ComponenteID' => $componente->ID])
                        ->andWhere(['between', 'CronogramaAreSubCategoria.Mes', $pasoCritico->MesInicio, $pasoCritico->MesFin])
                        ->groupBy('Actividad.ID,Actividad.Correlativo,Actividad.ComponenteID,Actividad.Nombre,Actividad.CostoUnitario')
                        ->orderBy('Actividad.Correlativo asc')
                        ->all();*/
                                                                                        
        $html.='<table width="100%"><tr><td width="5%" align="left">VI.</td><td width="95%"> EJECUCIÓN DE METAS FÍSICAS Y FINANCIERAS</td></tr></table>';
        $html.='<table width="100%">';
            foreach($componts as $comp){
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                $html.='<td width="15%"> <b>Objetivo'.$comp->Correlativo.'</b></td>';
                $html.='<td width="80%"> <b>'.$comp->Nombre.'</b></td>';
            $html.='</tr>';
                $activds = Actividad::find()
                        ->select('Actividad.ID,Actividad.ComponenteID,Actividad.Nombre,Actividad.CostoUnitario,Actividad.Correlativo')
                        ->innerJoin('ActRubroElegible','ActRubroElegible.ActividadID=Actividad.ID')
                        ->innerJoin('AreSubCategoria','AreSubCategoria.ActRubroElegibleID=ActRubroElegible.ID')
                        ->innerJoin('CronogramaAreSubCategoria','CronogramaAreSubCategoria.AreSubCategoriaID=AreSubCategoria.ID')
                        ->where('Actividad.Estado=:Estado and Actividad.ComponenteID=:ComponenteID and CronogramaAreSubCategoria.MetaFisica!=0',[':Estado' => 1, ':ComponenteID' => $comp->ID])
                        ->andWhere(['between', 'CronogramaAreSubCategoria.Mes', $pasoCritico->MesInicio, $pasoCritico->MesFin])
                        ->groupBy('Actividad.ID,Actividad.Correlativo,Actividad.ComponenteID,Actividad.Nombre,Actividad.CostoUnitario')
                        ->orderBy('Actividad.Correlativo asc')
                        ->all();
                
                foreach($activds as $act){
                $html.='<tr>';
                    $html.='<td width="5%" align="left"></td>';
                    $html.='<td width="15%"> <b>Actividad '.$comp->Correlativo.'.'.$act->Correlativo.'</b></td>';
                    $html.='<td width="80%"> <b>'.$act->Nombre.'</b></td>';
                $html.='</tr>';
                    $detalle=EjecucionMetaFisicaFinanciera::find()->where('ActividadID=:ActividadID and InformeTecnicoFinancieroID=:InformeTecnicoFinancieroID',[':ActividadID'=>$act->ID,':InformeTecnicoFinancieroID'=>$informe->ID])->one();
                    $html.='<tr>';
                        $html.='<td width="5%" align="left"></td>';
                        $html.='<td width="95%" colspan="2" style="border:1px solid black"> '.$detalle->Descripcion.'</td>';
                    $html.='</tr>';
                }
            }
        $html.='</table>';
        $html.='<br><br>';
        
        $html.='<table width="100%"><tr><td width="5%" align="left">VII.</td><td width="95%"> EJECUCIÓN FINANCIERA DE APORTES DE ENTIDADES COLABORADORAS</td></tr></table>';
        $html.='<table width="100%">';
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                $html.='<td width="95%">'.$informe->EjecucionFinancieraColaboradora.'</td>';
            $html.='</tr>';
        $html.='</table>';
        
        $html.='<table width="100%" style="font-size:12px;" cellspacing=0 cellspading=0>';
            $html.='<tr>';
                $html.='<td width="15%" align="left" style="border:1px solid black;background:#a8a8ab"> Entidad colaboradora</td>';
                $html.='<td width="15%" style="border:1px solid black;background:#a8a8ab"> Tipo de Aporte  (Monetario/No Monetario)</td>';
                $html.='<td width="15%" style="border:1px solid black;background:#a8a8ab"> Objetivo/Actividad</td>';
                $html.='<td width="15%" style="border:1px solid black;background:#a8a8ab"> Meta Total Programada (S/.)</td>';
                $html.='<td width="15%" style="border:1px solid black;background:#a8a8ab"> Meta Semestral Ejecutada (S/.)</td>';
                $html.='<td width="10%" style="border:1px solid black;background:#a8a8ab"> % avance</td>';
                $html.='<td width="15%" style="border:1px solid black;background:#a8a8ab"> Detalle del aporte</td>';
            $html.='</tr>';
            $tprogramado=0;
            $tejecutada=0;
            $colaboradorasT=EntidadParticipante::find()->where('InvestigadorID=:InvestigadorID and TipoInstitucionID not in (3)',[':InvestigadorID'=>$proyecto->InvestigadorID])->all();
            foreach($colaboradorasT as $colaboradora)
            {
                
                $ejecucionColaboradora=EjecucionFinancieraColaboradora::find()->where('ColaboradoraID=:ColaboradoraID and InformeTecnicoFinancieroID=:InformeTecnicoFinancieroID',[':ColaboradoraID'=>$colaboradora->ID,':InformeTecnicoFinancieroID'=>$informe->ID])->one();
                $tipoAporte="No Monetario";
                if($ejecucionColaboradora->TipoAporte==1)
                {
                    $tipoAporte="Monetario";
                }
                $html.='<tr>';
                    $html.='<td width="15%" align="left" style="border:1px solid black;">'.$colaboradora->RazonSocial.'</td>';
                    $html.='<td width="15%" style="border:1px solid black;"> '.$tipoAporte.'</td>';
                    $html.='<td width="15%" style="border:1px solid black;"> '.$ejecucionColaboradora->ActividadDetalle.'</td>';
                    $html.='<td width="15%" style="border:1px solid black;"> '.number_format(($ejecucionColaboradora->MetaTotalProgramado),2,'.',' ').'</td>';
                    $html.='<td width="15%" style="border:1px solid black;"> '.number_format(($ejecucionColaboradora->MetaSemestralEjecutada),2,'.',' ').'</td>';
                    $html.='<td width="10%" style="border:1px solid black;"> '.$ejecucionColaboradora->Avance.'</td>';
                    $html.='<td width="15%" style="border:1px solid black;"> '.$ejecucionColaboradora->DetalleAporte.'</td>';
                $html.='</tr>';
                $tprogramado=$tprogramado+$ejecucionColaboradora->MetaTotalProgramado;
                $tejecutada=$tejecutada+$ejecucionColaboradora->MetaSemestralEjecutada;
            }
            $html.='<tr>';
                $html.='<td width="15%" align="left" style="border:1px solid black;">Total S/</td>';
                $html.='<td width="15%" align="left" style="border:1px solid black;"></td>';
                $html.='<td width="15%" align="left" style="border:1px solid black;"></td>';
                $html.='<td width="15%" style="border:1px solid black;"> '.number_format($tprogramado,2,'.',' ').'</td>';
                $html.='<td width="15%" style="border:1px solid black;"> '.number_format($tejecutada,2,'.',' ').'</td>';
                $html.='<td width="10%" style="border:1px solid black;"></td>';
                $html.='<td width="15%" style="border:1px solid black;"></td>';
            $html.='</tr>';
        $html.='</table>';
        
        $html.='<br><br>';
        
        $html.='<table width="100%"><tr><td width="5%" align="left">VIII.</td><td width="95%"> INFORME DE CUMPLIMIENTO DE HITO DEL PROYECTO</td></tr></table>';
        $hito=Hito::find()->where('InformeTecnicoFinancieroID=:InformeTecnicoFinancieroID',[':InformeTecnicoFinancieroID'=>$informe->ID])->one();
        $html.='<table width="100%" style="font-size:12px;" cellspacing=0 cellspading=0>';
            $html.='<tr>';
                $html.='<td width="5%"></td>';
                $html.='<td width="95%" align="left" style="border:1px solid black;"> TITULO DE HITO: '.$hito->Titulo.'</td>';
            $html.='</tr>';
            $html.='<tr>';
                $html.='<td width="5%"></td>';
                $html.='<td width="95%" align="left" style="border:1px solid black;background:#a8a8ab"> 1.  Descripción del hito:</td>';
            $html.='</tr>';
            $html.='<tr>';
                $html.='<td width="5%"></td>';
                $html.='<td width="95%" align="left" style="border:1px solid black;"> '.$hito->Descripcion.'</td>';
            $html.='</tr>';
            
            $html.='<tr>';
                $html.='<td width="5%"></td>';
                $html.='<td width="95%" align="left" style="border:1px solid black;background:#a8a8ab"> 2.  Periodo del hito:</td>';
            $html.='</tr>';
            $html.='<tr>';
                $html.='<td width="5%"></td>';
                $html.='<td width="95%" align="left" style="border:1px solid black;"> '.$hito->PeriodoInicio.'</td>';
            $html.='</tr>';
            
            $html.='<tr>';
                $html.='<td width="5%"></td>';
                $html.='<td width="95%" align="left" style="border:1px solid black;background:#a8a8ab"> 3.  Se cumplió el Hito si (x)… no…:</td>';
            $html.='</tr>';
            $html.='<tr>';
                $html.='<td width="5%"></td>';
                $html.='<td width="95%" align="left" style="border:1px solid black;"> '.$hito->DescripcionCumplio.'</td>';
            $html.='</tr>';
            
            $html.='<tr>';
                $html.='<td width="5%"></td>';
                $html.='<td width="95%" align="left" style="border:1px solid black;background:#a8a8ab"> 4.  Ejecución financiera para alcanzar el hito:</td>';
            $html.='</tr>';
            $html.='<tr>';
                $html.='<td width="5%"></td>';
                $html.='<td width="95%" align="left" style="border:1px solid black;"> '.$hito->EjecucionFinanciera.'</td>';
            $html.='</tr>';
            
            $html.='<tr>';
                $html.='<td width="5%"></td>';
                $html.='<td width="95%" align="left" style="border:1px solid black;background:#a8a8ab"> 5.  Observaciones: sugerencias para mejorar el logro de hitos:</td>';
            $html.='</tr>';
            $html.='<tr>';
                $html.='<td width="5%"></td>';
                $html.='<td width="95%" align="left" style="border:1px solid black;"> '.$hito->Observacion.'</td>';
            $html.='</tr>';
            
            $html.='<tr>';
                $html.='<td width="5%"></td>';
                $html.='<td width="95%" align="left" style="border:1px solid black;background:#a8a8ab"> 6.  Logros adicionales al hito:</td>';
            $html.='</tr>';
            $html.='<tr>';
                $html.='<td width="5%"></td>';
                $html.='<td width="95%" align="left" style="border:1px solid black;"> '.$hito->Logros.'</td>';
            $html.='</tr>';
        $html.='</table>';
        $html.='<br><br>';
        $html.='<table width="100%"><tr><td width="5%" align="left">IX.</td><td width="95%"> APLICACION DE SALVAGUARDAS SOCIO AMBIENTALES</td></tr></table>';
        $html.='<table width="100%">';
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                $html.='<td width="95%">9.1.    LISTA DE ACCIONES PARA MITIGAR EFECTOS AMBIENTALES / CONSIDERANDO LOS ASPECTOS E IMPACTOS IDENTIFICADOS PREVIAMENTE</td>';
            $html.='</tr>';
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                $html.='<td width="95%" style="border:1px solid black;background:#a8a8ab">Describa las medidas socio ambientales implementadas para el proyecto en este periodo:  manejo de aguas, residuos sólidos, manejo de plagas, manejo de agroquímicos, sustancias peligrosas, manejo de suelos, reforestación y deforestación, etc:</td>';
            $html.='</tr>';
            $medidas=MedidaSocioAmbiental::find()->where('InformeTecnicoFinancieroID=:InformeTecnicoFinancieroID',[':InformeTecnicoFinancieroID'=>$informe->ID])->all();
            foreach($medidas as $medida){
                $html.='<tr>';
                    $html.='<td width="5%" align="left"></td>';
                    $html.='<td width="95%" style="border:1px solid black;">'.$medida->Descripcion.'</td>';
                $html.='</tr>';
            }
        $html.='</table>';
        
        $html.='<table width="100%">';
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                $html.='<td width="95%" style="border:1px solid black;background:#a8a8ab">Producción de residuos sólidos expresada en Kilogramos - Año 2017 :</td>';
            $html.='</tr>';
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                $html.='<td width="95%" >Residuos NO Peligrosos</td>';
            $html.='</tr>';
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                $html.='<td width="95%">';
                $bandera=$pasoCritico->MesInicio;
                for($i=1;$i<=($pasoCritico->MesFin-$pasoCritico->MesInicio+1);$i++){
                    $npMes=ProduccionResiduoSolido::find()
                            ->where('InformeTecnicoFinancieroID=:InformeTecnicoFinancieroID and Tipo=1 and Mes=:Mes',
                            [':InformeTecnicoFinancieroID'=>$informe->ID,':Mes'=>$bandera])->one();
                    $html.='<div class="col-sm-2">';
                        $html.='Mes '.$npMes->Mes.': '.$npMes->Cantidad.' KG';
                    $html.='</div>';
                    $bandera++;
                }
                $html.='</td>';
            $html.='</tr>';
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                $html.='<td width="95%" >Residuos Peligrosos</td>';
            $html.='</tr>';
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                $html.='<td width="95%">';
                $bandera=$pasoCritico->MesInicio;
                for($i=1;$i<=($pasoCritico->MesFin-$pasoCritico->MesInicio+1);$i++){
                    $npMes=ProduccionResiduoSolido::find()
                            ->where('InformeTecnicoFinancieroID=:InformeTecnicoFinancieroID and Tipo=2 and Mes=:Mes',
                            [':InformeTecnicoFinancieroID'=>$informe->ID,':Mes'=>$bandera])->one();
                    $html.='<div class="col-sm-2">';
                        $html.='Mes '.$npMes->Mes.': '.$npMes->Cantidad.' KG';
                    $html.='</div>';
                    $bandera++;
                }
                $html.='</td>';
            $html.='</tr>';
        $html.='</table>';
        
        $html.='<table width="100%">';
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                $html.='<td width="95%" style="border:1px solid black;background:#a8a8ab">Describa el manejo de residuos sólidos comunes y de residuos tóxicos peligrosos :</td>';
            $html.='</tr>';
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                $html.='<td width="95%" >'.$informe->DescribirResiduosPeligrosos.'</td>';
            $html.='</tr>';
        $html.='</table>';
        
        $html.='<table width="100%">';
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                $html.='<td width="95%" style="border:1px solid black;background:#a8a8ab">Consumos de combustibles / Galones (Gln) :</td>';
            $html.='</tr>';
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                $html.='<td width="95%">';
                $bandera=$pasoCritico->MesInicio;
                for($i=1;$i<=($pasoCritico->MesFin-$pasoCritico->MesInicio+1);$i++){
                    $ccMes=ConsumoCombustible::find()
                            ->where('InformeTecnicoFinancieroID=:InformeTecnicoFinancieroID and Tipo=1 and Mes=:Mes',
                            [':InformeTecnicoFinancieroID'=>$informe->ID,':Mes'=>$bandera])->one();
                    $html.='<div class="col-sm-2">';
                        $html.='Mes '.$ccMes->Mes.': '.$ccMes->Cantidad.' Gal.';
                    $html.='</div>';
                    $bandera++;
                }
                $html.='</td>';
            $html.='</tr>';
        $html.='</table>';
        
        $html.='<table width="100%">';
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                $html.='<td width="95%" style="border:1px solid black;background:#a8a8ab">Consumo de agua / Metros cubicos (m3) :</td>';
            $html.='</tr>';
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                $html.='<td width="95%" >'.$informe->DescribirConsumoAgua.'</td>';
            $html.='</tr>';
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                $html.='<td width="95%">';
                $bandera=$pasoCritico->MesInicio;
                for($i=1;$i<=($pasoCritico->MesFin-$pasoCritico->MesInicio+1);$i++){
                    $CAMes=ConsumoAgua::find()
                            ->where('InformeTecnicoFinancieroID=:InformeTecnicoFinancieroID and Tipo=1 and Mes=:Mes',
                            [':InformeTecnicoFinancieroID'=>$informe->ID,':Mes'=>$bandera])->one();
                    $html.='<div class="col-sm-2">';
                        $html.='Mes '.$CAMes->Mes.': '.$CAMes->Cantidad.' m3';
                    $html.='</div>';
                    $bandera++;
                }
                $html.='</td>';
            $html.='</tr>';
        $html.='</table>';
        
        $html.='<table width="100%">';
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                $html.='<td width="95%" style="border:1px solid black;background:#a8a8ab">Seguridad y Salud Ocupacional :</td>';
            $html.='</tr>';
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                $html.='<td width="95%" >'.$informe->SeguridadOcupacional.'</td>';
            $html.='</tr>';
        $html.='</table>';
        
        $html.='<table width="100%">';
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                $html.='<td width="95%" style="border:1px solid black;background:#a8a8ab">Liste las comunidades, distritos, asociaciones, agrupaciones, distritos involucrados en el proyecto para este périodo :</td>';
            $html.='</tr>';
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                $html.='<td width="95%" >'.$informe->ListaInvolucrados.'</td>';
            $html.='</tr>';
        $html.='</table>';
        
        $html.='<table width="100%" style="font-size:12px;" cellspacing=0 cellspading=0>';
            $html.='<tr>';
                $html.='<td width="5%" align="left"> </td>';
                $html.='<td width="20%" style="border:1px solid black;"> Fecha</td>';
                $html.='<td width="15%" style="border:1px solid black;"> Tema</td>';
                $html.='<td width="15%" style="border:1px solid black;"> Numero de personas</td>';
                $html.='<td width="15%" style="border:1px solid black;"> Comunidad, localidad</td>';
                $html.='<td width="10%" style="border:1px solid black;"> Lugar</td>';
                $html.='<td width="10%" style="border:1px solid black;"> Tiempo del evento en MIN</td>';
                $html.='<td width="10%" style="border:1px solid black;"> (**)Horas Hombre<br> (HH)</td>';
            $html.='</tr>';
            $horas=0;
            $participativas=ListaParticipativa::find()->where('InformeTecnicoFinancieroID=:InformeTecnicoFinancieroID',[':InformeTecnicoFinancieroID'=>$informe->ID])->all();
            foreach($participativas as $participativa){
                $html.='<tr>';
                    $html.='<td width="5%" align="left"> </td>';
                    $html.='<td width="20%" style="border:1px solid black;"> '.date('Y-m-d',strtotime($participativa->Fecha)).'</td>';
                    $html.='<td width="15%" style="border:1px solid black;"> '.$participativa->Tema.'</td>';
                    $html.='<td width="15%" style="border:1px solid black;"> '.$participativa->NumeroPersonas.'</td>';
                    $html.='<td width="15%" style="border:1px solid black;"> '.$participativa->Comunidad.'</td>';
                    $html.='<td width="10%" style="border:1px solid black;"> '.$participativa->Lugar.'</td>';
                    $html.='<td width="10%" style="border:1px solid black;"> '.$participativa->Tiempo.'</td>';
                    $html.='<td width="10%" style="border:1px solid black;"> '.number_format((($participativa->NumeroPersonas*$participativa->Tiempo)/60),2,'.',' ').'</td>';
                $html.='</tr>';
                $horas=$horas+(($participativa->NumeroPersonas*$participativa->Tiempo)/60);
            }
            $html.='<tr>';
                $html.='<td width="5%" align="left"> </td>';
                $html.='<td width="85%" colspan="6" style="border:1px solid black;">TOTAL /HH</td>';
                $html.='<td width="10%" style="border:1px solid black;">'.number_format($horas,2,'.',' ').'</td>';
            $html.='</tr>';
            $html.='<tr>';
                $html.='<td width="5%" align="left"> </td>';
                $html.='<td width="95%" colspan="7" style="border:1px solid black;">(**)Las horas hombre (HH), multiplicar el número de personas por los minutos de duración del evento dividido entre 60 = (NRO PERSONAS x MINUTOS) / 60 </td>';
            $html.='</tr>';
        $html.='</table>';
        
        $html.='<table width="100%">';
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                $html.='<td width="95%" style="border:1px solid black;background:#a8a8ab">Liste los incidentes socio ambientales ocurridos :</td>';
            $html.='</tr>';
        $html.='</table>';
        
        $html.='<table width="100%" style="font-size:12px;" cellspacing=0 cellspading=0>';
            $html.='<tr>';
                $html.='<td width="5%" align="left"> </td>';
                $html.='<td width="20%" style="border:1px solid black;"> Incidente/Accidente</td>';
                $html.='<td width="15%" style="border:1px solid black;"> Fecha</td>';
                $html.='<td width="15%" style="border:1px solid black;"> Medidas correctivas adoptadas</td>';
                $html.='<td width="15%" style="border:1px solid black;"> Medidas preventivas adoptadas</td>';
                $html.='<td width="10%" style="border:1px solid black;"> Numero de personas involucradas</td>';
                $html.='<td width="10%" style="border:1px solid black;"> Con daño a la persona (si /no)</td>';
                $html.='<td width="10%" style="border:1px solid black;"> Costo en S/. Pérdida</td>';
            $html.='</tr>';
            $costoIncidencia=0;
            $incidencias=ListaIncidencia::find()->where('InformeTecnicoFinancieroID=:InformeTecnicoFinancieroID',[':InformeTecnicoFinancieroID'=>$informe->ID])->all();
            foreach($incidencias as $incidencia){
                $inci="Accidente";
                if($incidencia->Tipo==1)
                {
                    $inci="Incidente";
                }
                $dano="No";
                if($incidencia->DanoPersona=="1")
                {
                    $dano="Si";
                }
                
                $html.='<tr>';
                    $html.='<td width="5%" align="left"> </td>';
                    $html.='<td width="20%" style="border:1px solid black;"> '.$inci.'</td>';
                    $html.='<td width="20%" style="border:1px solid black;"> '.date('Y-m-d',strtotime($incidencia->Fecha)).'</td>';
                    $html.='<td width="15%" style="border:1px solid black;"> '.$incidencia->MedidaCorrectiva.'</td>';
                    $html.='<td width="15%" style="border:1px solid black;"> '.$incidencia->MedidaPreventiva.'</td>';
                    $html.='<td width="15%" style="border:1px solid black;"> '.$incidencia->NumeroInvolucrados.'</td>';
                    $html.='<td width="10%" style="border:1px solid black;"> '.$dano.'</td>';
                    $html.='<td width="10%" style="border:1px solid black;"> '.$incidencia->Costo.'</td>';
                $html.='</tr>';
                $costoIncidencia=$costoIncidencia+$incidencia->Costo;
            }
            $html.='<tr>';
                $html.='<td width="5%" align="left"> </td>';
                $html.='<td width="85%" colspan="6" style="border:1px solid black;">TOTAL</td>';
                $html.='<td width="10%" style="border:1px solid black;">'.number_format($costoIncidencia,2,'.',' ').'</td>';
            $html.='</tr>';
        $html.='</table>';
        
        $html.='<table width="100%">';
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                $html.='<td width="95%" style="border:1px solid black;background:#a8a8ab">9.2.  SOBRE EQUIDAD E INCLUSION DE GENERO </td>';
            $html.='</tr>';
        $html.='</table>';
        
        $html.='<table width="100%" style="font-size:12px;" cellspacing=0 cellspading=0>';
            $html.='<tr>';
                $html.='<td width="5%" align="left"> </td>';
                $html.='<td width="20%" style="border:1px solid black;"> ITEM</td>';
                $html.='<td width="15%" style="border:1px solid black;"> Equidad</td>';
                $html.='<td width="15%" style="border:1px solid black;"> Hombres</td>';
                $html.='<td width="15%" style="border:1px solid black;"> Mujeres</td>';
            $html.='</tr>';
            $incidencias=ListaIncidencia::find()->where('InformeTecnicoFinancieroID=:InformeTecnicoFinancieroID',[':InformeTecnicoFinancieroID'=>$informe->ID])->all();
            foreach($incidencias as $incidencia){
                $inci="Accidente";
                if($incidencia->Tipo==1)
                {
                    $inci="Incidente";
                }
                $dano="No";
                if($incidencia->DanoPersona=="1")
                {
                    $dano="Si";
                }
                
                $html.='<tr>';
                    $html.='<td width="5%" align="left"> </td>';
                    $html.='<td width="20%" style="border:1px solid black;"> '.$inci.'</td>';
                    $html.='<td width="20%" style="border:1px solid black;"> '.date('Y-m-d',strtotime($incidencia->Fecha)).'</td>';
                    $html.='<td width="15%" style="border:1px solid black;"> '.$incidencia->MedidaCorrectiva.'</td>';
                    $html.='<td width="15%" style="border:1px solid black;"> '.$incidencia->MedidaPreventiva.'</td>';
                    $html.='<td width="15%" style="border:1px solid black;"> '.$incidencia->NumeroInvolucrados.'</td>';
                    $html.='<td width="10%" style="border:1px solid black;"> '.$dano.'</td>';
                    $html.='<td width="10%" style="border:1px solid black;"> '.$incidencia->Costo.'</td>';
                $html.='</tr>';
                $costoIncidencia=$costoIncidencia+$incidencia->Costo;
            }
            $html.='<tr>';
                $html.='<td width="5%" align="left"> </td>';
                $html.='<td width="85%" colspan="6" style="border:1px solid black;">TOTAL</td>';
                $html.='<td width="10%" style="border:1px solid black;">'.number_format($costoIncidencia,2,'.',' ').'</td>';
            $html.='</tr>';
        $html.='</table>';
        
        
        $html.='<br><br>';
        $html.='<table width="100%"><tr><td width="5%" align="left">X.</td><td width="95%"> LECCIONES APRENDIDAS</td></tr></table>';
        $html.='<table width="100%">';
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                $html.='<td width="95%">'.$informe->LeccionAprendida.'</td>';
            $html.='</tr>';
        $html.='</table>';
        $html.='<br><br>';
        $html.='<table width="100%"><tr><td width="5%" align="left">XI.</td><td width="95%"> CONCLUSIONES Y RECOMENDACIONES</td></tr></table>';
        $html.='<table width="100%">';
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                $html.='<td width="95%">Conclusión:<br>'.$informe->Conclusion.'</td>';
            $html.='</tr>';
        $html.='</table>';
        $html.='<table width="100%">';
            $html.='<tr>';
                $html.='<td width="5%" align="left"></td>';
                $html.='<td width="95%">Recomendación:<br>'.$informe->Recomendacion.'</td>';
            $html.='</tr>';
        $html.='</table>';
        $html.='<br><br>';
        $html.='<br><br>';
        $mpdf->WriteHTML($html);
        $mpdf->Output('ITF.pdf','I');
        die;
    }
}
