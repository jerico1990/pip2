<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\PasoCritico;
use app\models\Usuario;
use app\models\Persona;
use app\models\Investigador;
use app\models\Proyecto;
use app\models\InformacionGeneral;
use app\models\Poa;
use app\models\Componente;
use app\models\Actividad;
use app\models\CronogramaProyecto;
use app\models\ActRubroElegible;
use app\models\CronogramaComponente;
use app\models\CronogramaActividad;
use app\models\CronogramaActRubroElegible;
use app\models\RubroElegible;
use app\models\AreSubCategoria;
use app\models\CronogramaAreSubCategoria;
use app\models\MarcoLogicoActividad;
use app\models\AreSubCategoriaObservacion;
use app\models\CertificacionPresupuestal;
use app\models\DetalleCertificacionPresupuestal;
use app\models\UnidadOperativa;
use app\models\SiafSecuencialProyecto;
use app\models\CajaChica;
use app\models\Orden;
use app\models\Siaf;
use app\models\Requerimiento;
use app\models\UnidadEjecutora;
use app\models\ReProgramacionRecurso;
use app\models\DetalleReProgramacionRecurso;
use PhpOffice\PhpWord\PhpWord;
use yii\web\UploadedFile;


class ReProgramacionRecursoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout='estandar';
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $Investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $InformacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$Investigador->ID])->one();
        $Proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$Investigador->ID])->one();
        $Poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$Proyecto->ID])->one();
        $Componentes=Componente::find()->where('PoaID=:PoaID and Estado=1',[':PoaID'=>$Poa->ID])->orderBy('Correlativo asc')->all();
        
        
        return $this->render('index',['Componentes'=>$Componentes,'CodigoProyecto'=>$InformacionGeneral->Codigo]);
    }
    
    public function actionReProgramar($RecursoID=null)
    {
        $this->layout='estandar';
        $Recurso=AreSubCategoria::find()->where('ID=:ID',[':ID'=>$RecursoID])->one();
        $ActRubroElegible=ActRubroElegible::findOne($Recurso->ActRubroElegibleID);
        $Actividad=Actividad::findOne($ActRubroElegible->ActividadID);
        $Componente=Componente::findOne($Actividad->ComponenteID);
        $Poa=Poa::findOne($Componente->PoaID);
        $Proyecto=Proyecto::findOne($Poa->ProyectoID);
        $InformacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$Proyecto->InvestigadorID])->one();
        $CronogramaRecursos=CronogramaAreSubCategoria::find()->where('AreSubCategoriaID=:AreSubCategoriaID',[':AreSubCategoriaID'=>$Recurso->ID])->all();
        $ReProgramarRecurso=new ReProgramacionRecurso;
        if($ReProgramarRecurso->load(Yii::$app->request->post())){
            $ReProgramarRecurso->AreSubCategoriaID=$Recurso->ID;
            $ReProgramarRecurso->Situacion=1;
            $ReProgramarRecurso->Estado=1;
            $ReProgramarRecurso->FechaRegistro=date('Ymd');
            $ReProgramarRecurso->CodigoProyecto=$InformacionGeneral->Codigo;
            
            $ReProgramarRecurso->save();
            if(!$ReProgramarRecurso->MetasFisicas)
            {
                $countDetalles=0;
            }
            else
            {
                $countDetalles=count(array_filter($ReProgramarRecurso->MetasFisicas));
            }
            for($i=0;$i<$countDetalles;$i++)
            {
                if(isset($ReProgramarRecurso->MetasFisicas[$i]))
                {
                    $detalle=new DetalleReProgramacionRecurso;
                    $detalle->ReProgramacionRecursoID=$ReProgramarRecurso->ID;
                    $detalle->MetaFisica= $ReProgramarRecurso->MetasFisicas[$i];
                    $detalle->CostoUnitario=$ReProgramarRecurso->CostosUnitarios[$i];
                    $detalle->Mes = $ReProgramarRecurso->Meses[$i];;
                    $detalle->save();
                }
            }
            
            $ReProgramarRecurso->Archivo = UploadedFile::getInstance($ReProgramarRecurso, 'Archivo');
            if($ReProgramarRecurso->Archivo)
            {
                $ReProgramarRecurso->Archivo->saveAs('reprogramacionrecursos/' . $ReProgramarRecurso->ID . '.' . $ReProgramarRecurso->Archivo->extension);
                $ReProgramarRecurso->Evidencia=$ReProgramarRecurso->ID . '.' . $ReProgramarRecurso->Archivo->extension;
            }
            $ReProgramarRecurso->update();
            
            return $this->redirect(['listado-recursos','RecursoID'=>$RecursoID]);
        }
        return $this->render('re-programar',['Recurso'=>$Recurso,'Componente'=>$Componente,'Actividad'=>$Actividad,'CronogramaRecursos'=>$CronogramaRecursos]);
    }
    
    public function actionVerRecurso($ID=null)
    {
        $this->layout='vacio';
         $ReProgramarRecurso=ReProgramacionRecurso::find()->where('Situacion in (1,3) and ID=:ID',[':ID'=>$ID])->one();
        $Recurso=AreSubCategoria::find()->where('ID=:ID',[':ID'=>$ReProgramarRecurso->AreSubCategoriaID])->one();
        $ActRubroElegible=ActRubroElegible::findOne($Recurso->ActRubroElegibleID);
        $Actividad=Actividad::findOne($ActRubroElegible->ActividadID);
        $Componente=Componente::findOne($Actividad->ComponenteID);
        $Poa=Poa::findOne($Componente->PoaID);
        $Proyecto=Proyecto::findOne($Poa->ProyectoID);
        $InformacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$Proyecto->InvestigadorID])->one();
        $CronogramaRecursos=CronogramaAreSubCategoria::find()->where('AreSubCategoriaID=:AreSubCategoriaID',[':AreSubCategoriaID'=>$Recurso->ID])->all();
       
        
        return $this->render('ver-recurso',['Recurso'=>$Recurso,'Componente'=>$Componente,'Actividad'=>$Actividad,'CronogramaRecursos'=>$CronogramaRecursos,'ReProgramarRecurso'=>$ReProgramarRecurso]);
    }
    
    public function actionUafsi()
    {
        $this->layout='estandar';
        return $this->render('uafsi');
    }
    
    public function actionListadoRecursos($RecursoID=null)
    {
        $this->layout='estandar';
        return $this->render('listado-recursos',['RecursoID'=>$RecursoID]);
    }
    
    public function actionListadoReProRecursos($RecursoID=null)
    {
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        
        $resultados = (new \yii\db\Query())
                        ->select('ReProgramacionRecurso.ID ReProgramacionRecursoID,ActRubroElegible.*,ReProgramacionRecurso.*,AreSubCategoria.*,Situacion.ID SituacionID,Situacion.Descripcion')
                        ->from('ReProgramacionRecurso')
                        ->innerJoin('AreSubCategoria','AreSubCategoria.ID=ReProgramacionRecurso.AreSubCategoriaID')
                        ->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')
                        ->innerJoin('Situacion','Situacion.ID=ReProgramacionRecurso.Situacion')
                        ->where('ReProgramacionRecurso.AreSubCategoriaID=:AreSubCategoriaID and ReProgramacionRecurso.Situacion in (1,3,6) AND ReProgramacionRecurso.Estado=1 AND ReProgramacionRecurso.CodigoProyecto=:CodigoProyecto',[':CodigoProyecto'=>$informacionGeneral->Codigo,':AreSubCategoriaID'=>$RecursoID])
                        ->all();
        foreach($resultados as $resultado)
        {
            echo '<tr>';
                echo '<td>';
                    echo $resultado['RubroElegibleID'].'.'.$resultado['Codificacion'];
                echo '</td>';
                echo '<td>';
                    echo $resultado['Nombre'].'/'.$resultado['Especifica'];
                echo '</td>';
                echo '<td>';
                    echo $resultado['Justificacion'];
                echo '</td>';
                echo '<td>';
                    echo $resultado['Descripcion'];
                echo '</td>';
                echo '<td>';
                    if($resultado['SituacionID']==1)
                    {
                        echo '<a href="#" id='.$resultado['ReProgramacionRecursoID'].' class="btn btn-primary btn-ver"><span class="fa fa-eye"></span></a> ';
                        echo ' <a href="#" id='.$resultado['ReProgramacionRecursoID'].' class="btn btn-danger btn-eliminar"> <span class="fa fa-remove"></span></a>';
                    }
                    elseif($resultado['SituacionID']==3)
                    {
                        echo ' <a href="#" id='.$resultado['ReProgramacionRecursoID'].' class="btn btn-primary btn-ver"><span class="fa fa-eye"></span></a>';
                    }
                    elseif($resultado['SituacionID']==6)
                    {
                        echo ' <a href="#" id='.$resultado['ReProgramacionRecursoID'].' class="btn btn-primary btn-ver"><span class="fa fa-eye"></span></a>';
                    }
                echo '</td>';
            echo '</tr>';
        }
    }
    
    
    public function actionListadoReProgramacionRecursos()
    {
        $resultados = (new \yii\db\Query())
                        ->select('ReProgramacionRecurso.ID ReProgramacionRecursoID,ActRubroElegible.*,ReProgramacionRecurso.*,AreSubCategoria.*,Situacion.ID SituacionID,Situacion.Descripcion')
                        ->from('ReProgramacionRecurso')
                        ->innerJoin('AreSubCategoria','AreSubCategoria.ID=ReProgramacionRecurso.AreSubCategoriaID')
                        ->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')
                        ->innerJoin('UsuarioProyecto','UsuarioProyecto.CodigoProyecto=ReProgramacionRecurso.CodigoProyecto')
                        ->innerJoin('Usuario','Usuario.ID=UsuarioProyecto.UsuarioID')
                        ->innerJoin('Situacion','Situacion.ID=ReProgramacionRecurso.Situacion')
                        ->where('ReProgramacionRecurso.Situacion in (1,3) AND ReProgramacionRecurso.Estado=1 AND Usuario.ID=:ID',[':ID'=>\Yii::$app->user->id])
                        ->all();
        foreach($resultados as $resultado)
        {
            echo '<tr>';
                echo '<td>';
                    echo $resultado['CodigoProyecto'];
                echo '</td>';
                echo '<td>';
                    echo $resultado['RubroElegibleID'].'.'.$resultado['Codificacion'];
                echo '</td>';
                echo '<td>';
                    echo $resultado['Nombre'].'/'.$resultado['Especifica'];
                echo '</td>';
                echo '<td>';
                    echo $resultado['Justificacion'];
                echo '</td>';
                echo '<td>';
                    echo $resultado['Descripcion'];
                echo '</td>';
                echo '<td>';
                    if($resultado['SituacionID']==1)
                    {
                        echo '<a href="#" id='.$resultado['ReProgramacionRecursoID'].' class="btn btn-primary btn-confirmar"><span class="fa fa-check"></span></a>';
                        echo ' <a href="#" id='.$resultado['ReProgramacionRecursoID'].' class="btn btn-primary btn-ver"><span class="fa fa-eye"</a>';
                    }
                    elseif($resultado['SituacionID']==3)
                    {
                        
                        echo ' <a href="#" id='.$resultado['ReProgramacionRecursoID'].' class="btn btn-primary btn-ver"><span class="fa fa-eye"</a>';
                    }
                echo '</td>';
            echo '</tr>';
        }
    }
    public function actionEliminarRecurso($ID=null)
    {
        $ReProgramacionRecurso=ReProgramacionRecurso::find()->where('ID=:ID and Estado=1',[':ID'=>$ID])->one();
        $ReProgramacionRecurso->Situacion=6;
        $ReProgramacionRecurso->update();
        $arr = ['Success' => true];
        echo json_encode($arr);
    }
    
    public function actionAprobarRecurso($ID=null)
    {
        $ReProgramacionRecurso=ReProgramacionRecurso::find()->where('ID=:ID and Estado=1',[':ID'=>$ID])->one();
        $CountReProgramacionRecurso=ReProgramacionRecurso::find()->where('ID=:ID and Estado=1',[':ID'=>$ID])->count();
        $ReProgramacionRecurso->Situacion=3;
        $ReProgramacionRecurso->update();
        $CronogramaRecursos=CronogramaAreSubCategoria::find()->where('AreSubCategoriaID=:AreSubCategoriaID',[':AreSubCategoriaID'=>$ReProgramacionRecurso->AreSubCategoriaID])->all();
        if($CountReProgramacionRecurso==1)
        {
            $Recurso=AreSubCategoria::findOne($ReProgramacionRecurso->AreSubCategoriaID);
            $repro=new ReProgramacionRecurso;
            $repro->AreSubCategoriaID=$ReProgramacionRecurso->AreSubCategoriaID;
            $repro->Situacion=8;
            $repro->Estado=1;
            $repro->FechaRegistro=date('Ymd');
            $repro->CodigoProyecto=$ReProgramacionRecurso->CodigoProyecto;
            $repro->save();
            //$CronogramaRecursos=CronogramaAreSubCategoria::find()->where('AreSubCategoriaID=:AreSubCategoriaID',[':AreSubCategoriaID'=>$Recurso->ID])->all();
            Yii::$app->db->createCommand("  insert into DetalleReProgramacionRecurso (ReProgramacionRecursoID,CostoUnitario,MetaFisica,Mes)
                                            select ".$repro->ID.",".$Recurso->CostoUnitario.",MetaFisica,Mes from CronogramaAreSubCategoria where AreSubCategoriaID=".$Recurso->ID."")
                                            ->execute();
        }
        
        foreach($CronogramaRecursos as $CronogramaRecurso)
        {
            $detalle=DetalleReProgramacionRecurso::find()
                        ->where('ReProgramacionRecursoID=:ReProgramacionRecursoID and Mes=:Mes',[':ReProgramacionRecursoID'=>$ReProgramacionRecurso->ID,':Mes'=>$CronogramaRecurso->Mes])->one();
            if($CronogramaRecurso->Mes==$detalle->Mes)
            {
                $CronogramaRecurso->MetaFisica=$detalle->MetaFisica;
                $CronogramaRecurso->CostoUnitario=$detalle->CostoUnitario;
                $CronogramaRecurso->update();
            }
        }
        $arr = ['Success' => true];
        echo json_encode($arr);
        
    }
}
