<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\PasoCritico;
use app\models\Usuario;
use app\models\Persona;
use app\models\Investigador;
use app\models\Proyecto;
use app\models\InformacionGeneral;
use app\models\Poa;
use app\models\Componente;
use app\models\Actividad;
use app\models\CronogramaProyecto;
use app\models\ActRubroElegible;
use app\models\CronogramaComponente;
use app\models\CronogramaActividad;
use app\models\CronogramaActRubroElegible;
use app\models\RubroElegible;
use app\models\AreSubCategoria;
use app\models\CronogramaAreSubCategoria;
use app\models\MarcoLogicoActividad;
use app\models\AreSubCategoriaObservacion;
use app\models\Requerimiento;
use app\models\Experimento;
use yii\web\UploadedFile;


class ExperimentoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($CodigoProyecto=NULL)
    {
        $this->layout='estandar';
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        return $this->render('index',['CodigoProyecto'=>$CodigoProyecto]);
    }
    
    public function actionListaExperimentos($CodigoProyecto=null)
    {
        $resultados = (new \yii\db\Query())
            ->select('Experimento.*')
            ->from('Experimento')
            ->where(['Experimento.CodigoProyecto'=>$CodigoProyecto])
            ->all();
        $nro=0;
        foreach($resultados as $result)
        {
            $nro++;
            echo "<tr>";
            echo "<td> " . $result["ResponsableExperimento"] . "</td>";
            echo "<td> " . $result["Titulo"] . "</td>";
            echo "<td> " . $result["Duracion"] . "</td>";
            echo "<td> " . $result["FechaRegistro"] . "</td>";
            echo "<td><a class='btn btn-default' target='_blank' href='experimentos/" . $result["Documento"] . "'><span class='fa fa-cloud-download'></span> Descargar</a></td>";
            echo "<td><a href='#' class='btn btn-primary btn-edit-experimento' data-id='".$result["ID"]."'><i class='fa fa-edit fa-lg'></i></a> <a href='#' class='btn btn-danger btn-remove' data-id='".$result["ID"]."'><i class='fa fa-remove fa-lg'></i></a></td>";
            echo "</tr>";
        }
    }
    
    public function actionCrear($CodigoProyecto)
    {
        $this->layout='vacio';
        $informacionGeneral=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacionGeneral->InvestigadorID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
        $componentes=Componente::find()->where('PoaID=:PoaID',[':PoaID'=>$poa->ID])->all();
        $experimento=new Experimento;
        if($experimento->load(Yii::$app->request->post())){
            
            
            $experimento->Situacion=0;
            $experimento->Estado=1;
            $experimento->FechaRegistro=date('Ymd h:m:s');
            $experimento->save();
            $experimento->Archivo = UploadedFile::getInstance($experimento, 'Archivo');
            
            if($experimento->Archivo)
            {
                $experimento->Archivo->saveAs('experimentos/' . $experimento->ID . '.' . $experimento->Archivo->extension);
                $experimento->Documento=$experimento->ID . '.' . $experimento->Archivo->extension;
            }
            $experimento->update();
            return $this->redirect(['/experimento']);
        }
        return $this->render('_form',['CodigoProyecto'=>$CodigoProyecto,'experimento'=>$experimento,'componentes'=>$componentes]);
    }
    
    public function actionActualizar($ID=null)
    {
        $this->layout='vacio';
        $experimento=Experimento::find()
                ->select('Experimento.*,Componente.ID ComponenteID')
                ->innerJoin('Actividad','Actividad.ID=Experimento.ActividadID')
                ->innerJoin('Componente','Componente.ID=Actividad.ComponenteID')
                ->where('Experimento.ID=:ID',[':ID'=>$ID])
                ->one();
                
        $informacionGeneral=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$experimento->CodigoProyecto])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacionGeneral->InvestigadorID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
        $componentes=Componente::find()->where('PoaID=:PoaID',[':PoaID'=>$poa->ID])->all();
        $actividades=Actividad::find()->select('ID,Nombre,Correlativo')->where('ComponenteID=:ComponenteID',[':ComponenteID'=>$experimento->ComponenteID])->groupBy('ID,Nombre,Correlativo')->orderBy('Correlativo')->all();
        
        if($experimento->load(Yii::$app->request->post())){
            $experimento->save();
            $experimento->Archivo = UploadedFile::getInstance($experimento, 'Archivo');
            
            if($experimento->Archivo)
            {
                $experimento->Archivo->saveAs('experimentos/' . $experimento->ID . '.' . $experimento->Archivo->extension);
                $experimento->Documento=$experimento->ID . '.' . $experimento->Archivo->extension;
            }
            $experimento->update();
            return $this->redirect(['/experimento']);
        }
        return $this->render('_form_actualizar',['experimento'=>$experimento,'ID'=>$ID,'componentes'=>$componentes,'actividades'=>$actividades]);
    }
    
    
    public function actionActividades()
    {
        if(isset($_POST['ComponenteID']) && trim($_POST['ComponenteID'])!='')
        {
            $ComponenteID=$_POST['ComponenteID'];
            $countActividades=Actividad::find()->select('ID,Nombre')->where('ComponenteID=:ComponenteID',[':ComponenteID'=>$ComponenteID])->groupBy('ID,Nombre')->count();
            $actividades=Actividad::find()->select('ID,Nombre,Correlativo')->where('ComponenteID=:ComponenteID',[':ComponenteID'=>$ComponenteID])->groupBy('ID,Nombre,Correlativo')->orderBy('Correlativo asc')->all();
            if($countActividades>0){
                echo "<option value>Seleccionar</option>";
                foreach($actividades as $actividad){
                    echo "<option value='".$actividad->ID."'>".$actividad->Correlativo." ".$actividad->Nombre."</option>";
                }
            }
            else{
                echo "<option value>Seleccionar</option>";
            }
        }
    }
    
    
    public function actionRecursos()
    {
        if(isset($_POST['ActividadID']) && trim($_POST['ActividadID'])!='')
        {
            $ActividadID=$_POST['ActividadID'];
            
            $countRecursos=AreSubCategoria::find()->select('AreSubCategoria.ID,AreSubCategoria.Nombre')->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')->where('ActRubroElegible.ActividadID=:ActividadID',[':ActividadID'=>$ActividadID])->groupBy('AreSubCategoria.ID,AreSubCategoria.Nombre')->count();
            $recursos=AreSubCategoria::find()->select('AreSubCategoria.ID,AreSubCategoria.Nombre')->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')->where('ActRubroElegible.ActividadID=:ActividadID',[':ActividadID'=>$ActividadID])->groupBy('AreSubCategoria.ID,AreSubCategoria.Nombre,AreSubCategoria.Correlativo')->orderBy('AreSubCategoria.Correlativo')->all();
            if($countRecursos>0){
                echo "<option value>Seleccionar</option>";
                foreach($recursos as $recurso){
                    echo "<option value='".$recurso->ID."'>".$recurso->Nombre."</option>";
                }
            }
            else{
                echo "<option value>Seleccionar</option>";
            }
        }
    }
    
    public function getTipoServicio($tipoServicio=null)
    {
        if($tipoServicio==1)
        {
            return "Servicio";
        }
        else
        {
            return "compra";
        }
    }
    
    public function getSituacion($situacion=null)
    {
        if($situacion==0)
        {
            return "Pendiente de Anexar";
        }
        elseif($situacion==1)
        {
            return "Anexado a Requerimiento";
        }
        else if($situacion==2)
        {
            return "Anexado a Orden de Servicio";
        }
    }
    public function actionEliminar($id=null)
    {
        TerminoReferencia::findOne($id)->delete();
        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
    public function getUbicacion($archivo)
    {
        
    }
}
