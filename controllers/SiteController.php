<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Siaf;
use app\models\DetalleCertificacionPresupuestal;
use app\models\SiafSecuencialProyecto;


class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],

        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->redirect(['panel/index']);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout($q=null,$w=null)
    {
        Yii::$app->user->logout();
        if(isset($q) == 1){
          return $this->redirect(base64_decode($w).'?x=2');
        }else{
          return $this->goHome();
        }
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionItf()
    {
        $this->layout='estandar';
        return $this->render('_informe');
    }

    public function actionOracle()
    {
        $db_test = '(DESCRIPTION=( ADDRESS_LIST= (ADDRESS= (PROTOCOL=TCP) (HOST=172.168.3.12) (PORT=1521)))( CONNECT_DATA=(SID=XE) ))';
        $conn = oci_connect("SIGA01", "SIGA01", $db_test);
        //$query = oci_parse($conn,'SELECT * FROM ESTABLECIMIENTO order by 1 asc');
        $query = oci_parse($conn, 'INSERT INTO ESTABLECIMIENTO (CODI_ESTA_SAL) VALUES (1)');
        $r=oci_execute($query,OCI_NO_AUTO_COMMIT);
        /*
        while ($row = oci_fetch_array($query, OCI_ASSOC)) {
        foreach ($row as $item) {
            echo $item;
        }
            echo "\n";
        }*/
        if (!$r) {
            $e = oci_error($query);
            oci_rollback($conn);  // revertir los cambios en ambas tablas
            trigger_error(htmlentities($e['message']), E_USER_ERROR);
        }
        $r = oci_commit($conn);
        if (!$r) {
            $e = oci_error($conn);
            trigger_error(htmlentities($e['message']), E_USER_ERROR);
        }
        //oci_free_statement($query);
       // oci_close($conn);
    }

    public function actionOracleConsulta()
    {
        $db_test = '(DESCRIPTION=( ADDRESS_LIST= (ADDRESS= (PROTOCOL=TCP) (HOST=172.168.3.12) (PORT=1521)))( CONNECT_DATA=(SID=XE) ))';
        $conn = oci_connect("SIGA01", "SIGA01", $db_test);
        $query = oci_parse($conn,"SELECT FECH_CAMB_TIP,VALO_SOLE_TIP FROM TIPO_CAMBIO WHERE TO_CHAR(FECH_CAMB_TIP,'DD-MM-YYYY')=TO_CHAR(SYSDATE,'DD-MM-YYYY')");
        oci_execute($query);
        while ($row = oci_fetch_array($query, OCI_ASSOC)) {
            echo $row["VALO_SOLE_TIP"]."  ";
        }
        oci_free_statement($query);
        oci_close($conn);
    }

    public function Con($nombrestore,$arraydatos=null)
    {
        /*
        if($arrayconeccion == null){
            $arrayconeccion = $this->coneccion_sismethaweb();
        }

        $usuario = $arrayconeccion['usuario'];
        $clave = $arrayconeccion['clave'];
        $bd = $arrayconeccion['bd'];
        $tipo = $arrayconeccion['tipo'];
        */
        $usuario = "SIGA01";
        $clave = "SIGA01";
        $bd = '(DESCRIPTION=( ADDRESS_LIST= (ADDRESS= (PROTOCOL=TCP) (HOST=172.168.3.12) (PORT=1521)))( CONNECT_DATA=(SID=XE) ))';

        if (function_exists('oci_connect')) {
                $conec =  oci_connect($usuario, $clave, $bd);
        } else {
                echo "Las funciones de oci_connect no están disponibles.<br />\n";
        }

        if (!$conec) {
            echo 'Error en conecci&oacute;n a la BASE DE DATOS oracle...<br>';
        }	else{
                $stid = oci_parse ($conec, "alter session set nls_date_format = 'dd/mm/yyyy'");
                oci_execute ($stid);
        }

        $curs = oci_new_cursor($conec);

        $variables = "";
        for($i=0;$i<count($arraydatos);$i++){
            $variables .= $arraydatos[$i][0].',';
        }

        $ins =  "begin ".$nombrestore."(".$variables.":io_cursor);end;";
        //var_dump($ins);die;
        $stmt = oci_parse($conec, $ins);

        //oci_bind_by_name($stmt, ":p_cip", $cip);
        $v_clob = '';
        for($i=0;$i<count($arraydatos);$i++){
            //oci_bind_by_name($stmt, $datos[$i][0], $datos[$i][1]);
            if(count($arraydatos[$i])==2){
                oci_bind_by_name($stmt, $arraydatos[$i][0], $arraydatos[$i][1]);
            }else{
                if($arraydatos[$i][2] == 'OCI_B_CLOB'){
                    $clob = oci_new_descriptor($conec, OCI_D_LOB);
                    $v_clob = $arraydatos[$i][1];
                    oci_bind_by_name($stmt, $arraydatos[$i][0], $clob , -1 , OCI_B_CLOB );
                    $clob->WriteTemporary($v_clob);
                }
            }
        }
        oci_bind_by_name($stmt, ":io_cursor", $curs, -1, OCI_B_CURSOR);
        oci_execute($stmt, OCI_DEFAULT);
        oci_commit($conec);
        oci_execute($curs);
        $contador=0;
        $dato = null;
        while ($data = oci_fetch_row($curs)) {
            for($i = 0 ; $i<count($data);$i++){
                $arraydata[$i] = $data[$i];
            }
        $dato[$contador] = $arraydata;
            $contador++;
        }
        oci_free_statement($stmt);
        oci_free_statement($curs);

        if($v_clob != '')
            $clob->free();

        oci_close($conec);

        return $dato;
    }


    public function Con2($nombrestore,$arraydatos=null)
    {
        $usuario = "SIGA01";
        $clave = "SIGA01";
        $bd = '(DESCRIPTION=( ADDRESS_LIST= (ADDRESS= (PROTOCOL=TCP) (HOST=172.168.3.12) (PORT=1521)))( CONNECT_DATA=(SID=XE) ))';

        if (function_exists('oci_connect')) {
            $conec =  oci_connect($usuario, $clave, $bd);
        } else {
            echo "Las funciones de oci_connect no están disponibles.<br />\n";
        }

        if (!$conec) {
            echo 'Error en conecci&oacute;n a la BASE DE DATOS oracle...<br>';
        }else{
            $stid = oci_parse ($conec, "alter session set nls_date_format = 'dd/mm/yyyy'");
            oci_execute ($stid);
        }


        $stid = oci_parse($conec, 'BEGIN SIAF_INTERFASE.TRANSFERIR_DATOS_CERT_PIP2 (:l_coddoc, :l_numdoc, :l_fecdoc, :l_anio, :l_concepto, :l_tipoid,:l_ruc,:l_fuente,:l_tcambio,:l_siafcer,:l_siafsec,:l_siafcor,:l_intfcer,:l_intfsec,:l_intfcor,:l_tipofinan,:l_correlativotabla,:l_meta,:l_montototal,:l_clasificador, :l_secuencial); END;');

        $data="";
        $intf=NULL;
        $intf1=NULL;
        $intf2=NULL;
        oci_bind_by_name($stid, ':l_coddoc', $arraydatos[0][1]);
        oci_bind_by_name($stid, ':l_numdoc', $arraydatos[1][1]);
        oci_bind_by_name($stid, ':l_fecdoc', $arraydatos[2][1]);
        oci_bind_by_name($stid, ':l_anio', $arraydatos[3][1]);
        oci_bind_by_name($stid, ':l_concepto', $arraydatos[4][1]);
        oci_bind_by_name($stid, ':l_tipoid', $arraydatos[5][1]);
        oci_bind_by_name($stid, ':l_ruc', $arraydatos[6][1]);
        oci_bind_by_name($stid, ':l_fuente', $arraydatos[7][1]);
        oci_bind_by_name($stid, ':l_tcambio', $arraydatos[8][1]);
        oci_bind_by_name($stid, ':l_siafcer', $arraydatos[9][1]);
        oci_bind_by_name($stid, ':l_siafsec', $arraydatos[10][1]);
        oci_bind_by_name($stid, ':l_siafcor', $arraydatos[11][1]);
        oci_bind_by_name($stid, ':l_intfcer', $intf);
        oci_bind_by_name($stid, ':l_intfsec', $intf1);
        oci_bind_by_name($stid, ':l_intfcor', $intf2);
        oci_bind_by_name($stid, ':l_tipofinan', $arraydatos[15][1]);
        oci_bind_by_name($stid, ':l_correlativotabla', $arraydatos[16][1]);
        oci_bind_by_name($stid, ':l_meta', $arraydatos[17][1]);
        oci_bind_by_name($stid, ':l_montototal', $arraydatos[18][1]);
        oci_bind_by_name($stid, ':l_clasificador', $arraydatos[19][1]);
        oci_bind_by_name($stid, ':l_secuencial',$data,50);
        //$refcur = oci_new_cursor($conec);
        //1oci_bind_by_name($stid, ':REFCUR', $refcur, -1, OCI_B_CURSOR);
        oci_execute($stid);
        oci_free_statement($stid);
        oci_close($conec);
        return 	$data;
    }
    public function actionUtilizar()
    {
        ///coddoc=> 031 orden de compra
        $detalles = (new \yii\db\Query())
                ->select('*,Orden.Correlativo OrdenCorrelativo')
                ->from('CompromisoPresupuestal')
                ->innerJoin('DetalleCompromisoPresupuestal','DetalleCompromisoPresupuestal.CompromisoPresupuestalID=CompromisoPresupuestal.ID')
                ->innerJoin('Orden','Orden.ID=DetalleCompromisoPresupuestal.Correlativo')
                ->distinct()
                ->all();
        $certificado=DetalleCertificacionPresupuestal::find()
                    ->innerJoin('CertificacionPresupuestal','CertificacionPresupuestal.ID=DetalleCertificacionPresupuestal.CertificacionPresupuestalID')
                    ->where('DetalleCertificacionPresupuestal.Codigo=:Codigo',[':Codigo'=>'114_PTT'])
                    ->one();

        $siafSecuencial=SiafSecuencialProyecto::find()
                                        ->where('CodigoProyecto=:CodigoProyecto',[':CodigoProyecto'=>$certificado->Codigo])
                                        ->one();
        $contenido='';
        foreach($detalles as $detalle)
        {
            $siaf=new Siaf;
            $siaf->SiafCertificado=str_pad($certificado->CodigoCertificacion, 10, "0", STR_PAD_LEFT);
            $siaf->InterfaceCertificado=str_pad($siaf->InterfaceCertificado, 10, "0", STR_PAD_LEFT);
            if($detalle["TipoGasto"]==3)
            {
                $siaf->CodigoDocumento="031";
                $siaf->TipoID=1;
                $siaf->NumeroDocumento=str_pad($detalle["OrdenCorrelativo"], 3, "0", STR_PAD_LEFT)." ".$detalle["Annio"];
                $siaf->FechaDocumento=date('d/m/Y',strtotime($detalle["FechaOrden"]));
                $siaf->AnioDocumento=$detalle["Annio"];
                $siaf->ConceptoDocumento=substr ( $detalle["Concepto"] , 0 ,249 );
                $siaf->RUCDocumento=$detalle["RUC"];
                $siaf->TipoCambioDocumento=$detalle["TipoCambio"];
                $siaf->MontoTotal=$detalle["Monto"];
                $siaf->Clasificador='26 7 1 6 2';
            }
            $siaf->SiafSecuencial=str_pad($siafSecuencial->Secuencial, 4, "0", STR_PAD_LEFT);
            $siaf->Correlativo=str_pad($detalle["OrdenCorrelativo"], 12, "0", STR_PAD_LEFT);
            //var_dump($siaf->Correlativo);die;
            $ns = 'SIAF_INTERFASE.TRANSFERIR_DATOS_CERT_PIP2';
            $psest[] = array(':l_coddoc',$siaf->CodigoDocumento);
            $psest[] = array(':l_numdoc',$siaf->NumeroDocumento);
            $psest[] = array(':l_fecdoc',$siaf->FechaDocumento);
            $psest[] = array(':l_anio',$siaf->AnioDocumento);
            $psest[] = array(':l_concepto',$siaf->ConceptoDocumento);
            $psest[] = array(':l_tipoid',$siaf->TipoID);
            $psest[] = array(':l_ruc',$siaf->RUCDocumento);
            $psest[] = array(':l_fuente',$siaf->FuenteFinanciamiento);
            $psest[] = array(':l_tcambio',$siaf->TipoCambioDocumento);
            $psest[] = array(':l_siafcer',$siaf->SiafCertificado);
            $psest[] = array(':l_siafsec',$siaf->SiafSecuencial);
            $psest[] = array(':l_siafcor',$siaf->SiafCorrelativo);
            $psest[] = array(':l_intfcer',$siaf->InterfaceCertificado);
            $psest[] = array(':l_intfsec',$siaf->InterfaceSecuencial);
            $psest[] = array(':l_intfcor',$siaf->InterfaceCorrelativo);
            $psest[] = array(':l_tipofinan',$siaf->TipoFinanciamiento);
            $psest[] = array(':l_correlativotabla',$siaf->Correlativo);
            $psest[] = array(':l_meta',$siaf->MetaFinanciera);
            $psest[] = array(':l_montototal',$siaf->MontoTotal);
            $psest[] = array(':l_clasificador',$siaf->Clasificador);

            $datosestaciones = $this->Con2($ns,$psest);

            $siafSecuencial->Secuencial=$siafSecuencial->Secuencial+1;
            //var_dump($contenido);die;
        }
        $siafSecuencial->update();

        var_dump($datosestaciones);
    }

    public function actionDbf()
    {
        require(Yii::$app->basePath . '/web/php-xbase-master/src/XBase/Table.php');
        //require_once('odbc.php');

        $table = new Table(dirname(__FILE__).'/test.dbf');

        while ($record = $table->nextRecord()) {
            echo $record->my_column;
        }
    }


    public function actionGetDni($dni){
        try
        {


                    $Personas=$PersonasModel->findOneBy(array('dni'=>$dni));
                    if($Personas && $Personas->getPide()=="1" && $Personas->getSexo()<>null){

                        return array(   "estado"=>true,
                                    "datos"=>array("nombres"=>$Personas->getNombres(),"app"=>$Personas->getApellidoPaterno(),"apm"=>$Personas->getApellidoMaterno()),
                                    "persona"=>$Personas);

                    }elseif ($Personas && ($Personas->getPide()=="0" || $Personas->getSexo()==null)) {

                        if($this->ticket==0){
                            $valida=$this->getTicket();
                            if(!$valida){
                                /*return array("estado"=>false,"code"=>$this->ticket,"mensaje"=>$this->errorCode($this->ticket));*/
                                return array(   "estado"=>true,
                                                "datos"=>array("nombres"=>$Personas->getNombres(),"app"=>$Personas->getApellidoPaterno(),"apm"=>$Personas->getApellidoMaterno()),
                                                "persona"=>$Personas);
                            }
                        }
                            $result1 = $this->obtieneDatos($dni);
                            if($this->valida($result1)){

                                $xml = simplexml_load_string($result1);
                                $json = json_encode($xml);
                                $result1 = json_decode($json,TRUE);
                                //$Personas= new \Application\Entity\Personas();
                                if($result1["RESPUESTA"]["NOMBRES"]==""){
                                    return array("estado"=>false,"mensaje"=>"por favor volver a ingresar el DNI");
                                }
                                if(isset($result1["RESPUESTA"]["APMAT"])){
                                        $app=utf8_decode(trim($result1["RESPUESTA"]["APMAT"]));
                                    }else{$app="";}
                                    $Personas->setNombres(trim(utf8_decode($result1["RESPUESTA"]["NOMBRES"])))
                                            ->setApellidoPaterno(utf8_decode(trim($result1["RESPUESTA"]["APPAT"])))
                                            ->setApellidoMaterno($app)
                                            ->setPide(true)
                                            ->setFechaPide(new \DateTime())
                                            ->setFechaNac(trim($result1["RESPUESTA"]["FENAC"]))
                                            ->setSexo(trim($result1["RESPUESTA"]["SEXO"]));
                                    $em->persist($Personas);
                                    $em->flush();

                                return array(   "estado"=>true,
                                                "datos"=>array("nombres"=>$Personas->getNombres(),"app"=>$Personas->getApellidoPaterno(),"apm"=>$Personas->getApellidoMaterno()),
                                                "persona"=>$Personas);
                            }else{
                                if($result1=="-2"){
                                    $this->getTicket(1);
                                    return $this->getDni($dni);
                                }
                                /*return array("estado"=>false,"code"=>$result1,"mensaje"=>$this->errorCode($result1));*/
                                return array(   "estado"=>true,
                                                "datos"=>array("nombres"=>$Personas->getNombres(),"app"=>$Personas->getApellidoPaterno(),"apm"=>$Personas->getApellidoMaterno()),
                                                "persona"=>$Personas);
                                }

                    }  elseif (!$Personas) {
                        /*JVT*/
                        //return array("estado"=>false,"code"=>$this->ticket,"mensaje"=>"error JVT");
                        /*FIN JVT*/
                        if($this->ticket==0){
                            $valida=$this->getTicket();
                            if(!$valida){
                                return array("estado"=>false,"code"=>$this->ticket,"mensaje"=>$this->errorCode($this->ticket));
                            }
                        }
                            $result1 = $this->obtieneDatos($dni);
                            if($this->valida($result1)){

                                $xml = simplexml_load_string($result1);
                                $json = json_encode($xml);
                                $result1 = json_decode($json,TRUE);
                                if($result1["RESPUESTA"]["NOMBRES"]==""){
                                    return array("estado"=>false,"mensaje"=>"por favor volver a ingresar el DNI");
                                }
                                    $PersonaN= new \Application\Entity\Personas();
                                    if(isset($result1["RESPUESTA"]["APMAT"])){
                                        $app=utf8_decode(trim($result1["RESPUESTA"]["APMAT"]));
                                    }else{$app="";}
                                    $PersonaN->setDni($dni)
                                            ->setNombres(utf8_decode(trim($result1["RESPUESTA"]["NOMBRES"])))
                                            ->setApellidoPaterno(utf8_decode(trim($result1["RESPUESTA"]["APPAT"])))
                                            ->setApellidoMaterno($app)
                                            ->setPide(true)
                                            ->setFechaPide(new \DateTime())
                                            ->setFechaNac(trim($result1["RESPUESTA"]["FENAC"]))
                                            ->setSexo(trim($result1["RESPUESTA"]["SEXO"]));;
                                    $em->persist($PersonaN);
                                    $em->flush();
                                    $Personas=$PersonaN;
                                return array(   "estado"=>true,
                                                "datos"=>array("nombres"=>$Personas->getNombres(),"app"=>$Personas->getApellidoPaterno(),"apm"=>$Personas->getApellidoMaterno()),
                                                "persona"=>$Personas);
                            }else{
                                if($result1=="-2"){
                                    $this->getTicket(1);
                                    return $this->getDni($dni);
                                }elseif($result1=="-5") {
                                    $this->getTicket(1);
                                    return $this->getDni($dni);
                                }  else {
                                    return array("estado"=>false,"code"=>$result1,"mensaje"=>$this->errorCode($result1));
                                }

                                }

                    }
                    else{
                        return array("estado"=>false);
                    }



        }catch(\SoapFault  $fault){
                //trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
                return array("estado"=>false);
        }

        }

    public function getDatosPersonales($dni){
        if($this->ticket==0){
            $valida=$this->getTicket();
            if(!$valida){
                return array("estado"=>false,"code"=>$this->ticket,"mensaje"=>$this->errorCode($this->ticket));
                }
        }
        $result1 = $this->obtieneDatos($dni);

        if($this->valida($result1)){
            return $result1;
        }else{
            if($result1=="-2"){
                $this->getTicket(1);
                return $this->getDni($dni);
            }
            return array("estado"=>false,"code"=>$result1,"mensaje"=>$this->errorCode($result1));
            }

    }

    private function getTicket($val=0) {

        try {
            $controller= $this->getController();
            $em = $controller->em()->getEntityManager();
            $AlertasModel=$em->getRepository('Application\Entity\Alertas');
            $Alerta=$AlertasModel->find(1);
            if($val==0){
            $this->ticket= $Alerta->getDescripcion();
            return true;
            }else{
                try{
                    //error_log("prue Else ".$val, 0);
                $old = ini_get('default_socket_timeout');
                ini_set('default_socket_timeout', 3);
                $client = new \SoapClient($this->urlPide.'reniec2/WSAuthentication?wsdl',
                                            array('location' => $this->urlPide.'reniec2/WSAuthentication',"trace" => true,'connection_timeout'=> 3));
                $param=array("user"=>$this->user,"password"=>$this->password);
                ini_set('default_socket_timeout', $old);

                $result = $client->getTicket($param);
                }  catch (\Exception $e){
                    return false;
                }
                if(isset($result->return)){
                    $result=$result->return;
                }
                if($this->valida($result)){
                    //$Alerta= new Alertas();
                    $Alerta->setDescripcion($result)
                            ->setFecha(new \DateTime());
                    $em->persist($Alerta);
                    $em->flush();
                    return true;
                }

                $this->ticket=$result;
                return false;
            }
        }catch(\SoapFault  $fault){
            $this->ticket=0;
            //trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
            return false;


        }

    }

    private function obtieneDatos($dni){
        try{

             $para1=array("xmlDocumento"=>"<IN>
                                    <CONSULTA>
                                        <DNI>".$dni."</DNI>
                                    </CONSULTA>
                                    <IDENTIFICACION>
                                        <CODUSER>N00003</CODUSER>
                                        <CODTRANSAC>5</CODTRANSAC>
                                        <CODENTIDAD>03</CODENTIDAD>
                                        <SESION>".$this->ticket."</SESION>
                                    </IDENTIFICACION>
                                    </IN>");

                $old = ini_get('default_socket_timeout');
                ini_set('default_socket_timeout', 3);
                $client1 = new \SoapClient($this->urlPide.'reniec/WSDataVerificationBinding?wsdl',
                                        array('location' => $this->urlPide.'reniec/WSDataVerificationBinding','connection_timeout'=> 3,'exceptions' => true));
                ini_set('default_socket_timeout', $old);
            $result1 = $client1->getDataValidate($para1);

            if(isset($result1->return)){
                $result1=$result1->return;
            }
            return $result1;

        }  catch (\Exception $e){
            //error_log("Posible Error de Conexion a webservice comunicarse con ONGEI ".$e, 0);
            $this->ticket=0;
            return "false";
        }   catch(\SoapFault  $fault){
            $this->ticket=0;
            return "false";
                //trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);

        }

    }

    private function valida($valor){
        if(strlen($valor)<5){
            //error_log("comprobar valor: ".$valor, 0);
            return false;
        }  elseif ($valor=="false") {
            return false;
        }
        else{
            return true;
        }
    }

    private function errorCode($param) {
        switch ($param) {
            case "-1": $mensaje= "Error en el Servidor"; break;
            case "-2": $mensaje= "Sesión Expirada"; break;
            case "-3": $mensaje= "Excedió  el  máximo  nro  de  consultas  por minuto"; break;
            case "-4": $mensaje= "Código de operación no existe"; break;
            case "-5": $mensaje= "Usuario Invalido";  break;
            case "-6": $mensaje= "No se puede acceder  al servicio en esta fecha"; break;
            case "-7": $mensaje= "Formato de DNI no valido"; break;
            case "-8": $mensaje= "No existe DNI en base de datos de RENIEC"; break;
            case "-9": $mensaje= "Data incompleta en documento XML";  break;
            case "-10": $mensaje= "No es un documento XML"; break;
            default: $mensaje=""; break;
        }
        return $mensaje;
    }

    public function sendSms(){

        try {

        $para1["sender"]="SGP";
        $para1["subject"]="MENSAJE PARA TIC-SGP";
        $para1["message"]=" Fecha: ".date("Y-m-d H:i:s");
        $para1["receiver"]="984123717, 973817862, 986671703, 989447237, 971067879";
        $para1["key"]="01BEF2B50A686AAB1317B27BB77B86425F7A9DCF";
        $para1["login"]="psBYBHvEqVsy";
            try{

                $old = ini_get('default_socket_timeout');
                ini_set('default_socket_timeout', 5);
                $client1 = new \SoapClient('http://ws1.pide.gob.pe:9900/rest/services/uddi:3a89f428-5016-11e5-81fd-c2cc99e5a83d/wsdl',
                                            array('location' => 'http://ws1.pide.gob.pe/SMSService',"trace" => true,'connection_timeout'=> 5));

                $result1 = $client1->SendSMS($para1);
                ini_set('default_socket_timeout', $old);
                return $result1;

            }  catch (\Exception $e){
               // error_log("Posible Error de Conexion a webservice comunicarse con ONGEI".$e, 0);
                $this->ticket=0;
                return "false";
            }

        }catch(\SoapFault  $fault){
            $this->ticket=0;
            return "false";
                //trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);

        }

    }

}
