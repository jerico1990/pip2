<?php

namespace app\controllers;

use Yii;
use app\models\Usuario;
use app\models\UsuarioSearch;
use app\models\Persona;


use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UsuarioController implements the CRUD actions for Usuario model.
 */
class UsuarioController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $this->layout='estandar';
        if(\Yii::$app->user->identity->rol == 7){
            return $this->redirect(['panel/index']);
        }
        $searchModel = new UsuarioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        if(\Yii::$app->user->identity->rol == 7){
            return $this->redirect(['panel/index']);
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $this->layout='estandar';
        if(\Yii::$app->user->identity->rol == 7){
            return $this->redirect(['panel/index']);
        }
        $model = new Usuario();
        $personas=Persona::find()->all();
        if ($model->load(Yii::$app->request->post())) {
            $model->password = Yii::$app->getSecurity()->generatePasswordHash($model->password);
            $model->save();
            return $this->redirect(['/usuario']);
        }
    }

    public function actionUpdate($id)
    {
        $this->layout='estandar';
        if(\Yii::$app->user->identity->rol == 7){
            return $this->redirect(['panel/index']);
        }
        $model = $this->findModel($id);
        $personas=Persona::find()->all();
        // echo '<pre>';print_r(Yii::$app->request->post()); 
        if ($model->load(Yii::$app->request->post()) ) {
            $model->password = Yii::$app->getSecurity()->generatePasswordHash($model->password);
            $model->save();
            return $this->redirect(['/usuario']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'personas'=>$personas
            ]);
        }
    }


    public function actionEliminar($id)
    {

        $this->findModel($id)->delete();
        $json = array('Success' => true );
        echo json_encode($json);
    }


    protected function findModel($id)
    {
        if (($model = Usuario::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionCambiarContrasena($ID=null)
    {
        $this->layout='vacio';
        $usuario=Usuario::find()->where('ID=:ID',[':ID'=>$ID])->one();
        if($usuario->load(Yii::$app->request->post()))
        {
            $usuario->password=Yii::$app->getSecurity()->generatePasswordHash($usuario->Contrasena);
            $usuario->CodigoVerificacion='1';
            $usuario->update();
            Yii::$app->session->setFlash('CambioContrasena');
            
            return $this->redirect(['panel/index']);
        }
        return $this->render('cambiar-contrasena',['usuario'=>$usuario]);
    }


    public function actionListaUsuario()
    {
        $resultados = (new \yii\db\Query())
            ->select('Usuario.*,Persona.ApellidoPaterno,Persona.ApellidoMaterno,Persona.Nombre,Persona.Email')
            ->from('Usuario')
            ->innerJoin('Persona','Persona.ID=Usuario.PersonaID')
            ->orderBy('Usuario.ID desc')
            ->all();

        $nro=0;
        foreach($resultados as $result)
        {
            $nro++;
            $color='';
            echo "<tr >";
                echo "<td> " . $result["ID"] . "</td>";
                echo "<td> " . $result["username"] . "</td>";
                echo "<td> " . $result["ApellidoPaterno"].' '.$result["ApellidoMaterno"].' '.$result["Nombre"] . "</td>";
                echo "<td> " . $result["Email"] . "</td>";
                echo "<td><a href='#' class='btn btn-primary btn-edit' data-id='".$result["ID"]."'><i class='fa fa-edit fa-lg'></i></a> <a href='#' class='btn btn-danger btn-remove' data-id='".$result["ID"]."'><i class='fa fa-remove fa-lg'></i></a> </td>";
            echo "</tr>";
        }
    }


    public function actionCrear($id= null){
        $this->layout='vacio';
        
        $persona = Persona::find()->all();
        if(!is_null($id)){
            $model = Usuario::find()->where('ID=:ID',[':ID'=>$id])->one();
        }else{
            $model = array();
        }

        return $this->render('create',['persona'=>$persona,'model'=>$model]);
    }


}
