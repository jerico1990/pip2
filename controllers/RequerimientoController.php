<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\PasoCritico;
use app\models\Usuario;
use app\models\Persona;
use app\models\Investigador;
use app\models\Proyecto;
use app\models\InformacionGeneral;
use app\models\Poa;
use app\models\Componente;
use app\models\Actividad;
use app\models\CronogramaProyecto;
use app\models\ActRubroElegible;
use app\models\CronogramaComponente;
use app\models\CronogramaActividad;
use app\models\CronogramaActRubroElegible;
use app\models\RubroElegible;
use app\models\AreSubCategoria;
use app\models\CronogramaAreSubCategoria;
use app\models\MarcoLogicoActividad;
use app\models\AreSubCategoriaObservacion;
use app\models\Requerimiento;
use app\models\TerminoReferencia;
use app\models\CompromisoPresupuestal;
use app\models\DetalleCompromisoPresupuestal;
use app\models\UsuarioRol;
use app\models\DetalleRequerimiento;
use app\models\Situacion;
use app\models\RequerimientoEncargo;
use app\models\RequerimientoCajaChica;
use app\models\TipoGasto;
use app\models\CajaChica;
use app\models\DetalleCajaChica;
use app\models\Viatico;
use app\models\Encargo;
use app\models\DetalleEjecucion;
use app\models\SeguimientoCompromisoPresupuestal;
use app\models\SeguimientoRequerimiento;
use yii\web\UploadedFile;

use app\models\Orden;
class RequerimientoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($CodigoProyecto=NULL)
    {
        $this->layout='estandar';
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
            $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        }
        if($proyecto->Situacion!=1)
        {
            return $this->redirect(['panel/index']);
        }
        return $this->render('index',['CodigoProyecto'=>$CodigoProyecto]);
    }
    
    public function actionListaRequerimientos($CodigoProyecto=null)
    {
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        
        $resultados = (new \yii\db\Query())
            ->select('Requerimiento.*')
            ->from('InformacionGeneral')
            ->innerJoin('Investigador','InformacionGeneral.InvestigadorID=Investigador.ID')
            ->innerJoin('Usuario','Usuario.ID=Investigador.UsuarioID')
            ->innerJoin('Proyecto','Proyecto.InvestigadorID=Investigador.ID')
            ->innerJoin('Requerimiento','Requerimiento.CodigoProyecto=InformacionGeneral.Codigo')
            ->where('Requerimiento.Estado=1 AND InformacionGeneral.Codigo=:Codigo AND Requerimiento.Situacion !=6',[':Codigo'=>$CodigoProyecto] )
            ->orderBy('Requerimiento.Correlativo asc')
            ->distinct()
            ->all();

        $nro=0;
        foreach($resultados as $result)
        {
            $nro++;
            $color='';
            if($result["Situacion"]==3)
            {
                $color='#00794C';
            }
            elseif($result["Situacion"]==6)
            {
                $color='#E20612';
            }elseif($result["Situacion"]==1)
            {
                $color='#2460AA';
            }
            
            
            
            echo "<tr >";
            
            echo "<td > " . $this->getTipoServicio($result["TipoRequerimiento"])  . "</td>";
            echo "<td> Requerimiento N° " . str_pad($result["Correlativo"], 3, "0", STR_PAD_LEFT)  ."-".$result["Annio"]."</td>";
            echo "<td> " . $result["Referencia"] . "</td>";
            echo "<td>" . Yii::$app->tools->dateFormat($result["FechaSolicitud"]) . "</td>";
            echo "<td style='color:$color'> <b>" . $this->getSituacion($result["Situacion"]) . "</b></td>";
           
            if($result["Documento"])
            {
                echo "<td><a class='btn btn-default' target='_blank' href='requerimientos/" . $result["Documento"] . "'><span class='fa fa-cloud-download'></span> Descargar</a></td>";
            }
            else
            {
                echo "<td></td>";   
            }
            
            if($result["Situacion"]==1)
            {
                echo "<td><a href='#' class='btn btn-info enviar-requerimiento' data-id='".$result["ID"]."'>ENVIAR</a> <a href='#' class='btn btn-primary btn-edit-requerimiento' data-id='".$result["ID"]."'><i class='fa fa-edit fa-lg'></i></a> <a href='#' class='btn btn-danger btn-remove' data-id='".$result["ID"]."'><i class='fa fa-remove fa-lg'></i></a> </td>";
            }
            elseif($result["Situacion"]==3)
            {
                echo "<td ><a  href='#' class='btn btn-info btn-requerimiento-ver' data-id='".$result["ID"]."'><i class='fa fa-eye fa-lg'></i></a> <a class='btn btn-".$this->verificaDetalleCrear($result["ID"],$result["TipoRequerimiento"])."' href='requerimiento/generar?ID=".$result["ID"]."'  data-id='".$result["ID"]."'><i class='fa fa-cogs fa-lg'></i></a> </a> ".$this->verificarCompromisoPresupuestal($result["ID"])."</td>";
            }
            elseif($result["Situacion"]==4)
            {
                echo "<td><a href='#' class='btn btn-info enviar-requerimiento' data-id='".$result["ID"]."'>ENVIAR</a> <a href='#' class='btn btn-primary btn-edit-requerimiento' data-id='".$result["ID"]."'><i class='fa fa-edit fa-lg'></i></a> <a href='#' class='btn btn-danger btn-remove' data-id='".$result["ID"]."'><i class='fa fa-remove fa-lg'></i></a> </td>";
            }
            else
            {
                echo "<td><a href='#' class='btn btn-info btn-requerimiento-ver' data-id='".$result["ID"]."'><i class='fa fa-eye fa-lg'></i></td>";
            }
            echo "</tr>";
        }
    }
    
    
    public function actionListaRequerimientosUafsi()
    {
        if(\Yii::$app->user->identity->rol==2)
        {
            $resultados = (new \yii\db\Query())
            ->select('Requerimiento.*,UnidadOperativa.Nombre UnidadOperativaNombre')
            ->from('InformacionGeneral')
            ->innerJoin('UnidadOperativa','UnidadOperativa.ID=InformacionGeneral.UnidadOperativaID')
            ->innerJoin('Investigador','InformacionGeneral.InvestigadorID=Investigador.ID')
            ->innerJoin('Usuario','Usuario.ID=Investigador.UsuarioID')
            ->innerJoin('Proyecto','Proyecto.InvestigadorID=Investigador.ID')
            ->innerJoin('UsuarioProyecto','UsuarioProyecto.CodigoProyecto=InformacionGeneral.Codigo')
            ->innerJoin('Requerimiento','Requerimiento.CodigoProyecto=InformacionGeneral.Codigo')
            ->where(['Requerimiento.Estado'=>1,'UsuarioProyecto.UsuarioID'=>\Yii::$app->user->id])
            ->orderBy('Requerimiento.Correlativo asc')
            ->distinct()
            ->all();
        }
        else
        {
            $resultados = (new \yii\db\Query())
            ->select('Requerimiento.*,UnidadOperativa.Nombre UnidadOperativaNombre')
            ->from('InformacionGeneral')
            ->innerJoin('UnidadOperativa','UnidadOperativa.ID=InformacionGeneral.UnidadOperativaID')
            ->innerJoin('Investigador','InformacionGeneral.InvestigadorID=Investigador.ID')
            ->innerJoin('Usuario','Usuario.ID=Investigador.UsuarioID')
            ->innerJoin('Proyecto','Proyecto.InvestigadorID=Investigador.ID')
            ->innerJoin('Requerimiento','Requerimiento.CodigoProyecto=InformacionGeneral.Codigo')
            ->where(['Requerimiento.Estado'=>1])
            ->orderBy('Requerimiento.Correlativo asc')
            ->distinct()
            ->all();
        }
        
        $nro=0;
        foreach($resultados as $result)
        {
            $nro++;
            $color='';
            if($result["Situacion"]==3)
            {
                $color='#00794C';
            }
            elseif($result["Situacion"]==6)
            {
                $color='#E20612';
            }elseif($result["Situacion"]==1)
            {
                $color='#2460AA';
            }
            
            
            
            echo "<tr >";
            echo "<td > " . $result["UnidadOperativaNombre"]  . "</td>";
            echo "<td > " . $result["CodigoProyecto"]  . "</td>";
            echo "<td> " . $this->getTipoServicio($result["TipoRequerimiento"])  . "</td>";
            echo "<td> Requerimiento N° " . str_pad($result["Correlativo"], 3, "0", STR_PAD_LEFT)  ."-".$result["Annio"]."</td>";
            echo "<td> " . $result["Referencia"] . "</td>";
            echo "<td>" . Yii::$app->tools->dateFormat($result["FechaSolicitud"]) . "</td>";
            echo "<td style='color:$color'> <b>" . $this->getSituacion($result["Situacion"]) . "</b></td>";
            echo "<td style='color:$color'> <b>" . $this->getUbicacion($result["Situacion"]) . "</b></td>";
           
            if($result["Documento"])
            {
                echo "<td><a class='btn btn-default' target='_blank' href='".\Yii::$app->request->BaseUrl."/requerimientos/" . $result["Documento"] . "'><span class='fa fa-cloud-download'></span> Descargar</a></td>";
            }
            else
            {
                echo "<td></td>";   
            }
            
            echo "<td><a href='#' class='btn btn-info btn-requerimiento-ver' data-id='".$result["ID"]."'><i class='fa fa-eye fa-lg'></i></a> <a class='btn btn-info btn-requerimiento-ver-ubicacion' data-id='".$result["ID"]."'>Ubicación</a></td>";
            
            echo "</tr>";
        }
    }

    public function verificaDetalleCrear($id,$tipo){
        if($tipo == 1 || $tipo == 2 || $tipo == 3){
            $da = Orden::find()->where(['RequerimientoID'=>$id,'Estado'=>1])->one();
        }elseif($tipo == 4){
            $da = Viatico::find()->where(['RequerimientoID'=>$id,'Estado'=>1])->one();
        }elseif($tipo == 5){
            $da = DetalleCajaChica::find()->where(['RequerimientoID'=>$id,'Estado'=>1])->one();
        }elseif($tipo == 6){
            $da = Encargo::find()->where(['RequerimientoID'=>$id,'Estado'=>1])->one();
        }
        if(count($da) != 0){
            return 'info';
        }else{
            return 'danger';
        }
    }
    
    public function verificarCompromisoPresupuestal($id)
    {
        $compromiso=CompromisoPresupuestal::find()->where('RequerimientoID=:RequerimientoID and Estado=1',[':RequerimientoID'=>$id])->one();
        if(!$compromiso)
        {
            return "<a href='#' data-id='".$id."' class='btn btn-danger btn-eliminar btn-retraer'>Retraer</a>";
        }
        return "";
    }
    
    public function actionCrear($CodigoProyecto)
    {
        $this->layout='vacio';
        $tipoGasto = TipoGasto::find()->all();
        $requerimiento=new Requerimiento;
        if($requerimiento->load(Yii::$app->request->post())){
            $Informacion=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])->one();
            $Proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$Informacion->InvestigadorID])->one();
            $PasoCritico=PasoCritico::find()->where('ProyectoID=:ProyectoID and Estado=1 and Situacion=2',[':ProyectoID'=>$Proyecto->ID])->one();
            $requerimiento->PasoCriticoID=$PasoCritico->ID;
            $requerimiento->Situacion=1;
            $requerimiento->Estado=1;
            $requerimiento->FechaRegistro = date('Ymd h:m:s');
            $requerimiento->FechaSolicitud = date('Ymd h:m:s');
            $requerimiento->Annio=date('Y');
            $requerimiento->Correlativo=$this->CorrelativoRequerimiento($CodigoProyecto);
            $requerimiento->save();
            // echo '<pre>'; print_r($requerimiento); die();

            Yii::$app->tools->logData($requerimiento->ID,'insert','Requerimiento','Nuevo Requerimiento');
            
            if($requerimiento->TipoRequerimiento==5)
            {
                $detalle=new DetalleRequerimiento;
                $detalle->RequerimientoID   = $requerimiento->ID;
                $detalle->Situacion=2;
                $detalle->save();
                Yii::$app->tools->logData($detalle->ID,'insert','DetalleRequerimiento','Nuevo Detalle Requerimiento');
            }
            
            if(!$requerimiento->DetallesDescripciones)
            {
                $countDetallesDescripciones=0;
            }
            else
            {
                $countDetallesDescripciones=count(array_filter($requerimiento->DetallesDescripciones));
            }
            
            for($i=0;$i<$countDetallesDescripciones;$i++)
            {
                if(isset($requerimiento->DetallesDescripciones[$i]))
                {
                    $detalle=new DetalleRequerimiento;
                    $detalle->RequerimientoID   = $requerimiento->ID;
                    $detalle->CodigoMatriz      = $requerimiento->DetallesCodigos[$i];
                    $detalle->Objetivo          = $requerimiento->DetallesObjetivos[$i];
                    $detalle->Actividad         = $requerimiento->DetallesActividades[$i];
                    $detalle->Recurso           = $requerimiento->DetallesRecurs[$i];
                    $detalle->Codificacion      = $requerimiento->DetallesCodificaciones[$i];
                    $detalle->AreSubCategoriaID = $requerimiento->DetallesRecursos[$i];
                    $detalle->Cantidad          = str_replace(',','', $requerimiento->DetallesCantidades[$i]);
                    $detalle->Descripcion       = $requerimiento->DetallesDescripciones[$i];
                    $detalle->PrecioUnitario    = str_replace(',','', $requerimiento->DetallesPreciosUnitarios[$i]);
                    $detalle->Total             =(str_replace(',','', $requerimiento->DetallesCantidades[$i])*str_replace(',','', $requerimiento->DetallesPreciosUnitarios[$i]));
                    $detalle->Situacion=2;
                    $detalle->Estado = 1;
                    $detalle->save();
                    Yii::$app->tools->logData($detalle->ID,'insert','DetalleRequerimiento','Nuevo Detalle Requerimiento');
                }
            }
            
            $requerimiento->Archivo = UploadedFile::getInstance($requerimiento, 'Archivo');
            if($requerimiento->Archivo)
            {
                $requerimiento->Archivo->saveAs('requerimientos/' . $requerimiento->ID . '.' . $requerimiento->Archivo->extension);
                $requerimiento->Documento=$requerimiento->ID . '.' . $requerimiento->Archivo->extension;
            }
            $requerimiento->update();
            return $this->redirect(['/requerimiento']);
            // $arr = array('Success' => true);
            // echo json_encode($arr);
        }else{
            return $this->render('_form',['CodigoProyecto'=>$CodigoProyecto,'tipoGasto' => $tipoGasto]);
        }
        
    }
    
    public function CorrelativoRequerimiento($CodigoProyecto)
    {
        $orden=Requerimiento::find()->where('CodigoProyecto=:CodigoProyecto',[':CodigoProyecto'=>$CodigoProyecto])->orderBy('Correlativo desc')->one();
        if($orden)
        {
            return $orden->Correlativo+1;
        }
        else
        {
            return 1;
        }
    }
    
    public function actionActualizar($ID=null)
    {
        $this->layout='vacio';
        $tipoGasto = TipoGasto::find()->all();
        $requerimiento=Requerimiento::findOne($ID);
        $requerimientoAnt=Requerimiento::find()->where('Situacion=8 and Estado=0 and CodigoProyecto=:CodigoProyecto and Correlativo=:Correlativo',[':CodigoProyecto'=>$requerimiento->CodigoProyecto,':Correlativo'=>$requerimiento->Correlativo])
        ->orderBy('FechaRegistro asc')
        ->one();
        $resultados = array();
        $detallesRequerimientos=DetalleRequerimiento::find()->select('DetalleRequerimiento.*,(DetalleRequerimiento.PrecioUnitario*DetalleRequerimiento.Cantidad) Total')->where('RequerimientoID=:RequerimientoID',[':RequerimientoID'=>$requerimiento->ID])->all();

        foreach ($detallesRequerimientos as $req) {
            if(!empty($req->AreSubCategoriaID)){
                $resultados[]=Yii::$app->db->createCommand("
                    SELECT Componente.Correlativo CorrelaCompr,Actividad.Correlativo CorrelaActv,AreSubCategoria.Correlativo CorrelaRecurso,ActRubroElegible.RubroElegibleID,AreSubCategoria.Codificacion,
                    AreSubCategoria.CostoUnitario,AreSubCategoria.Nombre,AreSubCategoria.Especifica,
                    AreSubCategoria.UnidadMedida,AreSubCategoria.CostoUnitario,AreSubCategoria.MetaFisica
                    FROM AreSubCategoria 
                    INNER JOIN ActRubroElegible ON ActRubroElegible.ID = AreSubCategoria.ActRubroElegibleID
                    INNER JOIN RubroElegible ON RubroElegible.ID = ActRubroElegible.RubroElegibleID
                    INNER JOIN Actividad ON ActRubroElegible.ActividadID = Actividad.ID
                    INNER JOIN Componente ON Actividad.ComponenteID = Componente.ID
                    WHERE AreSubCategoria.ID =".$req->AreSubCategoriaID)->queryOne();                
            }else{
                $resultados = array();
            }
        }

        if($requerimiento->load(Yii::$app->request->post())){
            $requerimiento->save();
            
            if(!$requerimiento->DetallesDescripciones)
            {
                $countDetallesDescripciones=0;
            }
            else
            {
                $countDetallesDescripciones=count(array_filter($requerimiento->DetallesDescripciones));
            }
            
            for($i=0;$i<$countDetallesDescripciones;$i++)
            {
                $detalle=new DetalleRequerimiento;
                $detalle->RequerimientoID   = $requerimiento->ID;
                $detalle->CodigoMatriz      = $requerimiento->DetallesCodigos[$i];
                $detalle->Objetivo          = $requerimiento->DetallesObjetivos[$i];
                $detalle->Actividad         = $requerimiento->DetallesActividades[$i];
                $detalle->Recurso           = $requerimiento->DetallesRecurs[$i];
                $detalle->Codificacion      = $requerimiento->DetallesCodificaciones[$i];
                $detalle->AreSubCategoriaID = $requerimiento->DetallesRecursos[$i];
                $detalle->Cantidad          = str_replace(',','', $requerimiento->DetallesCantidades[$i]);
                $detalle->Descripcion       = $requerimiento->DetallesDescripciones[$i];
                $detalle->PrecioUnitario    = str_replace(',','', $requerimiento->DetallesPreciosUnitarios[$i]);
                $detalle->Total             =(str_replace(',','', $requerimiento->DetallesCantidades[$i])*str_replace(',','', $requerimiento->DetallesPreciosUnitarios[$i]));
                $detalle->Situacion=2;
                $detalle->Estado = 1;
                $detalle->save();
            }
            $requerimiento->Archivo = UploadedFile::getInstance($requerimiento, 'Archivo');
            if($requerimiento->Archivo)
            {
                $requerimiento->Archivo->saveAs('requerimientos/' . $requerimiento->ID . '.' . $requerimiento->Archivo->extension);
                $requerimiento->Documento=$requerimiento->ID . '.' . $requerimiento->Archivo->extension;
            }
            $requerimiento->update();
            return $this->redirect(['/requerimiento']);
            // $arr = array('Success' => true);
            // echo json_encode($arr);
        }
        return $this->render('_form_actualizar',['requerimiento'=>$requerimiento,'ID'=>$ID,'detallesRequerimientos'=>$detallesRequerimientos,'requerimientoAnt'=>$requerimientoAnt,'tipoGasto' => $tipoGasto,'resultados'=>$resultados]);
    }
    
    public function actionVer($ID=null)
    {
        $this->layout='vacio';
        $tipoGasto = TipoGasto::find()->all();
        $requerimiento=Requerimiento::findOne($ID);
        $detallesRequerimientos=DetalleRequerimiento::find()->where('RequerimientoID=:RequerimientoID',[':RequerimientoID'=>$requerimiento->ID])->all();
        $resultados = array();
        foreach ($detallesRequerimientos as $req) {
            if(!empty($req->AreSubCategoriaID)){
                $resultados[]=Yii::$app->db->createCommand("
                    SELECT Componente.Correlativo CorrelaCompr,Actividad.Correlativo CorrelaActv,AreSubCategoria.Correlativo CorrelaRecurso,ActRubroElegible.RubroElegibleID,AreSubCategoria.Codificacion,
                    AreSubCategoria.CostoUnitario,AreSubCategoria.Nombre,AreSubCategoria.Especifica,
                    AreSubCategoria.UnidadMedida,AreSubCategoria.CostoUnitario,AreSubCategoria.MetaFisica
                    FROM AreSubCategoria 
                    INNER JOIN ActRubroElegible ON ActRubroElegible.ID = AreSubCategoria.ActRubroElegibleID
                    INNER JOIN RubroElegible ON RubroElegible.ID = ActRubroElegible.RubroElegibleID
                    INNER JOIN Actividad ON ActRubroElegible.ActividadID = Actividad.ID
                    INNER JOIN Componente ON Actividad.ComponenteID = Componente.ID
                    WHERE AreSubCategoria.ID =".$req->AreSubCategoriaID)->queryOne();
            }else{
                $resultados = array();
            }
        }

        if($requerimiento->Situacion!=3)
        {
            $requerimientoAnt=Requerimiento::find()->where('Situacion=8 and Estado=0 and CodigoProyecto=:CodigoProyecto and Correlativo=:Correlativo',[':CodigoProyecto'=>$requerimiento->CodigoProyecto,':Correlativo'=>$requerimiento->Correlativo])
            ->orderBy('FechaRegistro asc')
            ->one();    
        }
        else{
            $requerimientoAnt=null;
        }
        
        return $this->render('_form_ver',['requerimiento'=>$requerimiento,'ID'=>$ID,'detallesRequerimientos'=>$detallesRequerimientos,'tipoGasto'=>$tipoGasto,'requerimientoAnt'=>$requerimientoAnt,'resultados'=>$resultados]);
    }
    
    
    public function actionVerUbicacion($ID=null)
    {
        $this->layout='vacio';
        $tipoGasto = TipoGasto::find()->all();
        $requerimiento=Requerimiento::findOne($ID);
        $detallesRequerimientos=DetalleRequerimiento::find()->where('RequerimientoID=:RequerimientoID',[':RequerimientoID'=>$requerimiento->ID])->all();
        $resultados = array();
        foreach ($detallesRequerimientos as $req) {
            if(!empty($req->AreSubCategoriaID)){
                $resultados[]=Yii::$app->db->createCommand("
                    SELECT Componente.Correlativo CorrelaCompr,Actividad.Correlativo CorrelaActv,AreSubCategoria.Correlativo CorrelaRecurso,ActRubroElegible.RubroElegibleID,AreSubCategoria.Codificacion,
                    AreSubCategoria.CostoUnitario,AreSubCategoria.Nombre,AreSubCategoria.Especifica,
                    AreSubCategoria.UnidadMedida,AreSubCategoria.CostoUnitario,AreSubCategoria.MetaFisica
                    FROM AreSubCategoria 
                    INNER JOIN ActRubroElegible ON ActRubroElegible.ID = AreSubCategoria.ActRubroElegibleID
                    INNER JOIN RubroElegible ON RubroElegible.ID = ActRubroElegible.RubroElegibleID
                    INNER JOIN Actividad ON ActRubroElegible.ActividadID = Actividad.ID
                    INNER JOIN Componente ON Actividad.ComponenteID = Componente.ID
                    WHERE AreSubCategoria.ID =".$req->AreSubCategoriaID)->queryOne();
            }else{
                $resultados = array();
            }
        }

        if($requerimiento->Situacion!=3)
        {
            $requerimientoAnt=Requerimiento::find()->where('Situacion=8 and Estado=0 and CodigoProyecto=:CodigoProyecto and Correlativo=:Correlativo',[':CodigoProyecto'=>$requerimiento->CodigoProyecto,':Correlativo'=>$requerimiento->Correlativo])
            ->orderBy('FechaRegistro asc')
            ->one();    
        }
        else{
            $requerimientoAnt=null;
        }
        
        return $this->render('_form_ver_ubicacion',['requerimiento'=>$requerimiento,'ID'=>$ID,'detallesRequerimientos'=>$detallesRequerimientos,'tipoGasto'=>$tipoGasto,'requerimientoAnt'=>$requerimientoAnt,'resultados'=>$resultados]);
    }
    
    public function getTipoServicio($tipoServicio=null)
    {
        $tipoGasto = TipoGasto::find()->where('ID=:ID',[':ID'=>$tipoServicio])->one();
        return $tipoGasto->Nombre;
    }
    
    public function getSituacion($codigo=null)
    {
        $situacion=Situacion::find()->where('Codigo=:Codigo',[':Codigo'=>$codigo])->one();
        return $situacion->Descripcion;
    }
    
    public function getUbicacion($situacion=null)
    {
        if($situacion==1)
        {
            return "IRP";
        }
        elseif($situacion==2)
        {
            return "Jefe de estación";
        }
        elseif($situacion==3)
        {
            return "IRP";
        }
    }
    
    public function actionEliminar($id=null)
    {
        $requerimiento=Requerimiento::findOne($id);
        $drequerimiento=DetalleRequerimiento::find()->where('RequerimientoID=:RequerimientoID',[':RequerimientoID'=>$id])->one();
        $requerimiento->Situacion=6;
        $requerimiento->update();
        //$drequerimiento->delete();
        //$requerimiento->delete();
        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
    public function actionRetraer($id=null)
    {
        $requerimiento=Requerimiento::findOne($id);
        $requerimiento->Situacion=1;
        $requerimiento->update();
        
        $usuario=Usuario::find()
                ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                ->innerJoin('UsuarioProyecto','UsuarioProyecto.UsuarioID=Usuario.ID')
                ->where('UsuarioRol.RolID=:Rol and UsuarioProyecto.CodigoProyecto=:CodigoProyecto',
                        [':Rol'=>5,':CodigoProyecto'=>$requerimiento->CodigoProyecto])
                ->one();
        
        $tiempo=SeguimientoRequerimiento::find()
                    ->innerJoin('Usuario','Usuario.ID=SeguimientoRequerimiento.UsuarioID')
                    ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                    ->where('SeguimientoRequerimiento.RequerimientoID=:RequerimientoID and UsuarioRol.RolID=:RolID and SeguimientoRequerimiento.Estado=:Estado',[':RequerimientoID'=>$requerimiento->ID,':RolID'=>5,':Estado'=>1])
                    ->one();
        if($tiempo)
        {
            $tiempo->FechaFin=date('Ymd h:m:s');
            $tiempo->Dias=(int)Yii::$app->tools->RestarFechas($tiempo->FechaInicio,date('Y-m-d h:m:s',strtotime($tiempo->FechaFin)));
            $tiempo->Situacion=6;
            $tiempo->Estado=0;
            $tiempo->update();
        }
        
        $detallesRequerimientos=DetalleRequerimiento::find()->where('RequerimientoID=:RequerimientoID',[':RequerimientoID'=>$requerimiento->ID])->all();
        if($requerimiento->TipoRequerimiento==1)
        {
            foreach($detallesRequerimientos as $detalleRequerimiento)
            {
                $ordenServicioFactura=Orden::findOne($detalleRequerimiento->TipoDetalleOrdenID);
                if($ordenServicioFactura)
                {
                    $ordenServicioFactura->RequerimientoID=NULL;
                    $ordenServicioFactura->Estado=0;
                    $ordenServicioFactura->Situacion=6;
                    $ordenServicioFactura->update();
                }
                $detalleRequerimiento->Situacion=2;
                $detalleRequerimiento->TipoDetalleOrdenID=NULL;
                $detalleRequerimiento->update();
            }
            
        }
        elseif($requerimiento->TipoRequerimiento==2)
        {
            foreach($detallesRequerimientos as $detalleRequerimiento)
            {
                $ordenServicioRH=Orden::findOne($detalleRequerimiento->TipoDetalleOrdenID);
                if($ordenServicioRH)
                {
                    $ordenServicioRH->RequerimientoID=NULL;
                    $ordenServicioRH->Estado=0;
                    $ordenServicioRH->Situacion=6;
                    $ordenServicioRH->update();
                }
                $detalleRequerimiento->Situacion=2;
                $detalleRequerimiento->TipoDetalleOrdenID=NULL;
                $detalleRequerimiento->update();
            }
        }
        elseif($requerimiento->TipoRequerimiento==3)
        {
            foreach($detallesRequerimientos as $detalleRequerimiento)
            {
                $ordenFactura=Orden::findOne($detalleRequerimiento->TipoDetalleOrdenID);
                if($ordenFactura)
                {
                    $ordenFactura->RequerimientoID=NULL;
                    $ordenFactura->Estado=0;
                    $ordenFactura->Situacion=6;
                    $ordenFactura->update();
                }
                $detalleRequerimiento->Situacion=2;
                $detalleRequerimiento->TipoDetalleOrdenID=NULL;
                $detalleRequerimiento->update();
            }
        }
        elseif($requerimiento->TipoRequerimiento==4)
        {
            foreach($detallesRequerimientos as $detalleRequerimiento)
            {
                $viatico=Viatico::findOne($detalleRequerimiento->TipoDetalleOrdenID);
                if($viatico)
                {
                    $viatico->RequerimientoID=NULL;
                    $viatico->Estado=0;
                    $viatico->Situacion=6;
                    $viatico->update();
                }
                $detalleRequerimiento->Situacion=2;
                $detalleRequerimiento->TipoDetalleOrdenID=NULL;
                $detalleRequerimiento->update();
            }
        }
        elseif($requerimiento->TipoRequerimiento==5)
        {
            foreach($detallesRequerimientos as $detalleRequerimiento)
            {
                $cajaChica=CajaChica::findOne($detalleRequerimiento->TipoDetalleOrdenID);
                if($cajaChica)
                {
                    $cajaChica->RequerimientoID=NULL;
                    $cajaChica->Estado=0;
                    $cajaChica->Situacion=6;
                    $cajaChica->update();
                }
            }
        }
        elseif($requerimiento->TipoRequerimiento==6)
        {
            foreach($detallesRequerimientos as $detalleRequerimiento)
            {
                $encargo=Encargo::findOne($detalleRequerimiento->TipoDetalleOrdenID);
                if($encargo)
                {
                    $encargo->RequerimientoID=NULL;
                    $encargo->Estado=0;
                    $encargo->Situacion=6;
                    $encargo->update();
                }
                $detalleRequerimiento->Situacion=2;
                $detalleRequerimiento->TipoDetalleOrdenID=NULL;
                $detalleRequerimiento->update();
            }
        }
        
        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
    public function actionEliminarDetalle()
    {
        if(isset($_POST['id']) && $_POST['id']!='')
        {
            $id=$_POST['id'];
            $requerimiento=DetalleRequerimiento::findOne($id);
            $requerimiento->delete();
            $arr = array('Success' => true);
            echo json_encode($arr); 
        }
    }
    
    
    public function actionUafsi()
    {
        $this->layout='estandar';
        return $this->render('uafsi');
    
    }
    
    // C:\Program Files (x86)\PremiumSoft\Navicat Premium\instantclient_10_2

    // Jefe de UAFSI

    /*
    public function actionUafsi($id = null,$contab=null){
        $this->layout='estandar';
        if(!is_null($id)){
            $cabecera=CompromisoPresupuestal::find()->where(['ID'=>$id])->one();

            $detReque = DetalleRequerimiento::find()->where(['RequerimientoID'=>$cabecera->RequerimientoID])->all();

            foreach ($detReque as $req) {
            
                $detResult[]=Yii::$app->db->createCommand("
                SELECT Componente.Correlativo CorrelaCompr,Actividad.Correlativo CorrelaActv,AreSubCategoria.Correlativo CorrelaRecurso,ActRubroElegible.RubroElegibleID,AreSubCategoria.Codificacion,
                    AreSubCategoria.CostoUnitario,AreSubCategoria.Nombre,AreSubCategoria.Especifica,
                    AreSubCategoria.UnidadMedida,AreSubCategoria.CostoUnitario,AreSubCategoria.MetaFisica
                FROM AreSubCategoria 
                INNER JOIN ActRubroElegible ON ActRubroElegible.ID = AreSubCategoria.ActRubroElegibleID
                INNER JOIN RubroElegible ON RubroElegible.ID = ActRubroElegible.RubroElegibleID
                INNER JOIN Actividad ON ActRubroElegible.ActividadID = Actividad.ID
                INNER JOIN Componente ON Actividad.ComponenteID = Componente.ID
                WHERE AreSubCategoria.ID =".$req->AreSubCategoriaID)->queryOne();
            }


            $requerimiento=Requerimiento::findOne($cabecera->RequerimientoID);
            if($requerimiento->TipoRequerimiento==1 || $requerimiento->TipoRequerimiento==2 || $requerimiento->TipoRequerimiento==3)
            {
               $resultados = (new \yii\db\Query())
                ->select('DetalleCompromisoPresupuestal.ID,
                    DetalleCompromisoPresupuestal.Correlativo,
                    DetalleCompromisoPresupuestal.TipoGasto,
                    DetalleCompromisoPresupuestal.GastoElegible,
                    DetalleCompromisoPresupuestal.Observacion,
                    DetalleCompromisoPresupuestal.CompromisoPresupuestalID,
                    DetalleCompromisoPresupuestal.Situacion,
                    CompromisoPresupuestal.Annio,
                    CompromisoPresupuestal.RequerimientoID,
                    Orden.FechaOrden,
                    Orden.Documento,
                    Orden.TipoProceso,
                    Orden.Monto,
                    Orden.RazonSocial,
                    Orden.RUC,
                    TipoGasto.Nombre, 
                    DetalleCompromisoPresupuestal.CodigoMatriz'
                )
                // Orden.SituacionSIAFT,
                ->from('DetalleCompromisoPresupuestal')
                ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.ID=DetalleCompromisoPresupuestal.CompromisoPresupuestalID')
                ->innerJoin('UsuarioProyecto','UsuarioProyecto.CodigoProyecto=CompromisoPresupuestal.Codigo')
                ->innerJoin('Usuario','Usuario.ID=UsuarioProyecto.UsuarioID')
                ->innerJoin('TipoGasto','TipoGasto.ID=DetalleCompromisoPresupuestal.TipoGasto')
                ->innerJoin('Orden','Orden.ID=DetalleCompromisoPresupuestal.Correlativo')

                ->where(['Usuario.ID'=>\Yii::$app->user->id,'DetalleCompromisoPresupuestal.CompromisoPresupuestalID'=>$id])
                ->distinct()
                ->all(); 
            }
            elseif($requerimiento->TipoRequerimiento==4)
            {
                $resultados = (new \yii\db\Query())
                ->select('DetalleCompromisoPresupuestal.ID,
                    DetalleCompromisoPresupuestal.Correlativo,
                    DetalleCompromisoPresupuestal.TipoGasto,
                    DetalleCompromisoPresupuestal.GastoElegible,
                    DetalleCompromisoPresupuestal.Observacion,
                    DetalleCompromisoPresupuestal.CompromisoPresupuestalID,
                    DetalleCompromisoPresupuestal.Situacion,
                    CompromisoPresupuestal.Annio,
                    CompromisoPresupuestal.Documento,
                    CompromisoPresupuestal.RequerimientoID,
                    Viatico.Apellido,
                    Viatico.Nombre,
                    Viatico.Dni,
                    Viatico.Monto,
                    TipoGasto.Nombre tipo,
                    Viatico.Documento,
                    DetalleCompromisoPresupuestal.CodigoMatriz'
                )
                ->from('DetalleCompromisoPresupuestal')
                ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.ID=DetalleCompromisoPresupuestal.CompromisoPresupuestalID')
                ->innerJoin('UsuarioProyecto','UsuarioProyecto.CodigoProyecto=CompromisoPresupuestal.Codigo')
                ->innerJoin('Usuario','Usuario.ID=UsuarioProyecto.UsuarioID')
                ->innerJoin('TipoGasto','TipoGasto.ID=DetalleCompromisoPresupuestal.TipoGasto')
                ->innerJoin('Viatico','Viatico.ID=DetalleCompromisoPresupuestal.Correlativo')
                ->where(['Usuario.ID'=>\Yii::$app->user->id,'DetalleCompromisoPresupuestal.CompromisoPresupuestalID'=>$id])
                ->distinct()
                ->all(); 
            }
            elseif($requerimiento->TipoRequerimiento==5)
            {
                $resultados = (new \yii\db\Query())
                ->select('DetalleCompromisoPresupuestal.ID,
                    DetalleCompromisoPresupuestal.Correlativo,
                    DetalleCompromisoPresupuestal.TipoGasto,
                    DetalleCompromisoPresupuestal.GastoElegible,
                    DetalleCompromisoPresupuestal.Observacion,
                    DetalleCompromisoPresupuestal.CompromisoPresupuestalID,
                    DetalleCompromisoPresupuestal.Situacion,
                    CompromisoPresupuestal.Annio,
                    CompromisoPresupuestal.Documento,
                    CompromisoPresupuestal.RequerimientoID,
                    DetalleCajaChica.Tipo,
                    DetalleCajaChica.Bienes,
                    DetalleCajaChica.Servicios,
                    DetalleCajaChica.Responsable,
                    DetalleCajaChica.ResolucionDirectorial,
                    TipoGasto.Nombre,
                    DetalleCompromisoPresupuestal.CodigoMatriz'
                )
                ->from('DetalleCompromisoPresupuestal')
                ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.ID=DetalleCompromisoPresupuestal.CompromisoPresupuestalID')
                ->innerJoin('UsuarioProyecto','UsuarioProyecto.CodigoProyecto=CompromisoPresupuestal.Codigo')
                ->innerJoin('Usuario','Usuario.ID=UsuarioProyecto.UsuarioID')
                ->innerJoin('TipoGasto','TipoGasto.ID=DetalleCompromisoPresupuestal.TipoGasto')
                ->innerJoin('DetalleCajaChica','DetalleCajaChica.ID=DetalleCompromisoPresupuestal.Correlativo')
                ->where(['Usuario.ID'=>\Yii::$app->user->id,'DetalleCompromisoPresupuestal.CompromisoPresupuestalID'=>$id])
                ->distinct()
                ->all(); 
            }
            elseif($requerimiento->TipoRequerimiento==6)
            {
                $resultados = (new \yii\db\Query())
                ->select('DetalleCompromisoPresupuestal.ID,
                    DetalleCompromisoPresupuestal.Correlativo,
                    DetalleCompromisoPresupuestal.TipoGasto,
                    DetalleCompromisoPresupuestal.GastoElegible,
                    DetalleCompromisoPresupuestal.Observacion,
                    DetalleCompromisoPresupuestal.CompromisoPresupuestalID,
                    DetalleCompromisoPresupuestal.Situacion,
                    CompromisoPresupuestal.Annio,
                    CompromisoPresupuestal.Documento,
                    CompromisoPresupuestal.RequerimientoID,
                    Encargo.Bienes,
                    Encargo.Servicios,
                    Encargo.NombresEncargo,
                    Encargo.ApellidosEncargo,
                    Encargo.DNIEncargo,
                    Encargo.Documento,
                    TipoGasto.Nombre,
                    DetalleCompromisoPresupuestal.CodigoMatriz'
                )

                ->from('DetalleCompromisoPresupuestal')
                ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.ID=DetalleCompromisoPresupuestal.CompromisoPresupuestalID')
                ->innerJoin('UsuarioProyecto','UsuarioProyecto.CodigoProyecto=CompromisoPresupuestal.Codigo')
                ->innerJoin('Usuario','Usuario.ID=UsuarioProyecto.UsuarioID')
                ->innerJoin('TipoGasto','TipoGasto.ID=DetalleCompromisoPresupuestal.TipoGasto')
                ->innerJoin('Encargo','Encargo.ID=DetalleCompromisoPresupuestal.Correlativo')
                ->where(['Usuario.ID'=>\Yii::$app->user->id,'DetalleCompromisoPresupuestal.CompromisoPresupuestalID'=>$id])
                ->distinct()
                ->all(); 
            }
            
            $res = array();

            return $this->render('_uafsiDetalle',['resultados'=>$resultados,'cabecera'=>$cabecera,'contab'=>$contab,'res'=>$res,'requerimiento'=>$requerimiento,'detResult'=>$detResult]);
        }else{
            return $this->render('_uafsiListado');
        }
    }

    public function getSituacionUafsi($situacion=null)
    {
        $situacion=Situacion::findOne($situacion);
        
        if($situacion->ID==15)
        {
            return "Pendiente";
        }
        else
        {
            return "Aprobado";
        }
        
    }

    public function actionListadoUafsi(){
        $this->layout='vacio';    
        // echo \Yii::$app->user->identity->rol;
        if(\Yii::$app->user->identity->rol == 15){
            $resultado=CompromisoPresupuestal::find()
                        ->select('CompromisoPresupuestal.*,TipoGasto.Nombre NombreGasto')
                        ->innerJoin('UsuarioProyecto','UsuarioProyecto.CodigoProyecto=CompromisoPresupuestal.Codigo')
                        ->innerJoin('Usuario','Usuario.ID=UsuarioProyecto.UsuarioID')
                        ->innerJoin('Requerimiento','Requerimiento.ID=CompromisoPresupuestal.RequerimientoID')
                        ->innerJoin('TipoGasto','TipoGasto.ID=Requerimiento.TipoRequerimiento')
                        ->where('CompromisoPresupuestal.Situacion IN(16) AND CompromisoPresupuestal.Estado=1 AND Usuario.ID=:ID',[':ID'=>\Yii::$app->user->id])
                        ->all();   
        }else{
            $resultado=CompromisoPresupuestal::find()
                        ->select('CompromisoPresupuestal.ID,CompromisoPresupuestal.Correlativo,CompromisoPresupuestal.Annio,CompromisoPresupuestal.Codigo,CompromisoPresupuestal.FechaRegistro,CompromisoPresupuestal.Situacion,TipoGasto.Nombre NombreGasto')
                        ->innerJoin('UsuarioProyecto','UsuarioProyecto.CodigoProyecto=CompromisoPresupuestal.Codigo')
                        ->innerJoin('Usuario','Usuario.ID=UsuarioProyecto.UsuarioID')
                        ->innerJoin('Requerimiento','Requerimiento.ID=CompromisoPresupuestal.RequerimientoID')
                        ->innerJoin('TipoGasto','TipoGasto.ID=Requerimiento.TipoRequerimiento')
                        ->where('CompromisoPresupuestal.Situacion!=0 AND CompromisoPresupuestal.Estado=1 and Usuario.ID=:ID',[':ID'=>\Yii::$app->user->id])
                        ->all();   
        }
        
        $res = (new \yii\db\Query())
            ->select('Persona.ApellidoPaterno,Persona.ApellidoMaterno,Persona.Nombre,Usuario.ID')
            ->from('Persona')
            ->innerJoin('Usuario','Usuario.PersonaID=Persona.ID')
            ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID = Usuario.ID')
            ->where(['UsuarioRol.RolID'=>14])
            ->distinct()
            ->all();

        $nro=0;
        echo '<pre>';
        foreach($resultado as $result)
        {
            $nro++;
            echo "<tr>";
                echo "<td> N° " . str_pad($result["Correlativo"], 3, "0", STR_PAD_LEFT)  ."-".$result["Annio"]."</td>";
                echo "<td> " . $result["NombreGasto"] . "</td>";
                echo "<td> " . $result["Codigo"] . "</td>";
                echo "<td>" . date('d-m-Y',strtotime($result["FechaRegistro"])) . "</td>";
                echo "<td>" . $this->getSituacionUafsi($result["Situacion"]) . "</td>";
                
                if(\Yii::$app->user->identity->rol == 2){
                    $tiempo=SeguimientoCompromisoPresupuestal::find()
                        ->innerJoin('Usuario','Usuario.ID=SeguimientoCompromisoPresupuestal.UsuarioID')
                        ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                        ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID and UsuarioRol.RolID=2',[':CompromisoPresupuestalID'=>$result["ID"]])
                        ->one();
                        
                    echo "<td>" . Yii::$app->tools->RestarFechas($tiempo->FechaInicio,date('Y-m-d h:m:s')) . "</td>";
                    if($result['Situacion'] == 15){
                        echo "<td><a href='?id=".$result["ID"]."' class='btn btn-danger'>Ver detalle</a></td>";
                    }else{
                        echo "<td><a href='?id=".$result["ID"]."' class='btn btn-info'>Ver detalle</a></td>";
                    }
                }
                
                if(\Yii::$app->user->identity->rol == 11){
                   
                    if($result['GastoElegible'] != ''){
                        echo '<td>'.$this->tipoGasto($result['GastoElegible']).'</td>';
                    }else{
                        echo '<td>-</td>';
                    }
                    echo "<td><a href='?id=".$result["ID"]."' class='btn btn-info'>Ver</a></td>";
                }
                
                if(\Yii::$app->user->identity->rol == 15){
                    $tiempo=SeguimientoCompromisoPresupuestal::find()
                        ->innerJoin('Usuario','Usuario.ID=SeguimientoCompromisoPresupuestal.UsuarioID')
                        ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                        ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID and UsuarioRol.RolID=15',[':CompromisoPresupuestalID'=>$result["ID"]])
                        ->one();
                        
                    echo "<td>" . Yii::$app->tools->RestarFechas($tiempo->FechaInicio,date('Y-m-d h:m:s')) . "</td>";
                    
                    if($result['Situacion'] == 2){
                        //echo "<td><a href='#' data-id='".$result["ID"]."' class='btn btn-primary verifica-aprobar' >Aprobar</a> <a href='?id=".$result["ID"]."' class='btn btn-info'>Ver</a></td>";
                        echo "<td><a href='?id=".$result["ID"]."' class='btn btn-danger'>Ver detalle</a></td>";
                    }
                    else{
                        echo "<td><a href='?id=".$result["ID"]."' class='btn btn-info'>Ver detalle</a></td>";
                    }
                }

            echo "</tr>";
        }
    }
    */
    //
    public function actionEeaa($id = null){
        $this->layout='estandar';
        if(!is_null($id)){
            
            $resultados = (new \yii\db\Query())
            // ->select('Requerimiento.*,TerminoReferencia.ID TerminoReferenciaID')
            ->from('DetalleCompromisoPresupuestal')
            ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.ID=DetalleCompromisoPresupuestal.CompromisoPresupuestalID')
            ->innerJoin('TipoGasto','TipoGasto.ID=DetalleCompromisoPresupuestal.TipoGasto')
            ->innerJoin('Orden','Orden.ID=DetalleCompromisoPresupuestal.Correlativo')
            ->where('DetalleCompromisoPresupuestal.CompromisoPresupuestalID=:CompromisoPresupuestalID',[':CompromisoPresupuestalID'=>$id])
            ->distinct()
            ->all();

            $cabecera=CompromisoPresupuestal::find()->where(['ID'=>$id])->one();
            $res = array();

            return $this->render('_eeaaDetalle',['resultados'=>$resultados,'cabecera'=>$cabecera,'res'=>$res]);
        }else{
            return $this->render('_eeaaListado');
        }
    }

    public function actionListadoEeaa(){
        $this->layout='vacio';    
        
        
        // $resultado = Requerimiento::find()->all();   
        

        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $usuarioRol=UsuarioRol::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->ID])->one();

        $nro=0;

        // if()

        $resultados = (new \yii\db\Query())
            ->select('Requerimiento.*')
            ->from('InformacionGeneral')
            ->innerJoin('Investigador','InformacionGeneral.InvestigadorID=Investigador.ID')
            ->innerJoin('Usuario','Usuario.ID=Investigador.UsuarioID')
            ->innerJoin('Requerimiento','Requerimiento.CodigoProyecto = InformacionGeneral.Codigo')
            ->innerJoin('Proyecto','Proyecto.InvestigadorID = InformacionGeneral.InvestigadorID')
            ->innerJoin('UsuarioProyecto','UsuarioProyecto.CodigoProyecto=InformacionGeneral.Codigo')
            ->where('Requerimiento.Estado=1 and UsuarioProyecto.UsuarioID ='.\Yii::$app->user->id.' and Requerimiento.Situacion NOT IN (1,6) ')
            ->distinct()
            ->orderBy('Requerimiento.ID asc')
            ->all();

        $nro=0;
        foreach($resultados as $result)
        {
            $nro++;
            $color='';
            if($result["Situacion"]==3)
            {
                $color='#00794C';
            }
            elseif($result["Situacion"]==2)
            {
                $color='#2460AA';
            }
            elseif($result["Situacion"]==6)
            {
                $color='#E20612';
            }
            
            echo "<tr>";
            echo "<td style='display: none'> " . $result["ID"] . "</td>";
            echo "<td> " . $result["CodigoProyecto"] . "</td>";
            echo "<td> " . $this->getTipoServicio($result["TipoRequerimiento"])  . "</td>";
            echo "<td> Requerimiento N° " . str_pad($result["Correlativo"], 3, "0", STR_PAD_LEFT) ."-".$result["Annio"]."</td>";
            echo "<td> " . $result["Referencia"] . "</td>";
            echo "<td>" . date('d-m-Y',strtotime($result["FechaSolicitud"])) . "</td>";
            if($result["Documento"])
            {
                echo "<td><a class='btn btn-default' target='_blank' class='btn' href='../requerimientos/" . $result["Documento"] . "'><span class='fa fa-cloud-download'></span> Descargar</a></td>";
            }
            else
            {
                echo "<td></td>";   
            }
            echo "<td style='color:$color'><b>" . $this->getSituacion($result["Situacion"]) . "</b></td>";
            //echo "<td><a target='_blank' href='../requerimientos/" . $result["Documento"] . "'><span class='fa fa-cloud-download'></span></a></td>";
            
            if($result["Situacion"]==2)
            {
                echo "<td><a href='#' class='verifica-aprobar' data-id='".$result["ID"]."'><span class='fa fa-check-square-o fa-lg'></span></a> <a href='#' class='observar' data-id='".$result["ID"]."'><span class='fa fa-commenting-o fa-lg'></span></a> <a href='#' class='btn-requerimiento-ver' data-id='".$result["ID"]."'><i class='fa fa-eye fa-lg'></i></a> <a href='#' class='btn-remove' data-id='".$result["ID"]."'><i class='fa fa-remove fa-lg'></i></a> </td>";
            }
            else
            {
                echo "<td><a href='#' class='btn-requerimiento-ver' data-id='".$result["ID"]."'><i class='fa fa-eye fa-lg'></i></a></td>";
            }
            echo "</tr>";
        }
    }

    public function actionActualizarRequerimiento()
    {
        $this->layout='vacio';
        if(isset($_POST['gastos'])){
            foreach ($_POST['gastos'] as $value) {
                $requerimiento=DetalleCompromisoPresupuestal::findOne($value['dataid']);
                // print_r($requerimiento);
                $requerimiento->GastoElegible = 1;
                $requerimiento->Situacion = 3;
                $requerimiento->update();
            }
        }

        if(isset($_POST['noelegible'])){
            foreach ($_POST['noelegible'] as $valuex) {
                $reqm = DetalleCompromisoPresupuestal::findOne($valuex['dataid']);
                // print_r($reqm);
                $reqm->GastoElegible = 0;
                $reqm->Situacion = 2;
                $reqm->Observacion = $valuex['desc'];
                $reqm->update();
            }
        }

        

        $compromiso = CompromisoPresupuestal::findOne($_POST['ID']);
        $compromiso->Situacion = 16;
        $compromiso->update();
        $tiempo=SeguimientoCompromisoPresupuestal::find()
                    ->innerJoin('Usuario','Usuario.ID=SeguimientoCompromisoPresupuestal.UsuarioID')
                    ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                    ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID and UsuarioRol.RolID=2',[':CompromisoPresupuestalID'=>$_POST["ID"]])
                    ->one();
        $tiempo->FechaFin=date('Ymd h:m:s');
        $tiempo->Dias=Yii::$app->tools->RestarFechas($tiempo->FechaInicio,date('Y-m-d h:m:s',strtotime($tiempo->FechaFin)));
        $tiempo->Situacion=8;
        $tiempo->update();
        
        $segu=new SeguimientoCompromisoPresupuestal;
        $segu->CompromisoPresupuestalID=$compromiso->ID;
        $segu->UsuarioID=1118;
        $segu->FechaInicio=date('Ymd h:m:s');
        $segu->Estado=1;
        $segu->Situacion=0;
        $segu->FechaRegistro=date('Ymd h:m:s');
        $segu->save();
        
        $arr = array('Success' => true);
        echo json_encode($arr);
        //print_r($_POST['gastos']);
        die();
        // $requerimiento=CompromisoPresupuestal::findOne($_POST['ID']);
        // $requerimiento->GastoElegible = $_POST['gastos'];
        // $requerimiento->Situacion = 2;
        // $requerimiento->update();
        // $arr = array('Success' => true);
        // echo json_encode($arr);
    }

    public function actionActualizarRequerimientoPac()
    {
        $this->layout='vacio';
        $requerimiento=CompromisoPresupuestal::findOne($_POST['ID']);
        $requerimiento->Situacion = 7;
        $requerimiento->update();
        $arr = array('Success' => true);
        echo json_encode($arr);
    }

    public function actionActualizarRequerimientoListado()
    {
        $this->layout='vacio';
        $requerimiento=Requerimiento::findOne($_POST['ID']);
        $requerimiento->Situacion = 2;
        $requerimiento->update();
        
        $usuario=Usuario::find()
                ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                ->innerJoin('UsuarioProyecto','UsuarioProyecto.UsuarioID=Usuario.ID')
                ->where('UsuarioRol.RolID=:Rol and UsuarioProyecto.CodigoProyecto=:CodigoProyecto',
                        [':Rol'=>5,':CodigoProyecto'=>$requerimiento->CodigoProyecto])
                ->one();
        
        $seguimiento=new SeguimientoRequerimiento;
        $seguimiento->RequerimientoID=$requerimiento->ID;
        $seguimiento->UsuarioID=$usuario->ID;
        $seguimiento->FechaInicio=date('Ymd h:m:s');
        $seguimiento->Estado=1;
        $seguimiento->Situacion=0;
        $seguimiento->FechaRegistro=date('Ymd h:m:s');
        $seguimiento->save();
        
        $arr = array('Success' => true);
        echo json_encode($arr);
    }

    public function beforeAction($action) {
        if($action->id = 'actualizar-requerimiento-listado-eeaa') {
            Yii::$app->request->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }
    
    public function actionActualizarRequerimientoListadoEeaa()
    {
        $this->layout='vacio';
        $requerimiento=Requerimiento::findOne($_POST['ID']);
        $requerimiento->Situacion = 3;
        $requerimiento->update();
        
        $usuario=Usuario::find()
                ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                ->innerJoin('UsuarioProyecto','UsuarioProyecto.UsuarioID=Usuario.ID')
                ->where('UsuarioRol.RolID=:Rol and UsuarioProyecto.CodigoProyecto=:CodigoProyecto',
                        [':Rol'=>5,':CodigoProyecto'=>$requerimiento->CodigoProyecto])
                ->one();
        
        $tiempo=SeguimientoRequerimiento::find()
                    ->innerJoin('Usuario','Usuario.ID=SeguimientoRequerimiento.UsuarioID')
                    ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                    ->where('SeguimientoRequerimiento.RequerimientoID=:RequerimientoID and UsuarioRol.RolID=:RolID and SeguimientoRequerimiento.Estado=:Estado',[':RequerimientoID'=>$requerimiento->ID,':RolID'=>5,':Estado'=>1])
                    ->one();
        if($tiempo)
        {
            $tiempo->FechaFin=date('Ymd h:m:s');
            $tiempo->Dias=(int)Yii::$app->tools->RestarFechas($tiempo->FechaInicio,date('Y-m-d h:m:s',strtotime($tiempo->FechaFin)));
            $tiempo->Situacion=8;
            $tiempo->update();
        }
        
        
        $arr = array('Success' => true);
        echo json_encode($arr);
    }


    public function actionActualizarRequerimientoContabilidad()
    {
        $this->layout='vacio';
        $requerimiento=CompromisoPresupuestal::findOne($_POST['ID']);
        $requerimiento->UsuarioAsignado = $_POST['gastos'];
        $requerimiento->update();
        $arr = array('Success' => true);
        echo json_encode($arr);
    }

    public function tipoGasto($id){
        switch ($id) {
            case '1':
                $gastos = 'Gasto Elegible';
            break;
            case '2':
                $gastos = 'Gasto No Elegible';
            break;
            case '0':
                $gastos = '';
            break;
        }
        return $gastos;
    }

    // Jefe de CONTABILIDAD

    public function actionContabilidad($id = null,$contab=null){
        $this->layout='estandar';
        if(!is_null($id)){
            $cabecera=CompromisoPresupuestal::find()
                ->select('CompromisoPresupuestal.ID,
                CompromisoPresupuestal.GastoElegible,
                CompromisoPresupuestal.Correlativo,
                CompromisoPresupuestal.Codigo,
                CompromisoPresupuestal.MontoTotal,
                CompromisoPresupuestal.Situacion,
                CompromisoPresupuestal.UsuarioAsignado,
                CompromisoPresupuestal.RequerimientoID,
                Persona.Nombre,
                Persona.ApellidoPaterno,
                Persona.ApellidoMaterno,
                CompromisoPresupuestal.CompromisoAnual,
                CompromisoPresupuestal.CompromisoAdministrativo,
                CompromisoPresupuestal.Devengado,
                CompromisoPresupuestal.FormatoUafsi
                ')
            ->innerJoin('Usuario','Usuario.ID = CompromisoPresupuestal.UsuarioAsignado')
            ->innerJoin('Persona','Persona.ID=Usuario.PersonaID')
            ->where('CompromisoPresupuestal.ID=:ID AND Situacion IN ( 3 , 5 )',[':ID'=>$id])
            ->one();
            $requerimiento=Requerimiento::findOne($cabecera->RequerimientoID);
            
            if($requerimiento->TipoRequerimiento==1 || $requerimiento->TipoRequerimiento==2 || $requerimiento->TipoRequerimiento==3)
            {
                $resultados = (new \yii\db\Query())
                    ->from('DetalleCompromisoPresupuestal')
                    ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.ID=DetalleCompromisoPresupuestal.CompromisoPresupuestalID')
                    ->innerJoin('TipoGasto','TipoGasto.ID=DetalleCompromisoPresupuestal.TipoGasto')
                    ->innerJoin('Orden','Orden.ID=DetalleCompromisoPresupuestal.Correlativo')
                    ->where(['DetalleCompromisoPresupuestal.CompromisoPresupuestalID'=>$id])
                    ->distinct()
                    ->all();
            }elseif($requerimiento->TipoRequerimiento==5){
                $resultados = (new \yii\db\Query())
                    ->from('DetalleCompromisoPresupuestal')
                    ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.ID=DetalleCompromisoPresupuestal.CompromisoPresupuestalID')
                    ->innerJoin('TipoGasto','TipoGasto.ID=DetalleCompromisoPresupuestal.TipoGasto')
                    ->innerJoin('DetalleCajaChica','DetalleCajaChica.ID=DetalleCompromisoPresupuestal.Correlativo')
                    ->where(['DetalleCompromisoPresupuestal.CompromisoPresupuestalID'=>$id])
                    ->distinct()
                    ->all();
            }elseif($requerimiento->TipoRequerimiento==4){
                $resultados = (new \yii\db\Query())
                    ->from('DetalleCompromisoPresupuestal')
                    ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.ID=DetalleCompromisoPresupuestal.CompromisoPresupuestalID')
                    ->innerJoin('TipoGasto','TipoGasto.ID=DetalleCompromisoPresupuestal.TipoGasto')
                    ->innerJoin('Viatico','Viatico.ID=DetalleCompromisoPresupuestal.Correlativo')
                    ->where(['DetalleCompromisoPresupuestal.CompromisoPresupuestalID'=>$id])
                    ->distinct()
                    ->all();
            }
            elseif($requerimiento->TipoRequerimiento==6)
            {
                $resultados = (new \yii\db\Query())
                    ->from('DetalleCompromisoPresupuestal')
                    ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.ID=DetalleCompromisoPresupuestal.CompromisoPresupuestalID')
                    ->innerJoin('TipoGasto','TipoGasto.ID=DetalleCompromisoPresupuestal.TipoGasto')
                    ->innerJoin('Encargo','Encargo.ID=DetalleCompromisoPresupuestal.Correlativo')
                    ->where(['DetalleCompromisoPresupuestal.CompromisoPresupuestalID'=>$id])
                    ->distinct()
                    ->all();
            }
            $res = (new \yii\db\Query())
            ->select('Persona.ApellidoPaterno,Persona.ApellidoMaterno,Persona.Nombre,Persona.ID')
            ->from('Persona')
            ->innerJoin('Usuario','Usuario.PersonaID=Persona.ID')
            ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID = Usuario.ID')
            ->where(['UsuarioRol.RolID'=>14])
            ->distinct()
            ->all();

            
            
            return $this->render('_contabilidad_detalle',['resultados'=>$resultados,'cabecera'=>$cabecera,'contab'=>$contab,'res'=>$res,'requerimiento'=>$requerimiento]);
        }else{
            return $this->render('_contabilidad');
        }
    }


    public function actionListadoContabilidad(){
        $this->layout='vacio';    
        
        if(\Yii::$app->user->identity->rol == 13 || \Yii::$app->user->identity->rol == 14)
        {
            $resultado=CompromisoPresupuestal::find()
            ->select('CompromisoPresupuestal.*,Persona.Nombre,Persona.ApellidoPaterno,Persona.ApellidoMaterno,Usuario.username,CompromisoPresupuestal.RequerimientoID')
            ->leftJoin('Usuario','Usuario.ID=CompromisoPresupuestal.UsuarioAsignado')
            ->leftJoin('Persona','Persona.ID=Usuario.PersonaID')
            ->where('CompromisoPresupuestal.Situacion IN ( 3 , 5 )  and CompromisoPresupuestal.Estado=1')
            ->all();
        }
        
        

        $res = (new \yii\db\Query())
            ->select('Persona.ApellidoPaterno,Persona.ApellidoMaterno,Persona.Nombre,Usuario.ID')
            ->from('Persona')
            ->innerJoin('Usuario','Usuario.PersonaID=Persona.ID')
            ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID = Usuario.ID')
            ->where(['UsuarioRol.RolID'=>14])
            ->distinct()
            ->all();

        $nro=0;
        foreach($resultado as $result)
        {
            $nro++;
            $req = TipoGasto::find()
            // ->select('.Nombre NombreGasto')
            ->innerJoin('Requerimiento','TipoGasto.ID=Requerimiento.TipoRequerimiento')
            ->where('Requerimiento.ID=:ID',[':ID'=>$result['RequerimientoID'] ])
            ->one();

            echo "<tr>";
                echo "<td> N° " . str_pad($result["Correlativo"], 3, "0", STR_PAD_LEFT) ."-".$result["Annio"] . "</td>";
                echo "<td> " . $result["Codigo"] . "</td>";
                echo "<td> " . $req->Nombre . "</td>";
                echo "<td>" . date('d-m-Y',strtotime($result["FechaRegistro"])) . "</td>";
                echo "<td>" . $this->getSituacionUafsi($result["Situacion"]) . "</td>";

                if(\Yii::$app->user->identity->rol == 14){
                    if($result['UsuarioAsignado'] != ''){
                        echo '<td>'.$result['Nombre'].'</td>';
                    }else{
                        echo '<td>-</td>';
                    }
                    if($result['CompromisoAnual'] !=''){
                        echo "<td><a href='?id=".$result["ID"]."&contab=1' class='btn btn-info'>Ver detalle</a></td>";
                    }else{
                        echo "<td><a href='?id=".$result["ID"]."&contab=1' class='btn btn-danger'>Ver detalle</a></td>";
                    }
                    // echo "<td><a href='#' data-id='".$result["ID"]."' data-codigo-proyecto='".$result["Codigo"]."' class='btn btn-primary generar-compromiso'>Generar Compromiso Anual</a> <a href='?id=".$result["ID"]."&contab=1' class='btn btn-info'>Ver</buttom></td>";
                }

                if(\Yii::$app->user->identity->rol == 13){
                    if($result['UsuarioAsignado'] == ''){
                        $ss = "<td><select name='gasto' class='form-control gastos' data-id='".$result["ID"]."'>";
                            $ss.= '<option value="">[SELECCIONE]</option>';
                            foreach ($res as $value) {
                                $ss.= '<option value="'.$value['ID'].'" >'.$value['Nombre'].'</option>';
                            }
                        $ss.="</td>";
                        echo $ss;
                        echo "<td><a href='#' class='btn btn-primary verifica'>Enviar</a> <a href='?id=".$result["ID"]."' class='btn btn-info'>Ver</buttom></td>";
                    }else{
                        echo '<td>'.$result['Nombre'].'</td>';
                        echo "<td><a href='?id=".$result["ID"]."' class='btn btn-info'>Ver</buttom></td>";
                    }
                }


            echo "</tr>";
        }
    }
    
    public function actionListadoPoa($CodigoProyecto=null,$ComponenteID=null,$ActividadID=null){
        $this->layout='vacio';    
        if($CodigoProyecto)
        {
            $informacionGeneral=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])->one();
            $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacionGeneral->InvestigadorID])->one();
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
            $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacionGeneral->InvestigadorID])->one();
        }
        $PasoCritico=PasoCritico::find()->where('ProyectoID=:ProyectoID and Estado=1 and Situacion=2',[':ProyectoID'=>$proyecto->ID])->one();
        if($ComponenteID)
        {
            $resultados = (new \yii\db\Query())
            ->select('AreSubCategoria.Especifica,Componente.Correlativo as ComponenteC,Actividad.Correlativo ActividadC,AreSubCategoria.Correlativo AreSubCategoriaC,ActRubroElegible.RubroElegibleID,AreSubCategoria.Nombre,AreSubCategoria.ID,Componente.ID ComponenteID,Actividad.ID ActividadID,AreSubCategoria.Codificacion,AreSubCategoria.MetaFisica')
            ->from('AreSubCategoria')
            ->innerJoin('CronogramaAreSubCategoria','CronogramaAreSubCategoria.AreSubCategoriaID=AreSubCategoria.ID')
            ->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')
            ->innerJoin('RubroElegible','RubroElegible.ID=ActRubroElegible.RubroElegibleID')
            ->innerJoin('Actividad','Actividad.ID=ActRubroElegible.ActividadID')
            ->innerJoin('Componente','Componente.ID=Actividad.ComponenteID')
            ->innerJoin('Poa','Poa.ID=Componente.PoaID')
            ->innerJoin('Proyecto','Proyecto.ID=Poa.ProyectoID')
            ->innerJoin('Investigador','Investigador.ID=Proyecto.InvestigadorID')
            ->innerJoin('InformacionGeneral','InformacionGeneral.InvestigadorID=Investigador.ID')
            ->where('CronogramaAreSubCategoria.MetaFisica!=0 and InformacionGeneral.Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])
            ->andWhere(['Componente.ID'=>$ComponenteID])
            ->andWhere(['between', 'CronogramaAreSubCategoria.Mes', $PasoCritico->MesInicio, $PasoCritico->MesFin])
            ->orderBy('Componente.Correlativo asc,AreSubCategoria.Nombre asc,AreSubCategoria.Correlativo asc')
            ->distinct()
            ->all();
        }
        elseif($ActividadID)
        {
            $resultados = (new \yii\db\Query())
            ->select('AreSubCategoria.Especifica,Componente.Correlativo as ComponenteC,Actividad.Correlativo ActividadC,AreSubCategoria.Correlativo AreSubCategoriaC,ActRubroElegible.RubroElegibleID,AreSubCategoria.Nombre,AreSubCategoria.ID,Componente.ID ComponenteID,Actividad.ID ActividadID,AreSubCategoria.Codificacion,AreSubCategoria.MetaFisica')
            ->from('AreSubCategoria')
            ->innerJoin('CronogramaAreSubCategoria','CronogramaAreSubCategoria.AreSubCategoriaID=AreSubCategoria.ID')
            ->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')
            ->innerJoin('RubroElegible','RubroElegible.ID=ActRubroElegible.RubroElegibleID')
            ->innerJoin('Actividad','Actividad.ID=ActRubroElegible.ActividadID')
            ->innerJoin('Componente','Componente.ID=Actividad.ComponenteID')
            ->innerJoin('Poa','Poa.ID=Componente.PoaID')
            ->innerJoin('Proyecto','Proyecto.ID=Poa.ProyectoID')
            ->innerJoin('Investigador','Investigador.ID=Proyecto.InvestigadorID')
            ->innerJoin('InformacionGeneral','InformacionGeneral.InvestigadorID=Investigador.ID')
            ->where('CronogramaAreSubCategoria.MetaFisica!=0 and InformacionGeneral.Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])
            ->andWhere(['between', 'CronogramaAreSubCategoria.Mes', $PasoCritico->MesInicio, $PasoCritico->MesFin])
            // ->andWhere(['Componente.ID'=>$ComponenteID])
            ->andWhere(['Actividad.ID'=>$ActividadID])
            ->orderBy('Componente.Correlativo asc,AreSubCategoria.Nombre asc,AreSubCategoria.Correlativo asc')
            ->distinct()
            ->all();
        }
        else{
            $resultados = (new \yii\db\Query())
            ->select('AreSubCategoria.Especifica,Componente.Correlativo as ComponenteC,Actividad.Correlativo ActividadC,ActRubroElegible.RubroElegibleID,AreSubCategoria.Correlativo AreSubCategoriaC,AreSubCategoria.Nombre,AreSubCategoria.ID,Componente.ID ComponenteID,Actividad.ID ActividadID,AreSubCategoria.Codificacion,AreSubCategoria.MetaFisica')
            ->from('AreSubCategoria')
            ->innerJoin('CronogramaAreSubCategoria','CronogramaAreSubCategoria.AreSubCategoriaID=AreSubCategoria.ID')
            ->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')
            ->innerJoin('RubroElegible','RubroElegible.ID=ActRubroElegible.RubroElegibleID')
            ->innerJoin('Actividad','Actividad.ID=ActRubroElegible.ActividadID')
            ->innerJoin('Componente','Componente.ID=Actividad.ComponenteID')
            ->innerJoin('Poa','Poa.ID=Componente.PoaID')
            ->innerJoin('Proyecto','Proyecto.ID=Poa.ProyectoID')
            ->innerJoin('Investigador','Investigador.ID=Proyecto.InvestigadorID')
            ->innerJoin('InformacionGeneral','InformacionGeneral.InvestigadorID=Investigador.ID')
            ->where('CronogramaAreSubCategoria.MetaFisica!=0 and InformacionGeneral.Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])
            ->andWhere(['between', 'CronogramaAreSubCategoria.Mes', $PasoCritico->MesInicio, $PasoCritico->MesFin])
            ->orderBy('AreSubCategoria.Nombre asc')
            ->distinct()
            ->all();
        }
        
            
            
        $nro=0;
        foreach($resultados as $result)
        {
            $dets = DetalleRequerimiento::find()
            ->select('sum(DetalleRequerimiento.Cantidad) Cantidad')
            ->innerJoin('Requerimiento','Requerimiento.ID=DetalleRequerimiento.RequerimientoID')
            ->where('DetalleRequerimiento.AreSubCategoriaID = :AreSubCategoriaID AND DetalleRequerimiento.Estado = 1 and Requerimiento.Situacion in (1,2,3)',[':AreSubCategoriaID'=>$result['ID'] ])
            ->one();
            $color = '';
            
            if(!empty($dets->Cantidad) && $dets->Cantidad==$result["MetaFisica"]){
                $color = 'background:#f96767';
            }
            elseif(!empty($dets->Cantidad) && $dets->Cantidad<$result["MetaFisica"])
            {
                $color = 'background:#F08801';
            }
            
            echo "<tr style='cursor:pointer' class='pintar_poa' id='".$nro."' data-id='".$result["ID"]."' data-componente='".$result["ComponenteID"]."' data-rubro-elegible='".$result["RubroElegibleID"]."' data-actividad='".$result["ActividadID"]."'>";
            echo "<td style='".$color."'> " . $result["ComponenteC"] . ".".$result["ActividadC"].".".$result["RubroElegibleID"].".".$result["AreSubCategoriaC"]."</td>";
            echo "<td style='".$color."'> " . $result["RubroElegibleID"].".".$result["Codificacion"]."</td>";
            echo "<td style='".$color."'> " . $result["Nombre"] ."/".$result["Especifica"]. "</td>";
            echo "</tr>";
            
            $nro++;
        }
        
    }
    
    public function actionPoa($CodigoProyecto=null)
    {
        $this->layout='vacio';
        $informacionGeneral=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacionGeneral->InvestigadorID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID and Estado=1',[':ProyectoID'=>$proyecto->ID])->one();
        $componentes=Componente::find()->where('PoaID=:PoaID',[':PoaID'=>$poa->ID])->OrderBy('Correlativo asc')->all();
        return $this->render('poa',['CodigoProyecto'=>$CodigoProyecto,'componentes'=>$componentes]);
    }
    
    public function actionGetDatoPoa($id=null,$componenteid=null,$actividadid=null,$rubroelegibleid=null)
    {
        $Recurso=AreSubCategoria::findOne($id);
        
                        
        $Componente=Componente::findOne($componenteid);
        $Actividad=Actividad::findOne($actividadid);
        $RubroElegible=RubroElegible::findOne($rubroelegibleid);
        $Poa=Poa::findOne($Componente->PoaID);
        $PasoCritico=PasoCritico::find()->where('ProyectoID=:ProyectoID and Estado=1 and Situacion=2',[':ProyectoID'=>$Poa->ProyectoID])->one();

        $RecursoCronograma=CronogramaAreSubCategoria::find()
                        ->select('sum(CronogramaAreSubCategoria.MetaFisica) MetaFisica')
                        ->where('AreSubCategoriaID=:AreSubCategoriaID',[':AreSubCategoriaID'=>$id])
                        ->andWhere(['between', 'CronogramaAreSubCategoria.Mes', $PasoCritico->MesInicio, $PasoCritico->MesFin])
                        ->one();

        $detalles=DetalleRequerimiento::find()
        ->select('sum(DetalleRequerimiento.Cantidad) Cantidad')
        ->innerJoin('Requerimiento','Requerimiento.ID=DetalleRequerimiento.RequerimientoID')
        ->where('DetalleRequerimiento.AreSubCategoriaID=:AreSubCategoriaID AND DetalleRequerimiento.Estado =1 and Requerimiento.Situacion in (1,3) AND Requerimiento.PasoCriticoID=:PasoCriticoID',[':AreSubCategoriaID'=>$Recurso->ID,':PasoCriticoID'=>$PasoCritico->ID])
        ->one();
        // if($detalles->Cantidad )
        $resultados=Yii::$app->db->createCommand("
            SELECT Componente.Correlativo CorrelaCompr,Actividad.Correlativo CorrelaActv,AreSubCategoria.Correlativo CorrelaRecurso,ActRubroElegible.RubroElegibleID,AreSubCategoria.Codificacion,
                AreSubCategoria.CostoUnitario,AreSubCategoria.Nombre,AreSubCategoria.Especifica,
                AreSubCategoria.UnidadMedida,AreSubCategoria.CostoUnitario,AreSubCategoria.MetaFisica
            FROM AreSubCategoria 
            INNER JOIN ActRubroElegible ON ActRubroElegible.ID = AreSubCategoria.ActRubroElegibleID
            INNER JOIN RubroElegible ON RubroElegible.ID = ActRubroElegible.RubroElegibleID
            INNER JOIN Actividad ON ActRubroElegible.ActividadID = Actividad.ID
            INNER JOIN Componente ON Actividad.ComponenteID = Componente.ID
            WHERE AreSubCategoria.ID =".$id)->queryAll();


        $arrayJson=['Success'=>true,
                    'aresubcategoriaid'=>$Recurso->ID,
                    'recurso'=>$Recurso->Correlativo.".".$Recurso->Nombre,
                    'componenteid'=>$Componente->ID,
                    'componente'=>$Componente->Correlativo.".".$Componente->Nombre,
                    'actividadid'=>$Actividad->ID,
                    'actividad'=>$Actividad->Correlativo.".".$Actividad->Nombre,
                    'codigo'=>$Componente->Correlativo.".".$Actividad->Correlativo.".".$RubroElegible->ID.".".$Recurso->Correlativo,
                    'matriz'=>$RubroElegible->ID.".".$Recurso->Correlativo,
                    'metafisica'=> ($RecursoCronograma->MetaFisica - $detalles->Cantidad),
                    'preciounitario'=>$Recurso->CostoUnitario,
                    'codificacion'=>$Recurso->Codificacion,
                    'resultado'=> $resultados,
                    'codematriz'=>$rubroelegibleid.'.'.$Recurso->Codificacion];
        return json_encode($arrayJson);
    }

    public function actionExcel($CodigoProyecto=null)
    {
        $resultados = (new \yii\db\Query())
            ->select('*,Requerimiento.TipoRequerimiento RTipoRequerimiento,Requerimiento.Correlativo RCorrelativo,Componente.Nombre NComponente,Componente.Correlativo CComponente,Actividad.Nombre NActividad,Actividad.Correlativo CActividad,AreSubCategoria.Nombre NAreSubCategoria,AreSubCategoria.Correlativo CAreSubCategoria,DetalleRequerimiento.Cantidad CDetalleRequerimiento')
            ->from('Requerimiento')
            ->innerJoin('DetalleRequerimiento','DetalleRequerimiento.RequerimientoID=Requerimiento.ID')
            ->innerJoin('AreSubCategoria','AreSubCategoria.ID=DetalleRequerimiento.AreSubCategoriaID')
            ->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')
            ->innerJoin('RubroElegible','RubroElegible.ID=ActRubroElegible.RubroElegibleID')
            ->innerJoin('Actividad','Actividad.ID=ActRubroElegible.ActividadID')
            ->innerJoin('Componente','Componente.ID=Actividad.ComponenteID')
            ->where(['Requerimiento.CodigoProyecto'=>$CodigoProyecto])
            ->distinct()
            ->all();
        $requerimiento=new Requerimiento;
        return $this->render('excel',['resultados'=>$resultados,'requerimiento'=>$requerimiento]);
    }

    public function actionSearch($term = null){
        if(empty($term)){
            $aa = '';
        }else{
            $aa = $term;
        }
        $Recurso=AreSubCategoria::find()->where(['LIKE ', 'Nombre', '%'.$aa.'%'])->all();
        $arr = array();
        foreach ($Recurso as $req) {
            $arr[] = array(
                'id' => $req->Nombre,
                'label' => $req->Nombre,
                'value' => $req->Nombre
            );
        }

        if(isset ($_GET['callback']))
        {
            header("Content-Type: application/json");
            echo $_GET['callback']."(".json_encode($arr).")";
        }
    }

    // Obtener recursos
    public function actionRecursosIdentificador($id = null,$identicador = null){
        $resultados = (new \yii\db\Query())
            ->select('Componente.Correlativo as ComponenteC,Actividad.Correlativo ActividadC,AreSubCategoria.Correlativo AreSubCategoriaC,AreSubCategoria.Nombre,AreSubCategoria.ID,Componente.ID ComponenteID,Actividad.ID ActividadID')
            ->from('Componente')
            ->innerJoin('Actividad','Actividad.ComponenteID = Componente.ID')
            ->innerJoin('ActRubroElegible','ActRubroElegible.ActividadID = Actividad.ID')
            ->innerJoin('AreSubCategoria','AreSubCategoria.ActRubroElegibleID = ActRubroElegible.ID')
            ->where(['Componente.ID'=>$_POST['id']])
            // ->distinct()
            ->all();


        $nro=0;
        foreach($resultados as $result)
        {
            echo "<tr class='pintar_poa' id='".$nro."' data-id='".$result["ID"]."' data-componente='".$result["ComponenteID"]."' data-actividad='".$result["ActividadID"]."'>";
            echo "<td> " . $result["ComponenteC"] . ".".$result["ActividadC"].".".$result["AreSubCategoriaC"]."</td>";
            echo "<td> " . $result["Nombre"] . "</td>";
            echo "</tr>";
            $nro++;
        }
         echo json_encode($arr);
    }
    
    public function actionAdjuntarDocumento($ID=null)
    {
        $this->layout='vacio';
        $requerimiento=Requerimiento::findOne($ID);
        if($requerimiento->load(Yii::$app->request->post())){
            
            $requerimiento->archivo = UploadedFile::getInstance($requerimiento, 'archivo');
            if($requerimiento->archivo)
            {
                $requerimiento->archivo->saveAs('requerimientos/' . $requerimiento->ID . '.' . $requerimiento->archivo->extension);
                $requerimiento->Documento=$requerimiento->ID . '.' . $requerimiento->archivo->extension;
            }
            $requerimiento->update();
            return $this->redirect(['/requerimiento']);
        }
        return $this->render('adjuntar-documento',['ID'=>$ID,'requerimiento'=>$requerimiento]);
    }
    
    
    
    public function actionGenerar($ID)
    {
        $this->layout='estandar';
        $tipoGasto = TipoGasto::find()->all();
        $requerimiento=Requerimiento::findOne($ID);
        $detallesrequerimientos=DetalleRequerimiento::find()->select('DetalleRequerimiento.ID,CodigoMatriz,TipoDetalleOrdenID,Situacion,PrecioUnitario,Codificacion,Recurso,sum(Cantidad) Cantidad')->where('RequerimientoID=:RequerimientoID',[':RequerimientoID'=>$ID])->groupBy('CodigoMatriz,TipoDetalleOrdenID,Situacion,PrecioUnitario,Codificacion,Recurso,DetalleRequerimiento.ID')->orderBy('TipoDetalleOrdenID')->all();
        
        $detalles=DetalleRequerimiento::find()->where('RequerimientoID=:RequerimientoID',[':RequerimientoID'=>$ID])->all();
        if($requerimiento->Situacion==1 || $requerimiento->Situacion==2)
        {
            // \Yii::$app->getSession()->setFlash('Message', 'OCurrio un eror en el registro');
            return $this->redirect(['/requerimiento']);
        }
        return $this->render('generar',['requerimiento'=>$requerimiento,'detallesrequerimientos'=>$detallesrequerimientos,'detalles'=>$detalles,'tipoGasto'=>$tipoGasto]);
    }
    
    public function actionEeaaObservacion($ID)
    {
        $this->layout='vacio';
        $requerimiento=Requerimiento::findOne($ID);
        
        if($requerimiento->load(Yii::$app->request->post())){
            $detalles=DetalleRequerimiento::find()->where('RequerimientoID=:RequerimientoID',[':RequerimientoID'=>$requerimiento->ID])->all();;
            $nuevo=new Requerimiento;
            $nuevo->CodigoProyecto=$requerimiento->CodigoProyecto;
            $nuevo->Situacion=4;
            $nuevo->FechaRegistro=date('Ymd h:m:s');
            $nuevo->Estado=1;
            $nuevo->Referencia=$requerimiento->Referencia;
            $nuevo->Asunto=$requerimiento->Asunto;
            $nuevo->TipoRequerimiento=$requerimiento->TipoRequerimiento;
            $nuevo->Documento=$requerimiento->Documento;
            $nuevo->FechaSolicitud=date('Ymd h:m:s',strtotime($requerimiento->FechaSolicitud));
            $nuevo->Correlativo=$requerimiento->Correlativo;
            $nuevo->Descripcion=$requerimiento->Descripcion;
            $nuevo->Annio=$requerimiento->Annio;
            $nuevo->CUT=$requerimiento->CUT;
            $nuevo->PasoCriticoID=$requerimiento->PasoCriticoID;
            $nuevo->save();
            $requerimiento->Estado=0;
            $requerimiento->Situacion=8;
            $requerimiento->update();
            foreach($detalles as $detalle)
            {
                $det=new DetalleRequerimiento;
                $det->RequerimientoID=$nuevo->ID;
                $det->AreSubCategoriaID=$detalle->AreSubCategoriaID;
                $det->CodigoMatriz=$detalle->CodigoMatriz;
                $det->Cantidad=$detalle->Cantidad;
                $det->PrecioUnitario=$detalle->PrecioUnitario;
                $det->Descripcion=$detalle->Descripcion;
                $det->Total=$detalle->Total;
                $det->Objetivo=$detalle->Objetivo;
                $det->Actividad=$detalle->Actividad;
                $det->Recurso=$detalle->Recurso;
                $det->Codificacion=$detalle->Codificacion;
                $det->Situacion=$detalle->Situacion;
                $det->save();
            }
            return $this->redirect(['/requerimiento/eeaa']);
        }
        
        return $this->render('eeaa-observacion',['requerimiento'=>$requerimiento]);
    }
    // public function action
}
