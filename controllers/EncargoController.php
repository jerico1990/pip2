<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\PasoCritico;
use app\models\Usuario;
use app\models\Persona;
use app\models\Investigador;
use app\models\Proyecto;
use app\models\InformacionGeneral;
use app\models\Poa;
use app\models\Componente;
use app\models\Actividad;
use app\models\CronogramaProyecto;
use app\models\ActRubroElegible;
use app\models\CronogramaComponente;
use app\models\CronogramaActividad;
use app\models\CronogramaActRubroElegible;
use app\models\RubroElegible;
use app\models\AreSubCategoria;
use app\models\CronogramaAreSubCategoria;
use app\models\MarcoLogicoActividad;
use app\models\AreSubCategoriaObservacion;
use app\models\CertificacionPresupuestal;
use app\models\DetalleCertificacionPresupuestal;
use app\models\UnidadOperativa;
use app\models\SiafSecuencialProyecto;
use app\models\Orden;
use app\models\Siaf;
use app\models\Encargo;
use app\models\Requerimiento;
use app\models\DetalleRequerimiento;
use app\models\TipoContratacion;
use PhpOffice\PhpWord\PhpWord;
use yii\web\UploadedFile;

//951 encargo
//viaticos
class EncargoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($CodigoProyecto=null)
    {
        $this->layout='estandar';
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        return $this->render('index',['CodigoProyecto'=>$CodigoProyecto]);
    }
    
    
    public function actionLista($CodigoProyecto=null){
        
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        
        $resultados = (new \yii\db\Query())
            ->select('Encargo.*')
            ->from('Encargo')
            ->where(['Encargo.CodigoProyecto'=>$CodigoProyecto])
            ->distinct()
            ->all();

        $nro=0;
        foreach($resultados as $result)
        {
            $nro++;
            echo "<tr>";
            echo "<td> N° Encargo  E" . str_pad($result["Correlativo"], 3, "0", STR_PAD_LEFT) . "</td>";
            echo "<td>" . $result["NombresEncargo"]." ".$result["ApellidosEncargo"] . "</td>";
            echo "<td>" . $result["CargoEncargo"] . "</td>";
            echo "<td>" . $result["Bienes"] . "</td>";
            echo "<td>" . $result["Servicios"] . "</td>";
            echo "<td>" . $result["Total"] . "</td>";
            echo "<td><a target='_blank' href='encargo/" . $result["Documento"] . "'><span class='fa fa-cloud-download'></span></a></td>";
            if($result["Situacion"]==0)
            {
                echo "<td><a href='#' class='btn btn-info verifica-aprobar' data-id='".$result["ID"]."'>ENVIAR</a> <a target='_blank' href='requerimiento-encargo/plantilla?ID=" . $result["ID"] . "'><span class='fa fa-cloud-download'></span></a> <a href='#' class='btn-edit-requerimiento-encargo' data-id='".$result["ID"]."'><i class='fa fa-edit fa-lg'></i></a> <a href='#' class='btn-remove' data-id='".$result["ID"]."'><i class='fa fa-remove fa-lg'></i></a> </td>";
            }
            else
            {
                echo "<td></td>";
            }
            
            echo "</tr>";
        }
        
    }
    
    public function actionCrear($RequerimientoID=null,$CodigoProyecto=null,$DetallesRequerimientosIDs=[])
    {
        $this->layout='vacio';
        $requerimientoEncargo=new Encargo;
        $Requerimiento=Requerimiento::findOne($RequerimientoID);
        $detallesRequerimientos=DetalleRequerimiento::find()->where('ID in ('.$DetallesRequerimientosIDs.')')->all();
        $detal=DetalleRequerimiento::find()->select('sum(Cantidad) Cantidad,sum(Cantidad*PrecioUnitario) Total')->where('ID in ('.$DetallesRequerimientosIDs.')')->one();
        
        $certificacion=DetalleCertificacionPresupuestal::find()
                    ->select('DetalleCertificacionPresupuestal.CodigoCertificacion')
                    ->innerJoin('CertificacionPresupuestal','DetalleCertificacionPresupuestal.CertificacionPresupuestalID=CertificacionPresupuestal.ID')
                    ->where('CertificacionPresupuestal.Situacion=1 and CertificacionPresupuestal.Estado=1 and DetalleCertificacionPresupuestal.Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])
                    ->one();
        $integrantes=RecursosHumanosController::actionIntegrantes($CodigoProyecto,2);
        $tipoContratacion=TipoContratacion::find()->where('Estado=1')->all();
        if($requerimientoEncargo->load(Yii::$app->request->post())){
            // $GetOracleTipoCambios = \Yii::$app->db->createCommand("Select * from openquery(ORACLE,'SELECT FECH_CAMB_TIP,VALO_SOLE_TIP FROM TIPO_CAMBIO WHERE TO_CHAR(FECH_CAMB_TIP,''DD-MM-YYYY'')=TO_CHAR(SYSDATE,''DD-MM-YYYY'')')")->queryOne();
            // var_dump($GetOracleTipoCambios);
            
            $requerimientoEncargo->FechaInicio=date('Ymd h:m:s',strtotime($requerimientoEncargo->FechaInicio));
            $requerimientoEncargo->FechaFin=date('Ymd h:m:s',strtotime($requerimientoEncargo->FechaFin));
            $requerimientoEncargo->Situacion=1;
            $requerimientoEncargo->Estado=1;
            $requerimientoEncargo->TipoCambio= 1;
            // $requerimientoEncargo->TipoCambio= $GetOracleTipoCambios['VALO_SOLE_TIP'];
            $requerimientoEncargo->FechaRegistro=date('Ymd');
            $requerimientoEncargo->Correlativo=$this->CorrelativoEncargo($CodigoProyecto);
            
            $requerimientoEncargo->Bienes    = str_replace(',','', $requerimientoEncargo->Bienes);
            $requerimientoEncargo->Servicios = str_replace(',','', $requerimientoEncargo->Servicios);

            // print_r($detalle);
            // die();

            $requerimientoEncargo->Total=str_replace(',','', $requerimientoEncargo->Bienes)+str_replace(',','', $requerimientoEncargo->Servicios);
            
            $requerimientoEncargo->Annio=date('Y');
            $requerimientoEncargo->Certificacion=$certificacion->CodigoCertificacion;
            $saved = $requerimientoEncargo->save();
            if($saved == 1){
                foreach($detallesRequerimientos as $detalleRequerimiento){
                    $detalle=DetalleRequerimiento::findOne($detalleRequerimiento->ID);
                    $detalle->TipoDetalleOrdenID=$requerimientoEncargo->ID;
                    $detalle->Situacion=3;
                    $detalle->update();
                }
                
                $requerimientoEncargo->archivo = UploadedFile::getInstance($requerimientoEncargo, 'archivo');
                if($requerimientoEncargo->archivo)
                {
                    $requerimientoEncargo->archivo->saveAs('encargos/R' . $requerimientoEncargo->ID . '.' . $requerimientoEncargo->archivo->extension);
                    $requerimientoEncargo->ResolucionDirectorial='R'.$requerimientoEncargo->ID . '.' . $requerimientoEncargo->archivo->extension;
                }
                $requerimientoEncargo->update();
            }else{
                \Yii::$app->getSession()->setFlash('Message', 'Ocurrio un eror en el registro');
            }
            
            return $this->redirect(['requerimiento/generar','ID'=>$Requerimiento->ID]);
        }
        return $this->render('_form',['CodigoProyecto'=>$CodigoProyecto,'Requerimiento'=>$Requerimiento,'PrecioTotal'=>$detal->Total,'integrantes'=>$integrantes,'tipoContratacion'=>$tipoContratacion,'DetallesRequerimientosIDs'=>$DetallesRequerimientosIDs]);
    }
    
    public function actionActualizar($ID=null)
    {
        $this->layout='vacio';
        $requerimientoEncargo=Encargo::findOne($ID);
        $AreSubCategoria=AreSubCategoria::findOne($requerimientoEncargo->AreSubCategoriaID);
        $ActRubroElegible=ActRubroElegible::findOne($AreSubCategoria->ActRubroElegibleID);
        $Actividad=Actividad::findOne($ActRubroElegible->ActividadID);
        $Componente=Componente::findOne($Actividad->ComponenteID);
        $requerimientoEncargo->Componente=$Componente->Correlativo.".".$Componente->Nombre;
        $requerimientoEncargo->ComponenteID=$Componente->ID;
        $requerimientoEncargo->Actividad=$Actividad->Correlativo.".".$Actividad->Nombre;
        $requerimientoEncargo->ActividadID=$Actividad->ID;
        $requerimientoEncargo->AreSubCategoria=$AreSubCategoria->Correlativo.".".$AreSubCategoria->Nombre;
        $requerimientoEncargo->AreSubCategoriaID=$AreSubCategoria->ID;
        $requerimientoEncargo->CodigoPoa=$Componente->Correlativo.".".$Actividad->Correlativo.".".$AreSubCategoria->Correlativo;
        if($requerimientoEncargo->load(Yii::$app->request->post())){

            $requerimientoEncargo->Bienes    = str_replace(',','', $requerimientoEncargo->Bienes);
            $requerimientoEncargo->Servicios = str_replace(',','', $requerimientoEncargo->Servicios);
            
            $requerimientoEncargo->save();
            $requerimientoEncargo->archivo = UploadedFile::getInstance($requerimientoEncargo, 'archivo');
            if($requerimientoEncargo->archivo)
            {
                $requerimientoEncargo->archivo->saveAs('encargos/R' . $requerimientoEncargo->ID . '.' . $requerimientoEncargo->archivo->extension);
                $requerimientoEncargo->ResolucionDirectorial=$requerimientoEncargo->ID . '.' . $requerimientoEncargo->archivo->extension;
            }
            $requerimientoEncargo->update();
            return $this->redirect(['requerimiento/generar','ID'=>$RequerimientoID]);
        }
        return $this->render('_form_actualizar',['requerimientoEncargo'=>$requerimientoEncargo,'ID'=>$ID]);
    }
    
    public function CorrelativoEncargo($CodigoProyecto)
    {
        $requerimientoEncargo=Encargo::find()->where('CodigoProyecto=:CodigoProyecto and Estado=1',[':CodigoProyecto'=>$CodigoProyecto])->orderBy('Correlativo desc')->one();
        if($requerimientoEncargo)
        {
            return $requerimientoEncargo->Correlativo+1;
        }
        else
        {
            return 1;
        }
    }
    
    
    public function actionPlantilla($ID=null)
    {
        $requerimiento=Encargo::findOne($ID);
        $informacion=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$requerimiento->CodigoProyecto])->one();
        $eea=UnidadOperativa::find()->where('ID=:ID',[':ID'=>$informacion->UnidadOperativaID])->one();
        $usuario=Usuario::find()->where('username=:username',[':username'=>$requerimiento->CodigoProyecto])->one();
        $persona=Persona::find()->where('ID=:ID',[':ID'=>$usuario->PersonaID])->one();
        $contrato="";
        if($requerimiento->ContratoEncargo==1)
        {
            $contrato="Consultor";
        }
        elseif($requerimiento->ContratoEncargo==2)
        {
            $contrato="Servicio de Tercero";
        }
        elseif($requerimiento->ContratoEncargo==3)
        {
            $contrato="Otros";
        }
        
        require_once(Yii::$app->basePath . '/web/PHPWord/src/PhpWord/Autoloader.php');     
        \PhpOffice\PhpWord\Autoloader::register();
        $phpWord = new PhpWord();
       
        $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/documentos/PLANTILLA_SOLICITUD_ENCARGOS.docx');
        //$template->setValue('ID', str_pad($orden->Correlativo, 3, "0", STR_PAD_LEFT)."-".$orden->Annio);
        $template->setValue('EEA',$eea->Nombre);
        //$template->setValue('FECHAORDEN', date('d/m/Y',strtotime($orden->FechaOrden)));
        $template->setValue('CODIGOPROYECTO', $requerimiento->CodigoProyecto);
        $template->setValue('IRP', $persona->Nombre." ".$persona->ApellidoPaterno." ".$persona->ApellidoMaterno);
        $template->setValue('ACTIVIDAD', $requerimiento->DescripcionActividad);
        $template->setValue('FECHAINICIO', date('d-m-Y',strtotime($requerimiento->FechaInicio)));
        $template->setValue('FECHAFIN', date('d-m-Y',strtotime($requerimiento->FechaFin)));
        $template->setValue('TOTAL', number_format($requerimiento->Total, 2, '.', ' '));
        $template->setValue('BIENES', number_format($requerimiento->Bienes, 2, '.', ' '));
        $template->setValue('SERVICIOS', number_format($requerimiento->Servicios, 2, '.', ' '));
        $template->setValue('NOMBRECOMPLETO', $requerimiento->NombresEncargo." ".$requerimiento->ApellidosEncargo);
        $template->setValue('DNI', $requerimiento->DNIEncargo);
        $template->setValue('CARGO', $requerimiento->CargoEncargo);
        $template->setValue('TIPOCONTRATO', $contrato);
        
        
        $temp_file = tempnam(sys_get_temp_dir(), 'PHPWord');
        $template->saveAs($temp_file);
        $contentType = 'Content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document;';
        header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
        header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
        header ( "Cache-Control: no-cache, must-revalidate" );
        header ( "Pragma: no-cache" );
        header ( $contentType );
        header ( "Content-Disposition: attachment; filename='Orden.docx'");
        readfile($temp_file); // or echo file_get_contents($temp_file);
        unlink($temp_file);  // remove temp file
        return true;    
    }
    
    
    public function actionListaEeaa(){
        $this->layout='estandar';
        return $this->render('lista-eeaa');
    }
    
    
    public function actionAdjuntarDocumento($ID=null){
        $this->layout='vacio';
        $encargo=Encargo::findOne($ID);
        $Requerimiento=Requerimiento::findOne($encargo->RequerimientoID);
        if($encargo->load(Yii::$app->request->post())){
            $encargo->Situacion=3;
            $encargo->archivo = UploadedFile::getInstance($encargo, 'archivo');
            if($encargo->archivo)
            {
                $encargo->archivo->saveAs('encargos/' . $encargo->ID . '.' . $encargo->archivo->extension);
                $encargo->Documento=$encargo->ID . '.' . $encargo->archivo->extension;
            }
            $encargo->update();
            return $this->redirect(['requerimiento/generar','ID'=>$Requerimiento->ID]);
        }
        return $this->render('adjuntar-documento',['ID'=>$ID,'encargo'=>$encargo]);
    }
    
    public function actionEliminar($id=null)
    {
        $encargo=Encargo::findOne($id);
        $encargo->Situacion=6;
        $encargo->Estado=0;
        $encargo->update();

        $requerimiento=Requerimiento::findOne($encargo->RequerimientoID);
        
        // print_r($encargo);
        // die();

        $req = DetalleRequerimiento::find()->where(['RequerimientoID'=>$requerimiento->ID,'TipoDetalleOrdenID'=>$id])->one();
        // $req->TipoDetalleOrdenID='';
        $req->Situacion=2;
        // print_r($req);
        // die();
        $req->update();

        // $detalles=DetalleRequerimiento::find()->where('RequerimientoID=:RequerimientoID',[':RequerimientoID'=>$requerimiento->ID])->all();
        // foreach($detalles as $detalle)
        // {
        //     $detalle->Situacion=2;
        //     $detalle->TipoDetalleOrdenID='';
        //     $detalle->update();
        // }

        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
    public function actionEliminarAdjunto($id=null)
    {
        $cajaChica=Encargo::findOne($id);
        $cajaChica->Documento='';
        $cajaChica->Situacion=1;
        $cajaChica->update();
       
        $arr = array('Success' => true);
        echo json_encode($arr);
    }

}
