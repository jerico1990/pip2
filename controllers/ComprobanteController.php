<?php

namespace app\controllers;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

use app\models\Usuario;
use app\models\ComprobantePago;
use app\models\Investigador;
use app\models\InformacionGeneral;

use yii\web\UploadedFile;

class ComprobanteController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $this->layout='estandar';
    	$usuario=Usuario::findOne(\Yii::$app->user->id);

        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        if(!empty($investigador)){
	        $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
	        $CodigoProyecto=$informacionGeneral->Codigo;
        }else{
        	$CodigoProyecto = '';
        }

        $disable = 'false';
    	if($CodigoProyecto)
        {
	        $disable = 'true';
        }
        else{
	        $disable = 'false';
        }
        return $this->render('index',['disable'=>$disable]);
    }


    public function actionListaComprobante($CodigoProyecto=null)
    {

        $usuario=Usuario::findOne(\Yii::$app->user->id);
        // $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        if(!empty($investigador)){
	        $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
	        $CodigoProyecto=$informacionGeneral->Codigo;
        }else{
        	$CodigoProyecto = '';
        }
        $disable = 'false';
    	if($CodigoProyecto)
        {
	        $resultados = (new \yii\db\Query())
	            ->select('ComprobantePago.*')
	            ->from('ComprobantePago')
	            ->where(['Estado'=>'1','CodigoProyecto'=>$CodigoProyecto])
	            ->all();
	        $disable = 'true';
        }
        else{
            $resultados = (new \yii\db\Query())
	            ->select('ComprobantePago.*')
	            ->from('ComprobantePago')
	            ->where(['Estado'=>'1'])
	            ->all();
	        $disable = 'false';
        }
        
        $nro=0;
        foreach($resultados as $result)
        {
            $nro++;
            echo "<tr>";
            echo "<td> " . $nro . "</td>";
            echo "<td> " . $result["CodigoProyecto"] . "</td>";
            if(!empty($result["Archivo"])){
            	echo "<td><a class='btn btn-default' target='_blank' href='".\Yii::$app->request->BaseUrl."/comprobantex/" . $result["Archivo"] . "'><span class=' fa fa-cloud-download'></span> Descargar</a></td>";
            }else{
            	echo "<td>-</td>";
            }
            echo "<td> " . $result["FechaRegistro"] . "</td>";
            if($disable == 'false'){
            	echo "<td><a href='#' class='btn btn-primary btn-edit-comprobante' data-id='".$result["ID"]."'><i class='fa fa-edit fa-lg'></i></a> <a href='#' class='btn btn-danger btn-remove' data-id='".$result["ID"]."'><i class='fa fa-remove fa-lg'></i></a></td>";
            }else{
            	echo "<td> " . $result["Descripcion"] . "</td>";
            }

            echo "</tr>";
        }
    }



    public function actionCrear()
    {
        $this->layout='vacio';
        $CodigoProyectoLista = Usuario::find()->where('Tipo=:Tipo',[':Tipo'=>1])->all();
		$pago=new ComprobantePago;
        if($pago->load(Yii::$app->request->post())){
            $pago->Estado=1;
            $pago->FechaRegistro=date('Ymd h:m:s');
            $ds = $pago->save();

            $pago->Archivo = UploadedFile::getInstance($pago, 'Archivo');
            
            if($pago->Archivo)
            {
                $pago->Archivo->saveAs('comprobantex/' . $pago->ID . '.' . $pago->Archivo->extension);
                $pago->Archivo=$pago->ID . '.' . $pago->Archivo->extension;
            }
            $pago->update();
            \Yii::$app->getSession()->setFlash('Message', 'Se registro correctamente');
            return $this->redirect(['/comprobante']);
        }
        return $this->render('_form',['CodigoProyectoLista'=>$CodigoProyectoLista]);
    }


    public function actionActualizar($ID=null)
    {
        $this->layout='vacio';
        $pago=ComprobantePago::find()
                ->where('ComprobantePago.ID=:ID',[':ID'=>$ID])
                ->one();
        $CodigoProyectoLista = Usuario::find()->where('Tipo=:Tipo',[':Tipo'=>1])->all();
        
        if($pago->load(Yii::$app->request->post())){
            $pago->save();
            $pago->Archivo = UploadedFile::getInstance($pago, 'Archivo');
            
            if($pago->Archivo)
            {
                $pago->Archivo->saveAs('comprobantex/' . $pago->ID . '.' . $pago->Archivo->extension);
                $pago->Archivo=$pago->ID . '.' . $pago->Archivo->extension;
            }
            $pago->update();
            \Yii::$app->getSession()->setFlash('Message', 'Se ha actualizado correctamente');
            return $this->redirect(['/comprobante']);
        }
        return $this->render('_form_actualizar',['pago'=>$pago,'ID'=>$ID,'CodigoProyectoLista'=>$CodigoProyectoLista]);
    }


    public function actionEliminar($id=null)
    {
        $pago=ComprobantePago::findOne($id);
        $pago->Estado=0;
        $pago->update();
        \Yii::$app->getSession()->setFlash('Message', 'Se ha eliminado correctamente');
        $arr = array('Success' => true);
        echo json_encode($arr);
    }

}
