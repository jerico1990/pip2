<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\InformacionGeneral;
use app\models\ProgramaTransversal;
use app\models\DireccionLinea;
use app\models\TipoInvestigacion;
use app\models\UnidadEjecutora;
use app\models\UnidadOperativa;
use app\models\Investigador;
use app\models\EntidadParticipante;
use app\models\TipoInstitucion;
use app\models\RepresentanteLegal;
use app\models\Persona;
use app\models\Usuario;
use app\models\Requerimiento;
use app\models\Padron;

use app\models\Situacion;
use app\models\Rendicion;
use app\models\Viatico;
use app\models\RendicionViatico;
use app\models\DetalleViatico;
use app\models\DetalleRequerimiento;
use app\models\DetalleRendicionViatico;
use app\models\TipoDocumento;
use app\models\GastosGenerales;
use app\models\Proyecto;
use app\models\Poa;
use app\models\Componente;
use app\models\Actividad;
use app\models\ActRubroElegible;
use app\models\AreSubCategoria;
use app\models\TipoGasto;
use app\models\RubroElegible;

use app\models\DetalleGastosGenerales;
use app\models\ObservacionGeneral;


use PhpOffice\PhpWord\PhpWord;
use yii\web\UploadedFile;

class GastosGeneralesController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $CodigoProyecto=$informacionGeneral->Codigo;

        $this->layout='estandar';
        $db = Yii::$app->db;
        $resultados=$db->createCommand("
            SELECT  
            isnull(sum(DetalleGastosGenerales.MetaFisica*DetalleGastosGenerales.CostoUnitario), 0) AS tot
            FROM GastosGenerales
            INNER JOIN TipoDocumento ON GastosGenerales.ClaseDocumentoGasto = TipoDocumento.ID 
            LEFT JOIN TipoGasto ON GastosGenerales.TipoDocumentoGasto= TipoGasto.ID 
            LEFT JOIN DetalleGastosGenerales
            ON GastosGenerales.ID = DetalleGastosGenerales.GastosGeneralesID 
            WHERE GastosGenerales.CodigoProyecto ='".$CodigoProyecto."' AND (Situacion = 1 OR Situacion = 2 OR Situacion = 4)
            ")->queryOne();

        $gasto = GastosGenerales::find()->select('Situacion')->where('CodigoProyecto=:CodigoProyecto AND Situacion != 3',[':CodigoProyecto'=>$CodigoProyecto])->one();
        // echo '<pre>';print_r($gasto); die;
        $observa = ObservacionGeneral::find()->where('CodigoProyecto=:CodigoProyecto AND Estado = 1',[':CodigoProyecto'=>$CodigoProyecto])->one();

        return $this->render('index',['CodigoProyecto'=>$CodigoProyecto,'total'=>$resultados,'gasto'=>$gasto,'observa'=>$observa]);
    }


    public function actionListaGastos($CodigoProyecto=null)
    {
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        
        $db = Yii::$app->db;
        $resultados=$db->createCommand("
            SELECT GastosGenerales.Situacion,GastosGenerales.ID,GastosGenerales.NumeroUnidadEjecutora,GastosGenerales.TipoDocumentoGasto,GastosGenerales.ClaseDocumentoGasto,isnull(TipoGasto.Nombre,'Gastos Bancarios') Tipo ,TipoDocumento.Nombre Clase
            , isnull(sum(DetalleGastosGenerales.MetaFisica*DetalleGastosGenerales.CostoUnitario),0) tot
            FROM GastosGenerales
            INNER JOIN TipoDocumento ON GastosGenerales.ClaseDocumentoGasto = TipoDocumento.ID
            LEFT JOIN TipoGasto ON GastosGenerales.TipoDocumentoGasto= TipoGasto.ID
            LEFT JOIN DetalleGastosGenerales ON GastosGenerales.ID = DetalleGastosGenerales.GastosGeneralesID
            WHERE GastosGenerales.CodigoProyecto ='".$CodigoProyecto."' AND (Situacion = 1 OR Situacion = 2 OR Situacion = 4)
            GROUP BY GastosGenerales.NumeroUnidadEjecutora,GastosGenerales.TipoDocumentoGasto,
            GastosGenerales.ClaseDocumentoGasto,TipoDocumento.Nombre,TipoGasto.Nombre
            ,GastosGenerales.ID,GastosGenerales.Situacion
            ORDER BY 1 desc
            ")->queryAll();

        $nro=0;
        foreach($resultados as $result)
        {
            $nro++;
            echo "<tr>";
                echo "<td>CP N°-" . $result["NumeroUnidadEjecutora"] . "</td>";
                echo "<td>" . $result["Tipo"] ."</td>";
                echo "<td>" . $result["Clase"] . "</td>";
                echo "<td class='input-number'>" . number_format($result["tot"], 2, '.', '') . "</td>";
                if($result['Situacion'] == 1 || $result['Situacion'] == 4){
                    echo "<td> 
                        <a href='gastos-generales/eliminar?id=".$result['ID']."' data-id='".$result['ID']."' class='btn btn-danger btn-delete'><i class='fa fa-remove fa-lg'></i></a>
                        <a href='#' class='btn btn-primary btn-editar-requerimiento' data-codigo-proyecto='".$CodigoProyecto."' data-id='".$result['ID']."'><i class='fa fa-edit fa-lg'></i></a>
                     </td>";
                }else{
                    echo '<td></td>';
                }
            echo "</tr>";
        }
    }

    public function actionCrear($CodigoProyecto = null,$id= null){
        $this->layout='vacio';
        
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
        $Componente=Componente::find()->where('PoaID=:PoaID',[':PoaID'=>$poa->ID])->all();
        $TipoDocumento=TipoDocumento::find()->all();
        $TipoGasto=TipoGasto::find()->all();
        
        $RubroElegible=RubroElegible::find()->all();

        if(!$CodigoProyecto)
        { 
            $CodigoProyecto = $informacionGeneral->Codigo;
        }

        if(!is_null($id)){
            $model = GastosGenerales::find()->where('ID=:ID',[':ID'=>$id])->one();
        }else{
            $model = array();
        }
            // print_r($model); die();
        $xobs = GastosGenerales::find()->where('CodigoProyecto=:CodigoProyecto AND Situacion = 3',[':CodigoProyecto'=>$CodigoProyecto])->one();

        $integrantes=RecursosHumanosController::actionIntegrantes($CodigoProyecto,2);

        $codificacion=Yii::$app->db->createCommand("SELECT 
            convert(NVARCHAR(200),ActRubroElegible.RubroElegibleID)+'.'+convert(NVARCHAR(200),AreSubCategoria.Codificacion) Codificacion2
            ,AreSubCategoria.Codificacion
            FROM AreSubCategoria
            INNER JOIN ActRubroElegible ON ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID
            INNER JOIN RubroElegible ON RubroElegible.ID=ActRubroElegible.RubroElegibleID
            INNER JOIN Actividad ON Actividad.ID=ActRubroElegible.ActividadID
            INNER JOIN Componente ON Componente.ID=Actividad.ComponenteID
            INNER JOIN Poa ON Componente.PoaID = Poa.ID
            INNER JOIN Proyecto ON Proyecto.ID= Poa.ProyectoID
            INNER JOIN Investigador ON Investigador.ID= Proyecto.InvestigadorID
            INNER JOIN Usuario ON Usuario.ID = Investigador.UsuarioID
            WHERE Usuario.username= '".$CodigoProyecto."'
            GROUP BY RubroElegible.ID,ActRubroElegible.RubroElegibleID,AreSubCategoria.Codificacion
            ORDER BY RubroElegible.ID ASC,Codificacion ASC")->queryAll();;

        return $this->render('_formulario_crear',['CodigoProyecto'=>$CodigoProyecto,'Componente'=>$Componente,'TipoGasto'=>$TipoGasto,'TipoDocumento'=>$TipoDocumento,'model'=>$model,'RubroElegible'=>$RubroElegible,'codificacion'=>$codificacion,'integrantes'=>$integrantes,'obs'=>count($xobs)]);
    }

    public function actionCrearGasto($CodigoProyecto=null){
        $this->layout='vacio';
        $detalleGasto=new GastosGenerales;
        echo '<pre>';
        // print_r(Yii::$app->request->post());die();

        if( $detalleGasto->load( Yii::$app->request->post() ) ){
            $detalleGasto->Situacion=1;
            $detalleGasto->Estado=1;
            $detalleGasto->CodigoMatriz= $detalleGasto->CodigoMatriz;
            $detalleGasto->FechaRegistro=date('Ymd');
            $detalleGasto->save();
            // print_r($detalleGasto);
            // die();
            if(!$detalleGasto->Codigos)
            {
                $countCodigos=0;
            }
            else
            {
                $countCodigos=count(($detalleGasto->Codigos));
            }
            
            for($i=0;$i<$countCodigos;$i++)
            {
                if(isset($detalleGasto->Codigos[$i]))
                {
                    $detallex=new DetalleGastosGenerales;
                    $detallex->GastosGeneralesID = $detalleGasto->ID;
                    $detallex->ObjetivoActividad = ($detalleGasto->ObjetivoActividad[$i]);
                    $detallex->CodigoMatriz = ($detalleGasto->CodigoMatriz2[$i]);
                    $detallex->Mes = $detalleGasto->Mes[$i];
                    $detallex->MetaFisica = str_replace(',','', $detalleGasto->MetaFisica[$i]);
                    $detallex->CostoUnitario = str_replace(',','', $detalleGasto->CostoUnitario[$i]);
                    $detallex->Estado  = 1;  
                    $detallex->FechaRegistro=date('Ymd');
                    $detallex->save();
                    // print_r($detallex); 
                    // $detallex->ID; die();
                }
            }



            return $this->redirect(['/gastos-generales']);
        }
    }

    public function actionEditarGasto($CodigoProyecto=null,$id=null){
        $this->layout='vacio';
        $detalleGasto=GastosGenerales::findOne($id);
        if( $detalleGasto->load(Yii::$app->request->post()) ){
            // echo '<pre>';
            // print_r(Yii::$app->request->post()); die;
            $detalleGasto->update();

            // echo $detalleGasto->ID;
            // $GastoDetalle = DetalleGastosGenerales::findOne($id);
            $GastoDetalle=DetalleGastosGenerales::find()->where('GastosGeneralesID=:GastosGeneralesID',[':GastosGeneralesID'=>$id])->one();
            // print_r($GastoDetalle);
            // die();
            if(!empty($GastoDetalle)){
                DetalleGastosGenerales::deleteAll(['GastosGeneralesID' => $id]);
            }

            if(!$detalleGasto->Codigos)
            {
                $countCodigos=0;
            }
            else
            {
                $countCodigos=count(($detalleGasto->Codigos));
            }
            
            // print_r($countCodigos);
            // die();
            for($i=0;$i<$countCodigos;$i++)
            {
                if(isset($detalleGasto->Codigos[$i]))
                {
                    $detallex=new DetalleGastosGenerales;
                    $detallex->GastosGeneralesID = $id;
                    $detallex->ObjetivoActividad = ($detalleGasto->ObjetivoActividad[$i]);
                    $detallex->Mes = $detalleGasto->Mes[$i];
                    $detallex->MetaFisica = str_replace(',','', $detalleGasto->MetaFisica[$i]);
                    $detallex->CostoUnitario = str_replace(',','', $detalleGasto->CostoUnitario[$i]);
                    $detallex->CodigoMatriz = ($detalleGasto->CodigoMatriz2[$i]);
                    // $detallex->MetaFisica = $detalleGasto->MetaFisica[$i];
                    // $detallex->CostoUnitario = $detalleGasto->CostoUnitario[$i];
                    $detallex->Estado  = 1;  
                    $detallex->FechaRegistro=date('Ymd');
                    $detallex->save();
                    // print_r($detallex); 
                    // $detallex->ID; 
                    // die();
                }
            }
            return $this->redirect(['/gastos-generales']);
        }
    }

    public function actionActividades()
    {
        if(isset($_POST['ComponenteID']) && trim($_POST['ComponenteID'])!='')
        {
            $ComponenteID=$_POST['ComponenteID'];
            $countActividades=Actividad::find()->select('ID,Nombre')->where('ComponenteID=:ComponenteID',[':ComponenteID'=>$ComponenteID])->groupBy('ID,Nombre')->count();
            $actividades=Actividad::find()->select('ID,Nombre,Correlativo')->where('ComponenteID=:ComponenteID',[':ComponenteID'=>$ComponenteID])->groupBy('ID,Nombre,Correlativo')->orderBy('Correlativo asc')->all();
            if($countActividades>0){
                echo "<option value>Seleccionar</option>";
                foreach($actividades as $actividad){
                    echo "<option value='".$actividad->ID."' data-correlativo=".$actividad->Correlativo.">".$actividad->Correlativo." ".$actividad->Nombre."</option>";
                }
            }
            else{
                echo "<option value>Seleccionar</option>";
            }
        }
    }
    
    public function actionRecursos()
    {
        if(isset($_POST['ActividadID']) && trim($_POST['ActividadID'])!='')
        {
            $ActividadID=$_POST['ActividadID'];
            
            $countRecursos=AreSubCategoria::find()->select('AreSubCategoria.ID,AreSubCategoria.Nombre')
                ->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')
                ->where('ActRubroElegible.ActividadID=:ActividadID',[':ActividadID'=>$ActividadID])
                ->groupBy('AreSubCategoria.ID,AreSubCategoria.Nombre')
                ->count();
            
            $recursos = AreSubCategoria::find()->select('ActRubroElegible.RubroElegibleID,AreSubCategoria.ID,AreSubCategoria.Nombre,AreSubCategoria.Especifica,AreSubCategoria.Codificacion')
                ->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')->where('ActRubroElegible.ActividadID=:ActividadID',[':ActividadID'=>$ActividadID])
                ->groupBy('AreSubCategoria.ID,AreSubCategoria.Nombre,AreSubCategoria.Especifica,AreSubCategoria.Correlativo,ActRubroElegible.RubroElegibleID,AreSubCategoria.Codificacion')
                ->orderBy('AreSubCategoria.Correlativo')
                ->all();

            if($countRecursos>0){
                echo "<option value>Seleccionar</option>";
                foreach($recursos as $recurso){
                    echo "<option value='".$recurso->ID."' data-matriz=".$recurso->RubroElegibleID.".".$recurso->Codificacion.">".$recurso->Nombre.' / '.$recurso->Especifica."</option>";
                }
            }
            else{
                echo "<option value>Seleccionar</option>";
            }
        }
    }

    public function actionRecursosRubro($RubroElegible = null, $codigoProyecto = null){

        $RubroElegible  = $_POST['RubroElegible'];
        $codigoProyecto = $_POST['CodigoProyecto'];

        $resultados=Yii::$app->db->createCommand("
            SELECT ActRubroElegible.RubroElegibleID,AreSubCategoria.Codificacion,AreSubCategoria.Nombre,AreSubCategoria.Especifica
            FROM RubroElegible 
            INNER JOIN ActRubroElegible 
            ON RubroElegible.ID = ActRubroElegible.RubroElegibleID
            INNER JOIN AreSubCategoria 
            ON AreSubCategoria.ActRubroElegibleID=ActRubroElegible.ID
            INNER JOIN Actividad ON ActRubroElegible.ActividadID = Actividad.ID
            INNER JOIN Componente ON Actividad.ComponenteID = Componente.ID
            INNER JOIN Poa ON Poa.ID = Componente.PoaID
            INNER JOIN Proyecto ON Proyecto.ID = Poa.ProyectoID
            INNER JOIN Investigador ON Investigador.ID = Proyecto.InvestigadorID
            INNER JOIN InformacionGeneral ON Investigador.ID = InformacionGeneral.InvestigadorID
            WHERE RubroElegible.ID = ".$RubroElegible." AND  Actividad.Estado = 1 AND Componente.Estado = 1 AND
            InformacionGeneral.Codigo = '".$codigoProyecto."' 
            GROUP BY ActRubroElegible.RubroElegibleID,AreSubCategoria.Codificacion,AreSubCategoria.Nombre,AreSubCategoria.Especifica")->queryAll();

            if( count($resultados)>0){
                echo "<option value>Seleccionar</option>";
                foreach($resultados as $res){
                    echo "<option value='".$res['Codificacion']."'>".$res['Codificacion'].'. '.$res['Nombre'].' / '.$res['Especifica']."</option>";
                }
            }
            else{
                echo "<option value>Seleccionar</option>";
            }
    }

    public function actionRecursosCodifica($Codificacion=null, $CodigoProyecto = null,$rubro = null){
        // print_r($_POST);die();
        // $RubroElegible  = $_POST['RubroElegible'];
        // $Codificacion = $_POST['Codificacion'];
        // $codigoProyecto = $_POST['CodigoProyecto'];
        $this->layout='vacio';
        $resultados=Yii::$app->db->createCommand("SELECT AreSubCategoria.Especifica,AreSubCategoria.Detalle,AreSubCategoria.Nombre,
            convert(NVARCHAR(200),ActRubroElegible.RubroElegibleID)+'.'+convert(NVARCHAR(200),AreSubCategoria.Codificacion) Codificacion2,
            AreSubCategoria.Codificacion,
            sum(AreSubCategoria.MetaFisica) Cantidad,
            AreSubCategoria.CostoUnitario,
            sum(AreSubCategoria.MetaFisica)*AreSubCategoria.CostoUnitario AS Total
            FROM AreSubCategoria
            INNER JOIN ActRubroElegible ON ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID
            INNER JOIN RubroElegible ON RubroElegible.ID=ActRubroElegible.RubroElegibleID
            INNER JOIN Actividad ON Actividad.ID=ActRubroElegible.ActividadID
            INNER JOIN Componente ON Componente.ID=Actividad.ComponenteID
            INNER JOIN Poa ON Componente.PoaID = Poa.ID
            INNER JOIN Proyecto ON Proyecto.ID= Poa.ProyectoID
            INNER JOIN Investigador ON Investigador.ID= Proyecto.InvestigadorID
            INNER JOIN Usuario ON Usuario.ID = Investigador.UsuarioID
            WHERE Usuario.username = '".$CodigoProyecto."'
            AND RubroElegible.ID = ".$rubro."
            AND AreSubCategoria.Codificacion =".$Codificacion." GROUP BY AreSubCategoria.Especifica,AreSubCategoria.Detalle,RubroElegible.ID,ActRubroElegible.RubroElegibleID,AreSubCategoria.Codificacion,AreSubCategoria.UnidadMedida,AreSubCategoria.CostoUnitario,AreSubCategoria.Nombre ORDER BY RubroElegible.ID ASC,Codificacion ASC")->queryOne();


        $detalle = Yii::$app->db->createCommand("SELECT [CronogramaAreSubCategoria].[Mes],
                InformacionGeneral.Codigo,
                Componente.Correlativo Componente,
                Actividad.Correlativo Actividad,
                CronogramaAreSubCategoria.MetaFisica,
                CronogramaAreSubCategoria.CostoUnitario
            FROM [CronogramaAreSubCategoria] 
            INNER JOIN [AreSubCategoria] ON AreSubCategoria.ID=CronogramaAreSubCategoria.AreSubCategoriaID 
            INNER JOIN [ActRubroElegible] ON ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID 
            INNER JOIN [Actividad] ON Actividad.ID=ActRubroElegible.ActividadID 
            INNER JOIN [Componente] ON Componente.ID=Actividad.ComponenteID 
            INNER JOIN [Poa] ON Poa.ID=Componente.PoaID 
            INNER JOIN [Proyecto] ON Proyecto.ID=Poa.ProyectoID 
            INNER JOIN [InformacionGeneral] ON InformacionGeneral.InvestigadorID=Proyecto.InvestigadorID 
            WHERE CronogramaAreSubCategoria.MetaFisica<>0
            AND InformacionGeneral.Codigo= '".$CodigoProyecto."'
            AND ActRubroElegible.RubroElegibleID = ".$rubro."
            AND AreSubCategoria.Codificacion =".$Codificacion." GROUP BY [CronogramaAreSubCategoria].[Mes],InformacionGeneral.Codigo,CronogramaAreSubCategoria.MetaFisica,Actividad.Correlativo,Componente.Correlativo,CronogramaAreSubCategoria.CostoUnitario 
            ORDER BY [CronogramaAreSubCategoria].[Mes],  Codigo")->queryAll();

        return $this->render('_form_objetivos',['CodigoProyecto'=>$CodigoProyecto,'resultados'=>$resultados,'detalle'=>$detalle,'rubro'=>$rubro,'codifica'=>$Codificacion]);
    }


    public function actionEliminar($id=null)
    {
        GastosGenerales::findOne($id)->delete();
        
        // DetalleGastosGenerales::find($id);
        $gastos=DetalleGastosGenerales::find()->where('GastosGeneralesID=:GastosGeneralesID',[':GastosGeneralesID'=>$id])->one();
        if(!empty($gastos)){
            DetalleGastosGenerales::deleteAll(['GastosGeneralesID' => $id]);
        }

        $arr = array('Success' => true);
        echo json_encode($arr);
    }


    public function actionExcel($CodigoProyecto=null)
    {
        if(!is_null($CodigoProyecto)){
            $resultados = (new \yii\db\Query())
                ->select('*')
                ->from('GastosGenerales')
                ->innerJoin('DetalleGastosGenerales','GastosGenerales.ID = DetalleGastosGenerales.GastosGeneralesID')
                ->where(['GastosGenerales.CodigoProyecto'=>$CodigoProyecto])
                ->all();
        }else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $resultados = (new \yii\db\Query())
                ->select('GastosGenerales.*,DetalleGastosGenerales.*')
                ->from('InformacionGeneral')
                ->innerJoin('Investigador','InformacionGeneral.InvestigadorID=Investigador.ID')
                ->innerJoin('Usuario','Usuario.ID=Investigador.UsuarioID')
                ->innerJoin('Proyecto','Proyecto.InvestigadorID=Investigador.ID')
                ->innerJoin('Poa','Poa.ProyectoID=Proyecto.ID')
                ->innerJoin('UsuarioProyecto','UsuarioProyecto.CodigoProyecto=InformacionGeneral.Codigo')
                ->innerJoin('GastosGenerales','GastosGenerales.CodigoProyecto = Usuario.username')
                ->innerJoin('DetalleGastosGenerales','GastosGenerales.ID = DetalleGastosGenerales.GastosGeneralesID')
                ->where('UsuarioProyecto.UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->ID])
                ->distinct()
                ->all();
        }
        return $this->render('excel',['resultados'=>$resultados,'CodigoProyecto'=>$CodigoProyecto]);
    }

    // -- CARGA EXCEL
    public function actionCargaTotal(){
        $this->layout='estandar';

        $model = new GastosGenerales;
        //$subC = new AreSubCategoria();
        // print_r(Yii::$app->request->post());die;
        if ( Yii::$app->request->post() ) {
            
            // Preguntar antes si el rubro elegible existe para esa actividad 
                // var_dump($_FILES);die;
                    //validate whether uploaded file is a csv file
                    $csvMimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
                    if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
                        //var_dump($_FILES['ActRubroElegible']);die;
                        if(is_uploaded_file($_FILES['file']['tmp_name'])){
                            
                            //open uploaded csv file with read only mode
                            $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
                            
                            //skip first line
                            fgetcsv($csvFile);
                            
                            //parse data from csv file line by line
                            while(($line = fgetcsv($csvFile)) !== FALSE){
                                //check whether member already exists in database with same email
                                // var_dump(utf8_encode($line[2]));die;
                                
                                $detalle=new GastosGenerales;
                                $detalle->PasoCritico = utf8_encode($line[1]);
                                $detalle->CodigoProyecto = utf8_encode($line[2]);
                                $detalle->FechaUnidadEjecutora = !empty($line[3])?Yii::$app->tools->dateFormat(utf8_encode($line[3]),'Y-m-d'):'';
                                $detalle->NumeroUnidadEjecutora =utf8_encode($line[4]);
                                $detalle->FechaDocumentoGasto =!empty($line[5])?Yii::$app->tools->dateFormat(utf8_encode($line[5]),'Y-m-d'):'';
                                $detalle->ClaseDocumentoGasto =utf8_encode($line[7]);
                                $detalle->NumeroSerieDocumentoGasto =utf8_encode($line[8]);
                                $detalle->NroDocumentoGasto =utf8_encode($line[9]);
                                $detalle->RucProveedor =utf8_encode($line[10]);
                                $detalle->RazonSocialProveedor =utf8_encode($line[11]);
                                $detalle->ConceptoProveedor =utf8_encode($line[12]);
                                $detalle->Importe =utf8_encode($line[13]);
                                $detalle->Clasificacion =utf8_encode($line[14]);
                                
                                $detalle->save();
                                $idz = $detalle->ID;
                            }
                            
                            //close opened csv file
                            fclose($csvFile);
                           
                            return $this->redirect(['/gastos-generales']);
                        }
                    }
            
        }

        return $this->render('carga');
    }

    public function actionMatrizCrear($CodigoProyecto = null,$id= null){
        $this->layout='vacio';
        
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
        $Componente=Componente::find()->where('PoaID=:PoaID',[':PoaID'=>$poa->ID])->all();
        $TipoDocumento=TipoDocumento::find()->all();
        $TipoGasto=TipoGasto::find()->all();
        
        $RubroElegible=RubroElegible::find()->all();

        if(!$CodigoProyecto)
        { 
            $CodigoProyecto = $informacionGeneral->Codigo;
        }

        if(!is_null($id)){
            $model = GastosGenerales::find()->where('ID=:ID',[':ID'=>$id])->one();
        }else{
            $model = array();
        }
            // print_r($model); die();
        $integrantes=RecursosHumanosController::actionIntegrantes($CodigoProyecto,2);

        $codificacion=Yii::$app->db->createCommand("SELECT 
            convert(NVARCHAR(200),ActRubroElegible.RubroElegibleID)+'.'+convert(NVARCHAR(200),AreSubCategoria.Codificacion) Codificacion2
            ,AreSubCategoria.Codificacion
            FROM AreSubCategoria
            INNER JOIN ActRubroElegible ON ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID
            INNER JOIN RubroElegible ON RubroElegible.ID=ActRubroElegible.RubroElegibleID
            INNER JOIN Actividad ON Actividad.ID=ActRubroElegible.ActividadID
            INNER JOIN Componente ON Componente.ID=Actividad.ComponenteID
            INNER JOIN Poa ON Componente.PoaID = Poa.ID
            INNER JOIN Proyecto ON Proyecto.ID= Poa.ProyectoID
            INNER JOIN Investigador ON Investigador.ID= Proyecto.InvestigadorID
            INNER JOIN Usuario ON Usuario.ID = Investigador.UsuarioID
            WHERE Usuario.username= '".$CodigoProyecto."'
            GROUP BY RubroElegible.ID,ActRubroElegible.RubroElegibleID,AreSubCategoria.Codificacion
            ORDER BY RubroElegible.ID ASC,Codificacion ASC")->queryAll();;

        return $this->render('_form_recurso',['CodigoProyecto'=>$CodigoProyecto,'Componente'=>$Componente,'TipoGasto'=>$TipoGasto,'TipoDocumento'=>$TipoDocumento,'model'=>$model,'RubroElegible'=>$RubroElegible,'codificacion'=>$codificacion,'integrantes'=>$integrantes]);
    }


    public function actionEnviarGastos($CodigoProyecto = null){
        $this->layout='vacio';
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $qqer = "UPDATE GastosGenerales SET Situacion = 2 WHERE CodigoProyecto='".$informacionGeneral->Codigo."' AND Situacion = 1";
        $queryy = Yii::$app->db->createCommand($qqer);
        $ss = $queryy->execute();
        // echo $ss;
        // die;
        //--------------------------------------------------
        if($ss != 0){
            $CodigoProyecto=$informacionGeneral->Codigo;
            $db = Yii::$app->db;
            $resultados=$db->createCommand("
                SELECT GastosGenerales.Situacion,GastosGenerales.ID,GastosGenerales.NumeroUnidadEjecutora,GastosGenerales.TipoDocumentoGasto,GastosGenerales.ClaseDocumentoGasto,isnull(TipoGasto.Nombre,'Gastos Bancarios') Tipo ,TipoDocumento.Nombre Clase
                , isnull(sum(DetalleGastosGenerales.MetaFisica*DetalleGastosGenerales.CostoUnitario),0) tot
                FROM GastosGenerales
                INNER JOIN TipoDocumento ON GastosGenerales.ClaseDocumentoGasto = TipoDocumento.ID
                LEFT JOIN TipoGasto ON GastosGenerales.TipoDocumentoGasto= TipoGasto.ID
                LEFT JOIN DetalleGastosGenerales ON GastosGenerales.ID = DetalleGastosGenerales.GastosGeneralesID
                WHERE GastosGenerales.CodigoProyecto ='".$CodigoProyecto."' AND (Situacion = 1 OR Situacion = 2 OR Situacion = 4)
                GROUP BY GastosGenerales.NumeroUnidadEjecutora,GastosGenerales.TipoDocumentoGasto,
                GastosGenerales.ClaseDocumentoGasto,TipoDocumento.Nombre,TipoGasto.Nombre
                ,GastosGenerales.ID,GastosGenerales.Situacion
                ORDER BY 1 desc
            ")->queryAll();

            $nro=0;
            foreach($resultados as $result)
            {
                $nro++;
                echo "<tr>";
                    echo "<td>CP N°-" . $result["NumeroUnidadEjecutora"] . "</td>";
                    echo "<td>" . $result["Tipo"] ."</td>";
                    echo "<td>" . $result["Clase"] . "</td>";
                    echo "<td class='input-number'>" . number_format($result["tot"], 2, '.', '') . "</td>";
                    echo '<td></td>';
                echo "</tr>";
            }
        }
        //--------------------------------------------------
        // echo json_encode(array('Success'=>true));
    }

    public function actionEnviarGastosObs($CodigoProyecto = null){
        $this->layout='vacio';
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $qqer = "UPDATE GastosGenerales SET Situacion = 2 WHERE CodigoProyecto='".$informacionGeneral->Codigo."' AND Situacion = 4";
        $queryy = Yii::$app->db->createCommand($qqer);
        $ss = $queryy->execute();
        
        $obs = $_POST['obs'];
        $qqer2 = "UPDATE ObservacionGeneral SET RespuestaObservacion = '".$obs."' WHERE CodigoProyecto='".$informacionGeneral->Codigo."' AND Estado = 1 ";
        $queryy2 = Yii::$app->db->createCommand($qqer2);
        $queryy2->execute();

        // echo $ss;
        // die;
        //--------------------------------------------------
        if($ss != 0){
            $CodigoProyecto=$informacionGeneral->Codigo;
            $db = Yii::$app->db;
            $resultados=$db->createCommand("
                SELECT GastosGenerales.Situacion,GastosGenerales.ID,GastosGenerales.NumeroUnidadEjecutora,GastosGenerales.TipoDocumentoGasto,GastosGenerales.ClaseDocumentoGasto,isnull(TipoGasto.Nombre,'Gastos Bancarios') Tipo ,TipoDocumento.Nombre Clase
                , isnull(sum(DetalleGastosGenerales.MetaFisica*DetalleGastosGenerales.CostoUnitario),0) tot
                FROM GastosGenerales
                INNER JOIN TipoDocumento ON GastosGenerales.ClaseDocumentoGasto = TipoDocumento.ID
                LEFT JOIN TipoGasto ON GastosGenerales.TipoDocumentoGasto= TipoGasto.ID
                LEFT JOIN DetalleGastosGenerales ON GastosGenerales.ID = DetalleGastosGenerales.GastosGeneralesID
                WHERE GastosGenerales.CodigoProyecto ='".$CodigoProyecto."'
                GROUP BY GastosGenerales.NumeroUnidadEjecutora,GastosGenerales.TipoDocumentoGasto,
                GastosGenerales.ClaseDocumentoGasto,TipoDocumento.Nombre,TipoGasto.Nombre
                ,GastosGenerales.ID,GastosGenerales.Situacion
                ORDER BY 1 desc
            ")->queryAll();

            $nro=0;
            foreach($resultados as $result)
            {
                $nro++;
                echo "<tr>";
                    echo "<td>CP N°-" . $result["NumeroUnidadEjecutora"] . "</td>";
                    echo "<td>" . $result["Tipo"] ."</td>";
                    echo "<td>" . $result["Clase"] . "</td>";
                    echo "<td class='input-number'>" . number_format($result["tot"], 2, '.', '') . "</td>";
                    echo '<td></td>';
                echo "</tr>";
            }
        }
        //--------------------------------------------------
        // echo json_encode(array('Success'=>true));
    }

    public function actionListaAprobacion($CodigoProyecto = null){
        $this->layout='vacio';
        $db = Yii::$app->db;

        $gastos = GastosGenerales::find()->where('CodigoProyecto=:CodigoProyecto and Situacion!=3',[':CodigoProyecto'=>$CodigoProyecto])->one();
        $observacion = '';
        if(!empty($gastos)){
            if($gastos->Situacion == 4 || $gastos->Situacion == 2){
                $observacion = ObservacionGeneral::find()->where('CodigoProyecto=:CodigoProyecto AND Estado = 1',[':CodigoProyecto'=>$CodigoProyecto])->one();
            }
        }
        // $situacion = '';
        // print_r($situacion); die();

        $resultados=$db->createCommand("
                SELECT GastosGenerales.Situacion,GastosGenerales.ID,GastosGenerales.NumeroUnidadEjecutora,isnull(TipoGasto.Nombre,'Gastos Bancarios') Tipo ,TipoDocumento.Nombre Clase
                , isnull(sum(DetalleGastosGenerales.MetaFisica*DetalleGastosGenerales.CostoUnitario),0) tot,GastosGenerales.CodigoMatriz
                FROM GastosGenerales
                INNER JOIN TipoDocumento ON GastosGenerales.ClaseDocumentoGasto = TipoDocumento.ID
                LEFT JOIN TipoGasto ON GastosGenerales.TipoDocumentoGasto= TipoGasto.ID
                LEFT JOIN DetalleGastosGenerales ON GastosGenerales.ID = DetalleGastosGenerales.GastosGeneralesID
                WHERE GastosGenerales.CodigoProyecto ='".$CodigoProyecto."' AND GastosGenerales.Situacion !=3
                GROUP BY GastosGenerales.NumeroUnidadEjecutora,TipoDocumento.Nombre,TipoGasto.Nombre
                ,GastosGenerales.ID,GastosGenerales.Situacion,GastosGenerales.CodigoMatriz
                ORDER BY 1 desc
            ")->queryAll();

        return $this->render('lista-aprobacion',['CodigoProyecto'=>$CodigoProyecto,'resultados'=>$resultados,'gastos'=>$gastos,'observacion' => $observacion]);
    }

}

