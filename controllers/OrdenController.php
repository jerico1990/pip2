<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\PasoCritico;
use app\models\Usuario;
use app\models\Persona;
use app\models\Investigador;
use app\models\Proyecto;
use app\models\InformacionGeneral;
use app\models\Poa;
use app\models\Componente;
use app\models\Actividad;
use app\models\CronogramaProyecto;
use app\models\ActRubroElegible;
use app\models\CronogramaComponente;
use app\models\CronogramaActividad;
use app\models\CronogramaActRubroElegible;
use app\models\RubroElegible;
use app\models\AreSubCategoria;
use app\models\CronogramaAreSubCategoria;
use app\models\MarcoLogicoActividad;
use app\models\AreSubCategoriaObservacion;
use app\models\Requerimiento;
use app\models\TerminoReferencia;
use app\models\Orden;
use app\models\DetalleOrden;
use app\models\DetalleCertificacionPresupuestal;
use app\models\UnidadOperativa;
use app\models\DetalleRequerimiento;
use app\models\CatalogoBienServicio;




use PhpOffice\PhpWord\PhpWord;
use yii\web\UploadedFile;


class OrdenController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($CodigoProyecto=NULL)
    {
        $this->layout='estandar';
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        return $this->render('index',['CodigoProyecto'=>$CodigoProyecto]);
    }
    
    public function actionListaOrdenes($CodigoProyecto=null)
    {
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        
        $resultados = (new \yii\db\Query())
            ->select('Orden.*')
            ->from('Orden')
            ->where(['Orden.CodigoProyecto'=>$CodigoProyecto])
            ->distinct()
            ->all();

        $nro=0;
        foreach($resultados as $result)
        {
            $nro++;
            echo "<tr>";
            echo "<td> " . $this->getEstado($result["Estado"]) . "</td>";
            echo "<td> " . $this->getTipoOrden($result["TipoOrden"]) . "</td>";
            
            if($result["TipoOrden"]==1)
            {
                echo "<td> O.S. N° " . str_pad($result["Correlativo"], 3, "0", STR_PAD_LEFT) ."-".$result["Annio"]. "</td>";
            }elseif($result["TipoOrden"]==2)
            {
                echo "<td> O.S. N° " . str_pad($result["Correlativo"], 3, "0", STR_PAD_LEFT) ."-".$result["Annio"]. "</td>";
            }
            elseif($result["TipoOrden"]==3)
            {
                echo "<td> O.C. N° " . str_pad($result["Correlativo"], 3, "0", STR_PAD_LEFT) ."-".$result["Annio"]. "</td>";
            }
            
            
            echo "<td>" . $result["FechaOrden"] . "</td>";
            echo "<td>" . $this->getTipoProceso($result["TipoProceso"]) . "</td>";
            echo "<td>" . $result["Monto"] . "</td>";
            if($result["Documento"])
            {
                echo "<td><a target='_blank' href='ordenCompra/" . $result["Documento"] . "'><span class='fa fa-cloud-download'></span></a></td>";
            }
            else
            {
                echo "<td><a target='_blank' href='orden/plantilla?ID=" . $result["ID"] . "'><span class='fa fa-cloud-download'></span></a> <a class='btn-upload' data-id='".$result["ID"]."' target='_blank' href='orden/plantilla?ID=" . $result["ID"] . "'><span class='fa fa-cloud-upload'></span></a></td>";   
            }
            
            //echo "<td></td>";
            echo "</tr>";
        }
    }
    
    public function actionListaOrdenesReportes($CodigoProyecto=null)
    {
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        
        $resultados = (new \yii\db\Query())
            ->select('Orden.*')
            ->from('Orden')
            ->where(['Orden.CodigoProyecto'=>$CodigoProyecto])
            ->distinct()
            ->all();

        $nro=0;
        foreach($resultados as $result)
        {
            $nro++;
            echo "<tr>";
            echo "<td> " . $this->getTipoOrden($result["TipoOrden"]) . "</td>";
            echo "<td> " . $result["SIAF"] . "</td>";
            if($result["TipoOrden"]==1)
            {
                echo "<td> O.S. N° " . str_pad($result["Correlativo"], 3, "0", STR_PAD_LEFT). "</td>";
            }
            elseif($result["TipoOrden"]==2)
            {
                echo "<td> O.S. N° " . str_pad($result["Correlativo"], 3, "0", STR_PAD_LEFT) . "</td>";
            }
            elseif($result["TipoOrden"]==3)
            {
                echo "<td> O.C. N° " . str_pad($result["Correlativo"], 3, "0", STR_PAD_LEFT) . "</td>";
            }
            
            
            echo "<td>" . $result["FechaOrden"] . "</td>";
            echo "<td>" . $this->getTipoProceso($result["TipoProceso"]) . "</td>";
            echo "<td>" . $result["Monto"] . "</td>";
            //echo "<td></td>";
            echo "</tr>";
        }
    }
    public function getTipoProceso($proceso)
    {
        $descripcion="";
        if($proceso==1)
        {
            $descripcion="Comparacion de Precios";
        }
        elseif($proceso==2)
        {
            $descripcion="Evaluacion de CVs";
        }
        elseif($proceso==3)
        {
            $descripcion="Contratacion Directa";
        }
        return $descripcion;
    }
    
    public function getTipoOrden($tipo)
    {
        $descripcion="";
        if($tipo==1)
        {
            $descripcion="Comparacion de Precios";
        }
        elseif($tipo==2)
        {
            $descripcion="Orden de Servicio Factura";
        }
        elseif($tipo==3)
        {
            $descripcion="Orden de Compra";
        }
        return $descripcion;
    }
    public function actionCrear($CodigoProyecto)
    {
        $this->layout='vacio';
        $informacionGeneral=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacionGeneral->InvestigadorID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
        $componentes=Componente::find()->where('PoaID=:PoaID',[':PoaID'=>$poa->ID])->all();
        $requerimiento=new Requerimiento;
        if($requerimiento->load(Yii::$app->request->post())){
            
            $requerimiento->Codigo=$CodigoProyecto;
            $requerimiento->Situacion=1;
            $requerimiento->Estado=1;
            $requerimiento->FechaRegistro=date('Ymd');
            $requerimiento->save();
            
            $requerimiento->archivo = UploadedFile::getInstance($requerimiento, 'archivo');
            
            if($requerimiento->archivo)
            {
                $requerimiento->archivo->saveAs('requerimientos/' . $requerimiento->ID . '.' . $requerimiento->archivo->extension);
                $requerimiento->Documento=$requerimiento->ID . '.' . $requerimiento->archivo->extension;
            }
            $requerimiento->update();
            return $this->redirect(['/requerimiento']);
        }
        
        return $this->render('_form',['CodigoProyecto'=>$CodigoProyecto,'componentes'=>$componentes]);
    }
    
    public function actionActividades()
    {
        if(isset($_POST['ComponenteID']) && trim($_POST['ComponenteID'])!='')
        {
            $ComponenteID=$_POST['ComponenteID'];
            $countActividades=Actividad::find()->select('ID,Nombre')->where('ComponenteID=:ComponenteID',[':ComponenteID'=>$ComponenteID])->groupBy('ID,Nombre')->count();
            $actividades=Actividad::find()->select('ID,Nombre,Correlativo')->where('ComponenteID=:ComponenteID',[':ComponenteID'=>$ComponenteID])->groupBy('ID,Nombre,Correlativo')->orderBy('Correlativo')->all();
            if($countActividades>0){
                echo "<option value>Seleccionar</option>";
                foreach($actividades as $actividad){
                    echo "<option value='".$actividad->ID."'>".$actividad->Nombre."</option>";
                }
            }
            else{
                echo "<option value>Seleccionar</option>";
            }
        }
    }
    
    public function actionRecursos()
    {
        if(isset($_POST['ActividadID']) && trim($_POST['ActividadID'])!='')
        {
            $ActividadID=$_POST['ActividadID'];
            
            $countRecursos=AreSubCategoria::find()->select('AreSubCategoria.ID,AreSubCategoria.Nombre')->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')->where('ActRubroElegible.ActividadID=:ActividadID',[':ActividadID'=>$ActividadID])->groupBy('AreSubCategoria.ID,AreSubCategoria.Nombre')->count();
            $recursos=AreSubCategoria::find()->select('AreSubCategoria.ID,AreSubCategoria.Nombre')->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')->where('ActRubroElegible.ActividadID=:ActividadID',[':ActividadID'=>$ActividadID])->groupBy('AreSubCategoria.ID,AreSubCategoria.Nombre,AreSubCategoria.Correlativo')->orderBy('AreSubCategoria.Correlativo')->all();
            if($countRecursos>0){
                echo "<option value>Seleccionar</option>";
                foreach($recursos as $recurso){
                    echo "<option value='".$recurso->ID."'>".$recurso->Nombre."</option>";
                }
            }
            else{
                echo "<option value>Seleccionar</option>";
            }
        }
    }
    
    public function getTipoServicio($tipoServicio=null)
    {
        if($tipoServicio==1)
        {
            return "Servicio";
        }
        else
        {
            return "compra";
        }
    }
    
    public function getSituacion($situacion=null)
    {
        if($situacion==1)
        {
            return "Aprobado";
        }
        else if($situacion==2)
        {
            return "Pendiente de aprobar";
        }
        else if($situacion==4)
        {
            return "Aprobado";
        }
    }
    public function getEstado($Estado)
    {
        if($Estado==1){
            return "Activo";
        }
        elseif($Estado==0)
        {
            return "Anulado";
        }
    }
    public function getUbicacion($archivo)
    {
        
    }

    public function actionCrearServicioFactura($CodigoProyecto = null){
        $this->layout='estandar';
        $informacionGeneral = InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])->one();
        $proyecto           = Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacionGeneral->InvestigadorID])->one();
        $poa                = Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
        $componentes        = Componente::find()->where('PoaID=:PoaID',[':PoaID'=>$poa->ID])->all();

        $tdr = Requerimiento::find()
            ->select('Requerimiento.Correlativo,TerminoReferencia.ID')
            // ->innerJoin('Requerimiento','Requerimiento.ID = TerminoReferencia.RequerimientoID')
            ->innerJoin('TerminoReferencia','TerminoReferencia.RequerimientoID=Requerimiento.ID')
            ->where('TerminoReferencia.Estado=:Estado',[':Estado'=>1])
            ->all();

        return $this->render('_form_servicio_factura',['CodigoProyecto' => $CodigoProyecto,'componentes'=>$componentes,'tdr'=>$tdr]);
    }



    public function actionTipoReferenciaOrdenServicio($id = null){
        $referencia = TerminoReferencia::find()->where('ID=:Codigo',[':Codigo'=>$id])->one();
        if(empty($referencia)){
            $aa = '';
        }else{
            $aa = $referencia->Referencia;
        }
        echo $aa;
    }
    public function CodigosProyectosExonerados($CodigoProyecto)
    {
        if($CodigoProyecto=='011_PI' || $CodigoProyecto=='017_PI' || $CodigoProyecto=='025_PI' || $CodigoProyecto=='040_PI' ||
           $CodigoProyecto=='042_PI' || $CodigoProyecto=='049_PI' || $CodigoProyecto=='038_PI' || $CodigoProyecto=='043_PTT' ||
           $CodigoProyecto=='058_PI' || $CodigoProyecto=='059_PI' || $CodigoProyecto=='060_PTT' || $CodigoProyecto=='132_PI' ||
           $CodigoProyecto=='039_PI' || $CodigoProyecto=='048_PI' || $CodigoProyecto=='087_PI')
        {
            return true;
        }
        return false;
    }
    
    public function actionCrearOrdenCompra($RequerimientoID=null,$CodigoProyecto=null,$DetallesRequerimientosIDs=[]){
        $this->layout='vacio';
        $tipo_Cambio=0.00;

        $Requerimiento=Requerimiento::findOne($RequerimientoID);
        //$detallesRequerimientos=DetalleRequerimiento::find()->where('Codificacion=:Codificacion and RequerimientoID=:RequerimientoID',[':RequerimientoID'=>$RequerimientoID,':Codificacion'=>$Codificacion])->all();
        $detallesRequerimientos=DetalleRequerimiento::find()->where('ID in ('.$DetallesRequerimientosIDs.')')->all();
        
        //$detal=DetalleRequerimiento::find()->where('Codificacion=:Codificacion and RequerimientoID=:RequerimientoID',[':RequerimientoID'=>$RequerimientoID,':Codificacion'=>$Codificacion])->one();
        $detal=DetalleRequerimiento::find()->select('sum(Cantidad*PrecioUnitario) Total')->where('ID in ('.$DetallesRequerimientosIDs.')')->one();
        $certificacion=DetalleCertificacionPresupuestal::find()
                    ->select('DetalleCertificacionPresupuestal.CodigoCertificacion')
                    ->innerJoin('CertificacionPresupuestal','DetalleCertificacionPresupuestal.CertificacionPresupuestalID=CertificacionPresupuestal.ID')
                    ->where('CertificacionPresupuestal.Situacion=1 and CertificacionPresupuestal.Estado=1 and DetalleCertificacionPresupuestal.Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])
                    ->one();
        $orden=new Orden;

        $catalogo = CatalogoBienServicio::find()->all();
        // print_r($catalogo); die();
        
        //$orden->Concepto=$detal->Descripcion;
        //$orden->Cantidad=$detal->Cantidad;
        if($orden->load(Yii::$app->request->post())){
            // echo '<pre>';
            // print_r(Yii::$app->request->post()); die();
            $orden->FechaOrden=date('Ymd h:m:s');
            $orden->UnidadEjecutoraNombres='PROGRAMA NACIONAL DE INNONACIÓN AGRARIA - PNIA';
            $orden->UnidadEjecutoraDireccion='AV. LA MOLINA N° 1981 LA MOLINA - LIMA';
            $orden->UnidadEjecutoraRUC='20563395746';
            $orden->CodigoProyecto=$CodigoProyecto;
            $orden->TipoOrden=3;
            $orden->Certificacion=$certificacion->CodigoCertificacion;
            $orden->Situacion=1;
            $orden->Correlativo=$this->CorrelativoOrden($orden->TipoOrden,$CodigoProyecto);
            $orden->Estado=1;
            $orden->TipoCambio=1;
            // $orden->TipoCambio=$GetOracleTipoCambios['VALO_SOLE_TIP'];//$tipo_Cambio;
            $orden->FechaRegistro=date('Ymd h:m:s');
            $orden->Annio=date('Y');
            // print_r($orden); die;
            $orden->save();
            foreach($detallesRequerimientos as $detalleRequerimiento){
                $detalle=DetalleRequerimiento::findOne($detalleRequerimiento->ID);
                $detalle->TipoDetalleOrdenID=$orden->ID;
                $detalle->Situacion=3;
                $detalle->update();
            }
            
            if(!$orden->Descripciones)
            {
                $countDescripciones=0;
            }
            else
            {
                $countDescripciones=count(array_filter($orden->Descripciones));
            }
            
            for($i=0;$i<$countDescripciones;$i++)
            {
                if(isset($orden->Descripciones[$i]))
                {
                    $detalle=new DetalleOrden;
                    $detalle->OrdenID=$orden->ID;
                    $detalle->Cantidad= str_replace(',','', $orden->Cantidades[$i]);

                    $detallex=CatalogoBienServicio::find()->where('Codigo = '.$orden->UnidadesMedidas[$i])->one();
                    // $detallex=CatalogoBienServicio::find()->where();
                    $detalle->codi_bser_cat=$orden->UnidadesMedidas[$i];
                    $detalle->UnidadMedida=$detallex->Descripcion;
                    
                    $detalle->Descripcion=$orden->Descripciones[$i];
                    $detalle->PrecioUnitario=str_replace(',','', $orden->PreciosUnitarios[$i]);
                    $detalle->Total=($orden->Cantidades[$i]*$orden->PreciosUnitarios[$i]);
                    $detalle->save();
                    $orden->Monto=$orden->Monto+($orden->Cantidades[$i]*$orden->PreciosUnitarios[$i]);
                }
            }

            if($this->CodigosProyectosExonerados($orden->CodigoProyecto) )
            {
                $orden->ValorVenta=$orden->Monto;
                $orden->IGV=0;                
            }
            else{
                $orden->ValorVenta=$orden->Monto/1.18;
                $orden->IGV=$orden->ValorVenta * 0.18;
            }
            // die();
            
            $orden->update();
            return $this->redirect(['requerimiento/generar','ID'=>$Requerimiento->ID]);
            /*return $this->redirect(['/orden'],302);
            exit(0);*/
        }
        return $this->render('_form_orden_compra',['DetallesRequerimientosIDs'=>$DetallesRequerimientosIDs,'CodigoProyecto'=>$CodigoProyecto,'RequerimientoID'=>$RequerimientoID,'Requerimiento'=>$Requerimiento,'PrecioTotal'=>$detal->Total,'orden'=>$orden,'catalogo'=>$catalogo]);
    }
    
    public function actionCrearOrdenServicioFactura($RequerimientoID=null,$CodigoProyecto=null,$DetallesRequerimientosIDs=[]){
        $this->layout='vacio';
        $tipo_Cambio=0.00;
            // $GetOracleTipoCambios = \Yii::$app->db->createCommand("Select * from openquery(ORACLE,'SELECT FECH_CAMB_TIP,VALO_SOLE_TIP FROM TIPO_CAMBIO WHERE TO_CHAR(FECH_CAMB_TIP,''DD-MM-YYYY'')=TO_CHAR(SYSDATE,''DD-MM-YYYY'')')")->queryOne();
            /*
            $db= '(DESCRIPTION=( ADDRESS_LIST= (ADDRESS= (PROTOCOL=TCP) (HOST=172.168.3.12) (PORT=1521)))( CONNECT_DATA=(SID=XE) ))';
            $conn = oci_connect("SIGA01", "SIGA01", $db);
            $query = oci_parse($conn,"SELECT FECH_CAMB_TIP,VALO_SOLE_TIP FROM TIPO_CAMBIO WHERE TO_CHAR(FECH_CAMB_TIP,'DD-MM-YYYY')=TO_CHAR(SYSDATE,'DD-MM-YYYY')");
            oci_execute($query);
            while ($row = oci_fetch_array($query, OCI_ASSOC)) {
                $tipo_Cambio=$row["VALO_SOLE_TIP"]."  ";
            }
            oci_free_statement($query);
            oci_close($conn);
        */
        
        $Requerimiento=Requerimiento::findOne($RequerimientoID);
        
        //$detallesRequerimientos=DetalleRequerimiento::find()->where('Codificacion=:Codificacion and RequerimientoID=:RequerimientoID',[':RequerimientoID'=>$RequerimientoID,':Codificacion'=>$Codificacion])->all();
        $detallesRequerimientos=DetalleRequerimiento::find()->where('ID in ('.$DetallesRequerimientosIDs.')')->all();
        //$detal=DetalleRequerimiento::find()->where('Codificacion=:Codificacion and RequerimientoID=:RequerimientoID',[':RequerimientoID'=>$RequerimientoID,':Codificacion'=>$Codificacion])->one();
        $detal=DetalleRequerimiento::find()->select('sum(Cantidad) Cantidad,sum(Cantidad*PrecioUnitario) Total')->where('ID in ('.$DetallesRequerimientosIDs.')')->one();
        //$areSub=AreSubCategoria::findOne($detal->AreSubCategoriaID);
        $certificacion=DetalleCertificacionPresupuestal::find()
                    ->select('DetalleCertificacionPresupuestal.CodigoCertificacion')
                    ->innerJoin('CertificacionPresupuestal','DetalleCertificacionPresupuestal.CertificacionPresupuestalID=CertificacionPresupuestal.ID')
                    ->where('CertificacionPresupuestal.Situacion=1 and CertificacionPresupuestal.Estado=1 and DetalleCertificacionPresupuestal.Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])
                    ->one();
        //var_dump($areSub);die;
        $orden=new Orden;
        //$orden->Concepto=$detal->Descripcion;
        $orden->Cantidad=$detal->Cantidad;
        //$orden->Descripcion=$areSub->Detalle;
        //$orden->UnidadMedida=$areSub->UnidadMedida;
        if($orden->load(Yii::$app->request->post())){
            $orden->FechaOrden=date('Ymd h:m:s');
            $orden->UnidadEjecutoraNombres='PROGRAMA NACIONAL DE INNONACIÓN AGRARIA - PNIA';
            $orden->UnidadEjecutoraDireccion='AV. LA MOLINA N° 1981 LA MOLINA - LIMA';
            $orden->UnidadEjecutoraRUC='20563395746';
            $orden->CodigoProyecto=$CodigoProyecto;
            $orden->TipoOrden=1;
            $orden->TipoCambio=1;
            $orden->Certificacion=$certificacion->CodigoCertificacion;
            $orden->Correlativo=$this->CorrelativoOrdenServicioFactura($orden->TipoOrden,$CodigoProyecto);
            $orden->Estado=1;
            $orden->Situacion=1;
            $orden->FechaRegistro=date('Ymd h:m:s');
            $orden->Annio=date('Y');
            $orden->save();
            foreach($detallesRequerimientos as $detalleRequerimiento){
                $detalle=DetalleRequerimiento::findOne($detalleRequerimiento->ID);
                $detalle->TipoDetalleOrdenID=$orden->ID;
                $detalle->Situacion=3;
                $detalle->update();
            }
            
            if(!$orden->Codigos)
            {
                $countCodigos=0;
            }
            else
            {
                $countCodigos=count(array_filter($orden->Codigos));
            }
            
            for($i=0;$i<$countCodigos;$i++)
            {
                if(isset($orden->Codigos[$i]))
                {
                    $detalle=new DetalleOrden;
                    $detalle->OrdenID=$orden->ID;
                    $detalle->Codigo=$orden->Codigos[$i];
                    $detalle->Cantidad=$orden->Cantidad;
                    $detalle->PrecioUnitario=str_replace(',','', $orden->PrecioUnitario);
                    $detalle->UnidadMedida=$orden->UnidadesMedidas[$i];
                    $detalle->Descripcion=$orden->Descripciones[$i];
                    $detalle->Total=$detalle->Cantidad*$detalle->PrecioUnitario;
                    $detalle->save();
                }
                $orden->Monto=$orden->Monto+$detalle->Total;
            }
            $orden->update();
            return $this->redirect(['requerimiento/generar','ID'=>$Requerimiento->ID]);
            /*
            return $this->redirect(['/orden'],302);
            exit(0);
            */
        }
        return $this->render('_form_orden_servicio_factura',['DetallesRequerimientosIDs'=>$DetallesRequerimientosIDs,'CodigoProyecto'=>$CodigoProyecto,'RequerimientoID'=>$RequerimientoID,'Requerimiento'=>$Requerimiento,'PrecioTotal'=>$detal->Total,'orden'=>$orden]);
    }
    
    // public function actionCrearOrdenServicioRh($DetalleRequerimientoID=null,$RequerimientoID=null,$Codificacion=null,$CodigoProyecto=null,$PrecioTotal=null){
    public function actionCrearOrdenServicioRh($RequerimientoID=null,$CodigoProyecto=null,$DetallesRequerimientosIDs=[]){
        $this->layout='vacio';
        $tipo_Cambio=0.00;

        $Requerimiento=Requerimiento::findOne($RequerimientoID);

        // $detallesRequerimientos=DetalleRequerimiento::find()->where('ID=:ID',[':ID'=>$DetalleRequerimientoID])->all();
        $detallesRequerimientos=DetalleRequerimiento::find()->where('ID in ('.$DetallesRequerimientosIDs.')')->all();
        $detal=DetalleRequerimiento::find()->select('sum(Cantidad) Cantidad,sum(Cantidad*PrecioUnitario) Total')->where('ID in ('.$DetallesRequerimientosIDs.')')->one();
        //
        // $detal=DetalleRequerimiento::find()->where('ID=:ID',[':ID'=>$DetalleRequerimientoID])->one();
        // $areSub=AreSubCategoria::find()->where('ID=:ID',[':ID'=>$detal->AreSubCategoriaID])->one();
        
        $orden=new Orden;
        // $orden->Concepto=$detal->Descripcion;
        $orden->Cantidad=$detal->Cantidad;
        // $orden->Descripcion=$areSub->Detalle;
        // $orden->UnidadMedida=$areSub->UnidadMedida;
        $certificacion=DetalleCertificacionPresupuestal::find()
                    ->select('DetalleCertificacionPresupuestal.CodigoCertificacion')
                    ->innerJoin('CertificacionPresupuestal','DetalleCertificacionPresupuestal.CertificacionPresupuestalID=CertificacionPresupuestal.ID')
                    ->where('CertificacionPresupuestal.Situacion=1 and CertificacionPresupuestal.Estado=1 and DetalleCertificacionPresupuestal.Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])
                    ->one();
        if($orden->load(Yii::$app->request->post())){
            $req=Requerimiento::findOne($orden->RequerimientoID);
            $orden->FechaOrden=date('Ymd h:m:s');
            $orden->UnidadEjecutoraNombres='PROGRAMA NACIONAL DE INNONACIÓN AGRARIA - PNIA';
            $orden->UnidadEjecutoraDireccion='AV. LA MOLINA N° 1981 LA MOLINA - LIMA';
            $orden->UnidadEjecutoraRUC='20563395746';
            $orden->CodigoProyecto=$CodigoProyecto;
            $orden->Certificacion=$certificacion->CodigoCertificacion;
            $orden->TipoOrden=2;
            // $orden->TipoCambio=$GetOracleTipoCambios['VALO_SOLE_TIP'];
            $orden->TipoCambio=1;
            $orden->Correlativo=$this->CorrelativoOrdenServicioRh($orden->TipoOrden,$CodigoProyecto);
            $orden->Estado=1;
            $orden->Situacion=1;
            $orden->FechaRegistro=date('Ymd');
            $orden->Annio=date('Y');
            $orden->save();
            foreach($detallesRequerimientos as $detalleRequerimiento){
                $detalle=DetalleRequerimiento::findOne($detalleRequerimiento->ID);
                $detalle->TipoDetalleOrdenID=$orden->ID;
                $detalle->Situacion=3;
                $detalle->update();
            }
            if(!$orden->Codigos)
            {
                $countCodigos=0;
            }
            else
            {
                $countCodigos=count(array_filter($orden->Codigos));
            }
            
            for($i=0;$i<$countCodigos;$i++)
            {
                if(isset($orden->Codigos[$i]))
                {
                    $detalle=new DetalleOrden;
                    $detalle->OrdenID=$orden->ID;
                    $detalle->Codigo=$orden->Codigos[$i];
                    $detalle->Cantidad=$orden->Cantidad;
                    $detalle->PrecioUnitario=str_replace(',','', $orden->PrecioUnitario);
                    $detalle->UnidadMedida=$orden->UnidadesMedidas[$i];
                    $detalle->Descripcion=$orden->Descripciones[$i];
                    $detalle->Total=$detalle->Cantidad*$detalle->PrecioUnitario;
                    $detalle->save();
                }
                $orden->Monto=$orden->Monto+$detalle->Total;
            }
            $orden->update();
            return $this->redirect(['requerimiento/generar','ID'=>$Requerimiento->ID]);
        }
        // return $this->render('_form_orden_servicio_rh',['DetalleRequerimientoID'=>$DetalleRequerimientoID,'CodigoProyecto'=>$CodigoProyecto,'requerimiento'=>$Requerimiento,'RequerimientoID'=>$RequerimientoID,'PrecioTotal'=>$PrecioTotal,'Codificacion'=>$Codificacion,'orden'=>$orden]);
        return $this->render('_form_orden_servicio_rh',['DetallesRequerimientosIDs'=>$DetallesRequerimientosIDs,'CodigoProyecto'=>$CodigoProyecto,'RequerimientoID'=>$RequerimientoID,'Requerimiento'=>$Requerimiento,'PrecioTotal'=>$detal->Total,'orden'=>$orden]);
        
    }
    
    public function CorrelativoOrden($tipoOrden,$CodigoProyecto){
        $orden=Orden::find()->where('CodigoProyecto=:CodigoProyecto and TipoOrden=:TipoOrden and Estado=1',[':CodigoProyecto'=>$CodigoProyecto,':TipoOrden'=>$tipoOrden])->orderBy('Correlativo desc')->one();
        if($orden)
        {
            return $orden->Correlativo+1;
        }
        else
        {
            return 1;
        }
    }
    
    public function CorrelativoOrdenServicio($tipoOrden,$CodigoProyecto)
    {
        $orden=Orden::find()->where('CodigoProyecto=:CodigoProyecto and TipoOrden in (1,2) and Estado=1',[':CodigoProyecto'=>$CodigoProyecto])->orderBy('Correlativo desc')->one();
        if($orden)
        {
            return $orden->Correlativo+1;
        }
        else
        {
            return 1;
        }
    }
    
    public function actionAdjuntarDocumento($ID=null){
        $this->layout='vacio';
        $orden=Orden::findOne($ID);
        $Requerimiento=Requerimiento::findOne($orden->RequerimientoID);
        if($orden->load(Yii::$app->request->post())){
            $orden->Situacion=3;
            $orden->archivo = UploadedFile::getInstance($orden, 'archivo');
            if($orden->archivo)
            {
                if($Requerimiento->TipoRequerimiento==1)
                {
                    $orden->archivo->saveAs('ordenServicioFactura/' . $orden->ID . '.' . $orden->archivo->extension);
                }
                elseif($Requerimiento->TipoRequerimiento==2)
                {
                    $orden->archivo->saveAs('ordenServicioRH/' . $orden->ID . '.' . $orden->archivo->extension);
                }
                elseif($Requerimiento->TipoRequerimiento==3)
                {
                    $orden->archivo->saveAs('ordenCompra/' . $orden->ID . '.' . $orden->archivo->extension);
                }
                
                $orden->Documento=$orden->ID . '.' . $orden->archivo->extension;
            }
            $orden->update();
            return $this->redirect(['requerimiento/generar','ID'=>$Requerimiento->ID]);
            //return $this->redirect(['/orden']);
        }
        return $this->render('adjuntar-documento',['ID'=>$ID,'orden'=>$orden]);
    }
    /*
    public function Plantilla($ID=null)
    {
        $orden=Orden::findOne($ID);
        $detalles=DetalleOrden::find()
                    ->Select('DetalleOrden.*')
                    ->where('OrdenID=:OrdenID',[':OrdenID'=>$ID])
                    ->all();
        $countDetalles=0;
        foreach($detalles as $detalle)
        {         
           $countDetalles++;
        }
      
        require_once(Yii::$app->basePath . '/web/PHPWord/src/PhpWord/Autoloader.php');     
        \PhpOffice\PhpWord\Autoloader::register();
        $phpWord = new PhpWord();
        $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/documentos/PLANTILLA_ORDEN_COMPRA.docx');
        $template->setValue('ID', $orden->Correlativo);
        $template->setValue('FECHAORDEN', date('d/m/Y',strtotime($orden->FechaOrden)));
        $template->setValue('CODIGOPROYECTO', $orden->CodigoProyecto);
        $template->setValue('RAZONSOCIAL', $orden->RazonSocial);
        $template->setValue('DIRECCION', $orden->Direccion);
        $template->setValue('RUC', $orden->RUC);
        $template->setValue('CODIGO', $orden->CodigoProyecto);
        $template->setValue('CERTIFICACION', $orden->Certificacion);
        $template->setValue('TIPOPROCESO', $this->getTipoProceso($orden->TipoProceso));
        $template->setValue('TELEFONO', $orden->Telefono);
        $template->setValue('TOTAL', $orden->Monto);
        $template->setValue('PNIARAZONSOCIAL', $orden->UnidadEjecutoraNombres);
        $template->setValue('PNIADIRECCION', $orden->UnidadEjecutoraDireccion);
        $template->setValue('PNIARUC', $orden->UnidadEjecutoraRUC);
        $template->setValue('VENTA', ($orden->Monto-($orden->Monto*0.18)));
        $template->setValue('IGV', ($orden->Monto*0.18));
        
        
        $template->cloneRow('CANT', $countDetalles);
        $countDetalles=1;
        foreach($detalles as $detalle)
        {
            
            $template->setValue('CANT#'.$countDetalles.'', "".$detalle->Cantidad."");
            $template->setValue('UNIDAD#'.$countDetalles.'', "".$detalle->UnidadMedida."");
            $template->setValue('DESCRIPCION#'.$countDetalles.'', "".$detalle->Descripcion."");
            $template->setValue('UNITARIO#'.$countDetalles.'', "".$detalle->PrecioUnitario."");
            $template->setValue('SUBTOTAL#'.$countDetalles.'', "".($detalle->Cantidad*$detalle->PrecioUnitario)."");
            //$template->setValue('DIRECTIVONOMBRE#'.$countdirectivos.'', "".ucwords(strtolower($registroparametros->getNombresWord($directivo->persona_id)))."");
            $countDetalles++;
            
        }
      
        $temp_file = tempnam(sys_get_temp_dir(), 'PHPWord');
        $template->saveAs($temp_file);
        //$template->ActiveDocument->ExportAsFixedFormat('yourdocument.pdf', 17, false, 0, 0, 0, 0, 7, true, true, 2, true, true, false);
        header("Content-Disposition: attachment; filename='a.docx'");
        readfile($temp_file); // or echo file_get_contents($temp_file);
        unlink($temp_file);  // remove temp file
        return true;    
    }
    */
    
    public function actionPlantilla($ID=null)
    {
        
        $orden=Orden::findOne($ID);
        $detalles=DetalleOrden::find()
                    ->Select('DetalleOrden.*')
                    ->where('OrdenID=:OrdenID',[':OrdenID'=>$ID])
                    ->all();
        $informacion=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$orden->CodigoProyecto])->one();
        $eea=UnidadOperativa::find()->where('ID=:ID',[':ID'=>$informacion->UnidadOperativaID])->one();
        $req=Requerimiento::findOne($orden->RequerimientoID);
        $detallesRequerimientos=DetalleRequerimiento::find()->where('RequerimientoID=:RequerimientoID and TipoDetalleOrdenID=:TipoDetalleOrdenID',[':RequerimientoID'=>$orden->RequerimientoID,':TipoDetalleOrdenID'=>$orden->ID])->all();
        //$term=TerminoReferencia::findOne($req->TerminoReferenciaID);
        $CadenaCodigoPoa="";
        $i=1;
        foreach($detallesRequerimientos as $detalleRequerimiento){
            if($i>1)
            {
                $CadenaCodigoPoa='O.'.explode('.', $detalleRequerimiento->Objetivo)[0].' A'.explode('.', $detalleRequerimiento->Actividad)[0].','.$CadenaCodigoPoa;    
            }
            else
            {
                $CadenaCodigoPoa='O.'.explode('.', $detalleRequerimiento->Objetivo)[0].' A'.explode('.', $detalleRequerimiento->Actividad)[0];
            }
            $i++;
        }
        
        $countDetalles=0;
        foreach($detalles as $detalle)
        {         
           $countDetalles++;
        }
      
        require_once(Yii::$app->basePath . '/web/PHPWord/src/PhpWord/Autoloader.php');     
        \PhpOffice\PhpWord\Autoloader::register();
        $phpWord = new PhpWord();
        
        if($orden->TipoOrden==1)
        {
            $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/documentos/PLANTILLA_ORDEN_SERVICIO_FACTURA.docx');
            $template->setValue('ID', str_pad($orden->Correlativo, 3, "0", STR_PAD_LEFT)."-".$orden->Annio);
            $template->setValue('EEA',$eea->Nombre);
            $template->setValue('FECHAORDEN', date('d-m-Y',strtotime($orden->FechaOrden)));
            $template->setValue('CODIGOPROYECTO', $orden->CodigoProyecto);
            $template->setValue('RAZONSOCIAL', $orden->RazonSocial);
            $template->setValue('DIRECCION', $orden->Direccion);
            $template->setValue('RUC', $orden->RUC);
            $template->setValue('CODIGO', $orden->CodigoProyecto);
            $template->setValue('CODIGOPOA', $CadenaCodigoPoa);
            $template->setValue('CONCEPTO', $orden->Concepto);
            $template->setValue('CERTIFICACION', $orden->Certificacion);
            $template->setValue('TIPOPROCESO', $this->getTipoProceso($orden->TipoProceso));
            $template->setValue('TELEFONO', $orden->Telefono);
            $template->setValue('TOTAL', number_format($orden->Monto, 2, '.', ' '));
            $template->setValue('PNIARAZONSOCIAL', $orden->UnidadEjecutoraNombres);
            $template->setValue('PNIADIRECCION', $orden->UnidadEjecutoraDireccion);
            $template->setValue('PNIARUC', $orden->UnidadEjecutoraRUC);
            /*
            $template->setValue('TOTALD', number_format(($orden->Monto-($orden->Monto*0.08)), 2, '.', ' '));
            $template->setValue('RETEN', number_format(($orden->Monto*0.08), 2, '.', ' '));
            */
            if($this->CodigosProyectosExonerados($orden->CodigoProyecto)){
                if($orden->ExoneradoIGV!=1)
                {  

                    $venta = $orden->Monto/1.18;
                    $igv   = $venta * 0.18;
                    $template->setValue('VENTA', number_format($venta, 2, '.', ' '));
                    $template->setValue('IGV', number_format($igv, 2, '.', ' '));
                    // $template->setValue('VENTA', number_format(($orden->Monto-($orden->Monto*0.18)), 2, '.', ' '));
                    // $template->setValue('IGV', number_format(($orden->Monto*0.18), 2, '.', ' ')); 
                    $template->setValue('EXO', number_format((0), 2, '.', ' '));
                }
                else
                {  
                    $template->setValue('VENTA', number_format((0), 2, '.', ' '));
                    $template->setValue('IGV', number_format((0), 2, '.', ' ')); 
                    $template->setValue('EXO', number_format(($orden->Monto), 2, '.', ' '));
                }
            }else{
                $template->setValue('VENTA', number_format((0), 2, '.', ' '));
                $template->setValue('IGV', number_format((0), 2, '.', ' ')); 
                $template->setValue('EXO', number_format(($orden->Monto), 2, '.', ' '));
            }
                

            
            
            $template->setValue('PLAZOSERVICIO', $orden->PlazoServicio);
            $template->setValue('LUGARENTREGA', $orden->LugarEntrega);
            $template->setValue('CONDICIONPAGO', $orden->CondicionPago);
            $template->setValue('PENALIDAD', $orden->Penalidad);
            $template->setValue('CUENTABANCARIA', $orden->CuentaBancaria);
            $template->setValue('CCI', $orden->CCI);
            $template->setValue('CLASI', '2 6 7 1 6 2');
            $template->setValue('CUENTADETRACCION', $orden->CuentaDetraccion);
            $template->setValue('CONFORMIDADSERVICIO', $orden->ConformidadServicio);
            $template->cloneRow('COD', $countDetalles);
            $countDetalles=1;
            foreach($detalles as $detalle)
            {
                $template->setValue('COD#'.$countDetalles.'', "".$detalle->Codigo."");
                $template->setValue('UNIDAD#'.$countDetalles.'', "".$detalle->UnidadMedida."");
                $template->setValue('DESCRIPCION#'.$countDetalles.'', "".$detalle->Descripcion."");
                $template->setValue('TOTAL#'.$countDetalles.'', "".number_format($detalle->Total, 2, '.', ' ')."");
                //$template->setValue('DIRECTIVONOMBRE#'.$countdirectivos.'', "".ucwords(strtolower($registroparametros->getNombresWord($directivo->persona_id)))."");
                $countDetalles++;
                
            }
        }
        elseif($orden->TipoOrden==2)
        {
            $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/documentos/PLANTILLA_ORDEN_SERVICIO_RH.docx');
            $template->setValue('ID', str_pad($orden->Correlativo, 3, "0", STR_PAD_LEFT)."-".$orden->Annio);
            $template->setValue('EEA',$eea->Nombre);
            $template->setValue('FECHAORDEN', date('d-m-Y',strtotime($orden->FechaOrden)));
            $template->setValue('CODIGOPROYECTO', $orden->CodigoProyecto);
            $template->setValue('RAZONSOCIAL', $orden->RazonSocial);
            $template->setValue('DIRECCION', $orden->Direccion);
            $template->setValue('RUC', $orden->RUC);
            $template->setValue('CODIGO', $orden->CodigoProyecto);
            $template->setValue('CODIGOPOA', $CadenaCodigoPoa);
            $template->setValue('CONCEPTO', $orden->Concepto);
            $template->setValue('CERTIFICACION', $orden->Certificacion);
            $template->setValue('TIPOPROCESO', $this->getTipoProceso($orden->TipoProceso));
            $template->setValue('TELEFONO', $orden->Telefono);
            $template->setValue('TOTAL', number_format($orden->Monto, 2, '.', ' '));
            $template->setValue('PNIARAZONSOCIAL', $orden->UnidadEjecutoraNombres);
            $template->setValue('PNIADIRECCION', $orden->UnidadEjecutoraDireccion);
            $template->setValue('PNIARUC', $orden->UnidadEjecutoraRUC);
            /*
            $template->setValue('VENTA', number_format(($orden->Monto-($orden->Monto*0.18)), 2, '.', ' '));
            $template->setValue('IGV', number_format(($orden->Monto*0.18), 2, '.', ' '));*/
            
            if($orden->Retencion4ta==1)
            {
                $template->setValue('TOTALD', number_format(($orden->Monto), 2, '.', ' '));
                $template->setValue('RETEN', number_format((0.00), 2, '.', ' '));
            }
            else
            {
                // $this->CodigosProyectosExonerados($orden->CodigoProyecto); die();
                if($this->CodigosProyectosExonerados($orden->CodigoProyecto)){
                    $template->setValue('TOTALD', number_format(($orden->Monto), 2, '.', ' '));
                    $template->setValue('RETEN', number_format((0.00), 2, '.', ' '));
                }else{
                    $template->setValue('TOTALD', number_format(($orden->Monto-($orden->Monto*0.08)), 2, '.', ' '));
                    $template->setValue('RETEN', number_format(($orden->Monto*0.08), 2, '.', ' '));
                }
            }
            
            
            $template->setValue('PLAZOSERVICIO', $orden->PlazoServicio);
            $template->setValue('LUGARENTREGA', $orden->LugarEntrega);
            $template->setValue('CONDICIONPAGO', $orden->CondicionPago);
            $template->setValue('PENALIDAD', $orden->Penalidad);
            $template->setValue('CUENTABANCARIA', $orden->CuentaBancaria);
            $template->setValue('CLASI', '2 6 7 1 6 2');
            $template->setValue('CCI', $orden->CCI);
            $template->setValue('CUENTADETRACCION', $orden->CuentaDetraccion);
            $template->setValue('CONFORMIDADSERVICIO', $orden->ConformidadServicio);
            $template->cloneRow('COD', $countDetalles);
            $countDetalles=1;
            foreach($detalles as $detalle)
            {
                
                $template->setValue('COD#'.$countDetalles.'', "".$detalle->Codigo."");
                $template->setValue('UNIDAD#'.$countDetalles.'', "".$detalle->UnidadMedida."");
                $template->setValue('DESCRIPCION#'.$countDetalles.'', "".$detalle->Descripcion."");
                $template->setValue('TOTAL#'.$countDetalles.'', "".number_format($detalle->Total, 2, '.', ' ')."");
                //$template->setValue('DIRECTIVONOMBRE#'.$countdirectivos.'', "".ucwords(strtolower($registroparametros->getNombresWord($directivo->persona_id)))."");
                $countDetalles++;
                
            }
        }
        elseif($orden->TipoOrden==3)
        {
            
            $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/documentos/PLANTILLA_ORDEN_COMPRA.docx');
            $template->setValue('ID', str_pad($orden->Correlativo, 3, "0", STR_PAD_LEFT)."-".$orden->Annio);
            $template->setValue('EEA',$eea->Nombre);
            $template->setValue('FECHAORDEN', date('d-m-Y',strtotime($orden->FechaOrden)));
            $template->setValue('CODIGOPROYECTO', $orden->CodigoProyecto);
            $template->setValue('RAZONSOCIAL', $orden->RazonSocial);
            $template->setValue('DIRECCION', $orden->Direccion);
            $template->setValue('RUC', $orden->RUC);
            $template->setValue('CODIGO', $orden->CodigoProyecto);
            $template->setValue('CONCEPTO', $orden->Concepto);
            $template->setValue('CODIGOPOA', $CadenaCodigoPoa);
            $template->setValue('CERTIFICACION', $orden->Certificacion);
            $template->setValue('TIPOPROCESO', $this->getTipoProceso($orden->TipoProceso));
            $template->setValue('TELEFONO', $orden->Telefono);
            $template->setValue('TOTAL', "S/. ".number_format($orden->Monto, 2, '.', ' '));
            $template->setValue('PNIARAZONSOCIAL', $orden->UnidadEjecutoraNombres);
            $template->setValue('PNIADIRECCION', $orden->UnidadEjecutoraDireccion);
            $template->setValue('PNIARUC', $orden->UnidadEjecutoraRUC);
            
            if($this->CodigosProyectosExonerados($orden->CodigoProyecto))
            {
                
                $template->setValue('VENTA', number_format($orden->Monto, 2, '.', ' '));
                $template->setValue('IGV', number_format(0, 2, '.', ' '));
            }
            else
            {
                $venta = $orden->Monto/1.18;
                $igv   = $venta * 0.18;
                
                $template->setValue('VENTA', number_format($venta, 2, '.', ' '));
                $template->setValue('IGV', number_format($igv, 2, '.', ' '));
            }
            
            $template->setValue('PLAZOENTREGA', $orden->PlazoEntrega);
            $template->setValue('LUGARENTREGA', $orden->LugarEntrega);
            $template->setValue('CONDICIONPAGO', $orden->CondicionPago);
            $template->setValue('PENALIDAD', $orden->Penalidad);
            $template->setValue('CUENTABANCARIA', $orden->CuentaBancaria);
            $template->setValue('CCI', $orden->CCI);
            $template->setValue('CLASI', '2 6 7 1 6 3');
            $template->cloneRow('CANT', $countDetalles);
            $countDetalles=1;
            foreach($detalles as $detalle)
            {
                $template->setValue('CANT#'.$countDetalles.'', "".number_format($detalle->Cantidad, 2, '.', ' ')."");
                $template->setValue('UNIDAD#'.$countDetalles.'', "".$detalle->UnidadMedida."");
                $template->setValue('DESCRIPCION#'.$countDetalles.'', "".$detalle->Descripcion."");
                $template->setValue('UNITARIO#'.$countDetalles.'', "".number_format($detalle->PrecioUnitario, 2, '.', ' ')."");
                $template->setValue('SUBTOTAL#'.$countDetalles.'', "".number_format(($detalle->Cantidad*$detalle->PrecioUnitario), 2, '.', ' ')."");
                //$template->setValue('DIRECTIVONOMBRE#'.$countdirectivos.'', "".ucwords(strtolower($registroparametros->getNombresWord($directivo->persona_id)))."");
                $countDetalles++;
            }
        }
        
        
      
        $temp_file = tempnam(sys_get_temp_dir(), 'PHPWord');
        $template->saveAs($temp_file);
        
        $contentType = 'Content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document;';
        header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
        header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
        header ( "Cache-Control: no-cache, must-revalidate" );
        header ( "Pragma: no-cache" );
        header ( $contentType );
        header ( "Content-Disposition: attachment; filename='Orden.docx'");
        readfile($temp_file); // or echo file_get_contents($temp_file);
        unlink($temp_file);  // remove temp file
        return true;    
    }
    
    
    public function actionServicio($valor=null)
    {
        $respuesta=1;
        if(!empty($valor)){
            if(strlen($valor)==11){
                $respuesta = Yii::$app->tools->ServicioSunat($valor);
                // $respuesta = file_get_contents("http://172.168.3.18/apisunat/api/consultarruc/" . $valor);
            }
            echo $respuesta;
        }
    }
    
    function curl_get_contents($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
    
    public function actionExcel($CodigoProyecto=null)
    {
        $resultados = (new \yii\db\Query())
            ->select('*,Orden.Correlativo OCorrelativo,Componente.Nombre NComponente,Componente.Correlativo CComponente,Actividad.Nombre NActividad,Actividad.Correlativo CActividad,AreSubCategoria.Nombre NAreSubCategoria,AreSubCategoria.Correlativo CAreSubCategoria,DetalleRequerimiento.Cantidad CDetalleRequerimiento')
            ->from('Orden')
            ->innerJoin('Requerimiento','Requerimiento.ID=Orden.RequerimientoID')
            ->innerJoin('DetalleRequerimiento','DetalleRequerimiento.RequerimientoID=Requerimiento.ID')
            ->innerJoin('AreSubCategoria','AreSubCategoria.ID=DetalleRequerimiento.AreSubCategoriaID')
            ->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')
            ->innerJoin('RubroElegible','RubroElegible.ID=ActRubroElegible.RubroElegibleID')
            ->innerJoin('Actividad','Actividad.ID=ActRubroElegible.ActividadID')
            ->innerJoin('Componente','Componente.ID=Actividad.ComponenteID')
            ->where(['Requerimiento.CodigoProyecto'=>$CodigoProyecto])
            ->distinct()
            ->all();
        $requerimiento=new Requerimiento;
        return $this->render('excel',['resultados'=>$resultados,'requerimiento'=>$requerimiento]);
    }
    
    public function actionReporte($CodigoProyecto=null)
    {
        $this->layout='estandar';
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        return $this->render('reporte',['CodigoProyecto'=>$CodigoProyecto]);
    }
    
    public function actionPoa($valor=null)
    {
        $requerimiento=Requerimiento::findOne($valor);
        $detalle=DetalleRequerimiento::find()->where('RequerimientoID=:RequerimientoID',[':RequerimientoID'=>$requerimiento->ID])->one();
        $AreSubCategoria=AreSubCategoria::findOne($detalle->AreSubCategoriaID);
        $ActRubroElegible=ActRubroElegible::findOne($AreSubCategoria->ActRubroElegibleID);
        $Actividad=Actividad::findOne($ActRubroElegible->ActividadID);
        $Componente=Componente::findOne($Actividad->ComponenteID);
        
        $arrayJson=['Success'=>true,'recurso'=>$AreSubCategoria->Correlativo.".".$AreSubCategoria->Nombre,
                    'componente'=>$Componente->Correlativo.".".$Componente->Nombre,
                    'actividad'=>$Actividad->Correlativo.".".$Actividad->Nombre,'codigo'=>$Componente->Correlativo.".".$Actividad->Correlativo.".".$AreSubCategoria->Correlativo];
        return json_encode($arrayJson);
    }
    
    
    public function actionCodificacion($CodigoProyecto=null)
    {
        $this->layout='vacio';
        $informacionGeneral=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacionGeneral->InvestigadorID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID and Estado=1',[':ProyectoID'=>$proyecto->ID])->one();
        //$componentes=Componente::find()->where('PoaID=:PoaID',[':PoaID'=>$poa->ID])->OrderBy('Correlativo asc')->all();
        $rubrosElegibles=RubroElegible::find()->all();
        return $this->render('codificacion',['CodigoProyecto'=>$CodigoProyecto,'rubrosElegibles'=>$rubrosElegibles]);
    }
    
    public function actionListadoCodificacion($CodigoProyecto=null,$RubroElegibleID=null){
        $this->layout='vacio';    
        if($CodigoProyecto)
        {
            $informacionGeneral=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])->one();
            $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacionGeneral->InvestigadorID])->one();
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
            $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacionGeneral->InvestigadorID])->one();
        }
        $PasoCritico=PasoCritico::find()->where('ProyectoID=:ProyectoID and Estado=1 and Situacion=2',[':ProyectoID'=>$proyecto->ID])->one();

        
        if($RubroElegibleID)
        {
            print_r($RubroElegibleID); //die();
            $resultados = (new \yii\db\Query())
            ->select('RubroElegible.ID,AreSubCategoria.Codificacion,AreSubCategoria.Especifica,AreSubCategoria.Nombre')
            ->from('AreSubCategoria')
            ->innerJoin('CronogramaAreSubCategoria','CronogramaAreSubCategoria.AreSubCategoriaID=AreSubCategoria.ID')
            ->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')
            ->innerJoin('RubroElegible','RubroElegible.ID=ActRubroElegible.RubroElegibleID')
            ->innerJoin('Actividad','Actividad.ID=ActRubroElegible.ActividadID')
            ->innerJoin('Componente','Componente.ID=Actividad.ComponenteID')
            ->innerJoin('Poa','Poa.ID=Componente.PoaID')
            ->innerJoin('Proyecto','Proyecto.ID=Poa.ProyectoID')
            ->innerJoin('Investigador','Investigador.ID=Proyecto.InvestigadorID')
            ->innerJoin('InformacionGeneral','InformacionGeneral.InvestigadorID=Investigador.ID')
            ->where(['InformacionGeneral.Codigo'=>$CodigoProyecto])
            ->andWhere(['RubroElegible.ID'=>$RubroElegibleID])
            ->andWhere(['between', 'CronogramaAreSubCategoria.Mes', $PasoCritico->MesInicio, $PasoCritico->MesFin])
            ->orderBy('RubroElegible.ID desc,AreSubCategoria.Codificacion desc')
            ->groupBy('RubroElegible.ID,AreSubCategoria.Codificacion,AreSubCategoria.Especifica,AreSubCategoria.Nombre')
            ->distinct()
            ->all();
        }
        else{
            $resultados = (new \yii\db\Query())
            ->select('RubroElegible.ID,AreSubCategoria.Codificacion,AreSubCategoria.Especifica,AreSubCategoria.Nombre')
            ->from('AreSubCategoria')
            ->innerJoin('CronogramaAreSubCategoria','CronogramaAreSubCategoria.AreSubCategoriaID=AreSubCategoria.ID')
            ->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')
            ->innerJoin('RubroElegible','RubroElegible.ID=ActRubroElegible.RubroElegibleID')
            ->innerJoin('Actividad','Actividad.ID=ActRubroElegible.ActividadID')
            ->innerJoin('Componente','Componente.ID=Actividad.ComponenteID')
            ->innerJoin('Poa','Poa.ID=Componente.PoaID')
            ->innerJoin('Proyecto','Proyecto.ID=Poa.ProyectoID')
            ->innerJoin('Investigador','Investigador.ID=Proyecto.InvestigadorID')
            ->innerJoin('InformacionGeneral','InformacionGeneral.InvestigadorID=Investigador.ID')
            ->where(['InformacionGeneral.Codigo'=>$CodigoProyecto])
            
            ->andWhere(['between', 'CronogramaAreSubCategoria.Mes', $PasoCritico->MesInicio, $PasoCritico->MesFin])
            
            ->orderBy('RubroElegible.ID desc,AreSubCategoria.Codificacion desc')
            ->groupBy('RubroElegible.ID,AreSubCategoria.Codificacion,AreSubCategoria.Especifica,AreSubCategoria.Nombre')
            ->distinct()
            ->all();
        }
            
        $nro=0;
        foreach($resultados as $result)
        {
            echo "<tr style='cursor:pointer' class='pintar_poa' id='".$nro."' data-rubro-elegible='".$result["ID"]. "' data-codificacion='".$result["Codificacion"]."'>";
            echo "<td> " . $result["ID"] . "." . $result["Codificacion"] . " </td>";
            echo "<td> " . $result["Nombre"] ."/".$result["Especifica"]. "</td>";
            echo "</tr>";
            $nro++;
        }
        
    }
    
    public function actionEliminar($id=null)
    {
        $orden=Orden::findOne($id);
        $orden->Estado=6;
        $orden->Estado=0;
        $orden->update();
        $requerimiento=Requerimiento::findOne($orden->RequerimientoID);
        $detalles=DetalleRequerimiento::find()->where('RequerimientoID=:RequerimientoID',[':RequerimientoID'=>$requerimiento->ID])->all();
        foreach($detalles as $detalle)
        {
            $detalle->Situacion=2;
            $detalle->TipoDetalleOrdenID='';
            $detalle->update();
        }
        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
    public function actionEliminarAdjunto($id=null)
    {
        $orden=Orden::findOne($id);
        $orden->Documento='';
        $orden->Situacion=1;
        $orden->update();
       
        $arr = array('Success' => true);
        echo json_encode($arr);
    }

    public function CorrelativoOrdenServicioRh($tipoOrden,$CodigoProyecto)
    {
        $orden=Orden::find()->where('CodigoProyecto=:CodigoProyecto and TipoOrden in (2,1) and Estado=1',[':CodigoProyecto'=>$CodigoProyecto])->orderBy('Correlativo desc')->one();
        if($orden)
        {
            return $orden->Correlativo+1;
        }
        else
        {
            return 1;
        }
    }
    
    public function CorrelativoOrdenServicioFactura($tipoOrden,$CodigoProyecto)
    {
        $orden=Orden::find()->where('CodigoProyecto=:CodigoProyecto and TipoOrden in (2,1) and Estado=1',[':CodigoProyecto'=>$CodigoProyecto])->orderBy('Correlativo desc')->one();
        if($orden)
        {
            return $orden->Correlativo+1;
        }
        else
        {
            return 1;
        }
    }
}
