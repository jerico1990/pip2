<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\PasoCritico;
use app\models\Usuario;
use app\models\Persona;
use app\models\Investigador;
use app\models\Proyecto;
use app\models\InformacionGeneral;
use app\models\Poa;
use app\models\Componente;
use app\models\Actividad;
use app\models\CronogramaProyecto;
use app\models\ActRubroElegible;
use app\models\CronogramaComponente;
use app\models\CronogramaActividad;
use app\models\CronogramaActRubroElegible;
use app\models\RubroElegible;
use app\models\AreSubCategoria;
use app\models\CronogramaAreSubCategoria;
use app\models\MarcoLogicoActividad;
use app\models\AreSubCategoriaObservacion;
use app\models\CompromisoPresupuestal;
use app\models\DetalleCompromisoPresupuestal;
use app\models\Orden;
use app\models\DetalleCertificacionPresupuestal;
use app\models\UsuarioProyecto;
use app\models\Requerimiento;
use app\models\DetalleRequerimiento;
use app\models\SiafSecuencialProyecto;
use app\models\Siaft;
use app\models\Siaf;
use app\models\CajaChica;
use app\models\Encargo;
use app\models\Viatico;
use app\models\DetalleCajaChica;
use app\models\Ejecucion;
use app\models\Situacion;
use app\models\SeguimientoCompromisoPresupuestal;
use app\models\ObservacionGeneral;

use PhpOffice\PhpWord\PhpWord;
use yii\web\UploadedFile;
use yii\base\ErrorException;
use yii\db\mssql\PDO;

class CompromisoPresupuestalController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($CodigoProyecto=NULL)
    {
        $this->layout='estandar';
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        return $this->render('index',['CodigoProyecto'=>$CodigoProyecto]);
    }
    
    public function actionListaCompromisos($CodigoProyecto=null)
    {
        date_default_timezone_set('America/Lima');
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        
        
        $resultados = (new \yii\db\Query())
            ->select('CompromisoPresupuestal.*,TipoGasto.Nombre NombreGasto')
            ->from('InformacionGeneral')
            ->innerJoin('Investigador','InformacionGeneral.InvestigadorID=Investigador.ID')
            ->innerJoin('Usuario','Usuario.ID=Investigador.UsuarioID')
            ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.Codigo=InformacionGeneral.Codigo')
            ->innerJoin('Requerimiento','Requerimiento.ID=CompromisoPresupuestal.RequerimientoID')
            ->innerJoin('TipoGasto','TipoGasto.ID=Requerimiento.TipoRequerimiento')
            ->where('InformacionGeneral.Codigo=:Codigo and CompromisoPresupuestal.Estado=1',[':Codigo'=>$CodigoProyecto])
            ->distinct()
            ->orderBy('CompromisoPresupuestal.ID asc')
            ->all();


        $nro=0;
        foreach($resultados as $result)
        {
            $nro++;
            echo "<tr>";
            echo "<td style='display:none'>" . $result["ID"] . "</td>";
            echo "<td class='text-center'>" . (str_pad($result["Correlativo"], 3, "0", STR_PAD_LEFT)  ."-".$result["Annio"]) . "</td>";
            echo "<td><a href='requerimiento/generar?ID=".$result["RequerimientoID"]."' target='_blank'>" . $result["NombreGasto"] . "</a></td>";
            echo "<td>" . $result["Memorando"] . "</td>";
            echo "<td>" .  Yii::$app->tools->dateFormat($result["FechaRegistro"]) . "</td>";
            if($result["Formato"] && $result["Situacion"]==14)
            {
                echo "<td align='middle'>
                    <a target='_blank' class='btn btn-default' href='compromisopresupuestal/" . $result["Formato"] . "'><span class='fa fa-cloud-download'></span> Descargar</a>
                    <a data-id=".$result["ID"]." class='btn btn-danger btn-remove-documento' href='#'><i class='fa fa-remove fa-lg'></i></a>    
                </td>";
            }
            else if(!$result["Formato"] && $result["Situacion"]==14)
            {
                echo "<td align='middle'>
                    <a target='_blank' class='btn btn-default' href='compromiso-presupuestal/plantilla?ID=" . $result["ID"] . "'><span class='fa fa-cloud-download'></span> Descargar</a>
                    <a data-id=".$result["ID"]." class='btn btn-default btn-adjuntar subir-doc' href='#'><span class='fa fa-cloud-upload'></span> Subir documento</a>    
                </td>";
            }
            else
            {
                echo "<td align='middle'><a target='_blank' class='btn btn-default' href='compromisopresupuestal/" . $result["Formato"] . "'><span class='fa fa-cloud-download'></span> Descargar</a></td>";
            }
            
            if($result['Situacion'] == 14){
                echo '<td class="red">Sin acción</td>';
                echo "<td>";
                if(!$result["Formato"] && $result["Situacion"]==14){
                    echo "<a href='javascript:;' class='btn btn-info enviar-aprobacion' data-id='".$result["ID"]."' disabled>Enviar</a> ";
                }else{
                    echo "<a href='javascript:;' class='btn btn-info enviar-aprobacion' data-id='".$result["ID"]."' >Enviar</a> ";
                }
                echo "<a href='#' class='btn btn-primary btn-edit-compromiso' data-id='".$result["ID"]."' data-codigo-proyecto='".$CodigoProyecto."'><i class='fa fa-edit fa-lg'></i></a> 
                    <a href='#' class='btn btn-danger btn-remove' data-id='".$result["ID"]."'><i class='fa fa-remove fa-lg'></i></a></td>";
                echo '<td>Inicio</td>';
            }elseif($result['Situacion'] == 15){
                
                $tiempo=SeguimientoCompromisoPresupuestal::find()
                        ->innerJoin('Usuario','Usuario.ID=SeguimientoCompromisoPresupuestal.UsuarioID')
                        ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                        ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID and UsuarioRol.RolID=2',[':CompromisoPresupuestalID'=>$result["ID"]])
                        ->one();
                echo '<td style="color:#6c6a6a;font-weight: bold;">En proceso</td>';
                echo "<td><a href='compromiso-presupuestal/ver?ID=".$result["ID"]."' class='btn btn-info'> Ver detalle </a> 
                <a href='seguimiento/ver?ID=".$result["ID"]."' class='btn btn-warning'> <i class='fa fa-code-fork'></i> </a></td>";
                echo '<td>Supervisor UAFSI - '.Yii::$app->tools->RestarFechas($tiempo->FechaInicio,date('Y-m-d h:m:s')).'</td>';
            }elseif($result['Situacion'] == 16){
                
                $tiempo=SeguimientoCompromisoPresupuestal::find()
                        ->innerJoin('Usuario','Usuario.ID=SeguimientoCompromisoPresupuestal.UsuarioID')
                        ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                        ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID and UsuarioRol.RolID=15',[':CompromisoPresupuestalID'=>$result["ID"]])
                        ->one();
                        
                echo '<td style="color:#6c6a6a;font-weight: bold;">En proceso</td>';
                echo "<td><a href='compromiso-presupuestal/ver?ID=".$result["ID"]."' class='btn btn-info'> Ver detalle </a> <a href='seguimiento/ver?ID=".$result["ID"]."' class='btn btn-warning'> <i class='fa fa-code-fork'></i> </a></td>";
                echo '<td>Esp. seguimiento UAFSI - '.Yii::$app->tools->RestarFechas($tiempo->FechaInicio,date('Y-m-d h:m:s')).'</td>';
            }elseif($result['Situacion'] == 17){
                
                $tiempo=SeguimientoCompromisoPresupuestal::find()
                        ->innerJoin('Usuario','Usuario.ID=SeguimientoCompromisoPresupuestal.UsuarioID')
                        ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                        ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID and UsuarioRol.RolID=11',[':CompromisoPresupuestalID'=>$result["ID"]])
                        ->one();
                        
                echo '<td style="color:#6c6a6a;font-weight: bold;">En proceso</td>';
                echo "<td><a href='compromiso-presupuestal/ver?ID=".$result["ID"]."' class='btn btn-info'> Ver detalle </a> <a href='seguimiento/ver?ID=".$result["ID"]."' class='btn btn-warning'> <i class='fa fa-code-fork'></i> </a></td>";
                echo '<td>Jefe UAFSI - '.Yii::$app->tools->RestarFechas($tiempo->FechaInicio,date('Y-m-d h:m:s')).'</td>';
            }elseif($result['Situacion'] == 18){
                
                $tiempo=SeguimientoCompromisoPresupuestal::find()
                        ->innerJoin('Usuario','Usuario.ID=SeguimientoCompromisoPresupuestal.UsuarioID')
                        ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                        ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID and UsuarioRol.RolID=12',[':CompromisoPresupuestalID'=>$result["ID"]])
                        ->one();
                        
                echo '<td style="color:#6c6a6a;font-weight: bold;">En proceso</td>';
                echo "<td><a href='compromiso-presupuestal/ver?ID=".$result["ID"]."' class='btn btn-info'> Ver detalle </a> <a href='seguimiento/ver?ID=".$result["ID"]."' class='btn btn-warning'> <i class='fa fa-code-fork'></i> </a></td>";
                echo '<td>Jefe Administración - '.Yii::$app->tools->RestarFechas($tiempo->FechaInicio,date('Y-m-d h:m:s')).'</td>';
            }elseif($result['Situacion'] == 19){
                
                $tiempo=SeguimientoCompromisoPresupuestal::find()
                        ->innerJoin('Usuario','Usuario.ID=SeguimientoCompromisoPresupuestal.UsuarioID')
                        ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                        ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID and UsuarioRol.RolID=13',[':CompromisoPresupuestalID'=>$result["ID"]])
                        ->one();
                        
                echo '<td style="color:#6c6a6a;font-weight: bold;">En proceso</td>';
                echo "<td><a href='compromiso-presupuestal/ver?ID=".$result["ID"]."' class='btn btn-info'> Ver detalle </a> <a href='seguimiento/ver?ID=".$result["ID"]."' class='btn btn-warning'> <i class='fa fa-code-fork'></i> </a></td>";
                echo '<td>Jefe Contabilidad - '.Yii::$app->tools->RestarFechas($tiempo->FechaInicio,date('Y-m-d h:m:s')).'</td>';
            }elseif($result['Situacion'] == 20){
                
                $tiempo=SeguimientoCompromisoPresupuestal::find()
                        ->innerJoin('Usuario','Usuario.ID=SeguimientoCompromisoPresupuestal.UsuarioID')
                        ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                        ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID and UsuarioRol.RolID=14',[':CompromisoPresupuestalID'=>$result["ID"]])
                        ->one();
                        
                echo '<td style="color:#6c6a6a;font-weight: bold;">En proceso</td>';
                echo "<td><a href='compromiso-presupuestal/ver?ID=".$result["ID"]."' class='btn btn-info'> Ver detalle </a> <a href='seguimiento/ver?ID=".$result["ID"]."' class='btn btn-warning'> <i class='fa fa-code-fork'></i> </a></td>";
                echo '<td>Especialista administrativo - '.Yii::$app->tools->RestarFechas($tiempo->FechaInicio,date('Y-m-d h:m:s')).'</td>';
            }elseif($result['Situacion'] == 21){
                
                $tiempo=SeguimientoCompromisoPresupuestal::find()
                        ->innerJoin('Usuario','Usuario.ID=SeguimientoCompromisoPresupuestal.UsuarioID')
                        ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                        ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID and UsuarioRol.RolID=14',[':CompromisoPresupuestalID'=>$result["ID"]])
                        ->one();
                        
                echo '<td style="color:#6c6a6a;font-weight: bold;">En proceso</td>';
                echo "<td><a href='compromiso-presupuestal/ver?ID=".$result["ID"]."' class='btn btn-info'> Ver detalle </a> <a href='seguimiento/ver?ID=".$result["ID"]."' class='btn btn-warning'> <i class='fa fa-code-fork'></i> </a></td>";
                // echo '<td>Especialista administrativo - '.Yii::$app->tools->RestarFechas($tiempo->FechaInicio,date('Y-m-d h:m:s')).'</td>';
                echo '<td>FINALIZADO</td>';
            }elseif($result['Situacion'] == 5){
                echo '<td class="red">Cerrado</td>';
                echo '<td><a href="compromiso-presupuestal/ver?ID='.$result["ID"].'" class="btn btn-info">Ver detalle</a></td>';
                echo '<td>CERRADO</td>';
                
            }elseif($result['Situacion'] == 6){
                echo '<td  style="color:red;font-weight: bold;">Anulado</td>';
                echo '<td><a href="compromiso-presupuestal/ver?ID='.$result["ID"].'" class="btn btn-info">Ver detalle</a></td>';
                echo '<td></td>';
                
            }elseif($result['Situacion'] == 4){

                $tiempo=SeguimientoCompromisoPresupuestal::find()
                        ->innerJoin('Usuario','Usuario.ID=SeguimientoCompromisoPresupuestal.UsuarioID')
                        ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                        ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID and UsuarioRol.RolID=7',[':CompromisoPresupuestalID'=>$result["ID"]])
                        ->one();

                echo '<td style="color:red;font-weight: bold;">Observado</td>';
                echo '<td><a href="compromiso-presupuestal/ver?ID='.$result["ID"].'" class="btn btn-info">Ver detalle</a></td>';
                echo '<td>IRP - '.Yii::$app->tools->RestarFechas($tiempo->FechaInicio,date('Y-m-d h:m:s')).'</td>';
            
            }else{
                echo '<td>-</td>';
                echo '<td><a href="compromiso-presupuestal/ver?ID='.$result["ID"].'" class="btn btn-info">Ver detalle</a></td>';
                echo '<td>-</td>';
            }
            echo "</tr>";
        }
    }

    public function actionActivarCompromiso(){
        $this->layout='vacio';
        $xid = $_POST['ID'];

        $xCodigo = CompromisoPresupuestal::find()->where('ID=:ID',[':ID'=>$xid ])->one();
        $xUsuProy = UsuarioProyecto::find()
                    ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=UsuarioProyecto.UsuarioID')
                    ->where(['CodigoProyecto'=>$xCodigo->Codigo,'UsuarioRol.RolID'=>2])
                    ->one();
        $compromiso=CompromisoPresupuestal::findOne($xid);
        $compromiso->Situacion = 15;
        $compromiso->UsuarioAsignado = $xUsuProy->UsuarioID;
        $compromiso->update();
        $segu=new SeguimientoCompromisoPresupuestal;
        $segu->CompromisoPresupuestalID=$compromiso->ID;
        $segu->UsuarioID=$xUsuProy->UsuarioID;
        $segu->FechaInicio=date('Ymd h:m:s');
        $segu->Estado=1;
        $segu->Situacion=0;
        $segu->FechaRegistro=date('Ymd h:m:s');
        $segu->save();
        
        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
    public function actionCrear($CodigoProyecto)
    {
        $this->layout='vacio';
        $informacionGeneral=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacionGeneral->InvestigadorID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
        $componentes=Componente::find()->where('PoaID=:PoaID',[':PoaID'=>$poa->ID])->all();
        
        $pasoCritico = PasoCritico::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();

        $requerimientos=Requerimiento::find()
        ->select('TipoGasto.*,Requerimiento.*')
        ->innerJoin('TipoGasto','TipoGasto.ID = Requerimiento.TipoRequerimiento')
        ->where('Requerimiento.ID not in (select RequerimientoID from DetalleRequerimiento where TipoDetalleOrdenID is null and RequerimientoID=Requerimiento.ID) and Requerimiento.Estado=1 and Requerimiento.Situacion=3 and Requerimiento.CodigoProyecto=:CodigoProyecto and Requerimiento.ID not in (select RequerimientoID from CompromisoPresupuestal where Estado=1 and CodigoProyecto=\''.$CodigoProyecto.'\')',[':CodigoProyecto'=>$CodigoProyecto])
        ->all();


        $compromisoPresupuestal=new CompromisoPresupuestal;
        if($compromisoPresupuestal->load(Yii::$app->request->post())){
            $compromisoPresupuestal->Annio=date('Y');
            $compromisoPresupuestal->FechaRegistro=date('Ymd');
            $compromisoPresupuestal->Estado=1;
            $compromisoPresupuestal->PasoCriticoID = $pasoCritico->ID;
            //$compromisoPresupuestal->Situacion=0;
            $compromisoPresupuestal->Situacion=14;
            $compromisoPresupuestal->GastoElegible=0;
            $compromisoPresupuestal->Correlativo=$this->CorrelativoCompromisoPresupuestal($CodigoProyecto);
            // echo '<pre>';print_r($compromisoPresupuestal); die;
            $compromisoPresupuestal->save();
            $requerimiento=Requerimiento::findOne($compromisoPresupuestal->RequerimientoID);
            // echo '<pre>';print_r($requerimiento); die;
            if($requerimiento->TipoRequerimiento==1)
            {
                $detallesRequerimientos=DetalleRequerimiento::find()->select('TipoDetalleOrdenID')->where('RequerimientoID=:RequerimientoID',[':RequerimientoID'=>$requerimiento->ID])->GroupBy('TipoDetalleOrdenID')->all();
                foreach($detallesRequerimientos as $detalleRequerimiento)
                {
                    $detalle=new DetalleCompromisoPresupuestal;
                    $detalle->CompromisoPresupuestalID=$compromisoPresupuestal->ID;
                    $detalle->TipoGasto=$requerimiento->TipoRequerimiento;
                    $detalle->Correlativo=$detalleRequerimiento->TipoDetalleOrdenID;
                    $detalle->CodigoMatriz=$this->getCodigosMatrices($requerimiento->ID,$detalleRequerimiento->TipoDetalleOrdenID);
                    $detalle->save();
                }
            }
            elseif($requerimiento->TipoRequerimiento==2)
            {
                $detallesRequerimientos=DetalleRequerimiento::find()->select('TipoDetalleOrdenID')->where('RequerimientoID=:RequerimientoID',[':RequerimientoID'=>$requerimiento->ID])->GroupBy('TipoDetalleOrdenID')->all();
                foreach($detallesRequerimientos as $detalleRequerimiento)
                {
                    $detalle=new DetalleCompromisoPresupuestal;
                    $detalle->CompromisoPresupuestalID=$compromisoPresupuestal->ID;
                    $detalle->TipoGasto=$requerimiento->TipoRequerimiento;
                    $detalle->Correlativo=$detalleRequerimiento->TipoDetalleOrdenID;
                    $detalle->CodigoMatriz=$this->getCodigosMatrices($requerimiento->ID,$detalleRequerimiento->TipoDetalleOrdenID);
                    $detalle->save();
                }
            }
            elseif($requerimiento->TipoRequerimiento==3)
            {
                $detallesRequerimientos=DetalleRequerimiento::find()->select('TipoDetalleOrdenID')->where('RequerimientoID=:RequerimientoID',[':RequerimientoID'=>$requerimiento->ID])->GroupBy('TipoDetalleOrdenID')->all();
                foreach($detallesRequerimientos as $detalleRequerimiento)
                {
                    $detalle=new DetalleCompromisoPresupuestal;
                    $detalle->CompromisoPresupuestalID=$compromisoPresupuestal->ID;
                    $detalle->TipoGasto=$requerimiento->TipoRequerimiento;
                    $detalle->Correlativo=$detalleRequerimiento->TipoDetalleOrdenID;
                    $detalle->CodigoMatriz=$this->getCodigosMatrices($requerimiento->ID,$detalleRequerimiento->TipoDetalleOrdenID);
                    $detalle->save();
                }
            }
            elseif($requerimiento->TipoRequerimiento==4)
            {
                $detallesRequerimientos=DetalleRequerimiento::find()->select('TipoDetalleOrdenID')->where('RequerimientoID=:RequerimientoID',[':RequerimientoID'=>$requerimiento->ID])->GroupBy('TipoDetalleOrdenID')->all();
                foreach($detallesRequerimientos as $detalleRequerimiento)
                {
                    $detalle=new DetalleCompromisoPresupuestal;
                    $detalle->CompromisoPresupuestalID=$compromisoPresupuestal->ID;
                    $detalle->TipoGasto=$requerimiento->TipoRequerimiento;
                    $detalle->Correlativo=$detalleRequerimiento->TipoDetalleOrdenID;
                    $detalle->CodigoMatriz=$this->getCodigosMatrices($requerimiento->ID,$detalleRequerimiento->TipoDetalleOrdenID);
                    $detalle->save();
                }
            }
            elseif($requerimiento->TipoRequerimiento==5)
            {
                /*
                $cajaChica=CajaChica::find()->where('RequerimientoID=:RequerimientoID and Estado=1 and Situacion=1',[':RequerimientoID'=>$requerimiento->ID])->one();
                $detalle=new DetalleCompromisoPresupuestal;
                $detalle->CompromisoPresupuestalID=$compromisoPresupuestal->ID;
                $detalle->TipoGasto=$requerimiento->TipoRequerimiento;
                $detalle->Correlativo=$cajaChica->ID;
                $detalle->save();*/
                $detallesRequerimientos=DetalleRequerimiento::find()->where('RequerimientoID=:RequerimientoID',[':RequerimientoID'=>$requerimiento->ID])->all();
                foreach($detallesRequerimientos as $detalleRequerimiento)
                {
                    $detalle=new DetalleCompromisoPresupuestal;
                    $detalle->CompromisoPresupuestalID=$compromisoPresupuestal->ID;
                    $detalle->TipoGasto=$requerimiento->TipoRequerimiento;
                    $detalle->Correlativo=$detalleRequerimiento->TipoDetalleOrdenID;
                    $detalle->CodigoMatriz=$detalleRequerimiento->CodigoMatriz;
                    $detalle->save();
                }
                
            }
            elseif($requerimiento->TipoRequerimiento==6)
            {
                $detallesRequerimientos=DetalleRequerimiento::find()->select('TipoDetalleOrdenID')->where('RequerimientoID=:RequerimientoID',[':RequerimientoID'=>$requerimiento->ID])->GroupBy('TipoDetalleOrdenID')->all();
                foreach($detallesRequerimientos as $detalleRequerimiento)
                {
                    $detalle=new DetalleCompromisoPresupuestal;
                    $detalle->CompromisoPresupuestalID=$compromisoPresupuestal->ID;
                    $detalle->TipoGasto=$requerimiento->TipoRequerimiento;
                    $detalle->Correlativo=$detalleRequerimiento->TipoDetalleOrdenID;
                    $detalle->CodigoMatriz=$this->getCodigosMatrices($requerimiento->ID,$detalleRequerimiento->TipoDetalleOrdenID);
                    $detalle->save();
                }
            }
            /*
            if(!$compromisoPresupuestal->Cantidades)
            {
                $countProveedores=0;
            }
            else
            {
                $countProveedores=count(array_filter($compromisoPresupuestal->Cantidades));
            }
            
            for($i=0;$i<$countProveedores;$i++)
            {
                if(isset($countProveedores->Cantidades[$i]))
                {
                    //$farmers=Farmers::model()->find('id=:id',array(':id'=>$model->farmers_ids[$i]));
                    //$farmers->form_id=$model->id;
                    //$farmers->name=$model->farmers_nombres[$i];
                    //$farmers->document_number=$model->farmers_dnis[$i];
                    //$farmers->update();
                }
                else
                {
                    $detalle=new DetalleCompromisoPresupuestal;
                    $detalle->CompromisoPresupuestalID=$compromisoPresupuestal->ID;
                    $detalle->TipoGasto=$compromisoPresupuestal->TiposGastos[$i];
                    $detalle->Correlativo=$compromisoPresupuestal->Correlativos[$i];
                    $detalle->save();
                    //$compromisoPresupuestal->MontoTotal=$compromisoPresupuestal->MontoTotal+$detalle->Monto;
                }
            }
            */
            $compromisoPresupuestal->archivo = UploadedFile::getInstance($compromisoPresupuestal, 'archivo');
            
            if($compromisoPresupuestal->archivo)
            {
                $compromisoPresupuestal->archivo->saveAs('compromisopresupuestal/' . $compromisoPresupuestal->ID . '.' . $compromisoPresupuestal->archivo->extension);
                $compromisoPresupuestal->Documento=$compromisoPresupuestal->ID . '.' . $compromisoPresupuestal->archivo->extension;
            }
            $compromisoPresupuestal->update();
            return $this->redirect(['/compromiso-presupuestal']);
            
        }
        return $this->render('_form',['CodigoProyecto'=>$CodigoProyecto,'componentes'=>$componentes,'requerimientos'=>$requerimientos]);
    }
    
    public function getCodigosMatrices($RequerimientoID,$TipoDetalleOrdenID)
    {
        $codigos="";
        $DetallesRequerimientos=DetalleRequerimiento::find()->where('RequerimientoID=:RequerimientoID and TipoDetalleOrdenID=:TipoDetalleOrdenID',[':RequerimientoID'=>$RequerimientoID,':TipoDetalleOrdenID'=>$TipoDetalleOrdenID])->OrderBy('CodigoMatriz asc')->all();
        $contador=0;
        foreach($DetallesRequerimientos as $DetalleRequerimiento)
        {
            if($contador==0)
            {
                $codigos=$DetalleRequerimiento->CodigoMatriz;    
            }
            else
            {
                $codigos=$codigos.",".$DetalleRequerimiento->CodigoMatriz;
            }
            
            $contador++;
        }
        
        return $codigos;
    }
    
    public function actionActualizar($ID)
    {
        $this->layout='vacio';
        $compromisoPresupuestal=CompromisoPresupuestal::findOne($ID);
        $RequerimientoIDant=$compromisoPresupuestal->RequerimientoID;

        $requerimientos=Requerimiento::find()
        ->select('TipoGasto.*,Requerimiento.*')
        ->innerJoin('TipoGasto','TipoGasto.ID = Requerimiento.TipoRequerimiento')
        ->where('Requerimiento.Estado=1 and Requerimiento.Situacion=3 and Requerimiento.CodigoProyecto=:CodigoProyecto and Requerimiento.ID not in (select RequerimientoID from CompromisoPresupuestal where Estado=1 and CodigoProyecto=\''.$compromisoPresupuestal->Codigo.'\' AND ID NOT IN ('.$ID.') )',[':CodigoProyecto'=>$compromisoPresupuestal->Codigo])
        ->all();

        if($compromisoPresupuestal->load(Yii::$app->request->post())){
            $compromisoPresupuestal->FechaRegistro=date('Ymd');
            $compromisoPresupuestal->Estado=1;
            $compromisoPresupuestal->Situacion=14;
            $compromisoPresupuestal->GastoElegible=0;
            //$compromisoPresupuestal->Correlativo=$this->CorrelativoCompromisoPresupuestal($compromisoPresupuestal->Codigo);
            $compromisoPresupuestal->save();
            $requerimiento=Requerimiento::findOne($compromisoPresupuestal->RequerimientoID);
            $detallesRequerimientos=DetalleRequerimiento::find()->where('RequerimientoID=:RequerimientoID',[':RequerimientoID'=>$requerimiento->ID])->all();
            if($compromisoPresupuestal->RequerimientoID!=$RequerimientoIDant)
            {
                DetalleCompromisoPresupuestal::deleteAll('CompromisoPresupuestalID=:CompromisoPresupuestalID',[':CompromisoPresupuestalID'=>$compromisoPresupuestal->ID]);
                foreach($detallesRequerimientos as $detalleRequerimiento)
                {
                    $detalle=new DetalleCompromisoPresupuestal;
                    $detalle->CompromisoPresupuestalID=$compromisoPresupuestal->ID;
                    $detalle->TipoGasto=$requerimiento->TipoRequerimiento;
                    $detalle->Correlativo=$detalleRequerimiento->TipoDetalleOrdenID;
                    $detalle->save();
                }
            }
            $compromisoPresupuestal->archivo = UploadedFile::getInstance($compromisoPresupuestal, 'archivo');
            
            if($compromisoPresupuestal->archivo)
            {
                $compromisoPresupuestal->archivo->saveAs('compromisopresupuestal/' . $compromisoPresupuestal->ID . '.' . $compromisoPresupuestal->archivo->extension);
                $compromisoPresupuestal->Documento=$compromisoPresupuestal->ID . '.' . $compromisoPresupuestal->archivo->extension;
            }
            $compromisoPresupuestal->update();
            return $this->redirect(['/compromiso-presupuestal']);
            
        }
        return $this->render('_actualizar',['requerimientos'=>$requerimientos,'compromisoPresupuestal'=>$compromisoPresupuestal]);
    }
    
    public function actionCorrelativoGasto()
    {
        if(isset($_POST["TipoGasto"]) && ($_POST["TipoGasto"]=="3" || $_POST["TipoGasto"]=="2"))
        {
            $TipoGasto=$_POST["TipoGasto"];
            $countOrdenes=Orden::find()->select('ID,Correlativo,TipoOrden')->where('TipoOrden=:TipoOrden',[':TipoOrden'=>$TipoGasto])->groupBy('ID,Correlativo,TipoOrden')->count();
            $ordenes=Orden::find()->select('ID,Correlativo,TipoOrden')->where('TipoOrden=:TipoOrden',[':TipoOrden'=>$TipoGasto])->groupBy('ID,Correlativo,TipoOrden')->orderBy('Correlativo')->all();
            if($countOrdenes>0){
                echo "<option value>Seleccionar</option>";
                foreach($ordenes as $orden){
                    if($orden->TipoOrden==2)
                    {
                        echo "<option value='".$orden->ID."'> O.S. N° ".str_pad($orden->Correlativo, 3, "0", STR_PAD_LEFT)."".$orden->Annio."</option>";   
                    }
                    elseif($orden->TipoOrden==3)
                    {
                        echo "<option value='".$orden->ID."'> O.C. N° ".str_pad($orden->Correlativo, 3, "0", STR_PAD_LEFT)."".$orden->Annio."</option>";
                    }
                    
                }
            }
            else{
                echo "<option value>Seleccionar</option>";
            }
        }
    }
    
    public function CorrelativoCompromisoPresupuestal($CodigoProyecto)
    {
        $compromiso=CompromisoPresupuestal::find()->where('Codigo=:Codigo and Estado=1',[':Codigo'=>$CodigoProyecto])->orderBy('Correlativo desc')->one();
        if($compromiso)
        {
            return $compromiso->Correlativo+1;
        }
        else
        {
            return 1;
        }
    }
    
    public function actionPlantilla($ID=null)
    {
        $compromiso=CompromisoPresupuestal::findOne($ID);
        $certificacion=DetalleCertificacionPresupuestal::find()
                    ->select('DetalleCertificacionPresupuestal.CodigoCertificacion')
                    ->innerJoin('CertificacionPresupuestal','DetalleCertificacionPresupuestal.CertificacionPresupuestalID=CertificacionPresupuestal.ID')
                    ->where('CertificacionPresupuestal.Situacion=1 and CertificacionPresupuestal.Estado=1 and DetalleCertificacionPresupuestal.Codigo=:Codigo',[':Codigo'=>$compromiso->Codigo])
                    ->one();
        $requerimiento=Requerimiento::findOne($compromiso->RequerimientoID);
        //var_dump($requerimiento);die;
        $detalles=DetalleCompromisoPresupuestal::find()
                    ->Select('DetalleCompromisoPresupuestal.Correlativo')
                    ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID',[':CompromisoPresupuestalID'=>$ID])
                    ->groupBy('DetalleCompromisoPresupuestal.Correlativo')
                    ->all();
        
        $informacionGeneral=InformacionGeneral::find()
                        ->select('InformacionGeneral.*,UnidadOperativa.Nombre UnidadEjecutoraID')
                        ->innerJoin('UnidadOperativa','UnidadOperativa.ID=InformacionGeneral.UnidadOperativaID')
                        ->where('InformacionGeneral.Codigo=:Codigo',[':Codigo'=>$compromiso->Codigo])
                        ->one();
                        
        $countDetalles=0;
        foreach($detalles as $detalle)
        {
           $countDetalles++;
        }
        
        require_once(Yii::$app->basePath . '/web/PHPWord/src/PhpWord/Autoloader.php');     
        \PhpOffice\PhpWord\Autoloader::register();
        $phpWord = new PhpWord();
        
        $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/documentos/PLANTILLA_SOLICITUD_COMPROMISO_PRESUPUESTAL.docx');
        
        if($requerimiento->TipoRequerimiento==1)
        {
            ///var_dump($requerimiento->Referencia);die;
            $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/documentos/PLANTILLA_SOLICITUD_COMPROMISO_PRESUPUESTAL_SERVICIO.docx');
            $template->setValue('CODIGOPROYECTO', $compromiso->Codigo);
            $template->setValue('CERTIFICACION', $certificacion->CodigoCertificacion);
            $template->setValue('MEMORANDO', $compromiso->Memorando  ."-".$compromiso->Annio);
            $template->setValue('EEA', $informacionGeneral->UnidadEjecutoraID);
            $template->setValue('REFERENCIA', ''.$requerimiento->Referencia.'');
            $template->cloneRow('NS', $countDetalles);
            $countDetalles=1;
            foreach($detalles as $detalle)
            {
                $orden=Orden::findOne($detalle->Correlativo);
                $template->setValue('NS#'.$countDetalles.'', "".$countDetalles."");
                $template->setValue('PROVEEDORS#'.$countDetalles.'', "".ucwords(strtolower($orden->RazonSocial))."");
                $template->setValue('RUCS#'.$countDetalles.'', "".ucwords(strtolower($orden->RUC))."");
                $template->setValue('MONTOS#'.$countDetalles.'', "".(($orden->Monto)?number_format(ucwords(strtolower($orden->Monto)), 2, '.', ' '):0)."");
                $countDetalles++;
            }
        }
        elseif($requerimiento->TipoRequerimiento==2)
        {
            $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/documentos/PLANTILLA_SOLICITUD_COMPROMISO_PRESUPUESTAL_SERVICIO.docx');
            $template->setValue('CODIGOPROYECTO', $compromiso->Codigo);
            $template->setValue('CERTIFICACION', $certificacion->CodigoCertificacion);
            $template->setValue('MEMORANDO', $compromiso->Memorando  ."-".$compromiso->Annio);
            $template->setValue('EEA', $informacionGeneral->UnidadEjecutoraID);
            $template->setValue('REFERENCIA', $requerimiento->Referencia);
            $template->cloneRow('NS', $countDetalles);
            $countDetalles=1;
            foreach($detalles as $detalle)
            {
                $orden=Orden::findOne($detalle->Correlativo);
                $template->setValue('NS#'.$countDetalles.'', "".$countDetalles."");
                $template->setValue('PROVEEDORS#'.$countDetalles.'', "".ucwords(strtolower($orden->RazonSocial))."");
                $template->setValue('RUCS#'.$countDetalles.'', "".ucwords(strtolower($orden->RUC))."");
                $template->setValue('MONTOS#'.$countDetalles.'', "".(($orden->Monto)?number_format(ucwords(strtolower($orden->Monto)), 2, '.', ' '):0)."");
                $countDetalles++;
            }
        }
        elseif($requerimiento->TipoRequerimiento==3)
        {
            $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/documentos/PLANTILLA_SOLICITUD_COMPROMISO_PRESUPUESTAL_COMPRA.docx');
            $template->setValue('CODIGOPROYECTO', $compromiso->Codigo);
            $template->setValue('CERTIFICACION', $certificacion->CodigoCertificacion);
            $template->setValue('MEMORANDO', $compromiso->Memorando  ."-".$compromiso->Annio);
            $template->setValue('EEA', $informacionGeneral->UnidadEjecutoraID);
            $template->setValue('REFERENCIA', $requerimiento->Referencia);
            $template->cloneRow('N', $countDetalles);
            $countDetalles=1;
            foreach($detalles as $detalle)
            {
                $orden=Orden::findOne($detalle->Correlativo);
                $template->setValue('N#'.$countDetalles.'', "".$countDetalles."");
                $template->setValue('PROVEEDOR#'.$countDetalles.'', "".ucwords(strtolower($orden->RazonSocial))."");
                $template->setValue('RUC#'.$countDetalles.'', "".ucwords(strtolower($orden->RUC))."");
                $template->setValue('MONTOS#'.$countDetalles.'', "".(($orden->Monto)?number_format(ucwords(strtolower($orden->Monto)), 2, '.', ' '):0)."");
                $countDetalles++;
            }
        }
        elseif($requerimiento->TipoRequerimiento==4)
        {
            $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/documentos/PLANTILLA_SOLICITUD_COMPROMISO_PRESUPUESTAL_VIATICO.docx');
            $template->setValue('CODIGOPROYECTO', $compromiso->Codigo);
            $template->setValue('CERTIFICACION', $certificacion->CodigoCertificacion);
            $template->setValue('MEMORANDO', $compromiso->Memorando  ."-".$compromiso->Annio);
            $template->setValue('EEA', $informacionGeneral->UnidadEjecutoraID);
            $template->setValue('ANIO', date('Y')-1);
            $template->setValue('REFERENCIA', $requerimiento->Referencia);
            $template->cloneRow('N', $countDetalles);
            $countDetalles=1;
            // echo '<pre>'; print_r($detalles); die;
            foreach($detalles as $detalle)
            {
                $viatico=Viatico::findOne($detalle->Correlativo);
                $template->setValue('N#'.$countDetalles.'', "".$countDetalles."");
                $template->setValue('COMISIONADO#'.$countDetalles.'', ucwords(strtolower($viatico->Apellido).' '.strtolower($viatico->Nombre)));
                $template->setValue('DNI#'.$countDetalles.'', "".ucwords(strtolower($viatico->Dni))."");
                $template->setValue('MONTO#'.$countDetalles.'', "".(($viatico->Monto)?number_format(ucwords(strtolower($viatico->Monto)),2,'.',' '):0)."");
                $countDetalles++;
            }
        }
        elseif($requerimiento->TipoRequerimiento==5)
        {
            $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/documentos/PLANTILLA_SOLICITUD_COMPROMISO_PRESUPUESTAL_APERTURA_CAJA.docx');
            $template->setValue('CODIGOPROYECTO', $compromiso->Codigo);
            $template->setValue('CERTIFICACION', $certificacion->CodigoCertificacion);
            $template->setValue('MEMORANDO', $compromiso->Memorando  ."-".$compromiso->Annio);
            $template->setValue('EEA', $informacionGeneral->UnidadEjecutoraID);
            $template->setValue('REFERENCIA', $requerimiento->Referencia);
            $template->cloneRow('N', $countDetalles);
            $countDetalles=1;
            foreach($detalles as $detalle)
            {
                $cajaChica=DetalleCajaChica::findOne($detalle->Correlativo);
                $template->setValue('N#'.$countDetalles.'', "".$countDetalles."");
                $template->setValue('RESPONSABLE#'.$countDetalles.'', "".$cajaChica->Responsable."");
                $template->setValue('BIENES#'.$countDetalles.'', "".(($cajaChica->Bienes)?number_format($cajaChica->Bienes, 2, '.', ' '):0)."");
                $template->setValue('SERVICIOS#'.$countDetalles.'', "".(($cajaChica->Servicios)?number_format($cajaChica->Servicios, 2, '.', ' '):0)."");
                //$template->setValue('PROVEEDOR#'.$countDetalles.'', "".ucwords(strtolower($orden->RazonSocial))."");
                //$template->setValue('RUC#'.$countDetalles.'', "".ucwords(strtolower($orden->RUC))."");
                //$template->setValue('MONTO#'.$countDetalles.'', "".ucwords(strtolower($orden->Monto))."");
                $countDetalles++;
            }
        }
        elseif($requerimiento->TipoRequerimiento==6)
        {
            $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/documentos/PLANTILLA_SOLICITUD_COMPROMISO_PRESUPUESTAL_ENCARGOS.docx');
            $template->setValue('CODIGOPROYECTO', $compromiso->Codigo);
            $template->setValue('CERTIFICACION', $certificacion->CodigoCertificacion);
            $template->setValue('MEMORANDO', $compromiso->Memorando  ."-".$compromiso->Annio);
            $template->setValue('EEA', $informacionGeneral->UnidadEjecutoraID);
            $template->setValue('REFERENCIA', $requerimiento->Referencia);
            $template->cloneRow('N', $countDetalles);
            $countDetalles=1;
            foreach($detalles as $detalle)
            {
                $encargo=Encargo::findOne($detalle->Correlativo);
                $template->setValue('N#'.$countDetalles.'', "".$countDetalles."");
                $template->setValue('RESPONSABLE#'.$countDetalles.'', "".$encargo->NombresEncargo." ".$encargo->ApellidosEncargo);
                $template->setValue('BIENES#'.$countDetalles.'', "".(($encargo->Bienes)?number_format($encargo->Bienes, 2, '.', ' '):0)."");
                $template->setValue('SERVICIOS#'.$countDetalles.'', "".(($encargo->Servicios)?number_format($encargo->Servicios, 2, '.', ' '):0)."");
                //$template->setValue('PROVEEDOR#'.$countDetalles.'', "".ucwords(strtolower($orden->RazonSocial))."");
                //$template->setValue('RUC#'.$countDetalles.'', "".ucwords(strtolower($orden->RUC))."");
                //$template->setValue('MONTO#'.$countDetalles.'', "".ucwords(strtolower($orden->Monto))."");
                $countDetalles++;
            }
        }
        $temp_file = tempnam(sys_get_temp_dir(), 'PHPWord');
        $template->saveAs($temp_file);
        
        $contentType = 'Content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document;';
        header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
        header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
        header ( "Cache-Control: no-cache, must-revalidate" );
        header ( "Pragma: no-cache" );
        header ( $contentType );
        header ( "Content-Disposition: attachment; filename='Compromiso.docx'");
        readfile($temp_file); // or echo file_get_contents($temp_file);
        unlink($temp_file);  // remove temp file
        return true; 
    }

    public function actionSituacionOrdenes(){
        $this->layout='vacio';
        $orden=Orden::findOne($_POST['ID']);
        $orden->SituacionSIAFT = 1;
        $orden->update();
        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
    public function actionExcel()
    {
        $resultados = (new \yii\db\Query())
            ->select('CompromisoPresupuestal.*')
            ->from('CompromisoPresupuestal')
            ->distinct()
            ->all();
        $compromisoPresupuestal=new CompromisoPresupuestal;
        return $this->render('excel',['resultados'=>$resultados,'compromisoPresupuestal'=>$compromisoPresupuestal]);
    }
    
    public function actionVer($ID=null)
    {
        $this->layout='estandar';
        $cabecera=CompromisoPresupuestal::find()->where(['ID'=>$ID])->one();
        $requerimiento=Requerimiento::findOne($cabecera->RequerimientoID);
        

        $dcompromiso=DetalleCompromisoPresupuestal::find()->where(["CompromisoPresupuestalID"=>$ID])->all();
        
        $observarJefeContabilidad=ObservacionGeneral::find()
                    ->where('CodigoID=:CodigoID and CodigoProyecto=:CodigoProyecto and TipoObservacion=19',[':CodigoID'=>$cabecera->ID,':CodigoProyecto'=>$cabecera->Codigo])
                    ->one();
        

        // $observarAdministrativo = "";
        // echo '<pre>';print_r($dcompromiso); die();
        $obsAdministrativo = array();
        foreach ($dcompromiso as $reque) {
            $observarAdministrativo = ObservacionGeneral::find()
            ->where('CodigoID=:CodigoID and CodigoProyecto=:CodigoProyecto and TipoObservacion=20',[':CodigoID'=>$reque->ID,':CodigoProyecto'=>$cabecera->Codigo])
            ->all();
            // echo '<pre>';print_r($observarAdministrativo); die;
            foreach ($observarAdministrativo as $adms ) {
                $obsAdministrativo[] = array(
                    'ID' => $reque->ID,
                    'CodigoProyecto' => $adms->CodigoProyecto,
                    'Observacion' => $adms->Observacion,
                    'Respuesta' => $adms->RespuestaObservacion
                );
            }
            // echo '<pre>';print_r($observarAdministrativo);
        }

        if($requerimiento->TipoRequerimiento==1 || $requerimiento->TipoRequerimiento==2 || $requerimiento->TipoRequerimiento==3)
        {
            // $resultados = (new \yii\db\Query())
            // ->from('DetalleCompromisoPresupuestal')
            // ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.ID=DetalleCompromisoPresupuestal.CompromisoPresupuestalID')
            // ->innerJoin('TipoGasto','TipoGasto.ID=DetalleCompromisoPresupuestal.TipoGasto')
            // ->innerJoin('Orden','Orden.ID=DetalleCompromisoPresupuestal.Correlativo')
            // ->where('DetalleCompromisoPresupuestal.CompromisoPresupuestalID=:CompromisoPresupuestalID',[':CompromisoPresupuestalID'=>$ID])
            // ->distinct()
            // ->all();

            $resultados = (new \yii\db\Query())
            ->select('DetalleCompromisoPresupuestal.ID,
                DetalleCompromisoPresupuestal.Correlativo,
                DetalleCompromisoPresupuestal.TipoGasto,
                DetalleCompromisoPresupuestal.GastoElegible,
                DetalleCompromisoPresupuestal.Observacion,
                DetalleCompromisoPresupuestal.CompromisoPresupuestalID,
                DetalleCompromisoPresupuestal.Situacion,
                CompromisoPresupuestal.Annio,
                CompromisoPresupuestal.RequerimientoID,
                Orden.ID OrdenID,
                Orden.Documento,
                Orden.SituacionSIAFT,
                Orden.FechaOrden,
                Orden.TipoProceso,
                Orden.Monto,
                TipoGasto.Nombre'
            )
            ->from('DetalleCompromisoPresupuestal')
            ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.ID=DetalleCompromisoPresupuestal.CompromisoPresupuestalID')
            ->innerJoin('UsuarioProyecto','UsuarioProyecto.CodigoProyecto=CompromisoPresupuestal.Codigo')
            ->innerJoin('Usuario','Usuario.ID=UsuarioProyecto.UsuarioID')
            ->innerJoin('TipoGasto','TipoGasto.ID=DetalleCompromisoPresupuestal.TipoGasto')
            ->innerJoin('Orden','Orden.ID=DetalleCompromisoPresupuestal.Correlativo')
            ->where(['DetalleCompromisoPresupuestal.CompromisoPresupuestalID'=>$ID])
            ->distinct()
            ->all(); 
            
        }
        elseif($requerimiento->TipoRequerimiento==4)
        {
            $resultados = (new \yii\db\Query())
            ->select('
                DetalleCompromisoPresupuestal.ID,
                DetalleCompromisoPresupuestal.Correlativo,
                DetalleCompromisoPresupuestal.TipoGasto,
                DetalleCompromisoPresupuestal.GastoElegible,
                DetalleCompromisoPresupuestal.Observacion,
                DetalleCompromisoPresupuestal.CompromisoPresupuestalID,
                DetalleCompromisoPresupuestal.Situacion,
                CompromisoPresupuestal.Annio,
                Viatico.ID ViaticoID,
                Viatico.Documento,
                CompromisoPresupuestal.RequerimientoID,
                Viatico.Apellido,
                Viatico.Dni,
                Viatico.Monto,
                Viatico.Nombre Nombres,
                TipoGasto.Nombre'
            )
            ->from('DetalleCompromisoPresupuestal')
            ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.ID=DetalleCompromisoPresupuestal.CompromisoPresupuestalID')
            ->innerJoin('UsuarioProyecto','UsuarioProyecto.CodigoProyecto=CompromisoPresupuestal.Codigo')
            ->innerJoin('Usuario','Usuario.ID=UsuarioProyecto.UsuarioID')
            ->innerJoin('TipoGasto','TipoGasto.ID=DetalleCompromisoPresupuestal.TipoGasto')
            ->innerJoin('Viatico','Viatico.ID=DetalleCompromisoPresupuestal.Correlativo')
            ->where(['DetalleCompromisoPresupuestal.CompromisoPresupuestalID'=>$ID])
            ->distinct()
            ->all(); 
        }
        elseif($requerimiento->TipoRequerimiento==5)
        {
            $resultados = (new \yii\db\Query())
            ->select('DetalleCompromisoPresupuestal.ID,
                DetalleCompromisoPresupuestal.Correlativo,
                DetalleCompromisoPresupuestal.TipoGasto,
                DetalleCompromisoPresupuestal.GastoElegible,
                DetalleCompromisoPresupuestal.Observacion,
                DetalleCompromisoPresupuestal.CompromisoPresupuestalID,
                DetalleCompromisoPresupuestal.Situacion,
                CompromisoPresupuestal.Annio,
                DetalleCajaChica.Documento,
                CompromisoPresupuestal.RequerimientoID,
                DetalleCajaChica.Tipo,
                DetalleCajaChica.Bienes,
                DetalleCajaChica.Servicios,
                DetalleCajaChica.ResolucionDirectorial,
                TipoGasto.Nombre'
            )
            ->from('DetalleCompromisoPresupuestal')
            ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.ID=DetalleCompromisoPresupuestal.CompromisoPresupuestalID')
            ->innerJoin('UsuarioProyecto','UsuarioProyecto.CodigoProyecto=CompromisoPresupuestal.Codigo')
            ->innerJoin('Usuario','Usuario.ID=UsuarioProyecto.UsuarioID')
            ->innerJoin('TipoGasto','TipoGasto.ID=DetalleCompromisoPresupuestal.TipoGasto')
            ->innerJoin('DetalleCajaChica','DetalleCajaChica.ID=DetalleCompromisoPresupuestal.Correlativo')
            ->where(['DetalleCompromisoPresupuestal.CompromisoPresupuestalID'=>$ID])
            ->distinct()
            ->all(); 
        }
        elseif($requerimiento->TipoRequerimiento==6)
        {
            $resultados = (new \yii\db\Query())
            ->select('DetalleCompromisoPresupuestal.ID,
                DetalleCompromisoPresupuestal.Correlativo,
                DetalleCompromisoPresupuestal.TipoGasto,
                DetalleCompromisoPresupuestal.GastoElegible,
                DetalleCompromisoPresupuestal.Observacion,
                DetalleCompromisoPresupuestal.CompromisoPresupuestalID,
                DetalleCompromisoPresupuestal.Situacion,
                CompromisoPresupuestal.Annio,
                Encargo.ID EncargoID, 
                Encargo.Documento,
                CompromisoPresupuestal.RequerimientoID,
                Encargo.Bienes,
                Encargo.Servicios,
                TipoGasto.Nombre'
            )

            ->from('DetalleCompromisoPresupuestal')
            ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.ID=DetalleCompromisoPresupuestal.CompromisoPresupuestalID')
            ->innerJoin('UsuarioProyecto','UsuarioProyecto.CodigoProyecto=CompromisoPresupuestal.Codigo')
            ->innerJoin('Usuario','Usuario.ID=UsuarioProyecto.UsuarioID')
            ->innerJoin('TipoGasto','TipoGasto.ID=DetalleCompromisoPresupuestal.TipoGasto')
            ->innerJoin('Encargo','Encargo.ID=DetalleCompromisoPresupuestal.Correlativo')
            ->where(['DetalleCompromisoPresupuestal.CompromisoPresupuestalID'=>$ID])
            ->distinct()
            ->all(); 
        }
        
            
        return $this->render('ver',['observarJefeContabilidad'=>$observarJefeContabilidad,'observarAdministrativo'=>$obsAdministrativo,'cabecera'=>$cabecera,'resultados'=>$resultados,'requerimiento'=>$requerimiento]);
    }
    public function actionEliminar($id=null)
    {
        $id = $_POST['ID'];
        $compromisoPresupuestal=CompromisoPresupuestal::findOne($id);
        $compromisoPresupuestal->Situacion=6;
        $compromisoPresupuestal->Estado=0;
        $compromisoPresupuestal->update();
        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
    public function actionUafsiObservacion($ID)
    {
        $this->layout='vacio';
        $compromisoPresupuestal=CompromisoPresupuestal::findOne($ID);
        
        if($compromisoPresupuestal->load(Yii::$app->request->post())){
            $compromisoPresupuestal->GastoElegible = 2;
            $compromisoPresupuestal->Situacion = 6;
            $compromisoPresupuestal->update();
            return $this->redirect(['/requerimiento/uafsi']);
        }
        
        return $this->render('uafsi-observacion',['compromisoPresupuestal'=>$compromisoPresupuestal]);
    }

    // Fuente Financiamiento 
        public function actionFuenteSecuencial(){
            // Yii::$app->tools->obtenerFuente();
        }
    
    // ANEXOS
        private function Anexos($Dni=null,$Nombres=null,$Ruc=null,$CodigoEjecutora=null,$TipoPersona=null,$Tipo=null){
            $siafAnual = \Yii::$app->db->createCommand('EXEC SP_ANEXOS :l_codigo, :l_descripcion, :l_ruc, :l_codiejec, :l_tipopersona, :l_tipo; ')
            ->bindValue(':l_codigo',          $Dni)
            ->bindValue(':l_descripcion',     $Nombres)
            ->bindValue(':l_ruc',             $Ruc)
            ->bindValue(':l_codiejec',        $CodigoEjecutora)
            ->bindValue(':l_tipopersona',     $TipoPersona)
            ->bindValue(':l_tipo',            $Tipo)
            ->execute();
            
            return 1;
        }

    // ANUAL
        public function actionGenerarCompromisoAnual()
        {
            // echo '<pre>';
            $CodigosIDs = explode(',',$_POST["CodigosIDs"]);
            // print_r($_POST);

            // die();
            
            if(isset($_POST["CodigoProyecto"]) && isset($_POST["ID"]) && isset($CodigosIDs))
            {
                $IDsGenerados="";
                for($i=0;$i<count($CodigosIDs); $i++)
                {
                    // echo $i;
                    if($i==0)
                    {
                        $IDsGenerados=$CodigosIDs[$i];
                    }
                    elseif($i>0)
                    {
                        $IDsGenerados=$IDsGenerados.",".$CodigosIDs[$i];
                    }
                }
                // echo $IDsGenerados; die();
                $CodigoProyecto=$_POST["CodigoProyecto"];
                $codeProyecto = explode('_',$CodigoProyecto)[0];
                $ID=$_POST["ID"];
                $compromiso = CompromisoPresupuestal::findOne($ID);
                $informacionGeneral = InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])->one();
                $proyecto = Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacionGeneral->InvestigadorID])->one();
                $pasoCritico = PasoCritico::find()->where('ProyectoID=:ProyectoID and Estado=1 and Situacion=2',[':ProyectoID'=>$proyecto->ID])->one();
                $MontoEjecutado=0;
                $requerimiento = Requerimiento::findOne($compromiso->RequerimientoID);
                $certificado = DetalleCertificacionPresupuestal::find()
                    ->innerJoin('CertificacionPresupuestal','CertificacionPresupuestal.ID=DetalleCertificacionPresupuestal.CertificacionPresupuestalID')
                    ->where('DetalleCertificacionPresupuestal.Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])
                    ->one();
                    
                $siafSecuencial = SiafSecuencialProyecto::find()
                    ->where('CodigoProyecto=:CodigoProyecto',[':CodigoProyecto'=>$CodigoProyecto])
                    ->one();

                $FuenteFinanciamiento = $_POST['fuenteFinanciamiento'];
                $CorrelativoFuente    = $_POST['CorrelativoFuente'];

                if($requerimiento->TipoRequerimiento==1 || $requerimiento->TipoRequerimiento==2 || $requerimiento->TipoRequerimiento==3) // Ordenes
                {
                    $detalles = (new \yii\db\Query())
                        ->select('*,Orden.Correlativo OrdenCorrelativo,Orden.ID OrdenID,Orden.RazonSocial')
                        ->from('CompromisoPresupuestal')
                        ->innerJoin('DetalleCompromisoPresupuestal','DetalleCompromisoPresupuestal.CompromisoPresupuestalID=CompromisoPresupuestal.ID')
                        ->innerJoin('Orden','Orden.ID=DetalleCompromisoPresupuestal.Correlativo')
                        ->where('CompromisoPresupuestal.ID=:ID and Orden.ID in ('.$IDsGenerados.')',[':ID'=>$ID])
                        ->distinct()
                        ->all();
                        
                    $contenido='';
                    foreach($detalles as $detalle)
                    {
                        //numregicab    es la PK de la tabla Integra papá
                        //clasif1      IN       VARCHAR2,
                        //monto1       IN       NUMBER,
                        //clasif2      IN       VARCHAR2,   si no envia es null
                        //monto2       IN       NUMBER,  si no envia es null
                        //motivo es concepto de la orden 
                        $orden=Orden::findOne($detalle["OrdenID"]);
                        $siaf=new Siaf;
                        $siafInterno = new Siaft;
                        
                        $siaf->SiafCertificado=str_pad($certificado->CodigoCertificacion, 10, "0", STR_PAD_LEFT);
                        $siaf->InterfaceCertificado=str_pad($siaf->InterfaceCertificado, 10, "0", STR_PAD_LEFT);
                        
                        $this->Anexos($detalle["RUC"],$detalle["RazonSocial"],$detalle["RUC"],NULL,'J','2');
                        // Fuente financiamiento
                        $siaf->FuenteFinanciamiento = $FuenteFinanciamiento;
                        $siaf->InterfaceSecuencial  = $CorrelativoFuente;

                        if($detalle["TipoGasto"]==1)
                        {
                            $siaf->CodigoDocumento="032";//tipodoc
                            $siaf->TipoID=1;
                            $siaf->NumeroDocumento= "F".str_pad($detalle["OrdenCorrelativo"], 3, "0", STR_PAD_LEFT).$detalle["Annio"].$codeProyecto;//numdoc
                            
                            // $siaf->FechaDocumento=date('d/m/Y',strtotime($detalle["FechaOrden"]));//fecha de movimiento
                            // if(date('m',strtotime($detalle["FechaOrden"])) == date('m')){
                            //     $siaf->FechaDocumento=date('d/m/Y',strtotime($detalle["FechaOrden"]));
                            // }else{
                            //     $siaf->FechaDocumento = '01/'.date('m').'/'.date('Y');
                            // }

                            if( Yii::$app->tools->dateFormat($detalle["FechaOrden"],'m') == date('m')){
                                $siaf->FechaDocumento=Yii::$app->tools->dateFormat($detalle["FechaOrden"],'d/m/Y');
                            }else{
                                $siaf->FechaDocumento = '01/'.date('m').'/'.date('Y');
                            }

                            $siaf->AnioDocumento=$detalle["Annio"];
                            $siaf->ConceptoDocumento=substr ( $detalle["Concepto"] , 0 ,249 );
                            $siaf->RUCDocumento=$detalle["RUC"];
                            $siaf->TipoCambioDocumento=$detalle["TipoCambio"];
                            $siaf->MontoTotal=$detalle["Monto"];
                            $siaf->Clasificador='26 7 1 6 3';
                            $siaf->MontoTotal1=NULL;
                            $siaf->Clasificador1=NULL;
                            $siaf->SiafSecuencial=str_pad($siafSecuencial->SecuencialOrdenServicioFactura, 4, "0", STR_PAD_LEFT);
                        }
                        elseif($detalle["TipoGasto"]==2)
                        {
                            $siaf->CodigoDocumento="032";
                            $siaf->TipoID=1;
                            $siaf->NumeroDocumento="H".str_pad($detalle["OrdenCorrelativo"], 3, "0", STR_PAD_LEFT).$detalle["Annio"].$codeProyecto;
                            // $siaf->FechaDocumento=date('d/m/Y',strtotime($detalle["FechaOrden"]));
                            // if(date('m',strtotime($detalle["FechaOrden"])) == date('m')){
                            //     $siaf->FechaDocumento=date('d/m/Y',strtotime($detalle["FechaOrden"]));
                            // }else{
                            //     $siaf->FechaDocumento = '01/'.date('m').'/'.date('Y');
                            // }

                            if( Yii::$app->tools->dateFormat($detalle["FechaOrden"],'m') == date('m')){
                                $siaf->FechaDocumento=Yii::$app->tools->dateFormat($detalle["FechaOrden"],'d/m/Y');
                            }else{
                                $siaf->FechaDocumento = '01/'.date('m').'/'.date('Y');
                            }

                            $siaf->AnioDocumento=$detalle["Annio"];
                            $siaf->ConceptoDocumento=substr ( $detalle["Concepto"] , 0 ,249 );
                            $siaf->RUCDocumento=$detalle["RUC"];
                            $siaf->TipoCambioDocumento=$detalle["TipoCambio"];
                            $siaf->MontoTotal=$detalle["Monto"];
                            $siaf->Clasificador='26 7 1 6 3';
                            $siaf->MontoTotal1=NULL;
                            $siaf->Clasificador1=NULL;
                            $siaf->SiafSecuencial=str_pad($siafSecuencial->SecuencialOrdenServicioRH, 4, "0", STR_PAD_LEFT);
                        }
                        if($detalle["TipoGasto"]==3)
                        {
                            $siaf->CodigoDocumento="031";
                            $siaf->TipoID=1;
                            $siaf->NumeroDocumento="C".str_pad($detalle["OrdenCorrelativo"], 3, "0", STR_PAD_LEFT).$detalle["Annio"].$codeProyecto;
                            // $siaf->FechaDocumento=date('d/m/Y',strtotime($detalle["FechaOrden"]));
                            // if(date('m',strtotime($detalle["FechaOrden"])) == date('m')){
                            //     $siaf->FechaDocumento=date('d/m/Y',strtotime($detalle["FechaOrden"]));
                            // }else{
                            //     $siaf->FechaDocumento = '01/'.date('m').'/'.date('Y');
                            // }

                            if( Yii::$app->tools->dateFormat($detalle["FechaOrden"],'m') == date('m')){
                                $siaf->FechaDocumento=Yii::$app->tools->dateFormat($detalle["FechaOrden"],'d/m/Y');
                            }else{
                                $siaf->FechaDocumento = '01/'.date('m').'/'.date('Y');
                            }

                            $siaf->AnioDocumento=$detalle["Annio"];
                            $siaf->ConceptoDocumento=substr ( $detalle["Concepto"] , 0 ,249 );
                            $siaf->RUCDocumento=$detalle["RUC"];
                            $siaf->TipoCambioDocumento=$detalle["TipoCambio"];
                            $siaf->MontoTotal=$detalle["Monto"];
                            $siaf->Clasificador='26 7 1 6 2';
                            $siaf->MontoTotal1=NULL;
                            $siaf->Clasificador1=NULL;
                            $siaf->SiafSecuencial=str_pad($siafSecuencial->SecuencialOrdenCompra, 4, "0", STR_PAD_LEFT);
                        }//Caja chica 942   //Viatico 043// Encargo 951
                        
                        $MontoEjecutado = $MontoEjecutado+$siaf->MontoTotal;
                        
                        $siaf->Correlativo=str_pad($detalle["OrdenCorrelativo"], 12, "0", STR_PAD_LEFT);
                        // $siaf->InterfaceSecuencial=$siaf->SiafCorrelativo;
                        $datosR = $this->OracleSiaftCompromisoAnual($siaf);
                        
                        //-----------
                            $siafInterno->Fase = 9; // Tabla Situacion ID
                            $siafInterno->Secuencial = $siafSecuencial->Secuencial+1;
                            $siafInterno->Fecha = date('Ymd');
                            $siafInterno->Monto = $detalle["Monto"];
                            $siafInterno->FechaProceso = date('Ymd',strtotime($detalle["FechaOrden"]));
                            $siafInterno->Estado = 1;
                            $siafInterno->CodigoProyecto = $CodigoProyecto;
                            $siafInterno->Orden = $detalle["OrdenID"];
                            $siafInterno->Moneda = 'S/';
                            $siafInterno->TipoGastoID = $detalle["TipoGasto"];
                            $siafInterno->Siaf = $datosR;
                            $siafInterno->InterfaceSecuencial  = $CorrelativoFuente;
                            $siafInterno->FuenteFinanciamiento = $FuenteFinanciamiento;
                            $siafInterno->save();
                        // ----------

                        $orden->SIAF=$datosR;
                        $orden->SituacionSIAFT = 2;
                        $orden->update();
                        $siafSecuencial->Secuencial=$siafSecuencial->Secuencial+1;
                    }
                    $siafSecuencial->update();
                }
                elseif($requerimiento->TipoRequerimiento==4) // Viatico
                {
                    $detalles = (new \yii\db\Query())
                        ->select('*,Viatico.Correlativo OrdenCorrelativo,Viatico.ID OrdenID,Viatico.TipoCambio')
                        ->from('CompromisoPresupuestal')
                        ->innerJoin('DetalleCompromisoPresupuestal','DetalleCompromisoPresupuestal.CompromisoPresupuestalID=CompromisoPresupuestal.ID')
                        ->innerJoin('Viatico','Viatico.ID=DetalleCompromisoPresupuestal.Correlativo')
                        ->where('CompromisoPresupuestal.ID=:ID and Viatico.ID in ('.$IDsGenerados.')',[':ID'=>$ID])
                        ->distinct()
                        ->all();
                        
                    $contenido='';
                    foreach($detalles as $detalle)
                    {
                        $siaf=new Siaf;
                        $siafInterno = new Siaft;
                        $this->Anexos($detalle["Dni"],$detalle["Nombre"].' '.$detalle["Apellido"],NULL,NULL,'N','1');
                        $siaf->InterfaceCertificado=str_pad($siaf->InterfaceCertificado, 10, "0", STR_PAD_LEFT);
                        $siaf->CodigoDocumento="043";

                        $siaf->FuenteFinanciamiento = $FuenteFinanciamiento;
                        $siaf->InterfaceSecuencial  = $CorrelativoFuente;

                        // $siaf->NumeroDocumento= trim(str_pad($detalle["OrdenCorrelativo"], 3, "0", STR_PAD_LEFT)."-".$detalle["Annio"].$codeProyecto);
                        $siaf->NumeroDocumento= trim("PV".date('Y').$codeProyecto.str_pad($detalle["OrdenCorrelativo"], 3, "0", STR_PAD_LEFT));
                        // $siaf->FechaDocumento=date('d/m/Y',strtotime($detalle["FechaCreacion"]));
                        // Yii::$app->tools->dateFormat($detalle["FechaCreacion"],'m');
                        if( Yii::$app->tools->dateFormat($detalle["FechaCreacion"],'m') == date('m')){
                            $siaf->FechaDocumento=Yii::$app->tools->dateFormat($detalle["FechaCreacion"],'d/m/Y');
                        }else{
                            $siaf->FechaDocumento = '01/'.date('m').'/'.date('Y');
                        }
                        $siaf->AnioDocumento=$detalle["Annio"];
                        $siaf->ConceptoDocumento=substr ( $detalle["PlanTrabajo"] , 0 ,249 );
                        $siaf->TipoID=9;
                        $siaf->RUCDocumento='           ';
                        
                        $siaf->TipoCambioDocumento=$detalle["TipoCambio"];
                        $siaf->SiafCertificado=str_pad($certificado->CodigoCertificacion, 10, "0", STR_PAD_LEFT);
                        $siaf->SiafSecuencial=str_pad($siafSecuencial->SecuencialViatico, 4, "0", STR_PAD_LEFT);

                        $siaf->MontoTotal=$detalle["Monto"];
                        $siaf->Clasificador='26 7 1 6 3';
                        $siaf->MontoTotal1=NULL;
                        $siaf->Clasificador1=NULL;
                        $MontoEjecutado = $MontoEjecutado+$siaf->MontoTotal;

                        $datosR = $this->OracleSiaftCompromisoAnual($siaf);

                        $siafInterno->Fase = 9;
                        $siafInterno->Secuencial = $siafSecuencial->SecuencialViatico+1;
                        $siafInterno->Fecha = date('Ymd');
                        $siafInterno->Monto = $detalle["Monto"];
                        $siafInterno->FechaProceso = date('Ymd',strtotime($detalle["FechaCreacion"]));
                        $siafInterno->Estado = 1;
                        $siafInterno->CodigoProyecto = $CodigoProyecto;
                        $siafInterno->Orden = $detalle["OrdenID"];
                        $siafInterno->Moneda = 'S/';
                        $siafInterno->TipoGastoID = $detalle["TipoGasto"];
                        $siafInterno->Siaf = $datosR;
                        $siafInterno->InterfaceSecuencial  = $CorrelativoFuente;
                        $siafInterno->FuenteFinanciamiento = $FuenteFinanciamiento;

                        $siafInterno->save();
                        $siafSecuencial->SecuencialViatico=$siafSecuencial->SecuencialViatico+1;
                    }
                    $siafSecuencial->update();

                    $viatico=Viatico::find()->where('RequerimientoID=:RequerimientoID and Estado=1 and Situacion=1',[':RequerimientoID'=>$requerimiento->ID])->one();
                    if($viatico)
                    {
                        $viatico->Situacion=3;
                        $viatico->update();
                    }
                }
                elseif($requerimiento->TipoRequerimiento==5) // Caja Chica
                {
                    $detalles = (new \yii\db\Query())
                        ->select('*,DetalleCajaChica.Correlativo OrdenCorrelativo,DetalleCajaChica.ID OrdenID,DetalleCajaChica.TipoCambio')
                        ->from('CompromisoPresupuestal')
                        ->innerJoin('DetalleCompromisoPresupuestal','DetalleCompromisoPresupuestal.CompromisoPresupuestalID=CompromisoPresupuestal.ID')
                        ->innerJoin('DetalleCajaChica','DetalleCajaChica.ID=DetalleCompromisoPresupuestal.Correlativo')
                        ->where('CompromisoPresupuestal.ID=:ID',[':ID'=>$ID])
                        ->distinct()
                        ->one();
                    $detallecajaChica=DetalleCajaChica::find()->where('RequerimientoID=:RequerimientoID and Estado=1 and Situacion=1',[':RequerimientoID'=>$requerimiento->ID])->one();
                    
                    if($detallecajaChica)
                    {
                        $siaf=new Siaf;
                        $siafInterno = new Siaft;
                        //$this->Anexos($detalle["Dni"],$detalle["Nombre"].' '.$detalle["Apellido"],NULL,NULL);
                        
                        // if($CodigoProyecto == '065_PI'){
                        //     $siaf->FuenteFinanciamiento='00';
                        //     $siaf->InterfaceSecuencial="0054";
                        // }
                        $siaf->FuenteFinanciamiento = $FuenteFinanciamiento;
                        $siaf->InterfaceSecuencial  = $CorrelativoFuente;

                        if($detallecajaChica->Tipo==1)
                        {
                            $cajaChica=CajaChica::findOne($detallecajaChica->CajaChicaID);
                            $cajaChica->Situacion=3;
                            $cajaChica->update();
                        }
                        elseif($detallecajaChica->Tipo==2)
                        {
                            $detallecajaChicaAnt=DetalleCajaChica::find()->where('CajaChicaID=:CajaChicaID and Estado=1 and Situacion=3',[':CajaChicaID'=>$detallecajaChica->CajaChicaID])->one();
                            $detallecajaChicaAnt->Situacion=8;
                            $detallecajaChicaAnt->update();
                            $cajaChica=CajaChica::findOne($detallecajaChica->CajaChicaID);
                            $cajaChica->SaldoActualBienes=$cajaChica->SaldoActualBienes+$detallecajaChica->Bienes;
                            $cajaChica->SaldoActualServicios=$cajaChica->SaldoActualServicios+$detallecajaChica->Servicios;
                            $cajaChica->update();
                        }
                        $this->Anexos($cajaChica->Dni,$detalles["Responsable"],NULL,NULL,'N','1');//Revisión
                        
                        $siaf->SiafCertificado=str_pad($certificado->CodigoCertificacion, 10, "0", STR_PAD_LEFT);
                        $siaf->InterfaceCertificado=str_pad($siaf->InterfaceCertificado, 10, "0", STR_PAD_LEFT);
                        $siaf->CodigoDocumento="234";//234
                        $siaf->TipoID=1;
                        $siaf->NumeroDocumento=str_pad($detalles["OrdenCorrelativo"], 3, "0", STR_PAD_LEFT).$detalles["Annio"].$codeProyecto;
                        // $siaf->FechaDocumento=date('d/m/Y',strtotime($detalles["FechaRegistro"]));
                        // if(date('m',strtotime($detalles["FechaRegistro"])) == date('m')){
                        //     $siaf->FechaDocumento=date('d/m/Y',strtotime($detalles["FechaRegistro"]));
                        // }else{
                        //     $siaf->FechaDocumento = '01/'.date('m').'/'.date('Y');
                        // }

                        if( Yii::$app->tools->dateFormat($detalle["FechaRegistro"],'m') == date('m')){
                            $siaf->FechaDocumento=Yii::$app->tools->dateFormat($detalle["FechaRegistro"],'d/m/Y');
                        }else{
                            $siaf->FechaDocumento = '01/'.date('m').'/'.date('Y');
                        }

                        $siaf->AnioDocumento=$detalles["Annio"];
                        $siaf->ConceptoDocumento=substr ( $detalles["Descripcion"] , 0 ,249 );
                        $siaf->RUCDocumento='           ';
                        $siaf->TipoCambioDocumento=$detalles["TipoCambio"];
                        $siaf->MontoTotal=$detalles["Servicios"];
                        $siaf->Clasificador='26 7 1 6 3';
                        $siaf->MontoTotal1=$detalles["Bienes"];
                        $siaf->Clasificador1='26 7 1 6 2';
                        
                        // $siaf->FuenteFinanciamiento='00';
                        // $siaf->InterfaceSecuencial="0014";

                        $siaf->SiafSecuencial=str_pad($siafSecuencial->SecuencialCajaChica, 4, "0", STR_PAD_LEFT);
                        $MontoEjecutado=$MontoEjecutado+$siaf->MontoTotal;
                        // echo '<pre>'; print_r($siaf); die();
                        
                        $datosR = $this->OracleSiaftCompromisoAnual($siaf);

                        // -----------------------------------
                            $siafInterno->Fase = 9;
                            $siafInterno->Secuencial = $siafSecuencial->SecuencialCajaChica+1;
                            $siafInterno->Fecha = date('Ymd');
                            $siafInterno->Monto = $detalles["Bienes"] + $detalles["Servicios"];
                            $siafInterno->FechaProceso = date('Ymd',strtotime($detalles["FechaRegistro"]));
                            $siafInterno->Estado = 1;
                            $siafInterno->CodigoProyecto = $CodigoProyecto;
                            $siafInterno->Orden = $detalles["OrdenID"];
                            $siafInterno->Moneda = 'S/';
                            $siafInterno->TipoGastoID = $detalles["TipoGasto"];
                            $siafInterno->Siaf = $datosR;
                            $siafInterno->InterfaceSecuencial  = $CorrelativoFuente;
                            $siafInterno->FuenteFinanciamiento = $FuenteFinanciamiento;
                            $siafInterno->save();
                        // ---------------------------------
                        $siafSecuencial->SecuencialCajaChica=$siafSecuencial->SecuencialCajaChica+1;
                        $siafSecuencial->update();
                            
                        $detallecajaChica->Situacion=3;
                        $detallecajaChica->update();
                    }
                }
                elseif($requerimiento->TipoRequerimiento==6) // Encargos
                {
                    $detalles = (new \yii\db\Query())
                        ->select('*,Encargo.Correlativo EncargoCorrelativo,
                            Encargo.ID OrdenID, Encargo.FechaRegistro FechaReg,Encargo.Annio Anio,
                            Encargo.Bienes,Encargo.Servicios,Encargo.TipoCambio')
                        ->from('CompromisoPresupuestal')
                        ->innerJoin('DetalleCompromisoPresupuestal','DetalleCompromisoPresupuestal.CompromisoPresupuestalID=CompromisoPresupuestal.ID')
                        ->innerJoin('Encargo','Encargo.ID=DetalleCompromisoPresupuestal.Correlativo')
                        ->where('CompromisoPresupuestal.ID=:ID and Encargo.ID in ('.$IDsGenerados.') ',[':ID'=>$ID])
                        ->distinct()
                        ->all();
                    foreach($detalles as $detalle)
                    {
                        $siaf=new Siaf;
                        $siafInterno = new Siaft;
                        $this->Anexos($detalle["DNIEncargo"],$detalle["NombresEncargo"].' '.$detalle["ApellidosEncargo"],NULL,NULL,'N','1');

                        $siaf->FuenteFinanciamiento = $FuenteFinanciamiento;
                        $siaf->InterfaceSecuencial  = $CorrelativoFuente;

                        $siaf->SiafCertificado=str_pad($certificado->CodigoCertificacion, 10, "0", STR_PAD_LEFT);
                        $siaf->InterfaceCertificado=str_pad($siaf->InterfaceCertificado, 10, "0", STR_PAD_LEFT);
                        $siaf->CodigoDocumento="234";
                        $siaf->TipoID=9;
                        $siaf->NumeroDocumento= trim("E".str_pad($detalle["EncargoCorrelativo"], 3, "0", STR_PAD_LEFT).$detalle["Annio"].$codeProyecto);
                        // $siaf->NumeroDocumento= trim(str_pad($detalle["OrdenCorrelativo"], 3, "0", STR_PAD_LEFT).$detalle["Anio"].$codeProyecto);
                        // $siaf->FechaDocumento=date('d/m/Y',strtotime($detalle["FechaReg"]));
                        // if(date('d/m/Y',strtotime($detalle["FechaReg"])) == date('m')){
                        //     $siaf->FechaDocumento=date('d/m/Y',strtotime($detalle["FechaReg"]));
                        // }else{
                        //     $siaf->FechaDocumento = '01/'.date('m').'/'.date('Y');
                        // }

                        if( Yii::$app->tools->dateFormat($detalle["FechaReg"],'m') == date('m')){
                            $siaf->FechaDocumento=Yii::$app->tools->dateFormat($detalle["FechaReg"],'d/m/Y');
                        }else{
                            $siaf->FechaDocumento = '01/'.date('m').'/'.date('Y');
                        }

                        
                        $siaf->AnioDocumento=$detalle["Anio"];
                        $siaf->ConceptoDocumento=substr ( $detalle["DescripcionActividad"] , 0 ,249 );
                        $siaf->RUCDocumento= '           ';
                        $siaf->TipoCambioDocumento=$detalle["TipoCambio"];
                        
                        if($detalle["Bienes"] && $detalle["Servicios"]==0)
                        {
                            $siaf->MontoTotal=$detalle["Bienes"];
                            $siaf->Clasificador='26 7 1 6 2';
                            $siaf->MontoTotal1=NULL;
                            $siaf->Clasificador1=NULL;
                        }
                        elseif($detalle["Servicios"] && $detalle["Bienes"]==0)
                        {
                            $siaf->MontoTotal=$detalle["Servicios"];
                            $siaf->Clasificador='26 7 1 6 3';
                            $siaf->MontoTotal1=NULL;
                            $siaf->Clasificador1=NULL;
                        }
                        elseif($detalle["Bienes"] && $detalle["Servicios"])
                        {
                            $siaf->MontoTotal=$detalle["Bienes"];
                            $siaf->Clasificador='26 7 1 6 2';
                            $siaf->MontoTotal1=$detalle["Servicios"];
                            $siaf->Clasificador1='26 7 1 6 3';
                        }
                        //var_dump($siaf->Clasificador1);die;
                        $siaf->SiafSecuencial=str_pad($siafSecuencial->SecuencialEncargo, 4, "0", STR_PAD_LEFT);
                        $MontoEjecutado=$MontoEjecutado+$siaf->MontoTotal;
                        $datosR = $this->OracleSiaftCompromisoAnual($siaf);
                        $siafInterno->Fase = 9;
                        $siafInterno->Secuencial = $siafSecuencial->SecuencialEncargo+1;
                        $siafInterno->Fecha = date('Ymd');
                        $siafInterno->Monto = $detalle["Bienes"] + $detalle["Servicios"];
                        $siafInterno->FechaProceso = date('Ymd',strtotime($detalle["FechaReg"]));
                        $siafInterno->Estado = 1;
                        $siafInterno->CodigoProyecto = $CodigoProyecto;
                        $siafInterno->Orden = $detalle["OrdenID"];
                        $siafInterno->Moneda = 'S/';
                        $siafInterno->TipoGastoID = $detalle["TipoGasto"];
                        $siafInterno->Siaf = $datosR;
                        $siafInterno->InterfaceSecuencial  = $CorrelativoFuente;
                        $siafInterno->FuenteFinanciamiento = $FuenteFinanciamiento;
                        $siafInterno->save();
                        $siafSecuencial->SecuencialEncargo=$siafSecuencial->SecuencialEncargo+1;
                    }
                    $siafSecuencial->update();
                    $encargo=Encargo::find()->where('RequerimientoID=:RequerimientoID and Estado=1 and Situacion=1',[':RequerimientoID'=>$requerimiento->ID])->one();
                    if($encargo)
                    {
                        $encargo->Situacion=3;
                        $encargo->update();
                    }
                }
                $pasoCritico->update();
                $compromiso->CompromisoAnual=1;
                $compromiso->update();
                
                return $this->redirect(['compromiso-presupuestal/detalle-especialista-administrativo','id'=>$ID]);
            }else{
                return $this->redirect(['compromiso-presupuestal/especialista-administrativo']);
            }

        }

        public function actionEliminarCompromisoAnual()
        {
            if(isset($_POST["CodigoProyecto"]) && isset($_POST["ID"]) && isset($_POST["Codigos"]) && isset($_POST['TipoGasto']))
            {
                //var_dump($_POST["Codigos"]);die;
                $CompromisoAnual=CompromisoPresupuestal::findOne($_POST["ID"]);
                $CompromisoAnual->CompromisoAnual=NULL;
                $CompromisoAnual->update();
                for($i=0;$i<count($_POST["Codigos"]);$i++)
                {
                    \Yii::$app->db->createCommand()->delete('Siaft', ['Fase'=>9,'Orden' => $_POST["Codigos"][$i],'TipoGastoID'=>$_POST['TipoGasto']])->execute();
                }
                
                //$siafs=Siaft::find()->where('TipoGastoID=:TipoGastoID and Orden in ('.$_POST["Codigos"].')',[':TipoGastoID'=>$_POST['TipoGasto']])->all();
                
            }
        }
        
        private function OracleSiaftCompromisoAnual($siaf){
            // echo '<pre>'  ;
            // print_r($siaf);
            // die();
            $siafAnual = \Yii::$app->db->createCommand('BEGIN DECLARE @a VARCHAR(255); EXEC SP_COMPROMISO_ANUAL :CodigoDocumento, :NumeroDocumento, :FechaDocumento, :AnioDocumento,:ConceptoDocumento, :TipoID, :RUCDocumento, :FuenteFinanciamiento, :TipoCambioDocumento,:SiafCertificado,:SiafSecuencial,:SiafCorrelativo,:SiafCertificadox,:InterfaceSecuencial,:Numeracion,:TipoFinanciamiento,:Correlativo,:MetaFinanciera,:MontoTotal,:Clasificador,:MontoTotal1,:Clasificador1,@a output;select @a as CorrelativoSiaf end;')
            ->bindValue(':CodigoDocumento',      $siaf->CodigoDocumento)
            ->bindValue(':NumeroDocumento',      $siaf->NumeroDocumento)
            ->bindValue(':FechaDocumento',       $siaf->FechaDocumento)
            ->bindValue(':AnioDocumento',        $siaf->AnioDocumento)
            ->bindValue(':ConceptoDocumento',    $siaf->ConceptoDocumento)
            ->bindValue(':TipoID',               $siaf->TipoID)
            ->bindValue(':RUCDocumento',         $siaf->RUCDocumento)
            ->bindValue(':FuenteFinanciamiento', $siaf->FuenteFinanciamiento)
            ->bindValue(':TipoCambioDocumento',  $siaf->TipoCambioDocumento)
            ->bindValue(':SiafCertificado',      $siaf->SiafCertificado)
            ->bindValue(':SiafSecuencial',       $siaf->SiafSecuencial)
            ->bindValue(':SiafCorrelativo',      $siaf->SiafCorrelativo)
            ->bindValue(':SiafCertificadox',     $siaf->SiafCertificado)
            ->bindValue(':InterfaceSecuencial',  $siaf->InterfaceSecuencial)
            ->bindValue(':Numeracion',           '0001')
            ->bindValue(':TipoFinanciamiento',   $siaf->TipoFinanciamiento)
            ->bindValue(':Correlativo',          $siaf->Correlativo)
            ->bindValue(':MetaFinanciera',       $siaf->MetaFinanciera)
            ->bindValue(':MontoTotal',           $siaf->MontoTotal)
            ->bindValue(':Clasificador',         $siaf->Clasificador)
            ->bindValue(':MontoTotal1',           $siaf->MontoTotal1)
            ->bindValue(':Clasificador1',         $siaf->Clasificador1)
            ->queryOne();
           
           if($siafAnual){
                return $siafAnual['CorrelativoSiaf'];
           }else{
                return 0;
           }
        }

        public function actionFinanciamiento($ID =null,$CodigoProyecto = null,$CodigosIDs = null){

            $ID             = $_POST['ID'];
            $CodigoProyecto = $_POST['CodigoProyecto'];
            $CodigosIDs     = $_POST['CodigosIDs'];
            $tempCode       = '';
            foreach ($CodigosIDs as $codes) {
                $tempCode .= $codes.',';
            }
            $tempCode = substr($tempCode, 0, -1);

            $this->layout='vacio';
            $compromiso = CompromisoPresupuestal::findOne($ID);
            $certificacion = Yii::$app->tools->ObtenerCertificacion($CodigoProyecto)->CodigoCertificacion;
            $CodeCertificacion = str_pad($certificacion, 10, "0", STR_PAD_LEFT);
            $annioEjec = $compromiso->Annio;
            $fuente = Yii::$app->administrativo->obtenerFuente($CodeCertificacion,$annioEjec);
            return $this->render('administrativo/_financiamiento',['ID'=>$ID,'CodigoProyecto'=>$CodigoProyecto,'Fuente'=>$fuente,'Certificacion'=>$CodeCertificacion,'Annio'=>$annioEjec,'CodigosIDs'=>$tempCode]);
        }

        public function actionFinanciamientoFuenteSaldo($certificacion =null,$annio = null,$fuente= null){
            $this->layout='vacio';
            $fuente = Yii::$app->administrativo->obtenerFuente($certificacion,$annio,$fuente);
            // print_r($fuente);
            // die();
            $saldoTotal = 0;
            foreach ($fuente as $roo) {
                $secuenciaTemp[] = array('Secuencial' => $roo['SECUENCIA'] );
                $saldoTotal = $saldoTotal+$roo['SALDO'];
            }

            $saldoFinal = array('Total' => $saldoTotal,'Secuencial'=>$secuenciaTemp);
            echo json_encode($saldoFinal);
        }

    // Administrativo    
        private function OracleSiaftCompromisoAdministrativo($siaf){

            $siafAnual = \Yii::$app->db->createCommand('BEGIN DECLARE @a VARCHAR(255); EXEC SP_COMPROMISO_ADMINISTRATIVO :Anio, :Mes, :RUCDocumento, :Secuencia,:TipoDoc, :NumDoc, :FechDoc, :Motivo, :MontoTotal,:MontoIgv,:Clasificador1,:Monto1,:Clasificador2,:Monto2,:Poa,@a output;select @a as ExpedienteSiaf end;')
            // \Yii::$app->db->createCommand("begin declare @a varchar(255);EXEC [dbo].SP_COMPROMISO_ANUAL '$siaf->CodigoDocumento', '$siaf->NumeroDocumento', '$siaf->FechaDocumento','$siaf->AnioDocumento','$siaf->ConceptoDocumento','$siaf->TipoID','$siaf->RUCDocumento','$siaf->FuenteFinanciamiento','$siaf->TipoCambioDocumento','$siaf->SiafCertificado','$siaf->SiafSecuencial','$siaf->SiafCorrelativo','$siaf->SiafCertificado','$siaf->InterfaceSecuencial','0001','$siaf->TipoFinanciamiento','$siaf->Correlativo','$siaf->MetaFinanciera',$siaf->MontoTotal,'$siaf->Clasificador',@a output;select @a as CorrelativoSiaf end;")->queryOne();
           ->bindValue(':Anio',        $siaf->Anio)
           ->bindValue(':Mes'          ,        $siaf->Mes)
           ->bindValue(':RUCDocumento',         $siaf->RUCDocumento)
           ->bindValue(':Secuencia',         $siaf->Secuencia)
           ->bindValue(':TipoDoc',      $siaf->TipoDoc)//tipo documento
           ->bindValue(':NumDoc',      $siaf->NumeroDocumento)
           ->bindValue(':FechDoc',       $siaf->FechaDocumento)
           ->bindValue(':Motivo',    $siaf->ConceptoDocumento)
           ->bindValue(':MontoTotal',           (float)$siaf->MontoTotal)
           ->bindValue(':MontoIgv',  (float)$siaf->MontoIGV)
           ->bindValue(':Clasificador1',         $siaf->Clasificador)
           ->bindValue(':Monto1',           (float)$siaf->MontoTotal)
           ->bindValue(':Clasificador2',         $siaf->Clasificador1)
           ->bindValue(':Monto2',           (float)$siaf->MontoTotal1)
           ->bindValue(':Poa',           $siaf->Poa)
           ->queryOne();
           if($siafAnual){
                return $siafAnual['ExpedienteSiaf'];
           }else{
                return 0;
           }
        }

        public function actionEliminarCompromisoMensualizado()
        {
            if(isset($_POST["CodigoProyecto"]) && isset($_POST["ID"]) && isset($_POST["Codigos"]) && isset($_POST['TipoGasto']))
            {
                $CompromisoAnual=CompromisoPresupuestal::findOne($_POST["ID"]);
                $CompromisoAnual->CompromisoAdministrativo=NULL;
                $CompromisoAnual->update();
                
                
                for($i=0;$i<count($_POST["Codigos"]);$i++)
                {
                    \Yii::$app->db->createCommand()->delete('Siaft', ['Fase'=>10,'Orden' => $_POST["Codigos"][$i],'TipoGastoID'=>$_POST['TipoGasto']])->execute();
                }
                
                //$siafs=Siaft::find()->where('TipoGastoID=:TipoGastoID and Orden in ('.$_POST["Codigos"].')',[':TipoGastoID'=>$_POST['TipoGasto']])->all();
                
            }
        }

        public function actionGenerarAdministrativo()
        {
            if(isset($_POST["CodigoProyecto"]) && isset($_POST["ID"]) && isset($_POST["CodigosIDs"]))
            {
                $IDsGenerados="";
                for($i=0;$i<count($_POST["CodigosIDs"]); $i++)
                {
                    if($i==0)
                    {
                        $IDsGenerados=$_POST["CodigosIDs"][$i];
                    }
                    elseif($i>0)
                    {
                        $IDsGenerados=$IDsGenerados.",".$_POST["CodigosIDs"][$i];
                    }
                }
                
                $CodigoProyecto=$_POST["CodigoProyecto"];
                $codeProyecto = explode('_',$CodigoProyecto)[0];
                $ID=$_POST["ID"];
                $compromiso=CompromisoPresupuestal::findOne($ID);
                $compromiso->CompromisoAdministrativo=1;
                $compromiso->update();
                $requerimiento=Requerimiento::findOne($compromiso->RequerimientoID);
                $detallesCompromisos=DetalleCompromisoPresupuestal::find()->where('GastoElegible=1 and CompromisoPresupuestalID=:CompromisoPresupuestalID',[':CompromisoPresupuestalID'=>$ID])->all();
                $siaf=new Siaf;
                
                $informacionGeneral = InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])->one();
                $proyecto = Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacionGeneral->InvestigadorID])->one();
                $pasoCritico = PasoCritico::find()->where('ProyectoID=:ProyectoID and Estado=1 and Situacion=2',[':ProyectoID'=>$proyecto->ID])->one();
                $MontoEjecutado=0;
                $requerimiento = Requerimiento::findOne($compromiso->RequerimientoID);

                $requeComprometido = Requerimiento::find()
                    ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.RequerimientoID = Requerimiento.ID')
                    ->where(['Requerimiento.ID'=>$compromiso->RequerimientoID])
                    ->one();

                $certificado = DetalleCertificacionPresupuestal::find()
                    ->innerJoin('CertificacionPresupuestal','CertificacionPresupuestal.ID=DetalleCertificacionPresupuestal.CertificacionPresupuestalID')
                    ->where('DetalleCertificacionPresupuestal.Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])
                    ->one();

                $siafSecuencial = SiafSecuencialProyecto::find()
                    ->where('CodigoProyecto=:CodigoProyecto',[':CodigoProyecto'=>$CodigoProyecto])
                    ->one();
                if($requerimiento->TipoRequerimiento==1 || $requerimiento->TipoRequerimiento==2 || $requerimiento->TipoRequerimiento==3) // Ordenes
                {
                    $detalles = (new \yii\db\Query())
                        ->select('*,Orden.Correlativo OrdenCorrelativo,Orden.ID OrdenID')
                        ->from('CompromisoPresupuestal')
                        ->innerJoin('DetalleCompromisoPresupuestal','DetalleCompromisoPresupuestal.CompromisoPresupuestalID=CompromisoPresupuestal.ID')
                        ->innerJoin('Orden','Orden.ID=DetalleCompromisoPresupuestal.Correlativo')
                        ->where('CompromisoPresupuestal.ID=:ID and Orden.ID in ('.$IDsGenerados.')',[':ID'=>$ID])
                        ->distinct()
                        ->all();
                        
                    $contenido='';
                    foreach($detalles as $detalle)
                    {
                        $orden=Orden::findOne($detalle["OrdenID"]);
                        $siaf=new Siaf;
                        
                        if($CodigoProyecto == '065_PI'){
                            $siaf->FuenteFinanciamiento='00';
                            $siaf->InterfaceSecuencial="0054";
                        }

                        $siaf->SiafCertificado=str_pad($certificado->CodigoCertificacion, 10, "0", STR_PAD_LEFT);
                        $siaf->InterfaceCertificado=str_pad($siaf->InterfaceCertificado, 10, "0", STR_PAD_LEFT);
                        if($detalle["TipoGasto"]==1)
                        {
                            $siaf->Anio=$detalle["Annio"];
                            $siaf->Mes=date('m');
                            $siaf->RUCDocumento=$detalle["RUC"];
                            $siaf->Secuencia=$detalle["SIAF"];
                            $siaf->TipoDoc="032";//tipodoc
                            
                            $siaf->NumeroDocumento="F".str_pad($detalle["OrdenCorrelativo"], 3, "0", STR_PAD_LEFT).$detalle["Annio"].$codeProyecto;//numdoc// mensual
                            
                            // $siaf->FechaDocumento=date('d/m/Y',strtotime($detalle["FechaOrden"]));//fecha de movimiento
                            if(date('m',strtotime($detalle["FechaOrden"])) == date('m')){
                                $siaf->FechaDocumento= Yii::$app->tools->dateFormat($detalle["FechaOrden"],'d/m/Y');
                            }else{
                                $siaf->FechaDocumento = '01/'.date('m').'/'.date('Y');
                            }
                            $siaf->ConceptoDocumento=substr ( $detalle["Concepto"] , 0 ,249 );
                            $siaf->MontoTotal=$detalle["Monto"];
                            $siaf->MontoIGV=$detalle["Monto"];
                            $siaf->Clasificador='26 7 1 6 3';
                            $siaf->MontoTotal=$detalle["Monto"];
                            $siaf->Clasificador1='';
                            $siaf->MontoTotal1='';
                            $siaf->Poa=$certificado->CodigoPoaPNIA;
                        }
                        elseif($detalle["TipoGasto"]==2)
                        {
                            $siaf->Anio=$detalle["Annio"];
                            $siaf->Mes=date('m');
                            $siaf->RUCDocumento=$detalle["RUC"];
                            $siaf->Secuencia=$detalle["SIAF"];
                            $siaf->TipoDoc="032";//tipodoc
                            $siaf->NumeroDocumento="H".str_pad($detalle["OrdenCorrelativo"], 3, "0", STR_PAD_LEFT).$detalle["Annio"].$codeProyecto;//numdoc
                            
                            // $siaf->FechaDocumento=date('d/m/Y',strtotime($detalle["FechaOrden"]));//fecha de movimiento
                            if(date('m',strtotime($detalle["FechaOrden"])) == date('m')){
                                $siaf->FechaDocumento= Yii::$app->tools->dateFormat($detalle["FechaOrden"],'d/m/Y');
                            }else{
                                $siaf->FechaDocumento = '01/'.date('m').'/'.date('Y');
                            }
                            $siaf->ConceptoDocumento=substr ( $detalle["Concepto"] , 0 ,249 );
                            $siaf->MontoTotal=$detalle["Monto"];
                            $siaf->MontoIGV=$detalle["Monto"];
                            $siaf->Clasificador='26 7 1 6 3';
                            $siaf->MontoTotal=$detalle["Monto"];
                            $siaf->Clasificador1='';
                            $siaf->MontoTotal1='';
                            $siaf->Poa=$certificado->CodigoPoaPNIA;
                        }
                        if($detalle["TipoGasto"]==3)
                        {
                            $siaf->Anio=$detalle["Annio"];
                            $siaf->Mes=date('m');
                            $siaf->RUCDocumento=$detalle["RUC"];
                            $siaf->Secuencia=$detalle["SIAF"];
                            $siaf->TipoDoc="031";//tipodoc
                            $siaf->NumeroDocumento="C".str_pad($detalle["OrdenCorrelativo"], 3, "0", STR_PAD_LEFT).$detalle["Annio"].$codeProyecto;//numdoc
                            
                            // $siaf->FechaDocumento=date('d/m/Y',strtotime($detalle["FechaOrden"]));//fecha de movimiento
                            if(date('m',strtotime($detalle["FechaOrden"])) == date('m')){
                                $siaf->FechaDocumento = Yii::$app->tools->dateFormat($detalle["FechaOrden"],'d/m/Y');
                            }else{
                                $siaf->FechaDocumento = '01/'.date('m').'/'.date('Y');
                            }
                            $siaf->ConceptoDocumento=substr ( $detalle["Concepto"] , 0 ,249 );
                            $siaf->MontoTotal=$detalle["Monto"];
                            $siaf->MontoIGV=$detalle["IGV"];
                            $siaf->Clasificador='26 7 1 6 2';
                            $siaf->MontoTotal=$detalle["Monto"];
                            $siaf->Clasificador1='';
                            $siaf->MontoTotal1='';
                            $siaf->Poa=$certificado->CodigoPoaPNIA;
                        }
                        
                        $MontoEjecutado = $MontoEjecutado+$siaf->MontoTotal;
                        
                        $detalleReq = DetalleRequerimiento::find()
                        ->where('RequerimientoID=:RequerimientoID',[':RequerimientoID'=>$requeComprometido->ID])
                        ->all();
                        foreach ($detalleReq as $xx) {
                            $detalleReq = AreSubCategoria::find()
                                ->where('ID=:ID',[':ID'=>$xx->AreSubCategoriaID])
                                ->one();
                        }
                        $datosR = $this->OracleSiaftCompromisoAdministrativo($siaf);
                        //-----------
                            $siafInterno = new Siaft;
                            $siafInterno->Fase = "10";
                            $siafInterno->Fecha = date('Ymd H:m:s');
                            $siafInterno->Monto = $detalle["Monto"];
                            $siafInterno->FechaProceso = date('Ymd',strtotime($detalle["FechaOrden"]));
                            $siafInterno->Estado = 1;
                            $siafInterno->CodigoProyecto = $CodigoProyecto;
                            $siafInterno->Orden = $detalle["OrdenID"];
                            $siafInterno->Moneda = 'S/';
                            $siafInterno->TipoGastoID = $detalle["TipoGasto"];
                            $siafInterno->Siaf = $datosR;
                            $siafInterno->save();
                        // ----------
                        $orden->update();
                        $siafSecuencial->Secuencial=$siafSecuencial->Secuencial+1;
                    }
                    $siafSecuencial->update();
                }
                elseif($requerimiento->TipoRequerimiento==4)
                {
                    $detalles = (new \yii\db\Query())
                        ->select('*,Viatico.Correlativo ViaticoCorrelativo,Viatico.ID ViaticoID')
                        ->from('CompromisoPresupuestal')
                        ->innerJoin('DetalleCompromisoPresupuestal','DetalleCompromisoPresupuestal.CompromisoPresupuestalID=CompromisoPresupuestal.ID')
                        ->innerJoin('Viatico','Viatico.ID=DetalleCompromisoPresupuestal.Correlativo')
                        ->where('CompromisoPresupuestal.ID=:ID and Viatico.ID in ('.$IDsGenerados.')',[':ID'=>$ID])
                        ->distinct()
                        ->all();
                        
                    $contenido='';
                    foreach($detalles as $detalle)
                    {
                        $CompAnual = Siaft::find()->where('TipoGastoID=:TipoGastoID AND Orden=:Orden and Fase=9',[':TipoGastoID'=>$requerimiento->TipoRequerimiento,':Orden'=>$detalle['ViaticoID']])->one();
                        
                        $viatico=Viatico::findOne($detalle["ViaticoID"]);
                        
                        $siaf=new Siaf;
                        $siaf->SiafCertificado=str_pad($certificado->CodigoCertificacion, 10, "0", STR_PAD_LEFT);
                        $siaf->InterfaceCertificado=str_pad($siaf->InterfaceCertificado, 10, "0", STR_PAD_LEFT);
                        $siaf->Anio=$detalle["Annio"];
                        $siaf->Mes=date('m');
                        $siaf->RUCDocumento=$detalle["Dni"];
                        $siaf->Secuencia=$CompAnual->Siaf;
                        $siaf->TipoDoc="043";//tipodoc
                        
                        if($CodigoProyecto == '065_PI'){
                            $siaf->FuenteFinanciamiento='00';
                            $siaf->InterfaceSecuencial="0054";
                        }

                        // $siaf->NumeroDocumento= trim(str_pad($detalle["ViaticoCorrelativo"], 3, "0", STR_PAD_LEFT)."-".$detalle["Annio"]);  12
                        
                        // $siaf->NumeroDocumento= trim(substr($detalle["Annio"],-2).$codeProyecto.str_pad($detalle["ViaticoCorrelativo"], 3, "0", STR_PAD_LEFT).str_pad($compromiso->Correlativo, 4, "0", STR_PAD_LEFT));
                        $siaf->NumeroDocumento= trim("PV".date('Y').$codeProyecto.str_pad($detalle["ViaticoCorrelativo"], 3, "0", STR_PAD_LEFT));//mensual
                        // trim("PV".date('Y').$codeProyecto.str_pad($viatico->Correlativo, 3, "0", STR_PAD_LEFT))
                        // $siaf->FechaDocumento=date('d/m/Y',strtotime($detalle["FechaCreacion"]));
                        if(date('m',strtotime($detalle["FechaCreacion"])) == date('m')){
                            $siaf->FechaDocumento = Yii::$app->tools->dateFormat($detalle["FechaCreacion"],'d/m/Y');
                        }else{
                            $siaf->FechaDocumento = '01/'.date('m').'/'.date('Y');
                        }
                        $siaf->ConceptoDocumento=substr ( $detalle["PlanTrabajo"] , 0 ,249 );
                        $siaf->MontoTotal=$detalle["Monto"];
                        $siaf->MontoIGV=0;
                        $siaf->Clasificador='26 7 1 6 3';
                        $siaf->MontoTotal=$detalle["Monto"];
                        $siaf->Clasificador1='';
                        $siaf->MontoTotal1='';
                        $siaf->Poa=$certificado->CodigoPoaPNIA;
                        //echo '<pre>';print_r($siaf); die;
                        $datosR = $this->OracleSiaftCompromisoAdministrativo($siaf);
                        $MontoEjecutado = $MontoEjecutado+$siaf->MontoTotal;
                        //-----------
                            $siafInterno = new Siaft;
                            $siafInterno->Fase = "10";
                            $siafInterno->Fecha = date('Ymd H:m:s');
                            $siafInterno->Monto = $detalle["Monto"];
                            $siafInterno->FechaProceso = date('Ymd',strtotime($detalle["FechaCreacion"]));
                            $siafInterno->Estado = 1;
                            $siafInterno->CodigoProyecto = $CodigoProyecto;
                            $siafInterno->Orden = $detalle["ViaticoID"];
                            $siafInterno->Moneda = 'S/';
                            $siafInterno->TipoGastoID = $detalle["TipoGasto"];
                            $siafInterno->Siaf = $datosR;
                            $siafInterno->save();
                        // ----------
                        $siafSecuencial->Secuencial=$siafSecuencial->Secuencial+1;
                    }
                    $siafSecuencial->update();
                }
                elseif($requerimiento->TipoRequerimiento==5)//Caja Chica
                {
                    
                    $detalles = (new \yii\db\Query())
                        ->select('*,DetalleCajaChica.Correlativo OrdenCorrelativo,DetalleCajaChica.ID OrdenID,DetalleCajaChica.TipoCambio,DetalleCajaChica.CajaChicaID CajaChicaID')
                        ->from('CompromisoPresupuestal')
                        ->innerJoin('DetalleCompromisoPresupuestal','DetalleCompromisoPresupuestal.CompromisoPresupuestalID=CompromisoPresupuestal.ID')
                        ->innerJoin('DetalleCajaChica','DetalleCajaChica.ID=DetalleCompromisoPresupuestal.Correlativo')
                        ->where('CompromisoPresupuestal.ID=:ID',[':ID'=>$ID])
                        ->distinct()
                        ->one();
                    
                    $detallecajaChica=DetalleCajaChica::find()->where('RequerimientoID=:RequerimientoID and Estado=1 and Situacion=3',[':RequerimientoID'=>$requerimiento->ID])->one();
                    
                    if($detallecajaChica)
                    {
                            $siaf=new Siaf;
                            $siafInterno = new Siaft;
                            $CompAnual = Siaft::find()->where('TipoGastoID=:TipoGastoID AND Orden=:Orden and Fase=9',[':TipoGastoID'=>$requerimiento->TipoRequerimiento,':Orden'=>$detalles['OrdenID']])->one();
                            if($detallecajaChica->Tipo==1)//apertura
                            {
                                
                            }
                            elseif($detallecajaChica->Tipo==2)//reembolso
                            {
                                
                            }
                            $siaf=new Siaf;
                            $siaf->SiafCertificado=str_pad($certificado->CodigoCertificacion, 10, "0", STR_PAD_LEFT);
                            $siaf->InterfaceCertificado=str_pad($siaf->InterfaceCertificado, 10, "0", STR_PAD_LEFT);
                            $siaf->Anio=$detalles["Annio"];
                            $siaf->Mes=date('m');
                            $siaf->RUCDocumento=$detallecajaChica->Dni;
                            $siaf->Secuencia=$CompAnual->Siaf;
                            $siaf->TipoDoc="234";//tipodoc
                            // $siaf->NumeroDocumento= str_pad($detalles["OrdenCorrelativo"], 3, "0", STR_PAD_LEFT)."-".$detalles["Annio".$codeProyecto];//mensual
                            $siaf->NumeroDocumento=str_pad($detalles["OrdenCorrelativo"], 3, "0", STR_PAD_LEFT).$detalles["Annio"].$codeProyecto;
                            
                            // $siaf->FechaDocumento=date('d/m/Y',strtotime($detalles["FechaRegistro"]));
                            if(date('m',strtotime($detalles["FechaRegistro"])) == date('m')){
                                $siaf->FechaDocumento= Yii::$app->tools->dateFormat($detalle["FechaRegistro"],'d/m/Y');
                            }else{
                                $siaf->FechaDocumento = '01/'.date('m').'/'.date('Y');
                            }
                            $siaf->ConceptoDocumento=substr ( $detalles["Descripcion"] , 0 ,249 );
                            $siaf->MontoTotal=$detalles["Servicios"]+$detalles["Bienes"];
                            $siaf->MontoIGV=0;
                            $siaf->Clasificador='26 7 1 6 3';
                            $siaf->MontoTotal=$detalles["Servicios"];
                            $siaf->Clasificador1='26 7 1 6 2';
                            $siaf->MontoTotal1=$detalles["Bienes"];
                            $siaf->Poa=$certificado->CodigoPoaPNIA;
                            
                            
                            if($CodigoProyecto == '065_PI'){
                                $siaf->FuenteFinanciamiento='00';
                                $siaf->InterfaceSecuencial="0054";
                            }

                            $datosR = $this->OracleSiaftCompromisoAdministrativo($siaf);
                            $MontoEjecutado = $MontoEjecutado+$siaf->MontoTotal;
                            //-----------
                                $siafInterno = new Siaft;
                                $siafInterno->Fase = "10";
                                $siafInterno->Fecha = date('Ymd H:m:s');
                                $siafInterno->Monto = $siaf->MontoTotal;
                                $siafInterno->FechaProceso = date('Ymd',strtotime($detalles["FechaRegistro"]));
                                $siafInterno->Estado = 1;
                                $siafInterno->CodigoProyecto = $CodigoProyecto;
                                $siafInterno->Orden = $detalles["OrdenID"];
                                $siafInterno->Moneda = 'S/';
                                $siafInterno->TipoGastoID = $detalles["TipoGasto"];
                                $siafInterno->Siaf = $datosR;
                                $siafInterno->save();
                            // ----------
                            $siafSecuencial->Secuencial=$siafSecuencial->Secuencial+1;
                    }
                    
                    $siafSecuencial->update();
                }
                elseif($requerimiento->TipoRequerimiento==6)
                {
                    $detalles = (new \yii\db\Query())
                        ->select('*,Encargo.Correlativo EncargoCorrelativo,Encargo.ID EncargoID,Encargo.FechaRegistro FechaReg,Encargo.Bienes,Encargo.Servicios')
                        ->from('CompromisoPresupuestal')
                        ->innerJoin('DetalleCompromisoPresupuestal','DetalleCompromisoPresupuestal.CompromisoPresupuestalID=CompromisoPresupuestal.ID')
                        ->innerJoin('Encargo','Encargo.ID=DetalleCompromisoPresupuestal.Correlativo')
                        ->where('CompromisoPresupuestal.ID=:ID and Encargo.ID in ('.$IDsGenerados.')',[':ID'=>$ID])
                        ->distinct()
                        ->all();
                        
                    $contenido='';
                    foreach($detalles as $detalle)
                    {
                        $encargo=Encargo::find()->where('ID=:ID and Estado=1 and Situacion=1',[':ID'=>$detalle['EncargoID']])->one();
                        $siaf=new Siaf;
                        $siafInterno = new Siaft;
                        $CompAnual = Siaft::find()->where('TipoGastoID=:TipoGastoID AND Orden=:Orden and Fase=9',[':TipoGastoID'=>$requerimiento->TipoRequerimiento,':Orden'=>$detalle['EncargoID']])->one();
                        
                        $siaf=new Siaf;
                        $siaf->SiafCertificado=str_pad($certificado->CodigoCertificacion, 10, "0", STR_PAD_LEFT);
                        $siaf->InterfaceCertificado=str_pad($siaf->InterfaceCertificado, 10, "0", STR_PAD_LEFT);
                        $siaf->Anio=$detalle["Annio"];
                        $siaf->Mes=date('m');
                        $siaf->RUCDocumento=$detalle["DNIEncargo"];
                        $siaf->Secuencia=$CompAnual->Siaf;
                        $siaf->TipoDoc="234";//tipodoc
                        $siaf->NumeroDocumento= trim("E".str_pad($detalle["EncargoCorrelativo"], 3, "0", STR_PAD_LEFT).$detalle["Annio"].$codeProyecto);
                        
                        // $siaf->FechaDocumento=date('d/m/Y',strtotime($detalle["FechaReg"]));
                        if(date('m',strtotime($detalle["FechaReg"])) == date('m')){
                            $siaf->FechaDocumento= Yii::$app->tools->dateFormat($detalle["FechaReg"],'d/m/Y');
                        }else{
                            $siaf->FechaDocumento = '01/'.date('m').'/'.date('Y');
                        }

                        if($CodigoProyecto == '065_PI'){
                            $siaf->FuenteFinanciamiento='00';
                            $siaf->InterfaceSecuencial="0054";
                        }
                        
                        $siaf->ConceptoDocumento=substr ( $detalle["DescripcionActividad"] , 0 ,249 );
                        $siaf->MontoTotal=$detalle["Servicios"]+$detalle["Bienes"];
                        $siaf->MontoIGV=0;
                        //var_dump($detalle["Bienes"]);die;
                        
                        if($detalle["Bienes"] && $detalle["Servicios"]==0)
                        {
                            $siaf->MontoTotal=$detalle["Bienes"];
                            $siaf->Clasificador='26 7 1 6 2';
                            $siaf->MontoTotal1=NULL;
                            $siaf->Clasificador1=NULL;
                        }
                        elseif($detalle["Servicios"] && $detalle["Bienes"]==0)
                        {
                            $siaf->MontoTotal=$detalle["Servicios"];
                            $siaf->Clasificador='26 7 1 6 3';
                            $siaf->MontoTotal1=NULL;
                            $siaf->Clasificador1=NULL;
                        }
                        elseif($detalle["Bienes"] && $detalle["Servicios"])
                        {
                            $siaf->MontoTotal=$detalle["Bienes"];
                            $siaf->Clasificador='26 7 1 6 2';
                            $siaf->MontoTotal1=$detalle["Servicios"];
                            $siaf->Clasificador1='26 7 1 6 3';
                        }
                        
                        $siaf->Poa=$certificado->CodigoPoaPNIA;
                        $datosR = $this->OracleSiaftCompromisoAdministrativo($siaf);
                        $MontoEjecutado = $MontoEjecutado+$siaf->MontoTotal;
                        //-----------
                            $siafInterno = new Siaft;
                            $siafInterno->Fase = "10";
                            $siafInterno->Fecha = date('Ymd H:m:s');
                            $siafInterno->Monto = $siaf->MontoTotal;
                            $siafInterno->FechaProceso = date('Ymd',strtotime($detalle["FechaReg"]));
                            $siafInterno->Estado = 1;
                            $siafInterno->CodigoProyecto = $CodigoProyecto;
                            $siafInterno->Orden = $detalle["EncargoID"];
                            $siafInterno->Moneda = 'S/';
                            $siafInterno->TipoGastoID = $detalle["TipoGasto"];
                            $siafInterno->Siaf = $datosR;
                            $siafInterno->save();
                        // ----------
                        $siafSecuencial->Secuencial=$siafSecuencial->Secuencial+1;
                    }
                    $siafSecuencial->update();
                }
                $pasoCritico->MontoTotalEjecutado=$MontoEjecutado;
                $pasoCritico->update();
            }
        }

        private function SiaftInternoSeguimiento($detalle = array(),$tipo = null){
            $internoSiaft = new Siaft;
            $internoSiaft->Fase = $tipo;
            $internoSiaft->Secuencial = $detalle['OrdenCorrelativo'];
            $internoSiaft->Fecha = $detalle['FechaOrden'];
            $internoSiaft->Monto = $detalle['Monto'];
            $internoSiaft->FechaProceso = date('Y-m-d');
            $internoSiaft->Estado = 1;
            $internoSiaft->CodigoProyecto = $detalle['CodigoProyecto'];
            $internoSiaft->Orden = $detalle['OrdenID'];
            $internoSiaft->Moneda = 'S/.';
            $saveid = $internoSiaft->insert();
            return $saveid;
        }
    
    
    public function actionGenerarDevengado()
    {
        if(isset($_POST["CodigoProyecto"]) && isset($_POST["ID"]) && isset($_POST["CodigosIDs"]))
        {
            $IDsGenerados="";
            for($i=0;$i<count($_POST["CodigosIDs"]); $i++)
            {
                if($i==0)
                {
                    $IDsGenerados=$_POST["CodigosIDs"][$i];
                }
                elseif($i>0)
                {
                    $IDsGenerados=$IDsGenerados.",".$_POST["CodigosIDs"][$i];
                }
            }
            
            $compromiso=CompromisoPresupuestal::findOne($_POST["ID"]);
            $requerimiento=Requerimiento::findOne($compromiso->RequerimientoID);
           //var_dump($IDsGenerados);die;
            $variable=0;
            if($requerimiento->TipoRequerimiento==4)//viatocps
            {
                $DetalleRequerimientos=DetalleRequerimiento::find()->select('TipoDetalleOrdenID')->where('RequerimientoID=:RequerimientoID and TipoDetalleOrdenID in ('.$IDsGenerados.')',[':RequerimientoID'=>$requerimiento->ID])->all();
                foreach($DetalleRequerimientos as $DetalleRequerimiento)
                {
                    $viatico=Viatico::findOne($DetalleRequerimiento->TipoDetalleOrdenID);
                    
                    $compromisoAnual=Siaft::find()->where('Fase=9 and Orden=:Orden and CodigoProyecto=:CodigoProyecto',
                                   [':Orden'=>$DetalleRequerimiento->TipoDetalleOrdenID,':CodigoProyecto'=>$_POST["CodigoProyecto"]])
                                ->one();
                    $compromisoAdministrativo=Siaft::find()->where('Fase=10 and Orden=:Orden and CodigoProyecto=:CodigoProyecto',
                                               [':Orden'=>$DetalleRequerimiento->TipoDetalleOrdenID,':CodigoProyecto'=>$_POST["CodigoProyecto"]])
                                            ->one();
                    $GetExpedienteSiaf=\Yii::$app->db->createCommand('BEGIN DECLARE @a VARCHAR(255); EXEC SP_OBTENER_EXPEDIENTE_SIAF :NUM_REGI_CAB, :ANNO_EJEC_EJE select @a as NUME_SIAF_CAB end;')
                                                                    ->bindValue(':NUM_REGI_CAB',$compromisoAnual->Siaf)
                                                                    ->bindValue(':ANNO_EJEC_EJE',$viatico->Annio)
                                                                    ->queryOne();
                    
                    $ActualizarDevengado = \Yii::$app->db->createCommand('BEGIN DECLARE @a VARCHAR(255); EXEC SP_DEVENGADO_ACTUALIZA :Codigo, :Anio, :Estado, @a output; select @a as ExpedienteSiaf end; ')
                    ->bindValue(':Codigo',    $compromisoAdministrativo->Siaf )
                    ->bindValue(':Anio',      $viatico->Annio)
                    ->bindValue(':Estado',    1)
                    ->queryOne();
                   
                    if($ActualizarDevengado){
                        $variable=$ActualizarDevengado['ExpedienteSiaf'];
                        
                    }else{
                        $variable=0;
                    }
                }
                $compromiso->Devengado=1;
                $compromiso->update();
                return $variable;
            }
            elseif($requerimiento->TipoRequerimiento==5)//caja
            {
                
            }
            elseif($requerimiento->TipoRequerimiento==6)//encargos
            {
                $DetalleRequerimientos=DetalleRequerimiento::find()->select('TipoDetalleOrdenID')->where('RequerimientoID=:RequerimientoID and TipoDetalleOrdenID in ('.$IDsGenerados.')',[':RequerimientoID'=>$requerimiento->ID])->GroupBy('TipoDetalleOrdenID')->all();
                foreach($DetalleRequerimientos as $DetalleRequerimiento)
                {
                    $encargo=Encargo::findOne($DetalleRequerimiento->TipoDetalleOrdenID);

                    $compromisoAnual=Siaft::find()->where('Fase=9 and Orden=:Orden and CodigoProyecto=:CodigoProyecto',
                                   [':Orden'=>$DetalleRequerimiento->TipoDetalleOrdenID,':CodigoProyecto'=>$_POST["CodigoProyecto"]])
                                ->one();
                    $compromisoAdministrativo=Siaft::find()->where('Fase=10 and Orden=:Orden and CodigoProyecto=:CodigoProyecto',
                                               [':Orden'=>$DetalleRequerimiento->TipoDetalleOrdenID,':CodigoProyecto'=>$_POST["CodigoProyecto"]])
                                            ->one();
                    $GetExpedienteSiaf=\Yii::$app->db->createCommand('BEGIN DECLARE @a VARCHAR(255); EXEC SP_OBTENER_EXPEDIENTE_SIAF :NUM_REGI_CAB, :ANNO_EJEC_EJE select @a as NUME_SIAF_CAB end;')
                                                                    ->bindValue(':NUM_REGI_CAB',$compromisoAnual->Siaf)
                                                                    ->bindValue(':ANNO_EJEC_EJE',$encargo->Annio)
                                                                    ->queryOne();
                    
                    $ActualizarDevengado = \Yii::$app->db->createCommand('BEGIN DECLARE @a VARCHAR(255); EXEC SP_DEVENGADO_ACTUALIZA :Codigo, :Anio, :Estado, @a output; select @a as ExpedienteSiaf end; ')
                    ->bindValue(':Codigo',   $compromisoAdministrativo->Siaf )
                    ->bindValue(':Anio',      $encargo->Annio)
                    ->bindValue(':Estado',    1)
                    ->queryOne();
                   
                    if($ActualizarDevengado){
                        $variable=$ActualizarDevengado['ExpedienteSiaf'];
                        
                    }else{
                        $variable=0;
                    }
                }
                $compromiso->Devengado=1;
                $compromiso->update();
                return $variable;
            }
            $arr = array('Success' => true);
            echo json_encode($arr);
        }
    }
    
    public function actionAdjuntarDocumento($ID=null){
        $this->layout='vacio';
        $compromiso=CompromisoPresupuestal::findOne($ID);
        if($compromiso->load(Yii::$app->request->post())){
            //$compromiso->Situacion=3;
            $compromiso->archivo = UploadedFile::getInstance($compromiso, 'archivo');
            if($compromiso->archivo)
            {
                $compromiso->archivo->saveAs('compromisopresupuestal/F' . $compromiso->ID . '.' . $compromiso->archivo->extension);
                $compromiso->Formato='F'.$compromiso->ID . '.' . $compromiso->archivo->extension;
            }
            $compromiso->update();
            return $this->redirect(['/compromiso-presupuestal']);
        }
        return $this->render('adjuntar-documento',['ID'=>$ID,'compromiso'=>$compromiso]);
    }
    
    public function actionEliminarAdjunto($id=null)
    {
        $compromiso=CompromisoPresupuestal::findOne($id);
        $compromiso->Formato='';
        $compromiso->update();
       
        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
    public function actionAdjuntarDocumentoUafsi($ID=null){
        $this->layout='vacio';
        $compromiso=CompromisoPresupuestal::findOne($ID);
        if($compromiso->load(Yii::$app->request->post())){
            $compromiso->archivo = UploadedFile::getInstance($compromiso, 'archivo');
            if($compromiso->archivo)
            {
                $compromiso->archivo->saveAs('compromisopresupuestal/UAF' . $compromiso->ID . '.' . $compromiso->archivo->extension);
                $compromiso->FormatoUafsi='UAF'.$compromiso->ID . '.' . $compromiso->archivo->extension;
            }
            $compromiso->update();
            return $this->redirect(['compromiso-presupuestal/detalle-uafsi','id'=>$ID]);
        }
        return $this->render('adjuntar-documento-uafsi',['ID'=>$ID,'compromiso'=>$compromiso]);
    }
    
    
    public function actionAdjuntarDocumentoJefeUafsi($ID=null){
        $this->layout='vacio';
        $compromiso=CompromisoPresupuestal::findOne($ID);
        if($compromiso->load(Yii::$app->request->post())){
            $compromiso->archivo = UploadedFile::getInstance($compromiso, 'archivo');
            if($compromiso->archivo)
            {
                $compromiso->archivo->saveAs('compromisopresupuestal/UAF' . $compromiso->ID . '.' . $compromiso->archivo->extension);
                $compromiso->FormatoUafsi='UAF'.$compromiso->ID . '.' . $compromiso->archivo->extension;
                $compromiso->FormatoJefeUafsi='UAF'.$compromiso->ID . '.' . $compromiso->archivo->extension;
            }
            $compromiso->update();
            return $this->redirect(['compromiso-presupuestal/detalle-uafsi','id'=>$ID]);
        }
        return $this->render('adjuntar-documento-jefe-uafsi',['ID'=>$ID,'compromiso'=>$compromiso]);
    }
    
    public function actionAdjuntarDocumentoSeguimientoUafsi($ID=null){
        $this->layout='vacio';
        $compromiso=CompromisoPresupuestal::findOne($ID);
        if($compromiso->load(Yii::$app->request->post())){
            $compromiso->archivo = UploadedFile::getInstance($compromiso, 'archivo');
            if($compromiso->archivo)
            {
                $compromiso->archivo->saveAs('compromisopresupuestal/UAF' . $compromiso->ID . '.' . $compromiso->archivo->extension);
                $compromiso->FormatoUafsi='UAF'.$compromiso->ID . '.' . $compromiso->archivo->extension;
                $compromiso->FormatoSeguimientoUafsi='UAF'.$compromiso->ID . '.' . $compromiso->archivo->extension;
            }
            $compromiso->update();
            return $this->redirect(['compromiso-presupuestal/detalle-uafsi','id'=>$ID]);
        }
        return $this->render('adjuntar-documento-seguimiento-uafsi',['ID'=>$ID,'compromiso'=>$compromiso]);
    }
    
    public function actionEliminarAdjuntoUafsi($id=null)
    {
        $compromiso=CompromisoPresupuestal::findOne($id);
        $compromiso->FormatoUafsi='';
        $compromiso->update();
       
        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
    public function actionEliminarAdjuntoJefeUafsi($id=null)
    {
        $compromiso=CompromisoPresupuestal::findOne($id);
        $compromiso->FormatoJefeUafsi='';
        $compromiso->update();
       
        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
    public function actionValidarDocumentoAdjuntoSeguimientoUafsi()
    {
        if(isset($_POST["ID"]) && $_POST["ID"]!='')
        {
            $id=$_POST["ID"];
           $compromiso=CompromisoPresupuestal::findOne($id);
            if($compromiso->FormatoSeguimientoUafsi!='' && $compromiso->FormatoSeguimientoUafsi)
            {
                $arr = array('Success' => true);
                echo json_encode($arr);
                die;
            }
            else
            {
                $arr = array('Success' => false);
                echo json_encode($arr);
                die;
            } 
        }
    }
    
    public function actionValidarDocumentoAdjuntoJefeUafsi()
    {
        if(isset($_POST["ID"]) && $_POST["ID"]!='')
        {
            $id=$_POST["ID"];
            $compromiso=CompromisoPresupuestal::findOne($id);
            if($compromiso->FormatoJefeUafsi!='' && $compromiso->FormatoJefeUafsi)
            {
                $arr = array('Success' => true);
                echo json_encode($arr);
                die;
            }
            else
            {
                $arr = array('Success' => false);
                echo json_encode($arr);
                die;
            } 
        }
    }
    
    public function actionEliminarAdjuntoSeguimientoUafsi($id=null)
    {
        $compromiso=CompromisoPresupuestal::findOne($id);
        $compromiso->FormatoSeguimientoUafsi='';
        $compromiso->update();
       
        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
    
    public function getSituacionJefeUafsi($situacion=null)
    {
        $situacion=Situacion::findOne($situacion);
        
        if($situacion->ID==17)
        {
            return "Pendiente";
        }
        else
        {
            return "Aprobado";
        }
    }
    
    public function getSituacionTecnicoUafsi($situacion=null)
    {
        $situacion=Situacion::findOne($situacion);
        
        if($situacion->ID==15)
        {
            return "Pendiente";
        }
        else
        {
            return "Aprobado";
        }
    }
    
    public function getSituacionSeguimientoUafsi($situacion=null)
    {
        $situacion=Situacion::findOne($situacion);
        
        if($situacion->ID==16)
        {
            return "Pendiente";
        }
        else
        {
            return "Aprobado";
        }
    }
    
    public function getUbicacionCompromisoPresupuestal($Situacion=null,$ID=null)
    {
        if($Situacion==15)
        {
            return "Supervisor UAFSI";
        }
        elseif($Situacion==16)
        {
            return "Esp. seguimiento UAFSI";
        }
        elseif($Situacion==17)
        {
            return "Jefe UAFSI";
        }
        elseif($Situacion==18)
        {
            return "Jefe Administración";
        }
        elseif($Situacion==19)
        {
            return "Jefe Contabilidad";
        }
        elseif($Situacion==20)
        {
            $seguimientoCompromiso=SeguimientoCompromisoPresupuestal::find()
                                    ->select('Persona.Nombre, Persona.ApellidoPaterno')
                                    ->innerJoin('Usuario','Usuario.ID=SeguimientoCompromisoPresupuestal.UsuarioID')
                                    ->innerJoin('Persona','Persona.ID=Usuario.PersonaID')
                                    ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                                    ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID and UsuarioRol.RolID=14',[':CompromisoPresupuestalID'=>$ID])
                                    ->one();
            
            return "Especialista administrativo - $seguimientoCompromiso->Nombre $seguimientoCompromiso->ApellidoPaterno";
        }
        
        elseif($Situacion==21)
        {
            return "Compromiso Presupuestal";
        }
    }
    
    public function actionListadoCompromisoUafsi()
    {
        $this->layout='vacio';
        if(\Yii::$app->user->identity->rol == 2){
            $resultado=CompromisoPresupuestal::find()
                        ->select('CompromisoPresupuestal.ID,CompromisoPresupuestal.Correlativo,CompromisoPresupuestal.Annio,CompromisoPresupuestal.Codigo,CompromisoPresupuestal.FechaRegistro,CompromisoPresupuestal.Situacion,TipoGasto.Nombre NombreGasto,CompromisoPresupuestal.Memorando')
                        ->innerJoin('UsuarioProyecto','UsuarioProyecto.CodigoProyecto=CompromisoPresupuestal.Codigo')
                        ->innerJoin('Usuario','Usuario.ID=UsuarioProyecto.UsuarioID')
                        ->innerJoin('Requerimiento','Requerimiento.ID=CompromisoPresupuestal.RequerimientoID')
                        ->innerJoin('TipoGasto','TipoGasto.ID=Requerimiento.TipoRequerimiento')
                        ->where('CompromisoPresupuestal.Situacion in (15,16,17,18,19,20,21) AND CompromisoPresupuestal.Estado=1 and Usuario.ID=:ID',[':ID'=>\Yii::$app->user->id])
                        ->all();
            $nro=0;
            echo '<pre>';
            foreach($resultado as $result)
            {
                $nro++;
                $tiempo=SeguimientoCompromisoPresupuestal::find()
                            ->innerJoin('Usuario','Usuario.ID=SeguimientoCompromisoPresupuestal.UsuarioID')
                            ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                            ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID and UsuarioRol.RolID=2',[':CompromisoPresupuestalID'=>$result["ID"]])
                            ->one();
                
                
                echo "<tr>";
                    echo "<td> N° " . str_pad($result["Correlativo"], 3, "0", STR_PAD_LEFT)  ."-".$result["Annio"]."</td>";
                    echo "<td> " . $result["Memorando"]."</td>";
                    echo "<td> " . $result["NombreGasto"] . "</td>";
                    echo "<td> " . $result["Codigo"] . "</td>";
                    echo "<td>" . date('d-m-Y',strtotime($result["FechaRegistro"])) . "</td>";
                    echo "<td>" . $this->getSituacionTecnicoUafsi($result["Situacion"]) . "</td>";
                    if($tiempo)
                    {
                        if($tiempo->Situacion==0)
                        {
                            echo "<td>" . Yii::$app->tools->RestarFechas($tiempo->FechaInicio,date('Y-m-d h:m:s')) . "</td>";
                        }
                        elseif($tiempo->Situacion==8)
                        {
                            echo "<td><b>" .Yii::$app->tools->FormatoDemora($tiempo->Dias). " </b></td>";
                        }
                    }
                    else
                    {
                        echo "<td></td>";
                    }
                    echo "<td>" . $this->getUbicacionCompromisoPresupuestal($result["Situacion"],$result["ID"]) . "</td>";
                    
                    if($result['Situacion'] == 15){
                        echo "<td><a href='detalle-uafsi?id=".$result["ID"]."' class='btn btn-danger'>Ver detalle</a></td>";
                    }else{
                        echo "<td><a href='detalle-uafsi?id=".$result["ID"]."' class='btn btn-info'>Ver detalle</a></td>";
                    }
                echo "</tr>";
            }
        }
        elseif(\Yii::$app->user->identity->rol == 11)
        {
            
            
            $resultado=CompromisoPresupuestal::find()
                        ->select('CompromisoPresupuestal.ID,CompromisoPresupuestal.Correlativo,CompromisoPresupuestal.Annio,CompromisoPresupuestal.Codigo,CompromisoPresupuestal.FechaRegistro,CompromisoPresupuestal.Situacion,TipoGasto.Nombre NombreGasto,CompromisoPresupuestal.Memorando')
                        ->innerJoin('UsuarioProyecto','UsuarioProyecto.CodigoProyecto=CompromisoPresupuestal.Codigo')
                        ->innerJoin('Usuario','Usuario.ID=UsuarioProyecto.UsuarioID')
                        ->innerJoin('Requerimiento','Requerimiento.ID=CompromisoPresupuestal.RequerimientoID')
                        ->innerJoin('TipoGasto','TipoGasto.ID=Requerimiento.TipoRequerimiento')
                        ->where('CompromisoPresupuestal.Situacion in (17,18,19,20,21) AND CompromisoPresupuestal.Estado=1 and Usuario.ID=:ID',[':ID'=>\Yii::$app->user->id])
                        ->all();
            $nro=0;
            echo '<pre>';
            foreach($resultado as $result)
            {
                $nro++;
                $tiempo=SeguimientoCompromisoPresupuestal::find()
                            ->innerJoin('Usuario','Usuario.ID=SeguimientoCompromisoPresupuestal.UsuarioID')
                            ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                            ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID and UsuarioRol.RolID=11',[':CompromisoPresupuestalID'=>$result["ID"]])
                            ->one();
                            
                echo "<tr>";
                    echo "<td>N° " . str_pad($result["Correlativo"], 3, "0", STR_PAD_LEFT)  ."-".$result["Annio"]."</td>";
                    echo "<td> " . $result["Memorando"]."</td>";
                    echo "<td>" . $result["NombreGasto"] . "</td>";
                    echo "<td>" . $result["Codigo"] . "</td>";
                    echo "<td>" . date('d-m-Y',strtotime($result["FechaRegistro"])) . "</td>";
                    echo "<td>" . $this->getSituacionJefeUafsi($result["Situacion"]) . "</td>";
                    if($tiempo)
                    {
                        if($tiempo->Situacion==0)
                        {
                            echo "<td>" . Yii::$app->tools->RestarFechas($tiempo->FechaInicio,date('Y-m-d h:m:s')) . "</td>";
                        }
                        elseif($tiempo->Situacion==8)
                        {
                            echo "<td><b>" .Yii::$app->tools->FormatoDemora($tiempo->Dias). " </b></td>";
                        }
                    }
                    else
                    {
                        echo "<td></td>";
                    }
                    echo "<td>" . $this->getUbicacionCompromisoPresupuestal($result["Situacion"],$result["ID"]) . "</td>";
                    /*
                    if($result['GastoElegible'] != ''){
                        echo '<td>'.$this->tipoGasto($result['GastoElegible']).'</td>';
                    }else{
                        echo '<td>-</td>';
                    }*/
                    if($result['Situacion'] == 17){
                        echo "<td><a href='detalle-uafsi?id=".$result["ID"]."' class='btn btn-danger'>Ver detalle</a></td>";
                    }
                    else{
                        echo "<td><a href='detalle-uafsi?id=".$result["ID"]."' class='btn btn-info'>Ver detalle</a></td>";
                    }
                echo "</tr>";
            }
        }
        elseif(\Yii::$app->user->identity->rol == 15)
        {
            $resultado=CompromisoPresupuestal::find()
                        ->select('CompromisoPresupuestal.*,TipoGasto.Nombre NombreGasto')
                        ->innerJoin('UsuarioProyecto','UsuarioProyecto.CodigoProyecto=CompromisoPresupuestal.Codigo')
                        ->innerJoin('Usuario','Usuario.ID=UsuarioProyecto.UsuarioID')
                        ->innerJoin('Requerimiento','Requerimiento.ID=CompromisoPresupuestal.RequerimientoID')
                        ->innerJoin('TipoGasto','TipoGasto.ID=Requerimiento.TipoRequerimiento')
                        ->where('CompromisoPresupuestal.Situacion in (16,17,18,19,20,21) AND CompromisoPresupuestal.Estado=1 AND Usuario.ID=:ID',[':ID'=>\Yii::$app->user->id])
                        ->all();
            
            $nro=0;
            echo '<pre>';
            foreach($resultado as $result)
            {
                $nro++;
                $tiempo=SeguimientoCompromisoPresupuestal::find()
                        ->innerJoin('Usuario','Usuario.ID=SeguimientoCompromisoPresupuestal.UsuarioID')
                        ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                        ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID and UsuarioRol.RolID=15',[':CompromisoPresupuestalID'=>$result["ID"]])
                        ->one();
                        
                echo "<tr>";
                    echo "<td> N° " . str_pad($result["Correlativo"], 3, "0", STR_PAD_LEFT)  ."-".$result["Annio"]."</td>";
                    echo "<td> " . $result["Memorando"]."</td>";
                    echo "<td> " . $result["NombreGasto"] . "</td>";
                    echo "<td> " . $result["Codigo"] . "</td>";
                    echo "<td>" . date('d-m-Y',strtotime($result["FechaRegistro"])) . "</td>";
                    echo "<td>" . $this->getSituacionSeguimientoUafsi($result["Situacion"]) . "</td>";
                    if($tiempo)
                    {
                        if($tiempo->Situacion==0)
                        {
                            echo "<td>" . Yii::$app->tools->RestarFechas($tiempo->FechaInicio,date('Y-m-d h:m:s')) . "</td>";
                        }
                        elseif($tiempo->Situacion==8)
                        {
                            echo "<td><b>" .Yii::$app->tools->FormatoDemora($tiempo->Dias). " </b></td>";
                        }
                    }
                    else
                    {
                        echo "<td></td>";
                    }
                    echo "<td>" . $this->getUbicacionCompromisoPresupuestal($result["Situacion"],$result["ID"]) . "</td>";
                    if($result['Situacion'] == 16){
                        echo "<td><a href='detalle-uafsi?id=".$result["ID"]."' class='btn btn-danger'>Ver detalle</a></td>";
                    }
                    else{
                        echo "<td><a href='detalle-uafsi?id=".$result["ID"]."' class='btn btn-info'>Ver detalle</a></td>";
                    }
                echo "</tr>";
            }
        }
        /*
        $res = (new \yii\db\Query())
            ->select('Persona.ApellidoPaterno,Persona.ApellidoMaterno,Persona.Nombre,Usuario.ID')
            ->from('Persona')
            ->innerJoin('Usuario','Usuario.PersonaID=Persona.ID')
            ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID = Usuario.ID')
            ->where(['UsuarioRol.RolID'=>14])
            ->distinct()
            ->all();
            */
        
    }
    
    public function actionAdjuntarDocumentoVer($ID=null){
        $this->layout='vacio';
        $compromiso=CompromisoPresupuestal::findOne($ID);
        if($compromiso->load(Yii::$app->request->post())){
            //$compromiso->Situacion=3;
            $compromiso->archivo = UploadedFile::getInstance($compromiso, 'archivo');
            if($compromiso->archivo)
            {
                $compromiso->archivo->saveAs('compromisopresupuestal/F' . $compromiso->ID . '.' . $compromiso->archivo->extension);
                $compromiso->Formato='F'.$compromiso->ID . '.' . $compromiso->archivo->extension;
            }
            $compromiso->update();
            return $this->redirect(['/compromiso-presupuestal']);
        }
        return $this->render('adjuntar-documento-ver',['ID'=>$ID,'compromiso'=>$compromiso]);
    }
    
    public function actionUafsi(){
        $this->layout='estandar';
        
        return $this->render('uafsi');
    }
    
    public function actionDetalleUafsi($id = null,$contab=null){
        $this->layout='estandar';
        if(!is_null($id)){
            $cabecera=CompromisoPresupuestal::find()->where(['ID'=>$id])->one();
            $detReque = DetalleRequerimiento::find()->where(['RequerimientoID'=>$cabecera->RequerimientoID])->all();
            $detResult=[];
            foreach ($detReque as $req) {
                
                if($req->AreSubCategoriaID){
                $detResult[]=Yii::$app->db->createCommand("
                SELECT Componente.Correlativo CorrelaCompr,Actividad.Correlativo CorrelaActv,AreSubCategoria.Correlativo CorrelaRecurso,ActRubroElegible.RubroElegibleID,AreSubCategoria.Codificacion,
                    AreSubCategoria.CostoUnitario,AreSubCategoria.Nombre,AreSubCategoria.Especifica,
                    AreSubCategoria.UnidadMedida,AreSubCategoria.CostoUnitario,AreSubCategoria.MetaFisica
                FROM AreSubCategoria 
                INNER JOIN ActRubroElegible ON ActRubroElegible.ID = AreSubCategoria.ActRubroElegibleID
                INNER JOIN RubroElegible ON RubroElegible.ID = ActRubroElegible.RubroElegibleID
                INNER JOIN Actividad ON ActRubroElegible.ActividadID = Actividad.ID
                INNER JOIN Componente ON Actividad.ComponenteID = Componente.ID
                WHERE AreSubCategoria.ID =".$req->AreSubCategoriaID)
                ->queryOne();
                }
            }
            $requerimiento=Requerimiento::findOne($cabecera->RequerimientoID);
            if($requerimiento->TipoRequerimiento==1 || $requerimiento->TipoRequerimiento==2 || $requerimiento->TipoRequerimiento==3)
            {
               $resultados = (new \yii\db\Query())
                ->select('DetalleCompromisoPresupuestal.ID,
                    DetalleCompromisoPresupuestal.Correlativo,
                    DetalleCompromisoPresupuestal.TipoGasto,
                    DetalleCompromisoPresupuestal.GastoElegible,
                    DetalleCompromisoPresupuestal.Observacion,
                    DetalleCompromisoPresupuestal.CompromisoPresupuestalID,
                    DetalleCompromisoPresupuestal.Situacion,
                    CompromisoPresupuestal.Annio,
                    CompromisoPresupuestal.RequerimientoID,
                    Orden.FechaOrden,
                    Orden.Documento,
                    Orden.TipoProceso,
                    Orden.Monto,
                    Orden.RazonSocial,
                    Orden.RUC,
                    TipoGasto.Nombre, 
                    DetalleCompromisoPresupuestal.CodigoMatriz'
                )
                // Orden.SituacionSIAFT,
                ->from('DetalleCompromisoPresupuestal')
                ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.ID=DetalleCompromisoPresupuestal.CompromisoPresupuestalID')
                ->innerJoin('UsuarioProyecto','UsuarioProyecto.CodigoProyecto=CompromisoPresupuestal.Codigo')
                ->innerJoin('Usuario','Usuario.ID=UsuarioProyecto.UsuarioID')
                ->innerJoin('TipoGasto','TipoGasto.ID=DetalleCompromisoPresupuestal.TipoGasto')
                ->innerJoin('Orden','Orden.ID=DetalleCompromisoPresupuestal.Correlativo')

                ->where(['Usuario.ID'=>\Yii::$app->user->id,'DetalleCompromisoPresupuestal.CompromisoPresupuestalID'=>$id])
                ->distinct()
                ->all(); 
            }
            elseif($requerimiento->TipoRequerimiento==4)
            {
                $resultados = (new \yii\db\Query())
                ->select('DetalleCompromisoPresupuestal.ID,
                    DetalleCompromisoPresupuestal.Correlativo,
                    DetalleCompromisoPresupuestal.TipoGasto,
                    DetalleCompromisoPresupuestal.GastoElegible,
                    DetalleCompromisoPresupuestal.Observacion,
                    DetalleCompromisoPresupuestal.CompromisoPresupuestalID,
                    DetalleCompromisoPresupuestal.Situacion,
                    CompromisoPresupuestal.Annio,
                    CompromisoPresupuestal.Documento,
                    CompromisoPresupuestal.RequerimientoID,
                    Viatico.Apellido,
                    Viatico.Nombre,
                    Viatico.Dni,
                    Viatico.Monto,
                    TipoGasto.Nombre tipo,
                    Viatico.Documento,
                    DetalleCompromisoPresupuestal.CodigoMatriz'
                )
                ->from('DetalleCompromisoPresupuestal')
                ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.ID=DetalleCompromisoPresupuestal.CompromisoPresupuestalID')
                ->innerJoin('UsuarioProyecto','UsuarioProyecto.CodigoProyecto=CompromisoPresupuestal.Codigo')
                ->innerJoin('Usuario','Usuario.ID=UsuarioProyecto.UsuarioID')
                ->innerJoin('TipoGasto','TipoGasto.ID=DetalleCompromisoPresupuestal.TipoGasto')
                ->innerJoin('Viatico','Viatico.ID=DetalleCompromisoPresupuestal.Correlativo')
                ->where(['Usuario.ID'=>\Yii::$app->user->id,'DetalleCompromisoPresupuestal.CompromisoPresupuestalID'=>$id])
                ->distinct()
                ->all(); 
            }
            elseif($requerimiento->TipoRequerimiento==5)
            {
                $resultados = (new \yii\db\Query())
                ->select('DetalleCompromisoPresupuestal.ID,
                    DetalleCompromisoPresupuestal.Correlativo,
                    DetalleCompromisoPresupuestal.TipoGasto,
                    DetalleCompromisoPresupuestal.GastoElegible,
                    DetalleCompromisoPresupuestal.Observacion,
                    DetalleCompromisoPresupuestal.CompromisoPresupuestalID,
                    DetalleCompromisoPresupuestal.Situacion,
                    CompromisoPresupuestal.Annio,
                    CompromisoPresupuestal.Documento,
                    CompromisoPresupuestal.RequerimientoID,
                    DetalleCajaChica.Tipo,
                    DetalleCajaChica.Bienes,
                    DetalleCajaChica.Servicios,
                    DetalleCajaChica.Responsable,
                    DetalleCajaChica.ResolucionDirectorial,
                    TipoGasto.Nombre,
                    DetalleCompromisoPresupuestal.CodigoMatriz'
                )
                ->from('DetalleCompromisoPresupuestal')
                ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.ID=DetalleCompromisoPresupuestal.CompromisoPresupuestalID')
                ->innerJoin('UsuarioProyecto','UsuarioProyecto.CodigoProyecto=CompromisoPresupuestal.Codigo')
                ->innerJoin('Usuario','Usuario.ID=UsuarioProyecto.UsuarioID')
                ->innerJoin('TipoGasto','TipoGasto.ID=DetalleCompromisoPresupuestal.TipoGasto')
                ->innerJoin('DetalleCajaChica','DetalleCajaChica.ID=DetalleCompromisoPresupuestal.Correlativo')
                ->where(['Usuario.ID'=>\Yii::$app->user->id,'DetalleCompromisoPresupuestal.CompromisoPresupuestalID'=>$id])
                ->distinct()
                ->all(); 
            }
            elseif($requerimiento->TipoRequerimiento==6)
            {
                $resultados = (new \yii\db\Query())
                ->select('DetalleCompromisoPresupuestal.ID,
                    DetalleCompromisoPresupuestal.Correlativo,
                    DetalleCompromisoPresupuestal.TipoGasto,
                    DetalleCompromisoPresupuestal.GastoElegible,
                    DetalleCompromisoPresupuestal.Observacion,
                    DetalleCompromisoPresupuestal.CompromisoPresupuestalID,
                    DetalleCompromisoPresupuestal.Situacion,
                    CompromisoPresupuestal.Annio,
                    CompromisoPresupuestal.Documento,
                    CompromisoPresupuestal.RequerimientoID,
                    Encargo.Bienes,
                    Encargo.Servicios,
                    Encargo.NombresEncargo,
                    Encargo.ApellidosEncargo,
                    Encargo.DNIEncargo,
                    Encargo.Documento,
                    TipoGasto.Nombre,
                    DetalleCompromisoPresupuestal.CodigoMatriz'
                )

                ->from('DetalleCompromisoPresupuestal')
                ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.ID=DetalleCompromisoPresupuestal.CompromisoPresupuestalID')
                ->innerJoin('UsuarioProyecto','UsuarioProyecto.CodigoProyecto=CompromisoPresupuestal.Codigo')
                ->innerJoin('Usuario','Usuario.ID=UsuarioProyecto.UsuarioID')
                ->innerJoin('TipoGasto','TipoGasto.ID=DetalleCompromisoPresupuestal.TipoGasto')
                ->innerJoin('Encargo','Encargo.ID=DetalleCompromisoPresupuestal.Correlativo')
                ->where(['Usuario.ID'=>\Yii::$app->user->id,'DetalleCompromisoPresupuestal.CompromisoPresupuestalID'=>$id])
                ->distinct()
                ->all(); 
            }
            $res = array();
            return $this->render('detalle-uafsi',['resultados'=>$resultados,'cabecera'=>$cabecera,'contab'=>$contab,'res'=>$res,'requerimiento'=>$requerimiento,'detResult'=>$detResult]);
        }
    }
    
    public function actionProcesarCompromisoTecnicoUafsi()
    {
        $this->layout='vacio';
        if(isset($_POST['gastos'])){
            foreach ($_POST['gastos'] as $value) {
                $requerimiento=DetalleCompromisoPresupuestal::findOne($value['dataid']);
                // print_r($requerimiento);
                $requerimiento->GastoElegible = 1;
                $requerimiento->Situacion = 3;
                $requerimiento->update();
            }
        }

        if(isset($_POST['noelegible'])){
            foreach ($_POST['noelegible'] as $valuex) {
                $reqm = DetalleCompromisoPresupuestal::findOne($valuex['dataid']);
                // print_r($reqm);
                $reqm->GastoElegible = 0;
                $reqm->Situacion = 2;
                $reqm->Observacion = $valuex['desc'];
                $reqm->update();
            }
        }

        

        $compromiso = CompromisoPresupuestal::findOne($_POST['ID']);
        $compromiso->Situacion = 16;
        $compromiso->update();
        $tiempo=SeguimientoCompromisoPresupuestal::find()
                    ->innerJoin('Usuario','Usuario.ID=SeguimientoCompromisoPresupuestal.UsuarioID')
                    ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                    ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID and UsuarioRol.RolID=:RolID',[':CompromisoPresupuestalID'=>$compromiso->ID,':RolID'=>2])
                    ->one();
        $tiempo->FechaFin=date('Ymd h:m:s');
        $tiempo->Dias=(int)Yii::$app->tools->RestarFechas($tiempo->FechaInicio,date('Y-m-d h:m:s',strtotime($tiempo->FechaFin)));
        $tiempo->Situacion=8;
        $tiempo->update();
        
        $segu=new SeguimientoCompromisoPresupuestal;
        $segu->CompromisoPresupuestalID=$compromiso->ID;
        $segu->UsuarioID=1118;
        $segu->FechaInicio=date('Ymd h:m:s');
        $segu->Estado=1;
        $segu->Situacion=0;
        $segu->FechaRegistro=date('Ymd h:m:s');
        $segu->save();
        
        $arr = array('Success' => true);
        echo json_encode($arr);
        //print_r($_POST['gastos']);
        die();
        // $requerimiento=CompromisoPresupuestal::findOne($_POST['ID']);
        // $requerimiento->GastoElegible = $_POST['gastos'];
        // $requerimiento->Situacion = 2;
        // $requerimiento->update();
        // $arr = array('Success' => true);
        // echo json_encode($arr);
    }
    
    
    public function actionProcesarCompromisoSeguimientoUafsi()
    {
        $this->layout='vacio';
        $compromiso=CompromisoPresupuestal::findOne($_POST['ID']);
        $compromiso->Situacion = 17;
        $compromiso->update();
        $tiempo=SeguimientoCompromisoPresupuestal::find()
                    ->innerJoin('Usuario','Usuario.ID=SeguimientoCompromisoPresupuestal.UsuarioID')
                    ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                    ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID and UsuarioRol.RolID=:RolID',[':CompromisoPresupuestalID'=>$compromiso->ID,':RolID'=>15])
                    ->one();
        $tiempo->FechaFin=date('Ymd h:m:s');
        $tiempo->Dias=(int)Yii::$app->tools->RestarFechas($tiempo->FechaInicio,date('Y-m-d h:m:s',strtotime($tiempo->FechaFin)));
        $tiempo->Situacion=8;
        $tiempo->update();
        
        $segu=new SeguimientoCompromisoPresupuestal;
        $segu->CompromisoPresupuestalID=$compromiso->ID;
        $segu->UsuarioID=1112;//ID de jefe de uafsi
        $segu->FechaInicio=date('Ymd h:m:s');
        $segu->Estado=1;
        $segu->Situacion=0;
        $segu->FechaRegistro=date('Ymd h:m:s');
        $segu->save();
        
        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
    public function actionProcesarCompromisoJefeUafsi()
    {
        $this->layout='vacio';
        $compromiso=CompromisoPresupuestal::findOne($_POST['ID']);
        $compromiso->Situacion = 18;
        $compromiso->update();
        $tiempo=SeguimientoCompromisoPresupuestal::find()
                    ->innerJoin('Usuario','Usuario.ID=SeguimientoCompromisoPresupuestal.UsuarioID')
                    ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                    ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID and UsuarioRol.RolID=:RolID',[':CompromisoPresupuestalID'=>$compromiso->ID,':RolID'=>11])
                    ->one();
        $tiempo->FechaFin=date('Ymd h:m:s');
        $tiempo->Dias=(int)Yii::$app->tools->RestarFechas($tiempo->FechaInicio,date('Y-m-d h:m:s',strtotime($tiempo->FechaFin)));
        $tiempo->Situacion=8;
        $tiempo->update();
        
        $segu=new SeguimientoCompromisoPresupuestal;
        $segu->CompromisoPresupuestalID=$compromiso->ID;
        $segu->UsuarioID=1104;//ID de jefe de administracion
        $segu->FechaInicio=date('Ymd h:m:s');
        $segu->Estado=1;
        $segu->Situacion=0;
        $segu->FechaRegistro=date('Ymd h:m:s');
        $segu->save();
        
        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
    public function getSituacionJefeAdministracion($situacion=null)
    {
        $situacion=Situacion::findOne($situacion);
        
        if($situacion->ID==18)
        {
            return "Pendiente";
        }
        else
        {
            return "Aprobado";
        }
    }
    
    public function actionAdministracion(){
        $this->layout='estandar';
        
        return $this->render('administracion');
    }
    
    public function actionListadoCompromisoAdministracion()
    {
        $this->layout='vacio';
        $resultado=CompromisoPresupuestal::find()
                    ->select('CompromisoPresupuestal.ID,CompromisoPresupuestal.Correlativo,CompromisoPresupuestal.Annio,CompromisoPresupuestal.Codigo,CompromisoPresupuestal.FechaRegistro,CompromisoPresupuestal.Situacion,TipoGasto.Nombre NombreGasto')
                    ->innerJoin('Requerimiento','Requerimiento.ID=CompromisoPresupuestal.RequerimientoID')
                    ->innerJoin('TipoGasto','TipoGasto.ID=Requerimiento.TipoRequerimiento')
                    ->where('CompromisoPresupuestal.Situacion in (18) AND CompromisoPresupuestal.Estado=1')
                    ->all();
        $nro=0;
        echo '<pre>';
        foreach($resultado as $result)
        {
            $nro++;
            $tiempo=SeguimientoCompromisoPresupuestal::find()
                        ->innerJoin('Usuario','Usuario.ID=SeguimientoCompromisoPresupuestal.UsuarioID')
                        ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                        ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID and UsuarioRol.RolID=12',[':CompromisoPresupuestalID'=>$result["ID"]])
                        ->one();
            
            echo "<tr>";
                echo "<td> N° " . str_pad($result["Correlativo"], 3, "0", STR_PAD_LEFT)  ."-".$result["Annio"]."</td>";
                echo "<td> " . $result["NombreGasto"] . "</td>";
                echo "<td> " . $result["Codigo"] . "</td>";
                echo "<td>" . date('d-m-Y',strtotime($result["FechaRegistro"])) . "</td>";
                echo "<td>" . $this->getSituacionJefeAdministracion($result["Situacion"]) . "</td>";
                if($tiempo)
                {
                    if($tiempo->Situacion==0)
                    {
                        echo "<td>" . Yii::$app->tools->RestarFechas($tiempo->FechaInicio,date('Y-m-d h:m:s')) . "</td>";
                    }
                    elseif($tiempo->Situacion==8)
                    {
                        echo "<td><b>" .Yii::$app->tools->FormatoDemora($tiempo->Dias). " </b></td>";
                    }
                }
                else
                {
                    echo "<td></td>";
                }
                echo "<td>" . $this->getUbicacionCompromisoPresupuestal($result["Situacion"],$result["ID"]) . "</td>";
                
                if($result['Situacion'] == 18){
                    echo "<td>
                    <a href='detalle-administracion?id=".$result["ID"]."' class='btn btn-danger'>Ver detalle</a>
                    <a href='#' class='btn btn-success verifica-aprobar' data-id='".$result['ID']."'>Aprobar</a>
                    </td>";
                }else{
                    echo "<td><a href='detalle-administracion?id=".$result["ID"]."' class='btn btn-info'>Ver detalle</a></td>";
                }
            echo "</tr>";
        }
    }
    
    // Administracion
    public function actionAdministracionAprobado(){
        $this->layout='estandar';
        
        return $this->render('administracion-aprobado');
    }


    public function actionListadoCompromisoAdministracionAprobado()
    {
        date_default_timezone_set('America/Lima');
        $this->layout='vacio';
        $resultado=CompromisoPresupuestal::find()
                    ->select('CompromisoPresupuestal.ID,CompromisoPresupuestal.Correlativo,CompromisoPresupuestal.Annio,CompromisoPresupuestal.Codigo,CompromisoPresupuestal.FechaRegistro,CompromisoPresupuestal.Situacion,TipoGasto.Nombre NombreGasto')
                    ->innerJoin('Requerimiento','Requerimiento.ID=CompromisoPresupuestal.RequerimientoID')
                    ->innerJoin('TipoGasto','TipoGasto.ID=Requerimiento.TipoRequerimiento')
                    ->where('CompromisoPresupuestal.Situacion in (19,20,21) AND CompromisoPresupuestal.Estado=1')
                    ->all();
        $nro=0;
        // echo '<pre>';
        foreach($resultado as $result)
        {
            $nro++;
            $tiempo=SeguimientoCompromisoPresupuestal::find()
                        ->innerJoin('Usuario','Usuario.ID=SeguimientoCompromisoPresupuestal.UsuarioID')
                        ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                        ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID and UsuarioRol.RolID=12',[':CompromisoPresupuestalID'=>$result["ID"]])
                        ->one();
            
            echo "<tr>";
                echo "<td> N° " . str_pad($result["Correlativo"], 3, "0", STR_PAD_LEFT)  ."-".$result["Annio"]."</td>";
                echo "<td> " . $result["NombreGasto"] . "</td>";
                echo "<td> " . $result["Codigo"] . "</td>";
                echo "<td>" . date('d-m-Y',strtotime($result["FechaRegistro"])) . "</td>";
                echo "<td>" . $this->getSituacionJefeAdministracion($result["Situacion"]) . "</td>";
                if($tiempo)
                {
                    if($tiempo->Situacion==0)
                    {
                        echo "<td>" . Yii::$app->tools->RestarFechas($tiempo->FechaInicio,date('Y-m-d h:m:s')) . "</td>";
                    }
                    elseif($tiempo->Situacion==8)
                    {
                        echo "<td><b>" .Yii::$app->tools->FormatoDemora($tiempo->Dias). " </b></td>";
                    }
                }
                else
                {
                    echo "<td></td>";
                }
                echo "<td>" . $this->getUbicacionCompromisoPresupuestal($result["Situacion"],$result["ID"]) . "</td>";
                
                if($result['Situacion'] == 18){
                    echo "<td>
                    <a href='detalle-administracion?id=".$result["ID"]."' class='btn btn-danger'>Ver detalle</a>
                    <a href='#' class='btn btn-success verifica-aprobar' data-id='".$result['ID']."'>Aprobar</a>
                    </td>";
                }else{
                    echo "<td><a href='detalle-administracion?id=".$result["ID"]."' class='btn btn-info'>Ver detalle</a></td>";
                }
            echo "</tr>";
        }
    }
    // --------------
    
    public function actionDetalleAdministracion($id = null,$contab=null){
        $this->layout='estandar';
        if(!is_null($id)){
            $cabecera=CompromisoPresupuestal::find()->where(['ID'=>$id])->one();
            $detReque = DetalleRequerimiento::find()->where(['RequerimientoID'=>$cabecera->RequerimientoID])->all();
            
            $detResult=[];
            foreach ($detReque as $req) {
                if($req->AreSubCategoriaID){
                    $detResult[]=Yii::$app->db->createCommand("
                    SELECT Componente.Correlativo CorrelaCompr,Actividad.Correlativo CorrelaActv,AreSubCategoria.Correlativo CorrelaRecurso,ActRubroElegible.RubroElegibleID,AreSubCategoria.Codificacion,
                        AreSubCategoria.CostoUnitario,AreSubCategoria.Nombre,AreSubCategoria.Especifica,
                        AreSubCategoria.UnidadMedida,AreSubCategoria.CostoUnitario,AreSubCategoria.MetaFisica
                    FROM AreSubCategoria 
                    INNER JOIN ActRubroElegible ON ActRubroElegible.ID = AreSubCategoria.ActRubroElegibleID
                    INNER JOIN RubroElegible ON RubroElegible.ID = ActRubroElegible.RubroElegibleID
                    INNER JOIN Actividad ON ActRubroElegible.ActividadID = Actividad.ID
                    INNER JOIN Componente ON Actividad.ComponenteID = Componente.ID
                    WHERE AreSubCategoria.ID =".$req->AreSubCategoriaID)
                    ->queryOne();
                }
            }
            $requerimiento=Requerimiento::findOne($cabecera->RequerimientoID);
            if($requerimiento->TipoRequerimiento==1 || $requerimiento->TipoRequerimiento==2 || $requerimiento->TipoRequerimiento==3)
            {
               $resultados = (new \yii\db\Query())
                ->select('DetalleCompromisoPresupuestal.ID,
                    DetalleCompromisoPresupuestal.Correlativo,
                    DetalleCompromisoPresupuestal.TipoGasto,
                    DetalleCompromisoPresupuestal.GastoElegible,
                    DetalleCompromisoPresupuestal.Observacion,
                    DetalleCompromisoPresupuestal.CompromisoPresupuestalID,
                    DetalleCompromisoPresupuestal.Situacion,
                    CompromisoPresupuestal.Annio,
                    CompromisoPresupuestal.RequerimientoID,
                    Orden.FechaOrden,
                    Orden.Documento,
                    Orden.TipoProceso,
                    Orden.Monto,
                    Orden.RazonSocial,
                    Orden.RUC,
                    TipoGasto.Nombre, 
                    DetalleCompromisoPresupuestal.CodigoMatriz'
                )
                // Orden.SituacionSIAFT,
                ->from('DetalleCompromisoPresupuestal')
                ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.ID=DetalleCompromisoPresupuestal.CompromisoPresupuestalID')
                ->innerJoin('TipoGasto','TipoGasto.ID=DetalleCompromisoPresupuestal.TipoGasto')
                ->innerJoin('Orden','Orden.ID=DetalleCompromisoPresupuestal.Correlativo')
                ->where(['DetalleCompromisoPresupuestal.CompromisoPresupuestalID'=>$id])
                ->distinct()
                ->all(); 
            }
            elseif($requerimiento->TipoRequerimiento==4)
            {
                $resultados = (new \yii\db\Query())
                ->select('DetalleCompromisoPresupuestal.ID,
                    DetalleCompromisoPresupuestal.Correlativo,
                    DetalleCompromisoPresupuestal.TipoGasto,
                    DetalleCompromisoPresupuestal.GastoElegible,
                    DetalleCompromisoPresupuestal.Observacion,
                    DetalleCompromisoPresupuestal.CompromisoPresupuestalID,
                    DetalleCompromisoPresupuestal.Situacion,
                    CompromisoPresupuestal.Annio,
                    CompromisoPresupuestal.Documento,
                    CompromisoPresupuestal.RequerimientoID,
                    Viatico.Apellido,
                    Viatico.Nombre,
                    Viatico.Dni,
                    Viatico.Monto,
                    TipoGasto.Nombre tipo,
                    Viatico.Documento,
                    DetalleCompromisoPresupuestal.CodigoMatriz'
                )
                ->from('DetalleCompromisoPresupuestal')
                ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.ID=DetalleCompromisoPresupuestal.CompromisoPresupuestalID')
                ->innerJoin('TipoGasto','TipoGasto.ID=DetalleCompromisoPresupuestal.TipoGasto')
                ->innerJoin('Viatico','Viatico.ID=DetalleCompromisoPresupuestal.Correlativo')
                ->where(['DetalleCompromisoPresupuestal.CompromisoPresupuestalID'=>$id])
                ->distinct()
                ->all(); 
            }
            elseif($requerimiento->TipoRequerimiento==5)
            {
                $resultados = (new \yii\db\Query())
                ->select('DetalleCompromisoPresupuestal.ID,
                    DetalleCompromisoPresupuestal.Correlativo,
                    DetalleCompromisoPresupuestal.TipoGasto,
                    DetalleCompromisoPresupuestal.GastoElegible,
                    DetalleCompromisoPresupuestal.Observacion,
                    DetalleCompromisoPresupuestal.CompromisoPresupuestalID,
                    DetalleCompromisoPresupuestal.Situacion,
                    CompromisoPresupuestal.Annio,
                    CompromisoPresupuestal.Documento,
                    CompromisoPresupuestal.RequerimientoID,
                    DetalleCajaChica.Tipo,
                    DetalleCajaChica.Bienes,
                    DetalleCajaChica.Servicios,
                    DetalleCajaChica.Responsable,
                    DetalleCajaChica.ResolucionDirectorial,
                    TipoGasto.Nombre,
                    DetalleCompromisoPresupuestal.CodigoMatriz'
                )
                ->from('DetalleCompromisoPresupuestal')
                ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.ID=DetalleCompromisoPresupuestal.CompromisoPresupuestalID')
                ->innerJoin('TipoGasto','TipoGasto.ID=DetalleCompromisoPresupuestal.TipoGasto')
                ->innerJoin('DetalleCajaChica','DetalleCajaChica.ID=DetalleCompromisoPresupuestal.Correlativo')
                ->where(['DetalleCompromisoPresupuestal.CompromisoPresupuestalID'=>$id])
                ->distinct()
                ->all(); 
            }
            elseif($requerimiento->TipoRequerimiento==6)
            {
                $resultados = (new \yii\db\Query())
                ->select('DetalleCompromisoPresupuestal.ID,
                    DetalleCompromisoPresupuestal.Correlativo,
                    DetalleCompromisoPresupuestal.TipoGasto,
                    DetalleCompromisoPresupuestal.GastoElegible,
                    DetalleCompromisoPresupuestal.Observacion,
                    DetalleCompromisoPresupuestal.CompromisoPresupuestalID,
                    DetalleCompromisoPresupuestal.Situacion,
                    CompromisoPresupuestal.Annio,
                    CompromisoPresupuestal.Documento,
                    CompromisoPresupuestal.RequerimientoID,
                    Encargo.Bienes,
                    Encargo.Servicios,
                    Encargo.NombresEncargo,
                    Encargo.ApellidosEncargo,
                    Encargo.DNIEncargo,
                    Encargo.Documento,
                    TipoGasto.Nombre,
                    DetalleCompromisoPresupuestal.CodigoMatriz'
                )

                ->from('DetalleCompromisoPresupuestal')
                ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.ID=DetalleCompromisoPresupuestal.CompromisoPresupuestalID')
                ->innerJoin('TipoGasto','TipoGasto.ID=DetalleCompromisoPresupuestal.TipoGasto')
                ->innerJoin('Encargo','Encargo.ID=DetalleCompromisoPresupuestal.Correlativo')
                ->where(['DetalleCompromisoPresupuestal.CompromisoPresupuestalID'=>$id])
                ->distinct()
                ->all(); 
            }
            $res = array();
            return $this->render('detalle-administracion',['resultados'=>$resultados,'cabecera'=>$cabecera,'contab'=>$contab,'res'=>$res,'requerimiento'=>$requerimiento,'detResult'=>$detResult]);
        }
    }
    
    public function actionProcesarCompromisoJefeAdministracion()
    {
        $this->layout='vacio';
        $compromiso=CompromisoPresupuestal::findOne($_POST['ID']);
        $compromiso->Situacion = 19;
        $compromiso->update();
        $tiempo=SeguimientoCompromisoPresupuestal::find()
                    ->innerJoin('Usuario','Usuario.ID=SeguimientoCompromisoPresupuestal.UsuarioID')
                    ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                    ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID and UsuarioRol.RolID=:RolID',[':CompromisoPresupuestalID'=>$compromiso->ID,':RolID'=>12])
                    ->one();
        $tiempo->FechaFin=date('Ymd h:m:s');
        $tiempo->Dias=(int)Yii::$app->tools->RestarFechas($tiempo->FechaInicio,date('Y-m-d h:m:s',strtotime($tiempo->FechaFin)));
        $tiempo->Situacion=8;
        $tiempo->update();
        
        $segu=new SeguimientoCompromisoPresupuestal;
        $segu->CompromisoPresupuestalID=$compromiso->ID;
        $segu->UsuarioID=1115;//ID de jefe de contabilidad
        $segu->FechaInicio=date('Ymd h:m:s');
        $segu->Estado=1;
        $segu->Situacion=0;
        $segu->FechaRegistro=date('Ymd h:m:s');
        $segu->save();
        
        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
    
    public function getSituacionJefeContabilidad($situacion=null)
    {
        $situacion=Situacion::findOne($situacion);
        
        if($situacion->ID==19)
        {
            return "Pendiente";
        }
        else
        {
            return "Aprobado";
        }
    }
    
    
    public function actionContabilidad(){
        $this->layout='estandar';
        
        return $this->render('contabilidad');
    }
    
    public function actionListadoCompromisoContabilidad()
    {
        $this->layout='vacio';
        $resultado=CompromisoPresupuestal::find()
                    ->select('CompromisoPresupuestal.ID,CompromisoPresupuestal.Correlativo,CompromisoPresupuestal.Annio,CompromisoPresupuestal.Codigo,CompromisoPresupuestal.FechaRegistro,CompromisoPresupuestal.Situacion,TipoGasto.Nombre NombreGasto')
                    ->innerJoin('Requerimiento','Requerimiento.ID=CompromisoPresupuestal.RequerimientoID')
                    ->innerJoin('TipoGasto','TipoGasto.ID=Requerimiento.TipoRequerimiento')
                    ->where('CompromisoPresupuestal.Situacion in (19,20,21) AND CompromisoPresupuestal.Estado=1')
                    ->all();
        $nro=0;
        echo '<pre>';
        foreach($resultado as $result)
        {
            $nro++;
            $tiempo=SeguimientoCompromisoPresupuestal::find()
                        ->innerJoin('Usuario','Usuario.ID=SeguimientoCompromisoPresupuestal.UsuarioID')
                        ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                        ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID and UsuarioRol.RolID=13',[':CompromisoPresupuestalID'=>$result["ID"]])
                        ->one();
            
            
            echo "<tr>";
                echo "<td> N° " . str_pad($result["Correlativo"], 3, "0", STR_PAD_LEFT)  ."-".$result["Annio"]."</td>";
                echo "<td> " . $result["NombreGasto"] . "</td>";
                echo "<td> " . $result["Codigo"] . "</td>";
                echo "<td>" . date('d-m-Y',strtotime($result["FechaRegistro"])) . "</td>";
                echo "<td>" . $this->getSituacionJefeContabilidad($result["Situacion"]) . "</td>";
                if($tiempo)
                {
                    if($tiempo->Situacion==0)
                    {
                        echo "<td>" . Yii::$app->tools->RestarFechas($tiempo->FechaInicio,date('Y-m-d h:m:s')) . "</td>";
                    }
                    elseif($tiempo->Situacion==8)
                    {
                        echo "<td><b>" .Yii::$app->tools->FormatoDemora($tiempo->Dias). " </b></td>";
                    }
                }
                else
                {
                    echo "<td></td>";
                }
                echo "<td>" . $this->getUbicacionCompromisoPresupuestal($result["Situacion"],$result["ID"]) . "</td>";
                
                if($result['Situacion'] == 19){
                    echo "<td><a href='detalle-contabilidad?id=".$result["ID"]."' class='btn btn-danger'>Ver detalle</a></td>";
                }else{
                    echo "<td><a href='detalle-contabilidad?id=".$result["ID"]."' class='btn btn-info'>Ver detalle</a></td>";
                }
            echo "</tr>";
        }
    }
    
    public function actionDetalleContabilidad($id = null,$contab=null){
        $this->layout='estandar';
        if(!is_null($id)){
            $cabecera=CompromisoPresupuestal::find()->where(['ID'=>$id])->one();
            $EspecialistasAdministrativos=Usuario::find()
                                    ->select('Usuario.ID,Usuario.username,Persona.Nombre, Persona.ApellidoPaterno, Persona.ApellidoMaterno')
                                    ->innerJoin('Persona','Persona.ID=Usuario.PersonaID')
                                    ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                                    ->where('UsuarioRol.RolID=14')
                                    ->all();
            $detReque = DetalleRequerimiento::find()->where(['RequerimientoID'=>$cabecera->RequerimientoID])->all();
            $detResult=[];
            
            foreach ($detReque as $req) {
                if($req->AreSubCategoriaID){
                    $detResult[]=Yii::$app->db->createCommand("
                    SELECT Componente.Correlativo CorrelaCompr,Actividad.Correlativo CorrelaActv,AreSubCategoria.Correlativo CorrelaRecurso,ActRubroElegible.RubroElegibleID,AreSubCategoria.Codificacion,
                        AreSubCategoria.CostoUnitario,AreSubCategoria.Nombre,AreSubCategoria.Especifica,
                        AreSubCategoria.UnidadMedida,AreSubCategoria.CostoUnitario,AreSubCategoria.MetaFisica
                    FROM AreSubCategoria 
                    INNER JOIN ActRubroElegible ON ActRubroElegible.ID = AreSubCategoria.ActRubroElegibleID
                    INNER JOIN RubroElegible ON RubroElegible.ID = ActRubroElegible.RubroElegibleID
                    INNER JOIN Actividad ON ActRubroElegible.ActividadID = Actividad.ID
                    INNER JOIN Componente ON Actividad.ComponenteID = Componente.ID
                    WHERE AreSubCategoria.ID =".$req->AreSubCategoriaID)
                    ->queryOne();
                }
            }
            $requerimiento=Requerimiento::findOne($cabecera->RequerimientoID);
            if($requerimiento->TipoRequerimiento==1 || $requerimiento->TipoRequerimiento==2 || $requerimiento->TipoRequerimiento==3)
            {
               $resultados = (new \yii\db\Query())
                ->select('DetalleCompromisoPresupuestal.ID,
                    DetalleCompromisoPresupuestal.Correlativo,
                    DetalleCompromisoPresupuestal.TipoGasto,
                    DetalleCompromisoPresupuestal.GastoElegible,
                    DetalleCompromisoPresupuestal.Observacion,
                    DetalleCompromisoPresupuestal.CompromisoPresupuestalID,
                    DetalleCompromisoPresupuestal.Situacion,
                    CompromisoPresupuestal.Annio,
                    CompromisoPresupuestal.RequerimientoID,
                    Orden.FechaOrden,
                    Orden.Documento,
                    Orden.TipoProceso,
                    Orden.Monto,
                    Orden.RazonSocial,
                    Orden.RUC,
                    TipoGasto.Nombre, 
                    DetalleCompromisoPresupuestal.CodigoMatriz'
                )
                // Orden.SituacionSIAFT,
                ->from('DetalleCompromisoPresupuestal')
                ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.ID=DetalleCompromisoPresupuestal.CompromisoPresupuestalID')
                ->innerJoin('TipoGasto','TipoGasto.ID=DetalleCompromisoPresupuestal.TipoGasto')
                ->innerJoin('Orden','Orden.ID=DetalleCompromisoPresupuestal.Correlativo')
                ->where(['DetalleCompromisoPresupuestal.CompromisoPresupuestalID'=>$id])
                ->distinct()
                ->all(); 
            }
            elseif($requerimiento->TipoRequerimiento==4)
            {
                $resultados = (new \yii\db\Query())
                ->select('DetalleCompromisoPresupuestal.ID,
                    DetalleCompromisoPresupuestal.Correlativo,
                    DetalleCompromisoPresupuestal.TipoGasto,
                    DetalleCompromisoPresupuestal.GastoElegible,
                    DetalleCompromisoPresupuestal.Observacion,
                    DetalleCompromisoPresupuestal.CompromisoPresupuestalID,
                    DetalleCompromisoPresupuestal.Situacion,
                    CompromisoPresupuestal.Annio,
                    CompromisoPresupuestal.Documento,
                    CompromisoPresupuestal.RequerimientoID,
                    Viatico.Apellido,
                    Viatico.Nombre,
                    Viatico.Dni,
                    Viatico.Monto,
                    TipoGasto.Nombre tipo,
                    Viatico.Documento,
                    DetalleCompromisoPresupuestal.CodigoMatriz'
                )
                ->from('DetalleCompromisoPresupuestal')
                ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.ID=DetalleCompromisoPresupuestal.CompromisoPresupuestalID')
                ->innerJoin('TipoGasto','TipoGasto.ID=DetalleCompromisoPresupuestal.TipoGasto')
                ->innerJoin('Viatico','Viatico.ID=DetalleCompromisoPresupuestal.Correlativo')
                ->where(['DetalleCompromisoPresupuestal.CompromisoPresupuestalID'=>$id])
                ->distinct()
                ->all(); 
            }
            elseif($requerimiento->TipoRequerimiento==5)
            {
                $resultados = (new \yii\db\Query())
                ->select('DetalleCompromisoPresupuestal.ID,
                    DetalleCompromisoPresupuestal.Correlativo,
                    DetalleCompromisoPresupuestal.TipoGasto,
                    DetalleCompromisoPresupuestal.GastoElegible,
                    DetalleCompromisoPresupuestal.Observacion,
                    DetalleCompromisoPresupuestal.CompromisoPresupuestalID,
                    DetalleCompromisoPresupuestal.Situacion,
                    CompromisoPresupuestal.Annio,
                    CompromisoPresupuestal.Documento,
                    CompromisoPresupuestal.RequerimientoID,
                    DetalleCajaChica.Tipo,
                    DetalleCajaChica.Bienes,
                    DetalleCajaChica.Servicios,
                    DetalleCajaChica.Responsable,
                    DetalleCajaChica.ResolucionDirectorial,
                    TipoGasto.Nombre,
                    DetalleCompromisoPresupuestal.CodigoMatriz'
                )
                ->from('DetalleCompromisoPresupuestal')
                ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.ID=DetalleCompromisoPresupuestal.CompromisoPresupuestalID')
                ->innerJoin('TipoGasto','TipoGasto.ID=DetalleCompromisoPresupuestal.TipoGasto')
                ->innerJoin('DetalleCajaChica','DetalleCajaChica.ID=DetalleCompromisoPresupuestal.Correlativo')
                ->where(['DetalleCompromisoPresupuestal.CompromisoPresupuestalID'=>$id])
                ->distinct()
                ->all(); 
            }
            elseif($requerimiento->TipoRequerimiento==6)
            {
                $resultados = (new \yii\db\Query())
                ->select('DetalleCompromisoPresupuestal.ID,
                    DetalleCompromisoPresupuestal.Correlativo,
                    DetalleCompromisoPresupuestal.TipoGasto,
                    DetalleCompromisoPresupuestal.GastoElegible,
                    DetalleCompromisoPresupuestal.Observacion,
                    DetalleCompromisoPresupuestal.CompromisoPresupuestalID,
                    DetalleCompromisoPresupuestal.Situacion,
                    CompromisoPresupuestal.Annio,
                    CompromisoPresupuestal.Documento,
                    CompromisoPresupuestal.RequerimientoID,
                    Encargo.Bienes,
                    Encargo.Servicios,
                    Encargo.NombresEncargo,
                    Encargo.ApellidosEncargo,
                    Encargo.DNIEncargo,
                    Encargo.Documento,
                    TipoGasto.Nombre,
                    DetalleCompromisoPresupuestal.CodigoMatriz'
                )

                ->from('DetalleCompromisoPresupuestal')
                ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.ID=DetalleCompromisoPresupuestal.CompromisoPresupuestalID')
                ->innerJoin('TipoGasto','TipoGasto.ID=DetalleCompromisoPresupuestal.TipoGasto')
                ->innerJoin('Encargo','Encargo.ID=DetalleCompromisoPresupuestal.Correlativo')
                ->where(['DetalleCompromisoPresupuestal.CompromisoPresupuestalID'=>$id])
                ->distinct()
                ->all(); 
            }
            $res = array();
            return $this->render('detalle-contabilidad',['resultados'=>$resultados,'cabecera'=>$cabecera,'contab'=>$contab,'res'=>$res,'requerimiento'=>$requerimiento,'detResult'=>$detResult,'EspecialistasAdministrativos'=>$EspecialistasAdministrativos]);
        }
    }

    public function actionExcelContabilidad(){

        $resultado=CompromisoPresupuestal::find()
        ->select('CompromisoPresupuestal.ID,CompromisoPresupuestal.Correlativo,CompromisoPresupuestal.Annio,CompromisoPresupuestal.Codigo,CompromisoPresupuestal.FechaRegistro,CompromisoPresupuestal.Situacion,TipoGasto.Nombre NombreGasto,TipoGasto.ID NombreID,DetalleRequerimiento.TipoDetalleOrdenID,sum(DetalleRequerimiento.Total) Total')
        ->innerJoin('Requerimiento','Requerimiento.ID=CompromisoPresupuestal.RequerimientoID')
        ->innerJoin('TipoGasto','TipoGasto.ID=Requerimiento.TipoRequerimiento')
        ->innerJoin('DetalleRequerimiento','DetalleRequerimiento.RequerimientoID = Requerimiento.ID')
        ->where('CompromisoPresupuestal.Situacion in (20,21) AND CompromisoPresupuestal.Estado=1')
        ->groupBy('CompromisoPresupuestal.ID,CompromisoPresupuestal.Correlativo,CompromisoPresupuestal.Annio,CompromisoPresupuestal.Codigo,CompromisoPresupuestal.FechaRegistro,CompromisoPresupuestal.Situacion,TipoGasto.Nombre,TipoGasto.ID,DetalleRequerimiento.TipoDetalleOrdenID')
        ->orderBy('TipoGasto.ID')
        ->all();

       

        // $detReque = DetalleRequerimiento::find()->where(['RequerimientoID'=>$cabecera->RequerimientoID])->all();
        $detResult=[];
        
        // foreach ($detReque as $req) {
        //     if($req->AreSubCategoriaID){
        //         $detResult[]=Yii::$app->db->createCommand("
        //         SELECT Componente.Correlativo CorrelaCompr,Actividad.Correlativo CorrelaActv,AreSubCategoria.Correlativo CorrelaRecurso,ActRubroElegible.RubroElegibleID,AreSubCategoria.Codificacion,
        //             AreSubCategoria.CostoUnitario,AreSubCategoria.Nombre,AreSubCategoria.Especifica,
        //             AreSubCategoria.UnidadMedida,AreSubCategoria.CostoUnitario,AreSubCategoria.MetaFisica
        //         FROM AreSubCategoria 
        //         INNER JOIN ActRubroElegible ON ActRubroElegible.ID = AreSubCategoria.ActRubroElegibleID
        //         INNER JOIN RubroElegible ON RubroElegible.ID = ActRubroElegible.RubroElegibleID
        //         INNER JOIN Actividad ON ActRubroElegible.ActividadID = Actividad.ID
        //         INNER JOIN Componente ON Actividad.ComponenteID = Componente.ID
        //         WHERE AreSubCategoria.ID =".$req->AreSubCategoriaID)
        //         ->queryOne();
        //     }
        // }
        return $this->render('excel-contabilidad',['resultado'=>$resultado]);
    }

    
    public function actionProcesarCompromisoJefeContabilidad()
    {
        $this->layout='vacio';
        $compromiso=CompromisoPresupuestal::findOne($_POST['ID']);
        $compromiso->Situacion = 20;
        $compromiso->update();
        $tiempo=SeguimientoCompromisoPresupuestal::find()
                    ->innerJoin('Usuario','Usuario.ID=SeguimientoCompromisoPresupuestal.UsuarioID')
                    ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                    ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID and UsuarioRol.RolID=:RolID',[':CompromisoPresupuestalID'=>$compromiso->ID,':RolID'=>13])
                    ->one();
        $tiempo->FechaFin=date('Ymd h:m:s');
        $tiempo->Dias=(int)Yii::$app->tools->RestarFechas($tiempo->FechaInicio,date('Y-m-d h:m:s',strtotime($tiempo->FechaFin)));
        $tiempo->Situacion=8;
        $tiempo->update();
        
        $segu=new SeguimientoCompromisoPresupuestal;
        $segu->CompromisoPresupuestalID=$compromiso->ID;
        $segu->UsuarioID=$_POST['EspecialistaAdministrativoID'];//ID de especialista administrativo
        $segu->FechaInicio=date('Ymd h:m:s');
        $segu->Estado=1;
        $segu->Situacion=0;
        $segu->FechaRegistro=date('Ymd h:m:s');
        $segu->save();
        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    

    /* Jefe de administrativo */
    public function getSituacionEspecialistaAdministrativo($situacion=null)
    {
        $situacion=Situacion::findOne($situacion);
        if($situacion->ID==20)
        {
            return "Pendiente";
        }
        else
        {
            return "Aprobado";
        }
    }
    

    // Especialista administrativo
    public function actionEspecialistaAdministrativo(){
        $this->layout='estandar';
        return $this->render('especialista-administrativo');
    }
    
    
    public function actionListadoCompromisoEspecialistaAdministrativo()
    {
        $this->layout='vacio';
        $resultados=CompromisoPresupuestal::find()
                    ->select('CompromisoPresupuestal.ID,CompromisoPresupuestal.Correlativo,CompromisoPresupuestal.Annio,CompromisoPresupuestal.Codigo,CompromisoPresupuestal.FechaRegistro,CompromisoPresupuestal.Situacion,TipoGasto.Nombre NombreGasto,CompromisoPresupuestal.NotasCompromiso')
                    ->innerJoin('Requerimiento','Requerimiento.ID=CompromisoPresupuestal.RequerimientoID')
                    ->innerJoin('TipoGasto','TipoGasto.ID=Requerimiento.TipoRequerimiento')
                    ->innerJoin('SeguimientoCompromisoPresupuestal','SeguimientoCompromisoPresupuestal.CompromisoPresupuestalID=CompromisoPresupuestal.ID')
                    ->where('CompromisoPresupuestal.Situacion in (20) AND CompromisoPresupuestal.Estado=1 and SeguimientoCompromisoPresupuestal.UsuarioID='.\Yii::$app->user->id.'')
                    ->all();
        $nro=0;
        // echo '<pre>';
        foreach($resultados as $result)
        {
            $nro++;
            $tiempo=SeguimientoCompromisoPresupuestal::find()
                        ->innerJoin('Usuario','Usuario.ID=SeguimientoCompromisoPresupuestal.UsuarioID')
                        ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                        ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID and UsuarioRol.RolID=14',[':CompromisoPresupuestalID'=>$result["ID"]])
                        ->one();
            
            echo "<tr>";

                if(!empty($result['NotasCompromiso'])){
                    $notas = " - <a href='' data-toggle='tooltip' data-original-title='".$result['NotasCompromiso']."' class='btn btn-warning btn-xs'>Notas</a>";
                }else{
                    $notas = "";
                }
                echo "<td> N° " . str_pad($result["Correlativo"], 3, "0", STR_PAD_LEFT)."-".$result["Annio"].$notas." </td>";
                echo "<td> " . $result["NombreGasto"] . "</td>";
                echo "<td> " . $result["Codigo"] . "</td>";
                echo "<td>" . date('d-m-Y',strtotime($result["FechaRegistro"])) . "</td>";
                echo "<td>" . $this->getSituacionEspecialistaAdministrativo($result["Situacion"]) . "</td>";
                if($tiempo)
                {
                    if($tiempo->Situacion==0)
                    {
                        echo "<td>" . Yii::$app->tools->RestarFechas($tiempo->FechaInicio,date('Y-m-d h:m:s')) . "</td>";
                    }
                    elseif($tiempo->Situacion==8)
                    {
                        echo "<td><b>" .Yii::$app->tools->FormatoDemora($tiempo->Dias). " </b></td>";
                    }
                }
                else
                {
                    echo "<td></td>";
                }
                
                
                if($result['Situacion'] == 20){
                    $btnEstado = 'btn-danger';;
                }else{
                    $btnEstado = 'btn-info';
                }
                
                    // <a href='#' class='btn btn-warning btn-previsualizar'>Previsualizar</a>
                echo "<td>
                    <a href='detalle-especialista-administrativo?id=".$result["ID"]."' class='btn ".$btnEstado." btn-sm'>Ver detalle</a>
                    </td>";
            echo "</tr>";
        }
    }
    
    public function actionEspecialistaAdministrativoAprobado(){
        $this->layout='estandar';
        return $this->render('especialista-administrativo-aprobado');
    }
    
    
    public function actionListadoCompromisoEspecialistaAdministrativoOk()
    {
        $this->layout='vacio';
        $resultados=CompromisoPresupuestal::find()
                    ->select('CompromisoPresupuestal.ID,CompromisoPresupuestal.Correlativo,CompromisoPresupuestal.Annio,CompromisoPresupuestal.Codigo,CompromisoPresupuestal.FechaRegistro,CompromisoPresupuestal.Situacion,TipoGasto.Nombre NombreGasto,CompromisoPresupuestal.NotasCompromiso')
                    ->innerJoin('Requerimiento','Requerimiento.ID=CompromisoPresupuestal.RequerimientoID')
                    ->innerJoin('TipoGasto','TipoGasto.ID=Requerimiento.TipoRequerimiento')
                    ->innerJoin('SeguimientoCompromisoPresupuestal','SeguimientoCompromisoPresupuestal.CompromisoPresupuestalID=CompromisoPresupuestal.ID')
                    ->where('CompromisoPresupuestal.Situacion in (21) AND CompromisoPresupuestal.Estado=1 and SeguimientoCompromisoPresupuestal.UsuarioID='.\Yii::$app->user->id.'')
                    ->all();
        $nro=0;
        echo '<pre>';
        foreach($resultados as $result)
        {
            $nro++;
            $tiempo=SeguimientoCompromisoPresupuestal::find()
                        ->innerJoin('Usuario','Usuario.ID=SeguimientoCompromisoPresupuestal.UsuarioID')
                        ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                        ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID and UsuarioRol.RolID=14',[':CompromisoPresupuestalID'=>$result["ID"]])
                        ->one();
            
            echo "<tr>";

                if(!empty($result['NotasCompromiso'])){
                    $notas = " - <a href='' data-toggle='tooltip' data-original-title='".$result['NotasCompromiso']."' class='btn btn-warning btn-xs'>Notas</a>";
                }else{
                    $notas = "";
                }
                echo "<td> N° " . str_pad($result["Correlativo"], 3, "0", STR_PAD_LEFT)."-".$result["Annio"].$notas." </td>";
                echo "<td> " . $result["NombreGasto"] . "</td>";
                echo "<td> " . $result["Codigo"] . "</td>";
                echo "<td>" . date('d-m-Y',strtotime($result["FechaRegistro"])) . "</td>";
                echo "<td>" . $this->getSituacionEspecialistaAdministrativo($result["Situacion"]) . "</td>";
                if($tiempo)
                {
                    if($tiempo->Situacion==0)
                    {
                        echo "<td>" . Yii::$app->tools->RestarFechas($tiempo->FechaInicio,date('Y-m-d h:m:s')) . "</td>";
                    }
                    elseif($tiempo->Situacion==8)
                    {
                        echo "<td><b>" .Yii::$app->tools->FormatoDemora($tiempo->Dias). " </b></td>";
                    }
                }
                else
                {
                    echo "<td></td>";
                }
                
                
                if($result['Situacion'] == 20){
                    $btnEstado = 'btn-danger';;
                }else{
                    $btnEstado = 'btn-info';
                }
                
                    // <a href='#' class='btn btn-warning btn-previsualizar'>Previsualizar</a>
                echo "<td>
                    <a href='detalle-especialista-administrativo?id=".$result["ID"]."' class='btn ".$btnEstado." btn-sm'>Ver detalle</a>
                    </td>";
            echo "</tr>";
        }
    }

    // -------------------------------
    
    public function actionDetalleEspecialistaAdministrativo($id = null,$contab=null)
    {
        $this->layout='estandar';
        if(!is_null($id)){
            $cabecera=CompromisoPresupuestal::find()
                ->select('CompromisoPresupuestal.ID,
                CompromisoPresupuestal.GastoElegible,
                CompromisoPresupuestal.Correlativo,
                CompromisoPresupuestal.Codigo,
                CompromisoPresupuestal.MontoTotal,
                CompromisoPresupuestal.Situacion,
                CompromisoPresupuestal.UsuarioAsignado,
                CompromisoPresupuestal.RequerimientoID,
                Persona.Nombre,
                Persona.ApellidoPaterno,
                Persona.ApellidoMaterno,
                CompromisoPresupuestal.CompromisoAnual,
                CompromisoPresupuestal.CompromisoAdministrativo,
                CompromisoPresupuestal.Devengado,
                CompromisoPresupuestal.Formato,
                CompromisoPresupuestal.FormatoUafsi,
                CompromisoPresupuestal.NotasCompromiso
                ')
            ->innerJoin('Usuario','Usuario.ID = CompromisoPresupuestal.UsuarioAsignado')
            ->innerJoin('Persona','Persona.ID=Usuario.PersonaID')
            ->where('CompromisoPresupuestal.ID=:ID AND Situacion IN ( 20,21 )',[':ID'=>$id])
            ->one();
            $requerimiento=Requerimiento::findOne($cabecera->RequerimientoID);
            
            if($requerimiento->TipoRequerimiento==1 || $requerimiento->TipoRequerimiento==2 || $requerimiento->TipoRequerimiento==3)
            {
                $resultados = (new \yii\db\Query())
                    ->select('DetalleCompromisoPresupuestal.ID DetalleCompromisoPresupuestalID,DetalleCompromisoPresupuestal.*,CompromisoPresupuestal.*,TipoGasto.Nombre,Orden.*')
                    ->from('DetalleCompromisoPresupuestal')
                    ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.ID=DetalleCompromisoPresupuestal.CompromisoPresupuestalID')
                    ->innerJoin('TipoGasto','TipoGasto.ID=DetalleCompromisoPresupuestal.TipoGasto')
                    ->innerJoin('Orden','Orden.ID=DetalleCompromisoPresupuestal.Correlativo')
                    ->where(['DetalleCompromisoPresupuestal.CompromisoPresupuestalID'=>$id])
                    ->distinct()
                    ->all();
            }
            elseif($requerimiento->TipoRequerimiento==4){
                $resultados = (new \yii\db\Query())
                    ->select('DetalleCompromisoPresupuestal.ID DetalleCompromisoPresupuestalID,DetalleCompromisoPresupuestal.*,CompromisoPresupuestal.*,TipoGasto.Nombre,Viatico.*,Viatico.Nombre Nombres')
                    ->from('DetalleCompromisoPresupuestal')
                    ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.ID=DetalleCompromisoPresupuestal.CompromisoPresupuestalID')
                    ->innerJoin('TipoGasto','TipoGasto.ID=DetalleCompromisoPresupuestal.TipoGasto')
                    ->innerJoin('Viatico','Viatico.ID=DetalleCompromisoPresupuestal.Correlativo')
                    ->where(['DetalleCompromisoPresupuestal.CompromisoPresupuestalID'=>$id,'Viatico.Estado'=>1])
                    ->distinct()
                    ->all();
            }
            elseif($requerimiento->TipoRequerimiento==5){
                $resultados = (new \yii\db\Query())
                    ->select('DetalleCompromisoPresupuestal.ID DetalleCompromisoPresupuestalID,DetalleCompromisoPresupuestal.*,CompromisoPresupuestal.*,TipoGasto.Nombre,DetalleCajaChica.*')
                    ->from('DetalleCompromisoPresupuestal')
                    ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.ID=DetalleCompromisoPresupuestal.CompromisoPresupuestalID')
                    ->innerJoin('TipoGasto','TipoGasto.ID=DetalleCompromisoPresupuestal.TipoGasto')
                    ->innerJoin('DetalleCajaChica','DetalleCajaChica.ID=DetalleCompromisoPresupuestal.Correlativo')
                    ->where(['DetalleCompromisoPresupuestal.CompromisoPresupuestalID'=>$id])
                    ->distinct()
                    ->all();
            }
            elseif($requerimiento->TipoRequerimiento==6)
            {
                $resultados = (new \yii\db\Query())
                    ->select('DetalleCompromisoPresupuestal.ID DetalleCompromisoPresupuestalID,DetalleCompromisoPresupuestal.*,CompromisoPresupuestal.*,TipoGasto.Nombre,Encargo.*')
                    ->from('DetalleCompromisoPresupuestal')
                    ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.ID=DetalleCompromisoPresupuestal.CompromisoPresupuestalID')
                    ->innerJoin('TipoGasto','TipoGasto.ID=DetalleCompromisoPresupuestal.TipoGasto')
                    ->innerJoin('Encargo','Encargo.ID=DetalleCompromisoPresupuestal.Correlativo')
                    ->where(['DetalleCompromisoPresupuestal.CompromisoPresupuestalID'=>$id])
                    ->distinct()
                    ->all();
            }
            $res = (new \yii\db\Query())
            ->select('Persona.ApellidoPaterno,Persona.ApellidoMaterno,Persona.Nombre,Persona.ID')
            ->from('Persona')
            ->innerJoin('Usuario','Usuario.PersonaID=Persona.ID')
            ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID = Usuario.ID')
            ->where(['UsuarioRol.RolID'=>14])
            ->distinct()
            ->all();

            
            
            return $this->render('detalle-especialista-administrativo',['resultados'=>$resultados,'cabecera'=>$cabecera,'contab'=>$contab,'res'=>$res,'requerimiento'=>$requerimiento]);
        }
    }
    
    public function actionRetraerCompromisoPresupuestalUafsi($id=null)
    {
        $CompromisoPresupuestal=CompromisoPresupuestal::findOne($id);
        $CompromisoPresupuestal->Situacion = 14;
        $CompromisoPresupuestal->UsuarioAsignado = NULL;
        $CompromisoPresupuestal->update();
        \Yii::$app->db->createCommand()->delete('SeguimientoCompromisoPresupuestal', ['CompromisoPresupuestalID' => $CompromisoPresupuestal->ID])->execute();
        
        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
    public function actionRetraerCompromisoPresupuestalRequerimientoUafsi($id=null)
    {
        $CompromisoPresupuestal=CompromisoPresupuestal::findOne($id);
        $compromisoPresupuestal->Situacion=6;
        $CompromisoPresupuestal->Estado=0;
        $CompromisoPresupuestal->update();        
        $UsuarioSupervisorUafsi = UsuarioProyecto::find()
                    ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=UsuarioProyecto.UsuarioID')
                    ->where(['CodigoProyecto'=>$CompromisoPresupuestal->Codigo,'UsuarioRol.RolID'=>2])
                    ->one();
        \Yii::$app->db->createCommand()->delete('SeguimientoCompromisoPresupuestal', ['CompromisoPresupuestalID' => $CompromisoPresupuestal->ID,'UsuarioID'=>$UsuarioSupervisorUafsi->ID])->execute();
        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
    public function actionFinalizarCompromiso($ID=null,$CodigoProyecto=null)
    {
        if(isset($_POST['ID']) && isset($_POST['CodigoProyecto']) && $_POST['ID']!='' && $_POST['CodigoProyecto']!='')
        {
            $ID=$_POST['ID'];
            $CodigoProyecto=$_POST['CodigoProyecto'];
            $CompromisoPresupuestal=CompromisoPresupuestal::findOne($ID);
        
            $tiempo=SeguimientoCompromisoPresupuestal::find()
                        ->innerJoin('Usuario','Usuario.ID=SeguimientoCompromisoPresupuestal.UsuarioID')
                        ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                        ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID and UsuarioRol.RolID=:RolID',[':CompromisoPresupuestalID'=>$CompromisoPresupuestal->ID,':RolID'=>14])
                        ->one();
            $tiempo->FechaFin=date('Ymd h:m:s');
            $tiempo->Dias=(int)Yii::$app->tools->RestarFechas($tiempo->FechaInicio,date('Y-m-d h:m:s',strtotime($tiempo->FechaFin)));
            $tiempo->Situacion=8;
            $tiempo->update();
            $CompromisoPresupuestal->Situacion=21;
            $CompromisoPresupuestal->update();
            $arr = array('Success' => true);
            echo json_encode($arr);
        }
    }

    public function actionRehacerCompromiso($ID=null,$CodigoProyecto=null)
    {
        if(isset($_POST['ID']) && isset($_POST['CodigoProyecto']) && $_POST['ID']!='' && $_POST['CodigoProyecto']!='')
        {
            $ID=$_POST['ID'];
            $CodigoProyecto=$_POST['CodigoProyecto'];
            $CompromisoPresupuestal=CompromisoPresupuestal::findOne($ID);
        
            $tiempo=SeguimientoCompromisoPresupuestal::find()
                        ->innerJoin('Usuario','Usuario.ID=SeguimientoCompromisoPresupuestal.UsuarioID')
                        ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                        ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID and UsuarioRol.RolID=:RolID',[':CompromisoPresupuestalID'=>$CompromisoPresupuestal->ID,':RolID'=>14])
                        ->one();
            $tiempo->FechaFin=date('Ymd h:m:s');
            $tiempo->Dias=(int)Yii::$app->tools->RestarFechas($tiempo->FechaInicio,date('Y-m-d h:m:s',strtotime($tiempo->FechaFin)));
            $tiempo->Situacion=0;
            $tiempo->update();

            $CompromisoPresupuestal->Situacion=20;
            $CompromisoPresupuestal->update();
            $arr = array('Success' => true);
            echo json_encode($arr);
        }
    }

    public function actionRehacerCompromisoUafsi($ID=null,$CodigoProyecto=null)
    {
        if(isset($_POST['ID']) && isset($_POST['CodigoProyecto']) && $_POST['ID']!='' && $_POST['CodigoProyecto']!='')
        {
            $ID=$_POST['ID'];
            $CodigoProyecto=$_POST['CodigoProyecto'];
            $CompromisoPresupuestal=CompromisoPresupuestal::findOne($ID);
        
            $tiempo=SeguimientoCompromisoPresupuestal::find()
                        ->innerJoin('Usuario','Usuario.ID=SeguimientoCompromisoPresupuestal.UsuarioID')
                        ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                        ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID and UsuarioRol.RolID=:RolID',[':CompromisoPresupuestalID'=>$CompromisoPresupuestal->ID,':RolID'=>14])
                        ->one();
            $tiempo->FechaFin=date('Ymd h:m:s');
            $tiempo->Dias=(int)Yii::$app->tools->RestarFechas($tiempo->FechaInicio,date('Y-m-d h:m:s',strtotime($tiempo->FechaFin)));
            $tiempo->Situacion=0;
            $tiempo->update();

            $CompromisoPresupuestal->Situacion=15;
            $CompromisoPresupuestal->update();
            $arr = array('Success' => true);
            echo json_encode($arr);
        }
    }

    public function actionObservarCompromiso($ID=null,$DetalleCompromisoID=null,$CodigoProyecto=null,$Tipo=null)
    {
        $this->layout='vacio';
        $observar=ObservacionGeneral::find()
                    ->where('CodigoID=:CodigoID and CodigoProyecto=:CodigoProyecto and TipoObservacion=20',[':CodigoID'=>$DetalleCompromisoID,':CodigoProyecto'=>$CodigoProyecto])
                    ->one();

        if(!$observar)
        {
            $observar=new ObservacionGeneral;
        }
        
        if($observar->load(Yii::$app->request->post())){
            $observar->CodigoID=$DetalleCompromisoID;
            $observar->CodigoProyecto=$CodigoProyecto;
            $observar->Estado=1;
            $observar->TipoObservacion=20;
            
            // echo '<pre>';print_r($observar); die();

            $observar->save();

            $compromiso = DetalleCompromisoPresupuestal::findOne($observar->CodigoID);
            $compPresupuestal = CompromisoPresupuestal::findOne($compromiso->CompromisoPresupuestalID);
            // $compPresupuestal->Situacion = 4;
            $compPresupuestal->save();

            // Tiempo 
            $tiempo=SeguimientoCompromisoPresupuestal::find()
                    ->innerJoin('Usuario','Usuario.ID=SeguimientoCompromisoPresupuestal.UsuarioID')
                    ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                    ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID and UsuarioRol.RolID=:RolID',[':CompromisoPresupuestalID'=>$ID,':RolID'=>14])
                    ->one();
            $tiempo->FechaFin=date('Ymd h:m:s');
            $tiempo->Dias=(int)Yii::$app->tools->RestarFechas($tiempo->FechaInicio,date('Y-m-d h:m:s',strtotime($tiempo->FechaFin)));
            $tiempo->Situacion=8;
            $tiempo->update();
            
            $informacion=InformacionGeneral::find()
                    ->where('Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])
                    ->one();

            $segu=new SeguimientoCompromisoPresupuestal;
            $segu->CompromisoPresupuestalID = $ID;
            $segu->UsuarioID = $informacion->ID;
            $segu->FechaInicio = date('Ymd h:m:s');
            $segu->Estado = 1;
            $segu->Situacion = 0;
            $segu->FechaRegistro = date('Ymd h:m:s');
            $segu->save();
            // End tiempo

            return $this->redirect(['detalle-especialista-administrativo','id'=>$ID]);
        }
        return $this->render('observar-compromiso',['Tipo'=>$Tipo,'ID'=>$ID,'observar'=>$observar,'DetalleCompromisoID'=>$DetalleCompromisoID,'CodigoProyecto'=>$CodigoProyecto]);
    }
    
    public function actionObservarCompromisoJefeContabilidad($ID=null,$CodigoProyecto=null,$Tipo=null)
    {
        $this->layout='vacio';
        $observar=ObservacionGeneral::find()
                    ->where('CodigoID=:CodigoID and CodigoProyecto=:CodigoProyecto and TipoObservacion=19',[':CodigoID'=>$ID,':CodigoProyecto'=>$CodigoProyecto])
                    ->one();
        if(!$observar)
        {
            $observar=new ObservacionGeneral;
        }
        
        if($observar->load(Yii::$app->request->post())){
            $observar->CodigoID=$ID;
            $observar->CodigoProyecto=$CodigoProyecto;
            $observar->Estado=1;
            $observar->TipoObservacion=19;
            $observar->save();
            return $this->redirect(['detalle-contabilidad','id'=>$ID]);
        }
        return $this->render('observar-compromiso-jefe-contabilidad',['Tipo'=>$Tipo,'ID'=>$ID,'observar'=>$observar,'CodigoProyecto'=>$CodigoProyecto]);
    }
    
    public function actionEliminarDerivacion($UsuarioID=null,$CodigoProyecto=null,$CompromisoPresupuestalID=null)
    {
        $compromiso=CompromisoPresupuestal::findOne($CompromisoPresupuestalID);
        $compromiso->Situacion = 19;
        $compromiso->update();
        
        \Yii::$app->db->createCommand()->delete('SeguimientoCompromisoPresupuestal', ['CompromisoPresupuestalID' => $compromiso->ID,'Situacion'=>0])->execute();
        
        $tiempo=SeguimientoCompromisoPresupuestal::find()
                    ->innerJoin('Usuario','Usuario.ID=SeguimientoCompromisoPresupuestal.UsuarioID')
                    ->innerJoin('UsuarioRol','UsuarioRol.UsuarioID=Usuario.ID')
                    ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID and UsuarioRol.RolID=:RolID',[':CompromisoPresupuestalID'=>$compromiso->ID,':RolID'=>13])
                    ->one();
        $tiempo->FechaFin=NULL;
        $tiempo->Dias=0;
        $tiempo->Situacion=0;
        $tiempo->update();
        
        $arr = array('Success' => true);
            echo json_encode($arr);
        
    }


    // Crear notas para personal administrativo
    public function actionCrearNota($ID=null,$CodigoProyecto=null,$Tipo=null)
    {
        $this->layout='vacio';
        $compromiso=CompromisoPresupuestal::find()
                    ->where('ID=:ID',[':ID'=>$ID])
                    ->one();
        if(!$compromiso)
        {
            $compromiso=new CompromisoPresupuestal;
        }
        
        if($compromiso->load(Yii::$app->request->post())){
            $compromiso->save();
            return $this->redirect(['detalle-especialista-administrativo','id'=>$ID]);
        }
        return $this->render('observar-nota',['ID'=>$ID,'compromiso'=>$compromiso]);
    }


}
