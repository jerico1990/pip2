<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\InformacionGeneral;
use app\models\ProgramaTransversal;
use app\models\DireccionLinea;
use app\models\TipoInvestigacion;
use app\models\UnidadEjecutora;
use app\models\UnidadOperativa;
use app\models\Persona;
use app\models\Programa;
use app\models\CultivoCrianza;
use app\models\Especie;
use app\models\Ubigeo;
use app\models\Usuario;
use app\models\Investigador;
use app\models\EntidadParticipante;
use app\models\TipoInstitucion;
use app\models\AmbitoIntervencion;
use app\models\MiembroEquipoGestion;
use app\models\MarcoLogicoFinProyecto;
use app\models\MarcoLogicoPropositoProyecto;
use app\models\Proyecto;
use app\models\Poa;
use app\models\MarcoLogicoActividad;
use app\models\Actividad;
use app\models\Componente;
use app\models\MarcoLogicoComponente;
use app\models\NivelAprobacion;
use app\models\Seguimiento;
use app\models\AreSubCategoria;
use app\models\AreSubCategoriaObservacion;
use app\models\TareaObservacion;
use app\models\Tarea;
use app\models\Pat;
use app\models\Pac;
use app\models\PacDetalle;
use app\models\PacObservacion;
use app\models\Situacion;
use app\models\UsuarioRol;

use yii\db\Query;



class ListadoProyectoController extends Controller
{
    /**
     * @inheritdoc
     */
    

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($investigador=null)
    {
        $this->layout='estandar';
        
        return $this->render('index');
    }
    
    public function actionListaProyectos()
    {
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $usuarioRol=UsuarioRol::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->ID])->one();
        
        if($usuarioRol->RolID==2)
        {
            $resultados = (new \yii\db\Query())
                ->select(['Investigador.ID','Usuario.username as username','InformacionGeneral.TituloProyecto','InformacionGeneral.Meses','Poa.Estado','Poa.Situacion'])
                ->from('InformacionGeneral')
                ->innerJoin('Investigador','InformacionGeneral.InvestigadorID=Investigador.ID')
                ->innerJoin('Usuario','Usuario.ID=Investigador.UsuarioID')
                ->innerJoin('Proyecto','Proyecto.InvestigadorID=Investigador.ID')
                ->innerJoin('Poa','Poa.ProyectoID=Proyecto.ID')
                ->innerJoin('UsuarioProyecto','UsuarioProyecto.CodigoProyecto=InformacionGeneral.Codigo')
                ->where('UsuarioProyecto.UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->ID])
                ->orderBy('Situacion desc','username','TituloProyecto','Meses') 
                ->distinct()
                ->all();
            $nro=0;
            foreach($resultados as $result)
            {
                $nro++;
                echo "<tr>";
                echo "<td>" . $result["username"] . "</td>";
                echo "<td>" . mb_substr ($result["TituloProyecto"],0,100) . "</td>";
                echo "<td>" . $result["Meses"] . "</td>";
                echo "<td>".$this->Accion($result["Situacion"],$result["ID"])."</td>";
                echo "</tr>";
            }
        }
        else
        {
            $resultados = (new \yii\db\Query())
                ->select(['Investigador.ID','Usuario.username as username','InformacionGeneral.TituloProyecto','InformacionGeneral.Meses','Poa.Estado','Poa.Situacion'])
                ->from('InformacionGeneral')
                ->innerJoin('Investigador','InformacionGeneral.InvestigadorID=Investigador.ID')
                ->innerJoin('Usuario','Usuario.ID=Investigador.UsuarioID')
                ->innerJoin('Proyecto','Proyecto.InvestigadorID=Investigador.ID')
                ->innerJoin('Poa','Poa.ProyectoID=Proyecto.ID')
                ->innerJoin('UsuarioProyecto','UsuarioProyecto.CodigoProyecto=InformacionGeneral.Codigo')
                ->where('UsuarioProyecto.UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->ID])
                ->orderBy('Situacion desc','username','TituloProyecto','Meses') 
                ->distinct()
                ->all();
            $nro=0;
            foreach($resultados as $result)
            {
                $nro++;
                echo "<tr>";
                echo "<td>" . $result["username"] . "</td>";
                echo "<td>" . mb_substr ($result["TituloProyecto"],0,100) . "</td>";
                echo "<td>" . $result["Meses"] . "</td>";
                echo "<td>".$this->Accion($result["Situacion"],$result["ID"])."</td>";
                echo "</tr>";
            }
        }
        
        
    }
    public function Accion($situacion=null,$id=null)
    {
        //$descripcion='';
        //if($situacion==2)
        //{
            $descripcion='<a class="btn btn-primary" href="listado-proyecto/evaluacion-proyecto?investigadorID='.$id.'">Revisar</a>';
        //}
        return $descripcion;
    }

    
    public function Situacion($codigo)
    {
        $situacion=Situacion::find()->where('Codigo=:Codigo',[':Codigo'=>$codigo])->one();
        return $situacion->Descripcion;
        // $descripcion='';
        // if($situacion==1)
        // {
        //     $descripcion='Aprobado';
        // }
        // elseif($situacion==2)
        // {
        //     $descripcion='Pendiente';
        // }
        // elseif($situacion==3)
        // {
        //     $descripcion='Observado';
        // }
        // elseif($situacion==4)
        // {
        //     $descripcion='Inicializado';
        // }
        // return $descripcion;
    }
    
    public function actionEvaluacionProyecto($investigadorID=null)
    {
	    $this->layout='estandar';
        if($investigadorID==NULL)
        {
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        }
        else
        {
            $investigador=Investigador::findOne($investigadorID);
            $usuario=Usuario::findOne($investigador->UsuarioID);
	    $datosInvestigador=Persona::findOne($usuario->PersonaID);
        }
    	$informacionGeneral=$this->InformacionGeneral($investigador->ID);
    	$proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID and Estado=1',[':ProyectoID'=>$proyecto->ID])->one();
    	$pat=Pat::find()->where('ProyectoID=:ProyectoID and Estado=1',[':ProyectoID'=>$proyecto->ID])->one();
        $componentes=Componente::find()->where('PoaID=:PoaID',[':PoaID'=>$poa->ID])->all();
    	$ObservacionesRecursos=Componente::find()
					->select('AreSubCategoriaObservacion.*')
					->innerJoin('Actividad','Actividad.ComponenteID=Componente.ID')
					->innerJoin('ActRubroElegible','ActRubroElegible.ActividadID=Actividad.ID')
					->innerJoin('AreSubCategoria','AreSubCategoria.ActRubroElegibleID=ActRubroElegible.ID')
					->innerJoin('AreSubCategoriaObservacion','AreSubCategoriaObservacion.AreSubCategoriaID=AreSubCategoria.ID')
					->where('Componente.PoaID=:PoaID and AreSubCategoriaObservacion.Estado in (1,3)',[':PoaID'=>$poa->ID])
					->all();
	    $ObservacionesTareas=Componente::find()
					->select('TareaObservacion.*')
					->innerJoin('Actividad','Actividad.ComponenteID=Componente.ID')
					->innerJoin('Tarea','Tarea.ActividadID=Actividad.ID')
					->innerJoin('TareaObservacion','TareaObservacion.TareaID=Tarea.ID')
					->where('Componente.PoaID=:PoaID and TareaObservacion.Estado in (1,3)',[':PoaID'=>$poa->ID])
					->all();
					
    	$situacionProyecto=$proyecto->Situacion;
    	$situacionRecurso=$poa->Situacion;
    	$situacionTarea=$pat->Situacion;
    	$seguimiento=Seguimiento::find()->where('Situacion=1 and EtapaCodigo=:EtapaCodigo and NivelAprobacionCorrelativo=:NivelAprobacionCorrelativo',
    						[':EtapaCodigo'=>'01',':NivelAprobacionCorrelativo'=>1])
    					->one();
    	
    	$seguimientosCount=Seguimiento::find()->where('Situacion=1 and ProyectoCodigo=:ProyectoCodigo and  EtapaCodigo=:EtapaCodigo and NivelAprobacionCorrelativo=:NivelAprobacionCorrelativo',
						[':EtapaCodigo'=>'01',':NivelAprobacionCorrelativo'=>1,':ProyectoCodigo'=>$informacionGeneral->Codigo])
					->count();
	
	
    	if($seguimientosCount>0)
    	{
    	    $NivelAprobacionCorrelativo=2;
    	}
    	else
    	{
    	    $NivelAprobacionCorrelativo=1;
    	}
        return $this->render('evaluacion-proyecto',
			    ['componentes'=>$componentes,
			     'informacion'=>$informacionGeneral,
			     'poa'=>$poa,
			     'investigadorID'=>$investigador->ID,
			     'situacionProyecto'=>$situacionProyecto,
			     'situacionRecurso'=>$situacionRecurso,
			     'situacionTarea'=>$situacionTarea,
			     'datosInvestigador'=>$datosInvestigador,
			     'seguimientosCount'=>$seguimientosCount,
			     'NivelAprobacionCorrelativo'=>$NivelAprobacionCorrelativo,
			     'ObservacionesRecursos'=>$ObservacionesRecursos,
			     'ObservacionesTareas'=>$ObservacionesTareas
			     ]);
    }
    
    public function InformacionGeneral($investigador=null)
    {
        $model=InformacionGeneral::find()
                ->select('InformacionGeneral.*,UnidadOperativa.UnidadEjecutoraID,Especie.CultivoCrianzaID,CultivoCrianza.ProgramaID,Departamento.ID as DepartamentoID,Provincia.ID as ProvinciaID,Proyecto.Fin,Proyecto.Proposito')
                ->innerJoin('Investigador','Investigador.ID=InformacionGeneral.InvestigadorID')
                ->innerJoin('Proyecto','Proyecto.InvestigadorID=Investigador.ID')
                ->innerJoin('UnidadOperativa','UnidadOperativaID=UnidadOperativa.ID')
                ->innerJoin('Especie','Especie.ID=InformacionGeneral.EspecieID')
                ->innerJoin('CultivoCrianza','CultivoCrianza.ID=Especie.CultivoCrianzaID')
                ->innerJoin('Ubigeo Distrito','Distrito.ID=InformacionGeneral.DistritoID')
                ->innerJoin('Ubigeo Provincia','Provincia.ID=SUBSTRING(InformacionGeneral.DistritoID, 1, 4)')
                ->innerJoin('Ubigeo Departamento','Departamento.ID=SUBSTRING(InformacionGeneral.DistritoID, 1, 2)')
                ->where('InformacionGeneral.InvestigadorID=:InvestigadorID',['InvestigadorID'=>$investigador])
                ->one();
                
        if(!$model)
        {
            $model=new InformacionGeneral;
        }
        return $model;
    }
    
    public function actionAprobarProyecto($aprobar=null,$codigo=null)
    {
        if($aprobar==1)
        {            
            $usuario=Usuario::find()->where('username=:username',[':username'=>$codigo])->one();
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=$this->InformacionGeneral($investigador->ID);
	    $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
	    $pat=Pat::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
	    if(\Yii::$app->user->identity->Rol==5)
	    {
		$seguimiento=Seguimiento::find()->where('NivelAprobacionCorrelativo=1 and ProyectoCodigo=:ProyectoCodigo and EtapaCodigo=:EtapaCodigo',['ProyectoCodigo'=>$informacionGeneral->Codigo,':EtapaCodigo'=>'01'])->one();
		$seguimiento->Situacion=1;
		$seguimiento->update();
		$updateTareaObservacion = Yii::$app->db->createCommand(' UPDATE TareaObservacion SET Estado=0
							FROM TareaObservacion
							inner join Tarea on Tarea.ID=TareaObservacion.TareaID
							inner join Actividad on Actividad.ID=Tarea.ActividadID
							inner join Componente on Componente.ID=Actividad.ComponenteID
							inner join Poa on Poa.ID=Componente.PoaID
							where Poa.ID='.$poa->ID);
		$updateTareaObservacion->execute();
		
		$updateRecursoObservacion = Yii::$app->db->createCommand(' UPDATE AreSubCategoriaObservacion SET Estado=0
							FROM AreSubCategoriaObservacion
							inner join AreSubCategoria on AreSubCategoria.ID=AreSubCategoriaObservacion.AreSubCategoriaID
							inner join ActRubroElegible on ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID
							inner join Actividad on Actividad.ID=ActRubroElegible.ActividadID
							inner join Componente on Componente.ID=Actividad.ComponenteID
							inner join Poa on Poa.ID=Componente.PoaID
							where Poa.ID='.$poa->ID);
		$updateRecursoObservacion->execute();
	    }
	    else if(\Yii::$app->user->identity->Rol==2)
	    {
		$seguimiento=Seguimiento::find()->where('NivelAprobacionCorrelativo=2 and ProyectoCodigo=:ProyectoCodigo and EtapaCodigo=:EtapaCodigo',['ProyectoCodigo'=>$informacionGeneral->Codigo,':EtapaCodigo'=>'01'])->one();
		$seguimiento->Situacion=1;
		$seguimiento->update();
		$proyecto->Situacion=1;
		$proyecto->update();
		$poa->Situacion=1;
		$poa->update();
		$pat->Situacion=1;
		$pat->update();
		$updateTarea = Yii::$app->db->createCommand(' UPDATE Tarea SET Situacion=1
							FROM Tarea
							inner join Actividad on Actividad.ID=Tarea.ActividadID
							inner join Componente on Componente.ID=Actividad.ComponenteID
							inner join Poa on Poa.ID=Componente.PoaID
							where Poa.ID='.$poa->ID);
		$updateTarea->execute();
		$updateRecurso = Yii::$app->db->createCommand(' UPDATE AreSubCategoria SET Situacion=1
							FROM AreSubCategoria
							inner join ActRubroElegible on ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID
							inner join Actividad on Actividad.ID=ActRubroElegible.ActividadID
							inner join Componente on Componente.ID=Actividad.ComponenteID
							inner join Poa on Poa.ID=Componente.PoaID
							where Poa.ID='.$poa->ID);
		$updateRecurso->execute();
		
		$updateTareaObservacion = Yii::$app->db->createCommand(' UPDATE TareaObservacion SET Estado=0
							FROM TareaObservacion
							inner join Tarea on Tarea.ID=TareaObservacion.TareaID
							inner join Actividad on Actividad.ID=Tarea.ActividadID
							inner join Componente on Componente.ID=Actividad.ComponenteID
							inner join Poa on Poa.ID=Componente.PoaID
							where Poa.ID='.$poa->ID);
		$updateTareaObservacion->execute();
		
		$updateRecursoObservacion = Yii::$app->db->createCommand(' UPDATE AreSubCategoriaObservacion SET Estado=0
							FROM AreSubCategoriaObservacion
							inner join AreSubCategoria on AreSubCategoria.ID=AreSubCategoriaObservacion.AreSubCategoriaID
							inner join ActRubroElegible on ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID
							inner join Actividad on Actividad.ID=ActRubroElegible.ActividadID
							inner join Componente on Componente.ID=Actividad.ComponenteID
							inner join Poa on Poa.ID=Componente.PoaID
							where Poa.ID='.$poa->ID);
		$updateRecursoObservacion->execute();
		
	    }
	    
        }
    }
    
    public function actionObservarProyecto($ProyectoCodigo=null,$NivelAprobacionCorrelativo=null)
    {
    	$this->layout='vacio';
    	$nivelAprobacion=NivelAprobacion::find()->where('RolID=:RolID and EtapaCodigo=:EtapaCodigo',[':RolID'=>\Yii::$app->user->identity->Rol,':EtapaCodigo'=>'01'])->one();
    	$model=Seguimiento::find()
		    ->where('ProyectoCodigo=:ProyectoCodigo and NivelAprobacionCorrelativo=:NivelAprobacionCorrelativo',
			    [':ProyectoCodigo'=>$ProyectoCodigo,':NivelAprobacionCorrelativo'=>$nivelAprobacion->Correlativo])
		    ->one();
    	if ( $model->load(Yii::$app->request->post()) ) {
    	    $model->Situacion=3;
    	    $model->update();
    	    
    	    $tareas=Tarea::find()
    		    ->innerJoin('Actividad','Actividad.ID=Tarea.ActividadID')
    		    ->innerJoin('Componente','Componente.ID=Actividad.ComponenteID')
    		    ->innerJoin('Poa','Poa.ID=Componente.PoaID')
    		    ->innerJoin('Proyecto','Proyecto.ID=Poa.ProyectoID')
    		    ->innerJoin('Investigador','Investigador.ID=Proyecto.InvestigadorID')
    		    ->innerJoin('InformacionGeneral','InformacionGeneral.InvestigadorID=Investigador.ID')
    		    ->where('InformacionGeneral.Codigo=:Codigo and Tarea.Situacion=3',[':Codigo'=>$ProyectoCodigo])
    		    ->count();
    	    
    	    $tareas=Tarea::find()
    		    ->innerJoin('Actividad','Actividad.ID=Tarea.ActividadID')
    		    ->innerJoin('Componente','Componente.ID=Actividad.ComponenteID')
    		    ->innerJoin('Poa','Poa.ID=Componente.PoaID')
    		    ->innerJoin('Proyecto','Proyecto.ID=Poa.ProyectoID')
    		    ->innerJoin('Investigador','Investigador.ID=Proyecto.InvestigadorID')
    		    ->innerJoin('InformacionGeneral','InformacionGeneral.InvestigadorID=Investigador.ID')
    		    ->where('InformacionGeneral.Codigo=:Codigo and Tarea.Situacion=3',[':Codigo'=>$ProyectoCodigo])
    		    ->count();
    	    
    	    $recursos=AreSubCategoria::find()
    		    ->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')
    		    ->innerJoin('Actividad','Actividad.ID=ActRubroElegible.ActividadID')
    		    ->innerJoin('Componente','Componente.ID=Actividad.ComponenteID')
    		    ->innerJoin('Poa','Poa.ID=Componente.PoaID')
    		    ->innerJoin('Proyecto','Proyecto.ID=Poa.ProyectoID')
    		    ->innerJoin('Investigador','Investigador.ID=Proyecto.InvestigadorID')
    		    ->innerJoin('InformacionGeneral','InformacionGeneral.InvestigadorID=Investigador.ID')
    		    ->where('InformacionGeneral.Codigo=:Codigo and AreSubCategoria.Situacion=3',[':Codigo'=>$ProyectoCodigo])
    		    ->count();
    		    
    	    $informacionGeneral=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$ProyectoCodigo])->one();
    	    $investigador=Investigador::findOne($informacionGeneral->InvestigadorID);
    	    $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
    	    $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
    	    $pat=Pat::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
    	    $proyecto->Situacion=3;
    	    $proyecto->update();
    	    /*if($tareas>0)
    	    {*/
    		$pat->Situacion=3;
    		$pat->update();
    	    //}
    	    //if($recursos>0)
    	    //{
    		$poa->Situacion=3;
    		$poa->update();
    	    //}
	    
	    return $this->redirect(['evaluacion-proyecto/evaluacion-proyecto','investigadorID'=>$informacionGeneral->InvestigadorID]);
    	}
	   return $this->render('observar-proyecto',['model'=>$model,'ProyectoCodigo'=>$ProyectoCodigo,'NivelAprobacionCorrelativo'=>$NivelAprobacionCorrelativo]);
    }
    
    public function actionObservarRecurso($RecursoID=null)
    {
    	$this->layout='vacio';
    	$recurso=AreSubCategoria::findOne($RecursoID);
    	$model=AreSubCategoriaObservacion::find()->where('AreSubCategoriaID=:AreSubCategoriaID and Estado=1',[':AreSubCategoriaID'=>$RecursoID])->one();
    	if(!$model)
    	{
    	    $model=new AreSubCategoriaObservacion;
    	}
	
    	if($model->load(Yii::$app->request->post()))
    	{
    	    $recurso->Situacion=3;
    	    $recurso->update();
    	    $model->Estado=1;
    	    $model->AreSubCategoriaID=$RecursoID;
    	    $model->save();
	    $arr = array(
		'Success' => true,
	    );
	    echo json_encode($arr);
	    die;
    	}
    	return $this->render('observar-recurso',['model'=>$model,'RecursoID'=>$RecursoID]);
    }
    
    public function actionObservarTarea($TareaID=null)
    {
    	$this->layout='vacio';
    	$tarea=Tarea::findOne($TareaID);
    	$model=TareaObservacion::find()->where('TareaID=:TareaID and Estado=1',[':TareaID'=>$TareaID])->one();
    	if(!$model)
    	{
    	    $model=new TareaObservacion;
    	}
	
    	if($model->load(Yii::$app->request->post()))
    	{
    	    $tarea->Situacion=3;
    	    $tarea->update();
    	    $model->Estado=1;
    	    $model->TareaID=$TareaID;
    	    $model->save();
	    $arr = array(
		'Success' => true,
	    );
	    echo json_encode($arr);
	    die;
    	}
    	return $this->render('observar-tarea',['model'=>$model,'TareaID'=>$TareaID]);
    }

    // -------------------------- PLAN ADQUISICIONES 
    public function AccionPa($id,$codigo)
    {
        $descripcion='';
        $descripcion='<a class="btn btn-primary" href="evaluacion-proyecto-pa?investigadorID='.$id.'&codigo='.$codigo.'">Evaluar PAC</a>';
        return $descripcion;
    }

    public function actionAprobacionPa(){
        $this->layout='estandar';
        return $this->render('evaluacion-pa');
    }

    public function actionListaProyectosPa()
    {
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $usuarioRol=UsuarioRol::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->ID])->one();
        echo $usuarioRol->RolID;
        if($usuarioRol->RolID==2)
        {
            // $resultados = (new \yii\db\Query())
            //     ->select(['Investigador.ID','Usuario.username as username','InformacionGeneral.TituloProyecto','InformacionGeneral.Meses','Pac.Estado','Pac.Situacion'])
            //     ->from('InformacionGeneral')
            //     ->innerJoin('Investigador','InformacionGeneral.InvestigadorID=Investigador.ID')
            //     ->innerJoin('Usuario','Usuario.ID=Investigador.UsuarioID')
            //     ->innerJoin('Proyecto','Proyecto.InvestigadorID=Investigador.ID')
            //     ->innerJoin('Pac','Pac.ProyectoID=Proyecto.ID')
            //     ->innerJoin('UsuarioProyecto','UsuarioProyecto.CodigoProyecto=InformacionGeneral.Codigo')
            //     ->where('UsuarioProyecto.UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->ID])
            //     // ->where(['Pac.Situacion'=>2])
            //     ->orderBy('Situacion desc','username','TituloProyecto','Meses') 
            //     ->distinct()
            //     ->all();
                
            // $nro=0;
            // foreach($resultados as $result)
            // {
            //     $nro++;
            //     echo "<tr>";
            //     echo "<td>" . $result["username"] . "</td>";
            //     echo "<td>" . mb_substr ($result["TituloProyecto"],0,100) . "</td>";
            //     echo "<td>" . $result["Meses"] . "</td>";
            //     // echo "<td>" . $this->Situacion($result["Situacion"]) . "</td>";
            //     echo "<td>" . $this->AccionPa($result["Situacion"],$result["ID"],$result["username"])."</td>";
            //     echo "</tr>";
            // }
        }
        else
        {
        } 
            $resultados = (new \yii\db\Query())
                ->select(['Investigador.ID','Usuario.username as username','InformacionGeneral.TituloProyecto','InformacionGeneral.Meses'])
                ->from('InformacionGeneral')
                ->innerJoin('Investigador','InformacionGeneral.InvestigadorID=Investigador.ID')
                ->innerJoin('Usuario','Usuario.ID=Investigador.UsuarioID')
                ->innerJoin('Proyecto','Proyecto.InvestigadorID=Investigador.ID')
                // ->innerJoin('Pac','Pac.ProyectoID=Proyecto.ID')
                // ->where(['Pac.Situacion'=>2])
                ->orderBy('Situacion desc','username','TituloProyecto','Meses') 
                // ->distinct()
                ->all();
    
            $nro=0;
            foreach($resultados as $result)
            {
                $nro++;
                echo "<tr>";
                echo "<td>" . $result["username"] . "</td>";
                echo "<td>" . mb_substr ($result["TituloProyecto"],0,100) . "</td>";
                echo "<td>" . $result["Meses"] . "</td>";
                // echo "<td>" . $this->Situacion($result["Situacion"]) . "</td>";
                echo "<td>" . $this->AccionPa($result["ID"],$result["username"])."</td>";
                echo "</tr>";
            }
            
        
    }

    public function actionEvaluacionProyectoPa($investigadorID=null,$anio = null,$codigo = null)
    {
       $this->layout='estandar';
        if($investigadorID==NULL)
        {
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        }
        else
        {
            $investigador=Investigador::findOne($investigadorID);
            $usuario=Usuario::findOne($investigador->UsuarioID);
        }
    
        $informacion=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        
        $pac=Pac::find()
        ->select('Pac.ID,Pac.PoaID,Pac.Situacion,Pac.Anio,Pac.Observacion,Pac.ProyectoID,Situacion.Descripcion,Situacion.Codigo')
        ->innerJoin('Situacion','Situacion.ID = Pac.Situacion')
        ->where('ProyectoID=:ProyectoID AND Situacion !=0',[':ProyectoID'=>$proyecto->ID])->all();
        
        // echo '<pre>';
        // print_r($pac);
        // die();
        return $this->render('evaluacion-proyecto-pa',
                [
                 'codigos'=>$codigo,
                 'pac'=>$pac,
                 'informacion' => $informacion
                ]);
    }

    public function actionAprobarProyectoPa($aprobar=null,$anio=null,$codigo = null)
    {
        if($aprobar==1)
        {            
            $poa=Pac::find()->where(['Anio'=>$anio,'ProyectoID'=>$codigo])->one();
            $poa->Situacion=1;
            $poa->update();
        }
    }

    public function actionAprobarProyectoPaCentral()
    {
        $central = $_POST['cental'];
        $jsonCentral = json_decode($central);


        foreach ($jsonCentral as $value) {
            $det = PacDetalle::find()->where(['ID'=>$value->ID])->one();
            $det->TipoOrden=$value->CEN;
            $det->update();
        }

        $anio = $_POST['anio'];
        $codigo = $_POST['codigo'];

        $poa=Pac::find()->where(['Anio'=>$anio,'ProyectoID'=>$codigo])->one();
        $poa->Situacion=3;
        $poa->update();
    }


    public function actionSeguimientoPa($anio = null,$codigo = null){
        $this->layout='vacio';
        // $usuario=Usuario::findOne(\Yii::$app->user->id);
        $usuario=Usuario::find()->where('username=:username',[':username'=>$codigo])->one();
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();

        $aax = Pac::find()
            ->where(['Anio'=>$anio,'ProyectoID'=>$proyecto->ID])
            ->one();
        // print_r($aax);
        // die();
	   
        $detalle = PacDetalle::find()
            ->select('AreSubCategoria.Nombre,AreSubCategoria.UnidadMedida,AreSubCategoria.CostoUnitario,Categoria.Nombre as Categoria,PacDetalle.Cantidad,PacDetalle.MetodoAdquisicion,PacDetalle.FechaInicioPA,EntidadParticipante.RazonSocial,EntidadParticipante.AporteMonetario,PacDetalle.ID,Categoria.ID Codigo,PacDetalle.Situacion,PacDetalle.NumeracionPAC,PacDetalle.Observaciones,PacDetalle.Codificacion,PacDetalle.TipoOrden,Categoria.ID CategoriaID')
            ->leftJoin('AreSubCategoria','AreSubCategoria.ID=PacDetalle.AreSubCategoriaID')
            ->leftJoin('Categoria','Categoria.ID=PacDetalle.CategoriaID')
            ->leftJoin('Pac','Pac.ID=PacDetalle.PacID')
            ->leftJoin('EntidadParticipante','EntidadParticipante.ID=AreSubCategoria.EntidadParticipanteID')
            ->where(['PacDetalle.PacID'=>$aax->ID])
            ->orderBy('PacDetalle.CategoriaID')
            ->all();

	
	
	
        return $this->render('seguimiento_pa',['detallepa' => $detalle,'anio'=>$anio,'Situacion'=>$aax->Situacion ]);
    }


    public function actionObservarPac($RecursoID=null)
    {
        $this->layout='vacio';
        $recurso=PacDetalle::findOne($RecursoID);

        $model=PacObservacion::find()->where('AreSubCategoriaID=:AreSubCategoriaID and Estado=1',[':AreSubCategoriaID'=>$RecursoID])->one();
        

        if(!$model)
        {
            $model=new PacObservacion;
        }
    
        if($model->load(Yii::$app->request->post()))
        {
            
            $recurso->Situacion=3;
            $recurso->update();
            $model->Estado=1;
            $model->AreSubCategoriaID=$RecursoID;
            $model->save();
            $arr = array(
            'Success' => true,
            );
            echo json_encode($arr);
            die;
        }
        return $this->render('observar-pac',['model'=>$model,'RecursoID'=>$RecursoID]);
    }


}