<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Usuario;
use app\models\Investigador;
use app\models\Persona;
use app\models\Proyecto;
use app\models\InformacionGeneral;
use app\models\UnidadOperativa;
use app\models\UnidadEjecutora;
use app\models\EntidadParticipante;
use app\models\Componente;
use app\models\Poa;
use app\models\Pat;
use app\models\Pac;
use app\models\Seguimiento;
use app\models\PasoCritico;
use app\models\Actividad;
use app\models\Evento;
use app\models\Experimento;
use app\models\Rol;
use app\models\UsuarioRol;
use app\models\Ubigeo;

class PanelController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout='estandar';
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $roles = Rol::find()
        ->select('Rol.Nombre')
        ->innerJoin('UsuarioRol','UsuarioRol.RolID = Rol.ID')
        ->innerJoin('Usuario','Usuario.ID = UsuarioRol.UsuarioID')
        ->where(['Usuario.ID'=>\Yii::$app->user->id])->one();
        // die();
        $departamentos=Ubigeo::find()->where('len(ID) = 2')->all();

        if(\Yii::$app->user->identity->rol==7){
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $db = Yii::$app->db;
        
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            //$informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $pasoCritico=PasoCritico::find()->where('ProyectoID=:ProyectoID and Situacion=2 and Estado=1',[':ProyectoID'=>$proyecto->ID])->one();
            $poa=Poa::find()->where('ProyectoID=:ProyectoID and Estado=1',[':ProyectoID'=>$proyecto->ID])->one();
            $pat=Pat::find()->where('ProyectoID=:ProyectoID and Estado=1',[':ProyectoID'=>$proyecto->ID])->one();
            $informacionGeneral=InformacionGeneral::find()
                ->select('InformacionGeneral.*,UnidadOperativa.UnidadEjecutoraID,Especie.CultivoCrianzaID,CultivoCrianza.ProgramaID,Departamento.ID as DepartamentoID,Departamento.Nombre as DepartamentoNombre,Provincia.ID as ProvinciaID,Proyecto.Fin,Proyecto.Proposito,Proyecto.Situacion')
                ->innerJoin('Investigador','Investigador.ID=InformacionGeneral.InvestigadorID')
                ->innerJoin('Proyecto','Proyecto.InvestigadorID=Investigador.ID')
                ->innerJoin('UnidadOperativa','UnidadOperativaID=UnidadOperativa.ID')
                ->innerJoin('Especie','Especie.ID=InformacionGeneral.EspecieID')
                ->innerJoin('CultivoCrianza','CultivoCrianza.ID=Especie.CultivoCrianzaID')
                ->innerJoin('Ubigeo Distrito','Distrito.ID=InformacionGeneral.DistritoID')
                ->innerJoin('Ubigeo Provincia','Provincia.ID=SUBSTRING(InformacionGeneral.DistritoID, 1, 4)')
                ->innerJoin('Ubigeo Departamento','Departamento.ID=SUBSTRING(InformacionGeneral.DistritoID, 1, 2)')
                ->where('InformacionGeneral.InvestigadorID=:InvestigadorID',['InvestigadorID'=>$investigador->ID])
                ->one();
            $even_exper=Actividad::find()
            ->select('sum(Actividad.Experimento) Experimento,sum(Actividad.Evento) Evento')
            ->innerJoin('Componente','Componente.ID=Actividad.ComponenteID')
            ->where('Actividad.Estado=1 and Componente.PoaID=:PoaID',[':PoaID'=>$poa->ID])
            ->one();
            $evento=Evento::find()->where('CodigoProyecto=:CodigoProyecto and Estado=1',[':CodigoProyecto'=>$informacionGeneral->Codigo])->count();
            $experimento=Experimento::find()->where('CodigoProyecto=:CodigoProyecto and Estado=1',[':CodigoProyecto'=>$informacionGeneral->Codigo])->count();
            $unidadOperativa=UnidadOperativa::findOne($informacionGeneral->UnidadOperativaID);
            $unidadEjecutora=UnidadEjecutora::findOne($unidadOperativa->UnidadEjecutoraID);
            $colaboradores=EntidadParticipante::find()->where('InvestigadorID=:InvestigadorID and Estado=1',[':InvestigadorID'=>$investigador->ID])->all();
            $MontoTotalPnia=$db->createCommand("
                    SELECT sum(AreSubCategoria.CostoUnitario*AreSubCategoria.MetaFisica) MontoTotalPnia,
                    sum(AreSubCategoria.CostoUnitarioEjecutada*AreSubCategoria.MetaFisicaEjecutada) MontoTotalPniaEjecutada
                    FROM RubroElegible
                    INNER JOIN ActRubroElegible ON ActRubroElegible.RubroElegibleID=RubroElegible.ID
                    INNER JOIN AreSubCategoria ON AreSubCategoria.ActRubroElegibleID=ActRubroElegible.ID
                    INNER JOIN Actividad ON Actividad.ID=ActRubroElegible.ActividadID
                    INNER JOIN Componente ON Componente.ID=Actividad.ComponenteID
                    INNER JOIN Poa ON Poa.ID=Componente.PoaID
                    INNER JOIN Proyecto ON Proyecto.ID=Poa.ProyectoID
                    INNER JOIN InformacionGeneral ON InformacionGeneral.InvestigadorID=Proyecto.InvestigadorID
                    WHERE InformacionGeneral.Codigo='".$informacionGeneral->Codigo."'")->queryOne();
        
            $seguimientosCount=Seguimiento::find()->where('Situacion=1 and ProyectoCodigo=:ProyectoCodigo and  EtapaCodigo=:EtapaCodigo and NivelAprobacionCorrelativo=:NivelAprobacionCorrelativo',
                        [':EtapaCodigo'=>'01',':NivelAprobacionCorrelativo'=>1,':ProyectoCodigo'=>$informacionGeneral->Codigo])
                    ->count();
                    
            $seguimientosCountT=Seguimiento::find()->where('Situacion=0 and ProyectoCodigo=:ProyectoCodigo and  EtapaCodigo=:EtapaCodigo',
                        [':EtapaCodigo'=>'01',':ProyectoCodigo'=>$informacionGeneral->Codigo])
                    ->count();
                    
            $NivelAprobacionCorrelativo=0;
            if($seguimientosCount && ($proyecto->Situacion==0  || $proyecto->Situacion==2 || $proyecto->Situacion==3)){
                if($seguimientosCount==2)
                {
                    $NivelAprobacionCorrelativo=2;
                }
                elseif($seguimientosCount==1)
                {
                    $NivelAprobacionCorrelativo=1;
                }
                elseif($seguimientosCount==0)
                {
                    $NivelAprobacionCorrelativo=3;
                }
            }elseif($seguimientosCount && $proyecto->Situacion==1){
                $NivelAprobacionCorrelativo=2;
            }
        
            if($seguimientosCountT && $seguimientosCountT==2)
            {
              $NivelAprobacionCorrelativo=1;
            }
            $ObservacionesRecursos=Componente::find()
                    ->select('AreSubCategoriaObservacion.*')
                    ->innerJoin('Actividad','Actividad.ComponenteID=Componente.ID')
                    ->innerJoin('ActRubroElegible','ActRubroElegible.ActividadID=Actividad.ID')
                    ->innerJoin('AreSubCategoria','AreSubCategoria.ActRubroElegibleID=ActRubroElegible.ID')
                    ->innerJoin('AreSubCategoriaObservacion','AreSubCategoriaObservacion.AreSubCategoriaID=AreSubCategoria.ID')
                    ->where('Componente.PoaID=:PoaID and AreSubCategoriaObservacion.Estado in (1,3)',[':PoaID'=>$poa->ID])
                    ->all();
            $ObservacionesTareas=Componente::find()
                    ->select('TareaObservacion.*')
                    ->innerJoin('Actividad','Actividad.ComponenteID=Componente.ID')
                    ->innerJoin('Tarea','Tarea.ActividadID=Actividad.ID')
                    ->innerJoin('TareaObservacion','TareaObservacion.TareaID=Tarea.ID')
                    ->where('Componente.PoaID=:PoaID and TareaObservacion.Estado in (1,3)',[':PoaID'=>$poa->ID])
                    ->all();
            return $this->render('panel-proyecto',
                 ['informacionGeneral'=>$informacionGeneral,
                  'unidadEjecutora'=>$unidadEjecutora,
                  'proyecto'=>$proyecto,
                  'departamentos'=>$departamentos,
                  'colaboradores'=>$colaboradores,
                  'ObservacionesRecursos'=>$ObservacionesRecursos,
                  'ObservacionesTareas'=>$ObservacionesTareas,
                  'NivelAprobacionCorrelativo'=>$NivelAprobacionCorrelativo,
                  'pasoCritico'=>$pasoCritico,
                  'even_exper'=>$even_exper,
                  'experimento'=>$experimento,
                  'evento'=>$evento,
                  'MontoTotalPnia'=>$MontoTotalPnia,
                  'usuario'=>$usuario]);
        }
        /* everth 
        elseif(\Yii::$app->user->identity->rol==11)
        {
            $estacion = (new \yii\db\Query())
                ->select('UnidadEjecutora.ID,UnidadEjecutora.Nombre AS Estacion,Count(Investigador.ID) AS Proyecto')
                ->from('Investigador')
                ->innerJoin('InformacionGeneral','InformacionGeneral.InvestigadorID = Investigador.ID')
                ->innerJoin('UnidadOperativa','InformacionGeneral.UnidadOperativaID = UnidadOperativa.ID')
                ->innerJoin('UnidadEjecutora','UnidadOperativa.UnidadEjecutoraID = UnidadEjecutora.ID')
                ->groupBy('UnidadEjecutora.ID,UnidadEjecutora.Nombre')
                ->all();


            foreach ($estacion as $est) {
            $proyectosEstacion[] = array(
                'Estacion' => $est['Estacion'],
                'ProyectoCant' => $this->detaEeaa($est['ID'])
                );
            }
            return $this->render('reporte',['roles'=>$roles,'estacion'=>$estacion,'cantUnidad'=>$proyectosEstacion]);
        }
        */
       elseif(\Yii::$app->user->identity->rol==16 || \Yii::$app->user->identity->rol==2 || \Yii::$app->user->identity->rol==11)
       {
            return $this->render('ReporteBI',['roles'=>$roles]);
       }

        return $this->render('index',['roles'=>$roles,'departamentos'=>$departamentos]);
    }

    public function actionProyectos()
    {
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $this->layout='estandar';
        $roles = Rol::find()
        ->select('Rol.Nombre')
        ->innerJoin('UsuarioRol','UsuarioRol.RolID = Rol.ID')
        ->innerJoin('Usuario','Usuario.ID = UsuarioRol.UsuarioID')
        ->where(['Usuario.ID'=>\Yii::$app->user->id])->one();
        // die();
        $departamentos=Ubigeo::find()->where('len(ID) = 2')->all();
        return $this->render('mapa-geolocaliza',['roles'=>$roles,'departamentos'=>$departamentos]);
    }

    public function actionGeoMapa($dep = null, $prov = null, $dist = null){
        $this->layout='vacio';
        $informacion = array();
        $programa = array();
        $listadoPrograma = array();

        if ( $dep != '') {
            $informacion=InformacionGeneral::find()
            ->where("SUBSTRING ( InformacionGeneral.DistritoID, 1, 2 ) = :dep AND InformacionGeneral.Latitud != '' AND InformacionGeneral.Longitud != '' ",[':dep'=>$dep])
            ->all();

            $programa = (new \yii\db\Query())
                ->select('Programa.Nombre,count(Programa.Nombre) AS Total')
                ->from('CultivoCrianza')
                ->innerJoin('Programa','Programa.ID = CultivoCrianza.ProgramaID')
                ->innerJoin('Especie','Especie.CultivoCrianzaID = CultivoCrianza.ID')
                ->innerJoin('InformacionGeneral','InformacionGeneral.EspecieID = Especie.ID')
                ->innerJoin('UnidadOperativa','InformacionGeneral.UnidadOperativaID = UnidadOperativa.ID')
                ->innerJoin('UnidadEjecutora','UnidadOperativa.UnidadEjecutoraID = UnidadEjecutora.ID')
                ->where(['SUBSTRING ( InformacionGeneral.DistritoID, 1, 2 )'=>$dep])
                ->groupBy('Programa.Nombre')
                ->all();

            $listadoPrograma = (new \yii\db\Query())
                ->select('Programa.Nombre,InformacionGeneral.Codigo,CultivoCrianza.Nombre Cultivo,EntidadParticipante.AporteMonetario,EntidadParticipante.AporteNoMonetario,EntidadParticipante.RazonSocial')
                ->from('CultivoCrianza')
                ->innerJoin('Programa','Programa.ID = CultivoCrianza.ProgramaID')
                ->innerJoin('Especie','Especie.CultivoCrianzaID = CultivoCrianza.ID')
                ->innerJoin('InformacionGeneral','InformacionGeneral.EspecieID = Especie.ID')
                ->innerJoin('UnidadOperativa','InformacionGeneral.UnidadOperativaID = UnidadOperativa.ID')
                ->innerJoin('UnidadEjecutora','UnidadOperativa.UnidadEjecutoraID = UnidadEjecutora.ID')
                ->innerJoin('Investigador','Investigador.ID = InformacionGeneral.InvestigadorID')
                ->innerJoin('EntidadParticipante','EntidadParticipante.InvestigadorID = Investigador.ID')
                ->where(['SUBSTRING ( InformacionGeneral.DistritoID, 1, 2 )'=>$dep,'EntidadParticipante.RazonSocial'=>'PNIA'])
                ->groupBy('Programa.Nombre,InformacionGeneral.Codigo,CultivoCrianza.Nombre,EntidadParticipante.AporteMonetario,EntidadParticipante.RazonSocial,EntidadParticipante.AporteNoMonetario')
                ->all();

        }

        if ( $prov != '') {
            $informacion=InformacionGeneral::find()
            ->where("SUBSTRING ( InformacionGeneral.DistritoID, 1, 4 ) = :prov AND InformacionGeneral.Latitud != '' AND InformacionGeneral.Longitud != '' ",[':prov'=>$prov])
            ->all();

            $programa = (new \yii\db\Query())
                ->select('Programa.Nombre,count(Programa.Nombre) AS Total')
                ->from('CultivoCrianza')
                ->innerJoin('Programa','Programa.ID = CultivoCrianza.ProgramaID')
                ->innerJoin('Especie','Especie.CultivoCrianzaID = CultivoCrianza.ID')
                ->innerJoin('InformacionGeneral','InformacionGeneral.EspecieID = Especie.ID')
                ->innerJoin('UnidadOperativa','InformacionGeneral.UnidadOperativaID = UnidadOperativa.ID')
                ->innerJoin('UnidadEjecutora','UnidadOperativa.UnidadEjecutoraID = UnidadEjecutora.ID')
                ->where(['SUBSTRING ( InformacionGeneral.DistritoID, 1, 4 )'=>$dep])
                ->groupBy('Programa.Nombre')
                ->all();

            $listadoPrograma = (new \yii\db\Query())
                ->select('Programa.Nombre,InformacionGeneral.Codigo,CultivoCrianza.Nombre Cultivo,EntidadParticipante.AporteMonetario,EntidadParticipante.AporteNoMonetario,EntidadParticipante.RazonSocial')
                ->from('CultivoCrianza')
                ->innerJoin('Programa','Programa.ID = CultivoCrianza.ProgramaID')
                ->innerJoin('Especie','Especie.CultivoCrianzaID = CultivoCrianza.ID')
                ->innerJoin('InformacionGeneral','InformacionGeneral.EspecieID = Especie.ID')
                ->innerJoin('UnidadOperativa','InformacionGeneral.UnidadOperativaID = UnidadOperativa.ID')
                ->innerJoin('UnidadEjecutora','UnidadOperativa.UnidadEjecutoraID = UnidadEjecutora.ID')
                ->innerJoin('Investigador','Investigador.ID = InformacionGeneral.InvestigadorID')
                ->innerJoin('EntidadParticipante','EntidadParticipante.InvestigadorID = Investigador.ID')
                ->where(['SUBSTRING ( InformacionGeneral.DistritoID, 1, 4 )'=>$dep,'EntidadParticipante.RazonSocial'=>'PNIA'])
                ->groupBy('Programa.Nombre,InformacionGeneral.Codigo,CultivoCrianza.Nombre,EntidadParticipante.AporteMonetario,EntidadParticipante.RazonSocial,EntidadParticipante.AporteNoMonetario')
                ->all();

        }

        if ( $dist != '') {
            $informacion=InformacionGeneral::find()
            ->where("SUBSTRING ( InformacionGeneral.DistritoID, 1, 6 ) = :dist AND InformacionGeneral.Latitud != '' AND InformacionGeneral.Longitud != '' ",[':dist'=>$dist])

            ->all();

            $programa = (new \yii\db\Query())
                ->select('Programa.Nombre,count(Programa.Nombre) AS Total')
                ->from('CultivoCrianza')
                ->innerJoin('Programa','Programa.ID = CultivoCrianza.ProgramaID')
                ->innerJoin('Especie','Especie.CultivoCrianzaID = CultivoCrianza.ID')
                ->innerJoin('InformacionGeneral','InformacionGeneral.EspecieID = Especie.ID')
                ->innerJoin('UnidadOperativa','InformacionGeneral.UnidadOperativaID = UnidadOperativa.ID')
                ->innerJoin('UnidadEjecutora','UnidadOperativa.UnidadEjecutoraID = UnidadEjecutora.ID')
                ->where(['SUBSTRING ( InformacionGeneral.DistritoID, 1, 6 )'=>$dep])
                ->groupBy('Programa.Nombre')
                ->all();

            $listadoPrograma = (new \yii\db\Query())
                ->select('Programa.Nombre,InformacionGeneral.Codigo,CultivoCrianza.Nombre Cultivo,EntidadParticipante.AporteMonetario,EntidadParticipante.AporteNoMonetario,EntidadParticipante.RazonSocial')
                ->from('CultivoCrianza')
                ->innerJoin('Programa','Programa.ID = CultivoCrianza.ProgramaID')
                ->innerJoin('Especie','Especie.CultivoCrianzaID = CultivoCrianza.ID')
                ->innerJoin('InformacionGeneral','InformacionGeneral.EspecieID = Especie.ID')
                ->innerJoin('UnidadOperativa','InformacionGeneral.UnidadOperativaID = UnidadOperativa.ID')
                ->innerJoin('UnidadEjecutora','UnidadOperativa.UnidadEjecutoraID = UnidadEjecutora.ID')
                ->innerJoin('Investigador','Investigador.ID = InformacionGeneral.InvestigadorID')
                ->innerJoin('EntidadParticipante','EntidadParticipante.InvestigadorID = Investigador.ID')
                ->where(['SUBSTRING ( InformacionGeneral.DistritoID, 1, 6 )'=>$dep,'EntidadParticipante.RazonSocial'=>'PNIA'])
                ->groupBy('Programa.Nombre,InformacionGeneral.Codigo,CultivoCrianza.Nombre,EntidadParticipante.AporteMonetario,EntidadParticipante.RazonSocial,EntidadParticipante.AporteNoMonetario')
                ->all();

        }

        return $this->render('geo-mapa',['dep'=>$dep,'informacion'=>$informacion,'estacion'=>$listadoPrograma,'programa'=>$programa]);

    }

    private function detaEeaa($id){
        $cantidadUnidad = (new \yii\db\Query())
            ->select('Count(dbo.Investigador.ID) AS Proyectos,ProgramaTransversal.Nombre Estacion,UnidadEjecutora.ID, UnidadEjecutora.Nombre')
            ->from('Investigador')
            ->innerJoin('InformacionGeneral','InformacionGeneral.InvestigadorID = Investigador.ID')
            ->innerJoin('UnidadOperativa','InformacionGeneral.UnidadOperativaID = UnidadOperativa.ID')
            ->innerJoin('UnidadEjecutora','UnidadOperativa.UnidadEjecutoraID = UnidadEjecutora.ID')
            ->innerJoin('ProgramaTransversal','ProgramaTransversal.ID = InformacionGeneral.ProgramaTransversalID')
            ->where(['UnidadEjecutora.ID'=>$id])
            ->groupBy('ProgramaTransversal.Nombre,UnidadEjecutora.ID, UnidadEjecutora.Nombre')
            ->all(); 

        return $cantidadUnidad;
    }
    public function actionPac(){

        $info=InformacionGeneral::find()
                    ->select('Proyecto.ID,InformacionGeneral.Meses')
                    ->innerJoin('Investigador','InformacionGeneral.InvestigadorID = Investigador.ID')
                    ->innerJoin('Proyecto','Proyecto.InvestigadorID = Investigador.ID')
                    ->innerJoin('Poa','Poa.ProyectoID = Proyecto.ID')
                    ->all();
        // echo '<pre>';
        // print_r($info);
        foreach ($info as $value) {

            echo '<br>-------------';
            echo '<br> Proyecto: ->'.$value->ID.'<br>';
            echo '<br> Meses: ->'.$value->Meses.'<br>';
            echo '<br> Anio: ->'.round($value->Meses/12).'<br>';
            for ($i=0; $i <= round($value->Meses/12) -1; $i++) { 
                echo 2016+$i.'<br>';
                $pacs = new Pac;
                $pacs->PoaID = $value->ID;
                $pacs->Estado = 1;
                $pacs->Situacion = 0;
                $pacs->Anio = 2016+$i;
                $pacs->ProyectoID = $value->ID;
                //$pacs->save();
            }
            echo '-------------<br>';
        }
    }

}
