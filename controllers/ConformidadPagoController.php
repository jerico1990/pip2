<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\PasoCritico;
use app\models\Usuario;
use app\models\Persona;
use app\models\Investigador;
use app\models\Proyecto;
use app\models\InformacionGeneral;
use app\models\Poa;
use app\models\Componente;
use app\models\Actividad;
use app\models\CronogramaProyecto;
use app\models\ActRubroElegible;
use app\models\CronogramaComponente;
use app\models\CronogramaActividad;
use app\models\CronogramaActRubroElegible;
use app\models\RubroElegible;
use app\models\AreSubCategoria;
use app\models\CronogramaAreSubCategoria;
use app\models\MarcoLogicoActividad;
use app\models\AreSubCategoriaObservacion;
use app\models\SolicitudCompromisoPresupuestal;
use app\models\DetalleSolicitudCompromisoPresupuestal;
use app\models\Orden;
use app\models\ConformidadPago;
use app\models\DetalleConformidadPago;
use app\models\Situacion;
use app\models\DetalleOrden;
use app\models\Requerimiento;
use app\models\Siaft;
use app\models\CompromisoPresupuestal;
use app\models\DetalleCompromisoPresupuestal;


use PhpOffice\PhpWord\PhpWord;

use app\models\DetalleRequerimiento;

use yii\web\UploadedFile;


class ConformidadPagoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    // public function beforeAction($action) {
    //     if($action->id = 'crear-conformidad-servicio') {
    //         Yii::$app->request->enableCsrfValidation = false;
    //     }
    //     return parent::beforeAction($action);
    // }

    public function beforeAction($action) {
        if($action->id = 'crear-conformidad-servicio') {
            Yii::$app->request->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($CodigoProyecto=NULL)
    {
        $this->layout='estandar';
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        return $this->render('index',['CodigoProyecto'=>$CodigoProyecto]);
    }
    
    public function actionListaConformidadesServicios($CodigoProyecto=null)
    {
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        
        
        $resultados = (new \yii\db\Query())
            ->select('ConformidadPago.*,TipoGasto.Nombre,Orden.TipoOrden,Situacion.Descripcion SituacionDesc,ConformidadPago.OrdenID ,CompromisoPresupuestal.ID CompromisoPresupuestalID')
            ->from('ConformidadPago')
            ->innerJoin('Orden','ConformidadPago.OrdenID = Orden.ID')
            ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.RequerimientoID = Orden.RequerimientoID')
            ->innerJoin('TipoGasto','TipoGasto.ID = Orden.TipoOrden')
            ->innerJoin('Situacion','Situacion.ID = ConformidadPago.Situacion')
            ->where(['ConformidadPago.CodigoProyecto'=>$CodigoProyecto,'CompromisoPresupuestal.Estado'=>1])
            ->all();

        $mes = array(1=>'Enero', 2=>'Febrero', 3=>'Marzo', 4=>'Abril',5=> 'Mayo', 6=>'Junio',7=>'Julio',8=> 'Agosto', 9=>'Septiembre',10=>'Octubre', 11=>'Noviembre',12=> 'Diciembre',13=>'--');
        $nro=0;
        foreach($resultados as $result)
        {
            $nro++;
            echo "<tr>";
            echo "<td>" . $result["Nombre"] . "</td>";
            echo "<td>" . $result["Memorando"] . "</td>";
            echo "<td>" . $result["SIAF"] . "</td>";
            if(empty($result["Entregable"])){
                echo "<td>" .$result["Annio"] . "</td>";
            }else{
                echo "<td>" . $mes[$result["Entregable"]].'-'. $result["Annio"] . "</td>";
            }
            echo "<td>" . 'S/.'.$result["Monto"] . "</td>";
            echo "<td>" . $result["SituacionDesc"] . "</td>";
            echo "<td><a data-id='".$result['OrdenID']."' href='conformidad-pago/plantilla?ID=".$result["CompromisoPresupuestalID"]."&Orden=".$result["OrdenID"]."&CodeID=".$result["ID"]."' data-codigo-proyecto=".$CodigoProyecto." data-edit='0' class='btn btn-default btn-sm'>Anexo 06</a></td>";
            echo "<td>" . Yii::$app->tools->dateFormat($result["FechaRegistro"]) . "</td>";
            if($result['Situacion'] == 7){
                echo "<td>
                        <a data-id='".$result['OrdenID']."' data-corr ='".$result['ID']."' data-gasto='".$result['TipoOrden']."' href='conformidad-pago/ver?ID=".$result["ID"]."' data-codigo-proyecto=".$CodigoProyecto." data-edit='0' class='btn btn-info btn-generar-documento btn-sm'><i class='fa fa-plus-square'></i> Generar</a>
                        <a data-remove-id='".$result['ID']."' href='conformidad-pago/delete?ID=".$result["ID"]."' class='btn btn-danger btn-remove btn-sm'><i class='fa fa-remove fa-lg'></i></a>
                    </td>";
            }elseif($result['Situacion'] == 1){
                echo "<td>
                    <a data-bloq='0' data-id='".$result['OrdenID']."' data-corr ='".$result['ID']."' data-gasto='".$result['TipoOrden']."' href='conformidad-pago/ver?ID=".$result["ID"]."&edit=1' data-edit='1' data-codigo-proyecto=".$CodigoProyecto." class='btn btn-info btn-generar-documento btn-sm'><i class='fa fa-eye'></i> Ver</a>
                    <a data-id='".$result['ID']."' href='#' class='btn btn-primary btn-enviar-aprobar btn-sm'>Enviar</i></a>
                    <a data-remove-id='".$result['ID']."' href='conformidad-pago/delete?ID=".$result["ID"]."' class='btn btn-danger btn-remove btn-sm'><i class='fa fa-remove fa-lg'></i></a>
                </td>";
            }else{
                echo "<td>
                <a data-bloq='1' data-id='".$result['OrdenID']."' data-corr ='".$result['ID']."' data-gasto='".$result['TipoOrden']."' href='conformidad-pago/ver?ID=".$result["ID"]."&edit=1' data-edit='1' data-codigo-proyecto='".$CodigoProyecto."' class='btn btn-info btn-generar-documento btn-sm'><i class='fa fa-eye'></i> Ver</a>
                </td>";
            }

            echo "</tr>";
        }
    }
    
    public function actionListado($CodigoProyecto = null)
    {

        $resultados = (new \yii\db\Query())
            ->select('ConformidadPago.ID, ConformidadPago.OrdenID, ConformidadPago.CodigoProyecto, TipoGasto.Sigla, Orden.Correlativo, Orden.Annio, Orden.RazonSocial, ConformidadPago.Concepto, ConformidadPago.Entregable, ConformidadPago.Annio AnnioEnt,ConformidadPago.Monto,Situacion.Descripcion AS SituacionDesc,Siaft.Siaf,Situacion.ID Situacion,Orden.TipoOrden, CompromisoPresupuestal.Correlativo CompromisoCorrelativo,CompromisoPresupuestal.Annio CompromisoAnnio')
            ->from('ConformidadPago')
            ->innerJoin('Orden','ConformidadPago.OrdenID = Orden.ID')
            ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.RequerimientoID = Orden.RequerimientoID')
            ->innerJoin('SeguimientoCompromisoPresupuestal','SeguimientoCompromisoPresupuestal.CompromisoPresupuestalID=CompromisoPresupuestal.ID')
            ->innerJoin('TipoGasto','TipoGasto.ID = Orden.TipoOrden')
            ->innerJoin('Situacion','Situacion.ID = ConformidadPago.Situacion')
            ->innerJoin('Siaft','Siaft.Orden = Orden.ID')
            ->where('ConformidadPago.Situacion IN (2,6) AND CompromisoPresupuestal.Situacion in (20,21) AND CompromisoPresupuestal.Estado=1 AND SeguimientoCompromisoPresupuestal.UsuarioID='.\Yii::$app->user->id.' AND Siaft.Fase = 10 AND Siaft.Estado = 1 AND Siaft.TipoGastoID IN (1,2,3)')
            ->all();

        $nro=0;
        $mes = array(1=>'Enero', 2=>'Febrero', 3=>'Marzo', 4=>'Abril',5=> 'Mayo', 6=>'Junio',7=>'Julio',8=> 'Agosto', 9=>'Septiembre',10=>'Octubre', 11=>'Noviembre',12=> 'Diciembre',13=>'--');
        foreach($resultados as $result)
        {
            switch ($result['Situacion']) {
                case 2:
                    $color = 'amarillo';
                    break;
                case 3:
                    $color = 'verde';
                    break;
                case 6:
                    $color = 'rojo';
                    break;
            }
            $nro++;
            echo "<tr>";
            echo "<td class='$color'>" . $result["CodigoProyecto"] . "</td>";
            echo "<td class='$color'> N° " . str_pad($result["CompromisoCorrelativo"], 3, "0", STR_PAD_LEFT)."-".$result["CompromisoAnnio"]." </td>";
            echo "<td class='$color'>" . $result["Sigla"].'/'.str_pad($result["Correlativo"], 3, "0", STR_PAD_LEFT).'-'.$result["CompromisoAnnio"] . "</td>";
            echo "<td class='$color'>" . $result["RazonSocial"] . "</td>";
            echo "<td class='$color' style='white-space: nowrap;text-overflow: ellipsis;width: 300px;overflow: hidden;display:block' data-toggle='tooltip' data-original-title='".$result["Concepto"] ."'>" . $result["Concepto"] . "</td>";
            echo "<td class='$color'>" . $result["Monto"] . "</td>";
            $dato = !is_null($result["Entregable"])?$result["Entregable"]:13;
            echo "<td class='$color'>" . $mes[$dato].'-'.$result["AnnioEnt"] . "</td>";
            
            $GetExpedienteSiaf=\Yii::$app->db->createCommand('BEGIN DECLARE @a VARCHAR(255); EXEC SP_OBTENER_EXPEDIENTE_SIAF :NUM_REGI_CAB, :ANNO_EJEC_EJE select @a as NUME_SIAF_CAB end;')
            ->bindValue(':NUM_REGI_CAB',$result["Siaf"])
            ->bindValue(':ANNO_EJEC_EJE',$result["Annio"])
            ->queryOne();
            
            echo "<td class='$color'>" . $GetExpedienteSiaf['NUME_SIAF_CAB'] . "</td>";
            echo "<td class='$color'>" . $result["SituacionDesc"] . "</td>";
            if($result['Situacion'] == 2){
                echo "<td class='$color'>
                        <a data-id='".$result['ID']."' href='conformidad-pago/aprobar?ID=".$result["ID"]."' class='btn btn-primary btn-aprobar btn-xs'>Aprobar</a>
                        <a data-id='".$result['ID']."' href='conformidad-pago/desaprobar?ID=".$result["ID"]."' class='btn btn-danger btn-desaprobar btn-xs'>Desaprobar</a>
                        <a data-bloq='1' data-id='".$result['OrdenID']."' data-corr ='".$result['ID']."' data-gasto='".$result['TipoOrden']."' href='conformidad-pago/ver?ID=".$result["ID"]."&edit=1' data-edit='1' data-codigo-proyecto='".$CodigoProyecto."' class='btn btn-info btn-generar-documento btn-xs'>Ver</a>
                    </td>";
            }elseif($result['Situacion'] == 6){
                echo "<td class='$color'>
                        <a data-bloq='1' data-id='".$result['OrdenID']."' data-corr ='".$result['ID']."' data-gasto='".$result['TipoOrden']."' href='conformidad-pago/ver?ID=".$result["ID"]."&edit=1' data-edit='1' data-codigo-proyecto='".$CodigoProyecto."' class='btn btn-info btn-generar-documento btn-xs'>Ver</a>
                    </td>";
            }else{
                echo "<td class='$color'>
                        <a data-bloq='1' data-id='".$result['OrdenID']."' data-corr ='".$result['ID']."' data-gasto='".$result['TipoOrden']."' href='conformidad-pago/ver?ID=".$result["ID"]."&edit=1' data-edit='1' data-codigo-proyecto='".$CodigoProyecto."' class='btn btn-info btn-generar-documento btn-xs'>Ver</a>
                    </td>";
            }
            echo "</tr>";
        }
    }

    public function actionListadoGeneral($CodigoProyecto=NULL)
    {
        $this->layout='estandar';
        return $this->render('_adm_listado',['CodigoProyecto'=>$CodigoProyecto]);
    }


    
    public function actionListadoConformidadAprobado($CodigoProyecto=NULL)
    {
        $this->layout='estandar';
        return $this->render('_adm_listado_conformidad_aprobado',['CodigoProyecto'=>$CodigoProyecto]);
    }

    public function actionListadoConformidadOk($CodigoProyecto = null)
    {

        $resultados = (new \yii\db\Query())
            ->select('ConformidadPago.ID,ConformidadPago.OrdenID,ConformidadPago.CodigoProyecto,TipoGasto.Sigla,Orden.Correlativo, Orden.Annio ,Orden.RazonSocial,ConformidadPago.Concepto,ConformidadPago.Entregable,ConformidadPago.Annio AnnioEnt,ConformidadPago.Monto,Situacion.Descripcion AS SituacionDesc,Siaft.Siaf,Situacion.ID Situacion,Orden.TipoOrden')
            ->from('ConformidadPago')
            ->innerJoin('Orden','ConformidadPago.OrdenID = Orden.ID')
            ->innerJoin('CompromisoPresupuestal','CompromisoPresupuestal.RequerimientoID = Orden.RequerimientoID')
            ->innerJoin('SeguimientoCompromisoPresupuestal','SeguimientoCompromisoPresupuestal.CompromisoPresupuestalID=CompromisoPresupuestal.ID')
            ->innerJoin('TipoGasto','TipoGasto.ID = Orden.TipoOrden')
            ->innerJoin('Situacion','Situacion.ID = ConformidadPago.Situacion')
            ->innerJoin('Siaft','Siaft.Orden = Orden.ID')
            ->where('ConformidadPago.Situacion IN (3) AND CompromisoPresupuestal.Situacion in (20,21) AND CompromisoPresupuestal.Estado=1 AND SeguimientoCompromisoPresupuestal.UsuarioID='.\Yii::$app->user->id.' AND Siaft.Fase = 10 AND Siaft.Estado = 1 AND Siaft.TipoGastoID IN (1,2,3)')
            // ->limit(2)
            // ->offset(2)
            ->all();
        $nro=0;
        $mes = array(1=>'Enero', 2=>'Febrero', 3=>'Marzo', 4=>'Abril',5=> 'Mayo', 6=>'Junio',7=>'Julio',8=> 'Agosto', 9=>'Septiembre',10=>'Octubre', 11=>'Noviembre',12=> 'Diciembre',13=>'--');
        foreach($resultados as $result)
        {
            switch ($result['Situacion']) {
                case 2:
                    $color = 'amarillo';
                    break;
                case 3:
                    $color = 'verde';
                    break;
                case 6:
                    $color = 'rojo';
                    break;
            }
            $nro++;
            echo "<tr>";
            echo "<td class='$color'>" . $result["CodigoProyecto"] . "</td>";
            echo "<td class='$color'>" . $result["Sigla"].'/'.str_pad($result["Correlativo"], 3, "0", STR_PAD_LEFT).'-'.$result["Annio"] . "</td>";
            echo "<td class='$color'>" . $result["RazonSocial"] . "</td>";
            
            echo "<td class='$color' style='white-space: nowrap;text-overflow: ellipsis;width: 300px;overflow: hidden; display:block' data-toggle='tooltip' data-original-title='".$result["Concepto"] ."'>" . $result["Concepto"] . "</td>";

            echo "<td class='$color'>" . $result["Monto"] . "</td>";
            $dato = !is_null($result["Entregable"])?$result["Entregable"]:13;
            echo "<td class='$color'>" . $mes[$dato].'-'.$result["AnnioEnt"] . "</td>";
            $GetExpedienteSiaf=\Yii::$app->db->createCommand('BEGIN DECLARE @a VARCHAR(255); EXEC SP_OBTENER_EXPEDIENTE_SIAF :NUM_REGI_CAB, :ANNO_EJEC_EJE select @a as NUME_SIAF_CAB end;')
            ->bindValue(':NUM_REGI_CAB',$result["Siaf"])
            ->bindValue(':ANNO_EJEC_EJE',$result["Annio"])
            ->queryOne();

            echo "<td class='$color'>" . $GetExpedienteSiaf['NUME_SIAF_CAB'] . "</td>";
            echo "<td class='$color'>" . $result["SituacionDesc"] . "</td>";
            if($result['Situacion'] == 2){
                echo "<td class='$color'>
                        <a data-id='".$result['ID']."' href='conformidad-pago/aprobar?ID=".$result["ID"]."' class='btn btn-primary btn-aprobar btn-xs'>Aprobar</a>
                        <a data-id='".$result['ID']."' href='conformidad-pago/desaprobar?ID=".$result["ID"]."' class='btn btn-danger btn-desaprobar btn-xs'>Desaprobar</a>
                        <a data-bloq='1' data-id='".$result['OrdenID']."' data-corr ='".$result['ID']."' data-gasto='".$result['TipoOrden']."' href='conformidad-pago/ver?ID=".$result["ID"]."&edit=1' data-edit='1' data-codigo-proyecto='".$CodigoProyecto."' class='btn btn-info btn-generar-documento btn-xs'>Ver</a>
                    </td>";
            }elseif($result['Situacion'] == 6){
                echo "<td class='$color'>
                        <a data-bloq='1' data-id='".$result['OrdenID']."' data-corr ='".$result['ID']."' data-gasto='".$result['TipoOrden']."' href='conformidad-pago/ver?ID=".$result["ID"]."&edit=1' data-edit='1' data-codigo-proyecto='".$CodigoProyecto."' class='btn btn-info btn-generar-documento btn-xs'>Ver</a>
                    </td>";
            }else{
                echo "<td class='$color'>
                        <a data-bloq='1' data-id='".$result['OrdenID']."' data-corr ='".$result['ID']."' data-gasto='".$result['TipoOrden']."' href='conformidad-pago/ver?ID=".$result["ID"]."&edit=1' data-edit='1' data-codigo-proyecto='".$CodigoProyecto."' class='btn btn-info btn-generar-documento btn-xs'>Ver</a>
                    </td>";
            }

            echo "</tr>";
        }
    }




    public function actionDesaprobarFrm($ID, $tipo = null, $CodigoProyecto = null, $edit = null, $bloq = null){
        $this->layout='vacio';
        return $this->render('desaprobar',['CodigoProyecto'=>$CodigoProyecto,'ID'=>$ID]);
    }

    public function Situacion($codigo)
    {
        $situacion=Situacion::find()->where('Codigo=:Codigo',[':Codigo'=>$codigo])->one();
        if(!empty($situacion)){
            return $situacion->Descripcion;
        }else{
            return 'Inicio';
        }
    }


    public function actionDelete($ID = null){
        $conf = ConformidadPago::findOne($ID);
        $conf->delete();
        $arr = array('Success' => true);
        echo json_encode($arr);
    }


    public function actionAprobar($ID = null){
        $conf = DetalleConformidadPago::find()->where(['ConformidadPagoID'=>$ID])->one();
        
        $confx = ConformidadPago::find()->where(['ID'=>$ID])->one();
        // VALIDACION CON SIAF PARA ENVIAR LOS DATOS AL SIAF
        $ord = Orden::find()->where(['ID'=>$confx->OrdenID])->one();
        $requerimiento=Requerimiento::findOne($ord->RequerimientoID);
        
        $detalle = DetalleRequerimiento::find()
        ->innerJoin('Requerimiento','DetalleRequerimiento.RequerimientoID = Requerimiento.ID')
        ->where(['DetalleRequerimiento.TipoDetalleOrdenID'=>$ord->ID,'CodigoProyecto'=>$requerimiento->CodigoProyecto])->one();
        // $detalle = DetalleRequerimiento::find()->where(['TipoDetalleOrdenID'=>$ord->ID])->one();

        $totCantidad = DetalleOrden::find()
        ->select('SUM(Cantidad) Cantidad, SUM(PrecioUnitario) PrecioUnitario')
        ->where(['OrdenID'=>$ord->ID])->one();
        
        $areSub = AreSubCategoria::find()->where(['ID'=>$detalle->AreSubCategoriaID])->one();
        $areSub->CostoUnitarioEjecutada = $totCantidad->PrecioUnitario;
        $areSub->MetaFisicaEjecutada    = $totCantidad->Cantidad;
        $areSub->update();
        
        $compromisoAnual=Siaft::find()->where('Fase=9 and Estado = 1 AND Siaft.TipoGastoID IN (1,2,3) AND Orden=:Orden and CodigoProyecto=:CodigoProyecto',[':Orden'=>$ord->ID,':CodigoProyecto'=>$requerimiento->CodigoProyecto])->one();

        $compromisoAdministrativo=Siaft::find()->where('Fase=10 AND Estado = 1 AND Siaft.TipoGastoID IN (1,2,3) and Orden=:Orden and CodigoProyecto=:CodigoProyecto',[':Orden'=>$ord->ID,':CodigoProyecto'=>$requerimiento->CodigoProyecto])->one();

        $GetExpedienteSiaf=\Yii::$app->db->createCommand('BEGIN DECLARE @a VARCHAR(255); EXEC SP_OBTENER_EXPEDIENTE_SIAF :NUM_REGI_CAB, :ANNO_EJEC_EJE select @a as NUME_SIAF_CAB end;')
                                                        ->bindValue(':NUM_REGI_CAB',$compromisoAnual->Siaf)
                                                        ->bindValue(':ANNO_EJEC_EJE',$ord->Annio)
                                                        ->queryOne();
        
        $ActualizarDevengado = \Yii::$app->db->createCommand('BEGIN DECLARE @a VARCHAR(255); EXEC SP_DEVENGADO_ACTUALIZA :Codigo, :Anio, :Estado, @a output; select @a as ExpedienteSiaf end; ')
        ->bindValue(':Codigo',   $compromisoAdministrativo->Siaf )
        ->bindValue(':Anio',      $ord->Annio)
        ->bindValue(':Estado',    1)
        ->queryOne();
       
        if($ActualizarDevengado){
            $conf->Situacion = 3;
            $conf->update();
            $confx->Situacion = 3;
            $confx->update();
            return $ActualizarDevengado['ExpedienteSiaf'];
            
        }else{
            return 0;
        }
        
        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
    public function actionAprobarOtros($CodigoProyecto)
    {
        
        $compromisoAnual=Siaft::find()->where('Fase=9 and Orden=:Orden and CodigoProyecto=:CodigoProyecto',
                                   [':Orden'=>$ord->ID,':CodigoProyecto'=>$requerimiento->CodigoProyecto])
                                ->one();
        $compromisoAdministrativo=Siaft::find()->where('Fase=10 and Orden=:Orden and CodigoProyecto=:CodigoProyecto',
                                   [':Orden'=>$ord->ID,':CodigoProyecto'=>$requerimiento->CodigoProyecto])
                                ->one();
        $GetExpedienteSiaf=\Yii::$app->db->createCommand('BEGIN DECLARE @a VARCHAR(255); EXEC SP_OBTENER_EXPEDIENTE_SIAF :NUM_REGI_CAB, :ANNO_EJEC_EJE select @a as NUME_SIAF_CAB end;')
                                                        ->bindValue(':NUM_REGI_CAB',$compromisoAnual->Siaf)
                                                        ->bindValue(':ANNO_EJEC_EJE',$ord->Annio)
                                                        ->queryOne();
        
        $ActualizarDevengado = \Yii::$app->db->createCommand('BEGIN DECLARE @a VARCHAR(255); EXEC SP_DEVENGADO_ACTUALIZA :Codigo, :Anio, :Estado, @a output; select @a as ExpedienteSiaf end; ')
        ->bindValue(':Codigo',   $compromisoAdministrativo->Siaf )
        ->bindValue(':Anio',      $ord->Annio)
        ->bindValue(':Estado',    1)
        ->queryOne();
       
        if($ActualizarDevengado){
            $conf->Situacion = 3;
            $conf->update();
            $confx->Situacion = 3;
            $confx->update();
            return $ActualizarDevengado['ExpedienteSiaf'];
            
        }else{
            return 0;
        }
        
        $arr = array('Success' => true);
        echo json_encode($arr);
    }

    public function actionFecha($fecha){
        
    }

    public function actionDesaprobar($CodigoProyecto = null){

        $conf = new ConformidadPago();
        
        if($conf->load(Yii::$app->request->post())){

            $confs = DetalleConformidadPago::find()->where(['ConformidadPagoID'=>$conf->ID])->one();
            $confs->Situacion = 6;
            $confs->update();

            $confx = ConformidadPago::find()->where(['ID'=>$conf->ID])->one();
            $confx->Situacion = 6;
            $confx->Observacion = $conf->Observacion;
            $confx->update();
        }

        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    

    public function actionEnviar($ID = null){
        $conf = DetalleConformidadPago::find()->where(['ConformidadPagoID'=>$ID])->one();
        $conf->Situacion = 2;
        $conf->update();

        $confx = ConformidadPago::find()->where(['ID'=>$ID])->one();
        $confx->Situacion = 2;
        $confx->update();

        $arr = array('Success' => true);
        echo json_encode($arr);
    }


    public function actionCrear($CodigoProyecto)
    {
        $this->layout='vacio';
        $ordenes = CompromisoPresupuestal::find()
        ->select('Orden.ID,Siaft.Siaf,Orden.Annio, TipoGasto.Sigla, Orden.RequerimientoID,Orden.Correlativo,CompromisoPresupuestal.Memorando')
        ->from('CompromisoPresupuestal')
        ->innerJoin('Orden','CompromisoPresupuestal.RequerimientoID = Orden.RequerimientoID')
        ->innerJoin('TipoGasto','TipoGasto.ID = Orden.TipoOrden')
        ->innerJoin('Siaft','Siaft.Orden = Orden.ID')
        ->where("CompromisoPresupuestal.CompromisoAdministrativo= 1 AND Siaft.Fase = 10 AND Siaft.Estado = 1 AND Siaft.TipoGastoID IN (1,2,3) AND CompromisoPresupuestal.Codigo ='".$CodigoProyecto."' AND Siaft.CodigoProyecto ='".$CodigoProyecto."'")
        // ->where("CompromisoPresupuestal.CompromisoAdministrativo= 1 AND Siaft.Fase = 10 AND Siaft.Estado = 1 AND CompromisoPresupuestal.Codigo ='".$CodigoProyecto."' AND Siaft.CodigoProyecto ='".$CodigoProyecto."' AND orden.ID NOT IN (SELECT ConformidadPago.OrdenID FROM ConformidadPago WHERE CodigoProyecto = '".$CodigoProyecto."')")
        ->all();
        
        $arr = array();
        foreach ($ordenes as $orden) {
            $GetExpedienteSiaf=\Yii::$app->db->createCommand('BEGIN DECLARE @a VARCHAR(255); EXEC SP_OBTENER_EXPEDIENTE_SIAF :NUM_REGI_CAB, :ANNO_EJEC_EJE select @a as NUME_SIAF_CAB end;')
            ->bindValue(':NUM_REGI_CAB',$orden->Siaf)
            ->bindValue(':ANNO_EJEC_EJE',$orden->Annio)
            ->queryOne();
            $arr[] = array(
                'ID'                => $orden->ID.'-'.$orden->Siaf,
                'SIAF'              => $GetExpedienteSiaf['NUME_SIAF_CAB'],
                'SIAFIntegra'       => $orden->Siaf,
                'Sigla'             => $orden->Sigla,
                'Correlativo'       => $orden->Correlativo,
                'Annio'             => $orden->Annio,
                'RequerimientoID'   => $orden->RequerimientoID,
                'Memorando'         => $orden->Memorando
            );
        }
        // print_r($arr);
        $conformidad=new ConformidadPago;
        // echo '<pre>';print_r(Yii::$app->request->post()); die;
        if($conformidad->load(Yii::$app->request->post())){
            // echo '<pre>'; print_r(Yii::$app->request->post()); die();
            // echo '<pre>';print_r($conformidad->OrdenID); die();

            $orden = $conformidad->OrdenID;//$_POST['OrdenID'];
            $xOrden = explode('-',$orden);
            $ord = CompromisoPresupuestal::find()
                ->select('Orden.ID,Siaft.Siaf,Orden.Annio,CompromisoPresupuestal.Memorando')
                ->from('CompromisoPresupuestal')
                ->innerJoin('Orden','CompromisoPresupuestal.RequerimientoID = Orden.RequerimientoID')
                ->innerJoin('TipoGasto','TipoGasto.ID = Orden.TipoOrden')
                ->innerJoin('Siaft','Siaft.Orden = Orden.ID')
                // ->innerJoin('ConformidadPago','ConformidadPago.OrdenID = Orden.ID')
                ->where("CompromisoPresupuestal.CompromisoAdministrativo= 1 AND Siaft.Fase = 10 AND Siaft.Estado = 1 AND Orden.ID=".$xOrden[0].' AND Siaft.Siaf='.$xOrden[1])
                ->one();

                $GetExpedienteSiaf=\Yii::$app->db->createCommand('BEGIN DECLARE @a VARCHAR(255); EXEC SP_OBTENER_EXPEDIENTE_SIAF :NUM_REGI_CAB, :ANNO_EJEC_EJE select @a as NUME_SIAF_CAB end;')
                ->bindValue(':NUM_REGI_CAB',$ord['Siaf'])
                ->bindValue(':ANNO_EJEC_EJE',$ord['Annio'])
                ->queryOne();
                
                $confx = ConformidadPago::find()->where(['OrdenID'=>$ord->ID,'SIAF'=>$ord->Siaf])->one();
                if(count($confx) ==0){
                    $entrega = $_POST['Entregable'];
                    $num = explode('-', $entrega);
                    $conformidad->Memorando=$ord['Memorando'];
                    // $conformidad->FechaRegistro=date('Y-m-d H:m:s');
                    $conformidad->Estado=1;

                    $conformidad->Entregable = $num[0];
                    $conformidad->Annio= $num[1];
                    $conformidad->Monto= str_replace(',','', $conformidad->Monto);
                    
                    $conformidad->Situacion=7;
                    $conformidad->SIAF=$GetExpedienteSiaf['NUME_SIAF_CAB'];
                    $conformidad->save();
                }
            return $this->redirect(['/conformidad-pago']);
            
        }
        return $this->render('_form',['CodigoProyecto'=>$CodigoProyecto,'ordenes'=>$arr]);
    }

    public function actionValidarSiaft(){
        $codigoID = $_POST['ID'];
        // $ordenes = Orden::find()
        // ->select('Orden.ID,Orden.Monto OrdenMonto,ConformidadPago.Monto MontoConformidad,(Orden.Monto - ConformidadPago.Monto) Saldo')
        // ->from('Orden')
        // ->innerJoin('ConformidadPago','Orden.ID = ConformidadPago.OrdenID')
        // ->where("Orden.ID =".$codigoID)
        // ->one();

        $ordenes = (new \yii\db\Query())
        ->select('Orden.ID,Orden.Monto OrdenMonto,sum(ConformidadPago.Monto) MontoConformidad,(Orden.Monto - sum(ConformidadPago.Monto)) Saldo')
        ->from('Orden')
        ->innerJoin('ConformidadPago','Orden.ID = ConformidadPago.OrdenID')
        ->where(['Orden.ID'=>$codigoID])
        ->groupBy('Orden.ID,Orden.Monto')
        ->one();

        if(empty($ordenes)){

            $ord = (new \yii\db\Query())
            ->select('Orden.ID,Orden.Monto')
            ->from('Orden')
            ->where(['Orden.ID'=>$codigoID])
            ->one();

            $ordenes = array(
                'ID' => '', 
                'MontoConformidad' => 0, 
                'OrdenMonto' => $ord['Monto'], 
                'Saldo' => $ord['Monto'],
            );
        }

        // print_r($ordenes);
        echo json_encode($ordenes);
    }
    
    
    public function actionVer($ID=null,$tipo= null,$CodigoProyecto=null,$edit=null,$bloq=null,$Code = null)
    {
        $this->layout='vacio';

        if($edit == 1){
            $resultados = (new \yii\db\Query())
                ->select('ConformidadPago.ID,ConformidadPago.Observacion Obs,TipoGasto.Nombre,Orden.ID OrdenID,DetalleConformidadPago.*')
                ->from('ConformidadPago')
                ->innerJoin('Orden','ConformidadPago.OrdenID = Orden.ID')
                ->innerJoin('TipoGasto','TipoGasto.ID = Orden.TipoOrden')
                ->innerJoin('DetalleConformidadPago','DetalleConformidadPago.ConformidadPagoID = ConformidadPago.ID')
                ->where('ConformidadPago.ID=:ConformidadPagoID',[':ConformidadPagoID'=>$Code])
                ->distinct()
                ->one();
        }else{
            $resultados = array();
        }
        
        if($tipo == 3){
            $vista = 'adjunto_compra';
        }elseif($tipo == 1 || $tipo == 2){
            $vista = 'adjunto_servicio';
        }
        
            
        return $this->render($vista,['OrdenID'=>$ID,'resultados'=>$resultados,'tipo'=>$tipo,'CodigoProyecto'=>$CodigoProyecto,'bloqueado'=>$bloq,'Code'=> $Code]);
    }

    public function actionCrearConformidadServicio($ID=null,$edit=0,$code=null){
        $this->layout='vacio';
        $cpago = ConformidadPago::find()->where(['OrdenID'=>$ID])->one();
        $Confor = ConformidadPago::find()->where(['ID'=>$code])->one();
        $orden = Orden::findOne($ID);

        $dePago = new DetalleConformidadPago();
        if($dePago->load(Yii::$app->request->post())){
            $dePago->ConformidadPagoID = $Confor->ID;
            $dePago->Estado = 1;
            $dePago->Situacion = 2;
            
            // echo '<pre>';
            // print_r(Yii::$app->request->post());
            // print_r($dePago);
            // die();
            $dePago->FechaCreacion = date('Ymd');

            if(!$dePago->Anexo06Archivo)
            {
                $dePago->Anexo06Archivo = UploadedFile::getInstance($dePago, 'Anexo06Archivo');
                $dePago->Anexo06Archivo->saveAs('conformidad/' . $Confor->ID . '-Canexo.' . $dePago->Anexo06Archivo->extension);
                $dePago->Anexo06 = $Confor->ID . '-Canexo.' . $dePago->Anexo06Archivo->extension;
            }


            if($orden->TipoOrden==1 || $orden->TipoOrden==2)
            {
                
                if(!$dePago->OrdenServicioArchivo)
                {
                    $dePago->OrdenServicioArchivo = UploadedFile::getInstance($dePago, 'OrdenServicioArchivo');
                    $dePago->OrdenServicioArchivo->saveAs('conformidad/' . $Confor->ID . '-Cos.' . $dePago->OrdenServicioArchivo->extension);
                    $dePago->OrdenServicio = $Confor->ID . '-Cos.' . $dePago->OrdenServicioArchivo->extension;
                }

                if(!$dePago->ConformidadServicioArchivo)
                {
                    $dePago->ConformidadServicioArchivo = UploadedFile::getInstance($dePago, 'ConformidadServicioArchivo');
                    $dePago->ConformidadServicioArchivo->saveAs('conformidad/' . $Confor->ID . '-Cconf.' . $dePago->ConformidadServicioArchivo->extension);
                    $dePago->ConformidadServicio = $Confor->ID . '-Cconf.' . $dePago->ConformidadServicioArchivo->extension;
                }


                if(!$dePago->ReciboHonorarioArchivo)
                {
                    $dePago->ReciboHonorarioArchivo = UploadedFile::getInstance($dePago, 'ReciboHonorarioArchivo');
                    $dePago->ReciboHonorarioArchivo->saveAs('conformidad/' . $Confor->ID . '-Crh.' . $dePago->ReciboHonorarioArchivo->extension);
                    $dePago->ReciboHonorario = $Confor->ID . '-Crh.' . $dePago->ReciboHonorarioArchivo->extension;
                }

                if(!$dePago->InformeActividadesArchivo)
                {
                    // var_dump($dePago->InformeActividadesArchivo);die;
                    $dePago->InformeActividadesArchivo = UploadedFile::getInstance($dePago, 'InformeActividadesArchivo');
                    $dePago->InformeActividadesArchivo->saveAs('conformidad/' . $Confor->ID . '-Cinforme.' . $dePago->InformeActividadesArchivo->extension);
                    $dePago->InformeActividades = $Confor->ID . '-Cinforme.' . $dePago->InformeActividadesArchivo->extension;
                }
                // echo '<pre>';
                // print_r($dePago);
                // die();
            }
            elseif($orden->TipoOrden==3)
            {
                if(!$dePago->OrdenCompraArchivo)
                {
                    $dePago->OrdenCompraArchivo = UploadedFile::getInstance($dePago, 'OrdenCompraArchivo');
                    $dePago->OrdenCompraArchivo->saveAs('conformidad/' . $Confor->ID . '-Coc.' . $dePago->OrdenCompraArchivo->extension);
                    $dePago->OrdenCompra = $Confor->ID . '-Coc.' . $dePago->OrdenCompraArchivo->extension;
                }

                if(!$dePago->ConformidadEspecificacionesArchivo)
                {
                    $dePago->ConformidadEspecificacionesArchivo = UploadedFile::getInstance($dePago, 'ConformidadEspecificacionesArchivo');
                    $dePago->ConformidadEspecificacionesArchivo->saveAs('conformidad/' . $Confor->ID . '-Cconf-esp.' . $dePago->ConformidadEspecificacionesArchivo->extension);
                    $dePago->ConformidadEspecificaciones = $Confor->ID . '-Cconf-esp.' . $dePago->ConformidadEspecificacionesArchivo->extension;
                }

                if(!$dePago->FacturaArchivo)
                {
                    $dePago->FacturaArchivo = UploadedFile::getInstance($dePago, 'FacturaArchivo');
                    $dePago->FacturaArchivo->saveAs('conformidad/' . $Confor->ID . '-Cfac.' . $dePago->FacturaArchivo->extension);
                    $dePago->Factura = $Confor->ID . '-Cfac.' . $dePago->FacturaArchivo->extension;
                }
            }

            if(!$dePago->OtrosArchivo)
            {
                $dePago->OtrosArchivo = UploadedFile::getInstance($dePago, 'OtrosArchivo');
                $dePago->OtrosArchivo->saveAs('conformidad/' . $Confor->ID . '-Cotro.' . $dePago->OtrosArchivo->extension);
                $dePago->Otros = $Confor->ID . '-Cotro.' . $dePago->OtrosArchivo->extension;
            }

            $dePago->save();

            $Confor->Situacion = 1;
            $Confor->update();

            return $this->redirect(['/conformidad-pago']);
        }
        return $this->redirect(['/conformidad-pago']);
    }


    public function actionUpdateConformidadServicio($OrdenID=null,$ID=null){
        $this->layout='vacio';
        $cpago = ConformidadPago::find()->where(['OrdenID'=>$OrdenID])->one();
        $orden = Orden::findOne($OrdenID);

        $dePago = new DetalleConformidadPago();
        if($dePago->load(Yii::$app->request->post())){
            $dePago->ConformidadPagoID = $cpago->ID;
            $dePago->Estado = 1;
            $dePago->Situacion = 2;
            // echo '<pre>';
            // print_r($dePago->ConformidadPagoID);
            // die();

            $dePago->FechaCreacion = date('Y-m-d');

            if(!$dePago->Anexo06Archivo)
            {
                $dePago->Anexo06Archivo = UploadedFile::getInstance($dePago, 'Anexo06Archivo');
                $dePago->Anexo06Archivo->saveAs('conformidad/' . $orden->ID . '-anexo.' . $dePago->Anexo06Archivo->extension);
                $dePago->Anexo06 = $orden->ID . '-anexo.' . $dePago->Anexo06Archivo->extension;
            }


            if($orden->TipoOrden==1 || $orden->TipoOrden==2)
            {
                if(!$dePago->OrdenServicioArchivo)
                {
                    $dePago->OrdenServicioArchivo = UploadedFile::getInstance($dePago, 'OrdenServicioArchivo');
                    $dePago->OrdenServicioArchivo->saveAs('conformidad/' . $orden->ID . '-os.' . $dePago->OrdenServicioArchivo->extension);
                    $dePago->OrdenServicio = $orden->ID . '-os.' . $dePago->OrdenServicioArchivo->extension;
                }

                if(!$dePago->ConformidadServicioArchivo)
                {
                    $dePago->ConformidadServicioArchivo = UploadedFile::getInstance($dePago, 'ConformidadServicioArchivo');
                    $dePago->ConformidadServicioArchivo->saveAs('conformidad/' . $orden->ID . '-conf.' . $dePago->ConformidadServicioArchivo->extension);
                    $dePago->ConformidadServicio = $orden->ID . '-conf.' . $dePago->ConformidadServicioArchivo->extension;
                }


                if(!$dePago->ReciboHonorarioArchivo)
                {
                    $dePago->ReciboHonorarioArchivo = UploadedFile::getInstance($dePago, 'ReciboHonorarioArchivo');
                    $dePago->ReciboHonorarioArchivo->saveAs('conformidad/' . $orden->ID . '-rh.' . $dePago->ReciboHonorarioArchivo->extension);
                    $dePago->ReciboHonorario = $orden->ID . '-rh.' . $dePago->ReciboHonorarioArchivo->extension;
                }

                if(!$dePago->InformeActividadesArchivo)
                {
                    $dePago->InformeActividadesArchivo = UploadedFile::getInstance($dePago, 'InformeActividadesArchivo');
                    $dePago->InformeActividadesArchivo->saveAs('conformidad/' . $orden->ID . '-informe.' . $dePago->InformeActividadesArchivo->extension);
                    $dePago->InformeActividades = $orden->ID . '-informe.' . $dePago->InformeActividadesArchivo->extension;
                }
            }
            elseif($orden->TipoOrden==3)
            {
                if(!$dePago->OrdenCompraArchivo)
                {
                    $dePago->OrdenCompraArchivo = UploadedFile::getInstance($dePago, 'OrdenCompraArchivo');
                    $dePago->OrdenCompraArchivo->saveAs('conformidad/' . $orden->ID . '-oc.' . $dePago->OrdenCompraArchivo->extension);
                    $dePago->OrdenCompra = $orden->ID . '-oc.' . $dePago->OrdenCompraArchivo->extension;
                }

                if(!$dePago->ConformidadEspecificacionesArchivo)
                {
                    $dePago->ConformidadEspecificacionesArchivo = UploadedFile::getInstance($dePago, 'ConformidadEspecificacionesArchivo');
                    $dePago->ConformidadEspecificacionesArchivo->saveAs('conformidad/' . $orden->ID . '-conf-esp.' . $dePago->ConformidadEspecificacionesArchivo->extension);
                    $dePago->ConformidadEspecificaciones = $orden->ID . '-conf-esp.' . $dePago->ConformidadEspecificacionesArchivo->extension;
                }

                if(!$dePago->FacturaArchivo)
                {
                    $dePago->FacturaArchivo = UploadedFile::getInstance($dePago, 'FacturaArchivo');
                    $dePago->FacturaArchivo->saveAs('conformidad/' . $orden->ID . '-fac.' . $dePago->FacturaArchivo->extension);
                    $dePago->Factura = $orden->ID . '-fac.' . $dePago->FacturaArchivo->extension;
                }
            }

            $dePago->save();

            $cpago->Situacion = 1;
            $cpago->update();

            return $this->redirect(['/conformidad-pago']);
        }
        return $this->redirect(['/conformidad-pago']);
    }

        
    public function actionServicio($valor='71802407')
    {
        $respuesta = file_get_contents("http://172.168.3.18/apipide/api/reniec/dni/" . $valor);
        //echo $respuesta;
        var_dump(json_decode($respuesta)->data->nombres);
    }
    
    function curl_get_contents($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }


    public function actionPlantilla($ID=null,$Orden = null,$CodeID = null)
    {

        $compromiso=CompromisoPresupuestal::findOne($ID);

        $requerimiento=Requerimiento::findOne($compromiso->RequerimientoID);
        //var_dump($requerimiento);die;
        $detallesx=DetalleCompromisoPresupuestal::find()
                    ->Select('DetalleCompromisoPresupuestal.Correlativo')
                    ->where('CompromisoPresupuestalID=:CompromisoPresupuestalID',[':CompromisoPresupuestalID'=>$ID])
                    ->groupBy('DetalleCompromisoPresupuestal.Correlativo')
                    ->all();
        
        $detalles = CompromisoPresupuestal::find()
        ->select('Orden.ID,Siaft.Siaf,Orden.Annio, TipoGasto.Sigla,Orden.RazonSocial,ConformidadPago.Monto, Orden.RequerimientoID,Orden.Correlativo,CompromisoPresupuestal.Memorando,ConformidadPago.Entregable,ConformidadPago.Annio AnnioEnt,ConformidadPago.NDocumento,ConformidadPago.DocumentoCorrelativo')
        ->from('CompromisoPresupuestal')
        ->innerJoin('Orden','CompromisoPresupuestal.RequerimientoID = Orden.RequerimientoID')
        ->innerJoin('TipoGasto','TipoGasto.ID = Orden.TipoOrden')
        ->innerJoin('Siaft','Siaft.Orden = Orden.ID')
        ->innerJoin('ConformidadPago','ConformidadPago.OrdenID = Orden.ID')
        ->where("CompromisoPresupuestal.CompromisoAdministrativo = 1 AND Siaft.Fase = 10 AND Siaft.Estado = 1 AND orden.ID =".$Orden." AND Siaft.CodigoProyecto = '".$compromiso->Codigo."' AND ConformidadPago.ID = ".$CodeID)
        ->all();

        $informacionGeneral=InformacionGeneral::find()
                        ->select('InformacionGeneral.*,UnidadOperativa.Nombre UnidadEjecutoraID')
                        ->innerJoin('UnidadOperativa','UnidadOperativa.ID=InformacionGeneral.UnidadOperativaID')
                        ->where('InformacionGeneral.Codigo=:Codigo',[':Codigo'=>$compromiso->Codigo])
                        ->one();
                        
        $countDetalles=0;
        foreach($detalles as $detalle)
        {
           $countDetalles++;
        }
        require_once(Yii::$app->basePath . '/web/PHPWord/src/PhpWord/Autoloader.php');     
        \PhpOffice\PhpWord\Autoloader::register();
        $phpWord = new PhpWord();
        

        $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/documentos/PLANTILLA_FORMATO_DE_SOLICITUD_DE_PAGO.docx');
        
        ///var_dump($requerimiento->Referencia);die;
        // $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/documentos/PLANTILLA_SOLICITUD_COMPROMISO_PRESUPUESTAL_SERVICIO.docx');
        $template->setValue('CODIGOPROYECTO', $compromiso->Codigo);
        $template->setValue('EEA', $informacionGeneral->UnidadEjecutoraID);
        $template->setValue('REFERENCIA', ''.$requerimiento->Referencia.'');
        $template->cloneRow('SIAFS', $countDetalles);
        $countDetalles=1;

        // echo $countDetalles; die;
        $mes = array(1=>'Enero', 2=>'Febrero', 3=>'Marzo', 4=>'Abril',5=> 'Mayo', 6=>'Junio',7=>'Julio',8=> 'Agosto', 9=>'Septiembre',10=>'Octubre', 11=>'Noviembre',12=> 'Diciembre',13=>'--');

        foreach($detalles as $detalle)
        {
            $template->setValue('MEMORANDO', !empty($detalle->DocumentoCorrelativo)?$detalle->DocumentoCorrelativo: $compromiso->Memorando  ."-".$compromiso->Annio );
            $GetExpedienteSiaf=\Yii::$app->db->createCommand('BEGIN DECLARE @a VARCHAR(255); EXEC SP_OBTENER_EXPEDIENTE_SIAF :NUM_REGI_CAB, :ANNO_EJEC_EJE select @a as NUME_SIAF_CAB end;')
            ->bindValue(':NUM_REGI_CAB',$detalle->Siaf)
            ->bindValue(':ANNO_EJEC_EJE',$detalle->Annio)
            ->queryOne();

            // $orden=Orden::findOne($detalle->Correlativo);
            // $template->setValue('NS#'.$countDetalles.'', "".$countDetalles."");
            $template->setValue('SIAFS#'.$countDetalles.'', "".$GetExpedienteSiaf['NUME_SIAF_CAB']."");
            $template->setValue('PROVEEDORS#'.$countDetalles.'', "".(!is_null($detalle->RazonSocial)?$detalle->RazonSocial:'' )."");
            $template->setValue('CPAGOS#'.$countDetalles.'', "".(!is_null($detalle->NDocumento)?$detalle->NDocumento:'')."");
            $template->setValue('MES#'.$countDetalles.'', "".(!is_null($detalle->Entregable)?$mes[$detalle->Entregable]."-".$detalle->AnnioEnt:'')."");

            // $template->setValue('RUCS#'.$countDetalles.'', "".ucwords(strtolower($orden->RUC))."");
            $template->setValue('MONTOS#'.$countDetalles.'', "".(($detalle->Monto)?number_format(ucwords(strtolower($detalle->Monto)), 2, '.', ' '):0)."");
            $countDetalles++;
        }
        
        $temp_file = tempnam(sys_get_temp_dir(), 'PHPWord');
        $template->saveAs($temp_file);
        
        $contentType = 'Content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document;';
        header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
        header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
        header ( "Cache-Control: no-cache, must-revalidate" );
        header ( "Pragma: no-cache" );
        header ( $contentType );
        header ( "Content-Disposition: attachment; filename='Anexo06-".$compromiso->Codigo.".docx'");
        readfile($temp_file); // or echo file_get_contents($temp_file);
        unlink($temp_file);  // remove temp file
        return true; 
    }


}
