<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\InformacionGeneral;
use app\models\ProgramaTransversal;
use app\models\DireccionLinea;
use app\models\TipoInvestigacion;
use app\models\UnidadEjecutora;
use app\models\UnidadOperativa;
use app\models\Persona;
use app\models\Programa;
use app\models\CultivoCrianza;
use app\models\Especie;
use app\models\Ubigeo;
use app\models\Usuario;
use app\models\Investigador;
use app\models\EntidadParticipante;
use app\models\TipoInstitucion;
use app\models\AmbitoIntervencion;
use app\models\MiembroEquipoGestion;
use app\models\MarcoLogicoFinProyecto;
use app\models\MarcoLogicoPropositoProyecto;
use app\models\Proyecto;
use app\models\Poa;
use app\models\MarcoLogicoActividad;
use app\models\Actividad;
use app\models\Componente;
use app\models\MarcoLogicoComponente;
use app\models\RubroElegible;

use app\models\Reporte;
use SoapClient;

class ReporteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionMarcoLogico($investigadorID=null)
    {
        $this->layout='estandar';
        $investigador=Investigador::findOne($investigadorID);
        $model = $this->InformacionGeneral($investigador->ID);
        $finProyectoDetalle = MarcoLogicoFinProyecto::find()->where(['Estado'=>1,'ProyectoID'=>$model->ID])->all();
        return $this->render('marco-logico',['model'=>$model,'finProyectoDetalle'=>$finProyectoDetalle]);
    }
    
    
    public function InformacionGeneral($investigador=null)
    {
        $model=InformacionGeneral::find()
                ->select('Proyecto.Fin,Proyecto.Proposito,Proyecto.ID,InformacionGeneral.TituloProyecto,InformacionGeneral.InvestigadorID,Usuario.username CodigoProyecto')
                ->innerJoin('Investigador','Investigador.ID=InformacionGeneral.InvestigadorID')
                ->innerJoin('Proyecto','Proyecto.InvestigadorID=Investigador.ID')
                ->innerJoin('Usuario','Usuario.ID=Investigador.UsuarioID')
                ->where('InformacionGeneral.InvestigadorID=:InvestigadorID',['InvestigadorID'=>$investigador])
                ->one();
                
        if(!$model)
        {
            $model=new InformacionGeneral;
        }
        return $model;
    }



    /* CONSOLIDADOS */
    
    public function actionConsolidadoBid(){
        $this->layout='estandar';
        $rubroelegible = RubroElegible::find()->all();

        $totalRubro = Yii::$app->db->createCommand('SELECT A.RubroElegibleID,A.Nombre,sum(A.total) total FROM (
                SELECT a.RubroElegibleID,c.Nombre,d.ID AS EntidadID,i.UsuarioID,(b.CostoUnitario*b.MetaFisica) total FROM ActRubroElegible a
                INNER JOIN AreSubCategoria b ON b.ActRubroElegibleID=a.ID
                INNER JOIN RubroElegible c ON c.ID=a.RubroElegibleID
                INNER JOIN EntidadParticipante d ON d.ID=b.EntidadParticipanteID
                INNER JOIN Investigador i ON i.UsuarioID = d.InvestigadorID) A
                GROUP BY A.RubroElegibleID,A.Nombre')->queryAll();

        $entidad = EntidadParticipante::find()->all();
        $users = Usuario::find()->where(['tipo'=>1])->all();

        return $this->render('consolidado-bid',['rubro' => $rubroelegible,'totalRubro'=>$totalRubro,'entidad'=>$entidad,'users'=>$users]);
    }


    public function actionConsolidadoBidFiltro($proyectos = null,$colaborador = null){
        // $this->layout='estandar';
        $where = '';
        $group = '';
        if(isset($proyectos) && trim($proyectos)!=''){
            $where .= ' A.UsuarioID ='.$proyectos;
            $group .= ',A.UsuarioID';
        }

        if(isset($colaborador) && trim($colaborador)!=''){
            $where .= ' AND A.EntidadID ='.$colaborador;
            $group .= ',A.EntidadID';
        }

        $totalRubro = Yii::$app->db->createCommand('SELECT A.RubroElegibleID,A.Nombre,sum(A.total) total FROM (
                SELECT a.RubroElegibleID,c.Nombre,d.ID AS EntidadID,i.UsuarioID,(b.CostoUnitario*b.MetaFisica) total FROM ActRubroElegible a
                INNER JOIN AreSubCategoria b ON b.ActRubroElegibleID=a.ID
                INNER JOIN RubroElegible c ON c.ID=a.RubroElegibleID
                INNER JOIN EntidadParticipante d ON d.ID=b.EntidadParticipanteID
                INNER JOIN Investigador i ON i.UsuarioID = d.InvestigadorID) A WHERE
                 '.$where.' 
                GROUP BY A.RubroElegibleID,A.Nombre'.$group)->queryAll();
        // A.RubroElegibleID,A.Nombre,A.UsuarioID 
        // print_r($totalRubro);
        if(!empty($totalRubro)){
            foreach ($totalRubro as $rub):
                    echo '<tr>';
                    echo '<th>'.$rub['Nombre'].'</th>';
                    echo '<td>'.$rub['total'].'</td>';
                    echo '</tr>';
            endforeach;
        }else{
            echo '<tr>
                    <th>Personal y Servicios</th>
                    <td>0</td>
                </tr>
                <tr>
                    <th>Pasajes y viáticos</th>
                    <td>0</td>
                </tr>
                <tr>
                    <th>Insumos y materiales</th>
                    <td>0</td>
                </tr>
                <tr>
                    <th>Bienes y Equipos</th>
                    <td>0</td>
                </tr>
                <tr>
                    <th>Capacitación, divulgación y comunicaciones</th>
                    <td>0</td>
                </tr>';
        }
        // return $this->render('consolidado-bid',['totalRubro'=>$totalRubro]);
    }

    public function actionProyectoEntidades()
    {
        if(isset($_POST['proyectos']) && trim($_POST['proyectos'])!='')
        {
            $proyectos=$_POST['proyectos'];

            $investiga = Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$proyectos])->one();

            $proyects=EntidadParticipante::find()->select('ID,RazonSocial')
                ->where('InvestigadorIDx=:proyectos',[':proyectos'=>$investiga->ID])
                ->all();
            

            if(!empty($proyects)){
                echo "<option value>[SELECCIONE]</option>";
                foreach($proyects as $entidad){
                    echo "<option value='".$entidad->ID."'>".$entidad->RazonSocial."</option>";
                }
            }
            else{
                echo "<option value>[SELECCIONE]</option>";
            }
        }
    }

    /* END CONSOLIDADOS */
    public function actionMapa()
    {
        $this->layout='estandar';
        
        return $this->render('mapa');
    }
    
    public function actionUafsi()
    {
        $this->layout='estandar';
        
        return $this->render('uafsi');
    }
    
    public function actionListadoRegion()
    {
        $this->layout='estandar';
        $ubigeos=InformacionGeneral::find()
                    ->select('Ubigeo.ID,Ubigeo.Nombre,count(Ubigeo.Nombre) Cantidad')
                    ->innerJoin('Ubigeo','Ubigeo.ID=substring(DistritoID,1,2)')
                    ->groupBy('Ubigeo.ID,Ubigeo.Nombre')
                    ->all();
        return $this->render('listado-region',['ubigeos'=>$ubigeos]);
    }
    
    public function actionReporteRegion($regionid=null)
    {
        if(isset($regionid) && $regionid!='')
        {
            $ID='0';
            if($regionid=='pe-am')
            {
                $ID='01';
            }
            elseif($regionid=='pe-ay')
            {
                $ID='05';
            }
            elseif($regionid=='pe-cj')
            {
                $ID='06';
            }
            elseif($regionid=='pe-cs')
            {
                $ID='08';
            }
            elseif($regionid=='pe-ic')
            {
                $ID='11';
            }
            elseif($regionid=='pe-ju')
            {
                $ID='12';
            }
            elseif($regionid=='pe-lb')
            {
                $ID='14';
            }
            elseif($regionid=='pe-lr')
            {
                $ID='15';
            }
            elseif($regionid=='pe-lo')
            {
                $ID='16';
            }
            elseif($regionid=='pe-pi')
            {
                $ID='20';
            }
            elseif($regionid=='pe-cl')
            {
                $ID='21';
            }
            elseif($regionid=='pe-sm')
            {
                $ID='22';
            }
            elseif($regionid=='pe-uc')
            {
                $ID='25';
            }
            $ubigeo=InformacionGeneral::find()
                    ->select('Ubigeo.ID,count(Ubigeo.Nombre) Cantidad')
                    ->innerJoin('Ubigeo','Ubigeo.ID=substring(DistritoID,1,2)')
                    ->where('Ubigeo.ID=:ID',[':ID'=>$ID])
                    ->groupBy('Ubigeo.ID')
                    ->one();
            
            if($ubigeo)
            {
                echo $ubigeo->Cantidad;
            }
            else
            {
                echo 0;
            }
        }
    }
    
    public function actionDinamico()
    {
        $this->layout='estandar';
        return $this->render('dinamico');
    }
    
    public function actionDinamicoRecursos()
    {
        $this->layout='estandar';
        return $this->render('dinamico-recursos');
    }
    
    public function actionColaboradora()
    {
        $this->layout='estandar';
        return $this->render('colaboradora');
    }
    
    public function actionAmbito()
    {
        $this->layout='estandar';
        return $this->render('ambito');
    }
    
    public function actionExperimentoEvento()
    {
        $this->layout='estandar';
        return $this->render('experimento-evento');
    }
    public function actionEstructuraDinamico()
    {
        $data=[];
        $resultados = (new \yii\db\Query())
            ->select('InformacionGeneral.Codigo,
                    InformacionGeneral.TituloProyecto,
                    ProgramaTransversal.Nombre ProgramaTransversalNombre,
                    d.Nombre dNombre,
                    p.Nombre pNombre,
                    r.Nombre rNombre,
                    DireccionLinea.Nombre DireccionLineaNombre,
                    TipoInvestigacion.Nombre TipoInvestigacionNombre, 
                    UnidadOperativa.Nombre UnidadOperativaNombre,
                    UnidadEjecutora.Nombre UnidadEjecutoraNombre,
                    Especie.Nombre EspecieNombre,
                    CultivoCrianza.Nombre CultivoCrianzaNombre,
                    Programa.Nombre ProgramaNombre,
                EntidadParticipante.AporteMonetario EntidadParticipanteAporteMonetario')
            ->from('InformacionGeneral')
            ->innerJoin('ProgramaTransversal','ProgramaTransversal.ID=InformacionGeneral.ProgramaTransversalID')
            ->innerJoin('Ubigeo d','d.ID=InformacionGeneral.DistritoID')
            ->innerJoin('Ubigeo p','p.ID=substring(InformacionGeneral.DistritoID,1,4)')
            ->innerJoin('Ubigeo r','r.ID=substring(InformacionGeneral.DistritoID,1,2)')
            ->innerJoin('DireccionLinea','DireccionLinea.ID=InformacionGeneral.DireccionLineaID')
            ->innerJoin('TipoInvestigacion','TipoInvestigacion.ID=InformacionGeneral.TipoInvestigacionID')
            ->innerJoin('UnidadOperativa','UnidadOperativa.ID=InformacionGeneral.UnidadOperativaID')
            ->innerJoin('UnidadEjecutora','UnidadEjecutora.ID=UnidadOperativa.UnidadEjecutoraID')
            ->innerJoin('Especie','Especie.ID=InformacionGeneral.EspecieID')
            ->innerJoin('CultivoCrianza','CultivoCrianza.ID=Especie.CultivoCrianzaID')
            ->innerJoin('Programa','Programa.ID=CultivoCrianza.ProgramaID')
            ->innerJoin('EntidadParticipante','EntidadParticipante.InvestigadorID=InformacionGeneral.InvestigadorID AND EntidadParticipante.TipoInstitucionID=3')
            ->all();
        $a=1;
        foreach($resultados as $resultado)
        {
            $data[] = [
                       'Codigo'=>$resultado['Codigo'],
                       'TituloProyecto'=>$resultado['TituloProyecto'],
                       'ProgramaTransversal'=>$resultado['ProgramaTransversalNombre'],
                       'Distrito'=>$resultado['dNombre'],
                       'Provincia'=>$resultado['pNombre'],
                       'Region'=>$resultado['rNombre'],
                       'DireccionLinea'=>$resultado['DireccionLineaNombre'],
                       'TipoInvestigacion'=>$resultado['TipoInvestigacionNombre'],
                       'UnidadOperativa'=>$resultado['UnidadEjecutoraNombre'],
                       'UnidadEjecutora'=>$resultado['UnidadEjecutoraNombre'],
                       'Especie'=>$resultado['EspecieNombre'],
                       'CultivoCrianza'=>$resultado['CultivoCrianzaNombre'],
                       'Programa'=>$resultado['ProgramaNombre'],
                       'MontoPNIA'=>$resultado['EntidadParticipanteAporteMonetario'],
                       'Cantidad'=>$a,];
        }
        
        echo json_encode($data);
    }
    
    public function actionEstructuraColaboradora()
    {
        $data=[];
        $resultados = (new \yii\db\Query())
            ->select('InformacionGeneral.Codigo,
                     InformacionGeneral.TituloProyecto,
                ProgramaTransversal.Nombre ProgramaTransversalNombre,
                d.Nombre dNombre,
                p.Nombre pNombre,
                r.Nombre rNombre,
                DireccionLinea.Nombre DireccionLineaNombre,
                TipoInvestigacion.Nombre TipoInvestigacionNombre, 
                UnidadOperativa.Nombre UnidadOperativaNombre,
                UnidadEjecutora.Nombre UnidadEjecutoraNombre,
                Especie.Nombre EspecieNombre,
                CultivoCrianza.Nombre CultivoCrianzaNombre,
                Programa.Nombre ProgramaNombre,
                EntidadParticipante.RazonSocial EntidadParticipanteRazonSocial,
                EntidadParticipante.AporteMonetario EntidadParticipanteAporteMonetario,
                EntidadParticipante.AporteNoMonetario EntidadParticipanteAporteNoMonetario,
                TipoInstitucion.Nombre TipoColaboradora')
            ->from('InformacionGeneral')
            ->innerJoin('ProgramaTransversal','ProgramaTransversal.ID=InformacionGeneral.ProgramaTransversalID')
            ->innerJoin('Ubigeo d','d.ID=InformacionGeneral.DistritoID')
            ->innerJoin('Ubigeo p','p.ID=substring(InformacionGeneral.DistritoID,1,4)')
            ->innerJoin('Ubigeo r','r.ID=substring(InformacionGeneral.DistritoID,1,2)')
            ->innerJoin('DireccionLinea','DireccionLinea.ID=InformacionGeneral.DireccionLineaID')
            ->innerJoin('TipoInvestigacion','TipoInvestigacion.ID=InformacionGeneral.TipoInvestigacionID')
            ->innerJoin('UnidadOperativa','UnidadOperativa.ID=InformacionGeneral.UnidadOperativaID')
            ->innerJoin('UnidadEjecutora','UnidadEjecutora.ID=UnidadOperativa.UnidadEjecutoraID')
            ->innerJoin('Especie','Especie.ID=InformacionGeneral.EspecieID')
            ->innerJoin('CultivoCrianza','CultivoCrianza.ID=Especie.CultivoCrianzaID')
            ->innerJoin('Programa','Programa.ID=CultivoCrianza.ProgramaID')
            ->innerJoin('EntidadParticipante','EntidadParticipante.InvestigadorID=InformacionGeneral.InvestigadorID')
            ->innerJoin('TipoInstitucion','TipoInstitucion.ID=EntidadParticipante.TipoInstitucionID')
            ->all();
        $a=1;
        foreach($resultados as $resultado)
        {
            $data[] = [
                       'TituloProyecto'=>$resultado['Codigo'].' '.$resultado['TituloProyecto'],
                       'ProgramaTransversal'=>$resultado['ProgramaTransversalNombre'],
                       'Distrito'=>$resultado['dNombre'],
                       'Provincia'=>$resultado['pNombre'],
                       'Region'=>$resultado['rNombre'],
                       'DireccionLinea'=>$resultado['DireccionLineaNombre'],
                       'TipoInvestigacion'=>$resultado['TipoInvestigacionNombre'],
                       'UnidadOperativa'=>$resultado['UnidadEjecutoraNombre'],
                       'UnidadEjecutora'=>$resultado['UnidadEjecutoraNombre'],
                       'Especie'=>$resultado['EspecieNombre'],
                       'CultivoCrianza'=>$resultado['CultivoCrianzaNombre'],
                       'Programa'=>$resultado['ProgramaNombre'],
                       'Colaboradora'=>$resultado['EntidadParticipanteRazonSocial'],
                       'AporteMonetario'=>$resultado['EntidadParticipanteAporteMonetario'],
                       'AporteNoMonetario'=>$resultado['EntidadParticipanteAporteNoMonetario'],
                       'TipoColaboradora'=>$resultado['TipoColaboradora'],
                       'Cantidad'=>$a,];
        }
        
        echo json_encode($data);
    }
    
    public function actionEstructuraAmbito()
    {
        $data=[];
        $resultados = (new \yii\db\Query())
            ->select('InformacionGeneral.Codigo,
                     InformacionGeneral.TituloProyecto,
                ProgramaTransversal.Nombre ProgramaTransversalNombre,
                d.Nombre dNombre,
                p.Nombre pNombre,
                r.Nombre rNombre,
                DireccionLinea.Nombre DireccionLineaNombre,
                TipoInvestigacion.Nombre TipoInvestigacionNombre, 
                UnidadOperativa.Nombre UnidadOperativaNombre,
                UnidadEjecutora.Nombre UnidadEjecutoraNombre,
                Especie.Nombre EspecieNombre,
                CultivoCrianza.Nombre CultivoCrianzaNombre,
                Programa.Nombre ProgramaNombre,
                dAmbito.Nombre dAmbitoNombre,
                pAmbito.Nombre pAmbitoNombre,
                rAmbito.Nombre rAmbitoNombre,
                ')
            ->from('InformacionGeneral')
            ->innerJoin('ProgramaTransversal','ProgramaTransversal.ID=InformacionGeneral.ProgramaTransversalID')
            ->innerJoin('Ubigeo d','d.ID=InformacionGeneral.DistritoID')
            ->innerJoin('Ubigeo p','p.ID=substring(InformacionGeneral.DistritoID,1,4)')
            ->innerJoin('Ubigeo r','r.ID=substring(InformacionGeneral.DistritoID,1,2)')
            ->innerJoin('DireccionLinea','DireccionLinea.ID=InformacionGeneral.DireccionLineaID')
            ->innerJoin('TipoInvestigacion','TipoInvestigacion.ID=InformacionGeneral.TipoInvestigacionID')
            ->innerJoin('UnidadOperativa','UnidadOperativa.ID=InformacionGeneral.UnidadOperativaID')
            ->innerJoin('UnidadEjecutora','UnidadEjecutora.ID=UnidadOperativa.UnidadEjecutoraID')
            ->innerJoin('Especie','Especie.ID=InformacionGeneral.EspecieID')
            ->innerJoin('CultivoCrianza','CultivoCrianza.ID=Especie.CultivoCrianzaID')
            ->innerJoin('Programa','Programa.ID=CultivoCrianza.ProgramaID')
            ->innerJoin('AmbitoIntervencion','AmbitoIntervencion.InformacionGeneralID=InformacionGeneral.ID')
            ->innerJoin('Ubigeo dAmbito','dAmbito.ID=AmbitoIntervencion.DistritoID')
            ->innerJoin('Ubigeo pAmbito','pAmbito.ID=substring(AmbitoIntervencion.DistritoID,1,4)')
            ->innerJoin('Ubigeo rAmbito','rAmbito.ID=substring(AmbitoIntervencion.DistritoID,1,2)')
            ->all();
        $a=1;
        foreach($resultados as $resultado)
        {
            $data[] = [
                       'TituloProyecto'=>$resultado['Codigo'].' '.$resultado['TituloProyecto'],
                       'ProgramaTransversal'=>$resultado['ProgramaTransversalNombre'],
                       'Distrito'=>$resultado['dNombre'],
                       'Provincia'=>$resultado['pNombre'],
                       'Region'=>$resultado['rNombre'],
                       'DireccionLinea'=>$resultado['DireccionLineaNombre'],
                       'TipoInvestigacion'=>$resultado['TipoInvestigacionNombre'],
                       'UnidadOperativa'=>$resultado['UnidadEjecutoraNombre'],
                       'UnidadEjecutora'=>$resultado['UnidadEjecutoraNombre'],
                       'Especie'=>$resultado['EspecieNombre'],
                       'CultivoCrianza'=>$resultado['CultivoCrianzaNombre'],
                       'Programa'=>$resultado['ProgramaNombre'],
                       'DistritoAmbito'=>$resultado['dAmbitoNombre'],
                       'ProvinciaAmbito'=>$resultado['pAmbitoNombre'],
                       'RegionAmbito'=>$resultado['rAmbitoNombre'],
                       'Cantidad'=>$a,];
        }
        
        echo json_encode($data);
    }
    
    public function actionEstructuraDinamicoRecurso()
    {
        $data=[];
        $resultados = (new \yii\db\Query())
            ->select('InformacionGeneral.Codigo,
                InformacionGeneral.TituloProyecto,
                Poa.Anno,
                d.Nombre dNombre,
                p.Nombre pNombre,
                r.Nombre rNombre,
                Componente.Correlativo ComponenteCorrelativo,
                Componente.Nombre ComponenteNombre, 
                Componente.Peso ComponentePeso,
                Componente.Experimento ComponenteExperimento,
                Componente.Evento ComponenteEvento,
                Actividad.Correlativo ActividadCorrelativo, 
                Actividad.Nombre ActividadNombre,
                Actividad.Peso ActividadPeso,
                Actividad.Experimento ActividadExperimento,
                Actividad.Evento ActividadEvento,
                RubroElegible.Nombre RubroElegibleNombre,
                AreSubCategoria.Nombre AreSubCategoriaNombre,
                AreSubCategoria.UnidadMedida,
                AreSubCategoria.CostoUnitario,
                AreSubCategoria.MetaFisica,
                AreSubCategoria.Total')
            ->from('InformacionGeneral')
            ->innerJoin('Proyecto','Proyecto.InvestigadorID=InformacionGeneral.InvestigadorID')
            ->innerJoin('Ubigeo d','d.ID=InformacionGeneral.DistritoID')
            ->innerJoin('Ubigeo p','p.ID=substring(InformacionGeneral.DistritoID,1,4)')
            ->innerJoin('Ubigeo r','r.ID=substring(InformacionGeneral.DistritoID,1,2)')
            ->innerJoin('Poa','Poa.ProyectoID=Proyecto.ID')
            ->innerJoin('Componente','Componente.PoaID=Poa.ID')
            ->innerJoin('Actividad','Actividad.ComponenteID=Componente.ID')
            ->innerJoin('ActRubroElegible','ActRubroElegible.ActividadID=Actividad.ID')
            ->innerJoin('RubroElegible','RubroElegible.ID=ActRubroElegible.RubroElegibleID')
            ->innerJoin('AreSubCategoria','AreSubCategoria.ActRubroElegibleID=ActRubroElegible.ID')
            ->all();
        $a=1;
        foreach($resultados as $resultado)
        {
            $data[] = [
                       'TituloProyecto'=>$resultado['TituloProyecto'],
                       'Codigo'=>$resultado['Codigo'],
                       'ComponenteNombre'=>$resultado['ComponenteNombre'],
                       'ActividadNombre'=>$resultado['ActividadNombre'],
                       'RubroElegibleNombre'=>$resultado['RubroElegibleNombre'],
                       'AreSubCategoriaNombre'=>$resultado['AreSubCategoriaNombre'],
                       'UnidadMedida'=>$resultado['UnidadMedida'],
                       'Distrito'=>$resultado['dNombre'],
                       'Provincia'=>$resultado['pNombre'],
                       'Region'=>$resultado['rNombre'],
                       //'ComponentePeso'=>$resultado['ComponentePeso'],
                       //'ComponenteExperimento'=>$resultado['ComponenteExperimento'],
                       //'ComponenteEvento'=>$resultado['ComponenteEvento'],
                       //'ActividadPeso'=>$resultado['ActividadPeso'],
                       //'ActividadExperimento'=>$resultado['ActividadExperimento'],
                       //'ActividadEvento'=>$resultado['ActividadEvento'],
                       //'ActividadExperimento'=>$resultado['ActividadExperimento'],
                       'CostoUnitario'=>$resultado['CostoUnitario'],
                       'MetaFisica'=>$resultado['MetaFisica'],
                       'Total'=>$resultado['Total'],
                       'Cantidad'=>$a,
                       ];
        }
        
        echo json_encode($data);
    }
    
    public function actionEstructuraExperimentoEvento()
    {
        $data=[];
        $resultados = (new \yii\db\Query())
            ->select('InformacionGeneral.Codigo,
                InformacionGeneral.TituloProyecto,
                Poa.Anno,
                ProgramaTransversal.Nombre ProgramaTransversalNombre,
                d.Nombre dNombre,
                p.Nombre pNombre,
                r.Nombre rNombre,
                DireccionLinea.Nombre DireccionLineaNombre,
                TipoInvestigacion.Nombre TipoInvestigacionNombre, 
                UnidadOperativa.Nombre UnidadOperativaNombre,
                UnidadEjecutora.Nombre UnidadEjecutoraNombre,
                Especie.Nombre EspecieNombre,
                CultivoCrianza.Nombre CultivoCrianzaNombre,
                Programa.Nombre ProgramaNombre,
                Componente.Correlativo ComponenteCorrelativo,
                Componente.Nombre ComponenteNombre, 
                Componente.Peso ComponentePeso,
                Componente.Experimento ComponenteExperimento,
                Componente.Evento ComponenteEvento,
                Actividad.Correlativo ActividadCorrelativo, 
                Actividad.Nombre ActividadNombre,
                Actividad.Peso ActividadPeso,
                Actividad.Experimento ActividadExperimento,
                Actividad.Evento ActividadEvento')
            ->from('InformacionGeneral')
            ->innerJoin('Proyecto','Proyecto.InvestigadorID=InformacionGeneral.InvestigadorID')
            ->innerJoin('ProgramaTransversal','ProgramaTransversal.ID=InformacionGeneral.ProgramaTransversalID')
            ->innerJoin('Ubigeo d','d.ID=InformacionGeneral.DistritoID')
            ->innerJoin('Ubigeo p','p.ID=substring(InformacionGeneral.DistritoID,1,4)')
            ->innerJoin('Ubigeo r','r.ID=substring(InformacionGeneral.DistritoID,1,2)')
            ->innerJoin('DireccionLinea','DireccionLinea.ID=InformacionGeneral.DireccionLineaID')
            ->innerJoin('TipoInvestigacion','TipoInvestigacion.ID=InformacionGeneral.TipoInvestigacionID')
            ->innerJoin('UnidadOperativa','UnidadOperativa.ID=InformacionGeneral.UnidadOperativaID')
            ->innerJoin('UnidadEjecutora','UnidadEjecutora.ID=UnidadOperativa.UnidadEjecutoraID')
            ->innerJoin('Especie','Especie.ID=InformacionGeneral.EspecieID')
            ->innerJoin('CultivoCrianza','CultivoCrianza.ID=Especie.CultivoCrianzaID')
            ->innerJoin('Programa','Programa.ID=CultivoCrianza.ProgramaID')
            ->innerJoin('Poa','Poa.ProyectoID=Proyecto.ID')
            ->innerJoin('Componente','Componente.PoaID=Poa.ID')
            ->innerJoin('Actividad','Actividad.ComponenteID=Componente.ID')
            ->all();
        $a=1;
        foreach($resultados as $resultado)
        {
            $data[] = [
                       'TituloProyecto'=>$resultado['Codigo'].' '.$resultado['TituloProyecto'],
                       'ProgramaTransversal'=>$resultado['ProgramaTransversalNombre'],
                       'Distrito'=>$resultado['dNombre'],
                       'Provincia'=>$resultado['pNombre'],
                       'Region'=>$resultado['rNombre'],
                       'DireccionLinea'=>$resultado['DireccionLineaNombre'],
                       'TipoInvestigacion'=>$resultado['TipoInvestigacionNombre'],
                       'UnidadOperativa'=>$resultado['UnidadEjecutoraNombre'],
                       'UnidadEjecutora'=>$resultado['UnidadEjecutoraNombre'],
                       'Especie'=>$resultado['EspecieNombre'],
                       'CultivoCrianza'=>$resultado['CultivoCrianzaNombre'],
                       'Programa'=>$resultado['ProgramaNombre'],
                       'ComponenteNombre'=>$resultado['ComponenteNombre'],
                       'ActividadNombre'=>$resultado['ActividadNombre'],
                       'Distrito'=>$resultado['dNombre'],
                       'Provincia'=>$resultado['pNombre'],
                       'Region'=>$resultado['rNombre'],
                       'ActividadExperimento'=>$resultado['ActividadExperimento'],
                       'ActividadEvento'=>$resultado['ActividadEvento'],
                       'Cantidad'=>$a,
                       ];
        }
        
        echo json_encode($data);
    }
    
    public function actionServicio()
    {
        
        $cliente = new SoapClient("http://ws.pide.gob.pe/ServiceGradoAcademico?wsdl");

        //obtener las funciones a las que puedo llamar
        $funciones = $cliente->__getFunctions();
        
        echo "<h2>Funciones del servicio</h2>";
        foreach ($funciones as $funcion) {
                echo $funcion  . "<br />";
        }
        
        //obtener los tipos de datos involucrados
        echo "<h2>Tipos en el servicio</h2>";
        $tipos = $cliente->__getTypes();
        
        foreach ($tipos as $tipo) {
                echo $tipo . "<br />";
        }
    }

    
    public function actionFenomenoClimatologico()
    {
        $this->layout='estandar';
        $climatologicos = (new \yii\db\Query())
            ->select('TipoImpacto.ID,TipoImpacto.Nomb,count(InformacionGeneral.Codigo) cantidad')
            ->from('InformacionGeneral')
            ->innerJoin('TipoImpacto','InformacionGeneral.TipoImpactoID=TipoImpacto.ID')
            ->groupBy('TipoImpacto.ID,TipoImpacto.Nomb')
            ->all();
        
        
        $unidadesEjecutorias = Yii::$app->db->createCommand('SELECT AA.Nombre,sum(AA.cantidad) cantidad,sum(AA.afectados) afectados FROM 
                                                (
                                                SELECT
                                                UnidadOperativa.ID,
                                                UnidadOperativa.Nombre,
                                                count(InformacionGeneral.Codigo) cantidad,
                                                (SELECT count(a.Codigo) FROM InformacionGeneral a WHERE a.UnidadOperativaID=UnidadOperativa.ID AND a.TipoImpactoID IN (2,3,4) GROUP BY a.UnidadOperativaID) afectados
                                                FROM InformacionGeneral 
                                                INNER JOIN UnidadOperativa ON UnidadOperativa.ID=InformacionGeneral.UnidadOperativaID
                                                --where InformacionGeneral.TipoImpactoID IN (2,3,4)
                                                WHERE UnidadOperativaID IN (9,
                                                10,
                                                6,
                                                11,
                                                12,
                                                7,
                                                3,
                                                13,
                                                8,
                                                16,
                                                17,
                                                18)
                                                GROUP BY 
                                                UnidadOperativa.ID,UnidadOperativa.Nombre,UnidadOperativa.ID) AA
                                                GROUP BY AA.NOMBRE')->queryAll();
            
            
        return $this->render('fenomeno-climatologico',['climatologicos'=>$climatologicos,'unidadesEjecutorias'=>$unidadesEjecutorias]);
    }
    
    public function actionFenomenoClimatologicoR1($TipoImpactoID)
    {
        $data=[];
        $climatologicos = (new \yii\db\Query())
            ->select('InformacionGeneral.*')
            ->from('InformacionGeneral')
            ->where('TipoImpactoID=:TipoImpactoID',[':TipoImpactoID'=>$TipoImpactoID])
            ->all();
            
        $a=1;
        $html='';
        $html.='<table class="table table-bordered"><tr><td style="background:#F5F5F6">Código Proyecto</td><td style="background:#F5F5F6">Título Proyecto</td></tr>';
        foreach($climatologicos as $climatologico)
        {
            $html.='<tr><td><a target=_blank href="'.Yii::$app->getUrlManager()->createUrl('evaluacion-proyecto/evaluacion-proyecto?investigadorID='.$climatologico['InvestigadorID'].'#tab-informacion-general').'">'.$climatologico['Codigo'].'</a></td><td>'.$climatologico['TituloProyecto'].'</td></tr>';
        }
        $html.='</table>';
        echo $html;
        
    }

    public function actionEstacionExperimentalGrafico(){
        $this->layout='estandar';
        $estacion = (new \yii\db\Query())
            ->select('UnidadEjecutora.ID,UnidadEjecutora.Nombre AS Estacion,Count(Investigador.ID) AS Proyecto')
            ->from('Investigador')
            ->innerJoin('InformacionGeneral','InformacionGeneral.InvestigadorID = Investigador.ID')
            ->innerJoin('UnidadOperativa','InformacionGeneral.UnidadOperativaID = UnidadOperativa.ID')
            ->innerJoin('UnidadEjecutora','UnidadOperativa.UnidadEjecutoraID = UnidadEjecutora.ID')
            ->groupBy('UnidadEjecutora.ID,UnidadEjecutora.Nombre')
            ->all();


        foreach ($estacion as $est) {
            $proyectosEstacion[] = array(
                'Estacion' => $est['Estacion'],
                'ProyectoCant' => $this->detaEeaa($est['ID'])
            );
        }

        return $this->render('estacion-experimental-grafico',['estacion'=>$estacion,'cantUnidad'=>$proyectosEstacion]);
    }
    
    public function actionMensualFinancieroGrafico(){
        $this->layout='estandar';
        $estaciones=UnidadOperativa::find()->all();
        $reporte=new Reporte;
        $Totales = (new \yii\db\Query())
            ->select('Mes,sum(MetaFisica*CostoUnitario) Total')
            ->from('CronogramaAreSubCategoria')
            ->where('MetaFisica<>0')
            ->GroupBy('Mes')
            ->OrderBy('Mes asc, Total asc')
            ->all();
        
        foreach ($Totales as $Total) {
            $mesesTotales[] = array(
                'Mes' => $Total['Mes'],
                'Total' => $Total['Total']
            );
        }

        return $this->render('mensual-financiero-grafico',['mesesTotales'=>$mesesTotales,'Totales'=>$Totales,'estaciones'=>$estaciones]);
    }
    
    public function actionMensualFinancieroGrillaJson($estacion=null)
    {
            
                
        if(!$estacion)
        {
            $unidadesoperativas = (new \yii\db\Query())
                ->select('UnidadOperativa.ID,UnidadOperativa.Nombre')
                ->from('UnidadOperativa')
                ->all();
        }
        else
        {
           $unidadesoperativas = (new \yii\db\Query())
                ->select('UnidadOperativa.ID,UnidadOperativa.Nombre')
                ->from('UnidadOperativa')
                ->where('UnidadOperativa.ID=:ID',[':ID'=>$estacion])
                ->all();
        }
        
        foreach ($unidadesoperativas as $unidadoperativa) {
            echo    '<tr>';
            echo        '<td>'.$unidadoperativa['Nombre'].'</td>';
                        
                        $mes=1;
                        $suma=0;
                        for($i=0;$i<36;$i++)
                        {
                            $Total = (new \yii\db\Query())
                                ->select('CronogramaAreSubCategoria.Mes,sum(CronogramaAreSubCategoria.MetaFisica*CronogramaAreSubCategoria.CostoUnitario) Total')
                                ->from('CronogramaAreSubCategoria')
                                ->innerJoin('AreSubCategoria','AreSubCategoria.ID=CronogramaAreSubCategoria.AreSubCategoriaID')
                                ->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')
                                ->innerJoin('Actividad','Actividad.ID=ActRubroElegible.ActividadID')
                                ->innerJoin('Componente','Componente.ID=Actividad.ComponenteID')
                                ->innerJoin('Poa','Poa.ID=Componente.PoaID')
                                ->innerJoin('Proyecto','Proyecto.ID=Poa.ProyectoID')
                                ->innerJoin('InformacionGeneral','InformacionGeneral.InvestigadorID=Proyecto.InvestigadorID')
                                ->where('CronogramaAreSubCategoria.MetaFisica<>0 and InformacionGeneral.UnidadOperativaID=:UnidadOperativaID and CronogramaAreSubCategoria.Mes=:Mes',[':UnidadOperativaID'=>$unidadoperativa['ID'],':Mes'=>$mes])
                                ->GroupBy('CronogramaAreSubCategoria.Mes')
                                ->OrderBy('CronogramaAreSubCategoria.Mes asc, Total asc')
                                ->one();
                            if($Total && $mes==$Total['Mes'])
                            {
                                echo '<td>'.$Total['Total'].'</td>';
                            }
                            else
                            {
                                echo '<td>0</td>';
                            }
                            $mes++;
                            $suma=$suma+$Total['Total'];
                        }
            echo        '<td>'.$suma.'</td>';
            echo    '</tr>';
        }
    }
    


    public function actionMensualFinancieroGraficoProyecto(){
        $this->layout='estandar';
        // $estaciones=UnidadOperativa::find()->all();
        $proyectos=InformacionGeneral::find()->all();
        $reporte=new Reporte;
        $Totales = (new \yii\db\Query())
            ->select('Mes,sum(MetaFisica*CostoUnitario) Total')
            ->from('CronogramaAreSubCategoria')
            ->where('MetaFisica<>0')
            ->GroupBy('Mes')
            ->OrderBy('Mes asc, Total asc')
            ->all();
        
        foreach ($Totales as $Total) {
            $mesesTotales[] = array(
                'Mes' => $Total['Mes'],
                'Total' => $Total['Total']
            );
        }

        return $this->render('mensual-financiero-grafico-proyecto',['mesesTotales'=>$mesesTotales,'Totales'=>$Totales,'proyectos'=>$proyectos]);
    }
    
    public function actionMensualFinancieroGrillaProyectoJson($estacion=null)
    {
            
                
        if(!$estacion)
        {
            $informacionGeneral = (new \yii\db\Query())
                ->select('InformacionGeneral.ID,InformacionGeneral.Codigo')
                ->from('InformacionGeneral')
                ->all();
        }
        else
        {
           $informacionGeneral = (new \yii\db\Query())
                ->select('UnidadOperativa.ID,UnidadOperativa.Nombre')
                ->from('UnidadOperativa')
                ->where('UnidadOperativa.ID=:ID',[':ID'=>$estacion])
                ->all();
        }
        
        foreach ($informacionGeneral as $proyecto) {
            echo    '<tr>';
            echo        '<td>'.$proyecto['Codigo'].'</td>';
                        
                        $mes=1;
                        $suma=0;
                        for($i=0;$i<36;$i++)
                        {
                            $Total = (new \yii\db\Query())
                                ->select('CronogramaAreSubCategoria.Mes,sum(CronogramaAreSubCategoria.MetaFisica*CronogramaAreSubCategoria.CostoUnitario) Total')
                                ->from('CronogramaAreSubCategoria')
                                ->innerJoin('AreSubCategoria','AreSubCategoria.ID=CronogramaAreSubCategoria.AreSubCategoriaID')
                                ->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')
                                ->innerJoin('Actividad','Actividad.ID=ActRubroElegible.ActividadID')
                                ->innerJoin('Componente','Componente.ID=Actividad.ComponenteID')
                                ->innerJoin('Poa','Poa.ID=Componente.PoaID')
                                ->innerJoin('Proyecto','Proyecto.ID=Poa.ProyectoID')
                                ->innerJoin('InformacionGeneral','InformacionGeneral.InvestigadorID=Proyecto.InvestigadorID')
                                ->where('CronogramaAreSubCategoria.MetaFisica<>0 and InformacionGeneral.ID=:ID and CronogramaAreSubCategoria.Mes=:Mes',[':ID'=>$proyecto['ID'],':Mes'=>$mes])
                                ->GroupBy('CronogramaAreSubCategoria.Mes')
                                ->OrderBy('CronogramaAreSubCategoria.Mes asc, Total asc')
                                ->one();
                            if($Total && $mes==$Total['Mes'])
                            {
                                echo '<td>'.$Total['Total'].'</td>';
                            }
                            else
                            {
                                echo '<td>0</td>';
                            }
                            $mes++;
                            $suma=$suma+$Total['Total'];
                        }
            echo        '<td>'.$suma.'</td>';
            echo    '</tr>';
        }
    }

    public function actionMensualFinancieroGraficoProyectoJson($estacion=null)
    {
        $mesesTotales=[];
        if(!$estacion)
        {
            $Totales = (new \yii\db\Query())
            ->select('Mes,sum(MetaFisica*CostoUnitario) Total')
            ->from('CronogramaAreSubCategoria')
            ->where('MetaFisica<>0')
            ->GroupBy('Mes')
            ->OrderBy('Mes asc, Total asc')
            ->all();
        }
        
        
        foreach ($Totales as $Total) {
            $mesesTotales[] = array(
                Yii::$app->tools->DescripcionMes($Total['Mes']),
                (float)$Total['Total']
            );
        }
        
        echo json_encode($mesesTotales);
    }



    public function actionDescargarMensualFinanciero($estacion = null){
        $this->layout='estandar';

        if(!$estacion)
        {
            $unidadesoperativas = (new \yii\db\Query())
                ->select('UnidadOperativa.ID,UnidadOperativa.Nombre')
                ->from('UnidadOperativa')
                ->all();
        }
        else
        {
           $unidadesoperativas = (new \yii\db\Query())
                ->select('UnidadOperativa.ID,UnidadOperativa.Nombre')
                ->from('UnidadOperativa')
                ->where('UnidadOperativa.ID=:ID',[':ID'=>$estacion])
                ->all();
        }
        
        return $this->render('descargar-mensual-financiero',['unidadesoperativas'=>$unidadesoperativas]);
    }


    public function actionMensualFinancieroGraficoJson($estacion=null)
    {
        $mesesTotales=[];
        if(!$estacion)
        {
            $Totales = (new \yii\db\Query())
            ->select('Mes,sum(MetaFisica*CostoUnitario) Total')
            ->from('CronogramaAreSubCategoria')
            ->where('MetaFisica<>0')
            ->GroupBy('Mes')
            ->OrderBy('Mes asc, Total asc')
            ->all();
        }
        else
        {
            $Totales = (new \yii\db\Query())
            ->select('CronogramaAreSubCategoria.Mes,sum(CronogramaAreSubCategoria.MetaFisica*CronogramaAreSubCategoria.CostoUnitario) Total')
            ->from('CronogramaAreSubCategoria')
            ->innerJoin('AreSubCategoria','AreSubCategoria.ID=CronogramaAreSubCategoria.AreSubCategoriaID')
            ->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')
            ->innerJoin('Actividad','Actividad.ID=ActRubroElegible.ActividadID')
            ->innerJoin('Componente','Componente.ID=Actividad.ComponenteID')
            ->innerJoin('Poa','Poa.ID=Componente.PoaID')
            ->innerJoin('Proyecto','Proyecto.ID=Poa.ProyectoID')
            ->innerJoin('InformacionGeneral','InformacionGeneral.InvestigadorID=Proyecto.InvestigadorID')
            ->where('CronogramaAreSubCategoria.MetaFisica<>0 and InformacionGeneral.UnidadOperativaID=:UnidadOperativaID',[':UnidadOperativaID'=>$estacion])
            ->GroupBy('CronogramaAreSubCategoria.Mes')
            ->OrderBy('CronogramaAreSubCategoria.Mes asc, Total asc')
            ->all();
        }
        
        
        foreach ($Totales as $Total) {
            $mesesTotales[] = array(
                Yii::$app->tools->DescripcionMes($Total['Mes']),
                (float)$Total['Total']
            );
        }
        
        echo json_encode($mesesTotales);
    }
    
    private function detaEeaa($id){
        $cantidadUnidad = (new \yii\db\Query())
            ->select('Count(dbo.Investigador.ID) AS Proyectos,ProgramaTransversal.Nombre Estacion,UnidadEjecutora.ID, UnidadEjecutora.Nombre')
            ->from('Investigador')
            ->innerJoin('InformacionGeneral','InformacionGeneral.InvestigadorID = Investigador.ID')
            ->innerJoin('UnidadOperativa','InformacionGeneral.UnidadOperativaID = UnidadOperativa.ID')
            ->innerJoin('UnidadEjecutora','UnidadOperativa.UnidadEjecutoraID = UnidadEjecutora.ID')
            ->innerJoin('ProgramaTransversal','ProgramaTransversal.ID = InformacionGeneral.ProgramaTransversalID')
            ->where(['UnidadEjecutora.ID'=>$id])
            ->groupBy('ProgramaTransversal.Nombre,UnidadEjecutora.ID, UnidadEjecutora.Nombre')
            ->all(); 

        return $cantidadUnidad;
    }

    public function actionDetalleProgramado($id = null){
        $this->layout='vacio';

        $programa = (new \yii\db\Query())
            ->select('Programa.Nombre,count(Programa.Nombre) AS Total')
            ->from('CultivoCrianza')
            ->innerJoin('Programa','Programa.ID = CultivoCrianza.ProgramaID')
            ->innerJoin('Especie','Especie.CultivoCrianzaID = CultivoCrianza.ID')
            ->innerJoin('InformacionGeneral','InformacionGeneral.EspecieID = Especie.ID')
            ->innerJoin('UnidadOperativa','InformacionGeneral.UnidadOperativaID = UnidadOperativa.ID')
            ->innerJoin('UnidadEjecutora','UnidadOperativa.UnidadEjecutoraID = UnidadEjecutora.ID')
            ->where(['UnidadEjecutora.ID'=>$id])
            ->groupBy('Programa.Nombre')
            ->all();

        $listadoPrograma = (new \yii\db\Query())
            ->select('Programa.Nombre,InformacionGeneral.Codigo,CultivoCrianza.Nombre Cultivo,EntidadParticipante.AporteMonetario,EntidadParticipante.AporteNoMonetario,EntidadParticipante.RazonSocial')
            ->from('CultivoCrianza')
            ->innerJoin('Programa','Programa.ID = CultivoCrianza.ProgramaID')
            ->innerJoin('Especie','Especie.CultivoCrianzaID = CultivoCrianza.ID')
            ->innerJoin('InformacionGeneral','InformacionGeneral.EspecieID = Especie.ID')
            ->innerJoin('UnidadOperativa','InformacionGeneral.UnidadOperativaID = UnidadOperativa.ID')
            ->innerJoin('UnidadEjecutora','UnidadOperativa.UnidadEjecutoraID = UnidadEjecutora.ID')
            ->innerJoin('Investigador','Investigador.ID = InformacionGeneral.InvestigadorID')
            ->innerJoin('EntidadParticipante','EntidadParticipante.InvestigadorID = Investigador.ID')
            ->where(['UnidadEjecutora.ID'=>$id,'EntidadParticipante.RazonSocial'=>'PNIA'])
            ->groupBy('Programa.Nombre,InformacionGeneral.Codigo,CultivoCrianza.Nombre,EntidadParticipante.AporteMonetario,EntidadParticipante.RazonSocial,EntidadParticipante.AporteNoMonetario')
            ->all();

        return $this->render('_programado_detalle',['estacion'=>$listadoPrograma,'programa'=>$programa]);
    }
    
    public function actionDetalleEventosExperimentos($id = null,$tipo=null){
        $this->layout='vacio';
        if($tipo==1)
        {
            $eventos_experimentos = (new \yii\db\Query())
            ->select('InformacionGeneral.Codigo,InformacionGeneral.TituloProyecto,sum(Actividad.Experimento) experimentos,sum(Actividad.Evento) eventos')
            ->from('InformacionGeneral')
            ->innerJoin('Proyecto','Proyecto.InvestigadorID=InformacionGeneral.InvestigadorID')
            ->innerJoin('Poa','Poa.ProyectoID=Proyecto.ID')
            ->innerJoin('Componente','Componente.PoaID=Poa.ID')
            ->innerJoin('Actividad','Actividad.ComponenteID=Componente.ID')
            ->innerJoin('UnidadOperativa','InformacionGeneral.UnidadOperativaID = UnidadOperativa.ID')
            ->innerJoin('UnidadEjecutora','UnidadOperativa.UnidadEjecutoraID = UnidadEjecutora.ID')
            ->where(['UnidadEjecutora.ID'=>$id])
            ->groupBy('InformacionGeneral.Codigo,InformacionGeneral.TituloProyecto')
            ->orderBy('InformacionGeneral.Codigo asc')
            ->all();
        }
        elseif($tipo==2)
        {
            $eventos_experimentos = (new \yii\db\Query())
            ->select('InformacionGeneral.Codigo,InformacionGeneral.TituloProyecto,sum(Actividad.Experimento) experimentos,sum(Actividad.Evento) eventos')
            ->from('InformacionGeneral')
            ->innerJoin('Proyecto','Proyecto.InvestigadorID=InformacionGeneral.InvestigadorID')
            ->innerJoin('Poa','Poa.ProyectoID=Proyecto.ID')
            ->innerJoin('Componente','Componente.PoaID=Poa.ID')
            ->innerJoin('Actividad','Actividad.ComponenteID=Componente.ID')
            ->innerJoin('UnidadOperativa','InformacionGeneral.UnidadOperativaID = UnidadOperativa.ID')
            ->innerJoin('UnidadEjecutora','UnidadOperativa.UnidadEjecutoraID = UnidadEjecutora.ID')
            ->groupBy('InformacionGeneral.Codigo,InformacionGeneral.TituloProyecto')
            ->orderBy('InformacionGeneral.Codigo asc')
            ->all();
        }
        
            
        return $this->render('_detalle_eventos_experimentos',['eventos_experimentos'=>$eventos_experimentos,'id'=>$id,'tipo'=>$tipo]);
    }
    
    public function actionDescargarDetalleEventosExperimentos($id = null,$tipo=null){
        $this->layout='vacio';
        if($tipo==1)
        {
            $eventos_experimentos = (new \yii\db\Query())
            ->select('InformacionGeneral.Codigo,InformacionGeneral.TituloProyecto,sum(Actividad.Experimento) experimentos,sum(Actividad.Evento) eventos')
            ->from('InformacionGeneral')
            ->innerJoin('Proyecto','Proyecto.InvestigadorID=InformacionGeneral.InvestigadorID')
            ->innerJoin('Poa','Poa.ProyectoID=Proyecto.ID')
            ->innerJoin('Componente','Componente.PoaID=Poa.ID')
            ->innerJoin('Actividad','Actividad.ComponenteID=Componente.ID')
            ->innerJoin('UnidadOperativa','InformacionGeneral.UnidadOperativaID = UnidadOperativa.ID')
            ->innerJoin('UnidadEjecutora','UnidadOperativa.UnidadEjecutoraID = UnidadEjecutora.ID')
            ->where(['UnidadEjecutora.ID'=>$id])
            ->groupBy('InformacionGeneral.Codigo,InformacionGeneral.TituloProyecto')
            ->orderBy('InformacionGeneral.Codigo asc')
            ->all();
        }
        elseif($tipo==2)
        {
            $eventos_experimentos = (new \yii\db\Query())
            ->select('InformacionGeneral.Codigo,InformacionGeneral.TituloProyecto,sum(Actividad.Experimento) experimentos,sum(Actividad.Evento) eventos')
            ->from('InformacionGeneral')
            ->innerJoin('Proyecto','Proyecto.InvestigadorID=InformacionGeneral.InvestigadorID')
            ->innerJoin('Poa','Poa.ProyectoID=Proyecto.ID')
            ->innerJoin('Componente','Componente.PoaID=Poa.ID')
            ->innerJoin('Actividad','Actividad.ComponenteID=Componente.ID')
            ->innerJoin('UnidadOperativa','InformacionGeneral.UnidadOperativaID = UnidadOperativa.ID')
            ->innerJoin('UnidadEjecutora','UnidadOperativa.UnidadEjecutoraID = UnidadEjecutora.ID')
            ->groupBy('InformacionGeneral.Codigo,InformacionGeneral.TituloProyecto')
            ->orderBy('InformacionGeneral.Codigo asc')
            ->all();
        }
        
            
        return $this->render('descargar-detalle-eventos-experimentos',['eventos_experimentos'=>$eventos_experimentos]);
    }

    public function actionEventosExperimentosGrafico(){
        $this->layout='estandar';
        $total = (new \yii\db\Query())
            ->select('sum(Actividad.Experimento) Experimento,sum(Actividad.Evento) Evento')
            ->from('Actividad')
            ->where('Actividad.Estado=1')
            ->innerJoin('Componente','Componente.ID=Actividad.ComponenteID')
            ->all();
        $estacion = (new \yii\db\Query())
            ->select('sum(Actividad.Experimento) Experimento,sum(Actividad.Evento) Evento, count(distinct InformacionGeneral.Codigo) CantProyectos,UnidadEjecutora.Nombre AS Estacion,UnidadEjecutora.ID ')
            ->from('Actividad')
            ->innerJoin('Componente','Componente.ID=Actividad.ComponenteID')
            ->innerJoin('Poa','Poa.ID = Componente.PoaID')
            ->innerJoin('Proyecto','Proyecto.ID = Poa.ProyectoID')
            ->innerJoin('Investigador','Investigador.ID = Proyecto.InvestigadorID')
            ->innerJoin('InformacionGeneral','InformacionGeneral.InvestigadorID = Investigador.ID')
            ->innerJoin('UnidadOperativa','InformacionGeneral.UnidadOperativaID = UnidadOperativa.ID')
            ->innerJoin('UnidadEjecutora','UnidadOperativa.UnidadEjecutoraID = UnidadEjecutora.ID')
            ->where('Actividad.Estado=1')
            ->groupBy('UnidadEjecutora.Nombre,UnidadEjecutora.ID')
            ->all();
        return $this->render('eventos-experimentos-grafico',['total'=>$total,'estacion'=>$estacion]);
    }
    
    public function actionAvanceProgramacionRecursos()
    {
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $recursos = Yii::$app->db->createCommand('
                                                 
                                                SELECT 
                                                        AA.Codigo,
                                                        AA.Nombre,
                                                        AA.Especifica,
                                                        AA.RubroElegibleID,
                                                        AA.Codificacion,
                                                        sum(AA.Total) Total,
                                                        sum(AA.Terminado) Terminado,
                                                        (sum(AA.Total)-Sum(AA.Terminado)) AS Falta 
                                                FROM (
                                                                SELECT 
                                                                        InformacionGeneral.Codigo,
                                                                        AreSubCategoria.Nombre,
                                                                        AreSubCategoria.Especifica,
                                                                        ActRubroElegible.RubroElegibleID,
                                                                        AreSubCategoria.Codificacion,
                                                                        sum(AreSubCategoria.MetaFisica) Total,
                                                                        (SELECT sum(MetaFisica) FROM CronogramaAreSubCategoria WHERE AreSubCategoriaID=AreSubCategoria.ID) Terminado
                                                                        
                                                                FROM [AreSubCategoria] 
                                                                INNER JOIN [ActRubroElegible] ON ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID 
                                                                INNER JOIN [Actividad] ON Actividad.ID=ActRubroElegible.ActividadID 
                                                                INNER JOIN [Componente] ON Componente.ID=Actividad.ComponenteID 
                                                                INNER JOIN [Poa] ON Poa.ID=Componente.PoaID INNER JOIN [Proyecto] ON Proyecto.ID=Poa.ProyectoID 
                                                                INNER JOIN [InformacionGeneral] ON InformacionGeneral.InvestigadorID=Proyecto.InvestigadorID 
                                                                
                                                                GROUP BY 
                                                                        InformacionGeneral.Codigo,
                                                                        AreSubCategoria.Nombre,
                                                                        AreSubCategoria.Especifica,
                                                                        ActRubroElegible.RubroElegibleID,
                                                                        AreSubCategoria.Codificacion,
                                                                        AreSubCategoria.ID
                                                                        
                                                                ) AA
                                                WHERE AA.Codigo in (select CodigoProyecto from UsuarioProyecto where UsuarioProyecto.UsuarioID='.$usuario->ID.')
                                                GROUP BY AA.Codigo,AA.RubroElegibleID,AA.Codificacion,AA.Nombre,
                                                        AA.Especifica
                                                ORDER BY 1 ASC,4 ASC , 5 asc
                                                 ')->queryAll();
        return $this->render('avance-programacion-recursos',['recursos'=>$recursos]);
    }
    
    public function actionAvanceProgramacionTareas()
    {
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $recursos = Yii::$app->db->createCommand('
                                                 
                                                SELECT 
                                                    AA.Codigo,
                                                    AA.Descripcion,
                                                    AA.Correlativo,
                                                    sum(AA.Total) Total,
                                                    sum(AA.Terminado) Terminado,
                                                    (sum(AA.Total)-Sum(AA.Terminado)) AS Falta 
                                                FROM (
                                                                SELECT 
                                                                    InformacionGeneral.Codigo,
                                                                    Tarea.Descripcion,
                                                                    Tarea.Correlativo,
                                                                    sum(Tarea.MetaFisica) Total,
                                                                    (SELECT sum(MetaFisica) FROM CronogramaTarea WHERE TareaID=Tarea.ID) Terminado
                                                                FROM [Tarea] 
                                                                INNER JOIN [Actividad] ON Actividad.ID=Tarea.ActividadID 
                                                                INNER JOIN [Componente] ON Componente.ID=Actividad.ComponenteID 
                                                                INNER JOIN [Poa] ON Poa.ID=Componente.PoaID INNER JOIN [Proyecto] ON Proyecto.ID=Poa.ProyectoID 
                                                                INNER JOIN [InformacionGeneral] ON InformacionGeneral.InvestigadorID=Proyecto.InvestigadorID 
                                                                GROUP BY 
                                                                        InformacionGeneral.Codigo,
                                                                        Tarea.Descripcion,
                                                                        Tarea.Correlativo,
                                                                        Tarea.ID
                                                                        
                                                                ) AA
                                                WHERE AA.Codigo in (select CodigoProyecto from UsuarioProyecto where UsuarioProyecto.UsuarioID='.$usuario->ID.')
                                                GROUP BY AA.Codigo,AA.Descripcion,AA.Correlativo
                                                ORDER BY 1 ASC,4 ASC , 5 asc
                                                 ')->queryAll();
        return $this->render('avance-programacion-tareas',['recursos'=>$recursos]);
    }

    public function actionTecnicoUafsi(){
        $this->layout='estandar';
        return $this->render('BI/tecnico-uafsi');
    }


    public function actionCompromisoPresupuestalReporte(){
        $this->layout='estandar';
        $usuario=Usuario::findOne(\Yii::$app->user->id);

        $seguimiento = Yii::$app->db->createCommand("
            SELECT InformacionGeneral.Codigo,
            ( SELECT count(c.CompromisoAnual) FROM CompromisoPresupuestal c 
            WHERE c.Codigo = InformacionGeneral.Codigo AND c.CompromisoAnual = 1) Anual,
            ( SELECT count(c.CompromisoAnual) FROM CompromisoPresupuestal c 
            WHERE c.Codigo = InformacionGeneral.Codigo AND c.CompromisoAdministrativo = 1) Admin,
            count(Requerimiento.CodigoProyecto) Req
            FROM InformacionGeneral
            LEFT JOIN Requerimiento ON Requerimiento.CodigoProyecto = InformacionGeneral.Codigo
            WHERE Requerimiento.Situacion = 3
            AND InformacionGeneral.Codigo IN (SELECT DISTINCT Usuario.username FROM InformacionGeneral
            INNER JOIN Investigador 
            ON InformacionGeneral.InvestigadorID=Investigador.ID 
            INNER JOIN Usuario ON Usuario.ID=Investigador.UsuarioID 
            INNER JOIN Proyecto ON Proyecto.InvestigadorID=Investigador.ID 
            INNER JOIN Poa ON Poa.ProyectoID=Proyecto.ID 
            INNER JOIN UsuarioProyecto 
            ON UsuarioProyecto.CodigoProyecto=InformacionGeneral.Codigo 
            WHERE UsuarioProyecto.UsuarioID=".$usuario->ID.")
            GROUP BY InformacionGeneral.Codigo")
        ->queryAll();

        return $this->render('otros/compromiso-presupuestal',['seguimiento'=>$seguimiento]);
    }

}

