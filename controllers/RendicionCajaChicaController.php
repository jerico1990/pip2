<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\PasoCritico;
use app\models\Usuario;
use app\models\Persona;
use app\models\Investigador;
use app\models\Proyecto;
use app\models\InformacionGeneral;
use app\models\Poa;
use app\models\Componente;
use app\models\Actividad;
use app\models\CronogramaProyecto;
use app\models\ActRubroElegible;
use app\models\CronogramaComponente;
use app\models\CronogramaActividad;
use app\models\CronogramaActRubroElegible;
use app\models\RubroElegible;
use app\models\AreSubCategoria;
use app\models\CronogramaAreSubCategoria;
use app\models\MarcoLogicoActividad;
use app\models\AreSubCategoriaObservacion;
use app\models\CertificacionPresupuestal;
use app\models\DetalleCertificacionPresupuestal;
use app\models\UnidadOperativa;
use app\models\SiafSecuencialProyecto;
use app\models\CajaChica;
use app\models\Orden;
use app\models\Siaf;
use app\models\Requerimiento;
use app\models\UnidadEjecutora;
use app\models\RendicionCajaChica;
use app\models\DetalleRendicionCajaChica;
use app\models\DetalleCajaChica;

use PhpOffice\PhpWord\PhpWord;
use yii\web\UploadedFile;


class RendicionCajaChicaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($CodigoProyecto=null)
    {
        $this->layout='estandar';
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        return $this->render('index',['CodigoProyecto'=>$CodigoProyecto]);
    }
    
    
    public function actionLista($CodigoProyecto=null){
        
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        
        $resultados = (new \yii\db\Query())
            ->select('RendicionCajaChica.*')
            ->from('RendicionCajaChica')
            ->where(['RendicionCajaChica.Estado'=>1,'RendicionCajaChica.CodigoProyecto'=>$CodigoProyecto])
            ->distinct()
            ->all();

        $nro=0;
        foreach($resultados as $result)
        {
            $nro++;
            if($result["Estado"]==1 )
            {
                echo "<tr>";
                echo "<td style='display:none'>" . $result["ID"] . "</td>";
                echo "<td>Rendición N° " . str_pad($result["Correlativo"], 3, "0", STR_PAD_LEFT)  ."-".$result["Annio"]."</td>";
                echo "<td>" . date('d/m/Y',strtotime($result["FechaRegistro"]))  . "</td>";
                echo "<td>" . $result["SolicitanteEncargo"] . "</td>";
                echo "<td>" . $result["Total"] . "</td>";
                echo "<td>" . $this->getSituacion($result["Situacion"]) . "</td>";
                echo "<td><a class='btn btn-default' target='_blank' href='rendicion-caja-chica/plantilla?ID=" . $result["ID"] . "'><span class='fa fa-cloud-download'></span> Descargar</a>  </td>";
                if($result["Situacion"]==3)
                {
                    echo "<td></td>";
                }
                else
                {
                    echo "<td><a href='#' class='btn-edit-rendicion-caja-chica' data-id='".$result["ID"]."'><i class='fa fa-edit fa-lg'></i></a><a href='#' class='btn-remove' data-id='".$result["ID"]."'><i class='fa fa-remove fa-lg'></i></a> </td>";    
                }
               echo "</tr>";
            }
        }
        
    }
    
   /* public function actionCrear($CodigoProyecto=null)
    {
        $this->layout='vacio';
        return $this->render('_form',['CodigoProyecto'=>$CodigoProyecto]);
    }*/

    public function actionCrear($CodigoProyecto=null,$RequerimientoID=null)
    {
        $this->layout='vacio';
        $rendicionCaja=new RendicionCajaChica;
        $cajaChica=DetalleCajaChica::find()->where('CodigoProyecto=:CodigoProyecto and Estado=1 and Situacion=3',[':CodigoProyecto'=>$CodigoProyecto])->one();
        
        
        
        if(!$cajaChica)
        {
            $cajaActual= new CajaChica;
            $rendicionCaja->Bienes=0;
            $rendicionCaja->Servicios=0;
        }
        else
        {
            $cajaActual=CajaChica::findOne($cajaChica->CajaChicaID);
            $rendicionCaja->Bienes=$cajaChica->SaldoActualBienes;
            $rendicionCaja->Servicios=$cajaChica->SaldoActualServicios;
        }
        if($rendicionCaja->load(Yii::$app->request->post())){
            $rendicionCajaAnt=RendicionCajaChica::find()->where('CodigoProyecto=:CodigoProyecto and Annio=:Annio and Situacion=1 and Estado=1',[':CodigoProyecto'=>$cajaChica->CodigoProyecto,':Annio'=>date('Y')])->one();
            if($rendicionCajaAnt)
            {
                \Yii::$app->getSession()->setFlash('error', '<div class="alert alert-dismissable alert-danger"><strong>Errores:</strong><ul> <li>Aún tiene un rendición de caja chica activa, debe finalizar para poder generar otra rendición.</li> </ul></div>');
                return $this->redirect(['/rendicion-caja-chica']);
            }
                
            $rendicionCaja->CajaChicaID=$cajaChica->ID;
            $rendicionCaja->Situacion=1;
            $rendicionCaja->Estado=1;
            $rendicionCaja->FechaRegistro=date('Ymd');
            $rendicionCaja->Annio=date('Y');
            $rendicionCaja->Correlativo=$this->CorrelativoRendicionCaja($CodigoProyecto);
            $rendicionCaja->save();
            
            if(!$rendicionCaja->NumerosDocumentos)
            {
                $countDetalles=0;
            }
            else
            {
                $countDetalles=count(array_filter($rendicionCaja->NumerosDocumentos));
            }
            $total=0;
            for($i=0;$i<$countDetalles;$i++)
            {
                $detalle=new DetalleRendicionCajaChica;
                $detalle->RendicionCajaChicaID=$rendicionCaja->ID;
                $detalle->FechaDocumento=$rendicionCaja->Fechas[$i];
                $detalle->ClaseDocumento=$rendicionCaja->Clases[$i];
                $detalle->TipoDocumento=$rendicionCaja->TiposDocumentos[$i];
                $detalle->Tipo=$rendicionCaja->Tipos[$i];
                $detalle->NumeroDocumento=$rendicionCaja->NumerosDocumentos[$i];
                $detalle->RUC=$rendicionCaja->Rucs[$i];
                $detalle->Proveedor=$rendicionCaja->Proveedores[$i];
                $detalle->DetalleGasto=$rendicionCaja->DetallesGastos[$i];
                $detalle->Importe=$rendicionCaja->Importes[$i];
                $detalle->save();
                
                
                $total=$total+$rendicionCaja->Importes[$i];
            }
            
            $cajaChica->SaldoActualBienes=$rendicionCaja->SaldoActualBienes;
            $cajaChica->SaldoActualServicios=$rendicionCaja->SaldoActualServicios;
            $cajaActual->SaldoActualBienes=$rendicionCaja->SaldoActualBienes;
            $cajaActual->SaldoActualServicios=$rendicionCaja->SaldoActualServicios;
            $cajaActual->update();
            
            $cajaChica->update();
            $rendicionCaja->Total=$total;
            $rendicionCaja->update();
            return $this->redirect(['/rendicion-caja-chica']);
        }

        return $this->render('_form',['CodigoProyecto'=>$CodigoProyecto,'cajaChica'=>$cajaChica,'rendicionCaja'=>$rendicionCaja,'cajaActual'=>$cajaActual]);
    }

    public function actionActualizar($ID=null)
    {
        $this->layout='vacio';
        $rendicionCaja=RendicionCajaChica::findOne($ID);
        $cajaChica=DetalleCajaChica::find()->where('CodigoProyecto=:CodigoProyecto and Estado=1 and Situacion=3',[':CodigoProyecto'=>$rendicionCaja->CodigoProyecto])->one();
        $cajaActual=CajaChica::findOne($cajaChica->CajaChicaID);
        $detalles=DetalleRendicionCajaChica::find()->where('RendicionCajaChicaID=:RendicionCajaChicaID',[':RendicionCajaChicaID'=>$ID])->all();
        if($rendicionCaja->load(Yii::$app->request->post())){
            
            $rendicionCaja->save();
            if(!$rendicionCaja->NumerosDocumentos)
            {
                $countDetalles=0;
            }
            else
            {
                $countDetalles=count(array_filter($rendicionCaja->NumerosDocumentos));
            }
            $total=0;
            for($i=0;$i<$countDetalles;$i++)
            {
                if(isset($rendicionCaja->IDs[$i]))
                {
                    $detalle=DetalleRendicionCajaChica::findOne($rendicionCaja->IDs[$i]);
                    $detalle->RendicionCajaChicaID=$rendicionCaja->ID;
                    $detalle->FechaDocumento=$rendicionCaja->Fechas[$i];
                    $detalle->TipoDocumento=$rendicionCaja->TiposDocumentos[$i];
                    $detalle->Tipo=$rendicionCaja->Tipos[$i];
                    $detalle->ClaseDocumento=$rendicionCaja->Clases[$i];
                    $detalle->NumeroDocumento=$rendicionCaja->NumerosDocumentos[$i];
                    $detalle->RUC=$rendicionCaja->Rucs[$i];
                    $detalle->Proveedor=$rendicionCaja->Proveedores[$i];
                    $detalle->DetalleGasto=$rendicionCaja->DetallesGastos[$i];
                    $detalle->Importe=$rendicionCaja->Importes[$i];
                    $detalle->save();
                }
                else{
                    $detalle=new DetalleRendicionCajaChica;
                    $detalle->RendicionCajaChicaID=$rendicionCaja->ID;
                    $detalle->FechaDocumento=$rendicionCaja->Fechas[$i];
                    $detalle->ClaseDocumento=$rendicionCaja->Clases[$i];
                    $detalle->TipoDocumento=$rendicionCaja->TiposDocumentos[$i];
                    $detalle->Tipo=$rendicionCaja->Tipos[$i];
                    $detalle->NumeroDocumento=$rendicionCaja->NumerosDocumentos[$i];
                    $detalle->RUC=$rendicionCaja->Rucs[$i];
                    $detalle->Proveedor=$rendicionCaja->Proveedores[$i];
                    $detalle->DetalleGasto=$rendicionCaja->DetallesGastos[$i];
                    $detalle->Importe=$rendicionCaja->Importes[$i];
                    $detalle->save();
                }
                $total=$total+$rendicionCaja->Importes[$i];
            }
            $cajaChica->SaldoActualBienes=$rendicionCaja->SaldoActualBienes;
            $cajaChica->SaldoActualServicios=$rendicionCaja->SaldoActualServicios;
            $cajaActual->SaldoActualBienes=$rendicionCaja->SaldoActualBienes;
            $cajaActual->SaldoActualServicios=$rendicionCaja->SaldoActualServicios;
            $cajaActual->update();
            
            $cajaChica->update();
            $rendicionCaja->Total=$total;
            if($rendicionCaja->Y01==2)
            {
                $rendicionCaja->Situacion=3;
            }
            $rendicionCaja->update();
            return $this->redirect(['/rendicion-caja-chica']);
        }
        return $this->render('_form_actualizar',['rendicionCaja'=>$rendicionCaja,'ID'=>$ID,'detalles'=>$detalles,'cajaChica'=>$cajaChica,'cajaActual'=>$cajaActual]);
    }

    public function actionEliminar($id=null)
    {
        $rendicioncajaChica=RendicionCajaChica::findOne($id);
        $rendicioncajaChica->Estado=0; 
        $rendicioncajaChica->update();

        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
    public function actionEliminarDetalle()
    {
        if(isset($_POST["id"]) && $_POST["id"]!='')
        {
            $id=$_POST["id"];
            $rendicioncajaChica=DetalleRendicionCajaChica::findOne($id);
            $rendicioncajaChica->delete();
            
            $arr = array('Success' => true);
            echo json_encode($arr);    
        }
        
    }

    public function actionEnviar($ID=null)
    {
        $requerimiento=CajaChica::findOne($_POST["ID"]);
        
        $requerimiento->Situacion=2;
        $requerimiento->update();

        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
     public function actionActualizarRequerimientoListado()
    {
        $this->layout='vacio';
        $requerimiento=CajaChica::findOne($_POST['ID']);
        $requerimiento->Situacion =6;
        $requerimiento->update();
        $arr = array('Success' => true);
        echo json_encode($arr);
    }

    public function CorrelativoRendicionCaja($CodigoProyecto)
    {
        $rendicion=RendicionCajaChica::find()->where('CodigoProyecto=:CodigoProyecto',[':CodigoProyecto'=>$CodigoProyecto])->orderBy('Correlativo desc')->one();
        if($rendicion)
        {
            return $rendicion->Correlativo+1;
        }
        else
        {
            return 1;
        }
    }

 public function actionExcel($CodigoProyecto=null)
    {
        $resultados = (new \yii\db\Query())
            ->select('CajaChica.FechaRegistro')
            ->from('CajaChica')
            ->where(['CajaChica.CodigoProyecto'=>$CodigoProyecto])
            ->distinct()
            ->all();
        $requerimiento=new CajaChica;
        return $this->render('excel',['resultados'=>$resultados,'requerimiento'=>$requerimiento]);
    }




     public function getSituacion($situacion=null)
    {
        if($situacion==1){
            return "Pendiente";
        }
        elseif($situacion==2)
        {
            return "En Proceso";
        }
         elseif($situacion==3)
        {
            return "Aprobado";
        }
        else if($situacion==4)
        {
            return "Observado";
        }
        else if($situacion==5)
        {
            return "Cierre";
        }
        else if($situacion==6)
        {
            return "Anulado";
        }
    }

    public function actionPlantilla($ID=null)
    {
        $rendicionCaja=RendicionCajaChica::findOne($ID);
        $informacion=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$rendicionCaja->CodigoProyecto])->one();
        $eea=UnidadOperativa::find()->where('ID=:ID',[':ID'=>$informacion->UnidadOperativaID])->one();
        $ejecutora=UnidadEjecutora::find()->where('ID=:ID',[':ID'=>$eea->UnidadEjecutoraID])->one();
        $usuario=Usuario::find()->where('username=:username',[':username'=>$rendicionCaja->CodigoProyecto])->one();
        $persona=Persona::find()->where('ID=:ID',[':ID'=>$usuario->PersonaID])->one();
        $caja=DetalleCajaChica::findOne($rendicionCaja->CajaChicaID);
        $countDetalles=DetalleRendicionCajaChica::find()
                    ->Select('DetalleRendicionCajaChica.*')
                    ->where('RendicionCajaChicaID=:RendicionCajaChicaID',[':RendicionCajaChicaID'=>$ID])
                    ->count();
        $detalles=DetalleRendicionCajaChica::find()
                    ->Select('DetalleRendicionCajaChica.*')
                    ->where('RendicionCajaChicaID=:RendicionCajaChicaID',[':RendicionCajaChicaID'=>$ID])
                    ->all();
        require_once(Yii::$app->basePath . '/web/PHPWord/src/PhpWord/Autoloader.php');     
        \PhpOffice\PhpWord\Autoloader::register();
        $phpWord = new PhpWord();
       
        $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/documentos/PLANTILLA_RENDICION_CAJA_CHICA.docx');
        //$template->setValue('ID', str_pad($orden->Correlativo, 3, "0", STR_PAD_LEFT)."-".$orden->Annio);
        $template->setValue('EEA',$eea->Nombre);
        //$template->setValue('FECHAORDEN', date('d/m/Y',strtotime($orden->FechaOrden)));
        $template->setValue('CORRELATIVO',  str_pad($rendicionCaja->Correlativo, 3, "0", STR_PAD_LEFT)  ."-".$rendicionCaja->Annio );
        $template->setValue('CODIGOPROYECTO', $rendicionCaja->CodigoProyecto);
        $template->setValue('IRP', $persona->Nombre.' '.$persona->ApellidoPaterno.' '.$persona->ApellidoMaterno);
        $template->setValue('CARGO', $rendicionCaja->Cargo);
        $template->setValue('FECHA', date('d-m-Y',strtotime($rendicionCaja->FechaRegistro)));
        $template->cloneRow('F', $countDetalles);
        $countDetalles=1;
        $total=0;
        foreach($detalles as $detalle)
        {
            $template->setValue('F#'.$countDetalles.'', "".date('d-m-Y',strtotime($detalle->FechaDocumento))."");
            $template->setValue('C#'.$countDetalles.'', "".$detalle->ClaseDocumento."");
            $template->setValue('N#'.$countDetalles.'', "".$detalle->NumeroDocumento."");
            $template->setValue('P#'.$countDetalles.'', "".$detalle->Proveedor."");
            $template->setValue('D#'.$countDetalles.'', "".$detalle->DetalleGasto."");
            $template->setValue('I#'.$countDetalles.'', "".number_format($detalle->Importe, 2, '.', ' ')."");
            $total=$total+$detalle->Importe;
            //$template->setValue('DIRECTIVONOMBRE#'.$countdirectivos.'', "".ucwords(strtolower($registroparametros->getNombresWord($directivo->persona_id)))."");
            $countDetalles++;
            
        }
        $template->setValue('TOTAL', number_format($total, 2, '.', ' '));
        
        
        
        $movimientosaldoAnterior=$caja->SaldoAnteriorBienes+$caja->SaldoAnteriorServicios;
        $movimientohabilitacionFondo=$total;
        $movimientototal=$caja->Bienes+$caja->Servicios;
        $movimientoimporterendicion=$total;
        $movimientosaldoactual=$caja->SaldoActualBienes+$caja->SaldoActualServicios;
        
        $template->setValue('SALDOANT', $movimientosaldoAnterior);
        $template->setValue('HABILFON', $movimientohabilitacionFondo);
        $template->setValue('TOTALC', $movimientototal);
        $template->setValue('IMPORTER', $movimientoimporterendicion);
        $template->setValue('SALDO', $movimientosaldoactual);
            
       
        
        
        
        $temp_file = tempnam(sys_get_temp_dir(), 'PHPWord');
        $template->saveAs($temp_file);
        header("Content-Disposition: attachment; filename='RendicionCajaChica.docx'");
        readfile($temp_file); // or echo file_get_contents($temp_file);
        unlink($temp_file);  // remove temp file
        return true;    
    }
    
    public function actionAdjuntarDocumento($ID=null){
        $this->layout='vacio';
        $cajaChica=CajaChica::findOne($ID);
        $Requerimiento=Requerimiento::findOne($cajaChica->RequerimientoID);
        if($cajaChica->load(Yii::$app->request->post())){
            
            $cajaChica->archivo = UploadedFile::getInstance($cajaChica, 'archivo');
            if($cajaChica->archivo)
            {
                $cajaChica->archivo->saveAs('ordenServicioFactura/' . $cajaChica->ID . '.' . $cajaChica->archivo->extension);
                $cajaChica->Documento=$cajaChica->ID . '.' . $cajaChica->archivo->extension;
            }
            $cajaChica->update();
            return $this->redirect(['requerimiento/generar','ID'=>$Requerimiento->ID]);
        }
        return $this->render('adjuntar-documento',['ID'=>$ID,'cajaChica'=>$cajaChica]);
    }
    public function actionEliminarAdjunto($id=null)
    {
        $cajaChica=CajaChica::findOne($id);
        $cajaChica->Documento='';
        $cajaChica->update();
       
        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
    public function actionApertura($CodigoProyecto=null)
    {
        $this->layout='estandar';
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        return $this->render('apertura',['CodigoProyecto'=>$CodigoProyecto]);
    }
    
    /*
    public function actionListaApertura($CodigoProyecto=null){
        
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        
        $resultados = (new \yii\db\Query())
            ->select('AperturaCajaChica.*')
            ->from('AperturaCajaChica')
            ->where(['AperturaCajaChica.CodigoProyecto'=>$CodigoProyecto])
            ->distinct()
            ->all();

        $nro=0;
        foreach($resultados as $result)
        {
            $nro++;
            if($result["Estado"]==1 )
            {
            echo "<tr>";
            echo "<td style='display:none'>" . $result["ID"] . "</td>";
            echo "<td>" . $result["Correlativo"] . "</td>";
            echo "<td>" . date('d/m/Y',strtotime($result["FechaRegistro"]))  . "</td>";
            echo "<td>" . $result["Descripcion"] . "</td>";
            echo "<td>" . $result["Bienes"] . "</td>";
            echo "<td>" . $result["Servicios"] . "</td>";
            echo "<td>" .( $result["Bienes"] + $result["Servicios"]). "</td>";
            echo "<td>" . $this->getSituacion($result["Situacion"]) . "</td>";
            echo "<td><a target='_blank' href='cajachica/R" . $result["ResolucionDirectorial"] . "'><span class='fa fa-cloud-download'></span></a></td>";
            echo "</tr>";
            }
        }
    }
    
    
    public function actionCrearApertura($CodigoProyecto=null,$RequerimientoID=null)
    {
        $this->layout='vacio';
        //$terminosReferencias=TerminoReferencia::find()->where('CodigoProyecto=:CodigoProyecto and Situacion=0',[':CodigoProyecto'=>$CodigoProyecto])->all();
        $aperturaCaja=new AperturaCajaChica;
        $requerimiento=Requerimiento::findOne($RequerimientoID);
        if($aperturaCaja->load(Yii::$app->request->post())){
            
            $requerimientoCaja->Situacion=1;
            $requerimientoCaja->Estado=1;
            $requerimientoCaja->FechaRegistro=date('Ymd');
            $requerimientoCaja->Correlativo=$this->CorrelativoRequerimiento($CodigoProyecto);
          


            $requerimientoCaja->save();
            $requerimientoCaja->archivo = UploadedFile::getInstance($requerimientoCaja, 'archivo');
            
            if($requerimientoCaja->archivo)
            {
                $requerimientoCaja->archivo->saveAs('cajachica/R' . $requerimientoCaja->ID . '.' . $requerimientoCaja->archivo->extension);
                $requerimientoCaja->ResolucionDirectorial='R'.$requerimientoCaja->ID . '.' . $requerimientoCaja->archivo->extension;
            }
            $requerimientoCaja->update();
            
           
            
            return $this->redirect(['requerimiento/generar','ID'=>$RequerimientoID]);
        }

        return $this->render('_form_apertura',['CodigoProyecto'=>$CodigoProyecto,'requerimiento'=>$requerimiento,'RequerimientoID'=>$RequerimientoID]);
    }

    public function actionActualizarApertura($ID=null)
    {
        $this->layout='vacio';
        $requerimiento=CajaChica::findOne($ID);

        if($requerimiento->load(Yii::$app->request->post())){

            $requerimiento->save();
            $requerimiento->archivo = UploadedFile::getInstance($requerimiento, 'archivo');

            if($requerimiento->archivo)
            {
                $requerimiento->archivo->saveAs('cajachica/R' . $requerimiento->ID . '.' . $requerimiento->archivo->extension);
                $requerimiento->ResolucionDirectorial='R'.$requerimiento->ID . '.' . $requerimiento->archivo->extension;
            }
            $requerimiento->update();
            return $this->redirect(['requerimiento/generar','ID'=>$RequerimientoID]);
        }
        return $this->render('_form_actualizar_apertura',['requerimiento'=>$requerimiento,'ID'=>$ID]);
    }

    public function actionEliminarApertura($id=null)
    {
        $cajaChica=AperturaCajaChica::findOne($id);
        $cajaChica->Estado=0; 
        $cajaChica->update();

        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    */
    public function getClases($clase)
    {
        if($clase==1)
        {
            return "FACTURA";
        }
        elseif($clase==2)
        {
            return "RECIBO POR HONORARIOS";
        }
        elseif($clase==3)
        {
            return "BOLETA DE VENTA";
        }
        elseif($clase==4)
        {
            return "NOTA DE CREDITO";
        }
        elseif($clase==5)
        {
            return "NOTA DE DEBITO";
        }
        elseif($clase==6)
        {
            return "BOLETO DE VIAJE INTERPROVINCIAL";
        }
        elseif($clase==7)
        {
            return "VOUCHER";
        }
        elseif($clase==8)
        {
            return "TICKET";
        }
        elseif($clase==9)
        {
            return "COMPROBANTE DE GASTO";
        }
        elseif($clase==10)
        {
            return "DECLARACION JURADA";
        }
    }
    
    public function actionVerificar($CodigoProyecto=null)
    {
        $rendicionCajaChica=RendicionCajaChica::find()->where('CodigoProyecto=:CodigoProyecto and Estado=1 and Situacion=1',[':CodigoProyecto'=>$CodigoProyecto])->one();
        if($rendicionCajaChica)
        {
            $arr = array('Success' => true);    
        }
        else
        {
            $arr = array('Success' => false);
        }
        
        echo json_encode($arr);
    }
}
