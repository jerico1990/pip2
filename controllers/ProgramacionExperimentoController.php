<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Usuario;
use app\models\Persona;
use app\models\Investigador;
use app\models\Proyecto;
use app\models\Componente;
use app\models\Actividad;
use app\models\Poa;
use app\models\InformacionGeneral;
use app\models\CronogramaProyecto;
use app\models\ActRubroElegible;
use app\models\CronogramaComponente;
use app\models\CronogramaActividad;
use app\models\CronogramaActRubroElegible;
use app\models\RubroElegible;
use app\models\ProgramacionExperimento;
use app\models\CronogramaAreSubCategoria;
use app\models\MarcoLogicoActividad;
use yii\web\UploadedFile;
use app\models\PasoCritico;
use app\models\CronogramaExperimento;

use app\models\Tarea;


class ProgramacionExperimentoController extends Controller
{
    /**
     * @inheritdoc
     */
    
    /*public $investigador = NULL;
    public function init(){
        // parent::__construct();
        $usuario            = Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador  = Persona::findOne($usuario->PersonaID);
        $investigador       = Investigador::find()->where(['UsuarioID'=>$usuario->id])->one();
        $this->investigador = $investigador->ID;
    }*/

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex($investigadorID=null)
    {
        $this->layout='estandar';
        if($investigadorID==NULL)
        {
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        }
        else
        {
            $investigador=Investigador::findOne($investigadorID);
            $usuario=Usuario::findOne($investigador->UsuarioID);
        }
        
        if(!$investigadorID && \Yii::$app->user->identity->rol!=7)
        {
            return $this->redirect(['panel/index']) ;
        }
        
        $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID and Estado=1',[':ProyectoID'=>$proyecto->ID])->one();
        $componentes=Componente::find()->where('PoaID=:PoaID',[':PoaID'=>$poa->ID])->all();
        return $this->render('index',['componentes'=>$componentes,'usuario'=>$usuario,'poa'=>$poa,'investigadorID'=>$investigador->ID,'informacionGeneral'=>$informacionGeneral]);
    }


    public function actionRecursosForm(){
        $this->layout='vacio';
        
        return $this->render('recursos/_recursos_form');
    }

    function microtime_float() {
        list($useg, $seg) = explode(" ", microtime());
        return ((float)$useg + (float)$seg);
    }

    /**
     * Rescursos JSON 
     * @return [type] [description]
     */
    public function actionExperimentosJson($codigo=null){
        $this->layout='vacio';
        // Proyectos
        $usuario=Usuario::find()->where('username=:username',[':username'=>$codigo])->one();
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        
        //$InformacionGeneral=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$codigo])->one();
        $tiempo_inicio = $this->microtime_float();
        $proyecto = Poa::find()
            ->select('Poa.ID, Poa.ProyectoID')
            ->innerJoin('Proyecto','Poa.ProyectoID = Proyecto.ID')
            ->where(['Proyecto.InvestigadorID'=> $investigador->ID,'Poa.Estado' => 1])
            ->one();
        $proyectox = array(
            'ID'             => $proyecto->ProyectoID, 
            'Objetivos'      => $this->get_objetivos($proyecto->ID),
            'Cronogramas'    => $this->getCronogramaProyecto($proyecto->ProyectoID,'proyecto')
        );

        $tiempo_fin = $this->microtime_float();
        $tiempo = bcsub($tiempo_fin, $tiempo_inicio, 4);

        if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start();
        array_push($proyectox, $tiempo);
        echo json_encode($proyectox);
    }
    private function get_objetivos($ProyectoID){
        $ObjetivoJson = [];
        $objetivos = Componente::find()
                ->select('Componente.ID,Componente.Nombre,Componente.Correlativo')
                ->where(['Componente.Estado' => 1, 'Componente.PoaID' => $ProyectoID])
                ->groupBy('Componente.ID,Componente.Nombre,Componente.Correlativo')
                ->orderBy('Componente.Correlativo asc')
                ->all();
        if(!empty($objetivos) ){
            foreach ($objetivos as $objetivo) {
                
                $componente = Componente::find()
                ->select('sum(AreSubCategoria.MetaFisica*AreSubCategoria.CostoUnitario) TotalObjetivo')
                ->innerJoin('Actividad','Actividad.ComponenteID=Componente.ID')
                ->innerJoin('ActRubroElegible','ActRubroElegible.ActividadID=Actividad.ID')
                ->innerJoin('AreSubCategoria','AreSubCategoria.ActRubroElegibleID=ActRubroElegible.ID')
                ->where(['Componente.ID' => $objetivo->ID,'Actividad.Estado'=>1,'Componente.Estado'=>1])
                ->one();
                
                $ObjetivoJson[] = array(
                    'ID'            => $objetivo->ID, 
                    'Nombre'        => $objetivo->Nombre,
                    'TotalObjetivo' => $componente->TotalObjetivo,
                    'Correlativo'   => $objetivo->Correlativo,
                    'Actividades'   => $this->get_actividades($objetivo->ID), 
                );

            }
        }
        return $ObjetivoJson;
    }

    private function get_actividades($ObjetivoID){
        $ActividadesJson = [];
        $actividades = Actividad::find()
                ->where(['Estado' => 1, 'ComponenteID' => $ObjetivoID])
                ->orderBy('Correlativo asc')
                ->all();
        if(!empty($actividades) && $actividades){
            foreach ($actividades as $actividad) {
                $ActividadesJson[] = array(
                    'ID'            => $actividad->ID, 
                    'ObjetivoID'    => $actividad->ComponenteID, 
                    'Nombre'        => $actividad->Nombre, 
                    'UnidadMedida'  => $this->get_actividadesmarco($actividad->ID)->IndicadorUnidadMedida, 
                    'CostoUnitario' => $actividad->CostoUnitario, 
                    'MetaFisica'    => $this->get_actividadesmarco($actividad->ID)->IndicadorMetaFisica,
                    'Experimentos'  => $this->get_experimentos($actividad->ID),
                );
            }
        }
        else
        {
                $ActividadesJson[] = array(
                    'ID'            => '', 
                    'ObjetivoID'  => '', 
                    'Nombre'        => '', 
                    'UnidadMedida'  => '', 
                    'CostoUnitario' => '', 
                    'MetaFisica'    => '',
                    'Experimentos' => '',
                );
        }
        return $ActividadesJson;
    }

    private function get_experimentos($ActividadID){
        $ExperimentosJson = [];
        $experimentos = ProgramacionExperimento::find()
                ->where(['ActividadID' => $ActividadID])
                ->all();
        if(!empty($experimentos) ){
            foreach ($experimentos as $experimento) {
                $ExperimentosJson[] = array(
                    'ID'                => $experimento->ID, 
                    'ActividadID'       => $experimento->ActividadID,
                    'Nombre'            => $experimento->Nombre,  
                    'MetaFisica'        => $experimento->MetaFisica, 
                    'Cronogramas'       => $this->getCronogramaProyecto($experimento->ID,'experimento')
                );
            }
        }
        return $ExperimentosJson;
    }

    private function get_actividadesmarco($idActividad){
        $jsonActividadesMarco = array();
        $actvmarco = MarcoLogicoActividad::find()
                ->Select('IndicadorUnidadMedida,sum(CAST(IndicadorMetaFisica AS FLOAT)) IndicadorMetaFisica')
                ->where(['Estado' => 1, 'ActividadID' => $idActividad])
                ->groupBy('IndicadorUnidadMedida')
                ->one();
        if(!$actvmarco)
        {
            $actvmarco=new MarcoLogicoActividad;
        }
        return $actvmarco;
    }

    private function getCronogramaProyecto($id,$tipo = ''){
        $this->layout='vacio';
        $meses = array();
        $cronograma = array();
        $time_start = microtime(true);
        switch ($tipo) {
            case 'proyecto':
                
                $indicador = 'ProyectoID';
                $meses=Yii::$app->db->createCommand('
                        SELECT ID,'.$indicador.',Mes,MetaFisica,MesDescripcion,Situacion
                        FROM CronogramaProyecto
                        WHERE ProyectoID='.$id.'
                        Order by Mes asc
                        ')
                        ->queryAll();
            break;
            case 'componente':
                $meses = CronogramaComponente::find()
                        ->where(['ComponenteID'=> $id])
                        ->Orderby('Mes asc')
                        ->all();
                $indicador = 'ComponenteID';
            break;

            case 'actividad':
                $meses = CronogramaActividad::find()
                        ->where(['ActividadID'=> $id])
                        ->Orderby('Mes asc')
                        ->all();
                $indicador = 'ActividadID';
            break;

            case 'experimento':
                $meses = CronogramaExperimento::find()
                        ->where(['ProgramacionExperimentoID'=> $id])
                        ->Orderby('Mes asc')
                        ->all();
                $indicador = 'ProgramacionExperimentoID';
            break;

            case 'areSubCategoria':
                $indicador = 'AreSubCategoriaID';
                $meses=Yii::$app->db->createCommand('
                        SELECT ID,'.$indicador.',Mes,MetaFisica,MesDescripcion,Situacion
                        FROM CronogramaAreSubCategoria
                        WHERE AreSubCategoriaID='.$id.'
                        Order by Mes asc
                        ')
                        ->queryAll();
                
            break;
        }
        $time_end = microtime(true);
        $foreach_time = $time_end - $time_start;
         
        if(!empty($meses)){
            foreach ($meses as $mes) {
                $cronograma[] = array(
                    'ID'                    => $mes['ID'],
                    $indicador              => $mes[''.$indicador.''],
                    'Mes'                   => $mes['Mes'],
                    'MetaFisica'            => $mes['MetaFisica'],
                    'Situacion'             => $mes['Situacion'],
                    'MesDescripcion'        => $this->DescripcionMes($mes['Mes']),
                    'Tiempo'                => number_format($foreach_time * 1000, 3) . "ms\n"
                    );
            }
        }else{
            $cronograma = array(
                'ID'                    => '',
                $indicador              => '',
                'Mes'                   => '',
                'Situacion'             => '',
                'MetaFisica'            => '',
                'MesDescripcion'        => '',
                'Tiempo'                => '',
            );
        }

        return $cronograma;
    }

    
    public function DescripcionMes($mes=null)
    {
        setlocale (LC_TIME,"spanish");
        //$mes = 01;
        $mes=$mes-1;
        $fechaInicio = '2016-05-01';
        $nuevafecha = strtotime ( '+'.$mes.' month' , strtotime ( $fechaInicio ) ) ;
        $nuevafecha = strtoupper(strftime ( '%b %Y' , $nuevafecha ) ) ;
        return $nuevafecha;
    }
    
    public function actionCrearExperimento($CodigoProyecto=null,$ActividadID=null)
    {
        $this->layout='vacio';
        
        $model=new ProgramacionExperimento;
        if($model->load(Yii::$app->request->post()))
        {
            $model->CodigoProyecto=$CodigoProyecto;
            $model->ActividadID=$ActividadID;
            $model->Estado=1;
            $model->Situacion=1;
            $model->FechaRegistro=date('Ymd h:m:s');
            $model->save();
            $arr = array('Success' => true);
            echo json_encode($arr);
            die;
        }
        
        return $this->render('_experimentos_form',['ActividadID'=>$ActividadID,'CodigoProyecto'=>$CodigoProyecto]);
    }
    
    public function actionEditarExperimento($ID=null)
    {
        $this->layout='vacio';
        $model=ProgramacionExperimento::findOne($ID);
        if($model->load(Yii::$app->request->post()))
        {
            $model->update();
            $arr = array('Success' => true);
            echo json_encode($arr);
            die;
        }
        return $this->render('_experimentos_actualizar_form',['model'=>$model]);
    }
}
