<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\LoginIntegraForm;
use app\models\ContactForm;

class LoginController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['integra-login'],
                        'allow' => true,
                        'verbs' => ['POST']
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout='login';

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            return $this->redirect(['panel/index']);
        }
        return $this->render('index',['model'=>$model]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        $session = Yii::$app->session;
        $ses = $session->get('SysIntegra');
        if(isset($ses) == 1){
          return $this->redirect($session->get('Url').'?x=2');
        }else{
          return $this->goHome();
        }


    }

    /**
     * Displays contact page.
     *
     * @return string
     */

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * IntegraLogin Action.
     *
     * @return string
     */

    public function beforeAction($action) {
        if($action->id = 'integra-login') {
            Yii::$app->request->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionByPass(){

        $model = new LoginForm();
        $session = Yii::$app->session;
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $session->set('SysIntegra', '1');
            return $this->goBack();
        }else{
          echo 'Ocurrio un error comuniquese con el administrador';
        }
    }

    public function actionIntegraLogin()
    {
        $this->layout='vacio';
        $session = Yii::$app->session;
        $token = $_POST['token'];
        $objToken = new Token($token);
        if ($objToken->DesEncripta() == '') {
          if ($objToken->LeerKey() == '123456789012345') {
            $session->set('Url', $objToken->LeerUrl());
            return $this->render('nuev',['usuario'=>$objToken->LeerUsuario(),'password'=>$objToken->LeerClave(),'url'=>$objToken->LeerUrl()]);
          }else {
              return $this->redirect($objToken->LeerUrl().'?x=2');
          }
        }else {
            return $this->redirect($objToken->LeerUrl().'?x=1');
        }
    }
}

class Token
{
    private $_token;
    private $_key;
    private $_usuario;
    private $_clave;
    private $_url;
    private $_tabla = array('!','"','#','$','%','&','\'','(',')','*','+',',','-','.','/','0','1','2','3','4','5','6','7','8','9',':',';','<','=','>','?','@','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','[','\\',']','^','_','`','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','{','|','}', '~');
    private $_tabla2 = array("1", "0", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "D", "E", "F", "G", "H", "I", "z", "2", "3", "4", "5", "6", "7", "8", "9", ":", ";", "<", "=", "@", "A", "B", "L", "M", ">", "?", "N", "O", "P", "Q", "R", "C", "J", "K", "!", "\"", "#", "$", "%", "&", "'", "(", ")", ".", "/", "{", "|", "}", "S", "T", "U", "V", "W", "*", "+", ",", "-", "X", "\\", "Y", "d", "e", "Z", "[", "j", "]", "^", "_", "`", "a",  "f", "g", "b", "c", "h","i","~");

    function __construct() {
      //obtengo un array con los parámetros enviados a la función
  		$params = func_get_args();
  		//saco el número de parámetros que estoy recibiendo
  		$num_params = func_num_args();
  		//cada constructor de un número dado de parámtros tendrá un nombre de función
  		//atendiendo al siguiente modelo __construct1() __construct2()...
  		$funcion_constructor ='__construct'.$num_params;
  		//compruebo si hay un constructor con ese número de parámetros
  		if (method_exists($this,$funcion_constructor)) {
  			//si existía esa función, la invoco, reenviando los parámetros que recibí en el constructor original
  			call_user_func_array(array($this,$funcion_constructor),$params);
  		}
    }

    private function __construct1($token)
    {
        $this->_token = $token;
    }

    private function __construct4($usuario, $clave, $url, $key)
    {
        $this->_usuario = $usuario;
        $this->_clave = $clave;
        $this->_url = $url;
        $this->_key = $key;
    }

    public function LeerUsuario() {
        return $this->_usuario;
    }
    public function LeerClave()
    {
        return $this->_clave;
    }
    public function LeerUrl()
    {
        return $this->_url;
    }
    public function LeerToken()
    {
        return $this->_token;
    }
    public function LeerKey()
    {
        return $this->_key;
    }

    public function DesEncripta(){
      $sMensage = '';

      //Validacion
      if (!isset($this->_token))
      {
          $sMensage = "El token no se ha definido";
          return $sMensage;
      }
      /*if (!isset($this->_key))
      {
          $sMensage = "El key no ha sido definido";
          return $sMensage;
      }*/

      //Procesamos
      $strCadena = '';//$this->_usuario + "*" + $this->_clave + "*" + $this->_url;

      for ($i=0; $i < mb_strlen($this->_token); $i++) {
          $strletra = substr($this->_token, $i, 1);
          $iascci = 0;
          $iascci = array_search($strletra, $this->_tabla2);
          $strCadena = $strCadena . $this->_tabla[$iascci];
      }
      $this->_usuario = explode('*', $strCadena)[0];
      $this->_clave = explode('*', $strCadena)[1];
      $this->_url = explode('*', $strCadena)[2];
      $this->_key = explode('*', $strCadena)[3];

      return $sMensage;
    }
}
