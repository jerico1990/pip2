<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Usuario;
use app\models\Persona;
use app\models\Investigador;
use app\models\Proyecto;
use app\models\Componente;
use app\models\Poa;

class TareaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        $this->layout='estandar';
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID and Estado=1',[':ProyectoID'=>$proyecto->ID])->one();
        $componentes=Componente::find()->where('PoaID=:PoaID',[':PoaID'=>$poa->ID])->all();
        return $this->render('index',['componentes'=>$componentes]);

        return $this->render('index');
    }
    
    public function actionCarga()
        {
            ini_set('memory_limit', '-1');
            $this->layout='estandar';
            $model = new ActRubroElegible();
            if ( $model->load(Yii::$app->request->post()) ) {
                
                $csvMimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
                if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
                    //var_dump($_FILES['ActRubroElegible']);die;
                    if(is_uploaded_file($_FILES['file']['tmp_name'])){
                        
                        //open uploaded csv file with read only mode
                        $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
                        
                        //skip first line
                        fgetcsv($csvFile);
                        
                        //parse data from csv file line by line
                        while(($line = fgetcsv($csvFile)) !== FALSE){
                            //
                            $informacion=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>utf8_encode($line[0])])->one();
                            if(!$informacion)
                            {
                                var_dump(utf8_encode($line[0]));die;
                            }
                            else
                            {
                                $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacion->InvestigadorID])->one();    
                            }
                            
                            $poa=Poa::find()->where('ProyectoID=:ProyectoID and Estado=1',[':ProyectoID'=>$proyecto->ID])->one();
                            $mes = InformacionGeneral::find()
                                ->select('Meses')
                                ->where(['InvestigadorID' => $informacion->InvestigadorID])
                                ->one();
                            if(utf8_encode($line[11])=="1"){
                                $componente=Componente::find()->where('Correlativo=:Correlativo and PoaID=:PoaID',[':Correlativo'=>utf8_encode($line[1]),':PoaID'=>$poa->ID])->one();
                                if(!$componente)
                                {
                                    $componente=new Componente;
                                    $componente->PoaID=$poa->ID;
                                    $componente->Correlativo=utf8_encode($line[1]);
                                    $componente->Nombre=utf8_encode($line[2]);
                                    $componente->Peso=utf8_encode($line[3]);
                                    $componente->Experimento=utf8_encode($line[7]);
                                    $componente->Evento=utf8_encode($line[8]);
                                    $componente->Estado=1;
                                    $componente->save();
                                    
                                    for ($a=1; $a <= $mes->Meses; $a++) {
                                        $cronoComponente = new CronogramaComponente();
                                        $cronoComponente->ComponenteID = $componente->ID;
                                        $cronoComponente->Mes = $a;
                                        $cronoComponente->save();
                                    }
                                }
                                
                                $marcoLogicoComponente=new MarcoLogicoComponente;
                                $marcoLogicoComponente->ComponenteID=$componente->ID;
                                $marcoLogicoComponente->IndicadorMeta=utf8_encode($line[4]);
                                $marcoLogicoComponente->IndicadorUnidadMedida=utf8_encode($line[5]);
                                $marcoLogicoComponente->IndicadorDescripcion=utf8_encode($line[6]);
                                $marcoLogicoComponente->MedioVerificacion=utf8_encode($line[9]);
                                $marcoLogicoComponente->Supuestos=utf8_encode($line[10]);
                                $marcoLogicoComponente->Estado=1;
                                $marcoLogicoComponente->save();
                            }
                            elseif(utf8_encode($line[11])=="2"){
                                $componenteID=substr(utf8_encode($line[1]),0,1);
                                $CorrelativoID=substr(utf8_encode($line[1]),2,1);
                                $componente=Componente::find()->where('Correlativo=:Correlativo and PoaID=:PoaID',[':Correlativo'=>$componenteID,':PoaID'=>$poa->ID])->one();//One($componenteID);
                                if(!$componente)
                                {
                                    var_dump(utf8_encode($line[0]));die;
                                }
                                
                                $actividad=Actividad::find()->where('Correlativo=:Correlativo and ComponenteID=:ComponenteID',[':Correlativo'=>$CorrelativoID,':ComponenteID'=>$componente->ID])->one();
                                if(!$actividad)
                                {
                                    $actividad=new Actividad;
                                    $actividad->ComponenteID=$componente->ID;
                                    $actividad->Correlativo=$CorrelativoID;
                                    $actividad->Nombre=utf8_encode($line[2]);
                                    $actividad->Peso=utf8_encode($line[3]);
                                    $actividad->Experimento=utf8_encode($line[7]);
                                    $actividad->Evento=utf8_encode($line[8]);
                                    $actividad->Estado=1;
                                    $actividad->save();
                                    
                                    for ($a=1; $a <= $mes->Meses; $a++) {
                                        $cronoActividad = new CronogramaActividad();
                                        $cronoActividad->ActividadID = $actividad->ID;
                                        $cronoActividad->Mes = $a;
                                        $cronoActividad->save();
                                    }
                                }
                                
                                $marcoLogicoActividad=new MarcoLogicoActividad;
                                $marcoLogicoActividad->ActividadID=$actividad->ID;
                                $marcoLogicoActividad->IndicadorMetaFisica=utf8_encode($line[4]);
                                $marcoLogicoActividad->IndicadorUnidadMedida=utf8_encode($line[5]);
                                $marcoLogicoActividad->IndicadorDescripcion=utf8_encode($line[6]);
                                $marcoLogicoActividad->IndicadorMedioVerificacion=utf8_encode($line[9]);
                                $marcoLogicoActividad->IndicadorSupuestos=utf8_encode($line[10]);
                                $marcoLogicoActividad->Estado=1;
                                $marcoLogicoActividad->save();
                            }
                        }
                        
                        //close opened csv file
                        fclose($csvFile);
                       
                    }
                }
                
            }
            
            return $this->render('carga');
        }

}
