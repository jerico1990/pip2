<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Usuario;
use app\models\Persona;
use app\models\Investigador;
use app\models\Proyecto;
use app\models\Componente;
use app\models\Actividad;
use app\models\Poa;
use app\models\InformacionGeneral;
use app\models\CronogramaProyecto;
use app\models\ActRubroElegible;
use app\models\CronogramaComponente;
use app\models\CronogramaActividad;
use app\models\CronogramaActRubroElegible;
use app\models\RubroElegible;
use app\models\AreSubCategoria;
use app\models\CronogramaAreSubCategoria;
use app\models\MarcoLogicoActividad;
use app\models\Tarea;
use app\models\CronogramaTarea;
use app\models\TareaObservacion;

class RecursosTareaController extends Controller
{
    /**
     * @inheritdoc
     */
    
    
    

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
    public $investigador = NULL;
    public function init(){
        // parent::__construct();
        if(!Yii::$app->user->isGuest){
            $usuario            = Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador  = Persona::findOne($usuario->PersonaID);
            $investigador       = Investigador::find()->where(['UsuarioID'=>$usuario->id])->one();
            if($investigador)
            {
                $this->investigador = $investigador->ID;
            }
        }
        else
        {
            $this->redirect('/login');
            return false; //not run the action
        }
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $this->layout='estandar';
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        
        
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID and Estado=1',[':ProyectoID'=>$proyecto->ID])->one();
        $componentes=Componente::find()->where('PoaID=:PoaID and Estado=1',[':PoaID'=>$poa->ID])->all();
        return $this->render('index',['componentes'=>$componentes,'usuario'=>$usuario]);
    }


    public function actionRecursosForm(){
        $this->layout='vacio';
        return $this->render('recursos/_recursos_form');
    }



    /**
     * Rescursos JSON 
     * @return [type] [description]
     */
    public function actionTareasJson($codigo=null){
        $this->layout='vacio';
        // Proyectos
        $usuario=Usuario::find()->where('username=:username',[':username'=>$codigo])->one();
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $proyecto = Poa::find()
            ->select('Poa.ID, Poa.ProyectoID')
            ->innerJoin('Proyecto','Poa.ProyectoID = Proyecto.ID')
            ->where(['Proyecto.InvestigadorID'=> $investigador->ID,'Poa.Estado' => 1])
            ->one();


        $proyectox = array(
            'ID'             => $proyecto->ProyectoID, 
            'Componentes'    => $this->get_objetivo($proyecto->ID),
            'Cronogramas'    => $this->getCronogramaProyecto($proyecto->ProyectoID,'proyecto')
        );

        echo json_encode($proyectox); 
    }

    private function get_objetivo($idPoa){
        $jsonComponente = array();
        $component = Componente::find()
                ->where(['Estado' => 1, 'PoaID' => $idPoa])
                ->orderBy('Correlativo asc')
                ->all();
        if(!empty($component) ){
            foreach ($component as $comp) {
                $jsonComponente[] = array(
                    'ID'            => $comp->ID, 
                    'Nombre'        => $comp->Nombre,
                    'Peso'          => $comp->Peso,
                    'Actividades'   => $this->get_actividades($comp->ID), 
                    'Cronogramas'   => $this->getCronogramaProyecto($comp->ID,'componente')
                );

            }
        }
        return $jsonComponente;
    }

    private function get_actividades($idComponente){
        $jsonActividades = array();
        $actv = Actividad::find()
                ->where(['Estado' => 1, 'ComponenteID' => $idComponente])
                ->orderBy('Correlativo asc')
                ->all();
        if(!empty($actv) ){
            foreach ($actv as $actvd) {
                $jsonActividades[] = array(
                    'ID'            => $actvd->ID, 
                    'ComponenteID'  => $actvd->ComponenteID, 
                    'Nombre'        => $actvd->Nombre,
                    'Peso'          => $actvd->Peso, 
                    'UnidadMedida'  => $this->get_actividadesmarco($actvd->ID)->IndicadorUnidadMedida, 
                    'CostoUnitario' => $actvd->CostoUnitario,
                    'Situacion' => $actvd->Situacion, 
                    'MetaFisica'    => $this->get_actividadesmarco($actvd->ID)->IndicadorMetaFisica,
                    'Tareas'        => $this->get_actTareas($actvd->ID),
                    'Cronogramas'   => $this->getCronogramaProyecto($actvd->ID,'actividad')
                );
            }
        }
        return $jsonActividades;
    }

    private function get_actTareas($idActividad){
        $jsonActividades = array();
        $actv = Tarea::find()
                ->where(['ActividadID' => $idActividad])
                ->all();
        if(!empty($actv) ){
            foreach ($actv as $actvd) {
                $jsonActividades[] = array(
                    'ID'                => $actvd->ID, 
                    'ActividadID'       => $actvd->ActividadID, 
                    'Descripcion'       => $actvd->Descripcion, 
                    'UnidadMedida'      => $actvd->UnidadMedida,
                    'Peso'              => $actvd->Peso,
                    'MetaFisica'          => $actvd->MetaFisica,
                    'Situacion'         => $actvd->Situacion,
                    'ObservacionID'         => $this->getObservacionTareaID($actvd->ID),
                    'Observacion'           => $this->getObservacionTarea($actvd->ID),
                    'ObservacionSituacionID'=> $this->getObservacionSituacionID($actvd->ID),
                    
                    //'CostoUnitario'     => $actvd->CostoUnitario, 
                    //'MetaFisica'        => $actvd->MetaFisica, 
                   // 'AreSubCategorias'  => $this->get_areSubCategoria($actvd->ID),
                    //'RubroElegible'     => $this->get_rubroElegible($actvd->RubroElegibleID),
                    'Cronogramas'       => $this->getCronogramaProyecto($actvd->ID,'Tareas')
                );
            }
        }
        return $jsonActividades;
    }

    private function get_rubroElegible($idRubro){
        $jsonActividades = array();
            $actv = RubroElegible::find()
                    ->where(['ID' => $idRubro])
                    ->all();

        if(!empty($actv) ){
            foreach ($actv as $actvd) {
                $jsonActividades = array(
                    'TipoRubroElegibleID'=> $actvd->TipoRubroElegibleID, 
                    'ID'                 => $actvd->ID, 
                    'Nombre'             => $actvd->Nombre
                );
            }
        }
        return $jsonActividades;
    }

    private function get_areSubCategoria($idActRubroElegible){
        $jsonActividades = array();
        $actv = AreSubCategoria::find()
                ->where(['ActRubroElegibleID' => $idActRubroElegible])
                ->all();
        if(!empty($actv) ){
            foreach ($actv as $actvd) {
                $jsonActividades[] = array(
                    'ID'                    => $actvd->ID, 
                    'ActRubroElegibleID'    => $actvd->ActRubroElegibleID, 
                    'Nombre'                => $actvd->Nombre, 
                    'UnidadMedida'          => $actvd->UnidadMedida, 
                    'CostoUnitario'         => $actvd->CostoUnitario, 
                    'MetaFisica'            => $actvd->MetaFisica, 
                    'Total'                 => $actvd->Total, 
                    'TotalConMetaFisica'    => $actvd->TotalConMetaFisica, 
                    'Cronogramas'           => $this->getCronogramaProyecto($actvd->ID,'areSubCategoria')
                );
            }
        }
        return $jsonActividades;
    }

    private function get_actividadesmarco($idActividad){
        $jsonActividadesMarco = array();
        $actvmarco = MarcoLogicoActividad::find()
                ->Select('IndicadorUnidadMedida,sum(CAST(IndicadorMetaFisica AS FLOAT)) IndicadorMetaFisica')
                ->where(['Estado' => 1, 'ActividadID' => $idActividad])
                ->groupBy('IndicadorUnidadMedida')
                ->one();
        if(!$actvmarco)
        {
            $actvmarco=new MarcoLogicoActividad;
        }
        return $actvmarco;
    }

    private function getCronogramaProyecto($id,$tipo = ''){
        $this->layout='vacio';
        $meses = array();
        $cronograma = array();
        switch ($tipo) {
            case 'proyecto':
                $meses = CronogramaProyecto::find()
                        ->where(['ProyectoID'=> $id])
                        ->Orderby('Mes asc')
                        ->all();
                $indicador = 'ProyectoID';
            break;
            case 'componente':
                $meses = CronogramaComponente::find()
                        ->where(['ComponenteID'=> $id])
                        ->Orderby('Mes asc')
                        ->all();
                $indicador = 'ComponenteID';
            break;

            case 'actividad':
                $meses = CronogramaActividad::find()
                        ->where(['ActividadID'=> $id])
                        ->Orderby('Mes asc')
                        ->all();
                $indicador = 'ActividadID';
            break;

            case 'Tareas':
                
                $meses = CronogramaTarea::find()
                        ->where(['TareaID'=> $id])
                        ->Orderby('Mes asc')
                        ->all();
                $indicador = 'TareaID';
            break;

            case 'areSubCategoria':
                $meses = CronogramaAreSubCategoria::find()
                        ->where(['AreSubCategoriaID'=> $id])
                        ->Orderby('Mes asc')
                        ->all();
                $indicador = 'AreSubCategoriaID';
            break;
        }

        if(!empty($meses)){
            foreach ($meses as $mes) {
                $cronograma[] = array(
                    'ID'                    => $mes->ID,
                    $indicador              => $mes->{$indicador},
                    'Mes'                   => $mes->Mes,
                    'MetaFinanciera'        => $mes->MetaFinanciera,
                    'PoafMetaFinanciera'    => $mes->PoafMetaFinanciera,
                    'MetaFisica'            => $mes->MetaFisica,
                    'Situacion'             => $mes->Situacion,
                    'MesDescripcion'        => Yii::$app->tools->DescripcionMes($mes->Mes),
                    );
            }
        }else{
            $cronograma = array(
                'ID'                    => '',
                $indicador              => '',
                'Mes'                   => '',
                'MetaFinanciera'        => '',
                'PoafMetaFinanciera'    => '',
                'MetaFisica'            => '',
                'Situacion'            => '',
                'MesDescripcion'        => '',
            );
        }

        return $cronograma;
    }


    public function actionTareas($id){
        $this->layout='vacio';
        $tareas = Tarea::find()
                ->all();
        return $this->render('tareas/_tareas_form',['idactividad'=>$id,'tareas' => $tareas]);

    }

    public function actionTareasCrear(){
        $this->layout='vacio';
        
        $mes = InformacionGeneral::find()
            ->select('Meses')
            ->where(['InvestigadorID' => $this->investigador])
            ->one();
        $model = new Tarea();
        if ( $model->load(Yii::$app->request->post()) ) {
            $model->Correlativo=$this->CorrelativoTarea($model->ActividadID);
            $model->Situacion=0;
            $model->MetaAvance=0;
            $model->MetaFisicaEjecutada=0;
            $model->ActividadPonderado=0;
            $model->save();
            $porcetanje=0;
            $CountTareas=Tarea::find()->where('ActividadID=:ActividadID',[':ActividadID'=>$model->ActividadID])->count();
            $porcetanje=100/$CountTareas;
            \Yii::$app->db->createCommand("UPDATE Tarea SET Peso=:Peso WHERE ActividadID=:ActividadID")
            ->bindValue(':ActividadID', $model->ActividadID)
            ->bindValue(':Peso', $porcetanje)
            ->execute();
            
            for ($i=1; $i <= $mes->Meses; $i++) { 
                $cronogramaTarea = new CronogramaTarea();
                $cronogramaTarea->TareaID = $model->ID;
                $cronogramaTarea->Mes = $i;
                $cronogramaTarea->MetaFisica=0;
                $cronogramaTarea->MetaFisicaEjecutada=0;
                $cronogramaTarea->MetaAvance=0;
                $cronogramaTarea->SituacionEjecucion=0;
                $cronogramaTarea->Situacion=0;
                $cronogramaTarea->save();
            }
            $arr = array(
                    'Success' => true,
                );
            echo json_encode($arr);
        }
    }
    
    public function actionGrabarMetaCronogramaTarea($ID=null,$MetaFisica=null)
    {
        $cronogramaTarea=CronogramaTarea::findOne($ID);
        if(is_null($MetaFisica)){
            $nMeta = 0;
        }else{
            $nMeta = str_replace(",", "", $MetaFisica);
        }
        $cronogramaTarea->MetaFisica= $nMeta;
        $cronogramaTarea->save();
        $arr = array('Success' => true,);
        echo json_encode($arr);
    }
    
    public function actionGrabarMetaCronogramaActividad($ID=null,$MetaFisica=null)
    {
        $cronogramaActividad=CronogramaActividad::findOne($ID);
        $cronogramaActividad->MetaFisica=str_replace(",", "", $MetaFisica);
        $cronogramaActividad->save();
        $arr = array('Success' => true,);
        echo json_encode($arr);
    }
    
    public function actionDescargarTarea($codigo=null)
    {
        $this->layout='estandar';
        $usuario=Usuario::find()->where('username=:username',[':username'=>$codigo])->one();
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
        $componentes=Componente::find()->where('PoaID=:PoaID',[':PoaID'=>$poa->ID])->orderBy('Correlativo asc')->all();
        
        return $this->render('descargar-tarea',['componentes'=>$componentes,'proyecto'=>$proyecto]);
    }
    
    public function actionActualizarTarea($id=null)
    {
        $this->layout='vacio';
        //var_dump($id);die;
        $tarea=Tarea::findOne($id);
        if ($tarea->load(Yii::$app->request->post()) ) {
            
            $tarea->update();
            // CronogramaTarea::updateAll(['MetaFisica' => 0], 'TareaID='.$tarea->ID);
            $arr = array(
                    'Success' => true,
                    
                );
            echo json_encode($arr);
            die;
        }
        return $this->render('tareas/actualizar_tareas_form',['tarea' => $tarea,'id'=>$id]);
    }
    
    public function actionDeleteTarea($id)
    {
        $tarea=Tarea::findOne($id);
        CronogramaTarea::deleteAll(['TareaID' =>$tarea->ID]);
        Tarea::deleteAll(['ID' =>$id]);
        
    }
    
    public function DescripcionMes($mes=null)
    {
        setlocale (LC_TIME,"spanish");
        //$mes = 01;
        $mes=$mes-1;
        $fechaInicio = '2016-01-01';
        $nuevafecha = strtotime ( '+'.$mes.' month' , strtotime ( $fechaInicio ) ) ;
        $nuevafecha = strtoupper(strftime ( '%b %Y' , $nuevafecha ) ) ;
        return $nuevafecha;
    }
    public function CorrelativoTarea($actividadID)
    {
        $tarea=Tarea::find()->where('ActividadID=:ActividadID',[':ActividadID'=>$actividadID])->orderBy('Correlativo desc')->one();
        if($tarea)
        {
            return $tarea->Correlativo+1;
        }
        else
        {
            return 1;
        }
    }
    
    public function getObservacionTarea($id)
    {
        $observacion=TareaObservacion::find()->where('TareaID=:TareaID and Estado=1',[':TareaID'=>$id])->one();
        if($observacion)
        {
            return $observacion->Observacion;
        }
        
        return "";
    }
    
    public function getObservacionSituacionID($id)
    {
        $observacion=TareaObservacion::find()->where('TareaID=:TareaID and Estado=3',[':TareaID'=>$id])->one();
        if($observacion)
        {
            return $observacion->Estado;
        }
        
        return "";
    }
    
    public function getObservacionTareaID($id)
    {
        $observacion=TareaObservacion::find()->where('TareaID=:TareaID and Estado=1',[':TareaID'=>$id])->one();
        if($observacion)
        {
            return $observacion->ID;
        }
        
        return "";
    }
    
    public function actionAprobarObservacion($ObservacionID)
    {
        $observacion=TareaObservacion::findOne($ObservacionID);
        $observacion->Estado=3;
        $observacion->update();
        $tarea=Tarea::find()->where('ID=:ID',[':ID'=>$observacion->TareaID])->one();
        $tarea->Situacion=2;
        $tarea->update();
        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
    
    
    public function actionCarga()
    {
        $this->layout='estandar';
        ini_set('memory_limit', '-1');
        $a = new ActRubroElegible();
        if ( $a->load(Yii::$app->request->post()) ) {
            
            $csvMimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
            if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
                //var_dump($_FILES['ActRubroElegible']);die;
                if(is_uploaded_file($_FILES['file']['tmp_name'])){
                    
                    //open uploaded csv file with read only mode
                    $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
                    
                    //skip first line
                    //fgetcsv($csvFile);
                    //parse data from csv file line by line
                    $Codigo=[];
                    $i=0;
                    while(($line = fgetcsv($csvFile)) !== FALSE){
                        if($i==0)
                        {
                            $Codigo=$line;
                            //var_dump($Codigo);die;
                           //$i++;
                        }
                        else
                        {
                            //echo '<pre>';print_r($Codigo);
                            //echo '<pre>';print_r($line); 
                            //for($a=0;$a<count($line);$a++)
                            //{
                                //if(!empty($line[$a]) && $line[$a]!=0)
                               // {
                                    if($line[3]=='')
                                    {
                                        $informacion=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>utf8_encode($line[0])])->one();
                                        $explode=explode(".",$line[1]);
                                       // var_dump($explode);die;
                                        $componenteID=utf8_encode($explode[0]);
                                        //if(isset($explode[1])){
                                            $actividadID=utf8_encode($explode[1]);
                                            
                                            $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacion->InvestigadorID])->one();    
                                            $poa=Poa::find()->where('ProyectoID=:ProyectoID and Estado=1',[':ProyectoID'=>$proyecto->ID])->one();
                                            
                                            $componente=Componente::find()
                                                                ->where('PoaID=:PoaID and Correlativo=:Correlativo and Estado=1',
                                                                [':PoaID'=>$poa->ID,':Correlativo'=>utf8_encode($componenteID)])
                                                                ->one();
                                                                
                                            $actividad=Actividad::find()
                                                                ->where('ComponenteID=:ComponenteID and Correlativo=:Correlativo and Estado=1',[':ComponenteID'=>$componente->ID,':Correlativo'=>utf8_encode($actividadID)])
                                                                ->one();
                                            if(!$actividad)
                                            {
                                                var_dump($explode);var_dump($componente->ID);die;
                                            }
                                            $mes = InformacionGeneral::find()
                                            ->select('Meses')
                                            ->where(['InvestigadorID' => $informacion->InvestigadorID])
                                            ->one(); 
                                            $model = new Tarea();
                                            $model->ActividadID=$actividad->ID;
                                            $model->Descripcion=utf8_encode($line[4]);
                                            $model->UnidadMedida=utf8_encode($line[6]);
                                            $model->MetaFisica=(float)utf8_encode($line[7]);
                                            $model->Correlativo=$this->CorrelativoTarea($actividad->ID);
                                            $model->Situacion=0;
                                            $model->MetaAvance=0;
                                            $model->MetaFisicaEjecutada=0;
                                            $model->ActividadPonderado=0;
                                            $model->save();
                                            
                                            $porcetanje=0;
                                            $CountTareas=Tarea::find()->where('ActividadID=:ActividadID',[':ActividadID'=>$actividad->ID])->count();
                                            if($CountTareas!=0)
                                            {
                                                $porcetanje=100/$CountTareas;    
                                            }
                                            
                                            \Yii::$app->db->createCommand("UPDATE Tarea SET Peso=:Peso WHERE ActividadID=:ActividadID")
                                            ->bindValue(':ActividadID', $actividad->ID)
                                            ->bindValue(':Peso', $porcetanje)
                                            ->execute();
                                            $a=8;
                                            for ($i=1; $i <= $mes->Meses; $i++) { 
                                                $cronogramaTarea = new CronogramaTarea();
                                                $cronogramaTarea->TareaID = $model->ID;
                                                $cronogramaTarea->Mes = $i;
                                                $cronogramaTarea->MetaFisica=(float)(isset($line[$a]))?utf8_encode($line[$a]):0;
                                                $cronogramaTarea->MetaFisicaEjecutada=0;
                                                $cronogramaTarea->MetaAvance=0;
                                                $cronogramaTarea->Situacion=0;
                                                $cronogramaTarea->SituacionEjecucion=0;
                                                $cronogramaTarea->save();
                                                $a++;
                                            } 
                                        //}
                                        
                                       
                                   // }
                                    
                                    
                                //}
                                
                            }
                            
                        }
                        $i++;
                        
                    }
                    
                    //close opened csv file
                    fclose($csvFile);
                   
                }
            }
            
        }
        return $this->render('carga');
    }
}
