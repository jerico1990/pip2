<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\InformacionGeneral;
use app\models\ProgramaTransversal;
use app\models\PasoCritico;
use app\models\DireccionLinea;
use app\models\TipoInvestigacion;
use app\models\UnidadEjecutora;
use app\models\UnidadOperativa;
use app\models\Persona;
use app\models\Programa;
use app\models\CultivoCrianza;
use app\models\Especie;
use app\models\Ubigeo;
use app\models\Usuario;
use app\models\Investigador;
use app\models\EntidadParticipante;
use app\models\TipoInstitucion;
use app\models\AmbitoIntervencion;
use app\models\MiembroEquipoGestion;
use app\models\Proyecto;
use app\models\Inbox;
use app\models\Poa;
use kartik\mpdf\Pdf;
use app\models\Pat;
use app\models\Seguimiento;
use app\models\NivelAprobacion;
use app\models\DocumentoInformacionGeneral;
use app\models\InformeTecnicoFinanciero;
use app\models\MarcoLogicoComponente;

// use PhpOffice\PhpWord\Writer\PDF;
use yii\web\UploadedFile;

class InformacionGeneralController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($investigador=null)
    {
        //var_dump(\Yii::$app->user->identity->rol);die;
        
       /* if($investigador)
        {
            
        }
        else
        {*/
            //$usuario=Usuario::findOne(\Yii::$app->user->id);
            //$datosInvestigador=Persona::findOne($usuario->PersonaID);
            //$investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            //$investigador=$investigador->ID;
        //}
        $deshabilitar='disabled';
        if($investigador==NULL)
        {
            $this->layout='estandar';
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $deshabilitar='';
        }
        else
        {
            $this->layout='vacio';
            $investigador=Investigador::findOne($investigador);
            $usuario=Usuario::findOne($investigador->UsuarioID);
	    $datosInvestigador=Persona::findOne($usuario->PersonaID);
        }
        if(!$investigador && \Yii::$app->user->identity->rol!=7)
        {
            return $this->redirect(['panel/index']) ;
        }
        
        $model=$this->InformacionGeneral($investigador->ID);
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        
        //CargaMaestros
        $programasTransversales=ProgramaTransversal::find()->all();
        $direccionesLineas=DireccionLinea::find()->all();
        $tiposInvestigaciones=TipoInvestigacion::find()->all();
        $unidadesEjecutoras=UnidadEjecutora::find()->all();
        $unidadesOperativas=$this->UnidadesOperativas($model->UnidadEjecutoraID);
        $programas=Programa::find()->all();
        $cultivosCrianzas=$this->CultivoCrianza($model->ProgramaID);
        $especies=$this->Especies($model->CultivoCrianzaID);
        $departamentos=Ubigeo::find()->where('len(ID) = 2')->all();
        $provincias=$this->Provincia($model->DepartamentoID);
        $distritos=$this->Distrito($model->ProvinciaID);
        $ambitosIntervenciones=$this->AmbitoIntervencion($model->ID);
        $documentosInformacionesGenerales=DocumentoInformacionGeneral::find()->where('InformacionGeneralID=:InformacionGeneralID',[':InformacionGeneralID'=>$model->ID])->all();
        $EntidadParticipante = EntidadParticipante::find()
            ->select('Investigador.UsuarioID,
                    EntidadParticipante.ID as EntidadID,
                    EntidadParticipante.RazonSocial,
                    EntidadParticipante.FechaCreacion,
                    EntidadParticipante.Ruc,
                    EntidadParticipante.AporteMonetario,
                    EntidadParticipante.AporteNoMonetario,
                    TipoInstitucion.Nombre')
            ->innerJoin('Investigador','Investigador.ID = EntidadParticipante.InvestigadorID')
            ->innerJoin('TipoInstitucion','EntidadParticipante.TipoInstitucionID = TipoInstitucion.ID ')
            ->where(['EntidadParticipante.Estado'=>1,'EntidadParticipante.InvestigadorID'=>$investigador->ID])
            ->all();


        if ($model->load(Yii::$app->request->post())) {
            $proyecto->Fin=$model->Fin;
            $proyecto->Proposito=$model->Proposito;
            $proyecto->save();
            $model->InvestigadorID=$investigador->ID;
            $saved = $model->save();

            if($saved == 1){
                $arr = array(
                    'Success' => true
                );    
            }else{
                $arr = array(
                    'Message' => 'Ocurrio un error intentelo nuevamente',
                    'Error'   =>  true
                );
                
            }
            echo json_encode($arr);

            die();
            /*
            $inbox=new Inbox;
            $inbox->CodigoProyecto=$usuario->username;
            $inbox->UsuarioEnvia=\Yii::$app->user->id;
            $inbox->UsuarioRolID=8;
            $inbox->Etapa=1;
            $inbox->Situacion=3;
            $inbox->Estado=1;
            $inbox->save();
            */
            // return $this->refresh();
        }
        
        // echo '<pre>';
        // print_r($proyecto);
        // die;
        if($proyecto->Situacion ==0){
            $deshabilitar = '';
        }else{
            $deshabilitar = 'disabled';
        }
        
        if(\Yii::$app->user->identity->rol==5 || \Yii::$app->user->identity->rol==2 || \Yii::$app->user->identity->rol==11 || \Yii::$app->user->identity->rol==16)
        {
            $deshabilitar='disabled';
        }
        
        return $this->render('index',['programasTransversales'=>$programasTransversales,
                                      'direccionesLineas'=>$direccionesLineas,
                                      'tiposInvestigaciones'=>$tiposInvestigaciones,
                                      'unidadesEjecutoras'=>$unidadesEjecutoras,
                                      'unidadesOperativas'=>$unidadesOperativas,
                                      'programas'=>$programas,
                                      'cultivosCrianzas'=>$cultivosCrianzas,
                                      'especies'=>$especies,
                                      'departamentos'=>$departamentos,
                                      'provincias'=>$provincias,
                                      'distritos'=>$distritos,
                                      'ambitosIntervenciones'=>$ambitosIntervenciones,
                                      'EntidadParticipante'=>$EntidadParticipante,
                                      'datosInvestigador'=>$datosInvestigador,
                                      'usuario'=>$usuario,
                                      'proyecto'=>$proyecto,
                                      'model'=>$model,
                                      'deshabilitar'=>$deshabilitar,
                                      'documentosInformacionesGenerales'=>$documentosInformacionesGenerales,
                                      'investigador'=>$investigador]);
    }
    
    public function UnidadesOperativas($unidadEjecutoraID=null)
    {
        $model=array();
        if($unidadEjecutoraID)
        {
            $model=UnidadOperativa::find()->where('UnidadEjecutoraID=:UnidadEjecutoraID',[':UnidadEjecutoraID'=>$unidadEjecutoraID])->all();
        }
        return $model;
    }
    
    public function CultivoCrianza($programaID=null)
    {
        $model=array();
        if($programaID)
        {
           
            $model=CultivoCrianza::find()->where('ProgramaID=:ProgramaID',[':ProgramaID'=>$programaID])->all();
        }
        return $model;
    }
    
    public function Especies($cultivoCrianzaID=null)
    {
        $model=array();
        if($cultivoCrianzaID)
        {
            $model=Especie::find()->where('CultivoCrianzaID=:CultivoCrianzaID',[':CultivoCrianzaID'=>$cultivoCrianzaID])->all();
        }
        return $model;
    }
    
    
    public function InformacionGeneral($investigador=null)
    {
        $model=InformacionGeneral::find()
                ->select('InformacionGeneral.*,UnidadOperativa.UnidadEjecutoraID,Especie.CultivoCrianzaID,CultivoCrianza.ProgramaID,Departamento.ID as DepartamentoID,Provincia.ID as ProvinciaID,Proyecto.Fin,Proyecto.Proposito')
                ->innerJoin('Investigador','Investigador.ID=InformacionGeneral.InvestigadorID')
                ->innerJoin('Proyecto','Proyecto.InvestigadorID=Investigador.ID')
                ->innerJoin('UnidadOperativa','UnidadOperativaID=UnidadOperativa.ID')
                ->innerJoin('Especie','Especie.ID=InformacionGeneral.EspecieID')
                ->innerJoin('CultivoCrianza','CultivoCrianza.ID=Especie.CultivoCrianzaID')
                ->innerJoin('Ubigeo Distrito','Distrito.ID=InformacionGeneral.DistritoID')
                ->innerJoin('Ubigeo Provincia','Provincia.ID=SUBSTRING(InformacionGeneral.DistritoID, 1, 4)')
                ->innerJoin('Ubigeo Departamento','Departamento.ID=SUBSTRING(InformacionGeneral.DistritoID, 1, 2)')
                ->where('InformacionGeneral.InvestigadorID=:InvestigadorID',['InvestigadorID'=>$investigador])
                ->one();
                
        if(!$model)
        {
            $model=new InformacionGeneral;
        }
        return $model;
    }
    
    public function Provincia($departamentoID=null)
    {
        $model=array();
        if($departamentoID)
        {
            $model=Ubigeo::find()->where('len(ID) = 4 and SUBSTRING(ID, 1, 2)=:departamentoID',[':departamentoID'=>$departamentoID])->all();
        }
        return $model;
    }
    
    public function Distrito($provinciaID)
    {
        $model=array();
        if($provinciaID)
        {
            $model=Ubigeo::find()->where('len(ID) = 6 and SUBSTRING(ID, 1, 4)=:provinciaID',[':provinciaID'=>$provinciaID])->all();
        }
        return $model;
    }
    
    public function AmbitoIntervencion($informacionGeneralID=null)
    {
        $model=array();
        if($informacionGeneralID)
        {
            $model=AmbitoIntervencion::find()
                    ->select('AmbitoIntervencion.ID,Departamento.Nombre Departamento,Provincia.Nombre Provincia,Distrito.Nombre Distrito,AmbitoIntervencion.Latitud,AmbitoIntervencion.Longitud')
                    ->innerJoin('Ubigeo Distrito','Distrito.ID=AmbitoIntervencion.DistritoID')
                    ->innerJoin('Ubigeo Provincia','Provincia.ID=SUBSTRING(AmbitoIntervencion.DistritoID, 1, 4)')
                    ->innerJoin('Ubigeo Departamento','Departamento.ID=SUBSTRING(AmbitoIntervencion.DistritoID, 1, 2)')
                    ->where('InformacionGeneralID=:InformacionGeneralID',[':InformacionGeneralID'=>$informacionGeneralID])
                ->all();
        }
        return $model;
    }
    
    public function actionEntidades($investigador=null)
    {
        $this->layout='estandar';
        if($investigador)
        {
            
        }
        else
        {
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $investigador=$investigador->ID;
        }
        
        
        
        $model = new EntidadParticipante();
        

        return $this->render('entidades/entidades');
    }

    // Cargar Combo Unidad Operativa
    public function actionUnidadOperativa()
    {
        if(isset($_POST['unidadEjecutoraID']) && trim($_POST['unidadEjecutoraID'])!='')
        {
            $unidadEjecutoraID=$_POST['unidadEjecutoraID'];
            $countUnidadesOperativas=UnidadOperativa::find()->select('ID,Nombre')->where('UnidadEjecutoraID=:UnidadEjecutoraID',[':UnidadEjecutoraID'=>$unidadEjecutoraID])->groupBy('ID,Nombre')->count();
            $unidadesOperativas=UnidadOperativa::find()->select('ID,Nombre')->where('UnidadEjecutoraID=:UnidadEjecutoraID',[':UnidadEjecutoraID'=>$unidadEjecutoraID])->groupBy('ID,Nombre')->orderBy('Nombre')->all();
            if($countUnidadesOperativas>0){
                echo "<option value>Seleccionar</option>";
                foreach($unidadesOperativas as $unidadOperativa){
                    echo "<option value='".$unidadOperativa->ID."'>".$unidadOperativa->Nombre."</option>";
                }
            }
            else{
                echo "<option value>Seleccionar</option>";
            }
        }
    }
    
    // Cargar Combo CultivoCrianza
    public function actionCultivoCrianza()
    {
        if(isset($_POST['programaID']) && trim($_POST['programaID'])!='')
        {
            $programaID=$_POST['programaID'];
            $countCultivosCrianzas=CultivoCrianza::find()->select('ID,Nombre')->where('ProgramaID=:ProgramaID',[':ProgramaID'=>$programaID])->groupBy('ID,Nombre')->count();
            $cultivosCrianzas=CultivoCrianza::find()->select('ID,Nombre')->where('ProgramaID=:ProgramaID',[':ProgramaID'=>$programaID])->groupBy('ID,Nombre')->orderBy('Nombre')->all();
            if($countCultivosCrianzas>0){
                echo "<option value>Seleccionar</option>";
                foreach($cultivosCrianzas as $cultivoCrianza){
                    echo "<option value='".$cultivoCrianza->ID."'>".$cultivoCrianza->Nombre."</option>";
                }
            }
            else{
                echo "<option value>Seleccionar</option>";
            }
        }
    }
    
    // Cargar Combo CultivoCrianza
    public function actionEspecie()
    {
        if(isset($_POST['cultivoCrianzaID']) && trim($_POST['cultivoCrianzaID'])!='')
        {
            $cultivoCrianzaID=$_POST['cultivoCrianzaID'];
            $countEspecies=Especie::find()->select('ID,Nombre')->where('CultivoCrianzaId=:CultivoCrianzaId',[':CultivoCrianzaId'=>$cultivoCrianzaID])->groupBy('ID,Nombre')->count();
            $especies=Especie::find()->select('ID,Nombre')->where('CultivoCrianzaId=:CultivoCrianzaId',[':CultivoCrianzaId'=>$cultivoCrianzaID])->groupBy('ID,Nombre')->orderBy('Nombre')->all();
            if($countEspecies>0){
                echo "<option value>Seleccionar</option>";
                foreach($especies as $especie){
                    echo "<option value='".$especie->ID."'>".$especie->Nombre."</option>";
                }
            }
            else{
                echo "<option value>Seleccionar</option>";
            }
        }
    }
    //Cargar Provincias
    public function actionProvincia()
    {
        if(isset($_POST['departamentoID']) && trim($_POST['departamentoID'])!='')
        {
            $departamentoID=$_POST['departamentoID'];
            $countProvincias=Ubigeo::find()->select('ID,Nombre')->where('len(ID) = 4 and SUBSTRING(ID, 1, 2)=:departamentoID',[':departamentoID'=>$departamentoID])->groupBy('ID,Nombre')->count();
            $provincias=Ubigeo::find()->select('ID,Nombre')->where('len(ID) = 4 and SUBSTRING(ID, 1, 2)=:departamentoID',[':departamentoID'=>$departamentoID])->groupBy('ID,Nombre')->orderBy('Nombre')->all();
            if($countProvincias>0){
                echo "<option value>Seleccionar</option>";
                foreach($provincias as $provincia){
                    echo "<option value='".$provincia->ID."'>".$provincia->Nombre."</option>";
                }
            }
            else{
                echo "<option value>Seleccionar</option>";
            }
        }
    }

    //Cargar Distritos
    public function actionDistrito()
    {
        if(isset($_POST['provinciaID']) && trim($_POST['provinciaID'])!='')
        {
            $provinciaID=$_POST['provinciaID'];
            $countDistritos=Ubigeo::find()->select('ID,Nombre')->where('len(ID) = 6 and SUBSTRING(ID, 1, 4)=:provinciaID',[':provinciaID'=>$provinciaID])->groupBy('ID,Nombre')->count();
            $distritos=Ubigeo::find()->select('ID,Nombre')->where('len(ID) = 6 and SUBSTRING(ID, 1, 4)=:provinciaID',[':provinciaID'=>$provinciaID])->groupBy('ID,Nombre')->orderBy('Nombre')->all();
            if($countDistritos>0){
                echo "<option value>Seleccionar</option>";
                foreach($distritos as $distrito){
                    echo "<option value='".$distrito->ID."'>".$distrito->Nombre."</option>";
                }
            }
            else{
                echo "<option value>Seleccionar</option>";
            }
        }
    }
    public function actionAmbitoIntervencionCreate($informaciongeneralid=null)
    {
        $this->layout="vacio";
        $model=new AmbitoIntervencion;
        $departamentos=Ubigeo::find()->where('len(ID) = 2')->all();
        if ($model->load(Yii::$app->request->post())) {
            $model->InformacionGeneralID=$informaciongeneralid;
            $model->save();
            // return $this->redirect(['informacion-general/index']);
            $arr = array(
               'Success' => true
               );
            echo json_encode($arr);
        }else{
            return $this->render('ambito-intervencion-create',['model'=>$model,'departamentos'=>$departamentos]);
        }
    }
    
    public function actionEliminarAmbito(){
        $this->layout='vacio';
        $ambitoID = Yii::$app->request->post('ambitoID');
        if($ambitoID)
        {
            $ambitoIntervencion = AmbitoIntervencion::findOne($ambitoID);
            $sa = $ambitoIntervencion->delete();
            if($sa == 1){
                $arr = array(
                    'Success' => true
                );
            }else{
                $arr = array(
                    'Error' => "No se pudo eliminar"
                );
            }
            echo json_encode($arr);
        }
    }
    
    public function actionEnviarEvaluacion()
    {
        $this->layout='estandar';
        if(\Yii::$app->user->identity->rol!=7)
        {
            return $this->redirect(['panel/index']) ;
        }
        
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $model=new InformacionGeneral;
        
        if($model->load(Yii::$app->request->post()))
        {
            $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
            $poa->Situacion=2;
            $poa->update();
            $pat=Pat::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
            $pat->Situacion=2;
            $pat->update();
            $proyecto->Situacion=2;
            $proyecto->update();
            $updateTarea = Yii::$app->db->createCommand(' UPDATE Tarea SET Situacion=2
                                                    FROM Tarea
                                                    inner join Actividad on Actividad.ID=Tarea.ActividadID
                                                    inner join Componente on Componente.ID=Actividad.ComponenteID
                                                    inner join Poa on Poa.ID=Componente.PoaID
                                                    where Poa.ID='.$poa->ID);
            $updateTarea->execute();
            
            $updateTareaCronogr = Yii::$app->db->createCommand(' UPDATE CronogramaTarea SET Situacion=2
                                                    FROM CronogramaTarea
                                                    inner join Tarea on Tarea.ID=CronogramaTarea.TareaID
                                                    inner join Actividad on Actividad.ID=Tarea.ActividadID
                                                    inner join Componente on Componente.ID=Actividad.ComponenteID
                                                    inner join Poa on Poa.ID=Componente.PoaID
                                                    where Poa.ID='.$poa->ID);
            $updateTareaCronogr->execute();
            
            $updateTareaObservacion = Yii::$app->db->createCommand(' UPDATE TareaObservacion SET Estado=0
                                                    FROM TareaObservacion
                                                    inner join Tarea on Tarea.ID=TareaObservacion.TareaID
                                                    inner join Actividad on Actividad.ID=Tarea.ActividadID
                                                    inner join Componente on Componente.ID=Actividad.ComponenteID
                                                    inner join Poa on Poa.ID=Componente.PoaID
                                                    where TareaObservacion.Estado=1 and Poa.ID='.$poa->ID);
            $updateTareaObservacion->execute();
            
            
            $updateActividad= Yii::$app->db->createCommand(' UPDATE Actividad SET Situacion=1
                                                    FROM Actividad
                                                    inner join Componente on Componente.ID=Actividad.ComponenteID
                                                    inner join Poa on Poa.ID=Componente.PoaID
                                                    where Poa.ID='.$poa->ID);
            $updateActividad->execute();
            
            
            //UPDATE Actividad SET Situacion=1 WHERE ID IN (SELECT ID FROM Actividad WHERE ComponenteID IN (SELECT ID FROM Componente WHERE PoaID=20))
            // $updateTareaCrono = Yii::$app->db->createCommand(' UPDATE CronogramaTarea SET Estado=1
            //                                         FROM CronogramaTarea t
            //                                         INNER JOIN Tarea ON Tarea.ID = t.TareaID
            //                                         INNER JOIN Actividad ON Tarea.ActividadID = Actividad.ID
            //                                         INNER JOIN Componente ON Actividad.ComponenteID = Componente.ID
            //                                         INNER JOIN Poa ON Poa.ID = Componente.PoaID
            //                                         where Poa.ID='.$poa->ID);
            // $updateTareaCrono->execute();

            $updateRecurso = Yii::$app->db->createCommand(' UPDATE AreSubCategoria SET Situacion=2
                                                    FROM AreSubCategoria
                                                    inner join ActRubroElegible on ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID
                                                    inner join Actividad on Actividad.ID=ActRubroElegible.ActividadID
                                                    inner join Componente on Componente.ID=Actividad.ComponenteID
                                                    inner join Poa on Poa.ID=Componente.PoaID
                                                    where Actividad.Estado=1 and Componente.Estado=1 and Poa.ID='.$poa->ID);
            $updateRecurso->execute();
            
            $updateRecursoCrono = Yii::$app->db->createCommand(' UPDATE CronogramaAreSubCategoria SET Situacion=2
                                                   FROM CronogramaAreSubCategoria t
                                                   INNER JOIN AreSubCategoria ON AreSubCategoria.ID = t.AreSubCategoriaID
                                                    inner join ActRubroElegible on ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID
                                                    inner join Actividad on Actividad.ID=ActRubroElegible.ActividadID
                                                    inner join Componente on Componente.ID=Actividad.ComponenteID
                                                    inner join Poa on Poa.ID=Componente.PoaID
                                                    where Actividad.Estado=1 and Componente.Estado=1 and Poa.ID='.$poa->ID);
            $updateRecursoCrono->execute();


            // $updateRecursoCrono = Yii::$app->db->createCommand(' UPDATE CronogramaAreSubCategoria SET Situacion=1
            //                                         FROM CronogramaAreSubCategoria t
            //                                         INNER JOIN AreSubCategoria ON AreSubCategoria.ID = t.AreSubCategoriaID
            //                                         INNER JOIN ActRubroElegible ON ActRubroElegible.ID = AreSubCategoria.ActRubroElegibleID
            //                                         INNER JOIN RubroElegible ON RubroElegible.ID = ActRubroElegible.RubroElegibleID
            //                                         INNER JOIN Actividad ON ActRubroElegible.ActividadID = Actividad.ID
            //                                         INNER JOIN Componente ON Actividad.ComponenteID = Componente.ID
            //                                         INNER JOIN Poa ON Poa.ID = Componente.PoaID
            //                                         where Poa.ID='.$poa->ID);
            // $updateRecursoCrono->execute();


            $updateRecursoObservacion = Yii::$app->db->createCommand(' UPDATE AreSubCategoriaObservacion SET Estado=0
                                                    FROM AreSubCategoriaObservacion
                                                    inner join AreSubCategoria on AreSubCategoria.ID=AreSubCategoriaObservacion.AreSubCategoriaID
                                                    inner join ActRubroElegible on ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID
                                                    inner join Actividad on Actividad.ID=ActRubroElegible.ActividadID
                                                    inner join Componente on Componente.ID=Actividad.ComponenteID
                                                    inner join Poa on Poa.ID=Componente.PoaID
                                                    where AreSubCategoriaObservacion.Estado=1 and Poa.ID='.$poa->ID);
            $updateRecursoObservacion->execute();
            $nivelesAprobaciones=NivelAprobacion::find()->where('EtapaCodigo=:EtapaCodigo',[':EtapaCodigo'=>'01'])->orderBy('Correlativo desc')->all();
            $Seguimientos=Seguimiento::find()->where('Estado=1 and ProyectoCodigo=:ProyectoCodigo and EtapaCodigo=:EtapaCodigo',[':ProyectoCodigo'=>$informacionGeneral->Codigo,':EtapaCodigo'=>'01'])->all();
            
            if(!$Seguimientos){
                foreach($nivelesAprobaciones as $nivelAprobacion)
                {
                    $aprobacion=new Seguimiento();
                    $aprobacion->ProyectoCodigo=$informacionGeneral->Codigo;
                    $aprobacion->EtapaCodigo='01';
                    $aprobacion->NivelAprobacionCorrelativo=$nivelAprobacion->Correlativo;
                    $aprobacion->Situacion=0;
                    $aprobacion->Estado=1;
                    $aprobacion->save();
                }
            }
            else
            {
                foreach($Seguimientos as $Seguimiento)
                {
                    $Seguimiento->Situacion=0;
                    $Seguimiento->update();
                }
            }
            return $this->redirect(['informacion-general/index']);
        }
        return $this->render('enviar-evaluacion',['investigador'=>$investigador,'informacionGeneral'=>$informacionGeneral]);
    }
    
    public function actionValidarInformacion()
    {
        
        $recursos = Yii::$app->db->createCommand("
                                                 SELECT sum(AA.MetaFisica) Meta,sum(AA.CRONO) MetaCrono FROM (
                                                    SELECT MetaFisica,(SELECT sum(MetaFisica) FROM CronogramaAreSubCategoria WHERE AreSubCategoriaID=AreSubCategoria.ID) CRONO 
                                                    FROM AreSubCategoria WHERE ActRubroElegibleID IN (SELECT ID FROM ActRubroElegible WHERE ActividadID IN 
                                                    (SELECT ID FROM Actividad WHERE Estado=1 and ComponenteID IN (SELECT ID FROM Componente WHERE Estado=1 and PoaID IN (SELECT ID FROM Poa WHERE ProyectoID IN (SELECT ID FROM Proyecto WHERE InvestigadorID IN (SELECT InvestigadorID FROM InformacionGeneral WHERE Codigo='".$_REQUEST["CodigoProyecto"]."') ) ))))
                                                    ) AA
                                                 ")->queryOne();
        
       
        if((float)$recursos["Meta"]>(float)$recursos["MetaCrono"])
        {
            $arr = array('Error' => "No se puede enviar el poa debido a que falta mensualizar en recursos");
            echo json_encode($arr);
            die;
        }
        
        $tareas = Yii::$app->db->createCommand("
                                                 SELECT sum(AA.MetaFisica) Meta,sum(AA.CRONO) MetaCrono FROM (
                                                    SELECT MetaFisica,(SELECT sum(MetaFisica) FROM CronogramaTarea WHERE TareaID=Tarea.ID) CRONO 
                                                    FROM  Tarea WHERE ActividadID IN 
                                                    (SELECT ID FROM Actividad WHERE Estado=1 and ComponenteID IN (SELECT ID FROM Componente WHERE Estado=1 and PoaID IN (SELECT ID FROM Poa WHERE ProyectoID IN (SELECT ID FROM Proyecto WHERE InvestigadorID IN (SELECT InvestigadorID FROM InformacionGeneral WHERE Codigo='".$_REQUEST["CodigoProyecto"]."') ) )))
                                                    ) AA
                                                 ")->queryOne();
        
        if((float)$tareas["Meta"]>(float)$tareas["MetaCrono"])
        {
            $arr = array('Error' => "No se puede enviar el poa debido a que falta mensualizar en tareas");
            echo json_encode($arr);
            die;
        }
        
        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
    
    public function actionAprobarProyecto($aprobar=null,$codigo=null)
    {
        if($aprobar==1)
        {
            $usuario=Usuario::find()->where('username=:username',[':username'=>$codigo])->one();
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
            $poa->Situacion=1;
            $poa->update();
        }
    }
    
    public function actionAdjuntar($informaciongeneralid=null)
    {
        $this->layout='vacio';
        $model=InformacionGeneral::findOne($informaciongeneralid);
        if($model->load(Yii::$app->request->post()))
        {
            // print_r(UploadedFile::getInstance($model, 'Archivos'));
            // die();
            $model->Archivos = UploadedFile::getInstance($model, 'Archivos');
            
            if($model->Archivos)
            {
                $documento=new DocumentoInformacionGeneral;
                $documento->InformacionGeneralID=$model->ID;
                $documento->save();
                $documento->Documento=$model->Archivos->name;
                $documento->update();
                $model->Archivos->saveAs('informaciongeneral/' . $model->Archivos->name );
            }
            return $this->redirect(['/informacion-general']);
        }
        return $this->render('adjuntar',['model'=>$model]);
    }
    
    
    public function actionEliminarAdjunto(){
        $this->layout='vacio';
        $adjuntoID = Yii::$app->request->post('adjuntoID');
        if($adjuntoID)
        {
            $adjunto= DocumentoInformacionGeneral::findOne($adjuntoID);
            $adjunto->delete();
            //return $this->redirect(['index']);
            $arr = array(
                'Success' => true
                );
            echo json_encode($arr);
        }
    }

    public function actionDescargar($investigador = null){
        require_once(Yii::$app->basePath . '/vendor/autoload.php');
        require_once(Yii::$app->basePath . '/web/mpdf/src/Mpdf.php');
        
        if($investigador==NULL)
        {
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->ID])->one();
            $investiga = $investigador->ID;
        }
        else
        {
            $investigador=Investigador::findOne($investigador);
            $investiga = $investigador->ID;
        }

        $pasoCritico=PasoCritico::findOne($investiga);
        $proyecto=Proyecto::findOne($pasoCritico->ProyectoID);
        
        $resultado = (new \yii\db\Query())
                ->select('InformacionGeneral.Codigo,
                    InformacionGeneral.ID,
                    InformacionGeneral.TituloProyecto,
                    InformacionGeneral.InvestigadorID,
                    InformacionGeneral.Resumen,
                    InformacionGeneral.FechaInicioProyecto,
                    InformacionGeneral.FechaFinProyecto,
                    ProgramaTransversal.Nombre ProgramaTransversalNombre,
                    d.Nombre dNombre,
                    p.Nombre pNombre,
                    r.Nombre rNombre,
                    Proyecto.Fin,
                    Proyecto.Proposito,
                    DireccionLinea.Nombre DireccionLineaNombre,
                    TipoInvestigacion.Nombre TipoInvestigacionNombre, 
                    UnidadOperativa.Nombre UnidadOperativaNombre,
                    UnidadEjecutora.Nombre UnidadEjecutoraNombre,
                    Persona.Nombre IRPNombre,
                    Persona.ApellidoPaterno IRPApellidoPaterno,
                    Persona.ApellidoMaterno IRPApellidoMaterno,
                    Especie.Nombre EspecieNombre,
                    CultivoCrianza.Nombre CultivoCrianzaNombre,
                    Programa.Nombre ProgramaNombre,
                    InformacionGeneral.Meses,
                    EntidadParticipante.RazonSocial EntidadParticipanteRazonSocial,
                    EntidadParticipante.AporteMonetario EntidadParticipanteAporteMonetario,
                    EntidadParticipante.AporteNoMonetario EntidadParticipanteAporteNoMonetario,
                    TipoInstitucion.Nombre TipoColaboradora')
                ->from('InformacionGeneral')
                ->innerJoin('Investigador','Investigador.ID=InformacionGeneral.InvestigadorID')
                ->innerJoin('Proyecto','Proyecto.InvestigadorID=Investigador.ID')
                ->innerJoin('Usuario','Usuario.ID=Investigador.UsuarioID')
                ->innerJoin('Persona','Persona.ID=Usuario.PersonaID')
                ->innerJoin('ProgramaTransversal','ProgramaTransversal.ID=InformacionGeneral.ProgramaTransversalID')
                ->innerJoin('Ubigeo d','d.ID=InformacionGeneral.DistritoID')
                ->innerJoin('Ubigeo p','p.ID=substring(InformacionGeneral.DistritoID,1,4)')
                ->innerJoin('Ubigeo r','r.ID=substring(InformacionGeneral.DistritoID,1,2)')
                ->innerJoin('DireccionLinea','DireccionLinea.ID=InformacionGeneral.DireccionLineaID')
                ->innerJoin('TipoInvestigacion','TipoInvestigacion.ID=InformacionGeneral.TipoInvestigacionID')
                ->innerJoin('UnidadOperativa','UnidadOperativa.ID=InformacionGeneral.UnidadOperativaID')
                ->innerJoin('UnidadEjecutora','UnidadEjecutora.ID=UnidadOperativa.UnidadEjecutoraID')
                ->innerJoin('Especie','Especie.ID=InformacionGeneral.EspecieID')
                ->innerJoin('CultivoCrianza','CultivoCrianza.ID=Especie.CultivoCrianzaID')
                ->innerJoin('Programa','Programa.ID=CultivoCrianza.ProgramaID')
                ->innerJoin('EntidadParticipante','EntidadParticipante.InvestigadorID=InformacionGeneral.InvestigadorID')
                ->innerJoin('TipoInstitucion','TipoInstitucion.ID=EntidadParticipante.TipoInstitucionID')
                ->where('InformacionGeneral.InvestigadorID=:InvestigadorID',['InvestigadorID'=>$proyecto->InvestigadorID])
                ->one();
        


        $colaboradoras=EntidadParticipante::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$resultado['InvestigadorID']])->all();

        $ambitosIntervenciones=$this->AmbitoIntervencion($resultado['ID']);

        
        $mpdf=new \mPDF;
        $html.='<link href="/pip2/web/recursos/css/styles.css" rel="stylesheet">' ;
        $html.='<link href="/pip2/web/recursos/css/fonts.css" rel="stylesheet">' ;


        $html.='<style>#col table { border-collapse: collapse;} #col td, th { border: 2px solid black;margin:0px;padding: 10px;text-align: left;} .cab{border:1px solid black;background:#a8a8ab}.centr{margin-top: 8px;}.control-label{font-weight: bold;}.ancho{width: 18%;}</style>';
        
        $html.='<div class="col-md-12" align="center">INFORMACIÓN GENERAL</div>';
        $html.='<div class="clearfix"></div>';
        
        $html.='<div class="container">';
            $html.='<div id="page-heading"><h3>I. Información general del proyecto</h3></div>';
            
        $html.='<div class="form-horizontal">';

            $html.='<div class="form-group">';
                $html.='<label class="col-sm-3 control-label ancho">Código de proyecto:</label>';
                $html.='<div class="col-sm-1 centr">';
                    $html.=$resultado['Codigo'];
                $html.='</div>';

                $html.='<label class="col-sm-3 control-label">Investigador responsable:</label>';
                $html.='<div class="col-sm-5 centr">';
                    $html.= $resultado['IRPNombre'].' '.$resultado['IRPApellidoPaterno'].' '.$resultado['IRPApellidoMaterno'];
                $html.='</div>';
            $html.='</div>';
            
            $html.='<div class="form-group">';
                $html.='<label class="col-sm-3 control-label ancho">Título del proyecto:</label>';
                $html.='<div class="col-sm-9 text-justify">';
                    $html.=$resultado['TituloProyecto'];
                $html.='</div>';
            $html.='</div>';

            $html.='<div class="form-group">';
                $html.='<label class="col-sm-3 control-label" style="width: 20%;">Resumen del proyecto:</label>';
                $html.='<div class="col-sm-11 text-justify">';
                    $html.=$resultado['Resumen'];
                $html.='</div>';
            $html.='</div>';

            $html.='<div class="form-group">';
                $html.='<label class="col-sm-3 control-label ancho">Fin del proyecto:</label>';
                $html.='<div class="col-sm-9 text-justify">';
                    $html.=$resultado['Fin'];
                $html.='</div>';
            $html.='</div>';

            $html.='<div class="form-group">';
                $html.='<label class="col-sm-3 control-label ancho">Propósito del proyecto:</label>';
                $html.='<div class="col-sm-9 text-justify">';
                    $html.=$resultado['Proposito'];
                $html.='</div>';
            $html.='</div>';

            $html.='<div class="form-group">';
                $html.='<label class="col-sm-3 control-label ancho">Tipo Investigación:</label>';
                $html.='<div class="col-sm-9 text-justify centr">';
                    $html.=$resultado['TipoInvestigacionNombre'];
                $html.='</div>';
            $html.='</div>';

            $html.='<div class="form-group">';
                $html.='<label class="col-sm-3 control-label ancho">Fecha de inicio:</label>';
                $html.='<div class="col-sm-2 centr">';
                    $html.=Yii::$app->tools->dateFormat($resultado['FechaInicioProyecto']);
                $html.='</div>';

                $html.='<label class="col-sm-3 control-label">Fecha de fin:</label>';
                $html.='<div class="col-sm-2 centr">';
                    $html.= Yii::$app->tools->dateFormat($resultado['FechaFinProyecto']);
                $html.='</div>';
            $html.='</div>';

            $html.='<div class="form-group">';
                $html.='<label class="col-sm-3 control-label ancho">Duración del Proyecto:</label>';
                $html.='<div class="col-sm-9 text-justify centr">';
                    $html.=$resultado['Meses'].' Meses';
                $html.='</div>';
            $html.='</div>';
            
            $html.='<div id="page-heading"><h3>II. Programa</h3></div>';
            $html.='<div class="form-group">';
                $html.='<label class="col-sm-3 control-label ancho">Programa:</label>';
                $html.='<div class="col-sm-9 text-justify centr">';
                    $html.=$resultado['ProgramaNombre'];
                $html.='</div>';
            $html.='</div>';

            $html.='<div class="form-group">';
                $html.='<label class="col-sm-3 control-label ancho">Cultivo Crianza:</label>';
                $html.='<div class="col-sm-9 text-justify centr">';
                    $html.=$resultado['CultivoCrianzaNombre'];
                $html.='</div>';
            $html.='</div>';

            $html.='<div class="form-group">';
                $html.='<label class="col-sm-3 control-label ancho">Especie Nombre:</label>';
                $html.='<div class="col-sm-9 text-justify centr">';
                    $html.=$resultado['EspecieNombre'];
                $html.='</div>';
            $html.='</div>';

            $html.='<div class="form-group">';
                $html.='<label class="col-sm-3 control-label ancho">Programa Transversal:</label>';
                $html.='<div class="col-sm-9 text-justify centr">';
                    $html.=$resultado['ProgramaTransversalNombre'];
                $html.='</div>';
            $html.='</div>';

            $html.='<div id="page-heading"><h3>III. Dirección y Unidad Ejecutora</h3></div>';
            $html.='<div class="form-group">';
                $html.='<label class="col-sm-3 control-label ancho">Dirección de Línea:</label>';
                $html.='<div class="col-sm-9 text-justify centr">';
                    $html.=$resultado['DireccionLineaNombre'];
                $html.='</div>';
            $html.='</div>';

            $html.='<div class="form-group">';
                $html.='<label class="col-sm-3 control-label ancho">Unidad Ejecutora:</label>';
                $html.='<div class="col-sm-9 text-justify centr">';
                    $html.=$resultado['UnidadEjecutoraNombre'];
                $html.='</div>';
            $html.='</div>';

            $html.='<div class="form-group">';
                $html.='<label class="col-sm-3 control-label ancho">Unidad Operativa:</label>';
                $html.='<div class="col-sm-9 text-justify centr">';
                    $html.=$resultado['UnidadOperativaNombre'];
                $html.='</div>';
            $html.='</div>';

            $html.='<div id="page-heading"><h3>IV. Localización del proyecto</h3></div>';
            $html.='<div class="form-group">';
                $html.='<label class="col-sm-3 control-label ancho">Departamento:</label>';
                $html.='<div class="col-sm-9 text-justify centr">';
                    $html.=$resultado['rNombre'];
                $html.='</div>';
            $html.='</div>';

            $html.='<div class="form-group">';
                $html.='<label class="col-sm-3 control-label ancho">Provincia:</label>';
                $html.='<div class="col-sm-9 text-justify centr">';
                    $html.=$resultado['pNombre'];
                $html.='</div>';
            $html.='</div>';

            $html.='<div class="form-group">';
                $html.='<label class="col-sm-3 control-label ancho">Distrito:</label>';
                $html.='<div class="col-sm-9 text-justify centr">';
                    $html.=$resultado['dNombre'];
                $html.='</div>';
            $html.='</div>';

            //-- 
            
        $html.='</div>';

            $html.='<div id="page-heading"><h3>VI. Ámbito de intervención</h3></div>';
            $html.='<table style="font-size:10px;" class="table">';
                $html.='<tr>';
                    $html.='<td width="20%" align="left" class="cab">Departamento</td>';
                    $html.='<td width="15%" class="cab">Provincia</td>';
                    $html.='<td width="15%" class="cab"> Distrito</td>';
                $html.='</tr>';
                $monetario=0;
                $nomonetario=0;
                foreach($ambitosIntervenciones as $intervencion)
                {
                    $html.='<tr>';
                        $html.='<td width="20%" align="left" style="border:1px solid black;">'.$intervencion->Departamento.'</td>';
                        $html.='<td width="15%" style="border:1px solid black;"> '.$intervencion->Provincia.'</td>';
                        $html.='<td width="15%" style="border:1px solid black;"> '.$intervencion->Distrito.'</td>';
                    $html.='</tr>';
                }
            $html.='</table>';

            $html.='<div id="page-heading"><h3>V. Colaboradores</h3></div>';
            $html.='<table style="font-size:10px;" class="table">';
                $html.='<tr>';
                    $html.='<td width="20%" align="left" style="border:1px solid black;background:#a8a8ab"></td>';
                    $html.='<td width="15%" class="cab"> Monetario</td>';
                    $html.='<td width="15%" class="cab"> No Monetario</td>';
                    $html.='<td width="15%" class="cab"> Total</td>';
                $html.='</tr>';
                $monetario=0;
                $nomonetario=0;
                foreach($colaboradoras as $colaboradora)
                {
                    $html.='<tr>';
                        $html.='<td width="20%" align="left" style="border:1px solid black;">'.$colaboradora->RazonSocial.'</td>';
                        $html.='<td width="15%" style="border:1px solid black;"> '.number_format($colaboradora->AporteMonetario,2,'.',' ').'</td>';
                        $html.='<td width="15%" style="border:1px solid black;"> '.number_format($colaboradora->AporteNoMonetario,2,'.',' ').'</td>';
                        $html.='<td width="15%" style="border:1px solid black;"> '.number_format(($colaboradora->AporteMonetario+$colaboradora->AporteNoMonetario),2,'.',' ').'</td>';
                    $html.='</tr>';
                    $monetario=$monetario+$colaboradora->AporteMonetario;
                    $nomonetario=$nomonetario+$colaboradora->AporteNoMonetario;
                }
                $html.='<tr>';
                    $html.='<td width="20%" align="left" style="border:1px solid black;">Total</td>';
                    $html.='<td width="15%" style="border:1px solid black;"> '.number_format($monetario,2,'.',' ').'</td>';
                    $html.='<td width="15%" style="border:1px solid black;"> '.$nomonetario.'</td>';
                    $html.='<td width="15%" style="border:1px solid black;"> '.number_format(($monetario+$nomonetario),2,'.',' ').'</td>';
                $html.='</tr>';
            $html.='</table>';

        $html.='</div>';
        

        // echo $html;
        // die();
        $mpdf->WriteHTML($html);
        $mpdf->Output('InformacionGeneral.pdf','I');
        die;
    }
}