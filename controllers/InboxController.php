<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\InformacionGeneral;
use app\models\ProgramaTransversal;
use app\models\DireccionLinea;
use app\models\TipoInvestigacion;
use app\models\UnidadEjecutora;
use app\models\UnidadOperativa;
use app\models\Persona;
use app\models\Programa;
use app\models\CultivoCrianza;
use app\models\Especie;
use app\models\Ubigeo;
use app\models\Usuario;
use app\models\Investigador;
use app\models\EntidadParticipante;
use app\models\TipoInstitucion;
use app\models\AmbitoIntervencion;
use app\models\MiembroEquipoGestion;
use app\models\MarcoLogicoFinProyecto;
use app\models\MarcoLogicoPropositoProyecto;
use app\models\Proyecto;
use app\models\Poa;
use app\models\MarcoLogicoActividad;
use app\models\Actividad;
use app\models\Componente;
use app\models\MarcoLogicoComponente;
use app\models\Inbox;
use yii\db\Query;

class InboxController extends Controller
{
    /**
     * @inheritdoc
     */
    

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($investigador=null)
    {
        $this->layout='estandar';
	    $usuario=Usuario::findOne(\Yii::$app->user->id);
        $inboxs=Inbox::find()->where('CodigoProyecto=:CodigoProyecto',[':CodigoProyecto'=>$usuario->username])->all();
        return $this->render('index',['inboxs'=>$inboxs]);
    }
}