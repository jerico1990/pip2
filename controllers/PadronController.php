<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\InformacionGeneral;
use app\models\ProgramaTransversal;
use app\models\DireccionLinea;
use app\models\TipoInvestigacion;
use app\models\UnidadEjecutora;
use app\models\UnidadOperativa;
use app\models\Investigador;
use app\models\EntidadParticipante;
use app\models\TipoInstitucion;
use app\models\RepresentanteLegal;
use app\models\Persona;
use app\models\Usuario;
use app\models\Padron;
class PadronController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout='estandar';
        return $this->render('index');
    }
    
    public function actionListaBeneficiarios()
    {
        $resultados = (new \yii\db\Query())
            ->select(['Padron.ID','Padron.Nombres','Padron.ApellidoPaterno','Padron.ApellidoMaterno','Padron.DNI'])
            ->from('Padron')
            ->innerJoin('InformacionGeneral','Padron.Codigo=InformacionGeneral.Codigo')
             ->innerJoin('Investigador','InformacionGeneral.InvestigadorID=Investigador.ID')
            ->innerJoin('Usuario','Usuario.ID=Investigador.UsuarioID')
            ->innerJoin('Proyecto','Proyecto.InvestigadorID=Investigador.ID')
            //->orderBy('Situacion desc','username','TituloProyecto','Meses') 
            ->distinct()
            ->all();
        $nro=0;
        foreach($resultados as $result)
        {
            $nro++;
            echo "<tr>";
            echo "<td>" . $result["nombres"] . "</td>";
	    echo "<td>" . mb_substr ($result["ApellidoPaterno"],0,100) . "</td>";
            echo "<td>" . $result["ApellidoMaterno"] . "</td>";
            echo "<td>". $result["DNI"] ."</td>";
            echo "<td>".$result["ID"]."</td>";
            echo "</tr>";
        }
    }
}

