<?php

namespace app\controllers;

use Yii;
use app\models\UsuarioRol;
use app\models\UsuarioRolSearch;
use app\models\Usuario;
use app\models\Rol;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
/**
 * UsuarioRolController implements the CRUD actions for UsuarioRol model.
 */
class UsuarioRolController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UsuarioRol models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout='estandar';
        $searchModel = new UsuarioRolSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UsuarioRol model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $this->layout='estandar';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $this->layout='estandar';
        $usuarios=Usuario::find()->all();
        $roles=Rol::find()->all();
        $model = new UsuarioRol();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/usuario-rol']);
        }
    }


    public function actionUpdate($id)
    {
        $this->layout='estandar';
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/usuario-rol']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionEliminar($id)
    {
        $this->findModel($id)->delete();
        $json = array('Success' => true );
        echo json_encode($json);
    }


    protected function findModel($id)
    {
        if (($model = UsuarioRol::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionListaUsuario()
    {
        $resultados = (new \yii\db\Query())
            ->select('UsuarioRol.*,Usuario.username,Persona.ApellidoPaterno,Persona.ApellidoMaterno,Persona.Nombre,Rol.Nombre RolNombre')
            ->from('UsuarioRol')
            ->innerJoin('Usuario','Usuario.ID = UsuarioRol.UsuarioID')
            ->innerJoin('Persona','Usuario.PersonaID = Persona.ID')
            ->innerJoin('Rol','Rol.ID = UsuarioRol.RolID')
            ->orderBy('UsuarioRol.ID desc')
            ->all();
        $nro=0;
        foreach($resultados as $result)
        {
            $nro++;
            $color='';
            echo "<tr >";
                echo "<td> " . $result["ID"] . "</td>";
                echo "<td> " . $result["username"] . "</td>";
                echo "<td> " . $result["ApellidoPaterno"].' '.$result["ApellidoMaterno"].' '.$result["Nombre"] . "</td>";
                echo "<td> " . $result["RolNombre"] . "</td>";
                echo "<td><a href='#' class='btn btn-primary btn-edit' data-id='".$result["ID"]."'><i class='fa fa-edit fa-lg'></i></a> <a href='#' class='btn btn-danger btn-remove' data-id='".$result["ID"]."'><i class='fa fa-remove fa-lg'></i></a> </td>";
            echo "</tr>";
        }
    }

    public function actionCrear($id= null){
        $this->layout='vacio';
        $rol = Rol::find()->all();
        $usuario = Usuario::find()->all();
        if(!is_null($id)){
            $model = UsuarioRol::find()->where('ID=:ID',[':ID'=>$id])->one();
        }else{
            $model = array();
        }
        return $this->render('create',['usuario'=>$usuario,'model'=>$model,'rol'=>$rol]);

    }


}
