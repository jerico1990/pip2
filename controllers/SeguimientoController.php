<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\CompromisoPresupuestal;
use app\models\Requerimiento;
use app\models\DetalleCompromisoPresupuestal;
use app\models\TipoGasto;
use app\models\SeguimientoRequerimiento;


class SeguimientoController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionIndex()
    {
        $this->layout='estandar';
     	return $this->render('index');   
    }

    public function actionVer( $ID = null)
    { 
        $val = Yii::$app->tools->SeguimientoCompromiso($ID);

        $this->layout='estandar';
        $cabecera = CompromisoPresupuestal::find()->where(['ID'=>$ID])->one();
        $requerimiento=Requerimiento::findOne($cabecera->RequerimientoID);
        $dcompromiso=DetalleCompromisoPresupuestal::find()->where(["CompromisoPresupuestalID"=>$ID])->all();

        $requerimiento=Requerimiento::find()
        ->select('Requerimiento.*,TipoGasto.Nombre')
        ->innerJoin('TipoGasto','TipoGasto.ID=Requerimiento.TipoRequerimiento')
        ->where('Requerimiento.ID=:ID',[':ID'=>$cabecera->RequerimientoID])
        ->one();

        $seguimiento = SeguimientoRequerimiento::find()->where('SeguimientoRequerimiento.RequerimientoID=:ID',[':ID'=>$cabecera->RequerimientoID])->one();

     	return $this->render('ver',['cabecera'=>$cabecera,'requerimiento'=>$requerimiento,'seguimiento'=>$seguimiento,'CodigoProyecto'=>$cabecera->Codigo,'ID'=>$ID,'segui'=>$val]);

    }


}
