<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\PasoCritico;
use app\models\Usuario;
use app\models\Persona;
use app\models\Investigador;
use app\models\Proyecto;
use app\models\InformacionGeneral;
use app\models\Poa;
use app\models\Componente;
use app\models\Actividad;
use app\models\CronogramaProyecto;
use app\models\ActRubroElegible;
use app\models\CronogramaComponente;
use app\models\CronogramaActividad;
use app\models\CronogramaActRubroElegible;
use app\models\RubroElegible;
use app\models\AreSubCategoria;
use app\models\CronogramaAreSubCategoria;
use app\models\MarcoLogicoActividad;
use app\models\AreSubCategoriaObservacion;
use app\models\SolicitudCompromisoPresupuestal;
use app\models\DetalleSolicitudCompromisoPresupuestal;

use yii\web\UploadedFile;


class PagoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($CodigoProyecto=NULL)
    {
        $this->layout='estandar';
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        return $this->render('index',['CodigoProyecto'=>$CodigoProyecto]);
    }
    
    public function actionListaCompromisos($CodigoProyecto=null)
    {
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        
        
        $resultados = (new \yii\db\Query())
            ->select('SolicitudCompromisoPresupuestal.*')
            ->from('InformacionGeneral')
            ->innerJoin('Investigador','InformacionGeneral.InvestigadorID=Investigador.ID')
            ->innerJoin('Usuario','Usuario.ID=Investigador.UsuarioID')
            ->innerJoin('SolicitudCompromisoPresupuestal','SolicitudCompromisoPresupuestal.Codigo=InformacionGeneral.Codigo')
            ->where(['InformacionGeneral.Codigo'=>$CodigoProyecto])
            ->distinct()
            ->all();

        $nro=0;
        foreach($resultados as $result)
        {
            $nro++;
            echo "<tr>";
            echo "<td>" . $result["Memorando"] . "</td>";
            echo "<td>" . mb_substr ($result["FechaRegistro"],0,100) . "</td>";
            echo "<td>" . $result["MontoTotal"] . "</td>";
            echo "<td><a target='_blank' href='compromisopresupuestal/" . $result["Documento"] . "'><span>descargar</span></a></td>";
            echo "<td></td>";
            echo "</tr>";
        }
    }
    
    public function actionCrear($CodigoProyecto)
    {
        $this->layout='vacio';
        $informacionGeneral=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacionGeneral->InvestigadorID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
        $componentes=Componente::find()->where('PoaID=:PoaID',[':PoaID'=>$poa->ID])->all();
        
        $compromisoPresupuestal=new SolicitudCompromisoPresupuestal;
        if($compromisoPresupuestal->load(Yii::$app->request->post())){
            $compromisoPresupuestal->FechaRegistro=date('Ymd');
            $compromisoPresupuestal->Estado=1;
            $compromisoPresupuestal->save();
            
            if(!$compromisoPresupuestal->Proveedores)
            {
                $countProveedores=0;
            }
            else
            {
                $countProveedores=count(array_filter($compromisoPresupuestal->Proveedores));
            }
            
            for($i=0;$i<$countProveedores;$i++)
            {
                if(isset($countProveedores->Proveedores[$i]))
                {
                    //$farmers=Farmers::model()->find('id=:id',array(':id'=>$model->farmers_ids[$i]));
                    //$farmers->form_id=$model->id;
                    //$farmers->name=$model->farmers_nombres[$i];
                    //$farmers->document_number=$model->farmers_dnis[$i];
                    //$farmers->update();
                }
                else
                {
                    $detalle=new DetalleSolicitudCompromisoPresupuestal;
                    $detalle->SolicitudCompromisoPresupuestalID=$compromisoPresupuestal->ID;
                    $detalle->Proveedor=$compromisoPresupuestal->Proveedores[$i];
                    $detalle->RUC=$compromisoPresupuestal->RUCs[$i];
                    $detalle->Monto=$compromisoPresupuestal->Montos[$i];
                    $detalle->AreSubCategoriaID=$compromisoPresupuestal->Recursos[$i];
                    
                    $detalle->save();
                    $compromisoPresupuestal->MontoTotal=$compromisoPresupuestal->MontoTotal+$detalle->Monto;
                }
            }
            
            $compromisoPresupuestal->archivo = UploadedFile::getInstance($compromisoPresupuestal, 'archivo');
            
            if($compromisoPresupuestal->archivo)
            {
                $compromisoPresupuestal->archivo->saveAs('compromisopresupuestal/' . $compromisoPresupuestal->ID . '.' . $compromisoPresupuestal->archivo->extension);
                $compromisoPresupuestal->Documento=$compromisoPresupuestal->ID . '.' . $compromisoPresupuestal->archivo->extension;
            }
            $compromisoPresupuestal->update();
            return $this->redirect(['/compromiso-presupuestal']);
            
        }
        return $this->render('_form',['CodigoProyecto'=>$CodigoProyecto,'componentes'=>$componentes]);
    }
    
}
