<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\PasoCritico;
use app\models\Usuario;
use app\models\Persona;
use app\models\Investigador;
use app\models\Proyecto;
use app\models\InformacionGeneral;
use app\models\Poa;
use app\models\Componente;
use app\models\Actividad;
use app\models\CronogramaProyecto;
use app\models\ActRubroElegible;
use app\models\CronogramaComponente;
use app\models\CronogramaActividad;
use app\models\CronogramaActRubroElegible;
use app\models\RubroElegible;
use app\models\AreSubCategoria;
use app\models\CronogramaAreSubCategoria;
use app\models\MarcoLogicoActividad;
use app\models\AreSubCategoriaObservacion;
use app\models\CertificacionPresupuestal;
use app\models\DetalleCertificacionPresupuestal;
use app\models\UnidadOperativa;
use app\models\SiafSecuencialProyecto;
use app\models\CajaChica;
use app\models\Orden;
use app\models\Siaf;
use app\models\Requerimiento;
use app\models\UnidadEjecutora;
use app\models\RendicionEncargo;
use app\models\DetalleRendicionEncargo;
use app\models\Encargo;
use app\models\DetalleRendicionEncargoDeclaracion;

use PhpOffice\PhpWord\PhpWord;
use yii\web\UploadedFile;


class RendicionEncargoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($CodigoProyecto=null)
    {
        $this->layout='estandar';
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        return $this->render('index',['CodigoProyecto'=>$CodigoProyecto]);
    }
    
    
    public function actionLista($CodigoProyecto=null){
        
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        
        $resultados = (new \yii\db\Query())
            ->select('RendicionEncargo.*,Encargo.NombresEncargo,Encargo.ApellidosEncargo')
            ->from('RendicionEncargo')
            ->innerJoin('Encargo','Encargo.ID=RendicionEncargo.EncargoID')
            ->where(['RendicionEncargo.Estado'=>1,'RendicionEncargo.CodigoProyecto'=>$CodigoProyecto])
            ->distinct()
            ->all();

        $nro=0;
        foreach($resultados as $result)
        {
            $nro++;
            if($result["Estado"]==1 )
            {
            echo "<tr>";
            echo "<td style='display:none'>" . $result["ID"] . "</td>";
            echo "<td>Rendición N° " . str_pad($result["Correlativo"], 3, "0", STR_PAD_LEFT)  ."-".$result["Annio"]."</td>";
            echo "<td>" . date('d-m-Y',strtotime($result["FechaRegistro"]))  . "</td>";
            echo "<td>".$result["NombresEncargo"]." ".$result["ApellidosEncargo"]."</td>";
            echo "<td>" . $result["Total"] . "</td>";
            echo "<td>" . $this->getSituacion($result["Situacion"]) . "</td>";
            echo "<td><a target='_blank' href='rendicion-encargo/plantilla?ID=" . $result["ID"] . "'><span class='fa fa-cloud-download'></span></a>  </td>";
            if($result["Situacion"]==2)
            {
                echo "<td></td>";
            }
            else
            {
                echo "<td><a href='#' class='btn-edit-rendicion-encargo' data-id='".$result["ID"]."'><i class='fa fa-edit fa-lg'></i></a><a href='#' class='btn-remove' data-id='".$result["ID"]."'><i class='fa fa-remove fa-lg'></i></a> </td>";    
            }
               echo "</tr>";
            }
           
            
           
        }
        
    }
    
   /* public function actionCrear($CodigoProyecto=null)
    {
        $this->layout='vacio';
        return $this->render('_form',['CodigoProyecto'=>$CodigoProyecto]);
    }*/

    public function actionCrear($CodigoProyecto=null,$RequerimientoID=null)
    {
        $this->layout='vacio';
        $rendicionEncargo=new RendicionEncargo;
        $encargos=Encargo::find()->where('CodigoProyecto=:CodigoProyecto and Estado=1 and Situacion=3 and ID not in (select EncargoID from RendicionEncargo where Estado=1)',[':CodigoProyecto'=>$CodigoProyecto])->all();
       
        if($rendicionEncargo->load(Yii::$app->request->post())){
            /*
            $rendicionCajaAnt=RendicionCajaChica::find()->where('CodigoProyecto=:CodigoProyecto and Annio=:Annio and Situacion=1 and Estado=1',[':CodigoProyecto'=>$cajaChica->CodigoProyecto,':Annio'=>date('Y')])->one();
            if($rendicionCajaAnt)
            {
                \Yii::$app->getSession()->setFlash('error', '<div class="alert alert-dismissable alert-danger"><strong>Errores:</strong><ul> <li>Aún tiene un rendición de caja chica activa, debe finalizar para poder generar otra rendición.</li> </ul></div>');
                return $this->redirect(['/rendicion-caja-chica']);
            }
                */
            $rendicionEncargo->Situacion=1;
            $rendicionEncargo->Estado=1;
            $rendicionEncargo->FechaRegistro=date('Ymd');
            $rendicionEncargo->Annio=date('Y');
            $rendicionEncargo->Correlativo=$this->CorrelativoRendicionEncargo($CodigoProyecto);
            $rendicionEncargo->save();
            
            if(!$rendicionEncargo->NumerosDocumentos)
            {
                $countDetalles=0;
            }
            else
            {
                $countDetalles=count(array_filter($rendicionEncargo->NumerosDocumentos));
            }
            $total=0;
            for($i=0;$i<$countDetalles;$i++)
            {
                $detalle=new DetalleRendicionEncargo;
                $detalle->RendicionEncargoID=$rendicionEncargo->ID;
                $detalle->FechaDocumento= Yii::$app->tools->dateFormat($rendicionEncargo->Fechas[$i],'d/m/Y');
                $detalle->ClaseDocumento=$rendicionEncargo->Clases[$i];
                $detalle->TipoDocumento=$rendicionEncargo->TiposDocumentos[$i];
                $detalle->Tipo=$rendicionEncargo->Tipos[$i];
                $detalle->NumeroDocumento=$rendicionEncargo->NumerosDocumentos[$i];
                $detalle->RUC=$rendicionEncargo->Rucs[$i];
                $detalle->Proveedor=$rendicionEncargo->Proveedores[$i];
                $detalle->DetalleGasto=$rendicionEncargo->DetallesGastos[$i];
                $detalle->Importe= str_replace(',','', $rendicionEncargo->Importes[$i]);
                $detalle->save();
                $total=$total+$rendicionEncargo->Importes[$i];
            }
            
            if(!$rendicionEncargo->DeclaracionDetalles)
            {
                $countDetallesDeclaraciones=0;
            }
            else
            {
                $countDetallesDeclaraciones=count(array_filter($rendicionEncargo->DeclaracionDetalles));
            }
            
            for($i=0;$i<$countDetallesDeclaraciones;$i++)
            {
                $detalle=new DetalleRendicionEncargoDeclaracion;
                $detalle->RendicionEncargoID=$rendicionEncargo->ID;
                $detalle->Fecha=$rendicionEncargo->DeclaracionFechas[$i];
                $detalle->Detalle=$rendicionEncargo->DeclaracionDetalles[$i];
                $detalle->Importe=$rendicionEncargo->DeclaracionImportes[$i];
                $detalle->save();
            }
            
            $rendicionEncargo->Total=$total;
            $rendicionEncargo->update();
            return $this->redirect(['/rendicion-encargo']);
        }

        return $this->render('_form',['CodigoProyecto'=>$CodigoProyecto,'rendicionEncargo'=>$rendicionEncargo,'encargos'=>$encargos]);
    }

    public function actionActualizar($ID=null)
    {
        $this->layout='vacio';
        $rendicionEncargo=RendicionEncargo::findOne($ID);
        $encar=Encargo::findOne($rendicionEncargo->EncargoID);
        $encargos=Encargo::find()->where('CodigoProyecto=:CodigoProyecto and Estado=1 and Situacion=3',[':CodigoProyecto'=>$rendicionEncargo->CodigoProyecto])->all();
        
        $declaracionDetalles=DetalleRendicionEncargoDeclaracion::find()->where('RendicionEncargoID=:RendicionEncargoID',[':RendicionEncargoID'=>$ID])->all();
        $detalles=DetalleRendicionEncargo::find()->where('RendicionEncargoID=:RendicionEncargoID',[':RendicionEncargoID'=>$ID])->all();
        if($rendicionEncargo->load(Yii::$app->request->post())){
            
            $rendicionEncargo->save();
            if(!$rendicionEncargo->NumerosDocumentos)
            {
                $countDetalles=0;
            }
            else
            {
                $countDetalles=count(array_filter($rendicionEncargo->NumerosDocumentos));
            }
            $total=0;
            for($i=0;$i<$countDetalles;$i++)
            {
                if(isset($rendicionEncargo->IDs[$i]))
                {
                    $detalle=DetalleRendicionEncargo::findOne($rendicionEncargo->IDs[$i]);
                    // $detalle->FechaDocumento=$rendicionEncargo->Fechas[$i];
                    $detalle->FechaDocumento= Yii::$app->tools->dateFormat($rendicionEncargo->Fechas[$i],'d/m/Y');
                    $detalle->ClaseDocumento=$rendicionEncargo->Clases[$i];
                    $detalle->TipoDocumento=$rendicionEncargo->TiposDocumentos[$i];
                    $detalle->Tipo=$rendicionEncargo->Tipos[$i];
                    $detalle->NumeroDocumento=$rendicionEncargo->NumerosDocumentos[$i];
                    $detalle->RUC=$rendicionEncargo->Rucs[$i];
                    $detalle->Proveedor=$rendicionEncargo->Proveedores[$i];
                    $detalle->DetalleGasto=$rendicionEncargo->DetallesGastos[$i];
                    $detalle->Importe=$rendicionEncargo->Importes[$i];
                    $detalle->save();
                }
                else{
                    $detalle=new DetalleRendicionEncargo;
                    $detalle->RendicionEncargoID=$rendicionEncargo->ID;
                    // $detalle->FechaDocumento=$rendicionEncargo->Fechas[$i];
                    $detalle->FechaDocumento= Yii::$app->tools->dateFormat($rendicionEncargo->Fechas[$i],'d/m/Y');
                    $detalle->ClaseDocumento=$rendicionEncargo->Clases[$i];
                    $detalle->TipoDocumento=$rendicionEncargo->TiposDocumentos[$i];
                    $detalle->Tipo=$rendicionEncargo->Tipos[$i];
                    $detalle->NumeroDocumento=$rendicionEncargo->NumerosDocumentos[$i];
                    $detalle->RUC=$rendicionEncargo->Rucs[$i];
                    $detalle->Proveedor=$rendicionEncargo->Proveedores[$i];
                    $detalle->DetalleGasto=$rendicionEncargo->DetallesGastos[$i];
                    $detalle->Importe=$rendicionEncargo->Importes[$i];
                    $detalle->save();
                }
                $total=$total+$rendicionEncargo->Importes[$i];
            }
            
            if(!$rendicionEncargo->DeclaracionDetalles)
            {
                $countDetallesDeclaraciones=0;
            }
            else
            {
                $countDetallesDeclaraciones=count(array_filter($rendicionEncargo->DeclaracionDetalles));
            }
            
            for($i=0;$i<$countDetallesDeclaraciones;$i++)
            {
                if(isset($rendicionEncargo->IDDeclaracions[$i]))
                {
                    $detalle=DetalleRendicionEncargoDeclaracion::findOne($rendicionEncargo->IDDeclaracions[$i]);
                    $detalle->Fecha=$rendicionEncargo->DeclaracionFechas[$i];
                    $detalle->Detalle=$rendicionEncargo->DeclaracionDetalles[$i];
                    $detalle->Importe=$rendicionEncargo->DeclaracionImportes[$i];
                    $detalle->save();
                }
                else
                {
                    $detalle=new DetalleRendicionEncargoDeclaracion;
                    $detalle->RendicionEncargoID=$rendicionEncargo->ID;
                    $detalle->Fecha=$rendicionEncargo->DeclaracionFechas[$i];
                    $detalle->Detalle=$rendicionEncargo->DeclaracionDetalles[$i];
                    $detalle->Importe=$rendicionEncargo->DeclaracionImportes[$i];
                    $detalle->save();
                }
            }
            
            $rendicionEncargo->Total=$total;
            if($rendicionEncargo->Y01==2)
            {
                $rendicionEncargo->Situacion=2;
            }
            $rendicionEncargo->update();
            return $this->redirect(['/rendicion-encargo']);
        }
        return $this->render('_form_actualizar',['rendicionEncargo'=>$rendicionEncargo,'ID'=>$ID,'detalles'=>$detalles,'encargos'=>$encargos,'encar'=>$encar,'declaracionDetalles'=>$declaracionDetalles]);
    }

    public function actionEliminar($id=null)
    {
        $rendicioncajaChica=RendicionEncargo::findOne($id);
        $rendicioncajaChica->Situacion=6;
        $rendicioncajaChica->Estado=0; 
        $rendicioncajaChica->update();

        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
    public function actionEliminarDetalle()
    {
        if(isset($_POST['id']) && $_POST['id']!='')
        {
            $id=$_POST['id'];
            $rendicionencargo=DetalleRendicionEncargo::findOne($id);
            $rendicionencargo->delete();
            $arr = array('Success' => true);
            echo json_encode($arr);
        }
    }
    
     public function actionEliminarDetalleDeclaracion()
    {
        if(isset($_POST['id']) && $_POST['id']!='')
        {
            $id=$_POST['id'];
            $rendicionencargoDecla=DetalleRendicionEncargoDeclaracion::findOne($id);
            $rendicionencargoDecla->delete();
            $arr = array('Success' => true);
            echo json_encode($arr);
        }
    }

    public function actionEnviar($ID=null)
    {
        $requerimiento=CajaChica::findOne($_POST["ID"]);
        
        $requerimiento->Situacion=2;
        $requerimiento->update();

        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
     public function actionActualizarRequerimientoListado()
    {
        $this->layout='vacio';
        $requerimiento=CajaChica::findOne($_POST['ID']);
        $requerimiento->Situacion =6;
        $requerimiento->update();
        $arr = array('Success' => true);
        echo json_encode($arr);
    }

    public function CorrelativoRendicionEncargo($CodigoProyecto)
    {
        $rendicion=RendicionEncargo::find()->where('CodigoProyecto=:CodigoProyecto',[':CodigoProyecto'=>$CodigoProyecto])->orderBy('Correlativo desc')->one();
        if($rendicion)
        {
            return $rendicion->Correlativo+1;
        }
        else
        {
            return 1;
        }
    }

 public function actionExcel($CodigoProyecto=null)
    {
        $resultados = (new \yii\db\Query())
            ->select('CajaChica.FechaRegistro')
            ->from('CajaChica')
            ->where(['CajaChica.CodigoProyecto'=>$CodigoProyecto])
            ->distinct()
            ->all();
        $requerimiento=new CajaChica;
        return $this->render('excel',['resultados'=>$resultados,'requerimiento'=>$requerimiento]);
    }




     public function getSituacion($situacion=null)
    {
        if($situacion==1){
            return "Pendiente";
        }
        elseif($situacion==2)
        {
            return "En Proceso";
        }
         elseif($situacion==3)
        {
            return "Aprobado";
        }
        else if($situacion==4)
        {
            return "Observado";
        }
        else if($situacion==5)
        {
            return "Cierre";
        }
        else if($situacion==6)
        {
            return "Anulado";
        }
    }

    public function actionPlantilla($ID=null)
    {
        $rendicionEncargo=RendicionEncargo::findOne($ID);
        $encargo=Encargo::find()->where('Estado=1 and ID=:ID',[':ID'=>$rendicionEncargo->EncargoID])->one();
        $informacion=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$rendicionEncargo->CodigoProyecto])->one();
        $eea=UnidadOperativa::find()->where('ID=:ID',[':ID'=>$informacion->UnidadOperativaID])->one();
        $ejecutora=UnidadEjecutora::find()->where('ID=:ID',[':ID'=>$eea->UnidadEjecutoraID])->one();
        $usuario=Usuario::find()->where('username=:username',[':username'=>$rendicionEncargo->CodigoProyecto])->one();
        $persona=Persona::find()->where('ID=:ID',[':ID'=>$usuario->PersonaID])->one();
       
        $countDetalles=DetalleRendicionEncargo::find()
                    ->Select('DetalleRendicionEncargo.*')
                    ->where('RendicionEncargoID=:RendicionEncargoID',[':RendicionEncargoID'=>$ID])
                    ->count();
        $detalles=DetalleRendicionEncargo::find()
                    ->Select('DetalleRendicionEncargo.*')
                    ->where('RendicionEncargoID=:RendicionEncargoID',[':RendicionEncargoID'=>$ID])
                    ->all();
        $countDetallesDecl=DetalleRendicionEncargoDeclaracion::find()
                    ->Select('DetalleRendicionEncargoDeclaracion.*')
                    ->where('RendicionEncargoID=:RendicionEncargoID',[':RendicionEncargoID'=>$ID])
                    ->count();
        $detallesDecl=DetalleRendicionEncargoDeclaracion::find()
                    ->Select('DetalleRendicionEncargoDeclaracion.*')
                    ->where('RendicionEncargoID=:RendicionEncargoID',[':RendicionEncargoID'=>$ID])
                    ->all();
        require_once(Yii::$app->basePath . '/web/PHPWord/src/PhpWord/Autoloader.php');     
        \PhpOffice\PhpWord\Autoloader::register();
        $phpWord = new PhpWord();
       
        $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/documentos/PLANTILLA_RENDICION_ENCARGO.docx');
        //$template->setValue('ID', str_pad($orden->Correlativo, 3, "0", STR_PAD_LEFT)."-".$orden->Annio);
        $template->setValue('EEA',$eea->Nombre);
        $template->setValue('CORRELATIVO', "Rendición N° " . str_pad($rendicionEncargo->Correlativo, 3, "0", STR_PAD_LEFT)  ."-".$rendicionEncargo->Annio );
        $template->setValue('CODIGOPROYECTO', $rendicionEncargo->CodigoProyecto);
        $template->setValue('NOMBRECOMPLETO', $encargo->NombresEncargo." ".$encargo->ApellidosEncargo);
        $template->setValue('MOTIVOENCARGO', $rendicionEncargo->InformeMotivoEncargo);
        $template->setValue('DETALLEACTIVIDADES', $rendicionEncargo->InformeDetallarActividadesEncargo);
        $template->setValue('DETALLELOGROS', $rendicionEncargo->InformeDetallarLogrosEncargo);
        $template->setValue('FECHA', date('d-m-Y',strtotime($rendicionEncargo->FechaRegistro)));
        $template->cloneRow('F', $countDetalles);
        $countDetalles=1;
        $total=0;
        $bienes=0;
        $servicios=0;
        foreach($detalles as $detalle)
        {
            
            $template->setValue('NU#'.$countDetalles,$countDetalles);
            $template->setValue('F#'.$countDetalles.'', "".date('d/m/Y',strtotime($detalle->FechaDocumento))."");
            $template->setValue('C#'.$countDetalles.'', "".$detalle->ClaseDocumento."");
            $template->setValue('N#'.$countDetalles.'', "".$detalle->NumeroDocumento."");
            $template->setValue('P#'.$countDetalles.'', "".$detalle->Proveedor."");
            $template->setValue('D#'.$countDetalles.'', "".$detalle->DetalleGasto."");
            $template->setValue('I#'.$countDetalles.'', "".number_format($detalle->Importe, 2, '.', ' ')."");
            $total=$total+$detalle->Importe;
            //$template->setValue('DIRECTIVONOMBRE#'.$countdirectivos.'', "".ucwords(strtolower($registroparametros->getNombresWord($directivo->persona_id)))."");
            $countDetalles++;
            if($detalle->Tipo==1)
            {
                $bienes=$bienes+$detalle->Importe;
            }
            elseif($detalle->Tipo==2){
                $servicios=$servicios+$detalle->Importe;
            }
        }
        $template->setValue('TOTAL', number_format($total, 2, '.', ' '));
        $template->setValue('RBIENES', number_format($bienes, 2, '.', ' '));
        $template->setValue('RSERVICIOS', number_format($servicios, 2, '.', ' '));
        
        $template->setValue('IMPORTEENCARGO', number_format($encargo->Total, 2, '.', ' '));
        $template->setValue('IMPORTEDEVOLVER', number_format(($encargo->Total-$total), 2, '.', ' '));
        
        $template->setValue('TOTALRENDIDO', number_format($total, 2, '.', ' '));
        $template->cloneRow('N', $countDetallesDecl);
        $countDetallesDecl=1;
        $totalDe=0;
        foreach($detallesDecl as $detalleDecl)
        {
            $template->setValue('N#'.$countDetallesDecl.'', "".$countDetallesDecl."");
            $template->setValue('FECHADECL#'.$countDetallesDecl.'', "".date('d/m/Y',strtotime($detalleDecl->Fecha))."");
            $template->setValue('DETALLEDECL#'.$countDetallesDecl.'', "".$detalleDecl->Detalle."");
            $template->setValue('IMPORTEDECL#'.$countDetallesDecl.'', "".number_format($detalleDecl->Importe, 2, '.', ' ')."");
            $totalDe=$totalDe+$detalleDecl->Importe;
            //$template->setValue('DIRECTIVONOMBRE#'.$countdirectivos.'', "".ucwords(strtolower($registroparametros->getNombresWord($directivo->persona_id)))."");
            $countDetallesDecl++;
            
        }
        $template->setValue('TOTALIMPORTE', number_format($totalDe, 2, '.', ' '));
        $template->setValue('DNI', $encargo->DNIEncargo);
        $template->setValue('FECHAENCARGO', date('d-m-Y',strtotime($encargo->FechaInicio)));
        $template->setValue('ACTIVIDAD',$rendicionEncargo->InformeDetallarActividadesEncargo);
        
        $temp_file = tempnam(sys_get_temp_dir(), 'PHPWord');
        $template->saveAs($temp_file);
        header("Content-Disposition: attachment; filename='RendicionEncargo.docx'");
        readfile($temp_file); // or echo file_get_contents($temp_file);
        unlink($temp_file);  // remove temp file
        return true;    
    }
    
    public function actionAdjuntarDocumento($ID=null){
        $this->layout='vacio';
        $cajaChica=CajaChica::findOne($ID);
        $Requerimiento=Requerimiento::findOne($cajaChica->RequerimientoID);
        if($cajaChica->load(Yii::$app->request->post())){
            
            $cajaChica->archivo = UploadedFile::getInstance($cajaChica, 'archivo');
            if($cajaChica->archivo)
            {
                $cajaChica->archivo->saveAs('ordenServicioFactura/' . $cajaChica->ID . '.' . $cajaChica->archivo->extension);
                $cajaChica->Documento=$cajaChica->ID . '.' . $cajaChica->archivo->extension;
            }
            $cajaChica->update();
            return $this->redirect(['requerimiento/generar','ID'=>$Requerimiento->ID]);
        }
        return $this->render('adjuntar-documento',['ID'=>$ID,'cajaChica'=>$cajaChica]);
    }
    public function actionEliminarAdjunto($id=null)
    {
        $cajaChica=CajaChica::findOne($id);
        $cajaChica->Documento='';
        $cajaChica->update();
       
        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
    public function actionApertura($CodigoProyecto=null)
    {
        $this->layout='estandar';
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        return $this->render('apertura',['CodigoProyecto'=>$CodigoProyecto]);
    }
    
    /*
    public function actionListaApertura($CodigoProyecto=null){
        
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        
        $resultados = (new \yii\db\Query())
            ->select('AperturaCajaChica.*')
            ->from('AperturaCajaChica')
            ->where(['AperturaCajaChica.CodigoProyecto'=>$CodigoProyecto])
            ->distinct()
            ->all();

        $nro=0;
        foreach($resultados as $result)
        {
            $nro++;
            if($result["Estado"]==1 )
            {
            echo "<tr>";
            echo "<td style='display:none'>" . $result["ID"] . "</td>";
            echo "<td>" . $result["Correlativo"] . "</td>";
            echo "<td>" . date('d/m/Y',strtotime($result["FechaRegistro"]))  . "</td>";
            echo "<td>" . $result["Descripcion"] . "</td>";
            echo "<td>" . $result["Bienes"] . "</td>";
            echo "<td>" . $result["Servicios"] . "</td>";
            echo "<td>" .( $result["Bienes"] + $result["Servicios"]). "</td>";
            echo "<td>" . $this->getSituacion($result["Situacion"]) . "</td>";
            echo "<td><a target='_blank' href='cajachica/R" . $result["ResolucionDirectorial"] . "'><span class='fa fa-cloud-download'></span></a></td>";
            echo "</tr>";
            }
        }
    }
    
    
    public function actionCrearApertura($CodigoProyecto=null,$RequerimientoID=null)
    {
        $this->layout='vacio';
        //$terminosReferencias=TerminoReferencia::find()->where('CodigoProyecto=:CodigoProyecto and Situacion=0',[':CodigoProyecto'=>$CodigoProyecto])->all();
        $aperturaCaja=new AperturaCajaChica;
        $requerimiento=Requerimiento::findOne($RequerimientoID);
        if($aperturaCaja->load(Yii::$app->request->post())){
            
            $requerimientoCaja->Situacion=1;
            $requerimientoCaja->Estado=1;
            $requerimientoCaja->FechaRegistro=date('Ymd');
            $requerimientoCaja->Correlativo=$this->CorrelativoRequerimiento($CodigoProyecto);
          


            $requerimientoCaja->save();
            $requerimientoCaja->archivo = UploadedFile::getInstance($requerimientoCaja, 'archivo');
            
            if($requerimientoCaja->archivo)
            {
                $requerimientoCaja->archivo->saveAs('cajachica/R' . $requerimientoCaja->ID . '.' . $requerimientoCaja->archivo->extension);
                $requerimientoCaja->ResolucionDirectorial='R'.$requerimientoCaja->ID . '.' . $requerimientoCaja->archivo->extension;
            }
            $requerimientoCaja->update();
            
           
            
            return $this->redirect(['requerimiento/generar','ID'=>$RequerimientoID]);
        }

        return $this->render('_form_apertura',['CodigoProyecto'=>$CodigoProyecto,'requerimiento'=>$requerimiento,'RequerimientoID'=>$RequerimientoID]);
    }

    public function actionActualizarApertura($ID=null)
    {
        $this->layout='vacio';
        $requerimiento=CajaChica::findOne($ID);

        if($requerimiento->load(Yii::$app->request->post())){

            $requerimiento->save();
            $requerimiento->archivo = UploadedFile::getInstance($requerimiento, 'archivo');

            if($requerimiento->archivo)
            {
                $requerimiento->archivo->saveAs('cajachica/R' . $requerimiento->ID . '.' . $requerimiento->archivo->extension);
                $requerimiento->ResolucionDirectorial='R'.$requerimiento->ID . '.' . $requerimiento->archivo->extension;
            }
            $requerimiento->update();
            return $this->redirect(['requerimiento/generar','ID'=>$RequerimientoID]);
        }
        return $this->render('_form_actualizar_apertura',['requerimiento'=>$requerimiento,'ID'=>$ID]);
    }

    public function actionEliminarApertura($id=null)
    {
        $cajaChica=AperturaCajaChica::findOne($id);
        $cajaChica->Estado=0; 
        $cajaChica->update();

        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    */
    public function getClases($clase)
    {
        if($clase==1)
        {
            return "FACTURA";
        }
        elseif($clase==2)
        {
            return "RECIBO POR HONORARIOS";
        }
        elseif($clase==3)
        {
            return "BOLETA DE VENTA";
        }
        elseif($clase==4)
        {
            return "NOTA DE CREDITO";
        }
        elseif($clase==5)
        {
            return "NOTA DE DEBITO";
        }
        elseif($clase==6)
        {
            return "BOLETO DE VIAJE INTERPROVINCIAL";
        }
        elseif($clase==7)
        {
            return "VOUCHER";
        }
        elseif($clase==8)
        {
            return "TICKET";
        }
        elseif($clase==9)
        {
            return "COMPROBANTE DE GASTO";
        }
        elseif($clase==10)
        {
            return "DECLARACION JURADA";
        }
    }
    
    public function actionDatos($valor=null)
    {
        $encargo=Encargo::findOne($valor);
        $arr = array('Success' => true,'Bienes'=>$encargo->Bienes,'Servicios'=>$encargo->Servicios);
        echo json_encode($arr);
    }
    
}
