<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\PasoCritico;
use app\models\Usuario;
use app\models\Persona;
use app\models\Investigador;
use app\models\Proyecto;
use app\models\InformacionGeneral;
use app\models\Poa;
use app\models\Componente;
use app\models\Actividad;
use app\models\CronogramaProyecto;
use app\models\ActRubroElegible;
use app\models\CronogramaComponente;
use app\models\CronogramaActividad;
use app\models\CronogramaActRubroElegible;
use app\models\RubroElegible;
use app\models\AreSubCategoria;
use app\models\CronogramaAreSubCategoria;
use app\models\MarcoLogicoActividad;
use app\models\AreSubCategoriaObservacion;
use app\models\CertificacionPresupuestal;
use app\models\DetalleCertificacionPresupuestal;
use app\models\UnidadOperativa;
use app\models\SiafSecuencialProyecto;
use app\models\Orden;
use app\models\Siaf;
use app\models\Siaft;

use PhpOffice\PhpWord\PhpWord;
use yii\web\UploadedFile;


class CertificacionPresupuestalController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout='estandar';
        
        return $this->render('index');
    }
    
    public function actionListaCertificaciones()
    {
        $resultados = (new \yii\db\Query())
            ->select('CertificacionPresupuestal.*')
            ->from('CertificacionPresupuestal')
            ->distinct()
            ->all();
        if(\Yii::$app->user->identity->rol==2){  
            $nro=0;
            foreach($resultados as $result)
            {
                $nro++;
                echo "<tr>";
                if($result["Situacion"]==2){
                    echo "<td><a target='_blank' href='certificacion-presupuestal/plantilla?ID=" . $result["ID"] . "'><span class='fa fa-cloud-download'></span></a></td>";
                }
                else
                {
                    echo "<td></td>";
                }
                echo "<td>" . $this->getSituacion($result["Situacion"]) . "</td>";
                echo "<td>" . $result["Memorando"] . "</td>";
                echo "<td>" . $result["PasoCriticoID"] . "</td>";
                echo "<td>" . $result["MontoTotal"] . "</td>";
                echo "<td><a target='_blank' href='certificacionpresupuestal/" . $result["Documento"] . "'><span class='fa fa-cloud-download'></span></a></td>";
                if($result["Situacion"]==2){
                    echo "<td><a href='#' class='btn-aprobar' data-id='".$result["ID"]."'><i class='fa fa-check fa-lg'></i></a> <a href='#' class='btn-edit' data-id='".$result["ID"]."'><i class='fa fa-edit fa-lg'></i></a> <a href='#' class='btn-remove' data-id='".$result["ID"]."'><i class='fa fa-remove fa-lg'></i></a></td>";
                }
                else
                {
                    echo "<td></td>";
                }
                echo "</tr>";
            }
        }
        else if(\Yii::$app->user->identity->rol==3)
        {
            $nro=0;
            foreach($resultados as $result)
            {
                $nro++;
                echo "<tr>";
                echo "<td>" . $this->getSituacion($result["Situacion"]) . "</td>";
                echo "<td>" . $result["Memorando"] . "</td>";
                echo "<td>" . $result["PasoCriticoID"] . "</td>";
                echo "<td>" . $result["MontoTotal"] . "</td>";
                echo "<td><a target='_blank' href='certificacionpresupuestal/" . $result["Documento"] . "'><span class='fa fa-cloud-download'></span></a></td>";
                if($result["Situacion"]==3){
                    echo "<td><a href='#' class='btn-edit-certificados' data-id='".$result["ID"]."'><i class='fa fa-edit fa-lg'></i></a> <a href='#' class='btn-confirmar-certificados' data-id='".$result["ID"]."'><i class='fa fa-check fa-lg'></i></a>  </td>";
                }
                else
                {
                    echo "<td></td>";
                }
                echo "</tr>";
            }
        }
    }
    
    public function actionCrear()
    {
        $this->layout='vacio';
        $certificacionPresupuestal=new CertificacionPresupuestal;
        if($certificacionPresupuestal->load(Yii::$app->request->post())){
            $certificacionPresupuestal->FechaRegistro=date('Ymd');
            $certificacionPresupuestal->Estado=1;
            $certificacionPresupuestal->Situacion=2;
            $certificacionPresupuestal->save();
            
            if(!$certificacionPresupuestal->Codigos)
            {
                $countCodigos=0;
            }
            else
            {
                $countCodigos=count(array_filter($certificacionPresupuestal->Codigos));
            }
            
            for($i=0;$i<$countCodigos;$i++)
            {
                if(isset($certificacionPresupuestal->Codigos[$i]))
                {
                    //$farmers=Farmers::model()->find('id=:id',array(':id'=>$model->farmers_ids[$i]));
                    //$farmers->form_id=$model->id;
                    //$farmers->name=$model->farmers_nombres[$i];
                    //$farmers->document_number=$model->farmers_dnis[$i];
                    //$farmers->update();
                }
                else
                {
                    $detalle=new DetalleCertificacionPresupuestal;
                    $detalle->CertificacionPresupuestalID=$certificacionPresupuestal->ID;
                    $detalle->Codigo=$certificacionPresupuestal->Codigos[$i];
                    $detalle->Monto=$certificacionPresupuestal->Montos[$i];
                    $detalle->save();
                    $certificacionPresupuestal->MontoTotal=$certificacionPresupuestal->MontoTotal+$detalle->Monto;
                }
            }
            /*
            $certificacionPresupuestal->archivo = UploadedFile::getInstance($certificacionPresupuestal, 'archivo');
            
            if($compromisoPresupuestal->archivo)
            {
                $compromisoPresupuestal->archivo->saveAs('compromisopresupuestal/' . $compromisoPresupuestal->ID . '.' . $compromisoPresupuestal->archivo->extension);
                $compromisoPresupuestal->Documento=$compromisoPresupuestal->ID . '.' . $compromisoPresupuestal->archivo->extension;
            }
            $compromisoPresupuestal->update();*/
            $certificacionPresupuestal->update();
            return $this->redirect(['/certificacion-presupuestal']);
            
        }
        return $this->render('_form');
    }
    public function actionActualizar($ID=null)
    {
        $this->layout='vacio';
        $certificacion=CertificacionPresupuestal::findOne($ID);
        $detallesCertificaciones=DetalleCertificacionPresupuestal::find()->where('CertificacionPresupuestalID=:CertificacionPresupuestalID',[':CertificacionPresupuestalID'=>$certificacion->ID])->all();
        if($certificacion->load(Yii::$app->request->post())){
            if(!$certificacion->Codigos)
            {
                $countCodigos=0;
            }
            else
            {
                $countCodigos=count(array_filter($certificacion->Codigos));
            }
            
            for($i=0;$i<$countCodigos;$i++)
            {
                if(isset($certificacion->IDs[$i]))
                {
                    $detalle=DetalleCertificacionPresupuestal::findOne($certificacion->IDs[$i]);
                    //$detalle->CertificacionPresupuestalID=$certificacionPresupuestal->ID;
                    $detalle->Codigo=$certificacion->Codigos[$i];
                    $detalle->Monto=$certificacion->Montos[$i];
                    $detalle->update();
                }
                else
                {
                    $detalle=new DetalleCertificacionPresupuestal;
                    $detalle->CertificacionPresupuestalID=$certificacion->ID;
                    $detalle->Codigo=$certificacion->Codigos[$i];
                    $detalle->Monto=$certificacion->Montos[$i];
                    $detalle->save();
                }
                $certificacion->MontoTotal=$certificacion->MontoTotal+$detalle->Monto;
            }
            
            $certificacion->archivo = UploadedFile::getInstance($certificacion, 'archivo');
            if($certificacion->archivo)
            {
                $certificacion->archivo->saveAs('certificacionpresupuestal/' . $certificacion->ID . '.' . $certificacion->archivo->extension);
                $certificacion->Documento=$certificacion->ID . '.' . $certificacion->archivo->extension;
            }
            $certificacion->update();
            return $this->redirect(['/certificacion-presupuestal']);
        }
        
        
        return $this->render('_form_actualizar',['certificacion'=>$certificacion,'detallesCertificaciones'=>$detallesCertificaciones,'ID'=>$ID]);
    }
    
    public function actionActualizarCertificacion($ID=null)
    {
        $this->layout='vacio';
        $certificacion=CertificacionPresupuestal::findOne($ID);
        $detallesCertificaciones=DetalleCertificacionPresupuestal::find()->where('CertificacionPresupuestalID=:CertificacionPresupuestalID',[':CertificacionPresupuestalID'=>$certificacion->ID])->all();
        if($certificacion->load(Yii::$app->request->post())){
            if(!$certificacion->Certificaciones)
            {
                $countCodigosCertificaciones=0;
            }
            else
            {
                $countCodigosCertificaciones=count(array_filter($certificacion->Certificaciones));
            }
            
            for($i=0;$i<$countCodigosCertificaciones;$i++)
            {
                if(isset($certificacion->IDs[$i]))
                {
                    $detalle=DetalleCertificacionPresupuestal::findOne($certificacion->IDs[$i]);
                    $detalle->CodigoCertificacion=$certificacion->Certificaciones[$i];
                    $detalle->update();
                }
                $certificacion->MontoTotal=$certificacion->MontoTotal+$detalle->Monto;
            }
            
            return $this->redirect(['/certificacion-presupuestal']);
        }
        return $this->render('_form_actualizar_certificacion',['certificacion'=>$certificacion,'detallesCertificaciones'=>$detallesCertificaciones,'ID'=>$ID]);
    }
    
    
    public function actionPlantilla($ID=null)
    {
        $certificacion=CertificacionPresupuestal::findOne($ID);
        $detalles=DetalleCertificacionPresupuestal::find()
                    ->Select('DetalleCertificacionPresupuestal.*')
                    ->where('CertificacionPresupuestalID=:CertificacionPresupuestalID',[':CertificacionPresupuestalID'=>$ID])
                    ->all();
        $countProyectos=0;
        foreach($detalles as $detalle)
        {         
           $countProyectos++;
        }
      
        require_once(Yii::$app->basePath . '/web/PHPWord/src/PhpWord/Autoloader.php');     
        \PhpOffice\PhpWord\Autoloader::register();
        $phpWord = new PhpWord();
        $template = $phpWord->loadTemplate(Yii::$app->basePath . '/web/documentos/PLANTILLA_CERTIFICACION_PRESUPUESTAL.docx');
        $template->setValue('MEMORANDO', $certificacion->Memorando);
        $template->setValue('JEFEPNIA', $certificacion->JefePnia);
        $template->setValue('ASUNTO', $certificacion->Asunto);
        $template->setValue('REFERENCIA', $certificacion->Referencia);
        $template->setValue('DESCRIPCION', $certificacion->Descripcion);
        $template->setValue('META', $certificacion->MetaPresupuestal);
        $template->setValue('CLASIFICACION', $certificacion->Clasificacion);
        $template->setValue('ANNIO', $certificacion->Annio);
        $template->cloneRow('N', $countProyectos);
        $countProyectos=1;
        foreach($detalles as $detalle)
        {
            $informacion=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$detalle->Codigo])->one();
            $eea=UnidadOperativa::find()->where('ID=:ID',[':ID'=>$informacion->UnidadOperativaID])->one();
            if($informacion)
            {
                $template->setValue('N#'.$countProyectos.'', "".ucwords($countProyectos)."");
                $template->setValue('TITULO#'.$countProyectos.'', "".ucwords($informacion->TituloProyecto)."");
                $template->setValue('CODIGO#'.$countProyectos.'', "".ucwords($informacion->Codigo)."");
                $template->setValue('EEA#'.$countProyectos.'', "".ucwords($eea->Nombre)."");
                $template->setValue('MONTO#'.$countProyectos.'', "".ucwords($detalle->Monto)."");
                //$template->setValue('DIRECTIVONOMBRE#'.$countdirectivos.'', "".ucwords(strtolower($registroparametros->getNombresWord($directivo->persona_id)))."");
                $countProyectos++;
            }
            
        }
      
        $temp_file = tempnam(sys_get_temp_dir(), 'PHPWord');
        $template->saveAs($temp_file);
        header("Content-Disposition: attachment; filename='a.docx'");
        readfile($temp_file); // or echo file_get_contents($temp_file);
        unlink($temp_file);  // remove temp file
        return true;    
    }
    
    public function actionEliminar($id=null){
        
        DetalleCertificacionPresupuestal::deleteAll(['CertificacionPresupuestalID' => $id]);
        CertificacionPresupuestal::findOne($id)->delete();
        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
    public function actionEnviarCertificacionPresupuestal($id)
    {
        $certificacion=CertificacionPresupuestal::findOne($id);
        $certificacion->Situacion=3;
        $certificacion->update();
        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
    public function actionAprobarCertificacionPresupuestal($id)
    {
        $certificacion=CertificacionPresupuestal::findOne($id);
        $certificacion->Situacion=1;
        $certificacion->update();
        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
    public function getSituacion($situacion)
    {
        $contenido="";
        if(\Yii::$app->user->identity->rol==2)
        {
            if($situacion==2)
            {
                $contenido="Pendiente de enviar";
            }
            elseif($situacion==3)
            {
                $contenido="Enviado";
            }
            elseif($situacion==1)
            {
                $contenido="Aprobado";
            }
        }
        elseif(\Yii::$app->user->identity->rol==3)
        {
            if($situacion==2)
            {
                $contenido="Pendiente de recibir";
            }
            elseif($situacion==3)
            {
                $contenido="Pendiente de aprobar";
            }
            elseif($situacion==1)
            {
                $contenido="Aprobado";
            }
        }
        
        return $contenido;
    }
    
    public function Con2($nombrestore,$arraydatos=null)
    {	
        $usuario = "SIGA01";
        $clave = "SIGA01";
        $bd = '(DESCRIPTION=( ADDRESS_LIST= (ADDRESS= (PROTOCOL=TCP) (HOST=172.168.3.12) (PORT=1521)))( CONNECT_DATA=(SID=XE) ))';
       
        if (function_exists('oci_connect')) {
            $conec =  oci_connect($usuario, $clave, $bd);
        } else {
            echo "Las funciones de oci_connect no están disponibles.<br />\n";
        }
        
        if (!$conec) {
            echo 'Error en conecci&oacute;n a la BASE DE DATOS oracle...<br>';
        }else{
            $stid = oci_parse ($conec, "alter session set nls_date_format = 'dd/mm/yyyy'");
            oci_execute ($stid);
        }
        
        
        $stid = oci_parse($conec, 'BEGIN SIAF_INTERFASE.TRANSFERIR_DATOS_CERT_PIP2 (:l_coddoc, :l_numdoc, :l_fecdoc, :l_anio, :l_concepto, :l_tipoid,:l_ruc,:l_fuente,:l_tcambio,:l_siafcer,:l_siafsec,:l_siafcor,:l_intfcer,:l_intfsec,:l_intfcor,:l_tipofinan,:l_correlativotabla,:l_meta,:l_montototal,:l_clasificador, :l_secuencial); END;');
        
        $data="";
        $intf=NULL;
        $intf1=NULL;
        $intf2=NULL;
        //var_dump($siaf->SiafCertificado);die;
        oci_bind_by_name($stid, ':l_coddoc', $arraydatos[0][1]);
        oci_bind_by_name($stid, ':l_numdoc', $arraydatos[1][1]);
        oci_bind_by_name($stid, ':l_fecdoc', $arraydatos[2][1]);
        oci_bind_by_name($stid, ':l_anio', $arraydatos[3][1]);
        oci_bind_by_name($stid, ':l_concepto', $arraydatos[4][1]);
        oci_bind_by_name($stid, ':l_tipoid', $arraydatos[5][1]);
        oci_bind_by_name($stid, ':l_ruc', $arraydatos[6][1]);
        oci_bind_by_name($stid, ':l_fuente', $arraydatos[7][1]);
        oci_bind_by_name($stid, ':l_tcambio', $arraydatos[8][1]);
        oci_bind_by_name($stid, ':l_siafcer', $arraydatos[9][1]);
        oci_bind_by_name($stid, ':l_siafsec', $arraydatos[10][1]);
        oci_bind_by_name($stid, ':l_siafcor', $arraydatos[11][1]);
        oci_bind_by_name($stid, ':l_intfcer', $arraydatos[12][1]);
        oci_bind_by_name($stid, ':l_intfsec', $arraydatos[13][1]);
        oci_bind_by_name($stid, ':l_intfcor', $arraydatos[14][1]);
        oci_bind_by_name($stid, ':l_tipofinan', $arraydatos[15][1]);
        oci_bind_by_name($stid, ':l_correlativotabla', $arraydatos[16][1]);
        oci_bind_by_name($stid, ':l_meta', $arraydatos[17][1]);
        oci_bind_by_name($stid, ':l_montototal', $arraydatos[18][1]);
        oci_bind_by_name($stid, ':l_clasificador', $arraydatos[19][1]);
        oci_bind_by_name($stid, ':l_secuencial',$data,50);
        oci_execute($stid);
        oci_free_statement($stid);
        oci_close($conec);
        return 	$data;
    }
    
    
    public function actionGenerarCompromisoAnual()
    {
        
        if(isset($_POST["CodigoProyecto"]) && isset($_POST["ID"]))
        {
            $CodigoProyecto=$_POST["CodigoProyecto"];
            $ID=$_POST["ID"];
            
            $detalles = (new \yii\db\Query())
                ->select('*,Orden.Correlativo OrdenCorrelativo,Orden.ID OrdenID')
                ->from('CompromisoPresupuestal')
                ->innerJoin('DetalleCompromisoPresupuestal','DetalleCompromisoPresupuestal.CompromisoPresupuestalID=CompromisoPresupuestal.ID')
                ->innerJoin('Orden','Orden.ID=DetalleCompromisoPresupuestal.Correlativo')
                ->where('CompromisoPresupuestal.ID=:ID',[':ID'=>$ID])
                ->distinct()
                ->all();
            
            $certificado=DetalleCertificacionPresupuestal::find()
                        ->innerJoin('CertificacionPresupuestal','CertificacionPresupuestal.ID=DetalleCertificacionPresupuestal.CertificacionPresupuestalID')
                        ->where('DetalleCertificacionPresupuestal.Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])
                        ->one();
            
            $siafSecuencial=SiafSecuencialProyecto::find()
                                            ->where('CodigoProyecto=:CodigoProyecto',[':CodigoProyecto'=>$CodigoProyecto])
                                            ->one();
                                            
            
            $contenido='';
            foreach($detalles as $detalle)
            {

                // echo '<pre>';
                
                $confirm = $this->SiaftInternoSeguimiento($detalle,'C');
                $confirm1 = $this->SiaftInternoSeguimiento($detalle,'D');
                if($confirm == 1 and $confirm2 == 1){
                    echo 'No se inserto para el devengado ';
                    die();
                }


                $orden=Orden::findOne($detalle["OrdenID"]);
                $siaf=new Siaf;
                $siaf->SiafCertificado=str_pad($certificado->CodigoCertificacion, 10, "0", STR_PAD_LEFT);
                $siaf->InterfaceCertificado=str_pad($siaf->InterfaceCertificado, 10, "0", STR_PAD_LEFT);
                if($detalle["TipoGasto"]==2)
                {
                    $siaf->CodigoDocumento="032";
                    $siaf->TipoID=1;
                    $siaf->NumeroDocumento=str_pad($detalle["OrdenCorrelativo"], 3, "0", STR_PAD_LEFT)." ".$detalle["Annio"];
                    $siaf->FechaDocumento=date('d/m/Y',strtotime($detalle["FechaOrden"]));
                    $siaf->AnioDocumento=$detalle["Annio"];
                    $siaf->ConceptoDocumento=substr ( $detalle["Concepto"] , 0 ,249 );
                    $siaf->RUCDocumento=$detalle["RUC"];
                    $siaf->TipoCambioDocumento=$detalle["TipoCambio"];
                    $siaf->MontoTotal=$detalle["Monto"];
                    $siaf->Clasificador='26 7 1 6 3';
                }
                if($detalle["TipoGasto"]==3)
                {
                    $siaf->CodigoDocumento="031";
                    $siaf->TipoID=1;
                    $siaf->NumeroDocumento=str_pad($detalle["OrdenCorrelativo"], 3, "0", STR_PAD_LEFT)." ".$detalle["Annio"];
                    $siaf->FechaDocumento=date('d/m/Y',strtotime($detalle["FechaOrden"]));
                    $siaf->AnioDocumento=$detalle["Annio"];
                    $siaf->ConceptoDocumento=substr ( $detalle["Concepto"] , 0 ,249 );
                    $siaf->RUCDocumento=$detalle["RUC"];
                    $siaf->TipoCambioDocumento=$detalle["TipoCambio"];
                    $siaf->MontoTotal=$detalle["Monto"];
                    $siaf->Clasificador='26 7 1 6 2';
                }//Caja chica 942   //Viatico 043// Encargo 951
                $siaf->SiafSecuencial=str_pad($siafSecuencial->Secuencial, 4, "0", STR_PAD_LEFT);
                $siaf->Correlativo=str_pad($detalle["OrdenCorrelativo"], 12, "0", STR_PAD_LEFT);
                //var_dump($siaf->Correlativo);die;
                //$siaf->InterfaceCertificado=$siaf->SiafCertificado;
                $siaf->InterfaceSecuencial=$siaf->SiafCorrelativo;
                
                $ns = 'SIAF_INTERFASE.TRANSFERIR_DATOS_CERT_PIP2';
                $psest[] = array(':l_coddoc',$siaf->CodigoDocumento);
                $psest[] = array(':l_numdoc',$siaf->NumeroDocumento);
                $psest[] = array(':l_fecdoc',$siaf->FechaDocumento);
                $psest[] = array(':l_anio',$siaf->AnioDocumento);
                $psest[] = array(':l_concepto',$siaf->ConceptoDocumento);
                $psest[] = array(':l_tipoid',$siaf->TipoID);
                $psest[] = array(':l_ruc',$siaf->RUCDocumento);
                $psest[] = array(':l_fuente',$siaf->FuenteFinanciamiento);
                $psest[] = array(':l_tcambio',$siaf->TipoCambioDocumento);
                $psest[] = array(':l_siafcer',$siaf->SiafCertificado);
                $psest[] = array(':l_siafsec',$siaf->SiafSecuencial);
                $psest[] = array(':l_siafcor',$siaf->SiafCorrelativo);
                $psest[] = array(':l_intfcer',$siaf->SiafCertificado);
                $psest[] = array(':l_intfsec',$siaf->InterfaceSecuencial);
                $psest[] = array(':l_intfcor','0001');
                $psest[] = array(':l_tipofinan',$siaf->TipoFinanciamiento);
                $psest[] = array(':l_correlativotabla',$siaf->Correlativo);
                $psest[] = array(':l_meta',$siaf->MetaFinanciera);
                $psest[] = array(':l_montototal',$siaf->MontoTotal);
                $psest[] = array(':l_clasificador',$siaf->Clasificador);
                    
                $datosestaciones = $this->Con2($ns,$psest);
                $orden->SIAF=$datosestaciones;
                // Cambiando estado
                $orden->SituacionSIAFT = 2;
                $orden->update();
                $siafSecuencial->Secuencial=$siafSecuencial->Secuencial+1;
                //var_dump($contenido);die;
            }
            $siafSecuencial->update();
        }
    }


    private function SiaftInternoSeguimiento($detalle = array(),$tipo = null){
        $internoSiaft = new Siaft;
        $internoSiaft->Fase = $tipo;
        $internoSiaft->Secuencial = $detalle['OrdenCorrelativo'];
        $internoSiaft->Fecha = $detalle['FechaOrden'];
        $internoSiaft->Monto = $detalle['Monto'];
        $internoSiaft->FechaProceso = date('Y-m-d');
        $internoSiaft->Estado = 1;
        $internoSiaft->CodigoProyecto = $detalle['CodigoProyecto'];
        $internoSiaft->Orden = $detalle['OrdenID'];
        $internoSiaft->Moneda = 'S/.';
        $saveid = $internoSiaft->insert();

        // print_r($internoSiaft);
        return $saveid;

    }
}
