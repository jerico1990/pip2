<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\InformacionGeneral;
use app\models\ProgramaTransversal;
use app\models\DireccionLinea;
use app\models\TipoInvestigacion;
use app\models\UnidadEjecutora;
use app\models\UnidadOperativa;
use app\models\Persona;
use app\models\Programa;
use app\models\CultivoCrianza;
use app\models\Especie;
use app\models\Ubigeo;
use app\models\Usuario;
use app\models\Investigador;
use app\models\MarcoLogicoPropositoProyecto;
use app\models\Componente;
use app\models\Poa;
use app\models\Proyecto;
use app\models\MarcoLogicoComponente;
use app\models\MarcoLogicoActividad;
use app\models\Actividad;
use app\models\CronogramaComponente;


class PlanOperativoController extends Controller
{

    public $investigador = NULL;
    
    public function init(){
        // parent::__construct();
        /*$usuario            = Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador  = Persona::findOne($usuario->PersonaID);
        $investigador       = Investigador::find()->where(['UsuarioID'=>$usuario->id])->one();
        $this->investigador = $investigador->ID;*/
    }
    

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($investigador=null)
    {
        $this->layout='estandar';
       /* if($investigador)
        {
            
        }
        else
        {*/
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            //$investigador=$investigador->ID;
        //}
        $model = $this->InformacionGeneral($investigador->ID);
            
        return $this->render('index',['infoGeneral' => $model]);
    }

    public function InformacionGeneral($investigador=null)
    {
        $model=InformacionGeneral::find()
                ->select('Proyecto.Fin,Proyecto.Proposito,Proyecto.ID,InformacionGeneral.TituloProyecto')
                ->innerJoin('Investigador','Investigador.ID=InformacionGeneral.InvestigadorID')
                ->innerJoin('Proyecto','Proyecto.InvestigadorID=Investigador.ID')
                ->where('InformacionGeneral.InvestigadorID=:InvestigadorID',['InvestigadorID'=>$investigador])
                ->one();
                
        if(!$model)
        {
            $model=new InformacionGeneral;
        }
        return $model;
    }


    //////////////////////////
    // PROPOSITO DEL PROYECTO
    //////////////////////////


    ## Form Crear de Fin del proyecto
        public function actionObjetivoform($id = null){
            $this->layout='vacio';
            
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $investigador=$investigador->ID;
            $xx = array();

            if(!is_null($id)){
                $model = Componente::findOne($id);
            }else{
                $model = new Componente();
            }
            return $this->render('objetivo/_form_objetivo',['model' => $model ,'marco' => $xx ]);
        }

        public function actionListaIndicadoresObjetivo($ComponenteID = null){
            $this->layout = 'vacio';
            if(!is_null($ComponenteID)){
                $model = MarcoLogicoComponente::find()
                ->where(['ComponenteID'=> $ComponenteID,'Estado' => 1])
                ->all();
            }else{
                $model = new Componente();
            }
            return $this->render('objetivo/_lista_indicadores_objetivo',[ 'model' => $model ]);
        }

    ## Crear de objetivo
        public function actionObjetivoCrear(){
            $this->layout='vacio';
            $model = new Componente();
            $usuario            = Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador  = Persona::findOne($usuario->PersonaID);
            $investigador       = Investigador::find()->where(['UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            
            if ( $model->load(Yii::$app->request->post()) ) {
                $marco = Poa::find()
                    ->select('Poa.ID,Poa.Anno,Proyecto.ID AS ProyectoID,Poa.Situacion')
                    ->innerJoin('Proyecto','Poa.ProyectoID = Proyecto.ID')
                    ->where(['Proyecto.InvestigadorID'=> $investigador->ID,'Poa.Estado' => 1])
                    ->one();
                // Validando Peso
                $query = (new \yii\db\Query())->from('Componente')->where('PoaID=:PoaID AND Estado = 1',[':PoaID'=>$marco->ID]);
                $sum = (int) $query->sum('Peso');
                $totalSuma = $sum + $model->Peso;
                if($totalSuma > 100 ){
                    $arr = array(
                        'Error' => true,
                        'Total'=> $totalSuma,
                        'Message' => 'EL PESO INGRESADO AH SUPERADO EL TOTAL DE LOS OBJETIVOS'
                    );
                }else{
                    $model->Estado = 1;
                    $model->PoaID  = $marco->ID;
                    // die();
                    $model->Correlativo=$this->CorrelativoComponente($marco->ID);
                    $model->save();
                    
                    for($i=1;$i<=$informacionGeneral->Meses;$i++)
                    {
                        $new= new CronogramaComponente;
                        $new->ComponenteID=$model->ID;
                        $new->Mes=$i;
                        $new->insert();
                    }
                    
                    $id = $model->ID;
                    // $id = 10;
                    if($id){
                        $arr = array(
                            'Success' => true,
                            'Total'=> $totalSuma,
                            'id'     => $id
                        );
                    }else{
                        $arr = array(
                            'Error' => true,
                            'Message' => 'Ocurrio un error intentelo nuevamente'
                        );
                    }
                }
                
                echo json_encode($arr);
            }
        }

        public function actionActualizaObjetivo($id = null){
            $this->layout='vacio';

            $usuario            = Usuario::findOne(\Yii::$app->user->id);
            $investigador       = Investigador::find()->where(['UsuarioID'=>$usuario->id])->one();


            $model = Componente::findOne($id);
            $ant = $model->Peso;
            if ( $model->load(Yii::$app->request->post()) ) {
                $marco = Poa::find()
                    ->select('Poa.ID,Poa.Anno,Proyecto.ID AS ProyectoID,Poa.Situacion')
                    ->innerJoin('Proyecto','Poa.ProyectoID = Proyecto.ID')
                    ->where(['Proyecto.InvestigadorID'=> $investigador->ID,'Poa.Estado' => 1])
                    ->one();
                $query = (new \yii\db\Query())->from('Componente')->where('PoaID=:PoaID AND Estado = 1',[':PoaID'=>$marco->ID]);
                $sum = (int) $query->sum('Peso');
                $sumaAnt = $sum - $ant;
                $totalSuma = ($sumaAnt + (int)$model->Peso);
                if($totalSuma > 100 ){
                    $arr = array(
                        'Error' => true,
                        'Total'=> $totalSuma,
                        'Message' => 'EL PESO INGRESADO AH SUPERADO EL TOTAL DE LOS OBJETIVOS'
                    );
                }else{
                    $model->update();
                    $id = $model->ID;
                    if($id){
                        $arr = array(
                            'Success' => true,
                            'Total'=> $totalSuma,
                            'id'     => $id
                        );
                    }else{
                        $arr = array(
                            'Error' => true,
                            'Message' => 'Ocurrio un error intentelo nuevamente'
                        );
                    }
                }
                echo json_encode($arr);
            }
        }

        public function actionEliminarIndicadorObjetivo(){
            $this->layout='vacio';
            $ds = Yii::$app->request->post('id');
            $comp = Yii::$app->request->post('comp');

            $customer = MarcoLogicoComponente::findOne($ds);
            $customer->Estado = 0;
            $customer->update();
            $arr = array(
                'Success' => true,
                'comp'    => $comp
                );
            echo json_encode($arr);
        }

        public function actionObjetivoIndicadoresCrear(){
            $this->layout='vacio';
            $model = new MarcoLogicoComponente();
            $xx = array();
            // print_r(Yii::$app->request->post());die();

            if ( $model->load(Yii::$app->request->post()) ) {
                $model->Estado = 1;
                $model->save();
                $id = $model->ComponenteID;
                // $id = 1;

                $marcox = MarcoLogicoComponente::find()
                    ->where(['ComponenteID'=> $model->ComponenteID,'Estado' => 1])
                    ->all();

                // print_r($marcox);
                foreach ($marcox as $value) {
                    $xx[] = array(
                        'ID'                    => $value->ID , 
                        'ComponenteID'          => $value->ComponenteID , 
                        'IndicadorDescripcion'  => $value->IndicadorDescripcion, 
                        'IndicadorUnidadMedida' => $value->IndicadorUnidadMedida, 
                        'IndicadorMeta'         => $value->IndicadorMeta, 
                        'MedioVerificacion'     => $value->MedioVerificacion, 
                        'Supuestos'             => $value->Supuestos
                    );
                }
                // die();
                $arr = array(
                    'Success' => true,
                    'id'     => $id,
                    'marco' => $xx
                );
                echo json_encode($arr);
            }
        }


    ### AJAX
        public function actionMarco($investigadorID=null){
            $this->layout='vacio';
            $marco = Poa::find()
                    ->select('Poa.ID,Poa.Anno,Proyecto.ID AS ProyectoID,Poa.Situacion')
                    ->innerJoin('Proyecto','Poa.ProyectoID = Proyecto.ID')
                    ->where(['Proyecto.InvestigadorID'=> $investigadorID,'Poa.Estado' => 1])
                    ->all();
                    
            foreach ($marco as $value) {
                $jsonAll = array(
                    'ID'            => $value->ID, 
                    'Anno'          => $value->Anno, 
                    'ProyectoID'    => $value->ProyectoID, 
                    'Situacion'     => $value->Situacion,
                    'Componentes'   => $this->get_objetivo($value->ID)
                );

                echo json_encode($jsonAll);
                // echo '<pre>';print_r($jsonAll);echo '</pre>';
            }
        }

        public function actionActividadesAjax($id = null){
            $this->layout='vacio';
            $jsonAll = array();
            if(empty($id) || $id !=0){
                $jsonAll = array(
                    'Actividades'   => $this->get_actividades($id)
                );
            }
            
            echo json_encode($jsonAll);
            // echo '<pre>';print_r($jsonAll);echo '</pre>';
        }

        private function get_objetivo($idPoa){
            $jsonComponente = array();
            $component = Componente::find()
                    ->where(['Estado' => 1, 'PoaID' => $idPoa])
                    ->orderBy('Correlativo asc')
                    ->all();
            if(!empty($component) ){
                foreach ($component as $comp) {
                    $jsonComponente[] = array(
                        'ID'            => $comp->ID, 
                        'Nombre'        => $comp->Nombre,
                        'Peso'        => $comp->Peso,
                        'Correlativo'     => $comp->Correlativo,
                        'Actividades'   => $this->get_actividades($comp->ID), 
                        'Indicadores'   => $this->get_marcologico($comp->ID)
                    );

                }
            }
            return $jsonComponente;
        }

        private function get_marcologico($idComponente){
            $jsonComponenteMarco = array();
            $marcoLog = MarcoLogicoComponente::find()
                    ->where(['Estado' => 1, 'ComponenteID' => $idComponente])
                    ->all();
            if(!empty($marcoLog) ){
                foreach ($marcoLog as $comp) {
                    $jsonComponenteMarco[] = array(
                        'ID'                    => $comp->ID, 
                        'IndicadorDescripcion'  => $comp->IndicadorDescripcion, 
                        'IndicadorUnidadMedida' => $comp->IndicadorUnidadMedida, 
                        'IndicadorMeta'         => $comp->IndicadorMeta,
                        'MedioVerificacion'     => $comp->MedioVerificacion,
                        'Supuestos'             => $comp->Supuestos,
                        'Estado'                => $comp->Estado
                    );
                }
            }
            return $jsonComponenteMarco;
        }

        private function get_actividades($idComponente){
            $jsonActividades = array();
            $actv = Actividad::find()
                    ->where(['Estado' => 1, 'ComponenteID' => $idComponente])
                    ->orderBy('Correlativo asc')
                    ->all();
            if(!empty($actv) ){
                foreach ($actv as $actvd) {
                    $jsonActividades[] = array(
                        'ID'            => $actvd->ID, 
                        'ComponenteID'  => $actvd->ComponenteID, 
                        'Nombre'        => $actvd->Nombre,
                        'Correlativo'     => $actvd->Correlativo,
                        'Indicadores'   => $this->get_actividadesmarco($actvd->ID)
                    );
                }
            }
            return $jsonActividades;
        }

        private function get_actividadesmarco($idActividad){
            $jsonActividadesMarco = array();
            $actvmarco = MarcoLogicoActividad::find()
                    ->where(['Estado' => 1, 'ActividadID' => $idActividad])
                    ->all();
            if(!empty($actvmarco) ){
                foreach ($actvmarco as $actvmarco) {
                    $jsonActividadesMarco[] = array(
                        'ID'                    => $actvmarco->ID, 
                        'IndicadorUnidadMedida' => $actvmarco->IndicadorUnidadMedida,
                        'IndicadorDescripcion' => $actvmarco->IndicadorDescripcion, 
                        'IndicadorMeta'         => $actvmarco->IndicadorMetaFisica,
                        'MedioVerificacion'     => $actvmarco->IndicadorMedioVerificacion,
                        'Supuestos'             => $actvmarco->IndicadorSupuestos,
                        'Estado'                => $actvmarco->Estado
                    );
                }
            }
            return $jsonActividadesMarco;
        }
        
        public function CorrelativoComponente($poaID)
        {
            $componente=Componente::find()->where('PoaID=:PoaID and Estado=1',[':PoaID'=>$poaID])->orderBy('Correlativo desc')->one();
            if($componente)
            {
                return $componente->Correlativo+1;
            }
            else
            {
                return 1;
            }
        }
        
        public function actionActualizarMarco($Valor=null,$ID=null,$Tipo=null){
            
                
                $marcologico=MarcoLogicoComponente::findOne($ID);
                if($Tipo==1)
                {
                    $marcologico->IndicadorDescripcion=$Valor;
                    
                }
                elseif($Tipo==2)
                {
                    $marcologico->IndicadorUnidadMedida=$Valor;
                }
                elseif($Tipo==3)
                {
                    $marcologico->IndicadorMeta=$Valor;
                }
                elseif($Tipo==4)
                {
                    $marcologico->MedioVerificacion=$Valor;
                }
                elseif($Tipo==5)
                {
                    $marcologico->Supuestos=$Valor;
                }
                $marcologico->update();
                $arr = array('Success' => true);
                echo json_encode($arr);
            
        }
        
        public function actionActualizarMarcoActividad($Valor=null,$ID=null,$Tipo=null){
            
                
                $marcologico=MarcoLogicoActividad::findOne($ID);
                if($Tipo==1)
                {
                    $marcologico->IndicadorDescripcion=$Valor;
                    
                    //var_dump($Tipo);die;
                }
                elseif($Tipo==2)
                {
                    $marcologico->IndicadorUnidadMedida=$Valor;
                }
                elseif($Tipo==3)
                {
                    $marcologico->IndicadorMetaFisica=$Valor;
                }
                elseif($Tipo==4)
                {
                    $marcologico->IndicadorMedioVerificacion=$Valor;
                }
                elseif($Tipo==5)
                {
                    $marcologico->IndicadorSupuestos=$Valor;
                }
                $marcologico->update();
                $arr = array('Success' => true);
                echo json_encode($arr);
            
        }
}