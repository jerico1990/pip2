<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Usuario;
use app\models\Persona;
use app\models\Investigador;
use app\models\Proyecto;
use app\models\Componente;
use app\models\Actividad;
use app\models\Poa;
use app\models\InformacionGeneral;
use app\models\CronogramaProyecto;
use app\models\ActRubroElegible;
use app\models\CronogramaComponente;
use app\models\CronogramaActividad;
use app\models\CronogramaActRubroElegible;
use app\models\RubroElegible;
use app\models\AreSubCategoria;
use app\models\CronogramaAreSubCategoria;
use app\models\MarcoLogicoActividad;
use app\models\Pat;
use app\models\AreSubCategoriaObservacion;
use app\models\PasoCritico;

class RecursosPoaEjecutadoController extends Controller
{
    /**
     * @inheritdoc
     */
    
    /*public $investigador = NULL;
    public function init(){
        // parent::__construct();
        $usuario            = Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador  = Persona::findOne($usuario->PersonaID);
        $investigador       = Investigador::find()->where(['UsuarioID'=>$usuario->id])->one();
        $this->investigador = $investigador->ID;
    }*/

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex($investigadorID=null)
    {
        $this->layout='estandar';
        if($investigadorID==NULL)
        {
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        }
        else
        {
            $investigador=Investigador::findOne($investigadorID);
            $usuario=Usuario::findOne($investigador->UsuarioID);
        }
        
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $pasocritico=PasoCritico::find()->where('Estado=1 and Situacion=2 and ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
        $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$proyecto->InvestigadorID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID and Estado=1',[':ProyectoID'=>$proyecto->ID])->one();
        $componentes=Componente::find()->where('PoaID=:PoaID and Estado=1',[':PoaID'=>$poa->ID])->all();
        return $this->render('index',['componentes'=>$componentes,'usuario'=>$usuario,'poa'=>$poa,'investigadorID'=>$investigador->ID,'informacionGeneral'=>$informacionGeneral,'pasocritico'=>$pasocritico]);
    }
    


    public function actionRecursosForm(){
        $this->layout='vacio';
        return $this->render('recursos/_recursos_form');
    }



    /**
     * Rescursos JSON 
     * @return [type] [description]
     */
    public function actionRecursosJson($codigo=null){
        $this->layout='vacio';
        // Proyectos
        $usuario=Usuario::find()->where('username=:username',[':username'=>$codigo])->one();
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        
        $proyecto = Poa::find()
            ->select('Poa.ID, Poa.ProyectoID')
            ->innerJoin('Proyecto','Poa.ProyectoID = Proyecto.ID')
            ->where(['Proyecto.InvestigadorID'=> $investigador->ID,'Poa.Estado' => 1])
            ->one();


        $proyectox = array(
            'ID'             => $proyecto->ProyectoID, 
            'Componentes'    => $this->get_objetivo($proyecto->ID),
            'Cronogramas'    => $this->getCronogramaProyecto($proyecto->ProyectoID,'proyecto')
        );

        echo json_encode($proyectox); 
    }
    
    public function actionRecursosFinanzasJson($codigo=null){
        $this->layout='vacio';
        // Proyectos
        $usuario=Usuario::find()->where('username=:username',[':username'=>$codigo])->one();
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        
        $proyecto = Poa::find()
            ->select('Poa.ID, Poa.ProyectoID')
            ->innerJoin('Proyecto','Poa.ProyectoID = Proyecto.ID')
            ->where(['Proyecto.InvestigadorID'=> $investigador->ID,'Poa.Estado' => 1])
            ->one();


        $proyectox = array(
            'ID'             => $proyecto->ProyectoID, 
            'PasoCritico'    => $this->get_pasos_criticos($proyecto->ID),
        );

        echo json_encode($proyectox); 
    }
    
    public function get_pasos_criticos($ProyectoID)
    {
        $json=[];
        $pasosCriticos=PasoCritico::find()->where('ProyectoID=:ProyectoID and MontoTotal!=0',[':ProyectoID'=>$ProyectoID])->all();
        
        foreach($pasosCriticos as $pasoCritico)
        {
            $monto = PasoCritico::find()
                ->select('sum(CronogramaAreSubCategoria.MetaFisica*CronogramaAreSubCategoria.CostoUnitario) Total')
                ->innerJoin('Poa','Poa.ProyectoID=PasoCritico.ProyectoID')
                ->innerJoin('Componente','Componente.PoaID=Poa.ID')
                ->innerJoin('Actividad','Actividad.ComponenteID=Componente.ID')
                ->innerJoin('ActRubroElegible','ActRubroElegible.ActividadID=Actividad.ID')
                ->innerJoin('AreSubCategoria','AreSubCategoria.ActRubroElegibleID=ActRubroElegible.ID')
                ->innerJoin('CronogramaAreSubCategoria','CronogramaAreSubCategoria.AreSubCategoriaID=AreSubCategoria.ID')
                ->where('PasoCritico.ID=:ID and CronogramaAreSubCategoria.MetaFisica!=0',[':ID' => $pasoCritico->ID])
                ->andWhere(['between', 'CronogramaAreSubCategoria.Mes', $pasoCritico->MesInicio, $pasoCritico->MesFin])
                ->one();
            $json[]=[
                        'MesInicio'=> Yii::$app->tools->DescripcionMes($pasoCritico->MesInicio),
                        'MesFin'=> Yii::$app->tools->DescripcionMes($pasoCritico->MesFin),
                        'Correlativo'=>$pasoCritico->Correlativo,
                        'Porcentage'=>$pasoCritico->PorcentageProgramado,
                        'MontoTotal'=>$pasoCritico->MontoTotal,
                        'MontoTotal2'=>$monto->Total,
                        'MontoTotalEjecutado'=>$pasoCritico->MontoTotalEjecutado,
                    ];
        }
        return $json;
    }
    public function actionRecursosJsonPa(){
        $this->layout='vacio';
        // Proyectos
        $usuario            = Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador  = Persona::findOne($usuario->PersonaID);
        $investigador       = Investigador::find()->where(['UsuarioID'=>$usuario->id])->one();
        
        $proyecto = Poa::find()
        ->select('Poa.ID, Poa.ProyectoID')
        ->innerJoin('Proyecto','Poa.ProyectoID = Proyecto.ID')
        ->where(['Proyecto.InvestigadorID'=> $investigador->ID,'Poa.Estado' => 1])
        ->one();

        $proyectox = array(
            'ID'             => $proyecto->ProyectoID, 
            'Componentes'    => $this->get_objetivo($proyecto->ID),
            'Cronogramas'    => $this->getCronogramaProyecto($proyecto->ProyectoID,'proyecto')
        );
         echo json_encode($proyectox); die();
    }


    private function get_objetivo($idPoa){
        $jsonComponente = array();
        $component = Componente::find()
                ->where(['Estado' => 1, 'PoaID' => $idPoa])
                ->orderBy('Correlativo asc')
                ->all();
        if(!empty($component) ){
            foreach ($component as $comp) {
                $jsonComponente[] = array(
                    'ID'            => $comp->ID, 
                    'Nombre'        => $comp->Nombre, 
                    'Actividades'   => $this->get_actividades($comp->ID), 
                    'Cronogramas'   => $this->getCronogramaProyecto($comp->ID,'componente')
                );

            }
        }
        return $jsonComponente;
    }

    private function get_actividades($idComponente){
        $jsonActividades = array();
        $actv = Actividad::find()
                ->where(['Estado' => 1, 'ComponenteID' => $idComponente])
                ->orderBy('Correlativo asc')
                ->all();
        if(!empty($actv) ){
            foreach ($actv as $actvd) {
                $jsonActividades[] = array(
                    'ID'            => $actvd->ID, 
                    'ComponenteID'  => $actvd->ComponenteID, 
                    'Nombre'        => $actvd->Nombre, 
                    'UnidadMedida'  => $this->get_actividadesmarco($actvd->ID)->IndicadorUnidadMedida, 
                    'CostoUnitario' => $actvd->CostoUnitario, 
                    'MetaFisica'    => $this->get_actividadesmarco($actvd->ID)->IndicadorMetaFisica,
                    'ActRubroElegibles' => $this->get_actRubroElegible($actvd->ID),
                    'Cronogramas'       => $this->getCronogramaProyecto($actvd->ID,'actividad')
                );
            }
        }
        return $jsonActividades;
    }

    private function get_actRubroElegible($idActividad){
        $jsonActividades = array();
        $actv = ActRubroElegible::find()
                ->where(['ActividadID' => $idActividad])
                ->all();
        if(!empty($actv) ){
            foreach ($actv as $actvd) {
                $jsonActividades[] = array(
                    'ID'                => $actvd->ID, 
                    'ActividadID'       => $actvd->ActividadID, 
                    'RubroElegibleID'   => $actvd->RubroElegibleID, 
                    'UnidadMedida'      => $actvd->UnidadMedida, 
                    'CostoUnitario'     => $actvd->CostoUnitario, 
                    'MetaFisica'        => $actvd->MetaFisica, 
                    'AreSubCategorias'  => $this->get_areSubCategoria($actvd->ID),
                    'RubroElegible'     => $this->get_rubroElegible($actvd->RubroElegibleID),
                    'Cronogramas'       => $this->getCronogramaProyecto($actvd->ID,'rubroElegible')
                );
            }
        }
        return $jsonActividades;
    }

    private function get_rubroElegible($idRubro){
        $jsonActividades = array();
            $actv = RubroElegible::find()
                    ->where(['ID' => $idRubro])
                    ->all();

        if(!empty($actv) ){
            foreach ($actv as $actvd) {
                $jsonActividades = array(
                    'TipoRubroElegibleID'=> $actvd->TipoRubroElegibleID, 
                    'ID'                 => $actvd->ID, 
                    'Nombre'             => $actvd->Nombre
                );
            }
        }
        return $jsonActividades;
    }

    private function get_areSubCategoria($idActRubroElegible){
        $jsonActividades = array();
        $actv = AreSubCategoria::find()
                ->where(['ActRubroElegibleID' => $idActRubroElegible])
                ->all();
        if(!empty($actv) ){
            foreach ($actv as $actvd) {
                $jsonActividades[] = array(
                    'ID'                    => $actvd->ID, 
                    'ActRubroElegibleID'    => $actvd->ActRubroElegibleID, 
                    'Nombre'                => $actvd->Nombre, 
                    'UnidadMedida'          => $actvd->UnidadMedida, 
                    'CostoUnitario'         => $actvd->CostoUnitario, 
                    'MetaFisica'            => $actvd->MetaFisica, 
                    'Total'                 => $actvd->Total, 
                    'TotalConMetaFisica'    => $actvd->TotalConMetaFisica,
                    'Observacion'           => $this->getObservacionRecurso($actvd->ID),
                    'Situacion'             => $actvd->Situacion,
                    'MetaFisicaEjecutada'   => $actvd->MetaFisicaEjecutada,
                    'MetaAvance'             => $actvd->MetaAvance,
                    
                    'Cronogramas'           => $this->getCronogramaProyecto($actvd->ID,'areSubCategoria')
                );
            }
        }
        return $jsonActividades;
    }

    private function get_actividadesmarco($idActividad){
        $jsonActividadesMarco = array();
        $actvmarco = MarcoLogicoActividad::find()
                ->where(['Estado' => 1, 'ActividadID' => $idActividad])
                ->one();
        if(!$actvmarco)
        {
            $actvmarco=new MarcoLogicoActividad;
        }
        return $actvmarco;
    }

    private function getCronogramaProyecto($id,$tipo = ''){
        $this->layout='vacio';
        $meses = array();
        $cronograma = array();
        switch ($tipo) {
            case 'proyecto':
                $meses = CronogramaProyecto::find()
                        ->where(['ProyectoID'=> $id])
                        ->all();
                $indicador = 'ProyectoID';
            break;
            case 'componente':
                $meses = CronogramaComponente::find()
                        ->where(['ComponenteID'=> $id])
                        ->all();
                $indicador = 'ComponenteID';
            break;

            case 'actividad':
                $meses = CronogramaActividad::find()
                        ->where(['ActividadID'=> $id])
                        ->all();
                $indicador = 'ActividadID';
            break;

            case 'rubroElegible':
                $meses = CronogramaActRubroElegible::find()
                        ->where(['ActRubroElegibleID'=> $id])
                        ->all();
                $indicador = 'ActRubroElegibleID';
            break;

            case 'areSubCategoria':
                $meses = CronogramaAreSubCategoria::find()
                        ->where(['AreSubCategoriaID'=> $id])
                        ->all();
                $indicador = 'AreSubCategoriaID';
            break;
        }

        if(!empty($meses)){
            foreach ($meses as $mes) {
                $cronograma[] = array(
                    'ID'                    => $mes->ID,
                    $indicador              => $mes->{$indicador},
                    'Mes'                   => $mes->Mes,
                    'MetaFisica'            => $mes->MetaFisica,
                    'MetaFinanciera'        => $mes->MetaFinanciera,
                    'PoafMetaFinanciera'    => $mes->PoafMetaFinanciera,
                    'MetaFisicaEjecutada'   => $mes->MetaFisicaEjecutada,
                    'MetaAvance'            => $mes->MetaAvance,
                    'SituacionEjecucion'    => $mes->SituacionEjecucion,
                    );
            }
        }else{
            $cronograma = array(
                'ID'                    => '',
                $indicador              => '',
                'Mes'                   => '',
                'MetaFisica'            => '',
                'MetaFinanciera'        => '',
                'PoafMetaFinanciera'    => '',
                'MetaFisicaEjecutada'   => '',
                'MetaAvance'            => '',
                'SituacionEjecucion'    => ''
            );
        }

        return $cronograma;
    }


    public function actionRecursos($id,$investigador){
        $this->layout='vacio';
        $actv = RubroElegible::find()
                ->all();
                
        return $this->render('recursos/_recursos_form',['idactividad'=>$id,'rubro' => $actv,'investigador'=>$investigador]);
    }
    
    public function actionRecursosActualizar($id=null,$actv=null){
        $this->layout='vacio';
        
        $rubrosElegibles=RubroElegible::find()
                ->all();
                
        $idActividad = Yii::$app->request->post('ActRubroElegible');
        $act = ActRubroElegible::find()
            ->where(['ActividadID'=> $actv,'RubroElegibleID' => $idActividad['RubroElegibleID']])
            ->one();
        $model = new ActRubroElegible();
        $subC=AreSubCategoria::find()
                    ->select('AreSubCategoria.*,ActRubroElegible.RubroElegibleID')
                    ->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')
                    ->where('AreSubCategoria.ID=:ID',[':ID'=>$id])
                    ->one();
        if($model->load(Yii::$app->request->post()))
        {
            if(!empty($act)){
                $id = $act->ID;
            }else{
                $model->save();
                $id = $model->ID;
            }
            CronogramaActRubroElegible::updateAll(['MetaFisica' => 0], 'ActRubroElegibleID='.$subC->ID);
            
            if ( $subC->load(Yii::$app->request->post()) ) {
                
                $CostoUnitario = Yii::$app->request->post('CostoUnitario');
                $MetaFisica = Yii::$app->request->post('MetaFisica');
                $subC->Total = ($CostoUnitario * $MetaFisica);
                $subC->ActRubroElegibleID = $id;
                $subC->save();
                $idz = $subC->ID;
                CronogramaAreSubCategoria::updateAll(['MetaFisica' => 0], 'AreSubCategoriaID='.$idz);
               
                $arr = array(
                    'Success' => true,
                    'id'     => $idz
                );
                echo json_encode($arr);
            }
        }
        return $this->render('recursos/actualizar_recursos_form',['recurso'=>$subC,'rubrosElegibles'=>$rubrosElegibles]);
    }

    public function actionRecursosCrear($investigadorID=null){
        $this->layout='vacio';
        
        $mes = InformacionGeneral::find()
            ->select('Meses')
            ->where(['InvestigadorID' => $investigadorID])
            ->one();
       // echo $mes->Meses;
        //die();
        $idActividad = Yii::$app->request->post('ActRubroElegible');
        $act = ActRubroElegible::find()
            ->where(['ActividadID'=> $idActividad['ActividadID'],'RubroElegibleID' => $idActividad['RubroElegibleID']])
            ->one();
       
        // echo '<pre>';print_r($act); echo '</pre>';

        // die();

        $model = new ActRubroElegible();
        $subC = new AreSubCategoria();
        if ( $model->load(Yii::$app->request->post()) ) {
            // Preguntar antes si el rubro elegible existe para esa actividad 
            if(!empty($act)){
                $id = $act->ID;
            }else{
                
                $model->save();
                $id = $model->ID;
            }
            // echo $id; die();
            for ($i=1; $i <= $mes->Meses; $i++) { 
                $cronogramaAct = new CronogramaActRubroElegible();
                $cronogramaAct->ActRubroElegibleID = $id;
                $cronogramaAct->Mes = $i;
                $cronogramaAct->MetaFinanciera = 0;
                $cronogramaAct->PoafMetaFinanciera = 0;
                $cronogramaAct->save();
            }
            
            if ( $subC->load(Yii::$app->request->post()) ) {
                
                $CostoUnitario = Yii::$app->request->post('CostoUnitario');
                $MetaFisica = Yii::$app->request->post('MetaFisica');
                $subC->Total = ($CostoUnitario * $MetaFisica);
                $subC->ActRubroElegibleID = $id;
                $subC->Correlativo=$this->CorrelativoRecurso($id);
                $subC->Situacion=0;
                $subC->save();
                $idz = $subC->ID;

                for ($i=1; $i <= $mes->Meses; $i++) {
                    //var_dump($investigadorID);
                    $cronoAreSubCat = new CronogramaAreSubCategoria();
                    $cronoAreSubCat->AreSubCategoriaID = $idz;
                    $cronoAreSubCat->Mes = $i;
                    $cronoAreSubCat->MetaFinanciera = 0;
                    $cronoAreSubCat->PoafMetaFinanciera = 0;
                    $cronoAreSubCat->save();
                }
                $arr = array(
                    'Success' => true,
                    'id'     => $idz
                );
                echo json_encode($arr);
            }
        }
    }

    public function actionGrabarMetaEjecutadaCronogramaRecurso($ID=null,$MetaFisicaEjecutada=null)
    {
        
        $cronogramaRecurso=CronogramaAreSubCategoria::findOne($ID);
        $cronogramaRecurso->MetaFisicaEjecutada=$MetaFisicaEjecutada;
        if($cronogramaRecurso->MetaFisica<$MetaFisicaEjecutada)
        {
            $MetaFisicaEjecutada=$cronogramaRecurso->MetaFisica;
        }
        $porcentaje_cronograma=($MetaFisicaEjecutada*100)/$cronogramaRecurso->MetaFisica;
        $cronogramaRecurso->MetaAvance=$porcentaje_cronograma;
        $cronogramaRecurso->update();
        
        
        $cronogramaRecursosAvances=CronogramaAreSubCategoria::find()->where('MetaFisicaEjecutada!=0 and AreSubCategoriaID=:AreSubCategoriaID',[':AreSubCategoriaID'=>$cronogramaRecurso->AreSubCategoriaID])->all();
        $MetaFisicaEjecutadaRecurso=0;
        foreach($cronogramaRecursosAvances as $cronogramaRecursoAvance)
        {
            if($cronogramaRecursoAvance->MetaAvance==100)
            {
                $MetaFisicaEjecutadaRecurso=$MetaFisicaEjecutadaRecurso+$cronogramaRecursoAvance->MetaFisica;
            }
            else
            {
                $MetaFisicaEjecutadaRecurso=$MetaFisicaEjecutadaRecurso+$cronogramaRecursoAvance->MetaFisicaEjecutada;
            }
        }
        
        $recurso=AreSubCategoria::findOne($cronogramaRecurso->AreSubCategoriaID);
        $recurso->MetaFisicaEjecutada=$MetaFisicaEjecutadaRecurso;
        $recurso->MetaAvance=($MetaFisicaEjecutadaRecurso*100)/$recurso->MetaFisica;
        $recurso->update();
        
        $arr = array(
                    'Success' => true,
                    
                );
        echo json_encode($arr);
    }
    
    public function actionEnviarEvaluacionPoa($codigo)
    {
        $usuario=Usuario::find()->where('username=:username',[':username'=>$codigo])->one();
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID and Estado=1',[':ProyectoID'=>$proyecto->ID])->one();
        $poa->Situacion=2;
        $poa->update();
    }
    
    
    public function actionDeleteRecurso($id=null)
    {
        if($id)
        {
            $subC=AreSubCategoria::find()
                        ->select('AreSubCategoria.*,ActRubroElegible.RubroElegibleID,ActRubroElegible.ActividadID')
                        ->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')
                        ->where('AreSubCategoria.ID=:ID',[':ID'=>$id])
                        ->one();
            $countRubrosElegibles=ActRubroElegible::find()
                    ->where('ActividadID=:ActividadID and RubroElegibleID=:RubroElegibleID',[':ActividadID'=>$subC->ActividadID,':RubroElegibleID'=>$subC->RubroElegibleID])->count();
            //echo $subC->ActividadID;
            CronogramaAreSubCategoria::deleteAll(['AreSubCategoriaID' => $id]);
            AreSubCategoria::deleteAll(['ID' => $id]);
            echo $subC->ActRubroElegibleID;
            if($countRubrosElegibles==1)
            {
                CronogramaActRubroElegible::deleteAll(['ActRubroElegibleID' => $subC->ActRubroElegibleID]);
                ActRubroElegible::deleteAll(['ActividadID' => $subC->ActividadID,'RubroElegibleID'=>$subC->RubroElegibleID]);
            }
            
            //\Yii::$app->db->createCommand()->delete('AreSubCategoria', ['id' => $id])->execute();
            //\Yii::$app->db->createCommand()->delete('AreSubCategoria', ['id' => $id])->execute();
            
        }
    }
    public function actionDescargarPoa($codigo=null)
    {
        $this->layout='estandar';
        $usuario=Usuario::find()->where('username=:username',[':username'=>$codigo])->one();
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
        $componentes=Componente::find()->where('PoaID=:PoaID',[':PoaID'=>$poa->ID])->orderBy('Correlativo asc')->all();
        
        return $this->render('descargar-poa',['componentes'=>$componentes,'proyecto'=>$proyecto]);
    }
    
    public function CorrelativoRecurso($actRubroElegibleID)
    {
        $recurso=AreSubCategoria::find()->where('ActRubroElegibleID=:ActRubroElegibleID',[':ActRubroElegibleID'=>$actRubroElegibleID])->orderBy('Correlativo desc')->one();
        if($recurso)
        {
            return $recurso->Correlativo+1;
        }
        else
        {
            return 1;
        }
    }
    
    public function getObservacionRecurso($id)
    {
        $observacion=AreSubCategoriaObservacion::find()->where('AreSubCategoriaID=:AreSubCategoriaID and Estado=1',[':AreSubCategoriaID'=>$id])->one();
        if($observacion)
        {
            return $observacion->Observacion;
        }
        
        return "";
    }
    
    public function actionProcesarEjecucionRecursos($codigo=null)
    {
        $usuario=Usuario::find()->where('username=:username',[':username'=>$codigo])->one();
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
        $updateRecurso = Yii::$app->db->createCommand(' UPDATE CronogramaAreSubCategoria SET SituacionEjecucion=1
                                                    FROM CronogramaAreSubCategoria
                                                    inner join AreSubCategoria on CronogramaAreSubCategoria.AreSubCategoriaID=AreSubCategoria.ID
                                                    inner join ActRubroElegible on ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID
                                                    inner join Actividad on Actividad.ID=ActRubroElegible.ActividadID
                                                    inner join Componente on Componente.ID=Actividad.ComponenteID
                                                    inner join Poa on Poa.ID=Componente.PoaID
                                                    where CronogramaAreSubCategoria.MetaFisicaEjecutada>0 and Poa.ID='.$poa->ID);
        $updateRecurso->execute();
        $arr = array(
                    'Success' => true,
                    
                );
        echo json_encode($arr);
    }
}
