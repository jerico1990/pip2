<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;

use app\models\Usuario;
use app\models\Persona;
use app\models\Investigador;
use app\models\Proyecto;
use app\models\InformacionGeneral;
use app\models\Poa;
use app\models\Componente;
use app\models\Actividad;
use app\models\ProgramacionTecnica;
use app\components\BNProgramacionTecnica;
use app\components\DAProgramacionTecnica;

class ProgramacionEventoNewController extends Controller
{
    private $_BNProgTecnica;
    private $_DAProgTecnica;
	/**
     * @inheritdoc
     */
    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions(){
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout='estandar';
        
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $CodigoProyecto=$informacionGeneral->Codigo;

		return $this->render('index', ['CodigoProyecto'=>$CodigoProyecto]);
    }
	
	/**
     * Rescursos JSON 
     * @return [type] [description]
     */
    public function actionListaEventosJson(){
        $this->layout='vacio';
        
        $_DAProgTecnica = new DAProgramacionTecnica();
		$experimentos = $_DAProgTecnica->ConsultarXtipo('E');

		header('Content-Type: application/json');		
        echo json_encode($experimentos);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionNuevo($CodigoProyecto)
    {
        $this->layout='vacio';
        
        $informacionGeneral=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacionGeneral->InvestigadorID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
        $componentes=Componente::find()->where('PoaID=:PoaID',[':PoaID'=>$poa->ID])->all();
        $_experimento=new ProgramacionTecnica();
        $_experimento->IdProgramacionTecnica = -1;
        
        return $this->render('_editar', ['CodigoProyecto'=>$CodigoProyecto, 'componentes'=>$componentes, 'experimento'=>$_experimento]);
    }

    public function actionRegistrar()
    {
        $this->layout='vacio';
        $retVal = ['Estado'=>1, 'Data'=>[], 'Msj'=>''];
        $_evento=new ProgramacionTecnica();
        if($_evento->load(Yii::$app->request->post())) {

            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $Proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();

            if($_evento->IdProgramacionTecnica == -1) {
                $_evento->ProyectoId = $Proyecto['ID'];
                $_evento->ProgramacionTipo='E';
                $_evento->Estado=1;
                $_evento->FechaRegistro=date('Ymd h:m:s');
                $_evento->Correlativo = 0;
            } 

            $_BNProgTecnica = new BNProgramacionTecnica();
            $_evento = $_BNProgTecnica->Registrar($_evento);

            $retVal['Data'] = $_evento;
            $retVal['Msj'] = 'El Evento se guardó correctamente.';
        }
        header('Content-Type: application/json');    
        return json_encode($retVal);   
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionEditar($CodigoProyecto, $IdProgramacionTecnica)
    {
        $this->layout='vacio';
        
        $informacionGeneral=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacionGeneral->InvestigadorID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
        $componentes=Componente::find()->where('PoaID=:PoaID',[':PoaID'=>$poa->ID])->all();

        $DA = new DAProgramacionTecnica();
        $ProgramacionTecnica = $DA->Selecciona($IdProgramacionTecnica);
        return $this->render('_editar',  ['CodigoProyecto'=>$CodigoProyecto, 'componentes'=>$componentes, 'experimento'=>$ProgramacionTecnica,]);
    }

    public function actionAnular()
    {
        $this->layout='vacio';
        $retVal = ['Estado'=>1, 'Data'=>[], 'Msj'=>''];
        $_experimento=new ProgramacionTecnica();
        if($_experimento->load(Yii::$app->request->post())) {

            $usuario=Usuario::findOne(\Yii::$app->user->id);
            
            if($_experimento->IdProgramacionTecnica != -1){
                $_BNProgTecnica = new BNProgramacionTecnica();
                $_BNProgTecnica->Eliminar($_experimento->IdProgramacionTecnica);
            } 
            $retVal['Data'] = $_experimento;
            $retVal['Msj'] = 'El Evento se eliminó correctamente.';
        }
        header('Content-Type: application/json');    
        return json_encode($retVal);   
    }

    /**
     * Rescursos JSON 
     * @return [type] [description]
     */
    public function actionConsultaEventoJson(){
        $this->layout='vacio';
        $IdProgramacionTecnica = Yii::$app->request->post('IdProgramacionTecnica');
        if($IdProgramacionTecnica != ''){
            $_DAProgTecnica = new DAProgramacionTecnica();
            $experimento = $_DAProgTecnica->Selecciona($IdProgramacionTecnica);
        } else {
            $experimento = [];
        }
        header('Content-Type: application/json');       
        echo json_encode($experimento);
    }
    /**
     * Rescursos JSON 
     * @return [type] [description]
     */
    public function actionListaEventosNoEjecutadosJson(){
        $this->layout='vacio';
        
        $_DAProgTecnica = new DAProgramacionTecnica();
        $experimentos = $_DAProgTecnica->ConsultarXtipo_NoEjecutado('E');

        header('Content-Type: application/json');       
        echo json_encode($experimentos);
    }
}