<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

class ServicioController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionReniec($valor=null)
    {
        return Yii::$app->tools->ServicioReniec($valor);
    }

    public function actionSunat($valor=null)
    {
        return Yii::$app->tools->ServicioSunat($valor);
    }

}
