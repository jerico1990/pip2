<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Usuario;
use app\models\Investigador;
use app\models\Persona;
use app\models\Proyecto;
use app\models\InformacionGeneral;
use app\models\UnidadOperativa;
use app\models\UnidadEjecutora;
use app\models\EntidadParticipante;
use app\models\Componente;
use app\models\Poa;
use app\models\Pat;
use app\models\Pac;
use app\models\Seguimiento;
use app\models\PasoCritico;
use app\models\Actividad;
use app\models\Evento;
use app\models\Experimento;
use app\models\Rol;
use app\models\UsuarioRol;
use app\models\Situacion;
use app\models\CronogramaAreSubCategoria;
use app\models\CronogramaTarea;


class ProyectoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($investigador=null)
    {
        $this->layout='estandar';
        
        return $this->render('index');
    }
    
    public function actionListaProyectos()
    {
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $usuarioRol=UsuarioRol::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->ID])->one();
	    $resultados = (new \yii\db\Query())
	    ->select(['Investigador.ID','Usuario.username as username','InformacionGeneral.TituloProyecto','InformacionGeneral.Meses','Poa.Estado','Poa.Situacion'])
	    ->from('InformacionGeneral')
	    ->innerJoin('Investigador','InformacionGeneral.InvestigadorID=Investigador.ID')
	    ->innerJoin('Usuario','Usuario.ID=Investigador.UsuarioID')
	    ->innerJoin('Proyecto','Proyecto.InvestigadorID=Investigador.ID')
	    ->innerJoin('Poa','Poa.ProyectoID=Proyecto.ID')
	    ->orderBy('Situacion desc','username','TituloProyecto','Meses') 
	    ->distinct()
	    ->all();
    	$nro=0;
    	foreach($resultados as $result)
    	{
    	    $nro++;
    	    echo "<tr>";
    	    echo "<td>" . $result["username"] . "</td>";
    	    echo "<td>" . mb_substr ($result["TituloProyecto"],0,100) . "</td>";
    	    echo "<td>" . $result["Meses"] . "</td>";
    	    echo "<td>".$this->Accion($result["Situacion"],$result["ID"])."</td>";
    	    echo "</tr>";
    	}
    }

    public function Accion($situacion=null,$id=null)
    {
        $descripcion='<a class="btn btn-primary" href="evaluacion-proyecto/evaluacion-proyecto?investigadorID='.$id.'">Revisar</a>';
        return $descripcion;
    }
    
    public function Situacion($codigo)
    {
        $situacion=Situacion::find()->where('Codigo=:Codigo',[':Codigo'=>$codigo])->one();
        return $situacion->Descripcion;
    }


    public function actionAmpliacion($investigador = null){
        $investigadorID=Investigador::findOne($investigador);
        $usuario=Usuario::findOne($investigadorID->UsuarioID);

    }

}
