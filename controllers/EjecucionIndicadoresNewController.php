<?php

namespace app\controllers;

use Yii;
use yii\web\UploadedFile;
use yii\web\Controller;

use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\LoginForm;

use app\models\Usuario;
use app\models\Persona;
use app\models\Investigador;
use app\models\Proyecto;
use app\models\InformacionGeneral;
use app\models\Poa;
use app\models\Componente;
use app\models\Actividad;
use app\models\EjecucionIndicador;
use app\models\ProgramacionTecnica;
use app\components\BNIndicadores;
use app\components\BNProgramacionTecnica;
use app\components\DAIndicadores;

class EjecucionIndicadoresNewController extends Controller
{
    private $_BNProgTecnica;
    private $_DAProgTecnica;
	/**
     * @inheritdoc
     */
    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions(){
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout='estandar';
        
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $CodigoProyecto=$informacionGeneral->Codigo;

		return $this->render('index', ['CodigoProyecto'=>$CodigoProyecto]);
    }
	
	/**
     * Rescursos JSON 
     * @return [type] [description]
     */
    public function actionListaIndicadoresPorProyecto(){
        $this->layout='vacio';
        $experimentos = [];
        if(isset($_POST['CodigoProyecto'])) {
            $_DAIndicadores = new DAIndicadores();
            $experimentos = $_DAIndicadores->Consultar(0, Yii::$app->request->post('CodigoProyecto'));
        }
        

		header('Content-Type: application/json');		
        echo json_encode($experimentos);
    }

    /**
     * Rescursos JSON 
     * @return [type] [description]
     */
    public function actionListaIndicadores(){
        $this->layout='vacio';
        $_DAIndicadores = new DAIndicadores();
        $experimentos = [];
        if(isset($_POST['CodigoProyecto'])) {
            $experimentos = $_DAIndicadores->ListarIndicadoresActivos(Yii::$app->request->post('CodigoProyecto'));
        } 
        header('Content-Type: application/json');       
        echo json_encode($experimentos);
    }

    public function actionRegistrar()
    {
        $this->layout='vacio';
        $retVal = ['Estado'=>1, 'Data'=>[], 'Msj'=>''];
        $_datos = ['CodigoProyecto'=>'', 'Indicadores'=>[] ];
        $_datos['CodigoProyecto'] = Yii::$app->request->post('CodigoProyecto');
        $_datos['Indicadores'] = Yii::$app->request->post('Indicadores');
        $retVal['Data'] = [];
        $i = 0;
        if(isset($_datos['CodigoProyecto']) && isset($_datos['Indicadores'])) {
            $_BNIndicadores = new BNIndicadores();
            foreach ($_datos['Indicadores'] as $indicador) {
                $_ejecucion = new EjecucionIndicador();
                $_ejecucion->CodigoProyecto = $_datos['CodigoProyecto'];
                $_ejecucion->IndicadorId = $indicador['IndicadorId'];
                $_ejecucion->Programado = $indicador['Programado'];
                $_ejecucion->Ejecutado = $indicador['Ejecutado'];
                $_ejecucion->Descripcion = $indicador['Descripcion'];
                $_ejecucion->Probabilidad = $indicador['Probabilidad'];
                $_ejecucion->Observacion = $indicador['Observacion'];
                $_ejecucion->Estado=1;
                // registro
                $_BNIndicadores->Registrar($_ejecucion);
                $retVal['Data'][$i] = $_ejecucion;
                $i++;
            }
        }
         //['ejecucion'=>$_ejecucion, 'programacion'=>$_programacion];
        $retVal['Msj'] = 'Los indicadores se guardaron correctamente.';
        header('Content-Type: application/json');    
        return json_encode($retVal);   
    }
    
    public function actionEliminaIndicadorProyecto()
    {
        $this->layout='vacio';
        $retVal = ['Estado'=>1, 'Data'=>[], 'Msj'=>''];
        $_ejecucion = new EjecucionIndicador();
        $_ejecucion->ProyectoId = Yii::$app->request->post('ProyectoId');
        $_ejecucion->IndicadorId = Yii::$app->request->post('IndicadorId');
        $retVal['Data'] = $_ejecucion;
        if(isset($_ejecucion->ProyectoId) && isset($_ejecucion->IndicadorId)) {

            $_BNIndicadores = new BNIndicadores();
            $_BNIndicadores->Eliminar($_ejecucion);
        
            $retVal['Msj'] = 'El indicador se eliminó correctamente.';
        }

        header('Content-Type: application/json');    
        return json_encode($retVal);   
    }

}