<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\PasoCritico;
use app\models\Usuario;
use app\models\Persona;
use app\models\Investigador;
use app\models\Proyecto;
use app\models\InformacionGeneral;
use app\models\Poa;
use app\models\Componente;
use app\models\Actividad;
use app\models\CronogramaProyecto;
use app\models\ActRubroElegible;
use app\models\CronogramaComponente;
use app\models\CronogramaActividad;
use app\models\CronogramaActRubroElegible;
use app\models\RubroElegible;
use app\models\AreSubCategoria;
use app\models\CronogramaAreSubCategoria;
use app\models\MarcoLogicoActividad;
use app\models\AreSubCategoriaObservacion;
use app\models\Requerimiento;
use app\models\TerminoReferencia;
use app\models\Tarea;
use yii\web\UploadedFile;


class TerminoReferenciaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($CodigoProyecto=NULL)
    {
        $this->layout='estandar';
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        return $this->render('index',['CodigoProyecto'=>$CodigoProyecto]);
    }
    
    public function actionListaTerminosReferencias($CodigoProyecto=null)
    {
        if($CodigoProyecto)
        {
            
        }
        else{
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        }
        
        $resultados = (new \yii\db\Query())
            ->select('TerminoReferencia.ID,TerminoReferencia.TipoServicio,TerminoReferencia.FechaRegistro,TerminoReferencia.Documento,TerminoReferencia.Situacion')
            ->from('InformacionGeneral')
            ->innerJoin('Investigador','InformacionGeneral.InvestigadorID=Investigador.ID')
            ->innerJoin('Usuario','Usuario.ID=Investigador.UsuarioID')
            ->innerJoin('Proyecto','Proyecto.InvestigadorID=Investigador.ID')
            ->innerJoin('TerminoReferencia','TerminoReferencia.CodigoProyecto=InformacionGeneral.Codigo')
            ->where(['InformacionGeneral.Codigo'=>$CodigoProyecto])
            ->distinct()
            ->all();

        $nro=0;
        foreach($resultados as $result)
        {
            $nro++;
            echo "<tr>";
            echo "<td> N° Término " . $result["ID"] . "</td>";
            echo "<td>" . $this->getTipoServicio($result["TipoServicio"]) . "</td>";
            echo "<td>" . $result["FechaRegistro"] . "</td>";
            echo "<td>" . $this->getSituacion($result["Situacion"]) . "</td>";
            echo "<td><a target='_blank' href='terminosreferencias/" . $result["Documento"] . "'><span>descargar</span></a></td>";
            if($result["Situacion"]==0)
            {
                echo "<td><a href='#' class='btn-edit-termino' data-id='".$result["ID"]."'><i class='fa fa-edit fa-lg'></i></a> <a href='#' class='btn-remove' data-id='".$result["ID"]."'><i class='fa fa-remove fa-lg'></i></a></td>";    
            }
            else
            {
                echo "<td></td>";
            }
            
            echo "</tr>";
        }
    }
    
    public function actionCrear($CodigoProyecto)
    {
        $this->layout='vacio';
        $informacionGeneral=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacionGeneral->InvestigadorID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
        $componentes=Componente::find()->where('PoaID=:PoaID',[':PoaID'=>$poa->ID])->all();
        $terminoReferencia=new TerminoReferencia;
        if($terminoReferencia->load(Yii::$app->request->post())){
            $Componente=Componente::findOne($terminoReferencia->ComponenteID);
            $Actividad=Actividad::findOne($terminoReferencia->ActividadID);
            $Recurso=AreSubCategoria::findOne($terminoReferencia->AreSubCategoriaID);
            //var_dump($Componente);die;
            $terminoReferencia->CodigoPoa="O.".$Componente->Correlativo." A.".$Actividad->Correlativo." R.".$Recurso->Correlativo;
            
            
            $terminoReferencia->Situacion=0;
            $terminoReferencia->Estado=1;
            $terminoReferencia->FechaRegistro=date('Ymd');
            $terminoReferencia->save();
            
            $terminoReferencia->archivo = UploadedFile::getInstance($terminoReferencia, 'archivo');
            
            if($terminoReferencia->archivo)
            {
                $terminoReferencia->archivo->saveAs('terminosreferencias/' . $terminoReferencia->ID . '.' . $terminoReferencia->archivo->extension);
                $terminoReferencia->Documento=$terminoReferencia->ID . '.' . $terminoReferencia->archivo->extension;
            }
            $terminoReferencia->update();
            return $this->redirect(['/termino-referencia']);
        }
        return $this->render('_form',['CodigoProyecto'=>$CodigoProyecto,'componentes'=>$componentes]);
    }
    
    public function actionActualizar($ID=null)
    {
        $this->layout='vacio';
        $terminoReferencia=TerminoReferencia::find()
                ->select('TerminoReferencia.*,Componente.ID as ComponenteID,Actividad.ID as ActividadID,AreSubCategoria.ID as AreSubCategoriaID')
                ->innerJoin('AreSubCategoria','AreSubCategoria.ID=TerminoReferencia.AreSubCategoriaID')
                ->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')
                ->innerJoin('Actividad','Actividad.ID=ActRubroElegible.ActividadID')
                ->innerJoin('Componente','Componente.ID=Actividad.ComponenteID')
                ->where('TerminoReferencia.ID=:ID',['ID'=>$ID])->one();
                
        $informacionGeneral=InformacionGeneral::find()->where('Codigo=:Codigo',[':Codigo'=>$terminoReferencia->CodigoProyecto])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$informacionGeneral->InvestigadorID])->one();
        $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
        $componentes=Componente::find()->where('PoaID=:PoaID',[':PoaID'=>$poa->ID])->all();
        $actividades=Actividad::find()->select('ID,Nombre,Correlativo')->where('ComponenteID=:ComponenteID',[':ComponenteID'=>$terminoReferencia->ComponenteID])->groupBy('ID,Nombre,Correlativo')->orderBy('Correlativo')->all();
        $recursos=AreSubCategoria::find()->select('AreSubCategoria.ID,AreSubCategoria.Nombre')->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')->where('ActRubroElegible.ActividadID=:ActividadID',[':ActividadID'=>$terminoReferencia->ActividadID])->groupBy('AreSubCategoria.ID,AreSubCategoria.Nombre,AreSubCategoria.Correlativo')->orderBy('AreSubCategoria.Correlativo')->all();
        
        if($terminoReferencia->load(Yii::$app->request->post())){
            $terminoReferencia->save();
            $terminoReferencia->archivo = UploadedFile::getInstance($terminoReferencia, 'archivo');
            if($terminoReferencia->archivo)
            {
                $terminoReferencia->archivo->saveAs('terminosreferencias/' . $terminoReferencia->ID . '.' . $terminoReferencia->archivo->extension);
                $terminoReferencia->Documento=$terminoReferencia->ID . '.' . $terminoReferencia->archivo->extension;
            }
            $terminoReferencia->update();
            return $this->redirect(['/termino-referencia']);
        }
        return $this->render('_form_actualizar',['termino'=>$terminoReferencia,'ID'=>$ID,'componentes'=>$componentes,'actividades'=>$actividades,'recursos'=>$recursos]);
    }
    
    
    public function actionActividades()
    {
        if(isset($_POST['ComponenteID']) && trim($_POST['ComponenteID'])!='')
        {
            $ComponenteID=$_POST['ComponenteID'];
            $countActividades=Actividad::find()->select('ID,Nombre')->where('ComponenteID=:ComponenteID',[':ComponenteID'=>$ComponenteID])->groupBy('ID,Nombre')->count();
            $actividades=Actividad::find()->select('ID,Nombre,Correlativo')->where('ComponenteID=:ComponenteID',[':ComponenteID'=>$ComponenteID])->groupBy('ID,Nombre,Correlativo')->orderBy('Correlativo asc')->all();
            if($countActividades>0){
                echo "<option value>Seleccionar</option>";
                foreach($actividades as $actividad){
                    echo "<option value='".$actividad->ID."'>".$actividad->Correlativo." ".$actividad->Nombre."</option>";
                }
            }
            else{
                echo "<option value>Seleccionar</option>";
            }
        }
    }
    
    
    public function actionRecursos()
    {
        if(isset($_POST['ActividadID']) && trim($_POST['ActividadID'])!='')
        {
            $ActividadID=$_POST['ActividadID'];
            
            $countRecursos=AreSubCategoria::find()->select('AreSubCategoria.ID,AreSubCategoria.Nombre')->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')->where('ActRubroElegible.ActividadID=:ActividadID',[':ActividadID'=>$ActividadID])->groupBy('AreSubCategoria.ID,AreSubCategoria.Nombre')->count();
            $recursos=AreSubCategoria::find()->select('AreSubCategoria.ID,AreSubCategoria.Nombre')->innerJoin('ActRubroElegible','ActRubroElegible.ID=AreSubCategoria.ActRubroElegibleID')->where('ActRubroElegible.ActividadID=:ActividadID',[':ActividadID'=>$ActividadID])->groupBy('AreSubCategoria.ID,AreSubCategoria.Nombre,AreSubCategoria.Correlativo')->orderBy('AreSubCategoria.Correlativo')->all();
            if($countRecursos>0){
                echo "<option value>Seleccionar</option>";
                foreach($recursos as $recurso){
                    echo "<option value='".$recurso->ID."'>".$recurso->Nombre."</option>";
                }
            }
            else{
                echo "<option value>Seleccionar</option>";
            }
        }
    }
    
    public function actionRecursosReprogramacion()
    {
        if(isset($_POST['ActividadID']) && trim($_POST['ActividadID'])!='')
        {
            $ActividadID=$_POST['ActividadID'];
            $data=[];
            $RubrosElegibles=ActRubroElegible::find()
                            ->select('RubroElegible.Nombre,ActRubroElegible.ID,ActRubroElegible.RubroElegibleID')
                            ->innerJoin('RubroElegible','RubroElegible.ID=ActRubroElegible.RubroElegibleID')
                            ->where('ActividadID=:ActividadID',[':ActividadID'=>$ActividadID])
                            ->all();
                            
            foreach($RubrosElegibles as $RubroElegible)
            {
                $data[]=[
                       'ID'=>$RubroElegible->ID,
                       'Nombre'=>$RubroElegible->Nombre,
                       'Recursos'=>$this->Recursos($RubroElegible->ID,$RubroElegible->RubroElegibleID)
                       ];
            }
            echo json_encode($data);
        }
    }
    
    public function actionTareasReprogramacion()
    {
        if(isset($_POST['ActividadID']) && trim($_POST['ActividadID'])!='')
        {
            $ActividadID=$_POST['ActividadID'];
            $data=[];
            $tareas=Tarea::find()
                            ->where('ActividadID=:ActividadID',[':ActividadID'=>$ActividadID])
                            ->all();
                            
            foreach($tareas as $tarea)
            {
                $data[]=[
                       'ID'=>$tarea->ID,
                       'Nombre'=>$tarea->Descripcion,
                       'UnidadMedida'=>$tarea->UnidadMedida,
                       'MetaFisica'=>$tarea->MetaFisica,
                       ];
            }
            echo json_encode($data);
        }
    }
    
    private function Recursos($ActRubroElegibleID=null,$RubroElegibleID)
    {
        $data=[];
        $recursos=AreSubCategoria::find()->where('AreSubCategoria.ActRubroElegibleID=:ActRubroElegibleID',[':ActRubroElegibleID'=>$ActRubroElegibleID])->all();
        foreach($recursos as $recurso)
        {
            $data[]=['ID'=>$recurso->ID,'Codificacion'=>$RubroElegibleID.'.'.$recurso->Codificacion,'Recurso'=>$recurso->Nombre.'/'.$recurso->Especifica,'UnidadMedida'=>$recurso->UnidadMedida,'CostoUnitario'=>$recurso->CostoUnitario,'MetaFisica'=>$recurso->MetaFisica];
        }
        return $data;
    }
    
    public function getTipoServicio($tipoServicio=null)
    {
        if($tipoServicio==1)
        {
            return "Servicio";
        }
        else
        {
            return "compra";
        }
    }
    
    public function getSituacion($situacion=null)
    {
        if($situacion==0)
        {
            return "Pendiente de Anexar";
        }
        elseif($situacion==1)
        {
            return "Anexado a Requerimiento";
        }
        else if($situacion==2)
        {
            return "Anexado a Orden de Servicio";
        }
    }
    public function actionEliminar($id=null)
    {
        TerminoReferencia::findOne($id)->delete();
        $arr = array('Success' => true);
        echo json_encode($arr);
    }
    
    public function getUbicacion($archivo)
    {
        
    }
}
