<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\InformacionGeneral;
use app\models\ProgramaTransversal;
use app\models\DireccionLinea;
use app\models\TipoInvestigacion;
use app\models\UnidadEjecutora;
use app\models\UnidadOperativa;
use app\models\Investigador;
use app\models\EntidadParticipante;
use app\models\TipoInstitucion;
use app\models\RepresentanteLegal;
use app\models\Persona;
use app\models\Usuario;

class EntidadParticipanteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout='estandar';
        if(\Yii::$app->user->identity->rol!=7)
        {
            return $this->redirect(['panel/index']) ;
        }
        return $this->render('entidades');
    }

    public function actionRecursos()
    {
        $this->layout='estandar';
        return $this->render('recursos');
    }


    //////////////////////////
    // ENTIDADES PARTICIPANTES
    //////////////////////////

    ## Vista Crear Entidades AJAX
        public function actionEntidadescrear($id = null){
            $TipoInstitucion=TipoInstitucion::find()->all();
            
            if(!is_null($id)){
                $model = EntidadParticipante::findOne($id);
            }else{
                $model = new EntidadParticipante();
            }
            //var_dump($model->AporteNoMonetario);die;
            $this->layout='vacio';
            return $this->render('_create_entidades',['TipoInstitucion'=>$TipoInstitucion,'Model' => $model ]);
        }

    ## Crear Entidades Participantes
        public function actionCrear($id = null){
            $this->layout='vacio';
            $model = new EntidadParticipante();
            if ( $model->load(Yii::$app->request->post()) ) {
                $model->FechaCreacion   = date('Y-m-d',strtotime($model->FechaCreacion));

                $usuario=Usuario::findOne(\Yii::$app->user->id);
                $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
                    
                $exists = EntidadParticipante::find()->where('InvestigadorID=:InvestigadorID AND Ruc=:Ruc',[':InvestigadorID'=>$usuario->id,':Ruc'=>$model->Ruc])->one();

                
                if(!$exists){
                    $investigador=$investigador->ID;
                    $model->InvestigadorID  = $investigador;
                    $model->AporteNoMonetario = str_replace(",", "", $model->AporteNoMonetario);
                    $model->AporteMonetario = str_replace(",", "", $model->AporteMonetario);
                    $model->Tipo            = 1;
                    $model->Estado          = 1;
                    
                    $model->insert();

                    $id = $model->ID;
                    if($id){
                        $arr = array(
                            'Success' => true,
                            'EntidadID' => $id
                        );
                    }else{
                        $arr = array(
                            'Error' => true,
                            'Message' => 'No se pudo insertar'
                        );
                    }
                }else{
                    $arr = array(
                        'Error' => true,
                        'Message' => 'Ya Existe Entidad Participante'
                    );
                }
                echo json_encode($arr);
            }
        }

    ## Eliminar Entidades Participantes
        public function actionDeletentidades(){
            $this->layout='vacio';
            $ds = Yii::$app->request->post('id');

            $customer = EntidadParticipante::findOne($ds);
            $customer->Estado = 0;
            $del = $customer->update();
            $repr = RepresentanteLegal::deleteAll(['EntidadParticipanteID'=>$customer->ID]);

            if($del==1){
                $arr = array(
                    'Success' => true
                );
            }else{
                $arr = array(
                    'Error' => "No se pudo eliminar Entidad"
                );
            }
            echo json_encode($arr);
        }

    ## Actualizar Entidades Participantes
        public function actionActualizaentidades($id = null){
            $this->layout='vacio';
            $model = EntidadParticipante::findOne($id);
            
            if ( $model->load(Yii::$app->request->post()) ) {
                //$model->AporteNoMonetario=null;
                $model->AporteNoMonetario = str_replace(",", "", $model->AporteNoMonetario);
                $model->AporteMonetario = str_replace(",", "", $model->AporteMonetario);
                $model->update();
                $arr = array(
                    'Success' => true
                    );
                echo json_encode($arr);
            }
        }

    ## Listado de Entidades Participantes
        public function actionEntidadeslistado(){
            $this->layout='vacio';
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $investigador=$investigador->ID;

            $EntidadParticipante = EntidadParticipante::find()
                ->select('Investigador.UsuarioID,
                        EntidadParticipante.ID as EntidadID,
                        EntidadParticipante.RazonSocial,
                        EntidadParticipante.FechaCreacion,
                        EntidadParticipante.Ruc,
                        TipoInstitucion.Nombre')
                ->innerJoin('Investigador','Investigador.ID = EntidadParticipante.InvestigadorID')
                ->innerJoin('TipoInstitucion','EntidadParticipante.TipoInstitucionID = TipoInstitucion.ID ')
                ->where(['EntidadParticipante.Estado'=>1,'EntidadParticipante.InvestigadorID'=>$investigador])
                ->all();

            return $this->render('_listado',['EntidadParticipante'=>$EntidadParticipante]);
        }


    /////////////////////////
    // REPRESENTENTES LEGALES
    /////////////////////////

    ## Listado de representantes Legales
        public function actionRepresentanteslegales($epID = null){
            $this->layout='vacio';
            
            if( !is_null($epID) && !empty($epID) && is_numeric($epID) ){
                $RepresentanteLegal = RepresentanteLegal::find()
                    ->select('Persona.Nombre,
                            Persona.ApellidoPaterno,
                            Persona.ApellidoMaterno,
                            Persona.NroDocumento,
                            Persona.Telefono,
                            Persona.Email,
                            RepresentanteLegal.ID as Codigo,
                            RepresentanteLegal.Cargo')
                    ->innerJoin('Persona','Persona.ID = RepresentanteLegal.PersonaID')
                    ->innerJoin('EntidadParticipante','EntidadParticipante.ID = RepresentanteLegal.EntidadParticipanteID ')
                    ->where(['RepresentanteLegal.EntidadParticipanteID'=>$epID])
                    ->all();
            }else{
                $RepresentanteLegal = array();
            }

            return $this->render('_representantes_legales',['RepresentanteLegal'=>$RepresentanteLegal]);
        }

    ## Vista Crear Representantes Legales AJAX    
        public function actionRepresentantelegalcrear($epID = null,$id = null){
            $this->layout='vacio';
            $TipoInstitucion=TipoInstitucion::find()->all();
             if( !is_null($epID) ){
                $idx = $epID;
            }else{
                $idx = '';
            }

            if(!is_null($id)){
                // $model = RepresentanteLegal::findOne($id);
                $model = RepresentanteLegal::find()
                    ->select('Persona.Nombre,
                            Persona.ApellidoPaterno,
                            Persona.ApellidoMaterno,
                            Persona.NroDocumento,
                            Persona.Telefono,
                            Persona.Email,
                            RepresentanteLegal.ID as Codigo,
                            RepresentanteLegal.Cargo')
                    ->innerJoin('Persona','Persona.ID = RepresentanteLegal.PersonaID')
                    ->innerJoin('EntidadParticipante','EntidadParticipante.ID = RepresentanteLegal.EntidadParticipanteID ')
                    ->where(['RepresentanteLegal.ID'=>$id])
                    ->one();
            }else{
                $model = new RepresentanteLegal();
            }
            
            return $this->render('_representante_legal_create',['TipoInstitucion'=>$TipoInstitucion,'Id'=>$idx, 'Modelor' => $model]);
        }

    ## Crear Representantes Legales
        public function actionCrearrepresentante(){
            $this->layout='vacio';
            $model = new Persona();
            if ( $model->load(Yii::$app->request->post()) ) {
                $exists = Persona::find()->where('NroDocumento=:NroDocumento',[':NroDocumento'=>$model->NroDocumento])->one();
                if(empty($exists)){
                    $model->insert();
                    $id = $model->ID;
                }else{
                    $id = $exists->ID;
                }
                

                $model2 = new RepresentanteLegal();
                $model2->load(Yii::$app->request->post());

                $repLegal = RepresentanteLegal::find()->where('PersonaID=:PersonaID AND EntidadParticipanteID=:EntidadParticipanteID',[':PersonaID'=>$id,':EntidadParticipanteID'=>$model2->EntidadParticipanteID])->one();
                if(empty($repLegal)){
                    $model2->PersonaID  = $id;
                    $ins = $model2->insert();
                    if($ins ==1){
                        $arr = array(
                            'Success' => true,
                            'id'      => $model2->EntidadParticipanteID
                        );
                    }else{
                        $arr = array(
                            'Error' => true,
                            'Message' => 'No se pudo insertar representante'
                        );
                    }
                }else{
                    $arr = array(
                            'Error' => true,
                            'Message' => 'Representante legal ya existe'
                        );
                }
                echo json_encode($arr);
            }

        }

    ## Actualizar Entidades Participantes
        public function actionActualizarepresentante($id = null){
            $this->layout='vacio';
            
            $model = RepresentanteLegal::findOne($id);
            if ( $model->load(Yii::$app->request->post()) ) {
                $model->update();
                // print_r($model->ID);
                // die();
                $model2 = Persona::findOne($model->ID);
                $model2->load(Yii::$app->request->post());
                $upd = $model2->update();
                // if($upd == 1){
                // }else{
                //     $arr = array(
                //         'Error' => "No se pudo Actualizar"
                //         );
                // }
                    $arr = array(
                        'Success' => true
                        );

                echo json_encode($arr);
            }
        }

    ## Actualizar Entidades Participantes
        public function actionDeleteResentante($id = null){
            $this->layout='vacio';        
            $model = RepresentanteLegal::findOne($id);
            $sa = $model->delete();
            if($sa == 1){
                $arr = array(
                    'Success' => true,
                    'id'      => $model->EntidadParticipanteID
                );
            }else{
                $arr = array(
                    'Error' => "No se pudo eliminar"
                );
            }
            echo json_encode($arr);
        }

}

