<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\InformacionGeneral;
use app\models\ProgramaTransversal;
use app\models\DireccionLinea;
use app\models\TipoInvestigacion;
use app\models\UnidadEjecutora;
use app\models\UnidadOperativa;
use app\models\Investigador;
use app\models\EntidadParticipante;
use app\models\TipoInstitucion;
use app\models\RepresentanteLegal;
use app\models\Persona;
use app\models\Usuario;
use app\models\Requerimiento;
use app\models\Padron;

use app\models\TipoGasto;
use app\models\Rendicion;
use app\models\Viatico;
use app\models\Situacion;
use app\models\RendicionViatico;
use app\models\DetalleRendicionViatico;


use PhpOffice\PhpWord\PhpWord;

class RendicionController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $this->layout='estandar';
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $CodigoProyecto = $informacionGeneral->Codigo;
        return $this->render('index',['CodigoProyecto'=> $CodigoProyecto]);
    }

    public function actionListaRendicion($CodigoProyecto=null)
    {
        if($CodigoProyecto)
        {
            
        }
        else{
        }
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $CodigoProyecto=$informacionGeneral->Codigo;
        

        $resultados = (new \yii\db\Query())
            ->select('Viatico.Correlativo, Viatico.Annio,Viatico.Apellido,Viatico.Nombre,RendicionViatico.Situacion,Viatico.ID,Viatico.CodigoProyecto,Viatico.Dni,RendicionViatico.AnticipoRecibido,RendicionViatico.GastoSustentado,RendicionViatico.Reintegrar,RendicionViatico.ID RendicionViatID')
            ->from('RendicionViatico')
            ->innerJoin('DetalleRendicionViatico','DetalleRendicionViatico.RendicionID=RendicionViatico.ID')
            ->innerJoin('Viatico','Viatico.ID=RendicionViatico.ViaticoID')
            ->where('RendicionViatico.CodigoProyecto=:CodigoProyecto',[':CodigoProyecto'=>$CodigoProyecto])
            ->distinct()
            ->all();




        // $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();


        $nro=0;
        foreach($resultados as $result)
        {
            // print_r($result);die();
            $nro++;
            echo "<tr>";
                echo "<td> N° " . $result["Correlativo"]."-".$result["Annio"] . "</td>";
                echo "<td>" . $result["Apellido"].' '.$result["Nombre"] . "</td>";
                echo "<td>" . $result["Dni"] . "</td>";
                echo "<td>" . $result["AnticipoRecibido"] . "</td>";
                echo "<td>" . $result["GastoSustentado"] . "</td>";
                echo "<td>" . $result["Reintegrar"] . "</td>";
                echo "<td>
                    <a class='btn btn-default' target='_blank' href='".\Yii::$app->request->BaseUrl."/viatico/plantilla-rendicion?ID=".$result['ID']."&CodigoProyecto=".$CodigoProyecto."'><span class='fa fa-cloud-download'></span> Formato Rendición</a>
                    <a href='#'' class='btn btn-danger btn-remove' data-id='".$result['RendicionViatID']."'><i class='fa fa-remove fa-lg'></i></a>
                    </td>";
            echo "</tr>";
        }
    }


    public function TipoGastoName($TipoGasto)
    {
        $tipo=TipoGasto::find()->where('ID=:ID AND Rendicion=1',[':ID'=>$TipoGasto])->one();
        if($tipo)
        {
            return $tipo->Nombre;
        }
        else
        {
            return 'no';
        }
    }

    public function Situacion($tipo)
    {
        $tipo=Situacion::find()->where('ID=:ID',[':ID'=>$tipo])->one();
        if($tipo)
        {
            return $tipo->Descripcion;
        }
        else
        {
            return 'no';
        }
    }

    public function actionGenerarRendicion(){
        $this->layout='vacio';
        $tipoGasto = TipoGasto::find()->where('Rendicion=1')->all();
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $informacionGeneral=InformacionGeneral::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $CodigoProyecto=$informacionGeneral->Codigo;

        return $this->render('_form_rendicion',['CodigoProyecto'=>$CodigoProyecto,'tipoGasto' => $tipoGasto,'listaRendicion'=> $listaRendicion]);
    }

    public function actionTipo($tipo = null){
        if($tipo == 4){
            $viatico = Viatico::find()->where('ID NOT IN (SELECT TransaccionID FROM Rendicion) AND Estado = 1')->all();
            foreach ($viatico as $value) {
                $aa[] = array(
                    'Success'   => true,
                    'ID'            => $value->ID,
                    'Correlativo'   => "Viatico Nº ".$value->Correlativo."-".$value->Annio."-".$value->Destino
                    );
            }
        }else{
            $aa = array(
                'Success'   => true,
                'ID' => '',
                'Correlativo' => ''
                );
        }

        echo json_encode($aa);
    }

    public function CorrelativoRendicion($CodigoProyecto)
    {
        $correl=Rendicion::find()->where('CodigoProyecto=:CodigoProyecto',[':CodigoProyecto'=>$CodigoProyecto])->orderBy('Correlativo desc')->one();
        if($correl)
        {
            return $correl->Correlativo+1;
        }
        else
        {
            return 1;
        }
    }

    public function actionEliminar($id)
    {
        $this->layout='vacio';
        $delx = DetalleRendicionViatico::find()->where(['RendicionID'=>$id])->one()->delete();
        $del = RendicionViatico::findOne($id)->delete();
        $arr = array('Success' => true);
        echo json_encode($arr);
    }


}

