<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\InformacionGeneral;
use app\models\ProgramaTransversal;
use app\models\DireccionLinea;
use app\models\TipoInvestigacion;
use app\models\UnidadEjecutora;
use app\models\UnidadOperativa;
use app\models\Persona;
use app\models\Programa;
use app\models\CultivoCrianza;
use app\models\Especie;
use app\models\Ubigeo;
use app\models\Usuario;
use app\models\Investigador;
use app\models\MarcoLogicoPropositoProyecto;
use app\models\Componente;
use app\models\Poa;
use app\models\Proyecto;
use app\models\MarcoLogicoComponente;
use app\models\MarcoLogicoActividad;
use app\models\Actividad;
use app\models\AreSubCategoria;
use app\models\EntidadParticipante;
use app\models\Pac;
use app\models\Categoria;
use app\models\PacDetalle;
use app\models\ActRubroElegible;
use app\models\RubroElegible;
use app\models\IndicadorPAC;


class PlanAdquisicionesController extends Controller
{

    public $investigador = NULL;
    
    public function init(){
        // parent::__construct();
        
        $usuario            = Usuario::findOne(\Yii::$app->user->id);
        
        $datosInvestigador  = Persona::findOne($usuario->PersonaID);
        $investigador       = Investigador::find()->where(['UsuarioID'=>$usuario->id])->one();
        $this->investigador = $investigador->ID;
        
    }
    

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionIndex($investigador=null,$anio=null)
    {
        $this->layout='estandar';
        if(\Yii::$app->user->identity->rol!=7)
        {
            return $this->redirect(['panel/index']) ;
        }
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $model = $this->InformacionGeneral($investigador->ID);
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $aax = Pac::find()
            ->where(['Anio'=>$anio,'ProyectoID'=>$proyecto->ID])
            ->one();
            //print_r($aax); die();
        
        return $this->render('index',['infoGeneral' => $model,'anio'=>$anio,'Situacion' => $aax->Situacion]);
    }


    public function InformacionGeneral($investigador=null)
    {
        $model=InformacionGeneral::find()
                ->select('Proyecto.Fin,Proyecto.Proposito,Proyecto.ID,InformacionGeneral.TituloProyecto')
                ->innerJoin('Investigador','Investigador.ID=InformacionGeneral.InvestigadorID')
                ->innerJoin('Proyecto','Proyecto.InvestigadorID=Investigador.ID')
                ->where('InformacionGeneral.InvestigadorID=:InvestigadorID',['InvestigadorID'=>$investigador])
                ->one();
                
        if(!$model)
        {
            $model=new InformacionGeneral;
        }
        return $model;
    }

    public function actionListado(){
        $this->layout='estandar';
        
        return $this->render('listado');
    }

    public function actionDetallePlanAdquisicion($anio = null){
        $this->layout='estandar';
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();

        $aax = Pac::find()
            ->where(['Anio'=>$anio,'ProyectoID'=>$proyecto->ID])
            ->one();

        $detalle = PacDetalle::find()
            ->select('PacDetalle.NombrePac Nombre,AreSubCategoria.Especifica,AreSubCategoria.Detalle,PacDetalle.UnidadMedidaPac UnidadMedida,PacDetalle.CostoUnitarioPac CostoUnitario,Categoria.Nombre as Categoria,PacDetalle.Cantidad,PacDetalle.MetodoAdquisicion,PacDetalle.FechaInicioPA,EntidadParticipante.RazonSocial,EntidadParticipante.AporteMonetario,PacDetalle.ID,Categoria.ID Codigo,PacDetalle.Situacion,PacDetalle.NumeracionPAC,PacDetalle.Observaciones,PacDetalle.Codificacion')
            ->innerJoin('AreSubCategoria','AreSubCategoria.ID=PacDetalle.AreSubCategoriaID')
            ->innerJoin('Categoria','Categoria.ID=PacDetalle.CategoriaID')
            ->innerJoin('Pac','Pac.ID=PacDetalle.PacID')
            ->innerJoin('EntidadParticipante','EntidadParticipante.ID=AreSubCategoria.EntidadParticipanteID')
            ->where(['PacDetalle.PacID'=>$aax->ID])
            ->orderBy('PacDetalle.CategoriaID')
            ->all();
        return $this->render('detalle_pa',['detallepa' => $detalle,'anio'=>$anio,'Situacion'=>$aax->Situacion,'Codigo'=>$usuario->username,'Pac'=>$aax->ID ]);
    }

    public function actionListadoPlanAdquisicion(){
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();

        $aaxs = Pac::find()
            ->where(['ProyectoID'=>$proyecto->ID])
            ->all();
          
        $nro=0;  
          // print_r($investigador->ID);
        foreach($aaxs as $result)
        {
            // print_r($result);
            $nro++;
            echo "<tr>";
            echo "<td width='80'>" . $nro . "</td>";
            echo "<td> PAC - " . $result["Anio"] . "</td>";
            // echo "<td>" . $result["Anio"] . "</td>";
            // echo "<td>" . $this->Situacion($result["Situacion"]) . "</td>";
            if($result['Anio'] <= date('Y')){
                echo '<td width="100"><a class="btn btn-primary" href="../plan-adquisiciones/detalle-plan-adquisicion?anio='.$result["Anio"].'"><i class="fa fa-check-square"></i> Detalle</a></td>';
                if($result['Situacion'] == 0){
                    echo '<td width="100"><a class="btn btn-info" href="../plan-adquisiciones?anio='.$result["Anio"].'"><i class="fa fa-check-square-o"></i> Generar PAC</a></td>';
                }else{
                    echo '<td width="100"><a class="btn btn-success" href="'.\Yii::$app->request->BaseUrl.'/plan-adquisiciones/descargar-pa?codigo='.$usuario->username.'&anio='.$result["Anio"].'"><i class="fa fa-download"></i> Descargar</a></td>';
                }
            }else{
                echo '<td colspan="2"><strong>Aun no existe apertura de registro</strong></td>';
            }
            // echo "<td>".$this->Accion(date('Y'),$result["ID"])."</td>";
            echo "</tr>";
        }
    }

    public function Accion($anio,$id)
    {
        $descripcion='';
        $anterior = $anio -1;
        $futuro = $anio +1;
        if($anio == $anio){
            $descripcion='<a class="btn primary" href="../plan-adquisiciones?anio='.$anio.'>VER PAC</a>';
        }else if($anio > $anterior){
            // $descripcion='<a class="btn" href="" >VER PAC</a>';
        }else{
            $descripcion = '';
        }
        
        // if($anio==2)
        // {
        //     $descripcion='<a class="btn" href="evaluacion-proyecto/evaluacion-proyecto?investigadorID='.$id.'">Evaluar POA</a>';
        // }
        return $descripcion;
    }
    
    public function actionPreview(){
        $this->layout='vacio';
        $as = array();
        $catx = array();
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $aaxs = Pac::find()
            ->where(['ProyectoID'=>$proyecto->ID,'anio'=>$_POST['Anio']])
            ->one();
        foreach ($_POST['PrevisualizacionPac'] as $value) {

            // $code = Yii::$app->db->createCommand("SELECT TOP 1 AreSubCategoria.ID
            $codificacion2 = Yii::$app->db->createCommand("SELECT DISTINCT  AreSubCategoria.ID,ActRubroElegible.RubroElegibleID,AreSubCategoria.Codificacion,AreSubCategoria.Nombre,Componente.Correlativo Objetivo,Actividad.Correlativo Actividad
            FROM Usuario 
            INNER JOIN Investigador ON Investigador.UsuarioID = Usuario.ID 
            INNER JOIN Proyecto ON Proyecto.InvestigadorID = Investigador.ID
            INNER JOIN Poa ON Poa.ProyectoID = Proyecto.ID
            INNER JOIN Componente ON Componente.PoaID = Poa.ID
            INNER JOIN Actividad ON Actividad.ComponenteID = Componente.ID
            INNER JOIN ActRubroElegible ON ActRubroElegible.ActividadID = Actividad.ID
            INNER JOIN AreSubCategoria ON AreSubCategoria.ActRubroElegibleID = ActRubroElegible.ID
            WHERE Usuario.username = '".$usuario->username."'
            AND ActRubroElegible.RubroElegibleID = ".$value['ID']."
            AND AreSubCategoria.Codificacion = ".$value['CODI'])
            ->queryAll();

            // echo '<pre>';
            // print_r($codificacion2);
            // die;
                $vsl = AreSubCategoria::find()
                    ->select('AreSubCategoria.Nombre,AreSubCategoria.Especifica,AreSubCategoria.Detalle,AreSubCategoria.UnidadMedida,AreSubCategoria.CostoUnitario,EntidadParticipante.RazonSocial,EntidadParticipante.AporteMonetario,AreSubCategoria.Correlativo')
                    ->innerJoin('EntidadParticipante','EntidadParticipante.ID=AreSubCategoria.EntidadParticipanteID')
                    ->where('AreSubCategoria.ID=:CodigoID',['CodigoID'=>$codificacion2[0]['ID']])
                    ->one();

                // echo '<pre>';
                // print_r($vsl);
                // die;

                // $codificacion = Yii::$app->db->createCommand("SELECT DISTINCT  ActRubroElegible.RubroElegibleID,AreSubCategoria.Codificacion,AreSubCategoria.Nombre,Componente.Correlativo Objetivo,Actividad.Correlativo Actividad
                //     FROM Usuario 
                //     INNER JOIN Investigador ON Investigador.UsuarioID = Usuario.ID 
                //     INNER JOIN Proyecto ON Proyecto.InvestigadorID = Investigador.ID
                //     INNER JOIN Poa ON Poa.ProyectoID = Proyecto.ID
                //     INNER JOIN Componente ON Componente.PoaID = Poa.ID
                //     INNER JOIN Actividad ON Actividad.ComponenteID = Componente.ID
                //     INNER JOIN ActRubroElegible ON ActRubroElegible.ActividadID = Actividad.ID
                //     INNER JOIN AreSubCategoria ON AreSubCategoria.ActRubroElegibleID = ActRubroElegible.ID
                //     WHERE Usuario.username = '".$usuario->username."' AND AreSubCategoria.Nombre = '".$vsl->Nombre."'")
                // ->queryAll();


            $sx = '';
            $seg = '';
                $qq = '';
            foreach ($codificacion2 as $codi) {
                $nombre = $codi['RubroElegibleID'].".".$codi['Codificacion'].",";
                if($qq != $nombre){
                    $sx  .= $nombre;
                    $qq = $nombre;
                }
                $seg .= $codi['Objetivo'].".".$codi['Actividad'].",";
            }
            
            // echo '<pre>';
            // print_r($codificacion2[0]['ID']);
            // die;
            
            $sdx[] = array(
                'DES' => $value['DES'],
                'ID'  => $codificacion2[0]['ID'],
                'RUB'  => substr($sx, 0, -1),
                'seg'  => substr($seg, 0, -1),
                'Nombre' => $vsl->Nombre.' '.$vsl->Especifica.' '.$vsl->Detalle,
                'Fuente' => $vsl->RazonSocial,
                'MontoFuente' => $vsl->AporteMonetario,
                'UnidadMedida' => $vsl->UnidadMedida,
                'CostoUnitario' => $vsl->CostoUnitario
            );
        }

        foreach ($_POST['PrevisualizacionCab'] as $valuex) {
            $cat = Categoria::find()
                ->where('ID=:CodigoID',['CodigoID'=>$valuex['INDICADOR']])
                ->one();
            $catx[] = array(
                'id' => $cat->ID,
                'nombre' => $cat->Nombre,
                );
        }
        return $this->render( 'preview',['info' => $sdx,'cab' => $catx, 'anio'=>$_POST['Anio'],'Situacion' => $aaxs->Situacion ]);
    }

    public function actionSave(){

        // print_r(Yii::$app->request->post());
        // die();
        $proceso = Yii::$app->request->post('Proceso');
        $anio = Yii::$app->request->post('Anio');
        
        $cat = json_decode(Yii::$app->request->post('Categoria'));
        $met = json_decode(Yii::$app->request->post('Metodo'));
        $fec = json_decode(Yii::$app->request->post('Fecha'));
        $obj = json_decode(Yii::$app->request->post('Objetivo'));
        $obs = json_decode(Yii::$app->request->post('Observa'));
        $corr = json_decode(Yii::$app->request->post('Correlativo'));

        $desc = json_decode(Yii::$app->request->post('Descripcion'));
        $unid = json_decode(Yii::$app->request->post('UnidadMedida'));
        $cost = json_decode(Yii::$app->request->post('CostoUnitario'));

        if(isset($_POST['Codificacion'])){
            $codi = json_decode(Yii::$app->request->post('Codificacion'));
        }else{
            $codi = array();
        }

        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$this->investigador])->one();
        $aax = Pac::find()
            ->where(['Anio'=>$anio,'ProyectoID'=>$proyecto->ID])
            ->one();
        //     print_r($aax);
        // die();
        $ddd = array_merge($cat,$met,$fec,$obj,$obs,$corr,$codi,$desc,$unid,$cost);
        array_multisort($ddd);
        $k = ''; $c='';
        $arr_k = array();
        $arr_x = array();
        $arr_a = array();
        
        foreach ($ddd as $categ) {
            
            if( ($categ->ID != $k) && ($categ->CAT != $c)){
                $k = $categ->ID; // Id recurso
                $c = $categ->CAT; // Categoria ids
                $sxa[$k][$c][]['met']  = isset($categ->MET)   ? $categ->MET   : '0';
                $sxa[$k][$c][]['cant'] = isset($categ->CANT)  ? $categ->CANT  : '0';
                $sxa[$k][$c][]['fec']  = isset($categ->FEC)   ? $categ->FEC   : '0';
                $sxa[$k][$c][]['obj']  = isset($categ->OBJ)   ? $categ->OBJ   : '0';
                $sxa[$k][$c][]['obs']  = isset($categ->OBS)   ? $categ->OBS   : '0';
                $sxa[$k][$c][]['cor']  = isset($categ->COR)   ? $categ->COR   : '0';
                $sxa[$k][$c][]['codi']  = isset($categ->CODI)   ? $categ->CODI   : '0';

                $sxa[$k][$c][]['desc']  = isset($categ->NOM)   ? $categ->NOM   : '0';
                $sxa[$k][$c][]['unid']  = isset($categ->UNID)   ? $categ->UNID   : '0';
                $sxa[$k][$c][]['cost']  = isset($categ->COSTO)   ? $categ->COSTO   : '0';
            }else{
                if($categ->ID == $k && $categ->CAT == $c){
                    $sxa[$k][$c][]['met']  = isset($categ->MET)   ? $categ->MET   : '0';
                    $sxa[$k][$c][]['cant'] = isset($categ->CANT)  ? $categ->CANT  : '0';
                    $sxa[$k][$c][]['fec']  = isset($categ->FEC)   ? $categ->FEC   : '0';
                    $sxa[$k][$c][]['obj']  = isset($categ->OBJ)   ? $categ->OBJ   : '0';
                    $sxa[$k][$c][]['obs']  = isset($categ->OBS)   ? $categ->OBS   : '0';
                    $sxa[$k][$c][]['cor']  = isset($categ->COR)   ? $categ->COR   : '0';
                    $sxa[$k][$c][]['codi']  = isset($categ->CODI)   ? $categ->CODI   : '0';

                    $sxa[$k][$c][]['desc']  = isset($categ->NOM)   ? $categ->NOM   : '0';
                    $sxa[$k][$c][]['unid']  = isset($categ->UNID)   ? $categ->UNID   : '0';
                    $sxa[$k][$c][]['cost']  = isset($categ->COSTO)   ? $categ->COSTO   : '0';
                }else{
                    $sxa[$k][$c][]['met']  = isset($categ->MET)   ? $categ->MET   : '0';
                    $sxa[$k][$c][]['cant'] = isset($categ->CANT)  ? $categ->CANT  : '0';
                    $sxa[$k][$c][]['fec']  = isset($categ->FEC)   ? $categ->FEC   : '0';
                    $sxa[$k][$c][]['obj']  = isset($categ->OBJ)   ? $categ->OBJ   : '0';
                    $sxa[$k][$c][]['obs']  = isset($categ->OBS)   ? $categ->OBS   : '0';
                    $sxa[$k][$c][]['cor']  = isset($categ->COR)   ? $categ->COR   : '0';
                    $sxa[$k][$c][]['codi']  = isset($categ->CODI)   ? $categ->CODI   : '0';

                    $sxa[$k][$c][]['desc']  = isset($categ->NOM)   ? $categ->NOM   : '0';
                    $sxa[$k][$c][]['unid']  = isset($categ->UNID)   ? $categ->UNID   : '0';
                    $sxa[$k][$c][]['cost']  = isset($categ->COSTO)   ? $categ->COSTO   : '0';
                }
            }
        }
        $array = $this->array_filter_recursive(array_map('array_filter', $sxa));
        
        foreach ($array as $key =>$value) {
            foreach ($value as $xkey => $valor) {
                    $i = 0;
                    $ii = 0;
                    $tmetodo   = '';
                    $tcantidad = '';
                    $tfecha    = '';
                    $tobj    = '';
                    $tobs    = '';
                    $tcor   = '';
                    $tcodi = '';

                    $tnom = '';
                    $tunid = '';
                    $tcost = '';

                foreach ($valor as $ksx => $xx) {
                    if( isset($xx['met']) ){
                        $tmetodo = $xx['met'];
                        $sx1 = array('metodo'=>$tmetodo);
                        $i++;
                    }

                    if(isset ($xx['cant']) ){
                        $tcantidad = $xx['cant'];
                        $sx2 = array('cantidad'=>$tcantidad);
                        $i++;
                    }

                    if( isset($xx['fec']) ){
                        $tfecha    = $xx['fec'];
                        $sx3 = array('fecha'=>$tfecha);
                        $i++;
                    }

                    if( isset($xx['obj']) ){
                        $tobj    = $xx['obj'];
                        $sx4 = array('objetivo'=>$tobj);
                        $i++;
                    }
                    
                    if( isset($xx['obs']) ){
                        $tobs    = $xx['obs'];
                        $sx5 = array('observa'=>$tobs);
                        $i++;
                    }

                    if( isset($xx['cor']) ){
                        $tcor    = $xx['cor'];
                        $sx6 = array('correlativo'=>$tcor);
                        $i++;
                    }

                    if( isset($xx['codi']) ){
                        $tcodi    = $xx['codi'];
                        $sx7 = array('codificacion'=>$tcodi);
                        $i++;
                    }

                    if( isset($xx['desc']) ){
                        $tnom    = $xx['desc'];
                        $sx8 = array('descripcion'=>$tnom);
                        $i++;
                    }

                    if( isset($xx['unid']) ){
                        $tunid    = $xx['unid'];
                        $sx9 = array('unidad'=>$tunid);
                        $i++;
                    }

                    if( isset($xx['cost']) ){
                        $tcost    = $xx['cost'];
                        $sx10 = array('costo'=>$tcost);
                        $i++;
                    }

                    // print_r($valor);
                    // die();
                    // echo $i;

                    if($i >=10){

                        $dae = array_merge($sx1,$sx3,$sx2,$sx4,$sx5,$sx6,$sx7,$sx8,$sx9,$sx10);
                        $i = 0;
                        // print_r($dae);
                        // die();
                        // echo $key;
                        // die();
                        if ($proceso == 'guardar' ) {
                            $moPa = PacDetalle::findOne($key);
                            $moPa->Cantidad             = $dae['cantidad'];

                            $moPa->NombrePac           = $dae['descripcion'];
                            $moPa->UnidadMedidaPac     = $dae['unidad'];
                            // $moPa->CostoUnitarioPac    = str_replace(',','', $dae['costo']);

                            $moPa->MetodoAdquisicion    = $dae['metodo'];
                            $moPa->FechaInicioPA        = $dae['fecha'];
                            $moPa->NumeracionPAC        = $dae['objetivo'];
                            $moPa->Observaciones        = $dae['observa'];
                            $moPa->Correlativo          = $dae['correlativo'];
                            $moPa->Codificacion         = $dae['codificacion'];
                            $moPa->update();
                        }
                        else{
                            $moPa = new PacDetalle();
                            $moPa->AreSubCategoriaID    = $key;
                            $moPa->CategoriaID          = $xkey;
                            $moPa->Cantidad             = $dae['cantidad'];

                            $moPa->NombrePac           = $dae['descripcion'];
                            $moPa->UnidadMedidaPac     = $dae['unidad'];
                            $moPa->CostoUnitarioPac    = str_replace(',','', $dae['costo']);

                            $moPa->MetodoAdquisicion    = $dae['metodo'];
                            $moPa->FechaInicioPA        = $dae['fecha'];
                            $moPa->NumeracionPAC        = $dae['objetivo'];
                            $moPa->Observaciones        = $dae['observa'];
                            $moPa->Correlativo          = $dae['correlativo'];
                            $moPa->Codificacion         = $dae['codificacion'];
                            $moPa->Estado               = 1;
                            $moPa->PacID                = $aax->ID;
                            $moPa->insert();
                            // echo '<pre>';print_r($moPa);

                        }
                    }
                }
            }
        }
        $arr = array('Success' => true);
        echo json_encode($arr);
    }


    public function actionSaveSend(){
        $idd = $_POST['id'];
        $pacMod = Pac::findOne($idd);
        $pacMod->Situacion = 2;
        $pacMod->update();
    }

    private function array_filter_recursive($input) 
    { 
       foreach ($input as &$value) 
        { 
            if (is_array($value)) 
            { 
                $value = $this->array_filter_recursive($value); 
            } 
       }     
       return array_filter($input); 
    }

    public function actionDelete(){
        $recurso = Yii::$app->request->post('recurso');
        $moPa = PacDetalle::findOne($recurso);
        $moPa->delete();
    }

    public function actionRecursosJsonPa($anio = null){
        $this->layout='vacio';
        // Proyectos
        $usuario            = Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador  = Persona::findOne($usuario->PersonaID);
        $investigador       = Investigador::find()->where(['UsuarioID'=>$usuario->id])->one();
        
        $proyecto = Poa::find()
        ->select('Poa.ID, Poa.ProyectoID')
        ->innerJoin('Proyecto','Poa.ProyectoID = Proyecto.ID')
        ->where(['Proyecto.InvestigadorID'=> $investigador->ID,'Poa.Estado' => 1])
        ->one();

        $categoria = Categoria::find()->select('ID,Nombre')->where(['Estado' => 1])->all();
        foreach ($categoria as $cat) {
            $cate[] = array(
                'ID' => $cat->ID,
                'Nombre' => $cat->Nombre
            );
        }
        $proyectox = array(
            'ID'       => $proyecto->ProyectoID, 
            'Rubro'    => $this->get_actRubroElegible($anio,$usuario->username),
            'Categoria'=> $cate
        );
        echo json_encode($proyectox); die();
    }

    public function actionComponenteForm($recurso = null){
        $this->layout='vacio';
        // Agregar ID de recurso
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();

        $proyecto = Poa::find()
            ->select('Poa.ID, Poa.ProyectoID')
            ->innerJoin('Proyecto','Poa.ProyectoID = Proyecto.ID')
            ->where(['Proyecto.InvestigadorID'=> $investigador->ID,'Poa.Estado' => 1])
            ->one();
        $resultado  = Componente::find()
                ->where(['PoaID'=>$proyecto->ID])
                ->orderBy('Correlativo')
                ->all();
        return $this->render( 'act_componente',['Componente'=>$resultado,'recurso' => $recurso,'json'=> $_POST['valor']]);
    }

    public function actionObjetivos($componente = null,$actividad = null){
        $this->layout='vacio';
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();

        $proyecto = Poa::find()
            ->select('Poa.ID, Poa.ProyectoID')
            ->innerJoin('Proyecto','Poa.ProyectoID = Proyecto.ID')
            ->where(['Proyecto.InvestigadorID'=> $investigador->ID,'Poa.Estado' => 1])
            ->one();
        $resultado = '';

        if(!is_null($componente) and is_null($actividad)){ 
            $resultado  = Actividad::find()
                ->where(['ComponenteID'=>$componente])
                ->all();

            if(!empty($resultado)){
                echo '<option value="00">[SELECCIONE]</option>';
                foreach ($resultado as $res) {
                    echo '<option value="'.$res->ID.'">'.'ACTV-'.$res->Correlativo.': '.$res->Nombre.'</option>';
                }
            }else{
                echo '<option>No existen actividades</option>';
            }

        }else if(!is_null($componente) and !is_null($actividad)){
            $resultado  = Componente::find()
                ->where(['ID'=>$componente])
                ->one();

            $actividad  = Actividad::find()
                ->where(['ID'=>$actividad])
                ->one();

                $temo = array(
                    'ComponenteID' => $resultado->ID,
                    'Componente' => $resultado->Nombre,
                    'ComponenteCorelativo' => $resultado->Correlativo,
                    'ActividadID' => isset($actividad) ? $actividad->ID :'',
                    'Actividad' => isset($actividad) ? $actividad->Nombre :'',
                    'ActividadCorrelativo' => isset($actividad)? $actividad->Correlativo :'',
                    'Error' => isset($actividad) ? 0 : 1
                    );

            // if(!empty($actividad) and !empty($resultado)){
            //     $moPa = new IndicadorPAC();
            //     $moPa->ComponenteID    = $resultado->ID;
            //     $moPa->ComponenteCorrelativo          = $resultado->Correlativo;
            //     $moPa->ActividadID             = $actividad->ID;
            //     $moPa->ActividadCorrelativo    = $actividad->Correlativo;
            //     $moPa->PacDetalleID        = $dae['fecha'];
            //     $moPa->insert();
            // }


                echo json_encode($temo);
                die();
            


        }else{
            $resultado  = Componente::find()
                ->where(['PoaID'=>$proyecto->ID])
                ->all();
            foreach ($resultado as $res) {
                $arr[] = array(
                    'ID' => $res->ID,
                    'Nombre' => $res->Nombre
                    );
            }

            echo json_encode($arr);
        }
    }

    private function get_actRubroElegible($anio,$CodigoProyecto){
        $jsonActividades = array();
        $actv = RubroElegible::find()->select('ID,Nombre')
            ->all();
        if(!empty($actv) ){
            foreach ($actv as $rubr) {
                $jsonActividades[] = array(
                    'ID'        => $rubr->ID, 
                    'Nombre'    => $rubr->Nombre,
                    'Recursos'  => $this->get_recursos($rubr->ID,$anio,$CodigoProyecto)
                );
            }
        }
        return $jsonActividades;
    }

    private function get_recursos($idRubro,$anio,$CodigoProyecto){
        $jsonrecursos = array();
        $pa = Pac::find()->select('Pac.ID')
            // ->innerJoin('PacDetalle','PacDetalle.PacID = Pac.ID')
            ->innerJoin('Proyecto','Proyecto.ID = Pac.ProyectoID')
            ->innerJoin('Investigador','Investigador.ID = Proyecto.InvestigadorID')
            ->innerJoin('InformacionGeneral','Investigador.ID = InformacionGeneral.InvestigadorID')
            ->where(['Anio' => $anio,'InformacionGeneral.Codigo'=>$CodigoProyecto])
            ->one();
        
        // print_r($pa);
        // die;
        
        $dea = PacDetalle::find()->select('AreSubCategoriaID')
            ->where(['PacID' => $pa->ID])
            ->groupBy('PacDetalle.AreSubCategoriaID')
            ->all();


        // $codi = Yii::$app->db->createCommand("SELECT RubroElegible.ID,AreSubCategoria.Codificacion
        //     FROM AreSubCategoria 
        //     INNER JOIN ActRubroElegible ON ActRubroElegible.ID = AreSubCategoria.ActRubroElegibleID
        //     INNER JOIN RubroElegible ON RubroElegible.ID = ActRubroElegible.RubroElegibleID
        //     INNER JOIN Actividad ON ActRubroElegible.ActividadID = Actividad.ID
        //     INNER JOIN Componente ON Actividad.ComponenteID = Componente.ID
        //     WHERE AreSubCategoria.ID IN (SELECT PacDetalle.AreSubCategoriaID 
        //     FROM PacDetalle WHERE PacDetalle.PacID = ".$pa->ID."
        //     GROUP BY PacDetalle.AreSubCategoriaID)
        //     GROUP BY RubroElegible.ID,AreSubCategoria.Codificacion")
        //     ->queryAll();

        // $sx = '';
        // foreach ($dea as $dd) {
        //     // $sx .= $dd->AreSubCategoriaID.",";
        //     $sx[] = Yii::$app->db->createCommand("SELECT RubroElegible.ID,AreSubCategoria.Codificacion
        //     FROM AreSubCategoria 
        //     INNER JOIN ActRubroElegible ON ActRubroElegible.ID = AreSubCategoria.ActRubroElegibleID
        //     INNER JOIN RubroElegible ON RubroElegible.ID = ActRubroElegible.RubroElegibleID
        //     INNER JOIN Actividad ON ActRubroElegible.ActividadID = Actividad.ID
        //     INNER JOIN Componente ON Actividad.ComponenteID = Componente.ID
        //     WHERE AreSubCategoria.ID = ".$dd->AreSubCategoriaID)
        //     ->queryOne();

        // }

        // $dd = array_diff($sx,$sx);
        // print_r($dd);
        // die();

        $codd = Yii::$app->db->createCommand("SELECT PacDetalle.Codificacion 
            FROM [PacDetalle] 
            INNER JOIN [AreSubCategoria] ON AreSubCategoria.ID=PacDetalle.AreSubCategoriaID 
            INNER JOIN [Categoria] ON Categoria.ID=PacDetalle.CategoriaID 
            INNER JOIN [Pac] ON Pac.ID=PacDetalle.PacID 
            INNER JOIN [EntidadParticipante] 
            ON EntidadParticipante.ID=AreSubCategoria.EntidadParticipanteID 
            WHERE 
            [PacDetalle].[PacID]=".$pa->ID." 
            AND SUBSTRING(PacDetalle.Codificacion, 1, 1)  = ".$idRubro." 
            ORDER BY [PacDetalle].[CategoriaID]")
            ->queryAll();

        // print_r($codd); die();

        if(empty($codd)){
            $sq = "0";
        }else{
            // $acumula = '';
            $sq = "";
            foreach ($codd as $cods) {
                $vars = explode('.',$cods['Codificacion']);
                if($vars[0] == $idRubro){
                    $sq .= $vars[1].',';
                }
            }

            $sq = substr($sq, 0, -1);
        }

            // $sq = "WHERE AreSubCategoria.ID IN (SELECT PacDetalle.AreSubCategoriaID 
            // FROM PacDetalle WHERE PacDetalle.PacID = ".$pa->ID.") )";

        // $codif = $codd['Codificacion'];

            // echo $sq;
            // die();

        $xx = Yii::$app->db->createCommand("SELECT ActRubroElegible.RubroElegibleID,AreSubCategoria.Codificacion,AreSubCategoria.Nombre,AreSubCategoria.Especifica,AreSubCategoria.Detalle
            FROM Usuario 
            INNER JOIN Investigador ON Investigador.UsuarioID = Usuario.ID 
            INNER JOIN Proyecto ON Proyecto.InvestigadorID = Investigador.ID
            INNER JOIN Poa ON Poa.ProyectoID = Proyecto.ID
            INNER JOIN Componente ON Componente.PoaID = Poa.ID
            INNER JOIN Actividad ON Actividad.ComponenteID = Componente.ID
            INNER JOIN ActRubroElegible ON ActRubroElegible.ActividadID = Actividad.ID
            INNER JOIN AreSubCategoria ON AreSubCategoria.ActRubroElegibleID = ActRubroElegible.ID
            WHERE Usuario.username = '".$CodigoProyecto."'
            AND ActRubroElegible.RubroElegibleID = ".$idRubro."
            AND AreSubCategoria.Codificacion NOT IN (".$sq.")
            GROUP BY ActRubroElegible.RubroElegibleID,AreSubCategoria.Codificacion,AreSubCategoria.Especifica,AreSubCategoria.Detalle,AreSubCategoria.Nombre
            ORDER BY 2")
            ->queryAll();

        if(!empty($xx) ){
            foreach ($xx as $ww) {
                $qq = '';
                $nombre = ($ww['Nombre']);
                // $id     = $ww['ID'];
                // 
                $jsonrecursos[] = array(
                    'ID'            => '',
                    'Codificacion'  => $ww['RubroElegibleID'].'.'.$ww['Codificacion'],
                    'Rubro'         => $ww['RubroElegibleID'],
                    'Codifica'      => $ww['Codificacion'],
                    'Nombre'        => $ww['Nombre'].' / '.$ww['Especifica'].' / '.$ww['Detalle']
                );
            }
        }
        return $jsonrecursos;
    }

    public function actionDescargarPa($codigo=null,$anio=null)
    {
        $this->layout='estandar';
        $usuario            = Usuario::find()->where('username=:username',[':username'=>$codigo])->one();
        $datosInvestigador=Persona::findOne($usuario->PersonaID);
        $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
        $titulo=InformacionGeneral::find()->where(['InvestigadorID'=>$investigador->ID])->one();
        $aax = Pac::find()
            ->where(['Anio'=>$anio,'ProyectoID'=>$proyecto->ID])
            ->one();
        $detalle = PacDetalle::find()
            ->select('PacDetalle.NombrePac Nombre,AreSubCategoria.Especifica,AreSubCategoria.Detalle,PacDetalle.UnidadMedidaPac UnidadMedida,PacDetalle.CostoUnitarioPac CostoUnitario,Categoria.Nombre as Categoria,PacDetalle.Cantidad,PacDetalle.MetodoAdquisicion,PacDetalle.FechaInicioPA,EntidadParticipante.RazonSocial,EntidadParticipante.AporteMonetario,PacDetalle.ID,Categoria.ID Codigo,PacDetalle.Situacion,PacDetalle.NumeracionPAC,PacDetalle.Observaciones,PacDetalle.Codificacion')
            ->innerJoin('AreSubCategoria','AreSubCategoria.ID=PacDetalle.AreSubCategoriaID')
            ->innerJoin('Categoria','Categoria.ID=PacDetalle.CategoriaID')
            ->innerJoin('Pac','Pac.ID=PacDetalle.PacID')
            ->innerJoin('EntidadParticipante','EntidadParticipante.ID=AreSubCategoria.EntidadParticipanteID')
            ->where(['PacDetalle.PacID'=>$aax->ID])
            ->orderBy('PacDetalle.CategoriaID')
            ->all();
        return $this->render('descarga-pac',['detallepa' => $detalle,'anio'=>$anio,'Situacion'=>$aax->Situacion,'codigo'=>$codigo,'titulo' =>$titulo->TituloProyecto,'investigador'=>$datosInvestigador->ApellidoPaterno.' '.$datosInvestigador->ApellidoMaterno.' '.$datosInvestigador->Nombre ]);
    }

}