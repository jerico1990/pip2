<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\InformacionGeneral;
use app\models\ProgramaTransversal;
use app\models\DireccionLinea;
use app\models\TipoInvestigacion;
use app\models\UnidadEjecutora;
use app\models\UnidadOperativa;
use app\models\Investigador;
use app\models\EntidadParticipante;
use app\models\TipoInstitucion;
use app\models\RepresentanteLegal;
use app\models\Persona;
use app\models\MiembroEquipoGestion;
use app\models\RolEquipoTecnico;
use app\models\Usuario;
use app\models\TipoContratacion;

class RecursosHumanosController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
    */

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
    */

    public function actionIndex($investigador=null)
    {
        $this->layout='estandar';
        if($investigador)
        {
            
        }
        else
        {
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
        }
        if(\Yii::$app->user->identity->rol!=7)
        {
            return $this->redirect(['panel/index']) ;
        }
        $miembrosEquipoGestion=$this->MiembrosEquipoGestion($investigador->ID);
        return $this->render('index',['investigador'=>$investigador->ID,'miembrosEquipoGestion'=>$miembrosEquipoGestion]);
    }
    public function actionUpdateMiembroEquipoGestion($ID=null)
    {
        $this->layout='vacio';
        $model=Persona::find()
            ->select('Persona.*,MiembroEquipoGestion.InvestigadorID,RolEquipoTecnico.ID RolEquipoTecnicoID,RolEquipoTecnico.Nombre as FuncionDesempenar,MiembroEquipoGestion.TituloObtenido,MiembroEquipoGestion.Experiencia,MiembroEquipoGestion.ID as MiembroEquipoGestionID,MiembroEquipoGestion.EntidadParticipanteID')
            ->innerJoin('MiembroEquipoGestion','Persona.ID=MiembroEquipoGestion.PersonaID')
            ->innerJoin('RolEquipoTecnico','RolEquipoTecnico.ID=MiembroEquipoGestion.RolEquipoTecnicoID')
            ->where('Persona.ID=:ID',[':ID'=>$ID])
            ->one();
        
        $entidadesColaboradoras=EntidadParticipante::find()->where('Estado=1 and InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$model->InvestigadorID])->all();
        $tipoContratacion=TipoContratacion::find()->where('Estado=1')->all();
        if ($model->load(Yii::$app->request->post())) {
            
            $rolEquipoTecnico=RolEquipoTecnico::findOne($model->RolEquipoTecnicoID);
            $rolEquipoTecnico->Nombre=$model->FuncionDesempenar;
            $rolEquipoTecnico->save();
            $model->save();
            
            $miembroEquipoGestion=MiembroEquipoGestion::findOne($model->MiembroEquipoGestionID);
            //$miembroEquipoGestion->InvestigadorID=$investigador->ID;
           // $miembroEquipoGestion->PersonaID=$model->ID;
            //$miembroEquipoGestion->RolEquipoTecnicoID=$rolEquipoTecnico->ID;
            //$miembroEquipoGestion->EntidadParticipanteID=$model->EntidadParticipanteID;
            $miembroEquipoGestion->TituloObtenido=$model->TituloObtenido;
            // $miembroEquipoGestion->Experiencia=$model->Experiencia;
            $miembroEquipoGestion->save();

            $arr = array(
                'Success' => true
                );
            echo json_encode($arr);
            // return $this->redirect(['recursos-humanos/index']);
        }else{
            return $this->render('_miembro-equipo-gestion',['entidadesColaboradoras'=>$entidadesColaboradoras,'model'=>$model,'tipoContratacion'=>$tipoContratacion]);
        }
    }
    
    public function actionCreateMiembroEquipoGestion($investigador=null)
    {
        $this->layout='vacio';
        $investigador=Investigador::findOne($investigador);
        
        $model=new Persona;
        $entidadesColaboradoras=EntidadParticipante::find()->where('Estado=1 and InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->all();
        
        $tipoContratacion=TipoContratacion::find()->where('Estado=1')->all();
        
        if ($model->load(Yii::$app->request->post())) {
            $exists = Persona::find()->where('NroDocumento=:NroDocumento',[':NroDocumento'=>$model->NroDocumento])->one();
            if(empty($exists)){
                $model->insert();
                $id = $model->ID;
            }else{
                $id = $exists->ID;
            }
 
            $rolEquipoTecnico=new RolEquipoTecnico;
            $rolEquipoTecnico->Nombre=$model->FuncionDesempenar;
            $rolEquipoTecnico->insert();

            //Log
            Yii::$app->tools->logData($rolEquipoTecnico->ID,'insert','RolEquipoTecnico','');

            $miembroEquipoGestion=new MiembroEquipoGestion;
            $miembroEquipoGestion->InvestigadorID       = $investigador->ID;
            $miembroEquipoGestion->PersonaID            = $id;
            $miembroEquipoGestion->RolEquipoTecnicoID   = $rolEquipoTecnico->ID;
            $miembroEquipoGestion->EntidadParticipanteID= $model->EntidadParticipanteID;
            $miembroEquipoGestion->TituloObtenido       = $model->TituloObtenido;
            $miembroEquipoGestion->Experiencia = 0;
            $miembroEquipoGestion->Estado = 1;
            $miembroEquipoGestion->insert();
            
            //Log
            Yii::$app->tools->logData($miembroEquipoGestion->ID,'insert','MiembroEquipoGestion','');

            $arr = array(
                'Success' => true
                );
            echo json_encode($arr);
            die();
            // return $this->redirect(['recursos-humanos/index']);
        }
        
        return $this->render('_miembro-equipo-gestion',['entidadesColaboradoras'=>$entidadesColaboradoras,'model'=>$model,'tipoContratacion'=>$tipoContratacion]);
    }
    
    public function MiembrosEquipoGestion($investigador)
    {
        $model=new Persona();
        if($investigador)
        {
            $model=Persona::find()
                ->select('Persona.*,RolEquipoTecnico.ID RolEquipoTecnicoID,RolEquipoTecnico.Nombre as FuncionDesempenar,MiembroEquipoGestion.TituloObtenido,MiembroEquipoGestion.ID as MiembroEquipoGestionID')
                ->innerJoin('MiembroEquipoGestion','Persona.ID=MiembroEquipoGestion.PersonaID')
                ->innerJoin('RolEquipoTecnico','RolEquipoTecnico.ID=MiembroEquipoGestion.RolEquipoTecnicoID')
                ->where('Estado=1 and MiembroEquipoGestion.InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador])
                ->all();
        }
        return $model;
    }
    
    public function actionIntegrantes($CodigoProyecto=null,$Tipo=null,$ID=null)
    {
        
        if($Tipo==1)//json
        {
            $integrante=Persona::find()
            ->select('Persona.*,RolEquipoTecnico.ID RolEquipoTecnicoID,RolEquipoTecnico.Nombre as FuncionDesempenar,MiembroEquipoGestion.TituloObtenido,MiembroEquipoGestion.ID as MiembroEquipoGestionID')
            ->innerJoin('MiembroEquipoGestion','Persona.ID=MiembroEquipoGestion.PersonaID')
            ->innerJoin('RolEquipoTecnico','RolEquipoTecnico.ID=MiembroEquipoGestion.RolEquipoTecnicoID')
            ->innerJoin('InformacionGeneral','InformacionGeneral.InvestigadorID=MiembroEquipoGestion.InvestigadorID')
            ->where('Estado=1 and InformacionGeneral.Codigo=:Codigo and Persona.ID=:ID',[':Codigo'=>$CodigoProyecto,':ID'=>$ID])
            ->one();
            
            $data=[];
            //foreach($integrantes as $integrante){
                $data=['Success' => true,'ID'=>$integrante->ID,'Nombres'=>$integrante->Nombre,'Apellidos'=>$integrante->ApellidoPaterno.' '.$integrante->ApellidoMaterno,'Cargo'=>$integrante->FuncionDesempenar,'TipoContratacion'=>$integrante->TipoContratacion,'NroDocumento'=>$integrante->NroDocumento];
            //}
            echo json_encode($data);
        }
        elseif($Tipo==2)//model
        {
            $integrantes=Persona::find()
            ->select('Persona.*,RolEquipoTecnico.ID RolEquipoTecnicoID,RolEquipoTecnico.Nombre as FuncionDesempenar,MiembroEquipoGestion.TituloObtenido,MiembroEquipoGestion.ID as MiembroEquipoGestionID')
            ->innerJoin('MiembroEquipoGestion','Persona.ID=MiembroEquipoGestion.PersonaID')
            ->innerJoin('RolEquipoTecnico','RolEquipoTecnico.ID=MiembroEquipoGestion.RolEquipoTecnicoID')
            ->innerJoin('InformacionGeneral','InformacionGeneral.InvestigadorID=MiembroEquipoGestion.InvestigadorID')
            ->where('Estado=1 and InformacionGeneral.Codigo=:Codigo',[':Codigo'=>$CodigoProyecto])
            ->all();
            
            return $integrantes;
        }
    }
    
    public function actionEliminarIntegrante()
    {
        $this->layout='vacio';
        $ds = Yii::$app->request->post('id');

        $integrante = MiembroEquipoGestion::findOne($ds);
        $integrante->Estado = 0;
        $integrante->update();
        //Log
        Yii::$app->tools->logData($integrante->ID,'delete','MiembroEquipoGestion','');
        $arr = array('Success' => true);
        echo json_encode($arr);
    }

}

