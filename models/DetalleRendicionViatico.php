<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "DetalleRendicionViatico".
 *
 * @property integer $ID
 * @property integer $RendicionID
 * @property string $Fecha
 * @property string $Clase
 * @property string $Proveedor
 * @property string $Detalle
 * @property integer $Importe
 * @property string $FechaCreacion
 */
class DetalleRendicionViatico extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DetalleRendicionViatico';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['RendicionID', 'Importe','TipoDoc','Ruc'], 'integer'],
            [['Fecha', 'FechaCreacion'], 'safe'],
            [['Clase', 'Proveedor', 'Detalle'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'RendicionID' => 'Rendicion ID',
            'Fecha' => 'Fecha',
            'Clase' => 'Clase',
            'Proveedor' => 'Proveedor',
            'Detalle' => 'Detalle',
            'Importe' => 'Importe',
            'FechaCreacion' => 'Fecha Creacion',
        ];
    }
}
