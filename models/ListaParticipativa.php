<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ListaParticipativa".
 *
 * @property integer $ID
 * @property string $CodigoProyecto
 * @property integer $InformeTecnicoFinancieroID
 * @property string $Fecha
 * @property string $Tema
 * @property integer $NumeroPersonas
 * @property string $Comunidad
 * @property string $Lugar
 * @property double $Tiempo
 * @property double $HorasHombre
 * @property string $FechaRegistro
 * @property integer $Estado
 */
class ListaParticipativa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ListaParticipativa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CodigoProyecto', 'Tema', 'Comunidad', 'Lugar'], 'string'],
            [['InformeTecnicoFinancieroID', 'NumeroPersonas', 'Estado'], 'integer'],
            [['Fecha', 'FechaRegistro'], 'safe'],
            [['Tiempo', 'HorasHombre'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'CodigoProyecto' => 'Codigo Proyecto',
            'InformeTecnicoFinancieroID' => 'Informe Tecnico Financiero ID',
            'Fecha' => 'Fecha',
            'Tema' => 'Tema',
            'NumeroPersonas' => 'Numero Personas',
            'Comunidad' => 'Comunidad',
            'Lugar' => 'Lugar',
            'Tiempo' => 'Tiempo',
            'HorasHombre' => 'Horas Hombre',
            'FechaRegistro' => 'Fecha Registro',
            'Estado' => 'Estado',
        ];
    }
}
