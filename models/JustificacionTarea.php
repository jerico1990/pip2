<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "JustificacionTarea".
 *
 * @property integer $ID
 * @property integer $CronogramaTareaID
 * @property string $Descripcion
 * @property string $Evidencia
 * @property string $FechaRegistro
 * @property integer $Estado
 * @property integer $Situacion
 */
class JustificacionTarea extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'JustificacionTarea';
    }

    /**
     * @inheritdoc
     */
    public $Archivo;
    public $y01;
    public function rules()
    {
        return [
            //[['ID'], 'required'],
            [['ID', 'CronogramaTareaID', 'Estado', 'Situacion'], 'integer'],
            [['Descripcion', 'Evidencia'], 'string'],
            [['FechaRegistro','Archivo','y01'], 'safe'],
            [['Archivo'],'file']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'CronogramaTareaID' => 'Cronograma Tarea ID',
            'Descripcion' => 'Descripcion',
            'Evidencia' => 'Evidencia',
            'FechaRegistro' => 'Fecha Registro',
            'Estado' => 'Estado',
            'Situacion' => 'Situacion',
        ];
    }
}
