<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Componente".
 *
 * @property integer $ID
 * @property string $Nombre
 * @property string $Total
 * @property string $TotalFinanciamientoPnia
 * @property string $TotalFinanciamientoAlianza
 * @property double $CostoUnitario
 * @property integer $Cantidad
 * @property integer $Peso
 * @property integer $Estado
 * @property integer $PoaID
 *
 * @property Actividad[] $actividads
 * @property ActividadPO[] $actividadPOs
 * @property AporteComponente[] $aporteComponentes
 * @property Poa $poa
 * @property CronogramaComponente[] $cronogramaComponentes
 * @property FlujoCajaProyectado[] $flujoCajaProyectados
 * @property MarcoLogicoComponente[] $marcoLogicoComponentes
 */
class Componente extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $TotalObjetivo;
    public static function tableName()
    {
        return 'Componente';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        
        return [
           [['Nombre' ], 'required'],
            [['Nombre'], 'string'],
           [['Total', 'TotalFinanciamientoPnia', 'TotalFinanciamientoAlianza', 'CostoUnitario'], 'number'],
            [['Cantidad', 'Estado', 'PoaID','Correlativo'], 'integer'],
            [['Peso'], 'safe'],
           [['PoaID'], 'exist', 'skipOnError' => true, 'targetClass' => Poa::className(), 'targetAttribute' => ['PoaID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nombre' => 'Nombre',
            'Total' => 'Total',
            'TotalFinanciamientoPnia' => 'Total Financiamiento Pnia',
            'TotalFinanciamientoAlianza' => 'Total Financiamiento Alianza',
            'CostoUnitario' => 'Costo Unitario',
            'Cantidad' => 'Cantidad',
            'Peso' => 'Peso',
            'Estado' => 'Estado',
            'PoaID' => 'Poa ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActividads()
    {
        return $this->hasMany(Actividad::className(), ['ComponenteID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActividadPOs()
    {
        return $this->hasMany(ActividadPO::className(), ['ComponenteID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAporteComponentes()
    {
        return $this->hasMany(AporteComponente::className(), ['ComponenteID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoa()
    {
        return $this->hasOne(Poa::className(), ['ID' => 'PoaID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCronogramaComponentes()
    {
        return $this->hasMany(CronogramaComponente::className(), ['ComponenteID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFlujoCajaProyectados()
    {
        return $this->hasMany(FlujoCajaProyectado::className(), ['ComponenteID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarcoLogicoComponentes()
    {
        return $this->hasMany(MarcoLogicoComponente::className(), ['ComponenteID' => 'ID']);
    }
}
