<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "IndicadorPAC".
 *
 * @property integer $ID
 * @property integer $ComponenteID
 * @property integer $ComponenteCorrelativo
 * @property integer $ActividadID
 * @property integer $ActividadCorrelativo
 * @property integer $PaDetalleID
 */
class IndicadorPAC extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'IndicadorPAC';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'required'],
            [['ID', 'ComponenteID', 'ComponenteCorrelativo', 'ActividadID', 'ActividadCorrelativo', 'PaDetalleID'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ComponenteID' => 'Componente ID',
            'ComponenteCorrelativo' => 'Componente Correlativo',
            'ActividadID' => 'Actividad ID',
            'ActividadCorrelativo' => 'Actividad Correlativo',
            'PaDetalleID' => 'Pa Detalle ID',
        ];
    }
}
