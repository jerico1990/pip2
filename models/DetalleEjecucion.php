<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "DetalleEjecucion".
 *
 * @property integer $ID
 * @property integer $AreSubCategoriaID
 * @property integer $Cantidad
 * @property double $PrecioUnitario
 * @property double $PrecioTotal
 * @property integer $Estado
 * @property integer $Situacion
 * @property string $FechaRegistro
 * @property string $CodigoMatriz
 */
class DetalleEjecucion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DetalleEjecucion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['AreSubCategoriaID', 'Cantidad', 'Estado', 'Situacion'], 'integer'],
            [['PrecioUnitario', 'PrecioTotal'], 'number'],
            [['FechaRegistro'], 'safe'],
            [['CodigoMatriz'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'AreSubCategoriaID' => 'Are Sub Categoria ID',
            'Cantidad' => 'Cantidad',
            'PrecioUnitario' => 'Precio Unitario',
            'PrecioTotal' => 'Precio Total',
            'Estado' => 'Estado',
            'Situacion' => 'Situacion',
            'FechaRegistro' => 'Fecha Registro',
            'CodigoMatriz' => 'Codigo Matriz',
        ];
    }
}
