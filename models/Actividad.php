<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Actividad".
 *
 * @property integer $ID
 * @property integer $ComponenteID
 * @property string $Nombre
 * @property string $CostoUnitario
 * @property string $Total
 * @property string $TotalFinanciamientoPnia
 * @property string $TotalFinanciamientoAlianza
 * @property integer $Cantidad
 * @property integer $Peso
 * @property integer $Estado
 *
 * @property Componente $componente
 * @property ActRubroElegible[] $actRubroElegibles
 * @property AporteActividad[] $aporteActividads
 * @property CronogramaActividad[] $cronogramaActividads
 * @property MarcoLogicoActividad[] $marcoLogicoActividads
 */
class Actividad extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Actividad';
    }
    
    /**
     * @inheritdoc
     */
    public $Meses;
    public function rules()
    {
        return [
            [['ComponenteID'], 'required'],
            [['ComponenteID', 'Cantidad', 'Estado','Experimento','Evento'], 'integer'],
            [['Nombre'], 'string'],
            [['CostoUnitario', 'Total', 'TotalFinanciamientoPnia', 'TotalFinanciamientoAlianza'], 'number'],
            [['Peso'],'safe'],
            [['ComponenteID'], 'exist', 'skipOnError' => true, 'targetClass' => Componente::className(), 'targetAttribute' => ['ComponenteID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ComponenteID' => 'Componente ID',
            'Nombre' => 'Nombre',
            'CostoUnitario' => 'Costo Unitario',
            'Total' => 'Total',
            'TotalFinanciamientoPnia' => 'Total Financiamiento Pnia',
            'TotalFinanciamientoAlianza' => 'Total Financiamiento Alianza',
            'Cantidad' => 'Cantidad',
            'Peso' => 'Peso',
            'Estado' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComponente()
    {
        return $this->hasOne(Componente::className(), ['ID' => 'ComponenteID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActRubroElegibles()
    {
        return $this->hasMany(ActRubroElegible::className(), ['ActividadID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAporteActividads()
    {
        return $this->hasMany(AporteActividad::className(), ['ActividadID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCronogramaActividads()
    {
        return $this->hasMany(CronogramaActividad::className(), ['ActividadID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarcoLogicoActividads()
    {
        return $this->hasMany(MarcoLogicoActividad::className(), ['ActividadID' => 'ID']);
    }
}
