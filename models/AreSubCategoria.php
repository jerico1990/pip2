<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;
/**
 * This is the model class for table "AreSubCategoria".
 *
 * @property integer $ID
 * @property integer $ActRubroElegibleID
 * @property string $Nombre
 * @property string $UnidadMedida
 * @property string $CostoUnitario
 * @property string $MetaFisica
 * @property string $Total
 * @property string $TotalFinanciamientoPnia
 * @property string $TotalFinanciamientoAlianza
 * @property string $TotalConMetaFisica
 *
 * @property AporteAreSubCategoria[] $aporteAreSubCategorias
 * @property ActRubroElegible $actRubroElegible
 * @property CronogramaAreSubCategoria[] $cronogramaAreSubCategorias
 */
class AreSubCategoria extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $RubroElegibleID;
    public $ActividadID;
    public $Recursos;
    public $Especificas;
    public $CostosUnitarios;
    public $UnidadesMedidas;
    public $MetasFisicas;
    public $archivo;
    public $CronogramaAreSubCategoriaMetaFisica;
    public $TareaID;
    public static function tableName()
    {
        return 'AreSubCategoria';
    }

    /**
     * @inheritdoc
     */
    public $RazonSocial;
    public $AporteMonetario;
    public function rules()
    {
        return [
            // [['ActRubroElegibleID', 'Nombre', 'UnidadMedida', 'CostoUnitario', 'MetaFisica'], 'required'],
            [['ActRubroElegibleID','EntidadParticipanteID','FondoEntidadParticipanteID','RubroElegibleID','TareaID'], 'integer'],
            [['Nombre', 'UnidadMedida','Especifica'], 'string'],
            [['Codificacion','archivo','Recursos','Especificas','CostosUnitarios','UnidadesMedidas','MetasFisicas'],'safe'],
            //[['RubroElegibleID'],'safe']
            [['archivo'],'file'],
            [['CostoUnitario', 'MetaFisica', 'Total', 'TotalFinanciamientoPnia', 'TotalFinanciamientoAlianza', 'TotalConMetaFisica'], 'number'],
            [['ActRubroElegibleID'], 'exist', 'skipOnError' => true, 'targetClass' => ActRubroElegible::className(), 'targetAttribute' => ['ActRubroElegibleID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ActRubroElegibleID' => 'Act Rubro Elegible ID',
            'Nombre' => 'Nombre',
            'UnidadMedida' => 'Unidad Medida',
            'CostoUnitario' => 'Costo Unitario',
            'MetaFisica' => 'Meta Fisica',
            'Total' => 'Total',
            'TotalFinanciamientoPnia' => 'Total Financiamiento Pnia',
            'TotalFinanciamientoAlianza' => 'Total Financiamiento Alianza',
            'TotalConMetaFisica' => 'Total Con Meta Fisica',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAporteAreSubCategorias()
    {
        return $this->hasMany(AporteAreSubCategoria::className(), ['AreSubCategoriaID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActRubroElegible()
    {
        return $this->hasOne(ActRubroElegible::className(), ['ID' => 'ActRubroElegibleID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCronogramaAreSubCategorias()
    {
        return $this->hasMany(CronogramaAreSubCategoria::className(), ['AreSubCategoriaID' => 'ID']);
    }
}
