<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "RendicionViatico".
 *
 * @property integer $ID
 * @property string $CodigoProyecto
 * @property integer $ViaticoID
 * @property integer $AnticipoRecibido
 * @property integer $GastoSustentado
 * @property integer $Reintegrar
 * @property integer $Situacion
 */
class RendicionViatico extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'RendicionViatico';
    }

    /**
     * @inheritdoc
     */
    public $Fecha;
    public $Clase;
    public $Proveedor;
    public $Detalle;
    public $Importe;
    public $Codigos;
    public $TipoDoc;
    public $Ruc;
    public function rules()
    {
        return [
            [['CodigoProyecto'], 'string'],
            [['Codigos','Fecha','Clase','Proveedor','Detalle','Importe','AnticipoRecibido','GastoSustentado','Reintegrar','TipoDoc','Ruc'], 'safe'],
            [['ViaticoID', 'Situacion'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'CodigoProyecto' => 'Codigo Proyecto',
            'ViaticoID' => 'Viatico ID',
            'AnticipoRecibido' => 'Anticipo Recibido',
            'GastoSustentado' => 'Gasto Sustentado',
            'Reintegrar' => 'Reintegrar',
            'Situacion' => 'Situacion',
        ];
    }
}
