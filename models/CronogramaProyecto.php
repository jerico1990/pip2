<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "CronogramaProyecto".
 *
 * @property integer $ID
 * @property integer $ProyectoID
 * @property integer $Mes
 * @property string $MetaFinanciera
 * @property string $PoafMetaFinanciera
 *
 * @property Proyecto $proyecto
 */
class CronogramaProyecto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CronogramaProyecto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ProyectoID', 'Mes', 'MetaFinanciera'], 'required'],
            [['ProyectoID', 'Mes'], 'integer'],
            [['MetaFinanciera', 'PoafMetaFinanciera'], 'number'],
            [['MesDescripcion'],'string'],
            [['ProyectoID'], 'exist', 'skipOnError' => true, 'targetClass' => Proyecto::className(), 'targetAttribute' => ['ProyectoID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ProyectoID' => 'Proyecto ID',
            'Mes' => 'Mes',
            'MetaFinanciera' => 'Meta Financiera',
            'PoafMetaFinanciera' => 'Poaf Meta Financiera',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProyecto()
    {
        return $this->hasOne(Proyecto::className(), ['ID' => 'ProyectoID']);
    }
}
