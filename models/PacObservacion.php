<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "PacObservacion".
 *
 * @property integer $ID
 * @property string $Observacion
 * @property integer $PacID
 * @property integer $Estado
 * @property integer $AreSubCategoriaID
 */
class PacObservacion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'PacObservacion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Observacion'], 'string'],
            [['PacID', 'Estado', 'AreSubCategoriaID'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Observacion' => 'Observacion',
            'PacID' => 'Pac ID',
            'Estado' => 'Estado',
            'AreSubCategoriaID' => 'Are Sub Categoria ID',
        ];
    }
}
