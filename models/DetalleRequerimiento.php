<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "DetalleRequerimiento".
 *
 * @property integer $ID
 * @property integer $RequerimientoID
 * @property integer $AreSubCategoriaID
 * @property string $CodigoMatriz
 * @property double $Cantidad
 * @property double $PrecioUnitario
 * @property string $Descripcion
 * @property double $Total
 * @property string $Objetivo
 * @property string $Actividad
 * @property string $Recurso
 * @property integer $Codificacion
 * @property integer $Situacion
 * @property integer $TipoDetalleOrdenID
 * @property integer $Estado
 */
class DetalleRequerimiento extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DetalleRequerimiento';
    }

    /**
     * @inheritdoc
     */
    public $Archivo;
    public function rules()
    {
        return [
            [['RequerimientoID', 'AreSubCategoriaID', 'Codificacion', 'Situacion', 'TipoDetalleOrdenID', 'Estado'], 'integer'],
            [['CodigoMatriz', 'Descripcion', 'Objetivo', 'Actividad', 'Recurso'], 'string'],
            [['Cantidad', 'PrecioUnitario', 'Total'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'RequerimientoID' => 'Requerimiento ID',
            'AreSubCategoriaID' => 'Are Sub Categoria ID',
            'CodigoMatriz' => 'Codigo Matriz',
            'Cantidad' => 'Cantidad',
            'PrecioUnitario' => 'Precio Unitario',
            'Descripcion' => 'Descripcion',
            'Total' => 'Total',
            'Objetivo' => 'Objetivo',
            'Actividad' => 'Actividad',
            'Recurso' => 'Recurso',
            'Codificacion' => 'Codificacion',
            'Situacion' => 'Situacion',
            'TipoDetalleOrdenID' => 'Tipo Detalle Orden ID',
            'Estado' => 'Estado',
        ];
    }
}
