<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "DetalleOrden".
 *
 * @property integer $ID
 * @property integer $OrdenID
 * @property integer $Cantidad
 * @property string $UnidadMedida
 * @property string $Descripcion
 * @property double $PrecioUnitario
 * @property double $Total
 */
class DetalleOrden extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DetalleOrden';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['ID'], 'required'],
            [['ID', 'OrdenID'], 'integer'],
            [['UnidadMedida', 'Descripcion','codi_bser_cat'], 'string'],
            [['PrecioUnitario', 'Total','Cantidad'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'OrdenID' => 'Orden ID',
            'Cantidad' => 'Cantidad',
            'UnidadMedida' => 'Unidad Medida',
            'Descripcion' => 'Descripcion',
            'PrecioUnitario' => 'Precio Unitario',
            'Total' => 'Total',
        ];
    }
}
