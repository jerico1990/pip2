<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Padron".
 *
 * @property integer $ID
 * @property integer $ClasificadorID
 * @property string $Nombres
 * @property string $ApellidoPaterno
 * @property string $ApellidoMaterno
 * @property string $DNI
 * @property string $FechaNacimiento
 * @property integer $Sexo
 * @property string $CentroPoblado
 * @property integer $IdiomaEspanol
 * @property integer $IdiomaQuechua
 * @property integer $IdiomaAymara
 * @property integer $IdiomaOtro
 * @property string $BeneficiarioDirecto
 * @property string $NivelEducativo
 * @property integer $ActividadEconomicaAgricultor
 * @property integer $ActividadEconomicaGanadero
 * @property integer $ActividadEconomicaForestal
 * @property integer $ActividadEconomicaAgroIndustria
 * @property integer $ActividadEconomicaOtro
 * @property integer $ComunidadID
 */
class Padron extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Padron';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ClasificadorID', 'Sexo', 'IdiomaEspanol', 'IdiomaQuechua', 'IdiomaAymara', 'IdiomaOtro', 'ActividadEconomicaAgricultor', 'ActividadEconomicaGanadero', 'ActividadEconomicaForestal', 'ActividadEconomicaAgroIndustria', 'ActividadEconomicaOtro', 'ComunidadID'], 'integer'],
            [['Nombres', 'ApellidoPaterno', 'ApellidoMaterno', 'DNI', 'CentroPoblado', 'BeneficiarioDirecto', 'NivelEducativo'], 'string'],
            [['FechaNacimiento'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ClasificadorID' => 'Clasificador ID',
            'Nombres' => 'Nombres',
            'ApellidoPaterno' => 'Apellido Paterno',
            'ApellidoMaterno' => 'Apellido Materno',
            'DNI' => 'Dni',
            'FechaNacimiento' => 'Fecha Nacimiento',
            'Sexo' => 'Sexo',
            'CentroPoblado' => 'Centro Poblado',
            'IdiomaEspanol' => 'Idioma Espanol',
            'IdiomaQuechua' => 'Idioma Quechua',
            'IdiomaAymara' => 'Idioma Aymara',
            'IdiomaOtro' => 'Idioma Otro',
            'BeneficiarioDirecto' => 'Beneficiario Directo',
            'NivelEducativo' => 'Nivel Educativo',
            'ActividadEconomicaAgricultor' => 'Actividad Economica Agricultor',
            'ActividadEconomicaGanadero' => 'Actividad Economica Ganadero',
            'ActividadEconomicaForestal' => 'Actividad Economica Forestal',
            'ActividadEconomicaAgroIndustria' => 'Actividad Economica Agro Industria',
            'ActividadEconomicaOtro' => 'Actividad Economica Otro',
            'ComunidadID' => 'Comunidad ID',
        ];
    }
}
