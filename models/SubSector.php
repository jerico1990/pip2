<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "SubSector".
 *
 * @property integer $ID
 * @property string $Nombre
 *
 * @property InformacionGeneral[] $informacionGenerals
 */
class SubSector extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SubSector';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nombre'], 'required'],
            [['Nombre'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformacionGenerals()
    {
        return $this->hasMany(InformacionGeneral::className(), ['SubSectorID' => 'ID']);
    }
}
