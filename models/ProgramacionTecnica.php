<?php

namespace app\models;

use yii\base\Model;

/**
 * This is the model class for table "ProgramacionTecnica".
 *
 * @property integer $IdProgramacionTecnica
 * @property integer $ProyectoId
 * @property integer $ActividadID
 * @property integer $Correlativo
 * @property string $Titulo
 * @property string $IntroduccionDescripcion
 * @property integer $Tipologia
 * @property string $LugarDesarrollo
 * @property string $FechaInicio
 * @property string $FechaFin
 * @property integer $Probabilidad
 * @property string $UniversidadNombre
 * @property string $RevistaPublicacion
 * @property string $RequiereAsesoramiento
 * @property integer $EsProgramado
 * @property integer $Estado
 * @property string $FechaRegistro
 * @property string $ProgramacionTipo
 * @property string $ObservacionInvestigador
 * @property string $Beneficiario
 *
 */
class ProgramacionTecnica extends Model
{
    public static $types = array("Operation", "Task", "Role");
    public $IdProgramacionTecnica;
    public $ProyectoId;
    public $ActividadID;
    public $Correlativo;
    public $Titulo;
    public $IntroduccionDescripcion;
    public $LugarDesarrollo;
    public $Tipologia;
    public $FechaInicio;
    public $FechaFin;
    public $Probabilidad;
    public $UniversidadNombre;
    public $RevistaPublicacion;
    public $RequiereAsesoramiento;
    public $EsProgramado;
    public $Estado;
    public $FechaRegistro;
    public $ProgramacionTipo;
    public $ObservacionInvestigador;
    public $Beneficiario;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ProyectoId'], 'required'],
            [['IdProgramacionTecnica','ProyectoId', 'ActividadID', 'Correlativo', 'Tipologia', 'Probabilidad', 'EsProgramado', 'Estado'], 'integer'],
            [['Titulo', 'IntroduccionDescripcion', 'LugarDesarrollo', 'UniversidadNombre', 'RevistaPublicacion', 'RequiereAsesoramiento', 'ProgramacionTipo', 'ObservacionInvestigador', 'Beneficiario'], 'string'],
            [['FechaInicio', 'FechaFin', 'FechaRegistro', 'Beneficiario'], 'safe'],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IdProgramacionTecnica' => 'Id Programacion Tecnica',
            'ProyectoId' => 'Proyecto ID',
            'ActividadID' => 'Actividad ID',
            'Correlativo' => 'Correlativo',
            'Titulo' => 'Titulo',
            'IntroduccionDescripcion' => 'Introduccion Descripcion',
            'Tipologia' => 'Tipologia',
            'LugarDesarrollo' => 'Lugar Desarrollo',
            'FechaInicio' => 'Fecha Inicio',
            'FechaFin' => 'Fecha Fin',
            'Probabilidad' => 'Probabilidad',
            'UniversidadNombre' => 'Universidad Nombre',
            'RevistaPublicacion' => 'Revista Publicacion',
            'RequiereAsesoramiento' => 'Requiere Asesoramiento',
            'EsProgramado' => 'Es Programado',
            'Estado' => 'Estado',
            'FechaRegistro' => 'Fecha Registro',
            'ProgramacionTipo' => 'Programacion Tipo',
            'ObservacionInvestigador' => 'Observacion Investigador',
            'Beneficiario' => 'Beneficiario',
        ];
    }

}
