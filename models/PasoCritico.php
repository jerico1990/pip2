<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "PasoCritico".
 *
 * @property integer $ID
 * @property integer $ProyectoID
 * @property integer $MesInicio
 * @property integer $MesFin
 *
 * @property AporteActividadPC[] $aporteActividadPCs
 * @property AporteActRubroElegiblePC[] $aporteActRubroElegiblePCs
 * @property AporteAreSubCategoriaPC[] $aporteAreSubCategoriaPCs
 * @property AporteComponentePC[] $aporteComponentePCs
 * @property AporteProyectoPC[] $aporteProyectoPCs
 * @property IndicadorPasoCritico[] $indicadorPasoCriticos
 * @property Proyecto $proyecto
 */
class PasoCritico extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $Total;
    public static function tableName()
    {
        return 'PasoCritico';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ProyectoID', 'MesInicio', 'MesFin'], 'required'],
            [['ProyectoID', 'MesInicio', 'MesFin'], 'integer'],
            [['ProyectoID'], 'exist', 'skipOnError' => true, 'targetClass' => Proyecto::className(), 'targetAttribute' => ['ProyectoID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ProyectoID' => 'Proyecto ID',
            'MesInicio' => 'Mes Inicio',
            'MesFin' => 'Mes Fin',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAporteActividadPCs()
    {
        return $this->hasMany(AporteActividadPC::className(), ['PasoCriticoID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAporteActRubroElegiblePCs()
    {
        return $this->hasMany(AporteActRubroElegiblePC::className(), ['PasoCriticoID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAporteAreSubCategoriaPCs()
    {
        return $this->hasMany(AporteAreSubCategoriaPC::className(), ['PasoCriticoID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAporteComponentePCs()
    {
        return $this->hasMany(AporteComponentePC::className(), ['PasoCriticoID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAporteProyectoPCs()
    {
        return $this->hasMany(AporteProyectoPC::className(), ['PasoCriticoID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndicadorPasoCriticos()
    {
        return $this->hasMany(IndicadorPasoCritico::className(), ['PasoCriticoID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProyecto()
    {
        return $this->hasOne(Proyecto::className(), ['ID' => 'ProyectoID']);
    }
}
