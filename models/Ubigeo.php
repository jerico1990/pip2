<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Ubigeo".
 *
 * @property string $ID
 * @property string $Nombre
 *
 * @property AmbitoIntervencion[] $ambitoIntervencions
 * @property InformacionGeneral[] $informacionGenerals
 */
class Ubigeo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ubigeo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'Nombre'], 'required'],
            [['ID', 'Nombre'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAmbitoIntervencions()
    {
        return $this->hasMany(AmbitoIntervencion::className(), ['UbigeoID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformacionGenerals()
    {
        return $this->hasMany(InformacionGeneral::className(), ['LocalizacionProyectoUbigeoID' => 'ID']);
    }
}
