<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "GastosGeneralesObservacion".
 *
 * @property integer $ID
 * @property string $Observacion
 * @property integer $GastosGeneralesID
 * @property integer $Estado
 */
class GastosGeneralesObservacion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'GastosGeneralesObservacion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Observacion'], 'string'],
            [['GastosGeneralesID', 'Estado'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Observacion' => 'Observacion',
            'GastosGeneralesID' => 'Gastos Generales ID',
            'Estado' => 'Estado',
        ];
    }
}
