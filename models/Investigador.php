<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Investigador".
 *
 * @property integer $ID
 * @property integer $UsuarioID
 * @property integer $ConvocatoriaID
 * @property integer $EtapaID
 * @property string $Extension_ListaDeChequeo
 * @property string $FechaEnvioProyecto
 * @property string $CodigoEnvioProyecto
 * @property string $AVANCE
 * @property integer $EstadoAprobatorio
 * @property integer $EstadoEvaPET
 * @property string $FechaEnvioConsEvaPET
 * @property string $FechaRespConsEvaPET
 * @property integer $EtapaEvaluacion
 * @property string $FechaEnvioProyectoPC
 * @property string $NroInfoConformidadUD
 * @property string $NroInfoConformidadUPMSI
 * @property string $NroInfoConformidadLegal
 * @property string $EstadoAprobatorioFinal
 * @property string $FechaInfoConformidadUD
 * @property string $ObservacionConformidadUD
 * @property string $MontoDesembolso1
 * @property string $Departamento
 * @property string $Provincia
 * @property string $Distrito
 *
 * @property AEComponente[] $aEComponentes
 * @property AEProducto[] $aEProductos
 * @property ConsSubcriterio[] $consSubcriterios
 * @property EntidadOferente[] $entidadOferentes
 * @property EntidadParticipante[] $entidadParticipantes
 * @property InformacionGeneral[] $informacionGenerals
 * @property InformeTecnico[] $informeTecnicos
 * @property Usuario $usuario
 * @property MiembroEquipoGestion[] $miembroEquipoGestions
 * @property PlanNegocios[] $planNegocios
 * @property Proyecto[] $proyectos
 */
class Investigador extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Investigador';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['UsuarioID', 'ConvocatoriaID', 'EtapaID'], 'required'],
            [['UsuarioID', 'ConvocatoriaID', 'EtapaID', 'EstadoAprobatorio', 'EstadoEvaPET', 'EtapaEvaluacion'], 'integer'],
            [['Extension_ListaDeChequeo', 'CodigoEnvioProyecto', 'NroInfoConformidadUD', 'NroInfoConformidadUPMSI', 'NroInfoConformidadLegal', 'EstadoAprobatorioFinal', 'FechaInfoConformidadUD', 'ObservacionConformidadUD', 'Departamento', 'Provincia', 'Distrito'], 'string'],
            [['FechaEnvioProyecto', 'FechaEnvioConsEvaPET', 'FechaRespConsEvaPET', 'FechaEnvioProyectoPC'], 'safe'],
            [['AVANCE', 'MontoDesembolso1'], 'number'],
            [['UsuarioID'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['UsuarioID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'UsuarioID' => 'Usuario ID',
            'ConvocatoriaID' => 'Convocatoria ID',
            'EtapaID' => 'Etapa ID',
            'Extension_ListaDeChequeo' => 'Extension  Lista De Chequeo',
            'FechaEnvioProyecto' => 'Fecha Envio Proyecto',
            'CodigoEnvioProyecto' => 'Codigo Envio Proyecto',
            'AVANCE' => 'Avance',
            'EstadoAprobatorio' => 'Estado Aprobatorio',
            'EstadoEvaPET' => 'Estado Eva Pet',
            'FechaEnvioConsEvaPET' => 'Fecha Envio Cons Eva Pet',
            'FechaRespConsEvaPET' => 'Fecha Resp Cons Eva Pet',
            'EtapaEvaluacion' => 'Etapa Evaluacion',
            'FechaEnvioProyectoPC' => 'Fecha Envio Proyecto Pc',
            'NroInfoConformidadUD' => 'Nro Info Conformidad Ud',
            'NroInfoConformidadUPMSI' => 'Nro Info Conformidad Upmsi',
            'NroInfoConformidadLegal' => 'Nro Info Conformidad Legal',
            'EstadoAprobatorioFinal' => 'Estado Aprobatorio Final',
            'FechaInfoConformidadUD' => 'Fecha Info Conformidad Ud',
            'ObservacionConformidadUD' => 'Observacion Conformidad Ud',
            'MontoDesembolso1' => 'Monto Desembolso1',
            'Departamento' => 'Departamento',
            'Provincia' => 'Provincia',
            'Distrito' => 'Distrito',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAEComponentes()
    {
        return $this->hasMany(AEComponente::className(), ['IdPostulante' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAEProductos()
    {
        return $this->hasMany(AEProducto::className(), ['IdPostulante' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsSubcriterios()
    {
        return $this->hasMany(ConsSubcriterio::className(), ['InvestigadorID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntidadOferentes()
    {
        return $this->hasMany(EntidadOferente::className(), ['InvestigadorID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntidadParticipantes()
    {
        return $this->hasMany(EntidadParticipante::className(), ['InvestigadorID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformacionGenerals()
    {
        return $this->hasMany(InformacionGeneral::className(), ['InvestigadorID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformeTecnicos()
    {
        return $this->hasMany(InformeTecnico::className(), ['InvestigadorID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['ID' => 'UsuarioID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMiembroEquipoGestions()
    {
        return $this->hasMany(MiembroEquipoGestion::className(), ['InvestigadorID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanNegocios()
    {
        return $this->hasMany(PlanNegocios::className(), ['InvestigadorID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProyectos()
    {
        return $this->hasMany(Proyecto::className(), ['InvestigadorID' => 'ID']);
    }
}
