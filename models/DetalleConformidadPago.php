<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "DetalleConformidadPago".
 *
 * @property integer $ID
 * @property integer $ConformidadPagoID
 * @property integer $Estado
 * @property string $FechaCreacion
 * @property integer $TipoGasto
 * @property string $OrdenServicio
 * @property string $ConformidadServicio
 * @property string $ReciboHonorario
 * @property string $InformeActividades
 * @property string $OrdenCompra
 * @property string $ConformidadEspecificaciones
 * @property string $Factura
 */
class DetalleConformidadPago extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DetalleConformidadPago';
    }

    /**
     * @inheritdoc
     */

    public $OrdenServicioArchivo;
    public $ConformidadServicioArchivo;
    public $ReciboHonorarioArchivo;
    public $InformeActividadesArchivo;
    public $OrdenCompraArchivo;
    public $ConformidadEspecificacionesArchivo;
    public $FacturaArchivo;
    public $Anexo06Archivo;
    public $OtrosArchivo;

    public function rules()
    {
        return [
            // [['ConformidadPagoID', 'Estado', 'TipoGasto','Situacion'], 'integer'],
            [['FechaCreacion','OrdenServicioArchivo','ConformidadServicioArchivo','ReciboHonorarioArchivo','InformeActividadesArchivo','OrdenCompraArchivo','ConformidadEspecificacionesArchivo','FacturaArchivo','Anexo06Archivo','OtrosArchivo','OrdenServicio', 'ConformidadServicio', 'ReciboHonorario', 'InformeActividades', 'OrdenCompra', 'ConformidadEspecificaciones', 'Factura','OrdenServicioArchivo','ConformidadServicioArchivo','ReciboHonorarioArchivo','InformeActividadesArchivo','OrdenCompraArchivo','ConformidadEspecificacionesArchivo','FacturaArchivo','Anexo06Archivo','OtrosArchivo'], 'safe'],
            //  [['OrdenServicio', 'ConformidadServicio', 'ReciboHonorario', 'InformeActividades', 'OrdenCompra', 'ConformidadEspecificaciones', 'Factura'], 'string'],
            // [['OrdenServicioArchivo','ConformidadServicioArchivo','ReciboHonorarioArchivo','InformeActividadesArchivo','OrdenCompraArchivo','ConformidadEspecificacionesArchivo','FacturaArchivo','Anexo06Archivo','OtrosArchivo'],'file']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ConformidadPagoID' => 'Conformidad Pago ID',
            'Estado' => 'Estado',
            'FechaCreacion' => 'Fecha Creacion',
            'TipoGasto' => 'Tipo Gasto',
            'OrdenServicio' => 'Orden Servicio',
            'ConformidadServicio' => 'Conformidad Servicio',
            'ReciboHonorario' => 'Recibo Honorario',
            'InformeActividades' => 'Informe Actividades',
            'OrdenCompra' => 'Orden Compra',
            'ConformidadEspecificaciones' => 'Conformidad Especificaciones',
            'Factura' => 'Factura',
        ];
    }
}
