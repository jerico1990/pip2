<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "EntidadOferente".
 *
 * @property integer $ID
 * @property integer $InvestigadorID
 * @property string $RazonSocial
 * @property string $Ruc
 * @property string $Telefono
 * @property string $Experiencia
 * @property integer $Tipo
 * @property string $Extension_CartaPresentacion
 *
 * @property Postulante $investigador
 * @property MiembroEquipoInvestigacion[] $miembroEquipoInvestigacions
 */
class EntidadOferente extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'EntidadOferente';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['InvestigadorID', 'RazonSocial', 'Ruc', 'Telefono', 'Experiencia', 'Tipo'], 'required'],
            [['InvestigadorID', 'Tipo'], 'integer'],
            [['RazonSocial', 'Ruc', 'Telefono', 'Experiencia', 'Extension_CartaPresentacion'], 'string'],
            [['InvestigadorID'], 'exist', 'skipOnError' => true, 'targetClass' => Postulante::className(), 'targetAttribute' => ['InvestigadorID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'InvestigadorID' => 'Investigador ID',
            'RazonSocial' => 'Razon Social',
            'Ruc' => 'Ruc',
            'Telefono' => 'Telefono',
            'Experiencia' => 'Experiencia',
            'Tipo' => 'Tipo',
            'Extension_CartaPresentacion' => 'Extension  Carta Presentacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvestigador()
    {
        return $this->hasOne(Postulante::className(), ['ID' => 'InvestigadorID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMiembroEquipoInvestigacions()
    {
        return $this->hasMany(MiembroEquipoInvestigacion::className(), ['EntidadOferenteID' => 'ID']);
    }
}
