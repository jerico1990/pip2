<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "AE_ComponenteValor".
 *
 * @property integer $IdComponenteValor
 * @property integer $IdComponente
 * @property integer $Anio
 * @property string $Valor
 *
 * @property AEComponente $idComponente
 */
class AEComponenteValor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'AE_ComponenteValor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['IdComponente', 'Anio'], 'required'],
            [['IdComponente', 'Anio'], 'integer'],
            [['Valor'], 'number'],
            [['IdComponente'], 'exist', 'skipOnError' => true, 'targetClass' => AEComponente::className(), 'targetAttribute' => ['IdComponente' => 'IdComponente']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IdComponenteValor' => 'Id Componente Valor',
            'IdComponente' => 'Id Componente',
            'Anio' => 'Anio',
            'Valor' => 'Valor',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdComponente()
    {
        return $this->hasOne(AEComponente::className(), ['IdComponente' => 'IdComponente']);
    }
}
