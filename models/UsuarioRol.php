<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "UsuarioRol".
 *
 * @property integer $ID
 * @property integer $UsuarioID
 * @property integer $RolID
 *
 * @property Rol $rol
 * @property Usuario $usuario
 */
class UsuarioRol extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'UsuarioRol';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['UsuarioID', 'RolID'], 'required'],
            [['UsuarioID', 'RolID'], 'integer'],
            [['RolID'], 'exist', 'skipOnError' => true, 'targetClass' => Rol::className(), 'targetAttribute' => ['RolID' => 'ID']],
            [['UsuarioID'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['UsuarioID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'UsuarioID' => 'Usuario ID',
            'RolID' => 'Rol ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRol()
    {
        return $this->hasOne(Rol::className(), ['ID' => 'RolID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['ID' => 'UsuarioID']);
    }
}
