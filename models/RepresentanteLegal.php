<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "RepresentanteLegal".
 *
 * @property integer $ID
 * @property integer $PersonaID
 * @property integer $EntidadParticipanteID
 * @property string $Cargo
 * @property string $PartidaResolucion
 * @property string $RegistroPartidaResolucion
 *
 * @property EntidadParticipante $entidadParticipante
 * @property Persona $persona
 */
class RepresentanteLegal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'RepresentanteLegal';
    }

    /**
     * @inheritdoc
     */
    public $Nombre;
    public $ApellidoPaterno;
    public $ApellidoMaterno;
    public $NroDocumento;
    public $Telefono;
    public $Email;
    public $Codigo;
    public function rules()
    {
        return [
            [['PersonaID', 'EntidadParticipanteID', 'Cargo'], 'required'],
            [['PersonaID', 'EntidadParticipanteID'], 'integer'],
            [['Cargo', 'PartidaResolucion', 'RegistroPartidaResolucion'], 'string'],
            [['EntidadParticipanteID'], 'exist', 'skipOnError' => true, 'targetClass' => EntidadParticipante::className(), 'targetAttribute' => ['EntidadParticipanteID' => 'ID']],
            [['PersonaID'], 'exist', 'skipOnError' => true, 'targetClass' => Persona::className(), 'targetAttribute' => ['PersonaID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'PersonaID' => 'Persona ID',
            'EntidadParticipanteID' => 'Entidad Participante ID',
            'Cargo' => 'Cargo',
            'PartidaResolucion' => 'Partida Resolucion',
            'RegistroPartidaResolucion' => 'Registro Partida Resolucion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntidadParticipante()
    {
        return $this->hasOne(EntidadParticipante::className(), ['ID' => 'EntidadParticipanteID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersona()
    {
        return $this->hasOne(Persona::className(), ['ID' => 'PersonaID']);
    }
}
