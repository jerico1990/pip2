<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Ejecucion".
 *
 * @property integer $ID
 * @property double $MetaEjecutada
 * @property double $PrecioUnitarioEjecutado
 * @property integer $TipoGastoID
 * @property integer $CodigoID
 * @property string $FechaEjecucion
 * @property integer $Situacion
 * @property integer $Estado
 */
class Ejecucion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Ejecucion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['MetaEjecutada', 'PrecioUnitarioEjecutado'], 'number'],
            [['TipoGastoID', 'CodigoID', 'Situacion', 'Estado'], 'integer'],
            [['FechaEjecucion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'MetaEjecutada' => 'Meta Ejecutada',
            'PrecioUnitarioEjecutado' => 'Precio Unitario Ejecutado',
            'TipoGastoID' => 'Tipo Gasto ID',
            'CodigoID' => 'Codigo ID',
            'FechaEjecucion' => 'Fecha Ejecucion',
            'Situacion' => 'Situacion',
            'Estado' => 'Estado',
        ];
    }
}
