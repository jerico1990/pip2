<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "CronogramaExperimento".
 *
 * @property integer $ID
 * @property integer $ProgramacionExperimentoID
 * @property integer $Mes
 * @property string $MetaFisica
 * @property integer $Situacion
 * @property integer $Estado
 * @property string $MesDescripcion
 * @property string $MetaFisicaEjecutada
 * @property string $MetaAvance
 * @property integer $SituacionEjecucion
 *
 * @property Tarea $programacionExperimento
 */
class CronogramaExperimento extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CronogramaExperimento';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ProgramacionExperimentoID', 'Mes', 'Situacion', 'Estado', 'SituacionEjecucion'], 'integer'],
            [['MetaFisica', 'MetaFisicaEjecutada', 'MetaAvance'], 'number'],
            [['MesDescripcion'], 'string'],
            [['ProgramacionExperimentoID'], 'exist', 'skipOnError' => true, 'targetClass' => Tarea::className(), 'targetAttribute' => ['ProgramacionExperimentoID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ProgramacionExperimentoID' => 'Programacion Experimento ID',
            'Mes' => 'Mes',
            'MetaFisica' => 'Meta Fisica',
            'Situacion' => 'Situacion',
            'Estado' => 'Estado',
            'MesDescripcion' => 'Mes Descripcion',
            'MetaFisicaEjecutada' => 'Meta Fisica Ejecutada',
            'MetaAvance' => 'Meta Avance',
            'SituacionEjecucion' => 'Situacion Ejecucion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgramacionExperimento()
    {
        return $this->hasOne(Tarea::className(), ['ID' => 'ProgramacionExperimentoID']);
    }
}
