<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Viatico".
 *
 * @property integer $ID
 * @property string $Nombre
 * @property string $Apellido
 * @property string $Dni
 * @property string $Cargo
 * @property string $PlanTrabajo
 * @property string $Regimen
 * @property string $TipoViaje
 * @property string $Destino
 * @property string $Salida
 * @property string $Regreso
 * @property string $FteFto
 * @property string $CuentaBancaria
 * @property string $LugarFecha
 * @property integer $Correlativo
 * @property integer $Situacion
 * @property integer $Estado
 * @property string $FechaCreacion
 * @property string $CodigoPoa
 * @property string $CodigoProyecto
 * @property integer $AreSubCategoriaID
 * @property integer $ActividadID
 * @property integer $ComponenteID
 * @property integer $RequerimientoID
 * @property integer $NroDias
 * @property integer $AsigHoras
 *
 * @property DetalleViatico[] $detalleViaticos
 * @property Requerimiento $requerimiento
 */
class Viatico extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Viatico';
    }

    /**
     * @inheritdoc
     */
    public $Codigos;
    public $Tipo;
    public $Importe;
    public $archivo;

    public $Fecha;
    public $CodigoMatriz;
    public $FechaProceso;
    public $TipoCambio;
    public $Codificacion;
    public $PersonaID;
    public $Siaf;
    public $DetalleRequerimientoID;
    public $DetallesRequerimientosIDs;
    public $Memorando;
    public function rules()
    {
        return [
            [['Nombre', 'Apellido', 'Dni', 'Cargo', 'PlanTrabajo', 'Regimen', 'TipoViaje', 'Destino', 'FteFto', 'CuentaBancaria', 'LugarFecha', 'CodigoPoa', 'CodigoProyecto','DetallesRequerimientosIDs'], 'string'],
            [['Salida', 'Regreso', 'FechaCreacion','Codigos','Tipo','Importe','Monto','Codificacion','PersonaID','DetalleRequerimientoID'], 'safe'],
            [['Correlativo', 'Situacion', 'Estado', 'AreSubCategoriaID', 'ActividadID', 'ComponenteID', 'RequerimientoID', 'NroDias', 'AsigHoras','Annio'], 'integer'],
            //[['RequerimientoID'], 'exist', 'skipOnError' => true, 'targetClass' => Requerimiento::className(), 'targetAttribute' => ['RequerimientoID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nombre' => 'Nombre',
            'Apellido' => 'Apellido',
            'Dni' => 'Dni',
            'Cargo' => 'Cargo',
            'PlanTrabajo' => 'Plan Trabajo',
            'Regimen' => 'Regimen',
            'TipoViaje' => 'Tipo Viaje',
            'Destino' => 'Destino',
            'Salida' => 'Salida',
            'Regreso' => 'Regreso',
            'FteFto' => 'Fte Fto',
            'CuentaBancaria' => 'Cuenta Bancaria',
            'LugarFecha' => 'Lugar Fecha',
            'Correlativo' => 'Correlativo',
            'Situacion' => 'Situacion',
            'Estado' => 'Estado',
            'FechaCreacion' => 'Fecha Creacion',
            'CodigoPoa' => 'Codigo Poa',
            'CodigoProyecto' => 'Codigo Proyecto',
            'AreSubCategoriaID' => 'Are Sub Categoria ID',
            'ActividadID' => 'Actividad ID',
            'ComponenteID' => 'Componente ID',
            'RequerimientoID' => 'Requerimiento ID',
            'NroDias' => 'Nro Dias',
            'AsigHoras' => 'Asig Horas',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetalleViaticos()
    {
        return $this->hasMany(DetalleViatico::className(), ['ViaticoID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequerimiento()
    {
        return $this->hasOne(Requerimiento::className(), ['ID' => 'RequerimientoID']);
    }
}
