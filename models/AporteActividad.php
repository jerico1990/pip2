<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "AporteActividad".
 *
 * @property integer $ID
 * @property integer $ActividadID
 * @property integer $EntidadParticipanteID
 * @property string $Monetario
 * @property string $NoMonetario
 *
 * @property Actividad $actividad
 * @property AporteActividadPC[] $aporteActividadPCs
 */
class AporteActividad extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'AporteActividad';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ActividadID', 'EntidadParticipanteID', 'Monetario', 'NoMonetario'], 'required'],
            [['ActividadID', 'EntidadParticipanteID'], 'integer'],
            [['Monetario', 'NoMonetario'], 'number'],
            [['ActividadID'], 'exist', 'skipOnError' => true, 'targetClass' => Actividad::className(), 'targetAttribute' => ['ActividadID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ActividadID' => 'Actividad ID',
            'EntidadParticipanteID' => 'Entidad Participante ID',
            'Monetario' => 'Monetario',
            'NoMonetario' => 'No Monetario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActividad()
    {
        return $this->hasOne(Actividad::className(), ['ID' => 'ActividadID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAporteActividadPCs()
    {
        return $this->hasMany(AporteActividadPC::className(), ['AporteActividadID' => 'ID']);
    }
}
