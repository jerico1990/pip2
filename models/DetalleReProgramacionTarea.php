<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "DetalleReProgramacionTarea".
 *
 * @property integer $ID
 * @property double $MetaFisica
 * @property double $CostoUnitario
 * @property integer $Mes
 * @property integer $ReProgramacionTareaID
 */
class DetalleReProgramacionTarea extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DetalleReProgramacionTarea';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['MetaFisica', 'CostoUnitario'], 'number'],
            [['Mes', 'ReProgramacionTareaID'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'MetaFisica' => 'Meta Fisica',
            'CostoUnitario' => 'Costo Unitario',
            'Mes' => 'Mes',
            'ReProgramacionTareaID' => 'Re Programacion Tarea ID',
        ];
    }
}
