<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "MiembroEquipoGestion".
 *
 * @property integer $ID
 * @property integer $InvestigadorID
 * @property integer $PersonaID
 * @property integer $RolEquipoTecnicoID
 * @property integer $EntidadParticipanteID
 * @property string $TituloObtenido
 * @property string $Experiencia
 * @property string $Extension_CV
 *
 * @property EntidadParticipante $entidadParticipante
 * @property Persona $persona
 * @property Investigador $investigador
 * @property RolEquipoTecnico $rolEquipoTecnico
 */
class MiembroEquipoGestion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'MiembroEquipoGestion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['InvestigadorID', 'PersonaID', 'RolEquipoTecnicoID', 'EntidadParticipanteID', 'TituloObtenido', 'Experiencia'], 'required'],
            [['InvestigadorID', 'PersonaID', 'RolEquipoTecnicoID', 'EntidadParticipanteID'], 'integer'],
            // [['TituloObtenido', 'Experiencia', 'Extension_CV'], 'string'],
            [['EntidadParticipanteID'], 'exist', 'skipOnError' => true, 'targetClass' => EntidadParticipante::className(), 'targetAttribute' => ['EntidadParticipanteID' => 'ID']],
            [['PersonaID'], 'exist', 'skipOnError' => true, 'targetClass' => Persona::className(), 'targetAttribute' => ['PersonaID' => 'ID']],
            [['InvestigadorID'], 'exist', 'skipOnError' => true, 'targetClass' => Investigador::className(), 'targetAttribute' => ['InvestigadorID' => 'ID']],
            [['RolEquipoTecnicoID'], 'exist', 'skipOnError' => true, 'targetClass' => RolEquipoTecnico::className(), 'targetAttribute' => ['RolEquipoTecnicoID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'InvestigadorID' => 'Investigador ID',
            'PersonaID' => 'Persona ID',
            'RolEquipoTecnicoID' => 'Rol Equipo Tecnico ID',
            'EntidadParticipanteID' => 'Entidad Participante ID',
            'TituloObtenido' => 'Titulo Obtenido',
            'Experiencia' => 'Experiencia',
            'Extension_CV' => 'Extension  Cv',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntidadParticipante()
    {
        return $this->hasOne(EntidadParticipante::className(), ['ID' => 'EntidadParticipanteID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersona()
    {
        return $this->hasOne(Persona::className(), ['ID' => 'PersonaID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvestigador()
    {
        return $this->hasOne(Investigador::className(), ['ID' => 'InvestigadorID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRolEquipoTecnico()
    {
        return $this->hasOne(RolEquipoTecnico::className(), ['ID' => 'RolEquipoTecnicoID']);
    }
}
