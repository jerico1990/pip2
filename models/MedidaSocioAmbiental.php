<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "MedidaSocioAmbiental".
 *
 * @property integer $ID
 * @property string $Descripcion
 * @property string $FechaRegistro
 * @property integer $InformeTecnicoFinancieroID
 */
class MedidaSocioAmbiental extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'MedidaSocioAmbiental';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Descripcion'], 'string'],
            [['FechaRegistro'], 'safe'],
            [['InformeTecnicoFinancieroID'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Descripcion' => 'Descripcion',
            'FechaRegistro' => 'Fecha Registro',
            'InformeTecnicoFinancieroID' => 'Informe Tecnico Financiero ID',
        ];
    }
}
