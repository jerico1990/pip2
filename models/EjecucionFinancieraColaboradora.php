<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "EjecucionFinancieraColaboradora".
 *
 * @property integer $ID
 * @property integer $InformeTecnicoFinancieroID
 * @property string $CodigoProyecto
 * @property integer $ColaboradoraID
 * @property integer $TipoAporte
 * @property integer $ActividadID
 * @property double $TotalProgramado
 * @property double $MetaSemestralEjecutada
 * @property double $Avance
 * @property string $DetalleAporte
 */
class EjecucionFinancieraColaboradora extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'EjecucionFinancieraColaboradora';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['InformeTecnicoFinancieroID', 'ColaboradoraID', 'TipoAporte', 'ActividadID'], 'integer'],
            [['CodigoProyecto', 'DetalleAporte'], 'string'],
            [['MetaTotalProgramado', 'MetaSemestralEjecutada', 'Avance'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'InformeTecnicoFinancieroID' => 'Informe Tecnico Financiero ID',
            'CodigoProyecto' => 'Codigo Proyecto',
            'ColaboradoraID' => 'Colaboradora ID',
            'TipoAporte' => 'Tipo Aporte',
            'ActividadID' => 'Actividad ID',
            'MetaTotalProgramado' => 'Total Programado',
            'MetaSemestralEjecutada' => 'Meta Semestral Ejecutada',
            'Avance' => 'Avance',
            'DetalleAporte' => 'Detalle Aporte',
        ];
    }
}
