<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "GastosGenerales".
 *
 * @property integer $ID
 * @property integer $PasoCritico
 * @property string $CodigoProyecto
 * @property string $FechaUnidadEjecutora
 * @property string $NumeroUnidadEjecutora
 * @property string $FechaDocumentoGasto
 * @property string $TipoDocumentoGasto
 * @property string $ClaseDocumentoGasto
 * @property string $NumeroSerieDocumentoGasto
 * @property string $NroDocumentoGasto
 * @property string $RucProveedor
 * @property string $RazonSocialProveedor
 * @property string $ConceptoProveedor
 * @property integer $CorrelativoObjetivo
 * @property string $Objetivo
 * @property integer $CorrelativoActividad
 * @property string $Actividad
 * @property string $CodigoMatriz
 * @property string $Recurso
 * @property integer $Cantidad
 * @property integer $MesEjecucion
 * @property string $TipoCheque
 * @property string $NroCheque
 * @property integer $Estado
 * @property integer $Situacion
 * @property string $FechaRegistro
 * @property double $Importe
 * @property string $Clasificacion
 */
class GastosGenerales extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'GastosGenerales';
    }

    /**
     * @inheritdoc
     */
    
    public $Codigos;
    public $ObjetivoActividad;
    public $Mes;
    public $MetaFisica;
    public $CostoUnitario;
    public $CodigoMatriz2;
    public function rules()
    {
        return [
            [['PasoCritico', 'CorrelativoObjetivo', 'CorrelativoActividad', 'Cantidad', 'MesEjecucion', 'Estado', 'Situacion'], 'integer'],
            [['CodigoProyecto', 'FechaUnidadEjecutora', 'NumeroUnidadEjecutora', 'FechaDocumentoGasto', 'TipoDocumentoGasto', 'ClaseDocumentoGasto', 'NumeroSerieDocumentoGasto', 'NroDocumentoGasto', 'RucProveedor', 'RazonSocialProveedor', 'ConceptoProveedor', 'Objetivo', 'Actividad', 'Recurso', 'TipoCheque', 'NroCheque', 'Clasificacion','ClaseOtro'], 'string'],
            [['FechaRegistro','Codigos','ObjetivoActividad','Mes','MetaFisica','CostoUnitario','CodigoMatriz2','CodigoMatriz'], 'safe'],
            [['Importe'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'PasoCritico' => 'Paso Critico',
            'CodigoProyecto' => 'Codigo Proyecto',
            'FechaUnidadEjecutora' => 'Fecha Unidad Ejecutora',
            'NumeroUnidadEjecutora' => 'Numero Unidad Ejecutora',
            'FechaDocumentoGasto' => 'Fecha Documento Gasto',
            'TipoDocumentoGasto' => 'Tipo Documento Gasto',
            'ClaseDocumentoGasto' => 'Clase Documento Gasto',
            'NumeroSerieDocumentoGasto' => 'Numero Serie Documento Gasto',
            'NroDocumentoGasto' => 'Nro Documento Gasto',
            'RucProveedor' => 'Ruc Proveedor',
            'RazonSocialProveedor' => 'Razon Social Proveedor',
            'ConceptoProveedor' => 'Concepto Proveedor',
            'CorrelativoObjetivo' => 'Correlativo Objetivo',
            'Objetivo' => 'Objetivo',
            'CorrelativoActividad' => 'Correlativo Actividad',
            'Actividad' => 'Actividad',
            'CodigoMatriz' => 'Codigo Matriz',
            'Recurso' => 'Recurso',
            'Cantidad' => 'Cantidad',
            'MesEjecucion' => 'Mes Ejecucion',
            'TipoCheque' => 'Tipo Cheque',
            'NroCheque' => 'Nro Cheque',
            'Estado' => 'Estado',
            'Situacion' => 'Situacion',
            'FechaRegistro' => 'Fecha Registro',
            'Importe' => 'Importe',
            'Clasificacion' => 'Clasificacion',
        ];
    }
}
