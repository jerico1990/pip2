<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "FondoRecibidoEntidad".
 *
 * @property integer $ID
 * @property integer $EntidadParticipanteID
 * @property string $InstitucionOtorgante
 * @property string $TituloProyecto
 * @property string $Monto
 * @property string $FechaInicio
 * @property string $FechaCierre
 * @property integer $NumeroEntidadesParticipantes
 *
 * @property EntidadParticipante $entidadParticipante
 */
class FondoRecibidoEntidad extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'FondoRecibidoEntidad';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['EntidadParticipanteID', 'InstitucionOtorgante', 'TituloProyecto', 'Monto', 'FechaInicio', 'FechaCierre', 'NumeroEntidadesParticipantes'], 'required'],
            [['EntidadParticipanteID', 'NumeroEntidadesParticipantes'], 'integer'],
            [['InstitucionOtorgante', 'TituloProyecto'], 'string'],
            [['Monto'], 'number'],
            [['FechaInicio', 'FechaCierre'], 'safe'],
            [['EntidadParticipanteID'], 'exist', 'skipOnError' => true, 'targetClass' => EntidadParticipante::className(), 'targetAttribute' => ['EntidadParticipanteID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'EntidadParticipanteID' => 'Entidad Participante ID',
            'InstitucionOtorgante' => 'Institucion Otorgante',
            'TituloProyecto' => 'Titulo Proyecto',
            'Monto' => 'Monto',
            'FechaInicio' => 'Fecha Inicio',
            'FechaCierre' => 'Fecha Cierre',
            'NumeroEntidadesParticipantes' => 'Numero Entidades Participantes',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntidadParticipante()
    {
        return $this->hasOne(EntidadParticipante::className(), ['ID' => 'EntidadParticipanteID']);
    }
}
