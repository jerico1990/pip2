<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "DetalleCertificacionPresupuestal".
 *
 * @property integer $ID
 * @property integer $CertificacionPresupuestalID
 * @property string $Codigo
 * @property string $Monto
 * @property string $CodigoCertificacion
 * @property string $Bienes
 * @property string $Servicio
 * @property integer $CodigoPoaPNIA
 *
 * @property CertificacionPresupuestal $certificacionPresupuestal
 */
class DetalleCertificacionPresupuestal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DetalleCertificacionPresupuestal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CertificacionPresupuestalID', 'CodigoPoaPNIA'], 'integer'],
            [['Codigo', 'CodigoCertificacion'], 'string'],
            [['Monto', 'Bienes', 'Servicio'], 'number'],
            [['CertificacionPresupuestalID'], 'exist', 'skipOnError' => true, 'targetClass' => CertificacionPresupuestal::className(), 'targetAttribute' => ['CertificacionPresupuestalID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'CertificacionPresupuestalID' => 'Certificacion Presupuestal ID',
            'Codigo' => 'Codigo',
            'Monto' => 'Monto',
            'CodigoCertificacion' => 'Codigo Certificacion',
            'Bienes' => 'Bienes',
            'Servicio' => 'Servicio',
            'CodigoPoaPNIA' => 'Codigo Poa Pnia',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCertificacionPresupuestal()
    {
        return $this->hasOne(CertificacionPresupuestal::className(), ['ID' => 'CertificacionPresupuestalID']);
    }
}
