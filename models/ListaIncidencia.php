<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ListaIncidencia".
 *
 * @property integer $ID
 * @property string $CodigoProyecto
 * @property integer $InformeTecnicoFinancieroID
 * @property integer $Tipo
 * @property string $Fecha
 * @property string $MedidaCorrectiva
 * @property string $MedidaPreventiva
 * @property integer $NumeroInvolucrados
 * @property integer $DanoPersona
 * @property double $Costo
 * @property string $FechaRegistro
 * @property integer $Estado
 */
class ListaIncidencia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ListaIncidencia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CodigoProyecto', 'MedidaCorrectiva', 'MedidaPreventiva'], 'string'],
            [['InformeTecnicoFinancieroID', 'Tipo', 'NumeroInvolucrados', 'DanoPersona', 'Estado'], 'integer'],
            [['Fecha', 'FechaRegistro'], 'safe'],
            [['Costo'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'CodigoProyecto' => 'Codigo Proyecto',
            'InformeTecnicoFinancieroID' => 'Informe Tecnico Financiero ID',
            'Tipo' => 'Tipo',
            'Fecha' => 'Fecha',
            'MedidaCorrectiva' => 'Medida Correctiva',
            'MedidaPreventiva' => 'Medida Preventiva',
            'NumeroInvolucrados' => 'Numero Involucrados',
            'DanoPersona' => 'Dano Persona',
            'Costo' => 'Costo',
            'FechaRegistro' => 'Fecha Registro',
            'Estado' => 'Estado',
        ];
    }
}
