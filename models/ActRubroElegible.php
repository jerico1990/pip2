<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ActRubroElegible".
 *
 * @property integer $ID
 * @property integer $ActividadID
 * @property integer $RubroElegibleID
 * @property string $UnidadMedida
 * @property string $CostoUnitario
 * @property string $MetaFisica
 * @property string $Total
 * @property string $TotalFinanciamientoPnia
 * @property string $TotalFinanciamientoAlianza
 *
 * @property Actividad $actividad
 * @property AporteActRubroElegible[] $aporteActRubroElegibles
 * @property AreSubCategoria[] $areSubCategorias
 * @property CronogramaActRubroElegible[] $cronogramaActRubroElegibles
 */
class ActRubroElegible extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $RubroElegible;
    public $Recurso;
    public $Nombre;
    public $ActRubroElegibleID;
    public $MetaFisicaCronograma;
    public $Codificacion;
    // public $RubroElegibleID;
    // public $ID;
    public static function tableName()
    {
        return 'ActRubroElegible';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['ActividadID', 'RubroElegibleID', 'UnidadMedida', 'CostoUnitario'], 'required'],
            
            [['ActividadID', 'RubroElegibleID'], 'integer'],
            [['UnidadMedida'], 'string'],
            [['CostoUnitario', 'MetaFisica', 'Total', 'TotalFinanciamientoPnia', 'TotalFinanciamientoAlianza'], 'number'],
            [['ActividadID'], 'exist', 'skipOnError' => true, 'targetClass' => Actividad::className(), 'targetAttribute' => ['ActividadID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ActividadID' => 'Actividad ID',
            'RubroElegibleID' => 'Rubro Elegible ID',
            'UnidadMedida' => 'Unidad Medida',
            'CostoUnitario' => 'Costo Unitario',
            'MetaFisica' => 'Meta Fisica',
            'Total' => 'Total',
            'TotalFinanciamientoPnia' => 'Total Financiamiento Pnia',
            'TotalFinanciamientoAlianza' => 'Total Financiamiento Alianza',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActividad()
    {
        return $this->hasOne(Actividad::className(), ['ID' => 'ActividadID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAporteActRubroElegibles()
    {
        return $this->hasMany(AporteActRubroElegible::className(), ['ActRubroElegibleID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAreSubCategorias()
    {
        return $this->hasMany(AreSubCategoria::className(), ['ActRubroElegibleID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCronogramaActRubroElegibles()
    {
        return $this->hasMany(CronogramaActRubroElegible::className(), ['ActRubroElegibleID' => 'ID']);
    }
}
