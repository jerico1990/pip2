<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "DocumentoInformacionGeneral".
 *
 * @property integer $ID
 * @property integer $InformacionGeneralID
 * @property string $Documento
 */
class DocumentoInformacionGeneral extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DocumentoInformacionGeneral';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['InformacionGeneralID'], 'integer'],
            [['Documento'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'InformacionGeneralID' => 'Informacion General ID',
            'Documento' => 'Documento',
        ];
    }
}
