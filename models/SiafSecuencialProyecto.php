<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "SiafSecuencialProyecto".
 *
 * @property integer $ID
 * @property string $CodigoProyecto
 * @property integer $Secuencial
 * @property integer $SecuencialOrdenServicioFactura
 * @property integer $SecuencialOrdenServicioRH
 * @property integer $SecuencialOrdenCompra
 * @property integer $SecuencialCajaChica
 * @property integer $SecuencialViatico
 * @property integer $SecuencialEncargo
 */
class SiafSecuencialProyecto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SiafSecuencialProyecto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CodigoProyecto'], 'string'],
            [['Secuencial', 'SecuencialOrdenServicioFactura', 'SecuencialOrdenServicioRH', 'SecuencialOrdenCompra', 'SecuencialCajaChica', 'SecuencialViatico', 'SecuencialEncargo'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'CodigoProyecto' => 'Codigo Proyecto',
            'Secuencial' => 'Secuencial',
            'SecuencialOrdenServicioFactura' => 'Secuencial Orden Servicio Factura',
            'SecuencialOrdenServicioRH' => 'Secuencial Orden Servicio Rh',
            'SecuencialOrdenCompra' => 'Secuencial Orden Compra',
            'SecuencialCajaChica' => 'Secuencial Caja Chica',
            'SecuencialViatico' => 'Secuencial Viatico',
            'SecuencialEncargo' => 'Secuencial Encargo',
        ];
    }
}
