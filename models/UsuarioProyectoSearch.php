<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UsuarioProyecto;

/**
 * UsuarioProyectoSearch represents the model behind the search form about `app\models\UsuarioProyecto`.
 */
class UsuarioProyectoSearch extends UsuarioProyecto
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'UsuarioID', 'Estado'], 'integer'],
            [['CodigoProyecto', 'FechaRegistro'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UsuarioProyecto::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'UsuarioID' => $this->UsuarioID,
            'Estado' => $this->Estado,
            'FechaRegistro' => $this->FechaRegistro,
        ]);

        $query->andFilterWhere(['like', 'CodigoProyecto', $this->CodigoProyecto]);

        return $dataProvider;
    }
}
