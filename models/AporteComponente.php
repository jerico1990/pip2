<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "AporteComponente".
 *
 * @property integer $ID
 * @property integer $ComponenteID
 * @property integer $EntidadParticipanteID
 * @property string $Monetario
 * @property string $NoMonetario
 *
 * @property Componente $componente
 * @property AporteComponentePC[] $aporteComponentePCs
 */
class AporteComponente extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'AporteComponente';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ComponenteID', 'EntidadParticipanteID', 'Monetario', 'NoMonetario'], 'required'],
            [['ComponenteID', 'EntidadParticipanteID'], 'integer'],
            [['Monetario', 'NoMonetario'], 'number'],
            [['ComponenteID'], 'exist', 'skipOnError' => true, 'targetClass' => Componente::className(), 'targetAttribute' => ['ComponenteID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ComponenteID' => 'Componente ID',
            'EntidadParticipanteID' => 'Entidad Participante ID',
            'Monetario' => 'Monetario',
            'NoMonetario' => 'No Monetario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComponente()
    {
        return $this->hasOne(Componente::className(), ['ID' => 'ComponenteID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAporteComponentePCs()
    {
        return $this->hasMany(AporteComponentePC::className(), ['AporteComponenteID' => 'ID']);
    }
}
