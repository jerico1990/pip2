<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "CronogramaAreSubCategoria".
 *
 * @property integer $ID
 * @property integer $AreSubCategoriaID
 * @property integer $Mes
 * @property string $MetaFinanciera
 * @property string $PoafMetaFinanciera
 *
 * @property AreSubCategoria $areSubCategoria
 */
class CronogramaAreSubCategoria extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CronogramaAreSubCategoria';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['AreSubCategoriaID', 'Mes', 'MetaFisica'], 'required'],
            [['AreSubCategoriaID', 'Mes'], 'integer'],
            [['MetaFinanciera', 'PoafMetaFinanciera','MetaFisica'], 'number'],
            [['AreSubCategoriaID'], 'exist', 'skipOnError' => true, 'targetClass' => AreSubCategoria::className(), 'targetAttribute' => ['AreSubCategoriaID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'AreSubCategoriaID' => 'Are Sub Categoria ID',
            'Mes' => 'Mes',
            'MetaFinanciera' => 'Meta Financiera',
            'PoafMetaFinanciera' => 'Poaf Meta Financiera',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAreSubCategoria()
    {
        return $this->hasOne(AreSubCategoria::className(), ['ID' => 'AreSubCategoriaID']);
    }
}
