<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "RubroElegible".
 *
 * @property integer $ID
 * @property string $Nombre
 * @property integer $TipoRubroElegibleID
 *
 * @property TipoRubroElegible $tipoRubroElegible
 */
class RubroElegible extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'RubroElegible';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nombre', 'TipoRubroElegibleID'], 'required'],
            [['Nombre'], 'string'],
            [['TipoRubroElegibleID'], 'integer'],
            [['TipoRubroElegibleID'], 'exist', 'skipOnError' => true, 'targetClass' => TipoRubroElegible::className(), 'targetAttribute' => ['TipoRubroElegibleID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nombre' => 'Nombre',
            'TipoRubroElegibleID' => 'Tipo Rubro Elegible ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoRubroElegible()
    {
        return $this->hasOne(TipoRubroElegible::className(), ['ID' => 'TipoRubroElegibleID']);
    }
}
