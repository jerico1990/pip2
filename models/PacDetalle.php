<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "PacDetalle".
 *
 * @property integer $ID
 * @property integer $AreSubCategoriaID
 * @property integer $CategoriaID
 * @property string $Cantidad
 * @property string $CostoEstimado
 * @property string $MetodoAdquisicion
 * @property integer $FuenteFinanciamientoID
 * @property string $MontoFuenteFinanciamiento
 * @property string $FechaInicioPA
 * @property string $FechaFinPA
 * @property string $Observaciones
 * @property integer $NumeracionPAC
 * @property integer $Estado
 * @property string $FechaRegistro
 * @property integer $PacID
 * @property string $Identificador
 * @property string $Codificacion
 * @property integer $Situacion
 */
class PacDetalle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'PacDetalle';
    }

    /**
     * @inheritdoc
     */

    public $Nombre;
    public $UnidadMedida;
    public $CostoUnitario;
    public $Categoria;
    public $Codigo;
    public $RazonSocial;
    public $AporteMonetario;
    public $Especifica;
    public $Detalle;
    
    public function rules()
    {
        return [
            [['AreSubCategoriaID', 'CategoriaID', 'FuenteFinanciamientoID', 'Estado', 'PacID', 'Situacion'], 'integer'],
            [['Cantidad', 'CostoEstimado', 'MontoFuenteFinanciamiento'], 'number'],
            [['NombrePac','UnidadMedidaPac','MetodoAdquisicion','NumeracionPAC', 'FechaInicioPA', 'FechaFinPA', 'Observaciones', 'Identificador', 'Codificacion','Correlativo'], 'string'],
            [['CostoUnitarioPac','FechaRegistro'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'AreSubCategoriaID' => 'Are Sub Categoria ID',
            'CategoriaID' => 'Categoria ID',
            'Cantidad' => 'Cantidad',
            'CostoEstimado' => 'Costo Estimado',
            'MetodoAdquisicion' => 'Metodo Adquisicion',
            'FuenteFinanciamientoID' => 'Fuente Financiamiento ID',
            'MontoFuenteFinanciamiento' => 'Monto Fuente Financiamiento',
            'FechaInicioPA' => 'Fecha Inicio Pa',
            'FechaFinPA' => 'Fecha Fin Pa',
            'Observaciones' => 'Observaciones',
            'NumeracionPAC' => 'Numeracion Pac',
            'Estado' => 'Estado',
            'FechaRegistro' => 'Fecha Registro',
            'PacID' => 'Pac ID',
            'Identificador' => 'Identificador',
            'Codificacion' => 'Codificacion',
            'Situacion' => 'Situacion',
        ];
    }
}
