<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ComprobantePago".
 *
 * @property integer $ID
 * @property string $CodigoProyecto
 * @property string $Documento
 * @property integer $Estado
 * @property integer $CompromisoPresupuestalID
 * @property integer $Situacion
 */
class ComprobantePago extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ComprobantePago';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CodigoProyecto', 'Archivo','Descripcion'], 'string'],
            [['Estado', 'CompromisoPresupuestalID', 'Situacion'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'CodigoProyecto' => 'Codigo Proyecto',
            'Archivo' => 'Archivo',
            'Estado' => 'Estado',
            'CompromisoPresupuestalID' => 'Compromiso Presupuestal ID',
            'Situacion' => 'Situacion',
        ];
    }
}
