<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "PlanNegocios".
 *
 * @property integer $ID
 * @property integer $InvestigadorID
 * @property string $CaracteristicasProductos
 * @property string $IdeaPlan
 * @property string $CaracteristicasDemanda
 * @property string $CaracteristicasOferta
 * @property string $PrincipalesClientes
 * @property string $ContribucionColaboradoras
 * @property string $ResultadosEvaluacionEconomica
 * @property string $Extension_AnexosEstadisticos
 * @property string $Extension_CartaIntencionCompra
 * @property string $Extension_ProyeccionVentas
 * @property string $Extension_AnalisisCostos
 *
 * @property Postulante $investigador
 * @property ProductoPlanNegocios[] $productoPlanNegocios
 */
class PlanNegocios extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'PlanNegocios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['InvestigadorID'], 'required'],
            [['InvestigadorID'], 'integer'],
            [['CaracteristicasProductos', 'IdeaPlan', 'CaracteristicasDemanda', 'CaracteristicasOferta', 'PrincipalesClientes', 'ContribucionColaboradoras', 'ResultadosEvaluacionEconomica', 'Extension_AnexosEstadisticos', 'Extension_CartaIntencionCompra', 'Extension_ProyeccionVentas', 'Extension_AnalisisCostos'], 'string'],
            [['InvestigadorID'], 'exist', 'skipOnError' => true, 'targetClass' => Postulante::className(), 'targetAttribute' => ['InvestigadorID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'InvestigadorID' => 'Investigador ID',
            'CaracteristicasProductos' => 'Caracteristicas Productos',
            'IdeaPlan' => 'Idea Plan',
            'CaracteristicasDemanda' => 'Caracteristicas Demanda',
            'CaracteristicasOferta' => 'Caracteristicas Oferta',
            'PrincipalesClientes' => 'Principales Clientes',
            'ContribucionColaboradoras' => 'Contribucion Colaboradoras',
            'ResultadosEvaluacionEconomica' => 'Resultados Evaluacion Economica',
            'Extension_AnexosEstadisticos' => 'Extension  Anexos Estadisticos',
            'Extension_CartaIntencionCompra' => 'Extension  Carta Intencion Compra',
            'Extension_ProyeccionVentas' => 'Extension  Proyeccion Ventas',
            'Extension_AnalisisCostos' => 'Extension  Analisis Costos',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvestigador()
    {
        return $this->hasOne(Postulante::className(), ['ID' => 'InvestigadorID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductoPlanNegocios()
    {
        return $this->hasMany(ProductoPlanNegocios::className(), ['PlanNegociosID' => 'ID']);
    }
}
