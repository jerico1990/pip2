<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "IndicadorPasoCritico".
 *
 * @property integer $ID
 * @property integer $PasoCriticoID
 * @property string $Descripcion
 * @property string $UnidadMedida
 * @property string $Cantidad
 *
 * @property PasoCritico $pasoCritico
 */
class IndicadorPasoCritico extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'IndicadorPasoCritico';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['PasoCriticoID', 'Descripcion', 'UnidadMedida'], 'required'],
            [['PasoCriticoID'], 'integer'],
            [['Descripcion', 'UnidadMedida'], 'string'],
            [['Cantidad'], 'number'],
            [['PasoCriticoID'], 'exist', 'skipOnError' => true, 'targetClass' => PasoCritico::className(), 'targetAttribute' => ['PasoCriticoID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'PasoCriticoID' => 'Paso Critico ID',
            'Descripcion' => 'Descripcion',
            'UnidadMedida' => 'Unidad Medida',
            'Cantidad' => 'Cantidad',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPasoCritico()
    {
        return $this->hasOne(PasoCritico::className(), ['ID' => 'PasoCriticoID']);
    }
}
