<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "EjecucionMetaFisicaFinanciera".
 *
 * @property integer $ID
 * @property integer $InformeTecnicoFinancieroID
 * @property integer $ActividadID
 * @property string $Descripcion
 */
class EjecucionMetaFisicaFinanciera extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'EjecucionMetaFisicaFinanciera';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['InformeTecnicoFinancieroID', 'ActividadID'], 'integer'],
            [['Descripcion'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'InformeTecnicoFinancieroID' => 'Informe Tecnico Financiero ID',
            'ActividadID' => 'Actividad ID',
            'Descripcion' => 'Descripcion',
        ];
    }
}
