<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "AmbitoIntervencion".
 *
 * @property integer $ID
 * @property integer $InformacionGeneralID
 * @property string $DistritoID
 *
 * @property InformacionGeneral $informacionGeneral
 * @property Ubigeo $distrito
 */
class AmbitoIntervencion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $DepartamentoID;
    public $ProvinciaID;
    public $Departamento;
    public $Provincia;
    public $Distrito;
    public static function tableName()
    {
        return 'AmbitoIntervencion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['InformacionGeneralID', 'DistritoID'], 'required'],
            [['InformacionGeneralID'], 'integer'],
            [['DistritoID','Latitud','Longitud'], 'string'],
            [['InformacionGeneralID'], 'exist', 'skipOnError' => true, 'targetClass' => InformacionGeneral::className(), 'targetAttribute' => ['InformacionGeneralID' => 'ID']],
            [['DistritoID'], 'exist', 'skipOnError' => true, 'targetClass' => Ubigeo::className(), 'targetAttribute' => ['DistritoID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'InformacionGeneralID' => 'Informacion General ID',
            'DistritoID' => 'Distrito ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformacionGeneral()
    {
        return $this->hasOne(InformacionGeneral::className(), ['ID' => 'InformacionGeneralID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrito()
    {
        return $this->hasOne(Ubigeo::className(), ['ID' => 'DistritoID']);
    }
}
