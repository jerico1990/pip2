<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "PrincipalSupuesto".
 *
 * @property integer $ID
 * @property integer $ProductoPlanNegociosID
 * @property string $Cantidad
 * @property string $PrecioUnitario
 * @property string $CostoUnitario
 * @property integer $Anio
 * @property integer $TipoProyecto
 *
 * @property ProductoPlanNegocios $productoPlanNegocios
 */
class PrincipalSupuesto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'PrincipalSupuesto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ProductoPlanNegociosID', 'Cantidad', 'PrecioUnitario', 'CostoUnitario', 'Anio', 'TipoProyecto'], 'required'],
            [['ProductoPlanNegociosID', 'Anio', 'TipoProyecto'], 'integer'],
            [['Cantidad', 'PrecioUnitario', 'CostoUnitario'], 'number'],
            [['ProductoPlanNegociosID'], 'exist', 'skipOnError' => true, 'targetClass' => ProductoPlanNegocios::className(), 'targetAttribute' => ['ProductoPlanNegociosID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ProductoPlanNegociosID' => 'Producto Plan Negocios ID',
            'Cantidad' => 'Cantidad',
            'PrecioUnitario' => 'Precio Unitario',
            'CostoUnitario' => 'Costo Unitario',
            'Anio' => 'Anio',
            'TipoProyecto' => 'Tipo Proyecto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductoPlanNegocios()
    {
        return $this->hasOne(ProductoPlanNegocios::className(), ['ID' => 'ProductoPlanNegociosID']);
    }
}
