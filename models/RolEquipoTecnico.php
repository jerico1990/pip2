<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "RolEquipoTecnico".
 *
 * @property integer $ID
 * @property string $Nombre
 *
 * @property MiembroEquipoGestion[] $miembroEquipoGestions
 * @property MiembroEquipoInvestigacion[] $miembroEquipoInvestigacions
 */
class RolEquipoTecnico extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'RolEquipoTecnico';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nombre'], 'required'],
            [['Nombre'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMiembroEquipoGestions()
    {
        return $this->hasMany(MiembroEquipoGestion::className(), ['RolEquipoTecnicoID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMiembroEquipoInvestigacions()
    {
        return $this->hasMany(MiembroEquipoInvestigacion::className(), ['RolEquipoTecnicoID' => 'ID']);
    }
}
