<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "TipoGasto".
 *
 * @property integer $ID
 * @property string $Nombre
 * @property integer $Estado
 */
class TipoGasto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'TipoGasto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nombre'], 'string'],
            [['Estado'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nombre' => 'Nombre',
            'Estado' => 'Estado',
        ];
    }
}
