<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;
/**
 * This is the model class for table "Usuario".
 *
 * @property integer $ID
 * @property string $Email
 * @property string $Clave
 * @property string $CodigoVerificacion
 * @property integer $Estado
 *
 * @property Postulante[] $postulantes
 * @property UsuarioRol[] $usuarioRols
 */
class Usuario extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * @inheritdoc
     */
    public $auth_key;
    public $Contrasena;
    public $Recontrasena;
    public $Nombre;
    public $ApellidoPaterno;
    public $ApellidoMaterno;
    // $rol;
    public static function tableName()
    {
        return 'Usuario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['usuario', 'password', 'Estado'], 'required'],
            [['username', 'password', 'CodigoVerificacion','Contrasena','Recontrasena','email'], 'string'],
            [['status','PersonaID'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'CodigoVerificacion' => 'Codigo Verificacion',
            'status' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostulantes()
    {
        return $this->hasMany(Postulante::className(), ['UsuarioID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioRols()
    {
        return $this->hasMany(UsuarioRol::className(), ['UsuarioID' => 'ID']);
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public static function findByUsername($username)
    {
       return static::find()
        ->innerJoin('Persona','Usuario.PersonaID = Persona.ID')
        ->where('username=:username and status=1',[':username' => $username])->one();
    }

    public function validatePassword($password,$username)
    {
        $model=static::find()
        ->innerJoin('Persona','Usuario.PersonaID = Persona.ID')
        ->where('username=:username and status=1',[':username' => $username])->one();
        // echo $password."--------------";
        // echo $model->password.'---->';
        // print_r(Yii::$app->getSecurity()->generatePasswordHash($password) ); die();
        if(Yii::$app->getSecurity()->validatePassword($password, '$2y$13$T6OEJW0WjS30nlKofw5gTetHkSrhKPAddewnCJR1jkjdljcWw0rvu'))
        // if(Yii::$app->getSecurity()->validatePassword($password, $model->password))
        {
            return $model;
        }
        return false;
    }
    public function getUsername()
    {
        return $this->username;
    }

    public function getDatos(){
        $usuario=Usuario::findOne(\Yii::$app->user->id);
        $rol = Rol::find()->innerJoin('UsuarioRol','Rol.ID = UsuarioRol.RolID')->where('UsuarioRol.UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();

        $persona = Persona::findOne($usuario->PersonaID);

        $data = array(
            'rol'       => $rol,
            'persona'   => $persona
        );

        return $rol->Nombre.': '.$persona->ApellidoPaterno.' '.$persona->ApellidoMaterno.' '.$persona->Nombre;

    }

    public function getRol()
    {
        $rol=UsuarioRol::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>\Yii::$app->user->id])->one();
        return $rol->RolID;
    }
    public function getSituacionRecurso()
    {
        $situacion=0;
        if(\Yii::$app->user->identity->rol==7)
        {
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
            $situacion=$poa->Situacion;
        }
        return $situacion;
    }

    public function getSituacionTarea()
    {
        $situacion=0;
        if(\Yii::$app->user->identity->rol==7)
        {
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            $pat=Pat::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
            $situacion=$pat->Situacion;
        }
        return $situacion;
    }


    public function getSituacionProyecto()
    {
        $situacion=0;

        if(\Yii::$app->user->identity->rol==7)
        {
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
           // $pat=Pat    ::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
            $situacion=$proyecto->Situacion;
        }
        return $situacion;
    }
    /*
    public function getSituacionProyecto()
    {
        $deshabilitar=0;
        if(\Yii::$app->user->identity->rol==7)
        {
            $usuario=Usuario::findOne(\Yii::$app->user->id);
            $datosInvestigador=Persona::findOne($usuario->PersonaID);
            $investigador=Investigador::find()->where('UsuarioID=:UsuarioID',[':UsuarioID'=>$usuario->id])->one();
            $proyecto=Proyecto::find()->where('InvestigadorID=:InvestigadorID',[':InvestigadorID'=>$investigador->ID])->one();
            //$poa=Poa::find()->where('ProyectoID=:ProyectoID',[':ProyectoID'=>$proyecto->ID])->one();
            if($proyecto->Situacion==4)
            {
                $deshabilitar=1;
            }
        }
        return $deshabilitar;
    }
    */
    public static function findIdentityByAccessToken($token, $type = null)
    {

        if ($user['accessToken'] === $token) {
           return new static($user);
        }
        return null;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
}
