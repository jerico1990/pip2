<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "AporteAreSubCategoria".
 *
 * @property integer $ID
 * @property integer $AreSubCategoriaID
 * @property integer $EntidadParticipanteID
 * @property string $Monetario
 * @property string $NoMonetario
 *
 * @property AreSubCategoria $areSubCategoria
 * @property AporteAreSubCategoriaPC[] $aporteAreSubCategoriaPCs
 */
class AporteAreSubCategoria extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'AporteAreSubCategoria';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['AreSubCategoriaID', 'EntidadParticipanteID', 'Monetario', 'NoMonetario'], 'required'],
            [['AreSubCategoriaID', 'EntidadParticipanteID'], 'integer'],
            [['Monetario', 'NoMonetario'], 'number'],
            [['AreSubCategoriaID'], 'exist', 'skipOnError' => true, 'targetClass' => AreSubCategoria::className(), 'targetAttribute' => ['AreSubCategoriaID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'AreSubCategoriaID' => 'Are Sub Categoria ID',
            'EntidadParticipanteID' => 'Entidad Participante ID',
            'Monetario' => 'Monetario',
            'NoMonetario' => 'No Monetario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAreSubCategoria()
    {
        return $this->hasOne(AreSubCategoria::className(), ['ID' => 'AreSubCategoriaID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAporteAreSubCategoriaPCs()
    {
        return $this->hasMany(AporteAreSubCategoriaPC::className(), ['AporteAreSubCategoriaID' => 'ID']);
    }
}
