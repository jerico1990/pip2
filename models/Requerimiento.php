<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Requerimiento".
 *
 * @property integer $ID
 * @property string $CodigoProyecto
 * @property integer $Situacion
 * @property string $FechaRegistro
 * @property integer $Estado
 * @property string $Referencia
 * @property string $Asunto
 * @property integer $TipoRequerimiento
 * @property double $Cantidad
 * @property double $PrecioUnitario
 * @property integer $Pac
 */
class Requerimiento extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $Archivo;
    public $ComponenteID;
    public $Componente;
    public $ActividadID;
    public $Actividad;
    public $AreSubCategoriaID;
    public $AreSubCategoria;
    public $PrecioUnitario;
    public $Cantidad;
    public $Pac;
    public $CodigoPoa;
    public $CatalogoBienesServicios;
    public $DetallesDescripciones;
    public $DetallesCantidades;
    public $DetallesPreciosUnitarios;
    public $DetallesCodigos;
    public $DetallesRecursos;
    public $DetallesObjetivos;
    public $DetallesActividades;
    public $DetallesRecurs;
    public $DetallesIDs;
    public $DetallesCodificaciones;
    public $Sigla;
    public static function tableName()
    {
        return 'Requerimiento';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CodigoProyecto', 'Referencia', 'Asunto','Descripcion','CUT'], 'string'],
            [['Situacion', 'Estado', 'TipoRequerimiento'], 'integer'],
            [['Observacion','DetallesCodificaciones','DetallesIDs','DetallesRecurs','DetallesActividades','DetallesObjetivos','DetallesRecursos','DetallesCodigos','DetallesPreciosUnitarios','DetallesCantidades','DetallesDescripciones','CodigoPoa','Pac','CatalogoBienesServicios','Cantidad','FechaRegistro','Archivo','ComponenteID','ActividadID','AreSubCategoriaID','PrecioUnitario','FechaSolicitud'], 'safe'],
            [['Archivo'],'file']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'CodigoProyecto' => 'Codigo Proyecto',
            'Situacion' => 'Situacion',
            'FechaRegistro' => 'Fecha Registro',
            'Estado' => 'Estado',
            'Referencia' => 'Referencia',
            'Asunto' => 'Asunto',
            'TipoRequerimiento' => 'Tipo Requerimiento',
            'Cantidad' => 'Cantidad',
            'PrecioUnitario' => 'Precio Unitario',
        ];
    }
    /*
    public function getTipoServicio($tipoServicio=null)
    {
        if($tipoServicio==1)
        {
            return "TDR";
        }
        elseif($tipoServicio==2)
        {
            return "EETT";
        }
    }*/
    
    public function getTipoServicio($tipoServicio=null)
    {
        $tipoGasto = TipoGasto::find()->where('ID=:ID',[':ID'=>$tipoServicio])->one();
        return $tipoGasto->Nombre;
    }
    
    public function getSituacion($situacion=null)
    {
        if($situacion==0){
            return "Pendiente";
        }
        elseif($situacion==1)
        {
            return "Aprobado";
        }
        else if($situacion==2)
        {
            return "Pendiente de aprobar";
        }
    }
}
