<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ProgramaTransversal".
 *
 * @property integer $ID
 * @property string $Nombre
 */
class ProgramaTransversal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ProgramaTransversal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nombre'], 'required'],
            [['Nombre'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nombre' => 'Nombre',
        ];
    }
}
