<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "AreSubCategoriaObservacion".
 *
 * @property integer $ID
 * @property string $Observacion
 * @property integer $AreSubCategoriaID
 * @property integer $Estado
 */
class AreSubCategoriaObservacion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'AreSubCategoriaObservacion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Observacion'], 'string'],
            [['AreSubCategoriaID', 'Estado'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Observacion' => 'Observacion',
            'AreSubCategoriaID' => 'Are Sub Categoria ID',
            'Estado' => 'Estado',
        ];
    }
}
