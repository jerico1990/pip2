<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "DetalleSolicitudCompromisoPresupuestal".
 *
 * @property integer $ID
 * @property integer $SolicitudCompromisoPresupuestalID
 * @property string $FechaRegistro
 * @property integer $Estado
 * @property string $Proveedor
 * @property string $RUC
 * @property double $Monto
 *
 * @property SolicitudCompromisoPresupuestal $solicitudCompromisoPresupuestal
 */
class DetalleCompromisoPresupuestal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DetalleCompromisoPresupuestal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['ID'], 'required'],
            [['ID', 'CompromisoPresupuestalID','GastoElegible','Situacion'], 'integer'],
            [['FechaRegistro','Observacion'], 'safe'],
            [[ 'RUC'], 'string'],
            [['MontoTotal'], 'number'],
            [['CompromisoPresupuestalID'], 'exist', 'skipOnError' => true, 'targetClass' => CompromisoPresupuestal::className(), 'targetAttribute' => ['CompromisoPresupuestalID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'CompromisoPresupuestalID' => 'Solicitud Compromiso Presupuestal ID',
            'FechaRegistro' => 'Fecha Registro',
            'RUC' => 'Ruc',
            'MontoTotal' => 'Monto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompromisoPresupuestal()
    {
        return $this->hasOne(CompromisoPresupuestal::className(), ['ID' => 'CompromisoPresupuestalID']);
    }
}
