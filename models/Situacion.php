<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Situacion".
 *
 * @property integer $ID
 * @property string $Descripcion
 * @property integer $Estado
 * @property integer $Codigo
 */
class Situacion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Situacion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Descripcion'], 'string'],
            [['Estado', 'Codigo'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Descripcion' => 'Descripcion',
            'Estado' => 'Estado',
            'Codigo' => 'Codigo',
        ];
    }
}
