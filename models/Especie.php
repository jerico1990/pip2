<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Especie".
 *
 * @property integer $ID
 * @property string $Nombre
 * @property integer $CultivoCrianzaID
 *
 * @property CultivoCrianza $cultivoCrianza
 * @property InformacionGeneral[] $informacionGenerals
 */
class Especie extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Especie';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nombre'], 'string'],
            [['CultivoCrianzaID'], 'integer'],
            [['CultivoCrianzaID'], 'exist', 'skipOnError' => true, 'targetClass' => CultivoCrianza::className(), 'targetAttribute' => ['CultivoCrianzaID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nombre' => 'Nombre',
            'CultivoCrianzaID' => 'Cultivo Crianza ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCultivoCrianza()
    {
        return $this->hasOne(CultivoCrianza::className(), ['ID' => 'CultivoCrianzaID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformacionGenerals()
    {
        return $this->hasMany(InformacionGeneral::className(), ['EspecieID' => 'ID']);
    }
}
