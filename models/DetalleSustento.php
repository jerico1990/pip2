<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "DetalleSustento".
 *
 * @property integer $ID
 * @property integer $CodigoID
 * @property string $Documento
 * @property string $Tipo
 * @property integer $Estado 
 */
class DetalleSustento extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DetalleSustento';
    }

    /**
     * @inheritdoc
     */
    public $Archivo;
    public function rules()
    {
        return [
            [['CodigoID', 'Estado'], 'integer'],
            [['Documento', 'Tipo','Nombre'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'CodigoID' => 'Codigo ID',
            'Documento' => 'Documento',
            'Tipo' => 'Tipo',
            'Estado ' => 'Estado',
        ];
    }
}
