<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Pac".
 *
 * @property integer $ID
 * @property integer $PoaID
 * @property integer $Estado
 * @property string $FechaRegistro
 * @property integer $Situacion
 * @property integer $Anio
 * @property string $Observacion
 * @property integer $ProyectoID
 */
class Pac extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Pac';
    }

    /**
     * @inheritdoc
     */

    public $Descripcion;
    public function rules()
    {
        return [
            [['PoaID', 'Estado', 'Situacion', 'Anio', 'ProyectoID'], 'integer'],
            [['FechaRegistro'], 'safe'],
            [['Observacion'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'PoaID' => 'Poa ID',
            'Estado' => 'Estado',
            'FechaRegistro' => 'Fecha Registro',
            'Situacion' => 'Situacion',
            'Anio' => 'Anio',
            'Observacion' => 'Observacion',
            'ProyectoID' => 'Proyecto ID',
        ];
    }
}
