<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Proyecto".
 *
 * @property integer $ID
 * @property integer $InvestigadorID
 * @property string $Fin
 * @property string $Proposito
 * @property string $DescripcionComponentesInvestigacion
 * @property string $DescripcionComponentesFortalecimiento
 * @property string $Total
 * @property string $TotalFinanciamientoPnia
 * @property string $TotalFinanciamientoAlianza
 *
 * @property AporteProyecto[] $aporteProyectos
 * @property CronogramaProyecto[] $cronogramaProyectos
 * @property MarcoLogicoFinProyecto[] $marcoLogicoFinProyectos
 * @property MarcoLogicoPropositoProyecto[] $marcoLogicoPropositoProyectos
 * @property PasoCritico[] $pasoCriticos
 * @property Poa[] $poas
 * @property Poa[] $poas0
 * @property Investigador $investigador
 */
class Proyecto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Proyecto';
    }

    /**
     * @inheritdoc
     */
    // public $ComponenteID;
    // public $Nombre;
    public function rules()
    {
        return [
            [['InvestigadorID'], 'required'],
            [['InvestigadorID'], 'integer'],
            [['Fin', 'Proposito', 'DescripcionComponentesInvestigacion', 'DescripcionComponentesFortalecimiento'], 'string'],
            [['Total', 'TotalFinanciamientoPnia', 'TotalFinanciamientoAlianza'], 'number'],
            [['InvestigadorID'], 'exist', 'skipOnError' => true, 'targetClass' => Investigador::className(), 'targetAttribute' => ['InvestigadorID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'InvestigadorID' => 'Investigador ID',
            'Fin' => 'Fin',
            'Proposito' => 'Proposito',
            'DescripcionComponentesInvestigacion' => 'Descripcion Componentes Investigacion',
            'DescripcionComponentesFortalecimiento' => 'Descripcion Componentes Fortalecimiento',
            'Total' => 'Total',
            'TotalFinanciamientoPnia' => 'Total Financiamiento Pnia',
            'TotalFinanciamientoAlianza' => 'Total Financiamiento Alianza',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAporteProyectos()
    {
        return $this->hasMany(AporteProyecto::className(), ['ProyectoID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCronogramaProyectos()
    {
        return $this->hasMany(CronogramaProyecto::className(), ['ProyectoID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarcoLogicoFinProyectos()
    {
        return $this->hasMany(MarcoLogicoFinProyecto::className(), ['ProyectoID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMarcoLogicoPropositoProyectos()
    {
        return $this->hasMany(MarcoLogicoPropositoProyecto::className(), ['ProyectoID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPasoCriticos()
    {
        return $this->hasMany(PasoCritico::className(), ['ProyectoID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoas()
    {
        return $this->hasMany(Poa::className(), ['ProyectoID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoas0()
    {
        return $this->hasMany(Poa::className(), ['ProyectoID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvestigador()
    {
        return $this->hasOne(Investigador::className(), ['ID' => 'InvestigadorID']);
    }
}
