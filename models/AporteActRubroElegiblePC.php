<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "AporteActRubroElegiblePC".
 *
 * @property integer $ID
 * @property integer $AporteActRubroElegibleID
 * @property integer $PasoCriticoID
 * @property string $Monetario
 * @property string $NoMonetario
 *
 * @property AporteActRubroElegible $aporteActRubroElegible
 * @property PasoCritico $pasoCritico
 */
class AporteActRubroElegiblePC extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'AporteActRubroElegiblePC';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['AporteActRubroElegibleID', 'PasoCriticoID', 'Monetario', 'NoMonetario'], 'required'],
            [['AporteActRubroElegibleID', 'PasoCriticoID'], 'integer'],
            [['Monetario', 'NoMonetario'], 'number'],
            [['AporteActRubroElegibleID'], 'exist', 'skipOnError' => true, 'targetClass' => AporteActRubroElegible::className(), 'targetAttribute' => ['AporteActRubroElegibleID' => 'ID']],
            [['PasoCriticoID'], 'exist', 'skipOnError' => true, 'targetClass' => PasoCritico::className(), 'targetAttribute' => ['PasoCriticoID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'AporteActRubroElegibleID' => 'Aporte Act Rubro Elegible ID',
            'PasoCriticoID' => 'Paso Critico ID',
            'Monetario' => 'Monetario',
            'NoMonetario' => 'No Monetario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAporteActRubroElegible()
    {
        return $this->hasOne(AporteActRubroElegible::className(), ['ID' => 'AporteActRubroElegibleID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPasoCritico()
    {
        return $this->hasOne(PasoCritico::className(), ['ID' => 'PasoCriticoID']);
    }
}
