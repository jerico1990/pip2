<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Conformidad".
 *
 * @property integer $ID
 * @property integer $OrdenID
 * @property string $Contenido
 * @property integer $Puntuacion
 * @property string $Observacion
 * @property integer $Tipo
 * @property integer $Estado
 * @property string $FechaRegistro
 *
 * @property Orden $orden
 */
class ConformidadPago extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $archivo;
    public $Cantidades;
    public $SIAFS;
    public $CompromisoPresupuestalID;
    // public $Memorando;
    public static function tableName()
    {
        return 'ConformidadPago';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['ID'], 'required'],
            [['ID',  'Puntuacion', 'Tipo', 'Estado','CompromisoID','Annio'], 'integer'],
            [['Contenido', 'Observacion','Correlativo','Concepto','NDocumento','DocumentoCorrelativo'], 'string'],
            [['CodigoProyecto','OrdenID','SIAFS','Memorando','Cantidades','FechaRegistro','archivo','Monto'], 'safe'],
            [['archivo'],'file'],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'OrdenID' => 'Orden ID',
            'Contenido' => 'Contenido',
            'Puntuacion' => 'Puntuacion',
            'Observacion' => 'Observacion',
            'Tipo' => 'Tipo',
            'Estado' => 'Estado',
            'FechaRegistro' => 'Fecha Registro',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
}
