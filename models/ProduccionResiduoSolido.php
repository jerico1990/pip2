<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ProduccionResiduoSolido".
 *
 * @property integer $ID
 * @property string $Mes
 * @property double $Kg
 * @property integer $Tipo
 * @property string $FechaRegistro
 * @property integer $Anno
 * @property integer $InformeTecnicoFinancieroID
 */
class ProduccionResiduoSolido extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ProduccionResiduoSolido';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Mes'], 'string'],
            [['Tipo', 'Anno', 'InformeTecnicoFinancieroID'], 'integer'],
            [['FechaRegistro'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Mes' => 'Mes',
            'Tipo' => 'Tipo',
            'FechaRegistro' => 'Fecha Registro',
            'Anno' => 'Anno',
            'InformeTecnicoFinancieroID' => 'Informe Tecnico Financiero ID',
        ];
    }
}
