<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Persona".
 *
 * @property integer $ID
 * @property string $ApellidoPaterno
 * @property string $ApellidoMaterno
 * @property string $Nombre
 * @property string $NroDocumento
 * @property string $Telefono
 * @property string $Email
 *
 * @property MiembroEquipoGestion[] $miembroEquipoGestions
 * @property MiembroEquipoInvestigacion[] $miembroEquipoInvestigacions
 * @property RepresentanteLegal[] $representanteLegals
 */
class Persona extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $FuncionDesempenar;
    public $TituloObtenido;
    public $Experiencia;
    public $EntidadParticipanteID;
    public $InvestigadorID;
    public $MiembroEquipoGestionID;
    public $RolEquipoTecnicoID;
    public static function tableName()
    {
        return 'Persona';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['ApellidoPaterno', 'ApellidoMaterno', 'Nombre', 'NroDocumento', 'Telefono', 'Email'], 'required'],
            [['ApellidoPaterno', 'ApellidoMaterno', 'Nombre', 'NroDocumento', 'Telefono', 'Email'], 'string'],
            [['TipoDocumento','FuncionDesempenar','TituloObtenido','Experiencia','EntidadParticipanteID','TipoContratacion'],'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ApellidoPaterno' => 'Apellido Paterno',
            'ApellidoMaterno' => 'Apellido Materno',
            'Nombre' => 'Nombre',
            'NroDocumento' => 'Nro Documento',
            'Telefono' => 'Telefono',
            'Email' => 'Email',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMiembroEquipoGestions()
    {
        return $this->hasMany(MiembroEquipoGestion::className(), ['PersonaID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMiembroEquipoInvestigacions()
    {
        return $this->hasMany(MiembroEquipoInvestigacion::className(), ['PersonaID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepresentanteLegals()
    {
        return $this->hasMany(RepresentanteLegal::className(), ['PersonaID' => 'ID']);
    }
}
