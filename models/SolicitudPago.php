<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "SolicitudPago".
 *
 * @property integer $ID
 * @property string $Correlativo
 * @property string $JefeUnidad
 * @property string $NumeroResolucionJefatural
 * @property string $FechaSolicitud
 * @property integer $ProyectoID
 * @property double $MontoTotal
 * @property string $Clasificacion
 * @property string $FechaRegistro
 * @property integer $Estado
 * @property string $Memorando
 *
 * @property DetalleSolicitudPago[] $detalleSolicitudPagos
 */
class SolicitudPago extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SolicitudPago';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Correlativo', 'JefeUnidad', 'NumeroResolucionJefatural', 'Clasificacion', 'Memorando'], 'string'],
            [['FechaSolicitud', 'FechaRegistro'], 'safe'],
            [['ProyectoID', 'Estado'], 'integer'],
            [['MontoTotal'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Correlativo' => 'Correlativo',
            'JefeUnidad' => 'Jefe Unidad',
            'NumeroResolucionJefatural' => 'Numero Resolucion Jefatural',
            'FechaSolicitud' => 'Fecha Solicitud',
            'ProyectoID' => 'Proyecto ID',
            'MontoTotal' => 'Monto Total',
            'Clasificacion' => 'Clasificacion',
            'FechaRegistro' => 'Fecha Registro',
            'Estado' => 'Estado',
            'Memorando' => 'Memorando',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetalleSolicitudPagos()
    {
        return $this->hasMany(DetalleSolicitudPago::className(), ['SolicitudPagoID' => 'ID']);
    }
}
