<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "UsuarioProyecto".
 *
 * @property integer $ID
 * @property integer $UsuarioID
 * @property string $CodigoProyecto
 * @property integer $Estado
 * @property string $FechaRegistro
 */
class UsuarioProyecto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'UsuarioProyecto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['ID'], 'required'],
            [['ID', 'UsuarioID', 'Estado'], 'integer'],
            [['CodigoProyecto'], 'string'],
            [['FechaRegistro'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'UsuarioID' => 'Usuario ID',
            'CodigoProyecto' => 'Codigo Proyecto',
            'Estado' => 'Estado',
            'FechaRegistro' => 'Fecha Registro',
        ];
    }
}
