<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "MiembroEquipoInvestigacion".
 *
 * @property integer $ID
 * @property integer $EntidadOferenteID
 * @property integer $PersonaID
 * @property integer $RolEquipoTecnicoID
 * @property string $Genero
 * @property string $FechaNacimiento
 * @property string $Ruc
 * @property string $Domicilio
 * @property string $TituloObtenido
 * @property string $ProyectosRealizados
 * @property string $ActividadesPrincipales
 * @property string $DedicacionHoraria
 * @property string $Extension_CV
 *
 * @property EntidadOferente $entidadOferente
 * @property Persona $persona
 * @property RolEquipoTecnico $rolEquipoTecnico
 */
class MiembroEquipoInvestigacion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'MiembroEquipoInvestigacion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['EntidadOferenteID', 'PersonaID', 'RolEquipoTecnicoID', 'Genero', 'FechaNacimiento', 'Ruc', 'Domicilio', 'TituloObtenido', 'ProyectosRealizados', 'ActividadesPrincipales', 'DedicacionHoraria'], 'required'],
            [['EntidadOferenteID', 'PersonaID', 'RolEquipoTecnicoID'], 'integer'],
            [['Genero', 'Ruc', 'Domicilio', 'TituloObtenido', 'ProyectosRealizados', 'ActividadesPrincipales', 'DedicacionHoraria', 'Extension_CV'], 'string'],
            [['FechaNacimiento'], 'safe'],
            [['EntidadOferenteID'], 'exist', 'skipOnError' => true, 'targetClass' => EntidadOferente::className(), 'targetAttribute' => ['EntidadOferenteID' => 'ID']],
            [['PersonaID'], 'exist', 'skipOnError' => true, 'targetClass' => Persona::className(), 'targetAttribute' => ['PersonaID' => 'ID']],
            [['RolEquipoTecnicoID'], 'exist', 'skipOnError' => true, 'targetClass' => RolEquipoTecnico::className(), 'targetAttribute' => ['RolEquipoTecnicoID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'EntidadOferenteID' => 'Entidad Oferente ID',
            'PersonaID' => 'Persona ID',
            'RolEquipoTecnicoID' => 'Rol Equipo Tecnico ID',
            'Genero' => 'Genero',
            'FechaNacimiento' => 'Fecha Nacimiento',
            'Ruc' => 'Ruc',
            'Domicilio' => 'Domicilio',
            'TituloObtenido' => 'Titulo Obtenido',
            'ProyectosRealizados' => 'Proyectos Realizados',
            'ActividadesPrincipales' => 'Actividades Principales',
            'DedicacionHoraria' => 'Dedicacion Horaria',
            'Extension_CV' => 'Extension  Cv',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntidadOferente()
    {
        return $this->hasOne(EntidadOferente::className(), ['ID' => 'EntidadOferenteID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersona()
    {
        return $this->hasOne(Persona::className(), ['ID' => 'PersonaID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRolEquipoTecnico()
    {
        return $this->hasOne(RolEquipoTecnico::className(), ['ID' => 'RolEquipoTecnicoID']);
    }
}
