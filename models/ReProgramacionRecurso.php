<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ReProgramacionRecurso".
 *
 * @property integer $ID
 * @property integer $AreSubCategoriaID
 * @property string $Justificacion
 * @property string $Evidencia
 * @property integer $Situacion
 * @property integer $Estado
 * @property string $FechaRegistro
 */
class ReProgramacionRecurso extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ReProgramacionRecurso';
    }
    public $MetasFisicas;
    public $CostosUnitarios;
    public $Meses;
    public $Archivo;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['ID'], 'required'],
            [['ID', 'AreSubCategoriaID', 'Situacion', 'Estado'], 'integer'],
            [['Justificacion', 'Evidencia'], 'string'],
            [['FechaRegistro','CostosUnitarios','MetasFisicas','Meses','Archivo'], 'safe'],
            [['Archivo'],'file']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'AreSubCategoriaID' => 'Are Sub Categoria ID',
            'Justificacion' => 'Justificacion',
            'Evidencia' => 'Evidencia',
            'Situacion' => 'Situacion',
            'Estado' => 'Estado',
            'FechaRegistro' => 'Fecha Registro',
        ];
    }
}
