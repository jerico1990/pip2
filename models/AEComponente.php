<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "AE_Componente".
 *
 * @property integer $IdComponente
 * @property integer $IdPostulante
 * @property string $NombreComponente
 * @property integer $Orden
 * @property integer $EsGestion
 *
 * @property Postulante $idPostulante
 * @property AEComponenteValor[] $aEComponenteValors
 */
class AEComponente extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'AE_Componente';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['IdPostulante', 'NombreComponente', 'Orden', 'EsGestion'], 'required'],
            [['IdPostulante', 'Orden', 'EsGestion'], 'integer'],
            [['NombreComponente'], 'string'],
            [['IdPostulante'], 'exist', 'skipOnError' => true, 'targetClass' => Postulante::className(), 'targetAttribute' => ['IdPostulante' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IdComponente' => 'Id Componente',
            'IdPostulante' => 'Id Postulante',
            'NombreComponente' => 'Nombre Componente',
            'Orden' => 'Orden',
            'EsGestion' => 'Es Gestion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPostulante()
    {
        return $this->hasOne(Postulante::className(), ['ID' => 'IdPostulante']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAEComponenteValors()
    {
        return $this->hasMany(AEComponenteValor::className(), ['IdComponente' => 'IdComponente']);
    }
}
