<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "DireccionLinea".
 *
 * @property integer $ID
 * @property string $Nombre
 */
class DireccionLinea extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DireccionLinea';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nombre'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nombre' => 'Nombre',
        ];
    }
}
