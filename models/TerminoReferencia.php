<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "TerminoReferencia".
 *
 * @property integer $ID
 * @property string $Documento
 * @property integer $Estado
 * @property string $FechaRegistro
 * @property integer $Situacion
 * @property string $CodigoProyecto
 * @property integer $TipoServicio
 * @property double $MontoTotal
 * @property integer $Duracion
 * @property integer $AreSubCategoriaID
 * @property integer $PaDetalleID
 *
 * @property Orden[] $ordens
 */
class TerminoReferencia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $archivo;
    public $ComponenteID;
    public $ActividadID;
    //public $AreSubCategoriaID;
    public $TerminoReferenciaID;
    public static function tableName()
    {
        return 'TerminoReferencia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Documento', 'CodigoProyecto'], 'string'],
            [['Estado', 'Situacion', 'TipoServicio', 'Duracion', 'AreSubCategoriaID', 'PaDetalleID'], 'integer'],
            [['FechaRegistro','archivo','ComponenteID','ActividadID'], 'safe'],
            [['MontoTotal'], 'number'],
            [['archivo'],'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Documento' => 'Documento',
            'Estado' => 'Estado',
            'FechaRegistro' => 'Fecha Registro',
            'Situacion' => 'Situacion',
            'CodigoProyecto' => 'Codigo Proyecto',
            'TipoServicio' => 'Tipo Servicio',
            'MontoTotal' => 'Monto Total',
            'Duracion' => 'Duracion',
            'AreSubCategoriaID' => 'Are Sub Categoria ID',
            'PaDetalleID' => 'Pa Detalle ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdens()
    {
        return $this->hasMany(Orden::className(), ['TerminoReferenciaID' => 'ID']);
    }
}
