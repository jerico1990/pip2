<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Rendicion".
 *
 * @property integer $ID
 * @property integer $Correlativo
 * @property integer $TipoGastoID
 * @property integer $TransaccionID
 * @property string $FechaRegistro
 * @property integer $Situacion
 * @property integer $Estado
 */
class Rendicion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Rendicion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Correlativo', 'TipoGastoID', 'TransaccionID', 'Situacion', 'Estado'], 'integer'],
            [['FechaRegistro','CodigoProyecto'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Correlativo' => 'Correlativo',
            'TipoGastoID' => 'Tipo Gasto ID',
            'TransaccionID' => 'Transaccion ID',
            'FechaRegistro' => 'Fecha Registro',
            'Situacion' => 'Situacion',
            'Estado' => 'Estado',
        ];
    }
}
