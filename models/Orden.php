<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Orden".
 *
 * @property integer $ID
 * @property string $Correlativo
 * @property string $FechaOrden
 * @property integer $RequerimientoID
 * @property string $RazonSocial
 * @property string $Direccion
 * @property string $RUC
 * @property string $Telefono
 * @property double $Monto
 * @property double $TipoCambio
 * @property string $Concepto
 * @property string $PlazoEntrega
 * @property string $LugarEntrega
 * @property double $ValorVenta
 * @property double $IGV
 * @property string $UnidadEjecutoraNombres
 * @property string $UnidadEjecutoraDireccion
 * @property string $UnidadEjecutoraRUC
 * @property string $FechaConformidad
 * @property integer $TipoOrden
 * @property double $Retencion
 * @property integer $Estado
 * @property string $FechaRegistro
 * @property string $Relacion
 * @property string $CodigoProyecto
 * @property integer $TipoProceso
 * @property string $CondicionPago
 * @property string $Penalidad
 * @property string $CuentaBancaria
 * @property string $CCI
 *
 * @property Conformidad[] $conformidads
 * @property TerminoReferencia $requerimiento
 */
class Orden extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $Cantidades;
    public $UnidadesMedidas;
    public $Descripciones;
    public $PreciosUnitarios;
    public $Totales;
    public $Codigos;
    public $archivo;
    
    public $DetallesDescripciones;
    public $DetallesCantidades;
    public $DetallesPreciosUnitarios;
    public $DetallesCodigos;
    public $DetallesRecursos;
    public $DetallesObjetivos;
    public $DetallesActividades;
    public $DetallesRecurs;
    public $DetallesIDs;
    

    public $Fecha;
    public $CodigoMatriz;
    public $FechaProceso;

    public $Sigla;
    public $Cantidad;
    public $UnidadMedida;
    public $Descripcion;
    public $PrecioUnitario;
    public $Memorando;
    public $Siaf;
    public $Saldo;
    //public $Retencion4ta;
    // public $TipoOrden;
    
    public static function tableName()
    {
        return 'Orden';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['ID'], 'required'],
            [['ID', 'RequerimientoID', 'TipoOrden', 'Estado', 'TipoProceso','Correlativo','Retencion4ta'], 'integer'],
            [[ 'RazonSocial', 'Direccion', 'RUC', 'Telefono', 'Concepto',  'LugarEntrega', 'UnidadEjecutoraNombres', 'UnidadEjecutoraDireccion','PlazoServicio','CuentaDetraccion','ConformidadServicio','PlazoEntrega', 'UnidadEjecutoraRUC', 'Relacion', 'CodigoProyecto', 'CondicionPago', 'Penalidad', 'CuentaBancaria', 'CCI'], 'string'],
            [['PrecioUnitario','DetallesIDs','DetallesRecurs','DetallesActividades','DetallesObjetivos','DetallesRecursos','DetallesCodigos','DetallesPreciosUnitarios','DetallesCantidades','DetallesDescripciones','AreSubCategoriaID','archivo','FechaOrden', 'FechaConformidad', 'FechaRegistro','Cantidades','UnidadesMedidas','Descripciones','PreciosUnitarios','Totales','Codigos','Memorando'], 'safe'],
            [['Monto', 'TipoCambio', 'ValorVenta', 'IGV', 'Retencion'], 'number'],
            //[['RequerimientoID'], 'exist', 'skipOnError' => true, 'targetClass' => TerminoReferencia::className(), 'targetAttribute' => ['RequerimientoID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Correlativo' => 'Correlativo',
            'FechaOrden' => 'Fecha Orden',
            'RequerimientoID' => 'Requerimiento ID',
            'RazonSocial' => 'Razon Social',
            'Direccion' => 'Direccion',
            'RUC' => 'Ruc',
            'Telefono' => 'Telefono',
            'Monto' => 'Monto',
            'TipoCambio' => 'Tipo Cambio',
            'Concepto' => 'Concepto',
            'PlazoEntrega' => 'Plazo Entrega',
            'LugarEntrega' => 'Lugar Entrega',
            'ValorVenta' => 'Valor Venta',
            'IGV' => 'Igv',
            'UnidadEjecutoraNombres' => 'Unidad Ejecutora Nombres',
            'UnidadEjecutoraDireccion' => 'Unidad Ejecutora Direccion',
            'UnidadEjecutoraRUC' => 'Unidad Ejecutora Ruc',
            'FechaConformidad' => 'Fecha Conformidad',
            'TipoOrden' => 'Tipo Orden',
            'Retencion' => 'Retencion',
            'Estado' => 'Estado',
            'FechaRegistro' => 'Fecha Registro',
            'Relacion' => 'Relacion',
            'CodigoProyecto' => 'Codigo Proyecto',
            'TipoProceso' => 'Tipo Proceso',
            'CondicionPago' => 'Condicion Pago',
            'Penalidad' => 'Penalidad',
            'CuentaBancaria' => 'Cuenta Bancaria',
            'CCI' => 'Cci',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConformidads()
    {
        return $this->hasMany(Conformidad::className(), ['OrdenID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequerimiento()
    {
        return $this->hasOne(TerminoReferencia::className(), ['ID' => 'RequerimientoID']);
    }
}
