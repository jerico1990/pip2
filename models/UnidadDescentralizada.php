<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "UnidadDescentralizada".
 *
 * @property integer $ID
 * @property string $NombreUD
 * @property string $FechaCrea
 * @property integer $UsuarioCrea
 * @property string $FechaMod
 * @property integer $UsuarioMod
 * @property integer $Estado
 *
 * @property UnidadDesReg[] $unidadDesRegs
 */
class UnidadDescentralizada extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'UnidadDescentralizada';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NombreUD'], 'string'],
            [['FechaCrea', 'FechaMod'], 'safe'],
            [['UsuarioCrea', 'UsuarioMod', 'Estado'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'NombreUD' => 'Nombre Ud',
            'FechaCrea' => 'Fecha Crea',
            'UsuarioCrea' => 'Usuario Crea',
            'FechaMod' => 'Fecha Mod',
            'UsuarioMod' => 'Usuario Mod',
            'Estado' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnidadDesRegs()
    {
        return $this->hasMany(UnidadDesReg::className(), ['UDid' => 'ID']);
    }
}
