<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "EntidadParticipante".
 *
 * @property integer $ID
 * @property integer $InvestigadorID
 * @property string $RazonSocial
 * @property string $Siglas
 * @property string $Ruc
 * @property string $Regimen
 * @property string $Procedencia
 * @property integer $TipoInstitucionID
 * @property string $FechaCreacion
 * @property string $DomicilioFiscal
 * @property string $Telefono
 * @property string $Email
 * @property string $PaginaWeb
 * @property string $TipoInstitucionOtro
 * @property integer $Tipo
 * @property string $Extension_PadronProductores
 * @property string $Extension_ActaEntidadProponente
 * @property string $Extension_CartaCompromisoColaboradora
 * @property string $Articulo
 *
 * @property AntecedenteEntidad[] $antecedenteEntidads
 * @property Investigador $investigador
 * @property TipoInstitucion $tipoInstitucion
 * @property FondoRecibidoEntidad[] $fondoRecibidoEntidads
 * @property MiembroEquipoGestion[] $miembroEquipoGestions
 * @property RepresentanteLegal[] $representanteLegals
 */
class EntidadParticipante extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'EntidadParticipante';
    }

    /**
     * @inheritdoc
     */
    public $Nombre; 
    public $UsuarioID; 
    public $EntidadID; 


    public function rules()
    {
        return [
            // [['InvestigadorID', 'RazonSocial', 'Siglas', 'Ruc', 'Regimen', 'Procedencia', 'TipoInstitucionID', 'FechaCreacion', 'DomicilioFiscal', 'Telefono', 'Email', 'Tipo'], 'required'],
            [['ID','InvestigadorID', 'TipoInstitucionID', 'Tipo','Estado'], 'integer'],
            
            [['RazonSocial', 'Siglas', 'Ruc', 'Regimen', 'Procedencia', 'DomicilioFiscal', 'Telefono', 'Email', 'PaginaWeb', 'TipoInstitucionOtro', 'Extension_PadronProductores', 'Extension_ActaEntidadProponente', 'Extension_CartaCompromisoColaboradora', 'Articulo'], 'string'],
            [['FechaCreacion','AporteMonetario','AporteNoMonetario'], 'safe'],
            [['InvestigadorID'], 'exist', 'skipOnError' => true, 'targetClass' => Investigador::className(), 'targetAttribute' => ['InvestigadorID' => 'ID']],
            [['TipoInstitucionID'], 'exist', 'skipOnError' => true, 'targetClass' => TipoInstitucion::className(), 'targetAttribute' => ['TipoInstitucionID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'InvestigadorID' => 'Investigador ID',
            'RazonSocial' => 'Razon Social',
            'Siglas' => 'Siglas',
            'AporteNoMonetario' => 'AporteNoMonetario',
            'AporteMonetario' => 'AporteMonetario',
            'Ruc' => 'Ruc',
            'Regimen' => 'Regimen',
            'Procedencia' => 'Procedencia',
            'TipoInstitucionID' => 'Tipo Institucion ID',
            'FechaCreacion' => 'Fecha Creacion',
            'DomicilioFiscal' => 'Domicilio Fiscal',
            'Telefono' => 'Telefono',
            'Email' => 'Email',
            'PaginaWeb' => 'Pagina Web',
            'TipoInstitucionOtro' => 'Tipo Institucion Otro',
            'Tipo' => 'Tipo',
            'Extension_PadronProductores' => 'Extension  Padron Productores',
            'Extension_ActaEntidadProponente' => 'Extension  Acta Entidad Proponente',
            'Extension_CartaCompromisoColaboradora' => 'Extension  Carta Compromiso Colaboradora',
            'Articulo' => 'Articulo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAntecedenteEntidads()
    {
        return $this->hasMany(AntecedenteEntidad::className(), ['EntidadParticipanteID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvestigador()
    {
        return $this->hasOne(Investigador::className(), ['ID' => 'InvestigadorID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoInstitucion()
    {
        return $this->hasOne(TipoInstitucion::className(), ['ID' => 'TipoInstitucionID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFondoRecibidoEntidads()
    {
        return $this->hasMany(FondoRecibidoEntidad::className(), ['EntidadParticipanteID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMiembroEquipoGestions()
    {
        return $this->hasMany(MiembroEquipoGestion::className(), ['EntidadParticipanteID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepresentanteLegals()
    {
        return $this->hasMany(RepresentanteLegal::className(), ['EntidadParticipanteID' => 'ID']);
    }
}
