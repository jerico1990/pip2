<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "AE_Producto".
 *
 * @property integer $IdProducto
 * @property integer $IdPostulante
 * @property string $NombreProducto
 * @property string $UnidadMedida
 * @property integer $Orden
 *
 * @property Postulante $idPostulante
 * @property AEProductoValor[] $aEProductoValors
 */
class AEProducto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'AE_Producto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['IdPostulante', 'NombreProducto', 'UnidadMedida', 'Orden'], 'required'],
            [['IdPostulante', 'Orden'], 'integer'],
            [['NombreProducto', 'UnidadMedida'], 'string'],
            [['IdPostulante'], 'exist', 'skipOnError' => true, 'targetClass' => Postulante::className(), 'targetAttribute' => ['IdPostulante' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IdProducto' => 'Id Producto',
            'IdPostulante' => 'Id Postulante',
            'NombreProducto' => 'Nombre Producto',
            'UnidadMedida' => 'Unidad Medida',
            'Orden' => 'Orden',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPostulante()
    {
        return $this->hasOne(Postulante::className(), ['ID' => 'IdPostulante']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAEProductoValors()
    {
        return $this->hasMany(AEProductoValor::className(), ['IdProducto' => 'IdProducto']);
    }
}
