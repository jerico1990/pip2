<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;
/**
 * This is the model class for table "SolicitudCompromisoPresupuestal".
 *
 * @property integer $ID
 * @property string $Correlativo
 * @property string $JefeUnidad
 * @property string $NumeroResolucionJefatural
 * @property string $FechaSolicitud
 * @property integer $ProyectoID
 * @property double $MetaPresupuestal
 * @property string $Clasificacion
 * @property string $FechaRegistro
 * @property integer $Estado
 * @property string $Memorando
 * @property integer $PasoCriticoID
 *
 * @property DetalleSolicitudCompromisoPresupuestal[] $detalleSolicitudCompromisoPresupuestals
 */
class CompromisoPresupuestal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $Cantidades;
    public $RUCs;
    public $Montos;
    public $Objetivos;
    public $Actividades;
    public $Recursos;
    public $archivo;
    public $TiposGastos;
    public $Correlativos;
    public $Nombre;
    public $username;
    public $NombreGasto;
    public $ApellidoPaterno;
    public $ApellidoMaterno;
    public $RazonSocial;
    public $Monto;
    public $NombreID;
    public $Total;
    

    public $Siaf;
    public $Sigla;
    public $Entregable;
    public $NDocumento;
    public $AnnioEnt;
    public $TipoDetalleOrdenID;
    public $DocumentoCorrelativo;
    //public $RequerimientoID;
    public static function tableName()
    {
        return 'CompromisoPresupuestal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'JefeUnidad', 'NumeroResolucionJefatural', 'Clasificacion', 'Memorando','Codigo','Observacion','NotasCompromiso'], 'string'],
            [['RequerimientoID','archivo','FechaSolicitud', 'FechaRegistro','Cantidades','TiposGastos','Montos','Correlativos','Actividades','Recursos'], 'safe'],
            [['Correlativo','Estado'], 'integer'],
            [['MontoTotal'], 'number'],
            [['archivo'],'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Correlativo' => 'Correlativo',
            'JefeUnidad' => 'Jefe Unidad',
            'NumeroResolucionJefatural' => 'Numero Resolucion Jefatural',
            'FechaSolicitud' => 'Fecha Solicitud',
            'Codigo' => 'Proyecto ID',
            'MontoTotal' => 'MontolTotal',
            'Clasificacion' => 'Clasificacion',
            'FechaRegistro' => 'Fecha Registro',
            'Estado' => 'Estado',
            'Memorando' => 'Memorando',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetalleCompromisoPresupuestals()
    {
        return $this->hasMany(DetalleCompromisoPresupuestal::className(), ['CompromisoPresupuestalID' => 'ID']);
    }
    
    public function getGastoElegible($bandera=null)
    {
        $descripcion="";
        if($bandera==1)
        {
            $descripcion="SI";
        }
        elseif($bandera==0)
        {
            $descripcion="NO";
        }
        
        return $descripcion;
    }
    
    public function getSituacion($bandera=null)
    {
        $descripcion="";
        if($bandera==3)
        {
            $descripcion="Aprobado";
        }
        elseif($bandera==0)
        {
            $descripcion="Eliminado";
        }
        
        return $descripcion;
    }
    
}
