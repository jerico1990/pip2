<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "DetalleCajaChica".
 *
 * @property integer $ID
 * @property string $FechaRegistro
 * @property string $CodigoProyecto
 * @property integer $Correlativo
 * @property string $Descripcion
 * @property double $Bienes
 * @property double $Servicios
 * @property string $ResolucionDirectorial
 * @property integer $Situacion
 * @property integer $Estado
 * @property integer $RequerimientoID
 * @property string $Documento
 * @property string $Responsable
 * @property string $Banco
 * @property string $CTA
 * @property string $CCI
 * @property string $NResolucionDirectorial
 * @property integer $Tipo
 * @property integer $Annio
 * @property double $SaldoAnteriorBienes
 * @property double $SaldoAnteriorServicios
 * @property double $SaldoActualBienes
 * @property double $SaldoActualServicios
 * @property integer $CajaChicaID
 */
class DetalleCajaChica extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DetalleCajaChica';
    }

    /**
     * @inheritdoc
     */
    public $archivo;
    public function rules()
    {
        return [
            [['FechaRegistro','archivo'], 'safe'],
            [['CodigoProyecto', 'Descripcion', 'ResolucionDirectorial', 'Documento', 'Responsable', 'Banco', 'CTA', 'CCI', 'NResolucionDirectorial'], 'string'],
            [['Correlativo', 'Situacion', 'Estado', 'RequerimientoID', 'Tipo', 'Annio', 'CajaChicaID'], 'integer'],
            [['Bienes', 'Servicios', 'SaldoAnteriorBienes', 'SaldoAnteriorServicios', 'SaldoActualBienes', 'SaldoActualServicios'], 'number'],
            [['archivo'],'file']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'FechaRegistro' => 'Fecha Registro',
            'CodigoProyecto' => 'Codigo Proyecto',
            'Correlativo' => 'Correlativo',
            'Descripcion' => 'Descripcion',
            'Bienes' => 'Bienes',
            'Servicios' => 'Servicios',
            'ResolucionDirectorial' => 'Resolucion Directorial',
            'Situacion' => 'Situacion',
            'Estado' => 'Estado',
            'RequerimientoID' => 'Requerimiento ID',
            'Documento' => 'Documento',
            'Responsable' => 'Responsable',
            'Banco' => 'Banco',
            'CTA' => 'Cta',
            'CCI' => 'Cci',
            'NResolucionDirectorial' => 'Nresolucion Directorial',
            'Tipo' => 'Tipo',
            'Annio' => 'Annio',
            'SaldoAnteriorBienes' => 'Saldo Anterior Bienes',
            'SaldoAnteriorServicios' => 'Saldo Anterior Servicios',
            'SaldoActualBienes' => 'Saldo Actual Bienes',
            'SaldoActualServicios' => 'Saldo Actual Servicios',
            'CajaChicaID' => 'Caja Chica ID',
        ];
    }
}
