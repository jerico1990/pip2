<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "TareaObservacion".
 *
 * @property integer $ID
 * @property string $Observacion
 * @property integer $TareaID
 * @property integer $Estado
 */
class TareaObservacion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'TareaObservacion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Observacion'], 'string'],
            [['TareaID', 'Estado'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Observacion' => 'Observacion',
            'TareaID' => 'Tarea ID',
            'Estado' => 'Estado',
        ];
    }
}
