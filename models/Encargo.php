<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "RequerimientoEncargo".
 *
 * @property integer $ID
 * @property integer $Correlativo
 * @property string $CodigoProyecto
 * @property string $IRP
 * @property string $Cargo
 * @property string $DescripcionActividad
 * @property string $FechaInicio
 * @property string $FechaFin
 * @property double $Total
 * @property double $Bienes
 * @property double $Servicios
 * @property string $NombresEncargo
 * @property string $ApellidosEncargo
 * @property string $DNIEncargo
 * @property string $CargoEncargo
 * @property integer $AreSubCategoriaID
 * @property string $Documento
 * @property integer $Situacion
 * @property integer $Estado
 * @property string $FechaRegistro
 */
class Encargo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $archivo;
    public $ComponenteID;
    public $Componente;
    public $ActividadID;
    public $Actividad;
    //public $AreSubCategoriaID;
    public $AreSubCategoria;
    // public $TipoCambio;
    public static function tableName()
    {
        return 'Encargo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['ID'], 'required'],
            [['ID', 'Correlativo', 'AreSubCategoriaID', 'Situacion', 'Estado','RequerimientoID'], 'integer'],
            [['CodigoProyecto', 'IRP', 'Cargo', 'DescripcionActividad', 'NombresEncargo', 'ApellidosEncargo', 'DNIEncargo', 'CargoEncargo', 'Documento','NResolucionDirectorial'], 'string'],
            [['CodigoPoa','FechaInicio', 'FechaFin', 'FechaRegistro','archivo','ContratoEncargo','TipoCambio'], 'safe'],
            [['Total', 'Bienes', 'Servicios'], 'number'],
            [['archivo'],'file']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Correlativo' => 'Correlativo',
            'CodigoProyecto' => 'Codigo Proyecto',
            'IRP' => 'Irp',
            'Cargo' => 'Cargo',
            'DescripcionActividad' => 'Descripcion Actividad',
            'FechaInicio' => 'Fecha Inicio',
            'FechaFin' => 'Fecha Fin',
            'Total' => 'Total',
            'Bienes' => 'Bienes',
            'Servicios' => 'Servicios',
            'NombresEncargo' => 'Nombres Encargo',
            'ApellidosEncargo' => 'Apellidos Encargo',
            'DNIEncargo' => 'Dniencargo',
            'CargoEncargo' => 'Cargo Encargo',
            'AreSubCategoriaID' => 'Are Sub Categoria ID',
            'Documento' => 'Documento',
            'Situacion' => 'Situacion',
            'Estado' => 'Estado',
            'FechaRegistro' => 'Fecha Registro',
        ];
    }
}
