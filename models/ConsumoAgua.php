<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ConsumoAgua".
 *
 * @property integer $ID
 * @property string $Mes
 * @property double $Cantidad
 * @property integer $Tipo
 * @property string $FechaRegistro
 * @property integer $Anno
 * @property integer $InformeTecnicoFinancieroID
 */
class ConsumoAgua extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ConsumoAgua';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Mes'], 'string'],
            [['Cantidad'], 'number'],
            [['Tipo', 'Anno', 'InformeTecnicoFinancieroID'], 'integer'],
            [['FechaRegistro'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Mes' => 'Mes',
            'Cantidad' => 'Cantidad',
            'Tipo' => 'Tipo',
            'FechaRegistro' => 'Fecha Registro',
            'Anno' => 'Anno',
            'InformeTecnicoFinancieroID' => 'Informe Tecnico Financiero ID',
        ];
    }
}
