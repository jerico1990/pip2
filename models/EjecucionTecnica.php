<?php

namespace app\models;

use yii\base\Model;

/**
 * This is the model class for table "EjecucionTecnica".
 *
 * @property integer $IdEjecucionTecnica
 * @property integer $Correlativo
 * @property string $ResponsableTecnico
 * @property double $DuracionHoras
 * @property double $Latitud
 * @property double $Longitud
 * @property integer $BeneficiarioHombres
 * @property integer $BeneficiarioMujeres
 * @property string $Material
 * @property string $Metodo
 * @property string $Resultado
 * @property string $Conclusion
 * @property string $ObservacionUafsi
 * @property string $ObservacionInvestigador
 * @property double $Costo
 * @property integer $Estado
 * @property string $FechaRegistro
 *
 */
class EjecucionTecnica extends Model
{
    public static $types = array("Operation", "Task", "Role");
    public $IdEjecucionTecnica;
    public $Correlativo;
    public $ResponsableTecnico;
    public $DuracionHoras;
    public $Latitud;
    public $Longitud;
    public $BeneficiarioHombres;
    public $BeneficiarioMujeres;
    public $Material;
    public $Metodo;
    public $Resultado;
    public $Conclusion;
    public $ObservacionUafsi;
    public $ObservacionInvestigador;
    public $Costo;
    public $Estado;
    public $FechaRegistro;
    // Campos aumentados 
    public $IdProgramacionTecnica;
    public $IntroduccionDescripcion;
    public $LugarDesarrollo;
    public $FechaInicio;
    public $FechaFin;    
    public $ActividadID;
    public $Titulo;
    public $Tipologia;
    public $Objetivo;
    public $CodigoProyecto;
    public $RevistaPublicacion;
    public $RequiereAsesoramiento;
    public $UniversidadNombre;
    public $ObservacionProgramacion;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['IdEjecucionTecnica', 'Correlativo', 'BeneficiarioHombres', 'BeneficiarioMujeres', 'Estado', 'IdProgramacionTecnica', 'ActividadID', 'Tipologia' ], 'integer'],
            [['ResponsableTecnico', 'Material', 'Metodo', 'Resultado', 'Conclusion', 'ObservacionUafsi', 'ObservacionInvestigador', 'IntroduccionDescripcion', 'LugarDesarrollo', 'FechaInicio', 'FechaFin', 'Titulo', 'CodigoProyecto', 'RevistaPublicacion', 'RequiereAsesoramiento', 'UniversidadNombre', 'ObservacionProgramacion' ], 'string'],
            [['DuracionHoras', 'Latitud', 'Longitud', 'Costo', 'Objetivo' ], 'number'],
            [['FechaRegistro', 'IntroduccionDescripcion', 'LugarDesarrollo', 'FechaInicio', 'FechaFin', 'Objetivo', 'ActividadID', 'Titulo', 'Tipologia', 'CodigoProyecto', 'ObservacionProgramacion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IdEjecucionTecnica' => 'Id Ejecucion Tecnica',
            'Correlativo' => 'Correlativo',
            'ResponsableTecnico' => 'Responsable Tecnico',
            'DuracionHoras' => 'Duracion Horas',
            'Latitud' => 'Latitud',
            'Longitud' => 'Longitud',
            'BeneficiarioHombres' => 'Beneficiario Hombres',
            'BeneficiarioMujeres' => 'Beneficiario Mujeres',
            'Material' => 'Material',
            'Metodo' => 'Metodo',
            'Resultado' => 'Resultado',
            'Conclusion' => 'Conclusion',
            'ObservacionUafsi' => 'Observacion Uafsi',
            'ObservacionInvestigador' => 'Observacion Investigador',
            'Costo' => 'Costo',
            'Estado' => 'Estado',
            'FechaRegistro' => 'Fecha Registro',
            
            'IdProgramacionTecnica' => 'Id Programacion Tecnica',
            'IntroduccionDescripcion' => 'Introduccion Descripcion',
            'LugarDesarrollo' => 'Lugar Desarrollo',
            'FechaInicio' => 'Fecha Inicio',
            'FechaFin' => 'Fecha Fin',
            'LugarDesarrollo' => 'Lugar Desarrollo',
            'Objetivo' => 'Objetivo',
            'ActividadID' => 'ActividadID',
            'Tipologia' => 'Tipologia',
            'CodigoProyecto' => 'Código Proyecto',
            'RevistaPublicacion' => 'Revista Publicacion',
            'RequiereAsesoramiento' => 'Requiere Asesoramiento',
            'UniversidadNombre' => 'UniversidadNombre',
            'ObservacionProgramacion' => 'ObservacionProgramacion'
        ];
    }

}
