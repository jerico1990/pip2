<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "CajaChica".
 *
 * @property integer $ID
 * @property string $FechaRegistro
 * @property string $CodigoProyecto
 * @property integer $Correlativo
 * @property string $Descripcion
 * @property double $Bienes
 * @property double $Servicios
 * @property string $ResolucionDirectorial
 * @property integer $Situacion
 * @property integer $Estado
 * @property integer $RequerimientoID
 * @property string $Documento
 * @property string $Responsable
 * @property string $Banco
 * @property string $CTA
 * @property string $CCI
 * @property string $NResolucion Directorial
 *
 * @property Requerimiento $requerimiento
 */
class CajaChica extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CajaChica';
    }
    public $archivo;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FechaRegistro','archivo'], 'safe'],
            [['CodigoProyecto', 'Descripcion', 'ResolucionDirectorial', 'Documento', 'Responsable', 'Banco', 'CTA', 'CCI', 'NResolucionDirectorial','Dni'], 'string'],
            [['Correlativo', 'Situacion', 'Estado', 'RequerimientoID','Tipo'], 'integer'],
            [['Bienes', 'Servicios'], 'number'],
            [['RequerimientoID'], 'exist', 'skipOnError' => true, 'targetClass' => Requerimiento::className(), 'targetAttribute' => ['RequerimientoID' => 'ID']],
            [['archivo'],'file']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'FechaRegistro' => 'Fecha Registro',
            'CodigoProyecto' => 'Codigo Proyecto',
            'Correlativo' => 'Correlativo',
            'Descripcion' => 'Descripcion',
            'Bienes' => 'Bienes',
            'Servicios' => 'Servicios',
            'ResolucionDirectorial' => 'Resolucion Directorial',
            'Situacion' => 'Situacion',
            'Estado' => 'Estado',
            'RequerimientoID' => 'Requerimiento ID',
            'Documento' => 'Documento',
            'Responsable' => 'Responsable',
            'Banco' => 'Banco',
            'CTA' => 'Cta',
            'CCI' => 'Cci',
            'NResolucion Directorial' => 'Nresolucion  Directorial',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequerimiento()
    {
        return $this->hasOne(Requerimiento::className(), ['ID' => 'RequerimientoID']);
    }
}
