<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "CronogramaActividad".
 *
 * @property integer $ID
 * @property integer $ActividadID
 * @property integer $Mes
 * @property string $MetaFisica
 * @property string $MetaFinanciera
 * @property string $PoafMetaFinanciera
 * @property string $PoafMetaFisica
 *
 * @property Actividad $actividad
 */
class CronogramaActividad extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CronogramaActividad';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['ActividadID', 'Mes', 'MetaFisica', 'MetaFinanciera'], 'required'],
            [['ActividadID', 'Mes'], 'integer'],
            [['MetaFisica', 'MetaFinanciera', 'PoafMetaFinanciera', 'PoafMetaFisica'], 'number'],
            [['ActividadID'], 'exist', 'skipOnError' => true, 'targetClass' => Actividad::className(), 'targetAttribute' => ['ActividadID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ActividadID' => 'Actividad ID',
            'Mes' => 'Mes',
            'MetaFisica' => 'Meta Fisica',
            'MetaFinanciera' => 'Meta Financiera',
            'PoafMetaFinanciera' => 'Poaf Meta Financiera',
            'PoafMetaFisica' => 'Poaf Meta Fisica',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActividad()
    {
        return $this->hasOne(Actividad::className(), ['ID' => 'ActividadID']);
    }
}
