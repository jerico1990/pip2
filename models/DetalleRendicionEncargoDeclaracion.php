<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "DetalleRendicionEncargoDeclaracion".
 *
 * @property integer $ID
 * @property integer $RendicionEncargoID
 * @property string $Fecha
 * @property string $Detalle
 * @property double $Importe
 */
class DetalleRendicionEncargoDeclaracion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DetalleRendicionEncargoDeclaracion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['RendicionEncargoID'], 'integer'],
            [['Fecha'], 'safe'],
            [['Detalle'], 'string'],
            [['Importe'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'RendicionEncargoID' => 'Rendicion Encargo ID',
            'Fecha' => 'Fecha',
            'Detalle' => 'Detalle',
            'Importe' => 'Importe',
        ];
    }
}
