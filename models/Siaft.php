<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Siaft".
 *
 * @property integer $ID
 * @property string $Fase
 * @property integer $Secuencial
 * @property string $Fecha
 * @property double $Monto
 * @property string $FechaProceso
 * @property integer $Estado
 * @property string $CodigoProyecto
 * @property string $Orden
 * @property string $Moneda
 * @property string $Siaf
 * @property integer $TipoGastoID
 */
class Siaft extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Siaft';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CodigoProyecto', 'Orden', 'Moneda', 'Siaf','FuenteFinanciamiento','InterfaceSecuencial'], 'string'],
            [['Secuencial', 'Estado', 'TipoGastoID'], 'integer'],
            [['Fecha', 'FechaProceso','Fase'], 'safe'],
            [['Monto'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Fase' => 'Fase',
            'Secuencial' => 'Secuencial',
            'Fecha' => 'Fecha',
            'Monto' => 'Monto',
            'FechaProceso' => 'Fecha Proceso',
            'Estado' => 'Estado',
            'CodigoProyecto' => 'Codigo Proyecto',
            'Orden' => 'Orden',
            'Moneda' => 'Moneda',
            'Siaf' => 'Siaf',
            'TipoGastoID' => 'Tipo Gasto ID',
        ];
    }
}
