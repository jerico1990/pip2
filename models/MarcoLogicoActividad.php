<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "MarcoLogicoActividad".
 *
 * @property integer $ID
 * @property integer $ActividadID
 * @property string $IndicadorMedioVerificacion
 * @property string $IndicadorSupuestos
 * @property integer $Estado
 * @property integer $IndicadorUnidadMedida
 * @property string $IndicadorMetaFisica
 *
 * @property Actividad $actividad
 */
class MarcoLogicoActividad extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $ActividadNombre;
    public static function tableName()
    {
        return 'MarcoLogicoActividad';
    }
    
    public $Nombre;
    public $ComponenteID;
    public $Meses;
    public $Peso;
    public $CostoUnitario;
    public $ActividadPeso;
    public $ActividadCorrelativo;
    public $ComponenteCO;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ActividadID'], 'required'],
            [['ActividadID', 'Estado', 'ComponenteID','Meses','Peso','IndicadorPniaID'], 'integer'],
            [['IndicadorMedioVerificacion', 'IndicadorSupuestos','IndicadorUnidadMedida','IndicadorMetaFisica','Nombre','IndicadorDescripcion'], 'string'],
            [['CostoUnitario'],'safe'],
            [['ActividadID'], 'exist', 'skipOnError' => true, 'targetClass' => Actividad::className(), 'targetAttribute' => ['ActividadID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ActividadID' => 'Actividad ID',
            'IndicadorMedioVerificacion' => 'Indicador Medio Verificacion',
            'IndicadorSupuestos' => 'Indicador Supuestos',
            'Estado' => 'Estado',
            'IndicadorUnidadMedida' => 'Indicador Unidad Medida',
            'IndicadorMetaFisica' => 'Indicador Meta Fisica',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActividad()
    {
        return $this->hasOne(Actividad::className(), ['ID' => 'ActividadID']);
    }
}
