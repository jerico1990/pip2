<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Inbox".
 *
 * @property integer $ID
 * @property integer $UsuarioEnvia
 * @property integer $UsuarioRecibe
 * @property string $Observacion
 * @property integer $Estado
 */
class Inbox extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Inbox';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['UsuarioEnvia', 'UsuarioRecibe', 'Estado','Situacion'], 'integer'],
            [['Observacion'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'UsuarioEnvia' => 'Usuario Envia',
            'UsuarioRecibe' => 'Usuario Recibe',
            'Observacion' => 'Observacion',
            'Estado' => 'Estado',
        ];
    }
    public function getEtapa($etapa=null)
    {
        $descripcion='';
        if($etapa==1)
        {
            $descripcion='Evaluación POA';
        }
        return $descripcion;
    }
    
    public function getSituacion($situacion=null)
    {
        $descripcion='';
        if($situacion==0)
        {
            $descripcion='Enviar';
        }
        elseif($situacion==1)
        {
            $descripcion='Aprobado';
        }
        elseif($situacion==2)
        {
            $descripcion='Pendiente';
        }
        elseif($situacion==3)
        {
            $descripcion='Observado';
        }
        return $descripcion;
    }
    
    public function getAcciones($etapa,$situacion)
    {
        $descripcion='';
        if($etapa==1 && $situacion==2)
        {
            $descripcion='<button class="enviar_evaluacion_poa">Enviar evaluación</button>';
        }
        return $descripcion;
    }
}
