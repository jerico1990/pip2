<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "DetalleSolicitudPago".
 *
 * @property integer $ID
 * @property integer $SolicitudPagoID
 * @property string $FechaRegistro
 * @property string $Proveedor
 * @property string $RUC
 * @property double $Monto
 * @property integer $AreSubCategoriaID
 * @property string $TerminoReferenciaDocumento
 * @property string $OrdenDocumento
 * @property string $InformeDocumento
 * @property string $ReciboDocumento
 * @property string $Suspension4taDocumento
 * @property string $ValidezRHDocumento
 * @property string $ConformidadServicioDocumento
 *
 * @property SolicitudPago $solicitudPago
 */
class DetalleSolicitudPago extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DetalleSolicitudPago';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'required'],
            [['ID', 'SolicitudPagoID', 'AreSubCategoriaID'], 'integer'],
            [['FechaRegistro'], 'safe'],
            [['Proveedor', 'RUC', 'TerminoReferenciaDocumento', 'OrdenDocumento', 'InformeDocumento', 'ReciboDocumento', 'Suspension4taDocumento', 'ValidezRHDocumento', 'ConformidadServicioDocumento'], 'string'],
            [['Monto'], 'number'],
            [['SolicitudPagoID'], 'exist', 'skipOnError' => true, 'targetClass' => SolicitudPago::className(), 'targetAttribute' => ['SolicitudPagoID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'SolicitudPagoID' => 'Solicitud Pago ID',
            'FechaRegistro' => 'Fecha Registro',
            'Proveedor' => 'Proveedor',
            'RUC' => 'Ruc',
            'Monto' => 'Monto',
            'AreSubCategoriaID' => 'Are Sub Categoria ID',
            'TerminoReferenciaDocumento' => 'Termino Referencia Documento',
            'OrdenDocumento' => 'Orden Documento',
            'InformeDocumento' => 'Informe Documento',
            'ReciboDocumento' => 'Recibo Documento',
            'Suspension4taDocumento' => 'Suspension4ta Documento',
            'ValidezRHDocumento' => 'Validez Rhdocumento',
            'ConformidadServicioDocumento' => 'Conformidad Servicio Documento',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSolicitudPago()
    {
        return $this->hasOne(SolicitudPago::className(), ['ID' => 'SolicitudPagoID']);
    }
}
