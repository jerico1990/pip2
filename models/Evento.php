<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Evento".
 *
 * @property integer $ID
 * @property integer $ActividadID
 * @property string $CodigoProyecto
 * @property string $InvestigadorResponsable
 * @property string $EEAA
 * @property string $Titulo
 * @property string $ResponsableExperimento
 * @property integer $Tipologia
 * @property integer $PasoCriticoID
 * @property double $Duracion
 * @property double $Latitud
 * @property double $Longitud
 * @property integer $CentroPobladoID
 * @property integer $BeneficiarioHombres
 * @property integer $BeneficiarioMujeres
 * @property string $Introduccion
 * @property integer $MaterialID
 * @property string $Metodo
 * @property string $Resultado
 * @property string $Conclusion
 * @property double $Costo
 * @property string $Documento
 * @property string $Nota
 * @property string $FechaRegistro
 * @property integer $Estado
 * @property integer $Situacion
 */
class Evento extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Evento';
    }

    /**
     * @inheritdoc
     */
    public $Archivo;
    public $ComponenteID;
    public function rules()
    {
        return [
            [['ActividadID', 'Tipologia', 'PasoCriticoID', 'CentroPobladoID', 'BeneficiarioHombres', 'BeneficiarioMujeres',  'Estado', 'Situacion'], 'integer'],
            [['CodigoProyecto', 'InvestigadorResponsable', 'EEAA', 'Titulo', 'ResponsableEvento', 'Introduccion', 'Metodo', 'Material' ,'Resultado', 'Conclusion', 'Documento', 'Nota'], 'string'],
            [['Duracion', 'Latitud', 'Longitud'], 'number'],
            [['FechaRegistro','Archivo','Costo'], 'safe'],
            [['Archivo'],'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ActividadID' => 'Actividad ID',
            'CodigoProyecto' => 'Codigo Proyecto',
            'InvestigadorResponsable' => 'Investigador Responsable',
            'EEAA' => 'Eeaa',
            'Titulo' => 'Titulo',
            'ResponsableEvento' => 'Responsable Experimento',
            'Tipologia' => 'Tipologia',
            'PasoCriticoID' => 'Paso Critico ID',
            'Duracion' => 'Duracion',
            'Latitud' => 'Latitud',
            'Longitud' => 'Longitud',
            'CentroPobladoID' => 'Centro Poblado ID',
            'BeneficiarioHombres' => 'Beneficiario Hombres',
            'BeneficiarioMujeres' => 'Beneficiario Mujeres',
            'Introduccion' => 'Introduccion',
            'Metodo' => 'Metodo',
            'Resultado' => 'Resultado',
            'Conclusion' => 'Conclusion',
            'Costo' => 'Costo',
            'Documento' => 'Documento',
            'Nota' => 'Nota',
            'FechaRegistro' => 'Fecha Registro',
            'Estado' => 'Estado',
            'Situacion' => 'Situacion',
        ];
    }
}
