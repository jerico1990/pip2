<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "GastoIft".
 *
 * @property integer $ID
 * @property string $FECH_EMIS_COP
 * @property string $FECH_PAGO_COP
 * @property string $NUME_COMP_COP
 * @property integer $ANNO_EJEC_EJE
 * @property string $FECH_DOCU_DOC
 * @property string $ABRE_DOCU_TPD
 * @property string $SERI_DOCU_TPD
 * @property string $NUME_DOCU_DOC
 * @property string $NUM_RUC_CAB
 * @property string $DESC_ANEX_ANX
 * @property string $TEXT_REFE_CAB
 * @property double $MNTO_NACI_CAB
 * @property integer $ESTADO
 * @property string $TIPO
 */
class GastoItf extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'GastoItf';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['FECH_EMIS_COP', 'FECH_PAGO_COP', 'FECH_DOCU_DOC'], 'safe'],
            [['NUME_COMP_COP', 'ABRE_DOCU_TPD', 'SERI_DOCU_TPD', 'NUME_DOCU_DOC', 'NUM_RUC_CAB', 'DESC_ANEX_ANX', 'TEXT_REFE_CAB', 'TIPO'], 'string'],
            [['ANNO_EJEC_EJE', 'ESTADO'], 'integer'],
            [['MNTO_NACI_CAB'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'FECH_EMIS_COP' => 'Fech  Emis  Cop',
            'FECH_PAGO_COP' => 'Fech  Pago  Cop',
            'NUME_COMP_COP' => 'Nume  Comp  Cop',
            'ANNO_EJEC_EJE' => 'Anno  Ejec  Eje',
            'FECH_DOCU_DOC' => 'Fech  Docu  Doc',
            'ABRE_DOCU_TPD' => 'Abre  Docu  Tpd',
            'SERI_DOCU_TPD' => 'Seri  Docu  Tpd',
            'NUME_DOCU_DOC' => 'Nume  Docu  Doc',
            'NUM_RUC_CAB' => 'Num  Ruc  Cab',
            'DESC_ANEX_ANX' => 'Desc  Anex  Anx',
            'TEXT_REFE_CAB' => 'Text  Refe  Cab',
            'MNTO_NACI_CAB' => 'Mnto  Naci  Cab',
            'ESTADO' => 'Estado',
            'TIPO' => 'Tipo',
        ];
    }
}
