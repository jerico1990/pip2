<?php

namespace app\models;

use yii\base\Model;

/**
 * This is the model class for table "EjecucionTecnica".
 *
 * @property integer $IndicadorId
 * @property integer $ProyectoId
 * @property string $Nombre
 * @property integer $Programado
 * @property integer $Ejecutado
 * @property string $Descripcion
 * @property integer $Probabilidad
 * @property string $Observacion
 * @property integer $Estado
 *
 */
class EjecucionIndicador extends Model
{
    public static $types = array("Operation", "Task", "Role");
    public $IndicadorId;
    public $ProyectoId;
    public $Nombre;
    public $Programado;
    public $Ejecutado;
    public $Descripcion;
    public $Probabilidad;
    public $Observacion;
    public $Estado;
    public $CodigoProyecto;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['IndicadorId', 'ProyectoId', 'Programado', 'Ejecutado', 'Probabilidad', 'Estado'], 'integer'],
            [['Nombre', 'Descripcion', 'Observacion', 'CodigoProyecto' ], 'string'],
            [['Programado', 'Ejecutado', 'Estado', 'ProyectoId', 'Nombre'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IndicadorId' => 'Id Indicador',
            'Nombre' => 'Nombre',
            'Programado' => 'Programado',
            'Ejecutado' => 'Ejecutado',
            'Descripcion' => 'Descripcion',
            'Probabilidad' => 'Probabilidad',
            'Observacion' => 'Observacion',
            'Estado' => 'Estado',
            'ProyectoId' => 'ProyectoId',
            'CodigoProyecto' => 'Codigo de Proyecto'
        ];
    }

}
