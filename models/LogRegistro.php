<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "LogRegistro".
 *
 * @property integer $ID
 * @property integer $UserID
 * @property integer $RowAffectedID
 * @property string $Action
 * @property string $TableAffected
 * @property string $Comment
 * @property string $RegistryDate
 */
class LogRegistro extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'LogRegistro';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['RowAffectedID'], 'integer'],
            [['UserID','Action', 'TableAffected', 'Comment'], 'string'],
            [['RegistryDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'UserID' => 'User ID',
            'RowAffectedID' => 'Row Affected ID',
            'Action' => 'Action',
            'TableAffected' => 'Table Affected',
            'Comment' => 'Comment',
            'RegistryDate' => 'Registry Date',
        ];
    }
}
