<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "UnidadDesReg".
 *
 * @property integer $ID
 * @property string $CodReg
 * @property string $NombreReg
 * @property string $NombreSede
 * @property string $Direccion
 * @property integer $UDid
 * @property string $FechaCrea
 * @property integer $UsuarioCrea
 * @property string $FechaMod
 * @property integer $UsuarioMod
 * @property integer $Estado
 *
 * @property UnidadDescentralizada $uD
 */
class UnidadDesReg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'UnidadDesReg';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CodReg', 'NombreReg', 'NombreSede', 'Direccion'], 'string'],
            [['UDid', 'UsuarioCrea', 'UsuarioMod', 'Estado'], 'integer'],
            [['FechaCrea', 'FechaMod'], 'safe'],
            [['UDid'], 'exist', 'skipOnError' => true, 'targetClass' => UnidadDescentralizada::className(), 'targetAttribute' => ['UDid' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'CodReg' => 'Cod Reg',
            'NombreReg' => 'Nombre Reg',
            'NombreSede' => 'Nombre Sede',
            'Direccion' => 'Direccion',
            'UDid' => 'Udid',
            'FechaCrea' => 'Fecha Crea',
            'UsuarioCrea' => 'Usuario Crea',
            'FechaMod' => 'Fecha Mod',
            'UsuarioMod' => 'Usuario Mod',
            'Estado' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUD()
    {
        return $this->hasOne(UnidadDescentralizada::className(), ['ID' => 'UDid']);
    }
}
