<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "DetalleRendicionCajaChica".
 *
 * @property integer $ID
 * @property integer $RendicionCajaChicaID
 * @property string $FechaDocumento
 * @property integer $ClaseDocumento
 * @property string $NumeroDocumento
 * @property string $Proveedor
 * @property string $DetalleGasto
 * @property double $Importe
 */
class DetalleRendicionCajaChica extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DetalleRendicionCajaChica';
    }

    /**
     * @inheritdoc
     */
    public $Bienes;
    public $Servicios;
    public function rules()
    {
        return [
            [['RendicionCajaChicaID'], 'integer'],
            [['FechaDocumento'], 'safe'],
            [['NumeroDocumento', 'ClaseDocumento', 'Proveedor', 'DetalleGasto'], 'string'],
            [['Importe','Bienes','Servicios'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'RendicionCajaChicaID' => 'Rendicion Caja Chica ID',
            'FechaDocumento' => 'Fecha Documento',
            'ClaseDocumento' => 'Clase Documento',
            'NumeroDocumento' => 'Numero Documento',
            'Proveedor' => 'Proveedor',
            'DetalleGasto' => 'Detalle Gasto',
            'Importe' => 'Importe',
        ];
    }
}
