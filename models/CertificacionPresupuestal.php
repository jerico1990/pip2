<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "CertificacionPresupuestal".
 *
 * @property integer $ID
 * @property string $Memorando
 * @property string $JefePnia
 * @property integer $PasoCriticoID
 * @property string $Asunto
 * @property string $Referencia
 * @property string $Descripcion
 * @property string $MetaPresupuestal
 * @property string $Clasificacion
 * @property string $Annio
 * @property integer $Situacion
 * @property integer $Estado
 * @property string $FechaRegistro
 * @property string $Documento
 */
class CertificacionPresupuestal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $Codigos;
    public $Montos;
    public $archivo;
    public $IDs;
    public $Certificaciones;
    public static function tableName()
    {
        return 'CertificacionPresupuestal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Memorando', 'JefePnia', 'Asunto', 'Referencia', 'Descripcion', 'MetaPresupuestal', 'Clasificacion', 'Annio', 'Documento'], 'string'],
            [['PasoCriticoID', 'Situacion', 'Estado'], 'integer'],
            [['FechaRegistro','Codigos','Montos','archivo','IDs','Certificaciones'], 'safe'],
            [['archivo'],'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Memorando' => 'Memorando',
            'JefePnia' => 'Jefe Pnia',
            'PasoCriticoID' => 'Paso Critico ID',
            'Asunto' => 'Asunto',
            'Referencia' => 'Referencia',
            'Descripcion' => 'Descripcion',
            'MetaPresupuestal' => 'Meta Presupuestal',
            'Clasificacion' => 'Clasificacion',
            'Annio' => 'Annio',
            'Situacion' => 'Situacion',
            'Estado' => 'Estado',
            'FechaRegistro' => 'Fecha Registro',
            'Documento' => 'Documento',
        ];
    }
}
