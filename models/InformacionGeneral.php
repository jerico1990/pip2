<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "InformacionGeneral".
 *
 * @property integer $ID
 * @property integer $InvestigadorID
 * @property string $TituloProyecto
 * @property integer $ProgramaTransversalID
 * @property string $DistritoID
 * @property string $FechaInicioProyecto
 * @property string $FechaFinProyecto
 * @property integer $Meses
 * @property integer $DireccionLineaID
 * @property integer $TipoInvestigacionID
 * @property integer $UnidadOperativaID
 * @property integer $EspecieID
 *
 * @property AmbitoIntervencion[] $ambitoIntervencions
 * @property DireccionLinea $direccionLinea
 * @property Especie $especie
 * @property Ubigeo $distrito
 * @property ProgramaTransversal $programaTransversal
 * @property TipoInvestigacion $tipoInvestigacion
 * @property UnidadOperativa $unidadOperativa
 */
class InformacionGeneral extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    
    public static function tableName()
    {
        return 'InformacionGeneral';
    }

    /**
     * @inheritdoc
     */
    public $Archivos;
    public $ProyectoID;
    public $ActividadID;
    public $ComponenteID;
    public $UnidadEjecutoraID;
    public $ProgramaID;
    public $CultivoCrianzaID;
    public $DepartamentoNombre;
    public $DepartamentoID;
    public $ProvinciaID;
    public $Proposito;
    public $Fin;
    public $EnviarProyecto;
    public $CodigoProyecto;
    public $Situacion;
    public $Nombre;
    public $Cantidad;
    public function rules()
    {
        return [
            //[['InvestigadorID', 'TituloProyecto', 'ProgramaTransversalID', 'DistritoID', 'FechaInicioProyecto', 'FechaFinProyecto', 'Meses', 'DireccionLineaID', 'TipoInvestigacionID', 'UnidadOperativaID', 'EspecieID'], 'required'],
            [['InvestigadorID', 'ProgramaTransversalID', 'Meses', 'DireccionLineaID', 'TipoInvestigacionID', 'UnidadOperativaID', 'EspecieID'], 'integer'],
            [['TituloProyecto', 'DistritoID','Resumen','EnviarProyecto','CodigoProyecto','DepartamentoNombre','Antecedente','Longitud','Latitud'], 'string'],
            [['Archivos','Fin','Proposito','FechaInicioProyecto', 'FechaFinProyecto','CultivoCrianzaID'], 'safe'],
            //[['DireccionLineaID'], 'exist', 'skipOnError' => true, 'targetClass' => DireccionLinea::className(), 'targetAttribute' => ['DireccionLineaID' => 'ID']],
            //[['EspecieID'], 'exist', 'skipOnError' => true, 'targetClass' => Especie::className(), 'targetAttribute' => ['EspecieID' => 'ID']],
            //[['DistritoID'], 'exist', 'skipOnError' => true, 'targetClass' => Ubigeo::className(), 'targetAttribute' => ['DistritoID' => 'ID']],
            //[['ProgramaTransversalID'], 'exist', 'skipOnError' => true, 'targetClass' => ProgramaTransversal::className(), 'targetAttribute' => ['ProgramaTransversalID' => 'ID']],
            //[['TipoInvestigacionID'], 'exist', 'skipOnError' => true, 'targetClass' => TipoInvestigacion::className(), 'targetAttribute' => ['TipoInvestigacionID' => 'ID']],
            //[['UnidadOperativaID'], 'exist', 'skipOnError' => true, 'targetClass' => UnidadOperativa::className(), 'targetAttribute' => ['UnidadOperativaID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'InvestigadorID' => 'Investigador ID',
            'TituloProyecto' => 'Titulo Proyecto',
            'ProgramaTransversalID' => 'Programa Transversal ID',
            'DistritoID' => 'Distrito ID',
            'FechaInicioProyecto' => 'Fecha Inicio Proyecto',
            'FechaFinProyecto' => 'Fecha Fin Proyecto',
            'Meses' => 'Meses',
            'DireccionLineaID' => 'Direccion Linea ID',
            'TipoInvestigacionID' => 'Tipo Investigacion ID',
            'UnidadOperativaID' => 'Unidad Operativa ID',
            'EspecieID' => 'Especie ID',
        ];
    }
/*
    public function getAmbitoIntervencions()
    {
        return $this->hasMany(AmbitoIntervencion::className(), ['InformacionGeneralID' => 'ID']);
    }

    public function getDireccionLinea()
    {
        return $this->hasOne(DireccionLinea::className(), ['ID' => 'DireccionLineaID']);
    }

    public function getEspecie()
    {
        return $this->hasOne(Especie::className(), ['ID' => 'EspecieID']);
    }

    public function getDistrito()
    {
        return $this->hasOne(Ubigeo::className(), ['ID' => 'DistritoID']);
    }

    public function getProgramaTransversal()
    {
        return $this->hasOne(ProgramaTransversal::className(), ['ID' => 'ProgramaTransversalID']);
    }

    public function getTipoInvestigacion()
    {
        return $this->hasOne(TipoInvestigacion::className(), ['ID' => 'TipoInvestigacionID']);
    }

    public function getUnidadOperativa()
    {
        return $this->hasOne(UnidadOperativa::className(), ['ID' => 'UnidadOperativaID']);
    }*/

    
    public function getHabilitar()
    {
        if(\Yii::$app->user->identity->rol==7){
            return true;
        }
        else{
            return false;
        }
    }
}
