<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "AperturaCajaChica".
 *
 * @property integer $ID
 * @property string $CodigoProyecto
 * @property string $Anio
 * @property string $NResolucionDirectorial
 * @property string $ResolucionDirectorial
 * @property string $Responsable
 * @property string $DNI
 * @property string $Banco
 * @property string $CCI
 * @property string $CTA
 * @property double $Bienes
 * @property double $Servicios
 * @property integer $Situacion
 * @property string $FechaRegistro
 * @property integer $Estado
 * @property integer $Correlativo
 */
class AperturaCajaChica extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'AperturaCajaChica';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CodigoProyecto', 'Anio', 'NResolucionDirectorial', 'ResolucionDirectorial', 'Responsable', 'DNI', 'Banco', 'CCI', 'CTA'], 'string'],
            [['Bienes', 'Servicios'], 'number'],
            [['Situacion', 'Estado', 'Correlativo'], 'integer'],
            [['FechaRegistro'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'CodigoProyecto' => 'Codigo Proyecto',
            'Anio' => 'Anio',
            'NResolucionDirectorial' => 'Nresolucion Directorial',
            'ResolucionDirectorial' => 'Resolucion Directorial',
            'Responsable' => 'Responsable',
            'DNI' => 'Dni',
            'Banco' => 'Banco',
            'CCI' => 'Cci',
            'CTA' => 'Cta',
            'Bienes' => 'Bienes',
            'Servicios' => 'Servicios',
            'Situacion' => 'Situacion',
            'FechaRegistro' => 'Fecha Registro',
            'Estado' => 'Estado',
            'Correlativo' => 'Correlativo',
        ];
    }
}
