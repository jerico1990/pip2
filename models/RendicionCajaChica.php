<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "RendicionCajaChica".
 *
 * @property integer $ID
 * @property integer $CajaChicaID
 * @property string $CodigoProyecto
 * @property string $SolicitanteEncargo
 * @property string $Cargo
 * @property string $FechaEncargo
 * @property double $SaldoAnterior
 * @property double $Total
 * @property integer $Situacion
 * @property integer $Estado
 * @property string $FechaRegistro
 * @property integer $Correlativo
 */
class RendicionCajaChica extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $Fechas;
    public $Clases;
    public $NumerosDocumentos;
    public $Proveedores;
    public $DetallesGastos;
    public $Importes;
    public $IDs;
    public $Tipos;
    public $TiposDocumentos;
    public $Rucs;
    public $Y01;
    public $Bienes;
    public $Servicios;
    public $SaldoActualBienes;
    public $SaldoActualServicios;
    
    public static function tableName()
    {
        return 'RendicionCajaChica';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CajaChicaID', 'Situacion', 'Estado', 'Correlativo'], 'integer'],
            [['CodigoProyecto', 'SolicitanteEncargo', 'Cargo'], 'string'],
            [['SaldoActualBienes','SaldoActualServicios','TiposDocumentos','Y01','Rucs','Tipos','IDs','FechaEncargo', 'FechaRegistro','Fechas','Clases','NumerosDocumentos','Proveedores','DetallesGastos','Importes'], 'safe'],
            [['SaldoAnterior', 'Total'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'CajaChicaID' => 'Caja Chica ID',
            'CodigoProyecto' => 'Codigo Proyecto',
            'SolicitanteEncargo' => 'Solicitante Encargo',
            'Cargo' => 'Cargo',
            'FechaEncargo' => 'Fecha Encargo',
            'SaldoAnterior' => 'Saldo Anterior',
            'Total' => 'Total',
            'Situacion' => 'Situacion',
            'Estado' => 'Estado',
            'FechaRegistro' => 'Fecha Registro',
            'Correlativo' => 'Correlativo',
        ];
    }
}
