<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "RendicionEncargo".
 *
 * @property integer $ID
 * @property string $CodigoProyecto
 * @property integer $EncargoID
 * @property string $SIAF
 * @property string $ComprobantePago
 * @property integer $Situacion
 * @property integer $Estado
 * @property string $FechaRegistro
 */
class RendicionEncargo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $Fechas;
    public $Clases;
    public $NumerosDocumentos;
    public $Proveedores;
    public $DetallesGastos;
    public $Importes;
    public $IDs;
    public $Tipos;
    public $TiposDocumentos;
    public $Rucs;
    public $Y01;
    public $Bienes;
    public $Servicios;
    public $DeclaracionFechas;
    public $DeclaracionDetalles;
    public $DeclaracionImportes;
    public $IDDeclaracions;
    public static function tableName()
    {
        return 'RendicionEncargo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['ID'], 'required'],
            [['ID', 'EncargoID', 'Situacion', 'Estado'], 'integer'],
            [['CodigoProyecto', 'SIAF', 'ComprobantePago','InformeMotivoEncargo','InformeDetallarActividadesEncargo','InformeDetallarLogrosEncargo'], 'string'],
            [['IDDeclaracions','DeclaracionImportes','DeclaracionDetalles','DeclaracionFechas','TiposDocumentos','Y01','Rucs','Tipos','IDs','FechaEncargo', 'Fechas','Clases','NumerosDocumentos','Proveedores','DetallesGastos','Importes','FechaRegistro'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'CodigoProyecto' => 'Codigo Proyecto',
            'EncargoID' => 'Encargo ID',
            'SIAF' => 'Siaf',
            'ComprobantePago' => 'Comprobante Pago',
            'Situacion' => 'Situacion',
            'Estado' => 'Estado',
            'FechaRegistro' => 'Fecha Registro',
        ];
    }
}
