<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "InformeTecnico".
 *
 * @property integer $ID
 * @property integer $InvestigadorID
 * @property string $NroInfoTecnico
 * @property string $Extension
 * @property string $Fecha
 *
 * @property Postulante $investigador
 */
class InformeTecnico extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'InformeTecnico';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['InvestigadorID', 'NroInfoTecnico'], 'required'],
            [['InvestigadorID'], 'integer'],
            [['NroInfoTecnico', 'Extension'], 'string'],
            [['Fecha'], 'safe'],
            [['InvestigadorID'], 'exist', 'skipOnError' => true, 'targetClass' => Postulante::className(), 'targetAttribute' => ['InvestigadorID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'InvestigadorID' => 'Investigador ID',
            'NroInfoTecnico' => 'Nro Info Tecnico',
            'Extension' => 'Extension',
            'Fecha' => 'Fecha',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvestigador()
    {
        return $this->hasOne(Postulante::className(), ['ID' => 'InvestigadorID']);
    }
}
