<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Hito".
 *
 * @property integer $ID
 * @property integer $InformeTecnicoFinancieroID
 * @property string $CodigoProyecto
 * @property integer $Correlativo
 * @property string $Titulo
 * @property string $Descripcion
 * @property string $PeriodoInicio
 * @property string $PeriodoFin
 * @property integer $Cumplio
 * @property string $DescripcionCumplio
 * @property string $EjecucionFinanciera
 * @property string $Observacion
 * @property string $Logros
 */
class Hito extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Hito';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['InformeTecnicoFinancieroID', 'Correlativo', 'Cumplio'], 'integer'],
            [['CodigoProyecto', 'Titulo', 'Descripcion', 'DescripcionCumplio', 'EjecucionFinanciera', 'Observacion', 'Logros'], 'string'],
            [['PeriodoInicio', 'PeriodoFin'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'InformeTecnicoFinancieroID' => 'Informe Tecnico Financiero ID',
            'CodigoProyecto' => 'Codigo Proyecto',
            'Correlativo' => 'Correlativo',
            'Titulo' => 'Titulo',
            'Descripcion' => 'Descripcion',
            'PeriodoInicio' => 'Periodo Inicio',
            'PeriodoFin' => 'Periodo Fin',
            'Cumplio' => 'Cumplio',
            'DescripcionCumplio' => 'Descripcion Cumplio',
            'EjecucionFinanciera' => 'Ejecucion Financiera',
            'Observacion' => 'Observacion',
            'Logros' => 'Logros',
        ];
    }
}
