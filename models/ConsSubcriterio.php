<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ConsSubcriterio".
 *
 * @property integer $ID
 * @property integer $SubcriterioID
 * @property integer $InvestigadorID
 * @property string $ComentarioProponente
 *
 * @property Postulante $investigador
 */
class ConsSubcriterio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ConsSubcriterio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['SubcriterioID', 'InvestigadorID', 'ComentarioProponente'], 'required'],
            [['SubcriterioID', 'InvestigadorID'], 'integer'],
            [['ComentarioProponente'], 'string'],
            [['InvestigadorID'], 'exist', 'skipOnError' => true, 'targetClass' => Postulante::className(), 'targetAttribute' => ['InvestigadorID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'SubcriterioID' => 'Subcriterio ID',
            'InvestigadorID' => 'Investigador ID',
            'ComentarioProponente' => 'Comentario Proponente',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvestigador()
    {
        return $this->hasOne(Postulante::className(), ['ID' => 'InvestigadorID']);
    }
}
