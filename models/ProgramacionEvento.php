<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ProgramacionEvento".
 *
 * @property integer $ID
 * @property string $Nombre
 * @property integer $ActividadID
 * @property integer $Estado
 * @property integer $Situacion
 * @property string $CodigoProyecto
 * @property integer $MetaFisica
 */
class ProgramacionEvento extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ProgramacionEvento';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'required'],
            [['ID', 'ActividadID', 'Estado', 'Situacion', 'MetaFisica'], 'integer'],
            [['Nombre', 'CodigoProyecto'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nombre' => 'Nombre',
            'ActividadID' => 'Actividad ID',
            'Estado' => 'Estado',
            'Situacion' => 'Situacion',
            'CodigoProyecto' => 'Codigo Proyecto',
            'MetaFisica' => 'Meta Fisica',
        ];
    }
}
