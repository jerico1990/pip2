<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "AporteProyecto".
 *
 * @property integer $ID
 * @property integer $ProyectoID
 * @property integer $EntidadParticipanteID
 * @property string $Monetario
 * @property string $NoMonetario
 *
 * @property Proyecto $proyecto
 * @property AporteProyectoPC[] $aporteProyectoPCs
 */
class AporteProyecto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'AporteProyecto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ProyectoID', 'EntidadParticipanteID', 'Monetario', 'NoMonetario'], 'required'],
            [['ProyectoID', 'EntidadParticipanteID'], 'integer'],
            [['Monetario', 'NoMonetario'], 'number'],
            [['ProyectoID'], 'exist', 'skipOnError' => true, 'targetClass' => Proyecto::className(), 'targetAttribute' => ['ProyectoID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ProyectoID' => 'Proyecto ID',
            'EntidadParticipanteID' => 'Entidad Participante ID',
            'Monetario' => 'Monetario',
            'NoMonetario' => 'No Monetario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProyecto()
    {
        return $this->hasOne(Proyecto::className(), ['ID' => 'ProyectoID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAporteProyectoPCs()
    {
        return $this->hasMany(AporteProyectoPC::className(), ['AporteProyectoID' => 'ID']);
    }
}
