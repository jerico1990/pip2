<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "AporteAreSubCategoriaPC".
 *
 * @property integer $ID
 * @property integer $AporteAreSubCategoriaID
 * @property integer $PasoCriticoID
 * @property string $Monetario
 * @property string $NoMonetario
 *
 * @property AporteAreSubCategoria $aporteAreSubCategoria
 * @property PasoCritico $pasoCritico
 */
class AporteAreSubCategoriaPC extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'AporteAreSubCategoriaPC';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['AporteAreSubCategoriaID', 'PasoCriticoID', 'Monetario', 'NoMonetario'], 'required'],
            [['AporteAreSubCategoriaID', 'PasoCriticoID'], 'integer'],
            [['Monetario', 'NoMonetario'], 'number'],
            [['AporteAreSubCategoriaID'], 'exist', 'skipOnError' => true, 'targetClass' => AporteAreSubCategoria::className(), 'targetAttribute' => ['AporteAreSubCategoriaID' => 'ID']],
            [['PasoCriticoID'], 'exist', 'skipOnError' => true, 'targetClass' => PasoCritico::className(), 'targetAttribute' => ['PasoCriticoID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'AporteAreSubCategoriaID' => 'Aporte Are Sub Categoria ID',
            'PasoCriticoID' => 'Paso Critico ID',
            'Monetario' => 'Monetario',
            'NoMonetario' => 'No Monetario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAporteAreSubCategoria()
    {
        return $this->hasOne(AporteAreSubCategoria::className(), ['ID' => 'AporteAreSubCategoriaID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPasoCritico()
    {
        return $this->hasOne(PasoCritico::className(), ['ID' => 'PasoCriticoID']);
    }
}
