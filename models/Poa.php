<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Poa".
 *
 * @property integer $ID
 * @property integer $ProyectoID
 * @property integer $Situacion
 * @property integer $Estado
 * @property integer $Anno
 *
 * @property Componente[] $componentes
 * @property Proyecto $proyecto
 */
class Poa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Poa';
    }

    /**
     * @inheritdoc
     */
    public $Nombre;
    public function rules()
    {
        return [
            [['ProyectoID', 'Situacion', 'Estado', 'Anno'], 'integer'],
            [['ProyectoID'], 'exist', 'skipOnError' => true, 'targetClass' => Proyecto::className(), 'targetAttribute' => ['ProyectoID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ProyectoID' => 'Proyecto ID',
            'Situacion' => 'Situacion',
            'Estado' => 'Estado',
            'Anno' => 'Anno',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComponentes()
    {
        return $this->hasMany(Componente::className(), ['PoaID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProyecto()
    {
        return $this->hasOne(Proyecto::className(), ['ID' => 'ProyectoID']);
    }
}
