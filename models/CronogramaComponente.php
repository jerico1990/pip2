<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "CronogramaComponente".
 *
 * @property integer $ID
 * @property integer $ComponenteID
 * @property integer $Mes
 * @property string $MetaFinanciera
 * @property string $PoafMetaFinanciera
 *
 * @property Componente $componente
 */
class CronogramaComponente extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CronogramaComponente';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['ComponenteID', 'Mes', 'MetaFinanciera'], 'required'],
            [['ComponenteID', 'Mes'], 'integer'],
            [['MetaFinanciera', 'PoafMetaFinanciera'], 'number'],
            [['ComponenteID'], 'exist', 'skipOnError' => true, 'targetClass' => Componente::className(), 'targetAttribute' => ['ComponenteID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ComponenteID' => 'Componente ID',
            'Mes' => 'Mes',
            'MetaFinanciera' => 'Meta Financiera',
            'PoafMetaFinanciera' => 'Poaf Meta Financiera',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComponente()
    {
        return $this->hasOne(Componente::className(), ['ID' => 'ComponenteID']);
    }
}
