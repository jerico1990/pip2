<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ProductoPlanNegocios".
 *
 * @property integer $ID
 * @property integer $PlanNegociosID
 * @property string $Nombre
 * @property string $UnidadMedida
 *
 * @property PrincipalSupuesto[] $principalSupuestos
 * @property PlanNegocios $planNegocios
 */
class ProductoPlanNegocios extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ProductoPlanNegocios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['PlanNegociosID', 'Nombre', 'UnidadMedida'], 'required'],
            [['PlanNegociosID'], 'integer'],
            [['Nombre', 'UnidadMedida'], 'string'],
            [['PlanNegociosID'], 'exist', 'skipOnError' => true, 'targetClass' => PlanNegocios::className(), 'targetAttribute' => ['PlanNegociosID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'PlanNegociosID' => 'Plan Negocios ID',
            'Nombre' => 'Nombre',
            'UnidadMedida' => 'Unidad Medida',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrincipalSupuestos()
    {
        return $this->hasMany(PrincipalSupuesto::className(), ['ProductoPlanNegociosID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanNegocios()
    {
        return $this->hasOne(PlanNegocios::className(), ['ID' => 'PlanNegociosID']);
    }
}
