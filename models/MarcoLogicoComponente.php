<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "MarcoLogicoComponente".
 *
 * @property integer $ID
 * @property integer $ComponenteID
 * @property string $IndicadorTipo
 * @property string $IndicadorDescripcion
 * @property string $IndicadorUnidadMedida
 * @property string $IndicadorInicio
 * @property string $IndicadorMeta
 * @property string $MedioVerificacion
 * @property string $Supuestos
 *
 * @property Componente $componente
 */
class MarcoLogicoComponente extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $Peso;
    public $ComponenteNombre;
    public $ComponentePeso;
    public $ComponenteCorrelativo;
    public static function tableName()
    {
        return 'MarcoLogicoComponente';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ComponenteID'], 'required'],
            [['ComponenteID'], 'integer'],
            [['IndicadorTipo', 'IndicadorDescripcion', 'IndicadorUnidadMedida', 'MedioVerificacion', 'IndicadorMeta', 'Supuestos'], 'string'],
            [['IndicadorInicio','Peso'], 'safe'],
            //[['ComponenteID'], 'exist', 'skipOnError' => true, 'targetClass' => Componente::className(), 'targetAttribute' => ['ComponenteID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ComponenteID' => 'Componente ID',
            'IndicadorTipo' => 'Indicador Tipo',
            'IndicadorDescripcion' => 'Indicador Descripcion',
            'IndicadorUnidadMedida' => 'Indicador Unidad Medida',
            'IndicadorInicio' => 'Indicador Inicio',
            'IndicadorMeta' => 'Indicador Meta',
            'MedioVerificacion' => 'Medio Verificacion',
            'Supuestos' => 'Supuestos',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComponente()
    {
        return $this->hasOne(Componente::className(), ['ID' => 'ComponenteID']);
    }
}
