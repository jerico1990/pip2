<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ObservacionGeneral".
 *
 * @property integer $ID
 * @property string $CodigoProyecto
 * @property string $Observacion
 * @property integer $TipoObservacion
 * @property integer $CodigoID
 * @property integer $Estado
 */
class ObservacionGeneral extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ObservacionGeneral';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CodigoProyecto', 'Observacion','RespuestaObservacion'], 'string'],
            [['TipoObservacion', 'CodigoID', 'Estado'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'CodigoProyecto' => 'Codigo Proyecto',
            'Observacion' => 'Observacion',
            'TipoObservacion' => 'Tipo Observacion',
            'CodigoID' => 'Codigo ID',
            'Estado' => 'Estado',
        ];
    }
}
