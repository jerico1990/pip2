<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "EquidadInclusion".
 *
 * @property integer $ID
 * @property string $CodigoProyecto
 * @property integer $InformeTecnicoFinancieroID
 * @property string $Descripcion
 * @property integer $Hombres
 * @property integer $Mujeres
 * @property integer $Tipo
 * @property string $FechaRegistro
 * @property integer $Estado
 */
class EquidadInclusion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'EquidadInclusion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CodigoProyecto', 'Descripcion'], 'string'],
            [['InformeTecnicoFinancieroID', 'Hombres', 'Mujeres', 'Tipo', 'Estado'], 'integer'],
            [['FechaRegistro'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'CodigoProyecto' => 'Codigo Proyecto',
            'InformeTecnicoFinancieroID' => 'Informe Tecnico Financiero ID',
            'Descripcion' => 'Descripcion',
            'Hombres' => 'Hombres',
            'Mujeres' => 'Mujeres',
            'Tipo' => 'Tipo',
            'FechaRegistro' => 'Fecha Registro',
            'Estado' => 'Estado',
        ];
    }
}
