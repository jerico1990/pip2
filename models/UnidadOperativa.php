<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "UnidadOperativa".
 *
 * @property integer $ID
 * @property string $Nombre
 * @property integer $UnidadEjecutoriaID
 *
 * @property InformacionGeneral[] $informacionGenerals
 * @property UnidadEjecutora $unidadEjecutoria
 */
class UnidadOperativa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'UnidadOperativa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nombre'], 'string'],
            [['UnidadEjecutoraID'], 'integer'],
            [['UnidadEjecutoraID'], 'exist', 'skipOnError' => true, 'targetClass' => UnidadEjecutora::className(), 'targetAttribute' => ['UnidadEjecutoraID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nombre' => 'Nombre',
            'UnidadEjecutoraID' => 'Unidad Ejecutoria ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInformacionGenerals()
    {
        return $this->hasMany(InformacionGeneral::className(), ['UnidadOperativaID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnidadEjecutora()
    {
        return $this->hasOne(UnidadEjecutora::className(), ['ID' => 'UnidadEjecutoraID']);
    }
}
