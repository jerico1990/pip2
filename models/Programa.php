<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Programa".
 *
 * @property integer $ID
 * @property string $Nombre
 *
 * @property CultivoCrianza[] $cultivoCrianzas
 */
class Programa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Programa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nombre'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCultivoCrianzas()
    {
        return $this->hasMany(CultivoCrianza::className(), ['ProgramaID' => 'ID']);
    }
}
