<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "InformeTecnicoFinanciero".
 *
 * @property integer $ID
 * @property integer $PasoCriticoID
 * @property string $CodigoProyecto
 * @property double $MontoDesembolso
 * @property double $SaldoDesembolsoAnterior
 * @property string $ResumenEjecutivo
 * @property string $ProgramacionActividad
 * @property string $EjecucionFinancieraColaboradora
 * @property string $LeccionAprendida
 * @property string $Conclusion
 * @property string $Recomendacion
 * @property string $Anexo1
 * @property string $Anexo2
 * @property string $Anexo3
 * @property string $Anexo4
 * @property string $Anexo5
 * @property string $Anexo6
 * @property string $Anexo7
 * @property string $Anexo8
 * @property string $Anexo9
 * @property string $Anexo10
 * @property string $Anexo11
 * @property string $Anexo12
 * @property integer $Estado
 * @property integer $Situacion
 * @property string $FechaRegistro
 */
class InformeTecnicoFinanciero extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'InformeTecnicoFinanciero';
    }

    /**
     * @inheritdoc
     */
    public $TituloHito;
    public $DescripcionHito;
    public $DescripcionCumplio;
    public $EjecucionFinanciera;
    public $Observacion;
    public $Logros;
    public $PeriodoInicioHito;
    public $PeriodoFinHito;
    public $archivo;
    public $EjecucionMetaFisicaFinancieraDescripciones;
    public $EjecucionMetaFisicaFinancieraIDs;
    public $EjecucionMetaFisicaFinancieraActividadesIDs;
    public $ColaboradoraTipos;
    public $ColaboradoraMetasProgramadas;
    public $ColaboradoraMetasEjecutadas;
    public $ColaboradoraAvances;
    public $ColaboradoraDetalles;
    public $ColaboradoraIDs;
    public $EjecucionColaboradoraIDs;
    public $MedidasSocioAmbientales;
    public $MedidasSocioAmbientalesIDs;
    public $NPMes1;
    public $NPMes2;
    public $NPMes3;
    public $NPMes4;
    public $NPMes5;
    public $NPMes6;
    public $NPMes7;
    public $NPMes8;
    public $NPMes9;
    public $NPMes10;
    public $NPMes11;
    public $NPMes12;
    public $NPMes13;
    public $NPMes14;
    public $NPMes15;
    public $NPMes16;
    public $NPMes17;
    public $NPMes18;
    public $PMes1;
    public $PMes2;
    public $PMes3;
    public $PMes4;
    public $PMes5;
    public $PMes6;
    public $PMes7;
    public $PMes8;
    public $PMes9;
    public $PMes10;
    public $PMes11;
    public $PMes12;
    public $PMes13;
    public $PMes14;
    public $PMes15;
    public $PMes16;
    public $PMes17;
    public $PMes18;
    public $CCMes1;
    public $CCMes2;
    public $CCMes3;
    public $CCMes4;
    public $CCMes5;
    public $CCMes6;
    public $CCMes7;
    public $CCMes8;
    public $CCMes9;
    public $CCMes10;
    public $CCMes11;
    public $CCMes12;
    public $CCMes13;
    public $CCMes14;
    public $CCMes15;
    public $CCMes16;
    public $CCMes17;
    public $CCMes18;
    public $CAMes1;
    public $CAMes2;
    public $CAMes3;
    public $CAMes4;
    public $CAMes5;
    public $CAMes6;
    public $CAMes7;
    public $CAMes8;
    public $CAMes9;
    public $CAMes10;
    public $CAMes11;
    public $CAMes12;
    public $CAMes13;
    public $CAMes14;
    public $CAMes15;
    public $CAMes16;
    public $CAMes17;
    public $CAMes18;
    public $NPMesID1;
    public $NPMesID2;
    public $NPMesID3;
    public $NPMesID4;
    public $NPMesID5;
    public $NPMesID6;
    public $NPMesID7;
    public $NPMesID8;
    public $NPMesID9;
    public $NPMesID10;
    public $NPMesID11;
    public $NPMesID12;
    public $NPMesID13;
    public $NPMesID14;
    public $NPMesID15;
    public $NPMesID16;
    public $NPMesID17;
    public $NPMesID18;
    public $PMesID1;
    public $PMesID2;
    public $PMesID3;
    public $PMesID4;
    public $PMesID5;
    public $PMesID6;
    public $PMesID7;
    public $PMesID8;
    public $PMesID9;
    public $PMesID10;
    public $PMesID11;
    public $PMesID12;
    public $PMesID13;
    public $PMesID14;
    public $PMesID15;
    public $PMesID16;
    public $PMesID17;
    public $PMesID18;
    public $CCMesID1;
    public $CCMesID2;
    public $CCMesID3;
    public $CCMesID4;
    public $CCMesID5;
    public $CCMesID6;
    public $CCMesID7;
    public $CCMesID8;
    public $CCMesID9;
    public $CCMesID10;
    public $CCMesID11;
    public $CCMesID12;
    public $CCMesID13;
    public $CCMesID14;
    public $CCMesID15;
    public $CCMesID16;
    public $CCMesID17;
    public $CCMesID18;
    public $CAMesID1;
    public $CAMesID2;
    public $CAMesID3;
    public $CAMesID4;
    public $CAMesID5;
    public $CAMesID6;
    public $CAMesID7;
    public $CAMesID8;
    public $CAMesID9;
    public $CAMesID10;
    public $CAMesID11;
    public $CAMesID12;
    public $CAMesID13;
    public $CAMesID14;
    public $CAMesID15;
    public $CAMesID16;
    public $CAMesID17;
    public $CAMesID18;
    public $ParticipativaFechas;
    public $ParticipativaTemas;
    public $ParticipativaNumerosPersonas;
    public $ParticipativaComunidades;
    public $ParticipativaLugares;
    public $ParticipativaTiempos;
    public $ParticipativaHorasHombres;
    
    public $IncidenciaTipos;
    public $IncidenciaFechas;
    public $IncidenciaMedidasCorrectivas;
    public $IncidenciaMedidasPreventivas;
    public $IncidenciaNumerosInvolucrados;
    public $IncidenciaDanosPersonas;
    public $IncidenciaCostos;
    
    public $ParticipativaIDs;
    public $IncidenciaIDs;
    
    public $EquidadInclusionTipos;
    public $EquidadInclusionIDs;
    public $EquidadInclusionDescripciones;
    public $EquidadInclusionHombres;
    public $EquidadInclusionMujeres;
    
    public $ColaboradoraActividadesDetalles;
    
    public function rules()
    {
        return [
            [['PasoCriticoID', 'Estado', 'Situacion'], 'integer'],
            [['CodigoProyecto', 'ResumenEjecutivo', 'ProgramacionActividad', 'EjecucionFinancieraColaboradora', 'LeccionAprendida', 'Conclusion', 'Recomendacion', 'Anexo1', 'Anexo2', 'Anexo3', 'Anexo4', 'Anexo5', 'Anexo6', 'Anexo7', 'Anexo8', 'Anexo9', 'Anexo10', 'Anexo11', 'Anexo12'], 'string'],
            [['MontoDesembolso', 'SaldoDesembolsoAnterior'], 'number'],
            [['NPMes1','NPMes2','NPMes3','NPMes4','NPMes5','NPMes6','NPMes7','NPMes8','NPMes9','NPMes10','PMes1','PMes2','PMes3','PMes4','PMes5','PMes6','PMes7','PMes8','PMes9','PMes10',
              'CCMes1','CCMes2','CCMes3','CCMes4','CCMes5','CCMes6','CCMes7','CCMes8','CCMes9','CCMes10','CAMes1','CAMes2','CAMes3','CAMes4','CAMes5','CAMes6','CAMes7','CAMes8','CAMes9','CAMes10',
              'NPMesID1','NPMesID2','NPMesID3','NPMesID4','NPMesID5','NPMesID6','NPMesID7','NPMesID8','NPMesID9','NPMesID10','PMesID1','PMesID2','PMesID3','PMesID4','PMesID5','PMesID6','PMesID7','PMesID8','PMesID9','PMesID10',
              'CCMesID1','CCMesID2','CCMesID3','CCMesID4','CCMesID5','CCMesID6','CCMesID7','CCMesID8','CCMesID9','CCMesID10','CAMesID1','CAMesID2','CAMesID3','CAMesID4','CAMesID5','CAMesID6','CAMesID7','CAMesID8','CAMesID9','CAMesID10',
              'ListaInvolucrados','SeguridadOcupacional','DescribirResiduosPeligrosos','MedidasSocioAmbientalesIDs','MedidasSocioAmbientales',
              'EjecucionColaboradoraIDs','ColaboradoraIDs','ColaboradoraDetalles','ColaboradoraAvances','ColaboradoraMetasEjecutadas','ColaboradoraMetasProgramadas',
              'ColaboradoraTipos','EjecucionMetaFisicaFinancieraActividadesIDs','EjecucionMetaFisicaFinancieraIDs','EjecucionMetaFisicaFinancieraDescripciones',
              'archivo','TituloHito','DescripcionHito','DescripcionCumplio','EjecucionFinanciera','Observacion','Logros','PeriodoFinHito','PeriodoInicioHito','FechaRegistro',
              'FechaInforme','FechaDesembolsoPNIA','ParticipativaIDs','IncidenciaIDs','EquidadInclusionTipos','EquidadInclusionIDs','EquidadInclusionDescripciones','EquidadInclusionHombres','EquidadInclusionMujeres',
              'ParticipativaFechas','ParticipativaTemas','ParticipativaNumerosPersonas','ParticipativaComunidades','ParticipativaLugares','ParticipativaTiempos','ParticipativaHorasHombres',
              'IncidenciaTipos','IncidenciaFechas','IncidenciaMedidasCorrectivas','IncidenciaMedidasPreventivas','IncidenciaNumerosInvolucrados','IncidenciaDanosPersonas','IncidenciaCostos',
              'ColaboradoraActividadesDetalles','DescribirConsumoAgua'], 'safe'],
            [['archivo'],'file']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'PasoCriticoID' => 'Paso Critico ID',
            'CodigoProyecto' => 'Codigo Proyecto',
            'MontoDesembolso' => 'Monto Desembolso',
            'SaldoDesembolsoAnterior' => 'Saldo Desembolso Anterior',
            'ResumenEjecutivo' => 'Resumen Ejecutivo',
            'ProgramacionActividad' => 'Programacion Actividad',
            'EjecucionFinancieraColaboradora' => 'Ejecucion Financiera Colaboradora',
            'LeccionAprendida' => 'Leccion Aprendida',
            'Conclusion' => 'Conclusion',
            'Recomendacion' => 'Recomendacion',
            'Anexo1' => 'Anexo1',
            'Anexo2' => 'Anexo2',
            'Anexo3' => 'Anexo3',
            'Anexo4' => 'Anexo4',
            'Anexo5' => 'Anexo5',
            'Anexo6' => 'Anexo6',
            'Anexo7' => 'Anexo7',
            'Anexo8' => 'Anexo8',
            'Anexo9' => 'Anexo9',
            'Anexo10' => 'Anexo10',
            'Anexo11' => 'Anexo11',
            'Anexo12' => 'Anexo12',
            'Estado' => 'Estado',
            'Situacion' => 'Situacion',
            'FechaRegistro' => 'Fecha Registro',
        ];
    }
}
