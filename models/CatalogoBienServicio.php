<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "CatalogoBienServicio".
 *
 * @property integer $ID
 * @property string $Codigo
 * @property string $Descripcion
 * @property integer $Estado
 */
class CatalogoBienServicio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CatalogoBienServicio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Codigo', 'Descripcion'], 'string'],
            [['Estado'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Codigo' => 'Codigo',
            'Descripcion' => 'Descripcion',
            'Estado' => 'Estado',
        ];
    }
}
