<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ReProgramacionTarea".
 *
 * @property integer $ID
 * @property integer $TareaID
 * @property string $Justificacion
 * @property string $Evidencia
 * @property integer $Situacion
 * @property integer $Estado
 * @property string $FechaRegistro
 * @property string $CodigoProyecto
 */
class ReProgramacionTarea extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ReProgramacionTarea';
    }
    public $MetasFisicas;
    public $CostosUnitarios;
    public $Meses;
    public $Archivo;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TareaID', 'Situacion', 'Estado'], 'integer'],
            [['Justificacion', 'Evidencia', 'CodigoProyecto'], 'string'],
            [['FechaRegistro','CostosUnitarios','MetasFisicas','Meses','Archivo'], 'safe'],
            [['Archivo'],'file']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'TareaID' => 'Tarea ID',
            'Justificacion' => 'Justificacion',
            'Evidencia' => 'Evidencia',
            'Situacion' => 'Situacion',
            'Estado' => 'Estado',
            'FechaRegistro' => 'Fecha Registro',
            'CodigoProyecto' => 'Codigo Proyecto',
        ];
    }
}
