<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "DetalleGastosGenerales".
 *
 * @property integer $ID
 * @property integer $GastosGeneralesID
 * @property string $ObjetivoActividad
 * @property integer $Mes
 * @property double $MetaFisica
 * @property double $CostoUnitario
 * @property string $FechaRegistro
 */
class DetalleGastosGenerales extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DetalleGastosGenerales';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['GastosGeneralesID', 'Mes'], 'integer'],
            [['ObjetivoActividad','CodigoMatriz'], 'string'],
            [['MetaFisica', 'CostoUnitario'], 'number'],
            [['FechaRegistro'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'GastosGeneralesID' => 'Gastos Generales ID',
            'ObjetivoActividad' => 'Objetivo Actividad',
            'Mes' => 'Mes',
            'MetaFisica' => 'Meta Fisica',
            'CostoUnitario' => 'Costo Unitario',
            'FechaRegistro' => 'Fecha Registro',
        ];
    }
}
