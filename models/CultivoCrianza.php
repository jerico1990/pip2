<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "CultivoCrianza".
 *
 * @property integer $ID
 * @property string $Nombre
 * @property integer $ProgramaID
 *
 * @property Programa $programa
 * @property Especie[] $especies
 */
class CultivoCrianza extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CultivoCrianza';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nombre'], 'string'],
            [['ProgramaID'], 'integer'],
            [['ProgramaID'], 'exist', 'skipOnError' => true, 'targetClass' => Programa::className(), 'targetAttribute' => ['ProgramaID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nombre' => 'Nombre',
            'ProgramaID' => 'Programa ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrograma()
    {
        return $this->hasOne(Programa::className(), ['ID' => 'ProgramaID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEspecies()
    {
        return $this->hasMany(Especie::className(), ['CultivoCrianzaID' => 'ID']);
    }
}
