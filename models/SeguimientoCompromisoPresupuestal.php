<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "SeguimientoCompromisoPresupuestal".
 *
 * @property integer $ID
 * @property integer $CompromisoPresupuestalID
 * @property integer $UsuarioID
 * @property string $FechaInicio
 * @property string $FechaFin
 * @property integer $Dias
 * @property integer $Estado
 * @property integer $Situacion
 * @property string $FechaRegistro
 */
class SeguimientoCompromisoPresupuestal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SeguimientoCompromisoPresupuestal';
    }
    
    public $Nombre;
    public $ApellidoPaterno;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CompromisoPresupuestalID', 'UsuarioID', 'Dias', 'Estado', 'Situacion'], 'integer'],
            [['FechaInicio', 'FechaFin', 'FechaRegistro'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'CompromisoPresupuestalID' => 'Compromiso Presupuestal ID',
            'UsuarioID' => 'Usuario ID',
            'FechaInicio' => 'Fecha Inicio',
            'FechaFin' => 'Fecha Fin',
            'Dias' => 'Dias',
            'Estado' => 'Estado',
            'Situacion' => 'Situacion',
            'FechaRegistro' => 'Fecha Registro',
        ];
    }
}
