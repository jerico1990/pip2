<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "TipoInstitucion".
 *
 * @property integer $ID
 * @property string $Nombre
 *
 * @property EntidadParticipante[] $entidadParticipantes
 */
class TipoInstitucion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'TipoInstitucion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nombre'], 'required'],
            [['Nombre'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntidadParticipantes()
    {
        return $this->hasMany(EntidadParticipante::className(), ['TipoInstitucionID' => 'ID']);
    }
}
