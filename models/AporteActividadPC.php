<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "AporteActividadPC".
 *
 * @property integer $ID
 * @property integer $AporteActividadID
 * @property integer $PasoCriticoID
 * @property string $Monetario
 * @property string $NoMonetario
 *
 * @property AporteActividad $aporteActividad
 * @property PasoCritico $pasoCritico
 */
class AporteActividadPC extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'AporteActividadPC';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['AporteActividadID', 'PasoCriticoID', 'Monetario', 'NoMonetario'], 'required'],
            [['AporteActividadID', 'PasoCriticoID'], 'integer'],
            [['Monetario', 'NoMonetario'], 'number'],
            [['AporteActividadID'], 'exist', 'skipOnError' => true, 'targetClass' => AporteActividad::className(), 'targetAttribute' => ['AporteActividadID' => 'ID']],
            [['PasoCriticoID'], 'exist', 'skipOnError' => true, 'targetClass' => PasoCritico::className(), 'targetAttribute' => ['PasoCriticoID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'AporteActividadID' => 'Aporte Actividad ID',
            'PasoCriticoID' => 'Paso Critico ID',
            'Monetario' => 'Monetario',
            'NoMonetario' => 'No Monetario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAporteActividad()
    {
        return $this->hasOne(AporteActividad::className(), ['ID' => 'AporteActividadID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPasoCritico()
    {
        return $this->hasOne(PasoCritico::className(), ['ID' => 'PasoCriticoID']);
    }
}
