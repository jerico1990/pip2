<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "NivelAprobacion".
 *
 * @property integer $ID
 * @property string $EtapaCodigo
 * @property integer $PerfilID
 * @property integer $Correlativo
 * @property integer $Estado
 */
class NivelAprobacion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'NivelAprobacion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['EtapaCodigo'], 'string'],
            [['RolID', 'Correlativo', 'Estado'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'EtapaCodigo' => 'Etapa Codigo',
            'RolID' => 'Perfil ID',
            'Correlativo' => 'Correlativo',
            'Estado' => 'Estado',
        ];
    }
}
