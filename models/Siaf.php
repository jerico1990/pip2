<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class Siaf extends Model
{
    public $CodigoDocumento;
    public $NumeroDocumento;
    public $FechaDocumento;
    public $AnioDocumento;
    public $ConceptoDocumento;
    public $TipoID; //1 es cuando es RUC y si no 9
    public $RUCDocumento;
    public $FuenteFinanciamiento='19';
    public $TipoCambioDocumento;
    public $SiafCertificado;//10 caracters
    public $SiafSecuencial;//4 caractares
    public $SiafCorrelativo='0001';//4 caracteres
    public $MontoIGV;
    public $InterfaceCertificado="19";
    public $InterfaceSecuencial="0001";
    public $InterfaceCorrelativo='0001';
    public $Secuencia;
    public $TipoFinanciamiento='E';
    public $Correlativo;
    public $MetaFinanciera='0003';
    public $MontoTotal;
    public $Clasificador;
    public $MontoTotal1;
    public $Clasificador1;
    public $MesDocumento;
    public $Anio;
    public $Mes;
    public $Poa;
    public $TipoDoc;
    
    /**
     * @return array the validation rules.
     */
    
}
