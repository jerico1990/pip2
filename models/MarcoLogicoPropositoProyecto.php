<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "MarcoLogicoPropositoProyecto".
 *
 * @property integer $ID
 * @property integer $ProyectoID
 * @property string $IndicadorTipo
 * @property string $IndicadorDescripcion
 * @property string $IndicadorUnidadMedida
 * @property string $IndicadorInicio
 * @property string $IndicadorMeta
 * @property string $MedioVerificacion
 * @property string $Supuestos
 *
 * @property Proyecto $proyecto
 */
class MarcoLogicoPropositoProyecto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'MarcoLogicoPropositoProyecto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['ProyectoID', 'IndicadorTipo', 'IndicadorDescripcion', 'IndicadorUnidadMedida', 'IndicadorInicio', 'IndicadorMeta', 'MedioVerificacion', 'Supuestos'], 'required'],
            [['ProyectoID'], 'integer'],
            [['IndicadorTipo', 'IndicadorDescripcion', 'IndicadorUnidadMedida', 'MedioVerificacion', 'Supuestos'], 'string'],
            [['IndicadorInicio', 'IndicadorMeta'], 'number'],
            [['ProyectoID'], 'exist', 'skipOnError' => true, 'targetClass' => Proyecto::className(), 'targetAttribute' => ['ProyectoID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ProyectoID' => 'Proyecto ID',
            'IndicadorTipo' => 'Indicador Tipo',
            'IndicadorDescripcion' => 'Indicador Descripcion',
            'IndicadorUnidadMedida' => 'Indicador Unidad Medida',
            'IndicadorInicio' => 'Indicador Inicio',
            'IndicadorMeta' => 'Indicador Meta',
            'MedioVerificacion' => 'Medio Verificacion',
            'Supuestos' => 'Supuestos',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProyecto()
    {
        return $this->hasOne(Proyecto::className(), ['ID' => 'ProyectoID']);
    }
    
    public function getHabilitar()
    {
        if(\Yii::$app->user->identity->rol==7){
            return true;
        }
        else{
            return false;
        }
    }
}
