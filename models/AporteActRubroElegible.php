<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "AporteActRubroElegible".
 *
 * @property integer $ID
 * @property integer $ActRubroElegibleID
 * @property integer $EntidadParticipanteID
 * @property string $Monetario
 * @property string $NoMonetario
 *
 * @property ActRubroElegible $actRubroElegible
 * @property AporteActRubroElegiblePC[] $aporteActRubroElegiblePCs
 */
class AporteActRubroElegible extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'AporteActRubroElegible';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ActRubroElegibleID', 'EntidadParticipanteID', 'Monetario', 'NoMonetario'], 'required'],
            [['ActRubroElegibleID', 'EntidadParticipanteID'], 'integer'],
            [['Monetario', 'NoMonetario'], 'number'],
            [['ActRubroElegibleID'], 'exist', 'skipOnError' => true, 'targetClass' => ActRubroElegible::className(), 'targetAttribute' => ['ActRubroElegibleID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ActRubroElegibleID' => 'Act Rubro Elegible ID',
            'EntidadParticipanteID' => 'Entidad Participante ID',
            'Monetario' => 'Monetario',
            'NoMonetario' => 'No Monetario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActRubroElegible()
    {
        return $this->hasOne(ActRubroElegible::className(), ['ID' => 'ActRubroElegibleID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAporteActRubroElegiblePCs()
    {
        return $this->hasMany(AporteActRubroElegiblePC::className(), ['AporteActRubroElegibleID' => 'ID']);
    }
}
