<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "TipoRubroElegible".
 *
 * @property integer $ID
 * @property string $Nombre
 *
 * @property RubroElegible[] $rubroElegibles
 */
class TipoRubroElegible extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'TipoRubroElegible';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nombre'], 'required'],
            [['Nombre'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRubroElegibles()
    {
        return $this->hasMany(RubroElegible::className(), ['TipoRubroElegibleID' => 'ID']);
    }
}
