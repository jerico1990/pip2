<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "CronogramaTarea".
 *
 * @property integer $ID
 * @property integer $TareaID
 * @property integer $Mes
 *
 * @property Tarea $tarea
 */
class CronogramaTarea extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CronogramaTarea';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TareaID', 'Mes'], 'integer'],
            [['MetaFisica'],'number'],
            [['TareaID'], 'exist', 'skipOnError' => true, 'targetClass' => Tarea::className(), 'targetAttribute' => ['TareaID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'TareaID' => 'Tarea ID',
            'Mes' => 'Mes',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTarea()
    {
        return $this->hasOne(Tarea::className(), ['ID' => 'TareaID']);
    }
}
