<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Pat".
 *
 * @property integer $ID
 * @property integer $ProyectoID
 * @property integer $Situacion
 * @property integer $Estado
 */
class Pat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Pat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'required'],
            [['ID', 'ProyectoID', 'Situacion', 'Estado'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ProyectoID' => 'Proyecto ID',
            'Situacion' => 'Situacion',
            'Estado' => 'Estado',
        ];
    }
}
