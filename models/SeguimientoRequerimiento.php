<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "SeguimientoRequerimiento".
 *
 * @property integer $ID
 * @property integer $RequerimientoID
 * @property integer $UsuarioID
 * @property string $FechaInicio
 * @property string $FechaFin
 * @property integer $Dias
 * @property integer $Estado
 * @property integer $Situacion
 * @property string $FechaRegistro
 */
class SeguimientoRequerimiento extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SeguimientoRequerimiento';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['RequerimientoID', 'UsuarioID', 'Dias', 'Estado', 'Situacion'], 'integer'],
            [['FechaInicio', 'FechaFin', 'FechaRegistro'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'RequerimientoID' => 'Requerimiento ID',
            'UsuarioID' => 'Usuario ID',
            'FechaInicio' => 'Fecha Inicio',
            'FechaFin' => 'Fecha Fin',
            'Dias' => 'Dias',
            'Estado' => 'Estado',
            'Situacion' => 'Situacion',
            'FechaRegistro' => 'Fecha Registro',
        ];
    }
}
