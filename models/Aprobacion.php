<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Aprobacion".
 *
 * @property integer $ID
 * @property integer $ProyectoID
 * @property integer $NivelAprobacionID
 * @property integer $Situacion
 * @property integer $Estado
 */
class Aprobacion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Aprobacion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ProyectoID', 'NivelAprobacionID', 'Situacion', 'Estado'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ProyectoID' => 'Proyecto ID',
            'NivelAprobacionID' => 'Nivel Aprobacion ID',
            'Situacion' => 'Situacion',
            'Estado' => 'Estado',
        ];
    }
}
