<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "GastosGeneralesDetalle".
 *
 * @property integer $ID
 * @property string $CodigoMatriz
 * @property integer $DetalleGastoID
 * @property string $ObjetivoActividad
 * @property integer $Mes
 * @property integer $MetaFisica
 * @property double $CostoUnitario
 * @property string $FechaRegistro
 * @property integer $AreSubCategoriaID
 */
class GastosGeneralesDetalle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'GastosGeneralesDetalle';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CodigoMatriz', 'ObjetivoActividad'], 'string'],
            [['DetalleGastoID', 'Mes', 'MetaFisica', 'AreSubCategoriaID'], 'integer'],
            [['CostoUnitario'], 'number'],
            [['FechaRegistro'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'CodigoMatriz' => 'Codigo Matriz',
            'DetalleGastoID' => 'Detalle Gasto ID',
            'ObjetivoActividad' => 'Objetivo Actividad',
            'Mes' => 'Mes',
            'MetaFisica' => 'Meta Fisica',
            'CostoUnitario' => 'Costo Unitario',
            'FechaRegistro' => 'Fecha Registro',
            'AreSubCategoriaID' => 'Are Sub Categoria ID',
        ];
    }
}
