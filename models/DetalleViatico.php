<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "DetalleViatico".
 *
 * @property integer $ID
 * @property integer $ViaticoID
 * @property string $Tipo
 * @property string $Importe
 * @property integer $Estado
 */
class DetalleViatico extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DetalleViatico';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ViaticoID', 'Estado'], 'integer'],
            [['Tipo'], 'string'],
            [['Importe'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ViaticoID' => 'Viatico ID',
            'Tipo' => 'Tipo',
            'Importe' => 'Importe',
            'Estado' => 'Estado',
        ];
    }
}
