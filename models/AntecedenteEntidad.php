<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "AntecedenteEntidad".
 *
 * @property integer $ID
 * @property integer $EntidadParticipanteID
 * @property string $Antecedentes
 * @property integer $AsociadosHombres
 * @property integer $AsociadosMujeres
 * @property integer $AsociadosAtenderHombres
 * @property integer $AsociadosAtenderMujeres
 *
 * @property EntidadParticipante $entidadParticipante
 */
class AntecedenteEntidad extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'AntecedenteEntidad';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['EntidadParticipanteID', 'AsociadosHombres', 'AsociadosMujeres', 'AsociadosAtenderHombres', 'AsociadosAtenderMujeres'], 'required'],
            [['EntidadParticipanteID', 'AsociadosHombres', 'AsociadosMujeres', 'AsociadosAtenderHombres', 'AsociadosAtenderMujeres'], 'integer'],
            [['Antecedentes'], 'string'],
            [['EntidadParticipanteID'], 'exist', 'skipOnError' => true, 'targetClass' => EntidadParticipante::className(), 'targetAttribute' => ['EntidadParticipanteID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'EntidadParticipanteID' => 'Entidad Participante ID',
            'Antecedentes' => 'Antecedentes',
            'AsociadosHombres' => 'Asociados Hombres',
            'AsociadosMujeres' => 'Asociados Mujeres',
            'AsociadosAtenderHombres' => 'Asociados Atender Hombres',
            'AsociadosAtenderMujeres' => 'Asociados Atender Mujeres',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntidadParticipante()
    {
        return $this->hasOne(EntidadParticipante::className(), ['ID' => 'EntidadParticipanteID']);
    }
}
