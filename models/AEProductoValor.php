<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "AE_ProductoValor".
 *
 * @property integer $IdProductoValor
 * @property integer $IdProducto
 * @property integer $Anio
 * @property string $Cantidad
 * @property string $PrecioUnitario
 * @property string $CostoUnitario
 * @property integer $TipoProyecto
 *
 * @property AEProducto $idProducto
 */
class AEProductoValor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'AE_ProductoValor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['IdProducto', 'Anio', 'TipoProyecto'], 'required'],
            [['IdProducto', 'Anio', 'TipoProyecto'], 'integer'],
            [['Cantidad', 'PrecioUnitario', 'CostoUnitario'], 'number'],
            [['IdProducto'], 'exist', 'skipOnError' => true, 'targetClass' => AEProducto::className(), 'targetAttribute' => ['IdProducto' => 'IdProducto']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IdProductoValor' => 'Id Producto Valor',
            'IdProducto' => 'Id Producto',
            'Anio' => 'Anio',
            'Cantidad' => 'Cantidad',
            'PrecioUnitario' => 'Precio Unitario',
            'CostoUnitario' => 'Costo Unitario',
            'TipoProyecto' => 'Tipo Proyecto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProducto()
    {
        return $this->hasOne(AEProducto::className(), ['IdProducto' => 'IdProducto']);
    }
}
