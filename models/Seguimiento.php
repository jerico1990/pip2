<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Seguimiento".
 *
 * @property integer $ID
 * @property integer $ProyectoID
 * @property string $EtapaCodigo
 * @property integer $NivelAprobacionCorrelativo
 * @property integer $Situacion
 * @property integer $Estado
 */
class Seguimiento extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Seguimiento';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NivelAprobacionCorrelativo', 'Situacion', 'Estado'], 'integer'],
            [['EtapaCodigo','ProyectoCodigo','Observacion'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ProyectoCodigo' => 'Proyecto ID',
            'EtapaCodigo' => 'Etapa Codigo',
            'NivelAprobacionCorrelativo' => 'Nivel Aprobacion Correlativo',
            'Situacion' => 'Situacion',
            'Estado' => 'Estado',
        ];
    }
}
