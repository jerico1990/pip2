<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "AporteProyectoPC".
 *
 * @property integer $ID
 * @property integer $AporteProyectoID
 * @property integer $PasoCriticoID
 * @property string $Monetario
 * @property string $NoMonetario
 *
 * @property AporteProyecto $aporteProyecto
 * @property PasoCritico $pasoCritico
 */
class AporteProyectoPC extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'AporteProyectoPC';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['AporteProyectoID', 'PasoCriticoID', 'Monetario', 'NoMonetario'], 'required'],
            [['AporteProyectoID', 'PasoCriticoID'], 'integer'],
            [['Monetario', 'NoMonetario'], 'number'],
            [['AporteProyectoID'], 'exist', 'skipOnError' => true, 'targetClass' => AporteProyecto::className(), 'targetAttribute' => ['AporteProyectoID' => 'ID']],
            [['PasoCriticoID'], 'exist', 'skipOnError' => true, 'targetClass' => PasoCritico::className(), 'targetAttribute' => ['PasoCriticoID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'AporteProyectoID' => 'Aporte Proyecto ID',
            'PasoCriticoID' => 'Paso Critico ID',
            'Monetario' => 'Monetario',
            'NoMonetario' => 'No Monetario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAporteProyecto()
    {
        return $this->hasOne(AporteProyecto::className(), ['ID' => 'AporteProyectoID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPasoCritico()
    {
        return $this->hasOne(PasoCritico::className(), ['ID' => 'PasoCriticoID']);
    }
}
