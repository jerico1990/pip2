<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "LibroBanco".
 *
 * @property integer $ID
 * @property string $SECU_COMP_COP
 * @property string $FECH_EMIS_COP
 * @property string $FECH_PAGO_COP
 * @property string $NUM_CHEQ_CHE
 * @property string $DESC_ANEX_ANX
 * @property string $CONC_PAGO_COP
 * @property double $MONT_COMP_COP
 * @property double $MONT_SOLE_CHE
 * @property string $CODI_BANC_BAN
 * @property string $CODI_CNTA_CTA
 * @property string $NUME_CORR_CHE
 * @property string $NUME_SIAF_COP
 * @property integer $ESTADO
 */
class LibroBanco extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'LibroBanco';
    }

    /**
     * @inheritdoc
     */
    
    public function rules()
    {
        return [
            [['SECU_COMP_COP', 'NUM_CHEQ_CHE', 'DESC_ANEX_ANX', 'CONC_PAGO_COP', 'CODI_BANC_BAN', 'CODI_CNTA_CTA', 'NUME_CORR_CHE', 'NUME_SIAF_COP'], 'string'],
            [['FECH_EMIS_COP', 'FECH_PAGO_COP','MONT_SOLE_CHE','MONT_COMP_COP'], 'safe'],
            // [[ ], 'number'],
            [['ESTADO'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'SECU_COMP_COP' => 'Secu  Comp  Cop',
            'FECH_EMIS_COP' => 'Fech  Emis  Cop',
            'FECH_PAGO_COP' => 'Fech  Pago  Cop',
            'NUM_CHEQ_CHE' => 'Num  Cheq  Che',
            'DESC_ANEX_ANX' => 'Desc  Anex  Anx',
            'CONC_PAGO_COP' => 'Conc  Pago  Cop',
            'MONT_COMP_COP' => 'Mont  Comp  Cop',
            'MONT_SOLE_CHE' => 'Mont  Sole  Che',
            'CODI_BANC_BAN' => 'Codi  Banc  Ban',
            'CODI_CNTA_CTA' => 'Codi  Cnta  Cta',
            'NUME_CORR_CHE' => 'Nume  Corr  Che',
            'NUME_SIAF_COP' => 'Nume  Siaf  Cop',
            'ESTADO' => 'Estado',
        ];
    }
}
