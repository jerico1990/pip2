<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "CronogramaActRubroElegible".
 *
 * @property integer $ID
 * @property integer $ActRubroElegibleID
 * @property integer $Mes
 * @property string $MetaFinanciera
 * @property string $PoafMetaFinanciera
 *
 * @property ActRubroElegible $actRubroElegible
 */
class CronogramaActRubroElegible extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CronogramaActRubroElegible';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ActRubroElegibleID', 'Mes', 'MetaFinanciera'], 'required'],
            [['ActRubroElegibleID', 'Mes'], 'integer'],
            [['MetaFinanciera', 'PoafMetaFinanciera'], 'number'],
            [['ActRubroElegibleID'], 'exist', 'skipOnError' => true, 'targetClass' => ActRubroElegible::className(), 'targetAttribute' => ['ActRubroElegibleID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ActRubroElegibleID' => 'Act Rubro Elegible ID',
            'Mes' => 'Mes',
            'MetaFinanciera' => 'Meta Financiera',
            'PoafMetaFinanciera' => 'Poaf Meta Financiera',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActRubroElegible()
    {
        return $this->hasOne(ActRubroElegible::className(), ['ID' => 'ActRubroElegibleID']);
    }
}
