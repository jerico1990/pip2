<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "DetalleReProgramacionRecurso".
 *
 * @property integer $ID
 * @property double $MetaFisica
 * @property double $CostoUnitario
 * @property integer $Mes
 */
class DetalleReProgramacionRecurso extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DetalleReProgramacionRecurso';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['ID'], 'required'],
            [['ID', 'Mes'], 'integer'],
            [['MetaFisica', 'CostoUnitario'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'MetaFisica' => 'Meta Fisica',
            'CostoUnitario' => 'Costo Unitario',
            'Mes' => 'Mes',
        ];
    }
}
