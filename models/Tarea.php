<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Tarea".
 *
 * @property integer $ID
 * @property integer $ActividadID
 * @property string $Descripcion
 * @property string $Peso
 * @property string $UnidadMedida
 * @property integer $Cantidad
 * @property integer $Correlativo
 * @property integer $Estado
 *
 * @property CronogramaTarea[] $cronogramaTareas
 * @property Actividad $actividad
 */
class Tarea extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Tarea';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ActividadID', 'Correlativo'], 'integer'],
            //[['Descripcion', 'UnidadMedida'], 'string'],
            [['Peso','Descripcion', 'UnidadMedida','MetaFisica'], 'safe'],
            //[['ActividadID'], 'exist', 'skipOnError' => true, 'targetClass' => Actividad::className(), 'targetAttribute' => ['ActividadID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ActividadID' => 'Actividad ID',
            'Descripcion' => 'Descripcion',
            'Peso' => 'Peso',
            'UnidadMedida' => 'Unidad Medida',
            'MetaFisica' => 'Cantidad',
            'Correlativo' => 'Correlativo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCronogramaTareas()
    {
        return $this->hasMany(CronogramaTarea::className(), ['TareaID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActividad()
    {
        return $this->hasOne(Actividad::className(), ['ID' => 'ActividadID']);
    }
}
