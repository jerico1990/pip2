<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "DetalleRendicionEncargo".
 *
 * @property integer $ID
 * @property integer $RendicionEncargoID
 * @property string $FechaDocumento
 * @property string $ClaseDocumento
 * @property string $NumeroDocumento
 * @property string $Proveedor
 * @property string $DetalleGasto
 * @property double $Importe
 * @property string $RUC
 * @property integer $TipoDocumento
 */
class DetalleRendicionEncargo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DetalleRendicionEncargo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['RendicionEncargoID', 'TipoDocumento'], 'integer'],
            [['FechaDocumento'], 'safe'],
            [['ClaseDocumento', 'NumeroDocumento', 'Proveedor', 'DetalleGasto', 'RUC'], 'string'],
            [['Importe'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'RendicionEncargoID' => 'Rendicion Encargo ID',
            'FechaDocumento' => 'Fecha Documento',
            'ClaseDocumento' => 'Clase Documento',
            'NumeroDocumento' => 'Numero Documento',
            'Proveedor' => 'Proveedor',
            'DetalleGasto' => 'Detalle Gasto',
            'Importe' => 'Importe',
            'RUC' => 'Ruc',
            'TipoDocumento' => 'Tipo Documento',
        ];
    }
}
