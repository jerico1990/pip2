<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "FlujoCajaProyectado".
 *
 * @property integer $ID
 * @property integer $ComponenteID
 * @property integer $Anio
 * @property string $Valor
 *
 * @property Componente $componente
 */
class FlujoCajaProyectado extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'FlujoCajaProyectado';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ComponenteID', 'Anio', 'Valor'], 'required'],
            [['ComponenteID', 'Anio'], 'integer'],
            [['Valor'], 'number'],
            [['ComponenteID'], 'exist', 'skipOnError' => true, 'targetClass' => Componente::className(), 'targetAttribute' => ['ComponenteID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ComponenteID' => 'Componente ID',
            'Anio' => 'Anio',
            'Valor' => 'Valor',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComponente()
    {
        return $this->hasOne(Componente::className(), ['ID' => 'ComponenteID']);
    }
}
