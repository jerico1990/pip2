<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "AporteComponentePC".
 *
 * @property integer $ID
 * @property integer $AporteComponenteID
 * @property integer $PasoCriticoID
 * @property string $Monetario
 * @property string $NoMonetario
 *
 * @property AporteComponente $aporteComponente
 * @property PasoCritico $pasoCritico
 */
class AporteComponentePC extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'AporteComponentePC';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['AporteComponenteID', 'PasoCriticoID', 'Monetario', 'NoMonetario'], 'required'],
            [['AporteComponenteID', 'PasoCriticoID'], 'integer'],
            [['Monetario', 'NoMonetario'], 'number'],
            [['AporteComponenteID'], 'exist', 'skipOnError' => true, 'targetClass' => AporteComponente::className(), 'targetAttribute' => ['AporteComponenteID' => 'ID']],
            [['PasoCriticoID'], 'exist', 'skipOnError' => true, 'targetClass' => PasoCritico::className(), 'targetAttribute' => ['PasoCriticoID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'AporteComponenteID' => 'Aporte Componente ID',
            'PasoCriticoID' => 'Paso Critico ID',
            'Monetario' => 'Monetario',
            'NoMonetario' => 'No Monetario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAporteComponente()
    {
        return $this->hasOne(AporteComponente::className(), ['ID' => 'AporteComponenteID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPasoCritico()
    {
        return $this->hasOne(PasoCritico::className(), ['ID' => 'PasoCriticoID']);
    }
}
